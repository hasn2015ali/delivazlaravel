@extends('admin.master')

@section('title')
    {{__("subscriber.subscribers")}}
@endsection
@section('title_page')
    {{__("subscriber.subscribers")}}

@endsection

@section('content')
    <livewire:subscribers.subscriber-component  />
@endsection
