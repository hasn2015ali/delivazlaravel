<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    {{--    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">--}}
    {{--    <link rel="icon" type="image/png" href="../assets/img/favicon.png">--}}
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('admin/assets/css/now-ui-dashboard.css?v=1.5.0')}}" rel="stylesheet"/>
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('admin/assets/demo/demo.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/controlPanel.css')}}" rel="stylesheet"/>
    <link href="{{asset('dropzone/1.css')}}" rel="stylesheet"/>
    {{--    OwlCarousel2--}}
    {{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">--}}
    {{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" rel="stylesheet">--}}
    {{--  <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css" rel="stylesheet">--}}
    {{--    <link--}}
    {{--        rel="stylesheet"--}}
    {{--        href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"--}}
    {{--    />--}}

    {{--    alpine.js--}}
    <script src="{{ asset('js/alpine.js') }}" defer></script>

    <script src="{{asset('dropzone/2.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/js/core/jquery.min.js')}}"></script>

    <script src="{{asset('js/OwlCarousel2/js/owl.carousel.js')}}"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" type="text/javascript"></script>--}}

    {{--    sweetalert2--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>--}}
    <script src="{{asset('sweetAlert/sweetalert2.js')}}"></script>

    {{--    <link rel="stylesheet" href="sweetalert2.min.css">--}}

    {{--    arabic style--}}
    @if(app()->getLocale() == "ar")
        <link href="{{asset('css/controlPanel_ar.css')}}" rel="stylesheet"/>
    @endif
    {{--    font-awesome--}}
    {{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}

    @livewireStyles
</head>

<body class="rtl-active">
<div class="wrapper ">
    <div class="sidebar" data-color="orange">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
      -->
        <div style="background: white" class="logo">
            {{--            <a href="http://www.creative-tim.com" class="simple-text logo-mini">--}}
            {{--                CT--}}
            {{--            </a>--}}

            <a style="color: #f96332 !important;" href="/"
               class="simple-text logo-normal d-flex flex-row justify-content-start">
                {{--               <strong>{{__("masterControl.Delivaz")}}</strong> --}}
                <img style="    width: calc(75px + 2.5vw);height: calc(50px + 1.3vw);" src="{{asset('templateIMG/logo1.png')}}" class="">
{{--                <strong class="mt-5"> {{__("masterControl.Control_Panel")}}</strong>--}}
            </a>
        </div>

        <div class="sidebar-wrapper" id="sidebar-wrapper">
            <ul class="nav  ul-side-bar">
                {{--                <li class="dashboard @if($active=='dashboard') active @endif ">--}}
                {{--                    <a  href="{{route('dashboard')}}">--}}
                {{--                        <i class="now-ui-icons design_app"></i>--}}
                {{--                        <p>{{__("masterControl.Dashboard")}}</p>--}}
                {{--                    </a>--}}
                {{--                </li>--}}

                @canany([ 'is-manager','is-translator','is-data_entry_manager'])
                    <li class="@if($active=='Countries') active @endif">
                        <a href="{{route('showCountries',['locale'=>app()->getLocale()])}}">
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>{{__("country.Countries")}}</p>
                        </a>
                    </li>
                    {{--                    <li class="@if($active=='Countries') active @endif" data-toggle="collapse" data-target="#Countries">--}}
                    {{--                        <a>--}}
                    {{--                            <i class="now-ui-icons design_bullet-list-67"></i>--}}
                    {{--                            <p>  {{__("country.CountriesMenu")}}</p>--}}
                    {{--                        </a>--}}

                    {{--                        <div id="Countries" class="collapse handel-collapse change-pointer-event @if($active=='Countries') show @endif">--}}
                    {{--                            <a class=" dropdown-item   " href="{{route('showCountries',['locale'=>app()->getLocale()])}}">--}}
                    {{--                                <i class="now-ui-icons design_bullet-list-67"></i>--}}
                    {{--                                <p>{{__("country.Countries")}}</p>--}}
                    {{--                            </a>--}}
                    {{--                            @can('is-manager')--}}
                    {{--                                <a class="dropdown-item   " href="{{route('countrySetting',['locale'=>app()->getLocale()])}}">--}}
                    {{--                                    <i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("country.countryConfig")}}--}}
                    {{--                                </a>--}}
                    {{--                            @endcan--}}
                    {{--                        </div>--}}
                    {{--                    </li>--}}
                @endcan

                @canany([ 'is-manager','is-coo'])
                    <li class="@if($active=='users') active @endif" data-toggle="collapse" data-target="#demo">
                        <a>
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <p>  {{__("user.Users")}}</p>
                        </a>

                        <div id="demo"
                             class="collapse handel-collapse change-pointer-event @if($active=='users') show @endif">
                            @can('is-manager')
                                <a class=" dropdown-item   "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}"><i
                                        class="fas fa-user-cog" aria-hidden="true"></i> {{__("masterControl.Admins")}}
                                </a>
                                <a class=" dropdown-item  "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Translator'])}}"><i
                                        class="fas fa-user-cog"
                                        aria-hidden="true"></i>{{__("masterControl.translator")}}</a>
                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'COO'])}}"><i
                                        class="fas fa-user-cog" aria-hidden="true"></i>{{__("masterControl.coo")}}</a>
                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Data_Entry_Manager'])}}"><i
                                        class="fas fa-user-cog"
                                        aria-hidden="true"></i>{{__("masterControl.data_entry_manager")}}</a>
                            @endcan
                            @canany([ 'is-manager','is-coo'])
                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Delivery_System_Team'])}}"><i
                                        class="fas fa-user-cog"
                                        aria-hidden="true"></i>{{__("masterControl.Delivery_System_Team")}}</a>
                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Delivery_Costumer_Team'])}}"><i
                                        class="fas fa-user-cog"
                                        aria-hidden="true"></i>{{__("masterControl.Delivery_Costumer_Team")}}</a>
                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Delivery_workers'])}}"><i
                                        class="fas fa-user-cog"
                                        aria-hidden="true"></i>{{__("masterControl.Delivery_Workers")}}</a>


                                <a class=" dropdown-item "
                                   href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Customers'])}}"><i
                                        class="fas fa-user-cog" aria-hidden="true"></i>{{__("masterControl.Customer")}}
                                </a>
                            @endcan
                        </div>
                    </li>
                @endcan
                @canany(['is-manager','is-translator','is-data_entry_manager'])
                    <li class="@if($active=='restaurant') active @endif" data-toggle="collapse" data-target="#demo1r">
                        <a>
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>{{__("masterControl.menuRestaurant")}}</p>
                        </a>

                        <div id="demo1r"
                             class="collapse handel-collapse change-pointer-event @if($active=='restaurant') show @endif">
                            @canany(['is-manager','is-translator'])

                                <a class=" dropdown-item "
                                   href="{{route('restaurantFilter',['locale'=>app()->getLocale()])}}"><i
                                        class="fa fa-gear fa-spin"
                                        aria-hidden="true"></i>{{__("masterControl.filterRestaurant")}}</a>
                                @if($firstMealFilter)
                                    <a class=" dropdown-item "
                                       href="{{route('mealsFilters',['locale'=>app()->getLocale(),'id'=>$firstMealFilter->filter_id])}}"><i
                                            class="fa fa-gear fa-spin "
                                            aria-hidden="true"></i>{{__("masterControl.mailFilter")}}</a>
                                @endif
                            @endcan
                            @canany(['is-manager','is-data_entry_manager'])
                                <a class=" dropdown-item "
                                   href="{{route('restaurantRegistered',['locale'=>app()->getLocale()])}}"><i
                                        class="fa fa-gear fa-spin"
                                        aria-hidden="true"></i>{{__("masterControl.restaurantRegistered")}}
                                    <div class="d-flex flex-row">
{{--                                        <livewire:category.new-restuarnt-count-component />--}}
                                    </div>
                                </a>
                            @endcan
                        </div>
                    </li>

                    <li class="@if($active=='vendor') active @endif" data-toggle="collapse" data-target="#demo1v">
                        <a>
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>{{__("masterControl.vendorMenu")}}</p>
                        </a>

                        <div id="demo1v"
                             class="collapse handel-collapse change-pointer-event @if($active=='vendor') show @endif">
                            @canany(['is-manager','is-translator'])
                                <a class=" dropdown-item "
                                   href="{{route('vendorsFilter',['locale'=>app()->getLocale()])}}"><i
                                        class="fa fa-gear fa-spin"
                                        aria-hidden="true"></i>{{__("masterControl.filterVendors")}}</a>
                                @if($firstProductFiler)
                                    <a class=" dropdown-item "
                                       href="{{route('productsFilters',['locale'=>app()->getLocale(),'id'=>$firstProductFiler->filter_id])}}"><i
                                            class="fa fa-gear fa-spin "
                                            aria-hidden="true"></i>{{__("masterControl.productFilter")}}</a>
                                @endif
                            @endcan

                            <a class=" dropdown-item "
                               href="{{route('vendorRegistered',['locale'=>app()->getLocale()])}}"><i
                                    class="fa fa-gear fa-spin"
                                    aria-hidden="true"></i>{{__("masterControl.vendorRegistered")}}
                                <div class="d-flex flex-row">
{{--                                    <livewire:category.new-vendor-count-component />--}}
                                </div>
                            </a>

                        </div>
                    </li>
                @endcan

                @canany(['is-manager','is-translator'])
                    <li class="@if($active=='footerPages') active @endif" data-toggle="collapse" data-target="#demo1fp">
                        <a>
                            <i class="now-ui-icons design_bullet-list-67"></i>
                            <p>{{__("masterControl.footerPages")}}</p>
                        </a>

                        <div id="demo1fp"
                             class="collapse handel-collapse change-pointer-event @if($active=='footerPages') show @endif">
                            <a class=" dropdown-item "
                               href="{{route('terms-conditions',['locale'=>app()->getLocale()])}}"><i
                                    class="fa fa-gear fa-spin"
                                    aria-hidden="true"></i>{{__("masterControl.TERMSCONDITIONS")}}</a>
                            <a class=" dropdown-item "
                               href="{{route('privacy-policy',['locale'=>app()->getLocale()])}}"><i
                                    class="fa fa-gear fa-spin"
                                    aria-hidden="true"></i>{{__("masterControl.PRIVACYPOLICY")}}</a>
                            <a class=" dropdown-item "
                               href="{{route('cookies-policy',['locale'=>app()->getLocale()])}}"><i
                                    class="fa fa-gear fa-spin"
                                    aria-hidden="true"></i>{{__("masterControl.COOKIESPOLICY")}}</a>
                            <a class=" dropdown-item "
                               href="{{route('faq-page',['locale'=>app()->getLocale()])}}"><i
                                    class="fa fa-gear fa-spin"
                                    aria-hidden="true"></i>{{__("masterControl.FAQ")}}</a>
                        </div>
                    </li>
                @endcan


                {{--                @canany(['is-manager','is-data_entry_manager'])--}}
                {{--                    <li class="@if($active=='pages') active @endif" data-toggle="collapse" data-target="#demo1rp">--}}
                {{--                        <a>--}}
                {{--                            <i class="now-ui-icons design_bullet-list-67"></i>--}}
                {{--                            <p>{{__("masterControl.PagesMenu")}}</p>--}}
                {{--                        </a>--}}

                {{--                        <div id="demo1rp" class="collapse handel-collapse change-pointer-event @if($active=='pages') show @endif">--}}

                {{--                            @canany(['is-manager','is-data_entry_manager'])--}}
                {{--                                <a class=" dropdown-item " href="{{route('restaurantsPage',['locale'=>app()->getLocale()])}}"><i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.RestaurantsPage")}}</a>--}}
                {{--                                <a class=" dropdown-item " href="{{route('vendorsPage',['locale'=>app()->getLocale()])}}"><i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.ShopsPage")}}</a>--}}
                {{--                            @endcan--}}
                {{--                        </div>--}}
                {{--                    </li>--}}
                {{--                @endcan--}}

                {{--                    <li class="@if($active=='section') active @endif" data-toggle="collapse" data-target="#demo1">--}}
                {{--                        <a>--}}
                {{--                            <i class="now-ui-icons design_bullet-list-67"></i>--}}
                {{--                            <p>{{__("masterControl.menu")}}</p>--}}
                {{--                        </a>--}}

                {{--                        <div id="demo1" class="collapse handel-collapse change-pointer-event @if($active=='section') show @endif">--}}
                {{--                            <a class=" dropdown-item " href="{{route('restaurantFilter',['locale'=>app()->getLocale()])}}"><i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.filterRestaurant")}}</a>--}}
                {{--                            <a class=" dropdown-item " href="{{route('vendorsFilter',['locale'=>app()->getLocale()])}}"><i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.filterVendors")}}</a>--}}

                {{--                            <a class=" dropdown-item " href="{{route('showSection',['locale'=>app()->getLocale()])}}"><i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.sectionM")}}</a>--}}
                {{--                            @if($firstService) <a class=" dropdown-item " href="{{route('showSectionChildrein',['locale'=>app()->getLocale(),'id'=>$firstService->section_id])}}"><i class="fa fa-gear fa-spin " aria-hidden="true"></i>{{__("masterControl.serviceM")}}</a> @endif--}}
                {{--                            @if($firstCategory) <a class=" dropdown-item " href="{{route('showCategory',['locale'=>app()->getLocale(),'id'=>$firstCategory->sub_section_id])}}"> <i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.categoryM")}}</a> @endif--}}
                {{--                            @if($firstProduct)  <a class=" dropdown-item " href="{{route('showProduct',['locale'=>app()->getLocale(),'id'=>$firstProduct->category_id])}}"> <i class="fa fa-gear fa-spin" aria-hidden="true"></i>{{__("masterControl.productM")}}</a> @endif--}}

                {{--                        </div>--}}
                {{--                    </li>--}}


                @canany(['is-manager','is-data_entry_manager'])
                    <li class="@if($active=='Subscriber') active @endif">
                        <a href="{{route('subscribers',['locale'=>app()->getLocale()])}}">
                            <i class="fa fa-users" aria-hidden="true"></i>
                            <p>{{__("masterControl.Subscribers")}}</p>
                        </a>
                    </li>
                @endcan

                @can('is-manager')
                    <li class="@if($active=='workSystem') active @endif">
                        <a href="{{route('workSystem',['locale'=>app()->getLocale()])}}">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <p>{{__("workSystem.workSystem")}}</p>
                        </a>
                    </li>
                @endcan

                @canany(['is-manager','is_delivery_system_team'])
                    <li class="@if($active=='orders') active @endif">
                        <a href="{{route('system.orders',['locale'=>app()->getLocale()])}}">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>

                            {{--                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                                <path d="M15.55 13C16.3 13 16.96 12.59 17.3 11.97L20.88 5.48C21.25 4.82 20.77 4 20.01 4H5.21L4.27 2H1V4H3L6.6 11.59L5.25 14.03C4.52 15.37 5.48 17 7 17H19V15H7L8.1 13H15.55ZM6.16 6H18.31L15.55 11H8.53L6.16 6ZM7 18C5.9 18 5.01 18.9 5.01 20C5.01 21.1 5.9 22 7 22C8.1 22 9 21.1 9 20C9 18.9 8.1 18 7 18ZM17 18C15.9 18 15.01 18.9 15.01 20C15.01 21.1 15.9 22 17 22C18.1 22 19 21.1 19 20C19 18.9 18.1 18 17 18Z" fill="#FE2E17"/>--}}
{{--                            </svg>--}}
                            <p>

                                {{__("orders.orders")}}
                            </p>
                        </a>
                    </li>
                @endcan

                @canany(['is-manager'])
                    <li class="@if($active=='taxes') active @endif">
                        <a href="{{route('taxes.show',['locale'=>app()->getLocale()])}}">
                            <i class="fa fa-money" aria-hidden="true"></i>

                            {{--                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
                            {{--                                <path d="M15.55 13C16.3 13 16.96 12.59 17.3 11.97L20.88 5.48C21.25 4.82 20.77 4 20.01 4H5.21L4.27 2H1V4H3L6.6 11.59L5.25 14.03C4.52 15.37 5.48 17 7 17H19V15H7L8.1 13H15.55ZM6.16 6H18.31L15.55 11H8.53L6.16 6ZM7 18C5.9 18 5.01 18.9 5.01 20C5.01 21.1 5.9 22 7 22C8.1 22 9 21.1 9 20C9 18.9 8.1 18 7 18ZM17 18C15.9 18 15.01 18.9 15.01 20C15.01 21.1 15.9 22 17 22C18.1 22 19 21.1 19 20C19 18.9 18.1 18 17 18Z" fill="#FE2E17"/>--}}
                            {{--                            </svg>--}}
                            <p>

                                {{__("tax.taxes")}}
                            </p>
                        </a>
                    </li>
                @endcan

                {{--                <li>--}}
                {{--                    <a href="./notifications.html">--}}
                {{--                        <i class="now-ui-icons ui-1_bell-53"></i>--}}
                {{--                        <p>Notifications</p>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li>--}}
                {{--                    <a href="addUser.blade.php">--}}
                {{--                        <i class="now-ui-icons users_single-02"></i>--}}
                {{--                        <p>User Profile</p>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li>--}}
                {{--                    <a href="users.blade.php">--}}
                {{--                        <i class="now-ui-icons design_bullet-list-67"></i>--}}
                {{--                        <p> Table List</p>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                {{--                <li>--}}
                {{--                    <a href="./typography.html">--}}
                {{--                        <i class="now-ui-icons text_caps-small"></i>--}}
                {{--                        <p>Typography</p>--}}
                {{--                    </a>--}}
                {{--                </li>--}}

            </ul>
        </div>
    </div>
    <div class="main-panel" id="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler btn-ar-open">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="#pablo">  @yield('title_page')</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                    <span class="navbar-toggler-bar navbar-kebab"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navigation">
                    <form>
                        <div class="input-group no-border">
                            <input type="text" value="" class="form-control" id="myInput" placeholder="Search...">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <i class="now-ui-icons ui-1_zoom-bold"></i>
                                </div>
                            </div>
                        </div>
                    </form>
                    <ul class="navbar-nav">
                        <li class="nav-item">
{{--                            <a class="nav-link" href="#pablo">--}}
                                <livewire:user.notifictation-component />

                                {{--                                <i class="now-ui-icons media-2_sound-wave"></i>--}}
{{--                                <p>--}}
{{--                                    <span class="d-lg-none d-md-block">Stats</span>--}}
{{--                                </p>--}}
{{--                            </a>--}}
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="now-ui-icons location_world"></i>
                                <p>
                                    <span class="">
                                        EN <img src="{{asset('assets/images/flags/us.png')}}" alt="">
                                      </span>
                                </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                {{--                                <a class="dropdown-item" href="#">EN</a>--}}
                                {{--                                <a class="dropdown-item" href="#">العربية</a>--}}
                                {{--                                <a class="dropdown-item" href="#">Something else here</a>--}}
                                <a class="dropdown-item"
                                   href="{{str_replace("/".app()->getLocale()."/","/en/", url()->current() )}}">EN</a>
                                <a class="dropdown-item"
                                   href="{{ str_replace("/".app()->getLocale()."/","/ar/", url()->current() )}}">العربية</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink1" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="now-ui-icons users_single-02"></i>

                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">

                                <a class="dropdown-item" href="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                                      method="POST">
                                    @csrf
                                </form>

                                <a class="dropdown-item"
                                   href="{{ route('Dashboard',['locale'=>app()->getLocale()]) }}">
                                    {{__("profile.myprofile1")}}
                                </a>
                            </div>
                            {{--                            <a class="nav-link" href="#pablo">--}}
                            {{--                              --}}
                            {{--                                <p>--}}
                            {{--                                   --}}
                            {{--                                    <span class="d-lg-none d-md-block">Account</span>--}}
                            {{--                                </p>--}}
                            {{--                            </a>--}}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        @yield('content')
        {{--        <footer class="footer">--}}
        {{--            <div class=" container-fluid ">--}}
        {{--                <nav>--}}
        {{--                    <ul>--}}
        {{--                        <li>--}}
        {{--                            <a href="https://www.creative-tim.com">--}}
        {{--                                Creative Tim--}}
        {{--                            </a>--}}
        {{--                        </li>--}}
        {{--                        <li>--}}
        {{--                            <a href="http://presentation.creative-tim.com">--}}
        {{--                                About Us--}}
        {{--                            </a>--}}
        {{--                        </li>--}}
        {{--                        <li>--}}
        {{--                            <a href="http://blog.creative-tim.com">--}}
        {{--                                Blog--}}
        {{--                            </a>--}}
        {{--                        </li>--}}
        {{--                    </ul>--}}
        {{--                </nav>--}}
        {{--                <div class="copyright" id="copyright">--}}
        {{--                    &copy; <script>--}}
        {{--                        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))--}}
        {{--                    </script>, Designed by <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </footer>--}}
    </div>
</div>
@livewireScripts
<!--   Core JS Files   -->


<script src="{{asset('admin/assets/js/core/popper.min.js')}}"></script>
<script src="{{asset('admin/assets/js/core/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>-->
<!-- Chart JS -->
<script src="{{asset('admin/assets/js/plugins/chartjs.min.js')}}"></script>
<!--  Notifications Plugin    -->
<script src="{{asset('admin/assets/js/plugins/bootstrap-notify.js')}}"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="{{asset('admin/assets/js/now-ui-dashboard.min.js?v=1.5.0')}}" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="{{asset('admin/assets/demo/demo.js')}}"></script>
<script>
    var PUSHER_APP_KEY = "{{ env('PUSHER_APP_KEY') }}";
    var PUSHER_APP_CLUSTER = "{{ env('PUSHER_APP_CLUSTER') }}";

</script>
{{--<script src="https://js.pusher.com/7.0/pusher.min.js"></script>--}}
{{--<script src="{{asset('js/pusherScripts.js')}}" ></script>--}}
<script src="{{asset('js/controlPanel.js')}}" defer></script>
<script src="{{asset('js/liveWireEvents.js')}}" defer></script>

@if(app()->getLocale() == "ar")
    <script src="{{asset('js/controlPanelAr.js')}}" defer></script>

@endif
<script>
    $(document).ready(function () {
        $("#myInput").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

<script>


</script>
</body>

</html>
