@extends('admin.master')

@section('title')
    {{__("restaurant.restaurant")}}
@endsection
@section('title_page')
    {{__("restaurant.restaurantMenu")}}

@endsection

@section('content')
    <livewire:category.restaurant-component />
@endsection
