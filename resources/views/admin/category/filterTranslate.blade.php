@extends('admin.master')

@section('title')
    {{__("filter.filterTrans")}}
@endsection
@section('title_page')
    {{__("filter.translationsMenu")}}

@endsection

@section('content')
    <livewire:category.filter-trans-component :filterId="$id" />
@endsection
