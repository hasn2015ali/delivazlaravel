@extends('admin.master')

@section('title')
    {{__("vendor.vendorSetting")}}
@endsection
@section('title_page')
    {{__("vendor.vendorSetting")}}

@endsection

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header  p-0">
                        <div class="restaurant-photo" style="">
                            <div class="owl-carousel owl-theme">
                                @if($shopBanners)
                                    @foreach($shopBanners as $banner)
                                        <div class="item"><img
                                                src="{{asset('storage/images/sliders/vendor/'.$banner->name)}}"></div>
                                    @endforeach
                                @endif

                            </div>

                        </div>

                    {{--                        @include('livewire.category.restuarant-setting.nav')--}}
                    <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link   "  href="{{route('shopDetails',['locale'=>app()->getLocale(),'id'=>$vendorID])}}" > {{__("vendor.About")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  " href="{{route('shopFilters',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.Filters")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('shopProducts',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.Product")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('shopWorkHours',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.work")}}</a>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="{{route('shopBanners',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.banner")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('shopDocuments',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.document")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{route('shopRatting',['locale'=>app()->getLocale(),'id'=>$vendorID])}}">{{__("vendor.ReviewAndRatting")}}</a>

                            </li>
                        </ul>

                    </div>

                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane mt-3  active">
                                <livewire:category.vendor-setting.ratting-component :vendorID="$vendorID"/>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: true,

                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            })
        });
    </script>
    <style>
        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: 3vw !important;
            height: 0.8vh !important;
        }

    </style>


@endsection
