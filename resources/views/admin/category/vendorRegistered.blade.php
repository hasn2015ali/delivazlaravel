@extends('admin.master')

@section('title')
    {{__("vendor.vendor")}}
@endsection
@section('title_page')
    {{__("vendor.vendorMenu")}}

@endsection

@section('content')
    <livewire:category.vendor-component />
@endsection
