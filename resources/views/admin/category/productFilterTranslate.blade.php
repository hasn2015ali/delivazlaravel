@extends('admin.master')

@section('title')
    {{__("filter.productFilterTrans")}}
@endsection
@section('title_page')
    {{__("filter.productFilterTranslationsMenu")}}

@endsection

@section('content')
    <livewire:category.product-filter-trans-component :productFilterId="$id" />
@endsection
