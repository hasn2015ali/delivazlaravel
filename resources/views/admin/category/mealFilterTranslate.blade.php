@extends('admin.master')

@section('title')
    {{__("filter.mealFilterTrans")}}
@endsection
@section('title_page')
    {{__("filter.mealFilterTranslationsMenu")}}

@endsection

@section('content')
    <livewire:category.meal-filter-trans-component :mealFilterId="$id" />
@endsection
