@extends('admin.master')

@section('title')
    {{__("filter.filters")}}
@endsection
@section('title_page')
    {{__("filter.filters_Menu")}}
@endsection

@section('content')
{{--    {{dd($id)}}--}}

    <livewire:category.meal-filter-component :filterID="$id"/>

@endsection

