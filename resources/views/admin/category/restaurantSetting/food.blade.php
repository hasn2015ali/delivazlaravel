@extends('admin.master')

@section('title')
    {{__("restaurant.restaurantSetting")}}
@endsection
@section('title_page')
    {{__("restaurant.restaurantSetting")}}

@endsection

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header  p-0">
                        <div class="restaurant-photo" style="">
                            <div class="owl-carousel owl-theme">
                                @if($restaurantBanners)
                                    @foreach($restaurantBanners as $banner)
                                        <div class="item"><img
                                                src="{{asset('storage/images/sliders/restaurant/'.$banner->name)}}"></div>
                                    @endforeach
                                @endif

                            </div>

                        </div>

                    {{--                        @include('livewire.category.restuarant-setting.nav')--}}
                    <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link   "  href="{{route('restaurantDetails',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}" > {{__("restaurant.About")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  " href="{{route('restaurantFilters',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.Filters")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{route('restaurantFood',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.Food")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('restaurantWorkHours',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.work")}}</a>

                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="{{route('restaurantBanners',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.banner")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('restaurantDocuments',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.document")}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('restaurantRatting',['locale'=>app()->getLocale(),'id'=>$restaurantID])}}">{{__("restaurant.ReviewAndRatting")}}</a>

                            </li>
                        </ul>

                    </div>

                    <div class="card-body">
                        <div class="tab-content">

                            <div class="tab-pane mt-3  active">
                                <livewire:category.restuarant-setting.food-component :restaurantID="$restaurantID"/>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.owl-carousel').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: true,

                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            })
        });
    </script>
    <style>
        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: 3vw !important;
            height: 0.8vh !important;
        }

    </style>


@endsection
