@extends('admin.master')

@section('title')
    {{__("filter.productFilters")}}
@endsection
@section('title_page')
    {{__("filter.productFilters_Menu")}}
@endsection

@section('content')

        <livewire:category.product-filter-component :filterID="$id" />

@endsection

