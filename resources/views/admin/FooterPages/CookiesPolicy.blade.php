@extends('admin.master')

@section('title')
    {{__("footerPages.cp")}}
@endsection
@section('title_page')
    {{__("footerPages.cp")}}

@endsection

@section('content')
    <livewire:footer-pages.terms-component type="cookie"/>
@endsection
