@extends('admin.master')

@section('title')
    {{__("footerPages.FAQ")}}
@endsection
@section('title_page')
    {{__("footerPages.FAQ")}}

@endsection

@section('content')
    <livewire:footer-pages.terms-component type="FAQ"/>
@endsection
