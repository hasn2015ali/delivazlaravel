@extends('admin.master')

@section('title')
    {{__("footerPages.PP")}}
@endsection
@section('title_page')
    {{__("footerPages.PP")}}

@endsection

@section('content')
    <livewire:footer-pages.terms-component type="privacy"/>
@endsection
