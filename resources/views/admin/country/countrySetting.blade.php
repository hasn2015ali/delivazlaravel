@extends('admin.master')

@section('title')
    {{__("country.Countries")}}
@endsection
@section('title_page')
    {{__("country.countryConfig")}}

@endsection

@section('content')

    <livewire:country.country-details-component :counytryID="$id" />

@endsection
