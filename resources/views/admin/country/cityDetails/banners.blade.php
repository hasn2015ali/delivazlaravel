@extends('admin.master')

@section('title')
    {{__("city.cities")}}
@endsection
@section('title_page')
    {{__("city.cityConfig")}}

@endsection

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div  class="card-header  p-0">
                        <div class="restaurant-photo" style="">
                            <div class="owl-carousel owl-theme">
                                @if($cityBanners)
                                    @foreach($cityBanners as $banner)
                                        <div class="item"> <img  src="{{asset('storage/images/sliders/city/'.$banner->name)}}"> </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                            <li class="breadcrumb-item"><a href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a> </li>
                            <li class="breadcrumb-item ">  {{$country->name}}</li>
                            <li class="breadcrumb-item ">  <a href="{{route('showCountryCity',['locale'=>app()->getLocale(),'id'=>$country->country_id])}}"> Cities </a> </li>
                            <li class="breadcrumb-item active">  {{$city->name}}</li>

                        </ol>

                    @include('admin.country.cityDetails.nav')
                    <!-- Tab panes -->


                    </div>

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane mt-3  active " >
                                <livewire:country.city-details.banner-component :cityID="$id"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('.owl-carousel').owlCarousel({
                autoplay:true,
                autoplayHoverPause:true,
                smartSpeed:250,
                loop:true,
                // margin:10,
                center:true,
                // // nav:true,
                dots:true,

                // items:1,
                margin:0,
                stagePadding:0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:1
                    },
                    1000:{
                        items:1
                    }
                }
            })
        });
    </script>
    <style>
        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span
        {
            background: #F96332!important;
        }
        .owl-theme .owl-dots  span
        {
            width: 3vw!important;
            height: 0.8vh!important;
        }
        /*.card-header{*/
        /*    border-radius: 15px!important;*/

        /*}*/
        /*.owl-carousel img{*/

        /*        border-radius: 15px!important;*/


        /*}*/
    </style>


@endsection
