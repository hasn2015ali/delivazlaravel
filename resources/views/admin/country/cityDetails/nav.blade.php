<!-- Nav tabs -->
@php
$locale=\Illuminate\Support\Facades\App::getLocale();
@endphp
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="shapes" ) active @endif"
          href="{{route('cityShapes',['locale'=>$locale,'id'=>$id])}}"
         >
            {{__("city.shape")}}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="Banners" ) active @endif"
           href="{{route('cityBanners',['locale'=>$locale,'id'=>$id])}}"
         >{{__("city.banner")}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="Recommended" ) active @endif"
           href="{{route('cityServiceProviders',['locale'=>$locale,'id'=>$id])}}">
            {{__("city.Recommended")}}
        </a>
    </li>


    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="restaurantsPage" ) active @endif"
           href="{{route('cityServiceProviderPages',['locale'=>$locale,'id'=>$id])}}">
            {{__("city.restaurantsPage")}}
        </a>

    </li>


    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="areas" ) active @endif"
           href="{{route('city.areas',['locale'=>$locale,'id'=>$id])}}">
            {{__("city.areas")}}
        </a>

    </li>

    <li class="nav-item">
        <a class="nav-link @if (session()->get('cityDetails')=="delivery" ) active @endif"
           href="{{route('city.delivery',['locale'=>$locale,'id'=>$id])}}">
            {{__("city.deliverySetting")}}
        </a>

    </li>

</ul>
