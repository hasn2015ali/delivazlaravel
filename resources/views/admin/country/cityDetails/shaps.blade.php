@extends('admin.master')

@section('title')
    {{__("city.cities")}}
@endsection
@section('title_page')
    {{__("city.cityConfig")}}

@endsection

@section('content')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div  class="card-header  p-0">

                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                            <li class="breadcrumb-item"><a href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a> </li>
                            <li class="breadcrumb-item ">  {{$country->name}}</li>
                            <li class="breadcrumb-item ">  <a href="{{route('showCountryCity',['locale'=>app()->getLocale(),'id'=>$country->country_id])}}"> Cities </a> </li>
                            <li class="breadcrumb-item active">  {{$city->name}}</li>

                        </ol>
                    @include('admin.country.cityDetails.nav')
                    <!-- Tab panes -->


                    </div>

                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane mt-3  active " >
                                <livewire:country.city-details.shape-component :cityID="$id"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
