@extends('admin.master')

@section('title')
    {{__("country.Countries")}}
@endsection
@section('title_page')
    {{__("country.Countries_Menu")}}

@endsection

@section('content')

    {{--    <livewire:country />--}}
{{--    {{dd($country)}}--}}
    <livewire:country.countries-translate :country="$country" />

@endsection
