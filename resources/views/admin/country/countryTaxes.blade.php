@extends('admin.master')

@section('title')
    {{__("country.Countries")}}
@endsection
@section('title_page')
    {{__("country.countryTaxes")}}

@endsection

@section('content')

    <livewire:country.country-taxes-component :counytryID="$id" />

@endsection
