@extends('admin.master')

@section('title')
         {{__("city.cities")}}
@endsection
@section('title_page')
     {{__("city.cities_Menu")}}
@endsection

@section('content')
{{--{{dd($country_id)}}--}}
    <livewire:country.cities :countryId="$country_id"/>

@endsection
