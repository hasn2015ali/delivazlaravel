<!-- Nav tabs -->
@php
    $locale=\Illuminate\Support\Facades\App::getLocale();
@endphp
<ul class="nav nav-tabs" id="myTab" role="tablist" wire:ignore>
    <li class="nav-item">
        <a class="nav-link @if (session()->get('countrySetting')=="details" ) active @endif"
           href="{{route('countrySetting',['locale'=>$locale,'id'=>$counytryID])}}"
        >
            {{__("country.CountryDetails")}}
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if (session()->get('countrySetting')=="taxes" ) active @endif"
           href="{{route('country.taxes',['locale'=>$locale,'id'=>$counytryID])}}"
        >{{__("country.CountryTaxes")}}</a>
    </li>

</ul>
