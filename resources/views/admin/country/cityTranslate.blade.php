@extends('admin.master')

@section('title')
    {{__("city.cities")}}
@endsection
@section('title_page')
    {{__("city.cities_Menu")}}

@endsection

@section('content')

    {{--    <livewire:country />--}}
    {{--    {{dd($country)}}--}}
    <livewire:country.cities-translation :city="$city" :country="$country"/>

@endsection
