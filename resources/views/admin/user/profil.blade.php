
@extends('admin.master')

@section('title')
    {{__("profile.profile")}}
@endsection
@section('title_page')
    {{__("profile.profile")}}
@endsection

@section('content')
    <div class="content">
        <div class="row">
    <livewire:user.user-profile />

    @include('admin.user.personalImageProfilePage')
        </div>
    </div>


@endsection
