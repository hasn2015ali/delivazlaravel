@extends('admin.master')

@section('title')
    {{__("workSystem.workSystem")}}
@endsection
@section('title_page')
    {{__("workSystem.workSystem")}}
@endsection

@section('content')

    <livewire:user.work-system  />

@endsection
