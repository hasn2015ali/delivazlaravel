
@extends('admin.master')

@section('title')
    {{__("user.Add_New_User")}}
@endsection
@section('title_page')
    {{__("user.Add_New_User")}}
@endsection

@section('content')
{{--    <livewire:add-user :type="$type" />--}}
    @livewire('user.add-user' , ['type' => $type])

@endsection
