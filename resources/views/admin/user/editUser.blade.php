
@extends('admin.master')

@section('title')
    {{__("user.Update_User")}}
@endsection
@section('title_page')
    {{__("user.Update_User")}}
@endsection

@section('content')
    <div class="content">
        <div class="row">

            <livewire:user.update-user :userId="$id" :type="$type" />

            @include('admin.user.userPersonalImage')

        </div>
        @if($type!='Customers')
         @include('admin.user.userImagesDetails')
        @endif

        <div class="row">
            @if($type!='manager' and $type!='Customers')
{{--                                {{dd($id,$type)}}--}}

                <livewire:user.update-user-work-hours :userId="$id" :type="$type" />
                @endif
{{--                {{dd($id,$type)}}--}}
        </div>
    </div>

{{--    <div class="content">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-9">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">--}}
{{--                        <h5 class="title">Enter User Details</h5>--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <form>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-5 pr-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>First Name</label>--}}
{{--                                        <input type="text" class="form-control"  placeholder="Enter First Name" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-3 px-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Last Name</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter Last Name" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-4 pl-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="exampleInputEmail1">Father Name</label>--}}
{{--                                        <input type="email" class="form-control" placeholder="Enter Father Name">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6 pr-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label> Email Address</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter Email Address" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6 pl-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Phone</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter Phone" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-12">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Address</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-4 pr-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Age</label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Enter Age" >--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-8 px-1">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Hours works </label>--}}
{{--                                        <input type="text" class="form-control" placeholder="Country" value="Andrew">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                            <div class="row">--}}
{{--                                <div class="col-md-9">--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>More Details </label>--}}
{{--                                        <textarea rows="4" cols="80" class="form-control" placeholder="Here can be your description" value="Mike">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</textarea>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-3">--}}
{{--                                    <button type="submit" class="btn btn-outline-primary">Add User </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-10">--}}

{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <div class="card card-user">--}}
{{--                    <div class="image">--}}
{{--                                        <img src="{{asset('assets/img/bg5.jpg')}}" alt="...">--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="author">--}}
{{--                            <a href="#">--}}
{{--                                <img class="avatar border-gray" src="{{asset('admin\assets\img\person.png')}}" alt="...">--}}
{{--                                <h5 class="title">Mike Andrew</h5>--}}
{{--                            </a>--}}
{{--                            <p class="description">--}}
{{--                                michael24--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                        <a href="#add" data-toggle="modal">--}}
{{--                            <button type="button" class="btn btn-outline-success">--}}
{{--                                Update Personal Image--}}
{{--                            </button>--}}
{{--                        </a>--}}
{{--                        <!-- The Modal -->--}}
{{--                        <div class="modal fade" id="add">--}}
{{--                            <div class="modal-dialog">--}}
{{--                                <div class="modal-content">--}}
{{--                                    <!-- Modal Header -->--}}
{{--                                    <div class="modal-header bg-warning">--}}
{{--                                        <div align="center">Update Personal Image </div>--}}
{{--                                        <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                                    </div>--}}
{{--                                    <!-- Modal body -->--}}
{{--                                    <div class="modal-body">--}}
{{--                                        <form method="post" action="{{route('addPersonalPhoto')}}" class="dropzone">--}}
{{--                                            @csrf--}}
{{--                                            <div class="fallback">--}}
{{--                                                <input name="file" type="file" multiple />--}}
{{--                                            </div>--}}
{{--                                        </form>--}}
{{--                                    </div>--}}
{{--                                    <!-- Modal footer -->--}}
{{--                                    <div align="center" class="modal-footer">--}}
{{--                                        <button style="margin-right:200px" type="button" class="btn btn-outline-warning" data-dismiss="modal">Cancel</button>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        --}}{{--end modal--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <div class="modal fade" id="myModalForImage">--}}
{{--        <div class="modal-dialog modal-xl">--}}
{{--            <div class="modal-content">--}}
{{--                <button style="width: 15%" type="button" class="btn btn-secondary" data-dismiss="modal">X</button>--}}
{{--                <img style="width: 100%" id="img_modle" src="" alt="placeholder"--}}
{{--                     class="img-fluid ">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <div class="content">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-9">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="card card-img-doc " style="width: 20rem; position: relative">--}}
{{--                            <button id="btn" class="btn btn-outline-warning btn-load-img">Show image</button>--}}

{{--                            <img class="card-img-top img-doc" src="{{asset('admin\assets\img\doc.jpg')}}" alt="Card image cap">--}}

{{--                            <div class="card-body">--}}
{{--                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="card card-img-doc" style="width: 20rem; position: relative">--}}
{{--                            <button id="btn" class="btn btn-outline-warning btn-load-img">Show image</button>--}}
{{--                            <img class="card-img-top img-doc" src="{{asset('admin\assets\img\doc.jpg')}}" alt="Card image cap">--}}

{{--                            <div class="card-body">--}}
{{--                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="card card-img-doc" style="width: 20rem; position: relative">--}}
{{--                            <button id="btn" class="btn btn-outline-warning btn-load-img">Show image</button>--}}
{{--                            <img class="card-img-top img-doc" src="{{asset('admin\assets\img\doc.jpg')}}" alt="Card image cap">--}}
{{--                            <div class="card-body">--}}
{{--                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}



{{--                <a href="#add1" data-toggle="modal">--}}
{{--                    <button type="button" class="btn btn-outline-success">--}}
{{--                        Add Images--}}
{{--                    </button>--}}
{{--                </a>--}}
{{--                <!-- The Modal -->--}}
{{--                <div class="modal fade" id="add1">--}}
{{--                    <div class="modal-dialog">--}}
{{--                        <div class="modal-content">--}}
{{--                            <!-- Modal Header -->--}}
{{--                            <div class="modal-header bg-warning">--}}
{{--                                <div  align="center">Add Images </div>--}}
{{--                                <button type="button" class="close" data-dismiss="modal">&times;</button>--}}
{{--                            </div>--}}
{{--                            <!-- Modal body -->--}}
{{--                            <div class="modal-body">--}}
{{--                                <form method="post" action="{{route('addUserFiles')}}" class="dropzone">--}}
{{--                                    @csrf--}}
{{--                                    <div class="fallback">--}}
{{--                                        <input name="file" type="file" multiple />--}}
{{--                                    </div>--}}
{{--                                </form>--}}
{{--                            </div>--}}
{{--                            <!-- Modal footer -->--}}
{{--                            <div align="center" class="modal-footer">--}}
{{--                                <button style="margin-right:200px" type="button" class="btn btn-outline-warning" data-dismiss="modal">Cancel</button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                --}}{{--                            end modal--}}
{{--                     </div>--}}
{{--                 </div>--}}
{{--               </div>--}}
{{--             </div>--}}
{{--         </div>--}}
{{--    </div>--}}
@endsection
