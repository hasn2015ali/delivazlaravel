@if($type!='manager')
    <div class="modal fade" id="myModalForImage">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <button style="width: 15%" type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
                <img style="width: 100%" id="img_modle" src="" alt="placeholder"
                     class="img-fluid ">
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row justify-content-center">
                            <a href="#addImagesDetails" data-toggle="modal">
                                <button type="button" class="btn btn-outline-success">
                                    {{__("user.Add_Images")}}
                                </button>
                            </a>
                            <!-- The Modal -->
                            <div class="modal fade" id="addImagesDetails">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header bg-warning">
                                            <div  align="center">Add Images </div>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <!-- Modal body -->
                                        <div class="modal-body">
                                            <form id="imagesDetails" method="post" action="{{route('addUserFiles',['id'=>$id])}}" class="dropzone">
                                                @csrf
                                                <div class="fallback">
                                                    <input name="file" type="file" multiple />
                                                </div>
                                            </form>
                                            <div class="d-flex flex-row justify-content-center">
                                                <button id="up_person_images" class="btn btn-outline-primary">  {{__("user.up_Images")}}</button>
                                            </div>
                                        </div>
                                        <!-- Modal footer -->
                                        <div align="center" class="modal-footer">
                                            <button style="margin-right:200px" type="button" class="btn btn-outline-warning" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr style="background: #f96332; "/>
                        <div id="uploadedImages">

                        </div>





{{--                        <div class="card card-img-doc" style="width: 20rem; position: relative">--}}
{{--                            <button id="btn" class="btn btn-outline-warning btn-load-img">Show image</button>--}}
{{--                            <img class="card-img-top img-doc" src="{{asset('admin\assets\img\doc.jpg')}}" alt="Card image cap">--}}

{{--                            <div class="card-body">--}}
{{--                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}






                        {{--                            end modal--}}
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        Dropzone.options.imagesDetails={
            autoProcessQueue: false,
            addRemoveLinks: true,
            maxFiles: 2,
            acceptedFiles: ".jpeg,.jpg,.png,.gif,.png,.jpg,.PNG,.Gif,.JPG",

            init:function () {
                var up_person_images= document.querySelector('#up_person_images');
                myDropzon=this;
                up_person_images.addEventListener('click',function(){
                    myDropzon.processQueue();
                });
                this.on("complete",function () {
                    if(this.getQueuedFiles().length ==0 && this.getUploadingFiles().length==0)
                    {
                        var _this=this;
                        _this.removeAllFiles();
                    }
                    $('#addImagesDetails').modal('hide');
                    loadPersonalImages();

                })
            }
        };
        // $(document).ready(function() {
        loadPersonalImages();
        // });
        function loadPersonalImages() {
            id={{$id}};
            // console.log('test');
            // console.log(id);

            // let uploadedImage=document.querySelector('#uploadedImage');
            $.ajax({
                type:"get",
                url:'/fetchUserImages/'+id,
                {{--                        url:{{ route('fetchPersonalImage') }},--}}


                success:function (data) {
                    $('#uploadedImages').html(data);
                    // console.log(data);
                    handelImageInModal();
                    deleteImageDetail();
                    // loadPersonalImages();
                }
            });

        }

        function handelImageInModal() {
            let btnLoadImg=document.querySelectorAll('.btn-load-img');
            btnLoadImg.forEach((btn)=>{
                btn.addEventListener('mouseenter',()=>{
                    btn.style.display = 'block';
                })
                btn.addEventListener('click',(e)=>{
                    let src=  e.target.parentElement.querySelector('img').src;
                    let myModalForImage=document.querySelector('#myModalForImage');
                    let img_modle=document.querySelector('#img_modle');
                    img_modle.src=src;
                    $("#myModalForImage").modal('show');

                })
            })

            // show botton hover
            let cardImgDoc=document.querySelectorAll('.card-img-doc');
            let body=document.querySelector('body');
            cardImgDoc.forEach(card=>{
                let imgDoc=card.querySelector('.img-doc');
                let btnLoadImg=card.querySelector('.btn-load-img');
                imgDoc.addEventListener('mouseover',()=>{
                    btnLoadImg.style.display='block';

                })

                imgDoc.addEventListener('mouseleave',()=>{
                    btnLoadImg.style.display='none';
                })

            })
        }


        function deleteImageDetail() {
            let btn_delete_image_details=document.querySelectorAll('.btn-delete-image-detail');
            btn_delete_image_details.forEach(btn=>{
                btn.addEventListener('click',()=>{
                    id=btn.getAttribute("id");
                    // console.log('id: '+id);
                    $.ajax({
                        type:"get",
                        url:'/deleteImageDetail/'+id,
                        success:function (data) {

                            $('#uploadedImages').html(data);
                            loadPersonalImages();
                            // console.log(data);

                        }
                    });

                })
            })
            // loadPersonalImages();

        }

    </script>



@endif

