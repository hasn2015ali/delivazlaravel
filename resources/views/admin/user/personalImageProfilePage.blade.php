<div class="col-md-3">
    <div class="card card-user">
        <div class="card-body d-flex flex-column align-items-center">

            {{--                                <a href="#">--}}
            <div id="uploadedImageSuccess">

            </div>
            <div id="uploadedImage">


            </div>
            <a href="#addPersonalImage" data-toggle="modal">
                <button type="button" class="btn btn-outline-success">
                    {{__("user.Update_Image")}}
                </button>
            </a>
            <!-- The Modal -->
            <div class="modal fade" id="addPersonalImage">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header bg-warning">
                            <div  align="center">           {{__("user.Add_Personal_Image")}} </div>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body">
                            <form id="dropzoneFrom" method="post" action="{{route('addPersonalPhotoFromProfile')}}" class="dropzone">
                                @csrf
                                <div class="fallback">
                                    <input name="file" type="file" />
                                </div>
                            </form>
                            <div class="d-flex flex-row justify-content-center">
                                <button id="up_person_image" class="btn btn-outline-primary"> {{__("user.Up_Load_image")}}</button>
                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div align="center" class="modal-footer">
                            <button style="margin-right:200px" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
                        </div>
                    </div>
                </div>
            </div>
            {{--                            end modal--}}

        </div>
    </div>
</div>

<script>
    Dropzone.options.dropzoneFrom={
        autoProcessQueue: false,
        maxFiles: 1,
        acceptedFiles: ".jpeg,.jpg,.png,.gif,.png,.jpg,.PNG,.Gif,.JPG",

        init:function () {
            var up_person_image= document.querySelector('#up_person_image');
            myDropzon=this;
            up_person_image.addEventListener('click',function(){
                myDropzon.processQueue();
            });
            this.on("complete",function () {
                if(this.getQueuedFiles().length ==0 && this.getUploadingFiles().length==0)
                {
                    var _this=this;
                    _this.removeAllFiles();
                }
                $('#addPersonalImage').modal('hide');
                loadPersonalImage();


            });
            this.on("success", function(file, responseText) {
                $('#uploadedImageSuccess').html(responseText);
                // console.log(responseText); // console should show the ID you pointed to
                // do stuff with file.id ...
            });

        }
    };
    // $(document).ready(function() {
    loadPersonalImage();
    // });
    function loadPersonalImage() {
        $.ajax({
            type:"get",
            url:'/fetchMyPersonalImage/',



            success:function (data) {
                $('#uploadedImage').html(data);
                // uploadedImage.html(data);
                {{--if(data!=null){--}}
                {{--    uploadedImage.src={{asset('storage\images\avatars\person.png')}}data;--}}
                {{--}--}}
                console.log(data);
            }
        });

    }
</script>
