@extends('admin.master')

@section('title')
        {{__("user.Users")}}

@endsection
@section('title_page')
    {{__("user.Users_Menu")}}
@endsection

@section('content')

    <livewire:user.users :type="$type" />

@endsection
