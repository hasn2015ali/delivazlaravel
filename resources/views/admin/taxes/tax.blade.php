@extends('admin.master')

@section('title')
    {{__("tax.taxes")}}
@endsection
@section('title_page')
    {{__("tax.tax_Menu")}}

@endsection

@section('content')

    <livewire:taxes.taxes-component />

@endsection
