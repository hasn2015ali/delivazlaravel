@extends('public.master')

@section('title')

    {{__('serviceProvider.RegisterIsNotComplete')}}
@endsection

@section('content')


    <div class="sorry-section  mb-4">
        <div class="container">
            <div class="d-flex flex-row justify-content-between flex-wrap" style="width: 100%">
                <div class="d-flex flex-row justify-content-start" style="width: 55%">

                    <div class="d-flex flex-column justify-content-center  ">
                        <div class="d-flex flex-row justify-content-start">
                            <h1 class="address">{{__('serviceProvider.RegisterIsNotComplete')}}</h1>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <p class="text">{{__('serviceProvider.sorry')}} </p>
                        </div>
                        <div class="d-flex flex-row justify-content-start alert alert-primary">
                            <p class="text">{{__('serviceProvider.now')}} </p>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-start" style="width: 35%">
                    <div class="img">
                        <img src="{{asset('/templateIMG/sorry.png')}}" />
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection
