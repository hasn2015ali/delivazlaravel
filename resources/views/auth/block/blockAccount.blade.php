@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div style="margin-top: 10%" >
                    <h1 style="background: orangered; color: white" class="alert">{{__("user.blocked")}}

                    </h1>
                    <h6  style="background: orangered; color: white" class="alert">
                        <a class="dropdown-item" href="{{ route('logout',['locale'=>app()->getLocale()]) }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout',['locale'=>app()->getLocale()]) }}" method="POST">
                            @csrf
                        </form>
                    </h6>

                </div>
            </div>
        </div>
    </div>
@endsection
