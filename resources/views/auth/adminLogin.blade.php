<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{__("admin.login")}}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>



    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body{
            background: #fac24f;
        }
        .card-header{
            background: white!important;
            color: red;
            font-weight: bolder;
        }
        .margin{
            margin-top: 15%;
        }
        .form-group label{
            color: red;
            font-size: 17px;
        }
        .btn-login{
            background-image: url(/images/btn_back.jpg?46b9c16d0d20ef3739c7d324ba18ed29);
            color: white;
            border: none;
            width: 23%;
            height: 43px;

            border-radius: 10px;
            font-size: 17px;
        }
        .form-group{
            margin-bottom: 30px
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row justify-content-center margin">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"><h3>{{ __('admin.welcome') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login',['locale'=>app()->getLocale()]) }}">
                        @csrf

                        <div style="" class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('admin.email') }}</label>

                            <div class="col-md-6">
                                <input id="email"
                                       placeholder="{{ __('admin.emailEnter') }}"
                                       type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('admin.Password') }}</label>

                            <div class="col-md-6">
                                <input id="password"
                                       placeholder="{{ __('admin.PasswordEnter') }}"
                                       type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

{{--                        <div class="form-group row">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button style=" " type="submit" class="btn-login">
                                    {{ __('admin.Login1') }}
                                </button>

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
