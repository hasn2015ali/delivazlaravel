<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    product page font links--}}
{{--    content--}}
{{--    <link rel="preconnect" href="https://fonts.googleapis.com">--}}
{{--    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>--}}
{{--    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Bree+Serif&family=Comfortaa:wght@300&family=Encode+Sans&family=Oxygen:wght@700&family=Patrick+Hand&family=Patua+One&family=Saira+Condensed&display=swap" rel="stylesheet">--}}

{{--    headers--}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Bodoni+Moda:wght@600&family=Bree+Serif&family=Carter+One&family=Comfortaa:wght@300&family=Encode+Sans&family=Oxygen:wght@700&family=Patrick+Hand&family=Patua+One&family=Saira+Condensed&display=swap" rel="stylesheet">

    {{--    add to basket--}}
{{--    <link rel="preconnect" href="https://fonts.googleapis.com">--}}
{{--    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>--}}
{{--    <link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@1,700&family=Bodoni+Moda:wght@600&family=Bree+Serif&family=Carter+One&family=Comfortaa:wght@300&family=Encode+Sans&family=Oxygen:wght@700&family=Patrick+Hand&family=Patua+One&family=Saira+Condensed&display=swap" rel="stylesheet">--}}

@if(app()->getLocale() == "en")
{{--        <link rel="preload" href="{{asset('css/template/en_style.css')}}"--}}
{{--              as="style" onload="this.rel='stylesheet'">--}}
{{--        <link rel="import" href="{{asset('css/template/en_style.css')}}" async>--}}
        <link href="{{asset('css/template/en_style.css')}}" rel="stylesheet"/>
    @endif
    @if(app()->getLocale() == "ar")
        <link href="{{asset('css/template/ar_style.css')}}" rel="stylesheet"/>
    @endif
    @if(app()->getLocale() == "fre")
        <link href="{{asset('css/template/fre_style.css')}}" rel="stylesheet"/>
    @endif
    @if(app()->getLocale() == "rus")
        <link href="{{asset('css/template/rus_style.css')}}" rel="stylesheet"/>
    @endif
    @if(app()->getLocale() == "ukr")
        <link href="{{asset('css/template/ukr_style.css')}}" rel="stylesheet"/>
    @endif


    <script src="{{asset('admin/assets/js/core/jquery.min.js')}}" async></script>
    <script src="{{asset('js/OwlCarousel2/js/owl.carousel.js')}}" defer></script>
    <script src="{{asset('admin/assets/js/core/popper.min.js')}}" defer></script>
    <script src="{{asset('admin/assets/js/core/bootstrap.min.js')}}" defer></script>
    <script src="{{asset('sweetAlert/sweetalert2.js')}}" defer></script>
{{--    <script src="{{asset('js/datepicker/datepicker.min.js') }}" defer></script>--}}
    @stack('head-scripts')

    {{--    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>--}}
    {{--    links--}}
{{--        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>--}}
{{--        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous" defer></script>--}}
{{--        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>--}}
{{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>--}}
    <script src="{{ asset('js/app.js') }}" defer></script>


    {{--    filesaver--}}
    {{--    <script src="http://cdn.jsdelivr.net/g/filesaver.js"></script>--}}

    {{--    english fonts--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap" rel="stylesheet">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&family=Lora:ital,wght@1,500&display=swap" rel="stylesheet">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&family=Lora:ital,wght@1,500&family=Roboto+Slab&display=swap" rel="stylesheet">--}}
    {{--  --}}
    {{--    arabic style--}}
    {{--    @if(app()->getLocale() == "ar")--}}
    {{--        <link href="{{asset('css/controlPanel_ar.css')}}" rel="stylesheet" />--}}
    {{--    @endif--}}

    @livewireStyles
</head>
<livewire:template.nav-bar :country="$country" :city="$city"/>
@yield('content')
<livewire:template.footer :country="$country" :city="$city"/>

<body>
{{-- <div id="loading">

    <x-template.loading class="loading"></x-template.loading>
</div> --}}
<script>
    // function loadingFunction() {
    //     console.log('loaded');
    //     $("#loading").fadeOut("slow");
    //
    //
    // }
</script>

@livewireScripts
{{--<script  src="https://unpkg.com/alpinejs@3.2.4/dist/cdn.min.js"></script>--}}
<script src="{{ asset('js/alpine.js') }}"></script>

{{--<script src="{{asset('js/liveWireEventsPublic.js')}}" defer></script>--}}
{{----}}
{{--@if(app()->getLocale() == "ar")--}}
{{--    <script src="{{asset('js/controlPanelAr.js')}}" defer></script>--}}

{{--@endif--}}

@stack('custom-scripts')

<script>

    $( document ).ready(function() {
        $('#countryInput').val('');
        $('#cityInput').val('');
        $('#countryCodeInput').val('');

    });
    function filterCountryFunction() {
        var  input,filter, ul, li, dropdown_item, i;
        input = document.getElementById("countryInput");
        filter = input.value.toUpperCase();
        div = document.getElementById("countryDropdown");
        console.log(div);
        dropdown_item = div.getElementsByTagName("button");
        // console.log(dropdown_item);

        for (i = 0; i < dropdown_item.length; i++) {
            txtValue = dropdown_item[i].textContent || dropdown_item[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                dropdown_item[i].style.display = "";
            } else {
                dropdown_item[i].style.display = "none";
            }
        }

        if (screen && screen.width < 600) {
            window.scrollTo(0, 0);
            // console.log('mobile');
        }

    }

    function filterCityFunction() {
        var  input,filter, ul, li, p, i;
        input = document.getElementById("cityInput");
        filter = input.value.toUpperCase();
        div = document.getElementById("cityDropdown");
        dropdown_item = div.getElementsByTagName("button");
        for (i = 0; i < dropdown_item.length; i++) {
            txtValue = dropdown_item[i].textContent || dropdown_item[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                dropdown_item[i].style.display = "";
            } else {
                dropdown_item[i].style.display = "none";
            }
        }

        if (screen && screen.width < 600) {
            window.scrollTo(0, 0);
            // console.log('mobile');
        }
    }

    function filterCountryCodeFunction() {
        var  input,filter, ul, li, p, i;
        input = document.getElementById("countryCodeInput");
        filter = input.value.toUpperCase();
        div = document.getElementById("countryCodesDropdown");
        p = div.getElementsByTagName("p");
        for (i = 0; i < p.length; i++) {
            txtValue = p[i].textContent || p[i].innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                p[i].style.display = "";
            } else {
                p[i].style.display = "none";
            }
        }

        if (screen && screen.width < 600) {
            window.scrollTo(0, 0);
            // console.log('mobile');
        }
    }
</script>
</body>

</html>
