@extends('public.master')

@section('title')

    {{$city}}
@endsection

@section('content')
        <livewire:template.city.banners :country="$country" :city="$city">
{{--        <livewire:template.index.country :countries="$countries" :checkedCity="$cityTrans" :country="$country" :cities="$cities " :city="$city" :countrySelected="$country_lang">--}}
            <livewire:template.city.shape :country="$country" :city="$city">
                <livewire:template.city.restaurant-banners :country="$country" :city="$city">
                    <livewire:template.city.vendor-banners :country="$country" :city="$city">
                <livewire:template.mobile >


@endsection
