@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.Foods')}}
@endsection

@section('content')
    <livewire:template.control-panel.food.food-component :user="$user"/>
{{--    <livewire:template.control-panel.info.owner-password :user="$user"/>--}}
{{--    <livewire:template.control-panel.info.owner-password :user="$user"/>--}}
@endsection
