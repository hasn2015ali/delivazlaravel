@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.Products')}}
@endsection

@section('content')
    <livewire:template.control-panel.food.product-component :user="$user"/>
@endsection
