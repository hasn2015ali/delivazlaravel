@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.BasicInformation')}}
@endsection

@section('content')

    <div class="basic-info-section "
         x-data="app()">
        <livewire:template.control-panel.info.list-component :user="$user" pageName="{{__('RestaurantControlPanel.BasicInformation')}}"/>
        <livewire:template.control-panel.info.service-provider-info :user="$user"/>
        <livewire:template.control-panel.info.owner-info :user="$user"/>
        <livewire:template.control-panel.info.document-component :user="$user"/>
        <livewire:template.control-panel.info.owner-password :user="$user"/>
{{--        <livewire:template.control-panel.info.document-component :user="$user"/>--}}


    </div>

<script>


</script>
@endsection
