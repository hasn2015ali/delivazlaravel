<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <title>
        @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>

    @if(app()->getLocale() == "en")
        {{--        <link rel="preload" href="{{asset('css/template/en_style.css')}}"--}}
        {{--              as="style" onload="this.rel='stylesheet'">--}}
        {{--        <link rel="import" href="{{asset('css/template/en_style.css')}}" async>--}}
        <link href="{{asset('css/template/controlPanel/controlPanelStyles.css')}}" rel="stylesheet"/>
    @endif
    @if(app()->getLocale() == "ar")
        <link href="{{asset('css/template/controlPanel/controlPanelStyles.css')}}" rel="stylesheet"/>

    @endif
    @if(app()->getLocale() == "fre")
        <link href="{{asset('css/template/controlPanel/controlPanelStyles.css')}}" rel="stylesheet"/>

    @endif
    @if(app()->getLocale() == "rus")
        <link href="{{asset('css/template/controlPanel/controlPanelStyles.css')}}" rel="stylesheet"/>

    @endif
    @if(app()->getLocale() == "ukr")
        <link href="{{asset('css/template/controlPanel/controlPanelStyles.css')}}" rel="stylesheet"/>

    @endif


    <script src="{{asset('admin/assets/js/core/jquery.min.js')}}" async></script>
    <script src="{{asset('js/OwlCarousel2/js/owl.carousel.js')}}" defer></script>
    <script src="{{asset('admin/assets/js/core/popper.min.js')}}" defer></script>
    <script src="{{asset('admin/assets/js/core/bootstrap.min.js')}}" defer></script>
    <script src="{{asset('sweetAlert/sweetalert2.js')}}" defer></script>

    {{--    <script src="{{ asset('js/datepicker/datepicker.min.js') }}" defer></script>--}}
    {{--    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>--}}
    {{--    links--}}
    {{--    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous" defer></script>--}}
    {{--    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>--}}
    {{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    {{--    filesaver--}}
    {{--    <script src="http://cdn.jsdelivr.net/g/filesaver.js"></script>--}}

    {{--    english fonts--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&display=swap" rel="stylesheet">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&family=Lora:ital,wght@1,500&display=swap" rel="stylesheet">--}}
    {{--    <link rel="preconnect" href="https://fonts.gstatic.com">--}}
    {{--    <link href="https://fonts.googleapis.com/css2?family=Lato:ital@1&family=Lora:ital,wght@1,500&family=Roboto+Slab&display=swap" rel="stylesheet">--}}
    {{--  --}}
    {{--    arabic style--}}
    {{--    @if(app()->getLocale() == "ar")--}}
    {{--        <link href="{{asset('css/controlPanel_ar.css')}}" rel="stylesheet" />--}}
    {{--    @endif--}}

    @livewireStyles
</head>
<livewire:template.control-panel.nav-bar :active="$active"/>
<div class="d-flex flex-row width-full">
    <div class="side-bar-section width-18">

        <livewire:template.control-panel.side-bar :active="$active"/>
        <div class="mobile-btn-container">
            <button class="btn-side-bar-menu mt-1">
                <span class="close-icon-sideBar">
{{--                    <i class="fas fa-arrow-left"></i>--}}
{{--                    <i class="fas fa-angle-left"></i>--}}

                    <i class="fas fa-times"></i>
                </span>
                <span class="open-icon-sideBar show-on-mobil-block">
                    <i class="fas fa-arrow-right"></i>
{{--                    <i class="fas fa-angle-right"></i>--}}


{{--                    <i class="fas fa-cogs"></i>--}}
                </span>
            </button>

        </div>


        {{--        side-bar-section--}}
    </div>
    <div class="content-section width-82">
        {{--        content-section--}}
        @yield('content')

    </div>

</div>
<livewire:template.control-panel.footer/>
{{--<livewire:template.footer :country="$country" :city="$city"/>--}}

<body>
{{-- <div id="loading">

    <x-template.loading class="loading"></x-template.loading>
</div> --}}
<script>
    // function loadingFunction() {
    //     console.log('loaded');
    //     $("#loading").fadeOut("slow");
    //
    //
    // }
</script>

@livewireScripts
{{--<script  src="https://unpkg.com/alpinejs@3.2.4/dist/cdn.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/cleave.min.js" integrity="sha512-KaIyHb30iXTXfGyI9cyKFUIRSSuekJt6/vqXtyQKhQP6ozZEGY8nOtRS6fExqE4+RbYHus2yGyYg1BrqxzV6YA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/addons/cleave-phone.ua.js" integrity="sha512-8gOWYs4/2d6jzO0rG5uEl3gdZgY+gfsm4kuPXhGLtQviMOgG4pYTqBBmLvolnEFaQSzU/wywCKSjRfF1TOwyOw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/cleave.js/1.6.0/addons/cleave-phone.ac.js" integrity="sha512-cYtsAr+CNMRRO2JHNjSuoHDicNa3n+CU6jO03/WimK1a+mLgh7Iehem9NgrBevHLe30TonEXE9Z4i/qxF2zS4A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
<script src="{{ asset('js/alpine.js') }}"></script>
{{--<script src="{{ asset('js/cleave.min.js') }}" defer></script>--}}
{{--<script src="{{ asset('js/cleave-phone.i18n.js') }}" defer></script>--}}

<script src="{{ asset('js/app2.js') }}"></script>

{{--<script src="{{asset('js/liveWireEventsPublic.js')}}" defer></script>--}}
{{----}}
{{--@if(app()->getLocale() == "ar")--}}
{{--    <script src="{{asset('js/controlPanelAr.js')}}" defer></script>--}}

{{--@endif--}}

</body>

</html>
