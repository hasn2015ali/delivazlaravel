@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.Banners')}}
@endsection

@section('content')
    <livewire:template.control-panel.banners.banner-component />


@endsection
