@extends('public.controlPanel.master')

@section('title')

    @if($typ=="food")

        {{__('RestaurantControlPanel.UpdateFood')}}
    @elseif($typ=="product")
        {{__('RestaurantControlPanel.UpdateProduct')}}
    @endif

@endsection

@section('content')

    @if($typ=="food")
        <livewire:template.control-panel.food.update-food-component :foodID="$id" />
    @elseif($typ=="product")
        <livewire:template.control-panel.food.update-product-component :productID="$id" />
    @endif

@endsection
