@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.Sizes')}}
@endsection

@section('content')

    <livewire:template.control-panel.sizes.size-component />

@endsection
