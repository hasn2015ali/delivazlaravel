@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.MyMenu')}}
@endsection

@section('content')
    <livewire:template.control-panel.food.menu-component />
@endsection
