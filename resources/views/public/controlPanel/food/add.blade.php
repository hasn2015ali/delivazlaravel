@extends('public.controlPanel.master')

@section('title')

    @if($typ=="food")

    {{__('RestaurantControlPanel.AddNewFood')}}
    @elseif($typ=="product")
    {{__('RestaurantControlPanel.AddNewProduct')}}
    @endif

@endsection

@section('content')

    @if($typ=="food")
        <livewire:template.control-panel.food.add-new-food-component />
    @elseif($typ=="product")
        <livewire:template.control-panel.food.add-new-product-component />

    @endif

@endsection
