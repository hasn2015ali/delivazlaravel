@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.workHours')}}
@endsection

@section('content')

    <livewire:template.control-panel.scheduling-time.work-time />

@endsection
