@extends('public.controlPanel.master')

@section('title')

    {{__('RestaurantControlPanel.Orders')}}
@endsection

@section('content')

    <livewire:template.control-panel.order.order-component :user="$user"/>
@endsection
