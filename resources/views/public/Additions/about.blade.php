@extends('public.master')

@section('title')


        {{__('addition.About')}}

@endsection

@section('content')

       <section class="about-section">

           <div class="container">
               <div class="d-flex flex-row justify-content-center">
                   <h2 class="address"> {{__('addition.AboutUs')}}</h2>
               </div>

               <div class="d-flex flex-column first-part">
                   <div class="d-flex flex-column justify-content-start">


                        <p class="text">
                           {{__('addition.Left_Side1')}}
                        </p>
                        <p class="text">
                           {{__('addition.Left_Side2')}}
                        </p>
                        <p class="text">
                            {{__('addition.Left_Side3')}}
                        </p>


                   </div>

                   <div class="d-flex flex-column justify-content-start">
                       <p class="text">
                           {{__('addition.right_Side1')}}
                       </p>
                       <p class="text">
                           {{__('addition.right_Side2')}}
                       </p>
                       <p class="text">
                           {{__('addition.right_Side3')}}
                       </p>
                   </div>

               </div>
           </div>

           <div class="container">
               <div class="d-flex flex-row justify-content-center">
                   <h2 class="address"> {{__('addition.OurValues')}}</h2>
               </div>

               <div class="d-flex flex-column second-part">
                   <div class="d-flex flex-column justify-content-start content">
                       <p class="text">
                           {{__('addition.part2Left_Side1')}}
                       </p>
                       <p class="text">
                           {{__('addition.part2Left_Side2')}}
                       </p>
{{--                       <p class="text">--}}
{{--                           {{__('addition.Left_Side3')}}--}}
{{--                       </p>--}}
                   </div>
                   <div class="d-flex flex-column justify-content-start img">
                       <img src="{{asset('storage/images/additions/about/1.png')}}">
                   </div>
               </div>
           </div>


           <div class="container">
               <div class="d-flex flex-row justify-content-center">
                   <h2 class="address"> {{__('addition.OurPromise')}}</h2>
               </div>

               <div class="d-flex flex-column second-part part-3">
                   <div class="d-flex flex-column justify-content-start img">
                       <img src="{{asset('storage/images/additions/about/1.png')}}">
                   </div>

                   <div class="d-flex flex-column justify-content-start content">
                       <p class="text">
                           {{__('addition.part3Left_Side1')}}
                       </p>
                       <p class="text">
                           {{__('addition.part3Left_Side2')}}
                       </p>
                      <p class="text">
                           {{__('addition.part3Left_Side3')}}
                       </p>

                       <p class="text">
                           {{__('addition.part3Left_Side4')}}
                       </p>
{{--                       <p class="text">--}}
{{--                           {{__('addition.part3Left_Side5')}}--}}
{{--                       </p>--}}

                   </div>

               </div>
           </div>

       </section>


@endsection
