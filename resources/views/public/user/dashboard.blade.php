@extends('public.master')

@section('title')

    {{__("userFront.Dashboard")}}
@endsection

@section('content')


        <section class="user-dashboard-section "  >
            <div class="container">
                @if(session()->has('emailMessage'))
                    <div style="width: 90%" class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        {{ session('emailMessage') }}
                        <a class="dropdown-item" href="{{ route('Verify-My-Email',['locale'=>app()->getLocale()]) }}" >
                            <strong>{{__("userFront.verifyMyEmail")}}</strong>
                        </a>
                    </div>
                    {{session()->forget(['emailMessage'])}}
                @endif
                @if(session()->has('mobileMessage'))
                    <div style="width: 90%" class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        {{ session('mobileMessage') }}
                        <a class="dropdown-item" href="{{ route('Verify-My-Phone',['locale'=>app()->getLocale()]) }}" >
                            <strong> {{__("userFront.PhoneMyVerification")}} </strong>
                        </a>
                    </div>
                    {{session()->forget(['mobileMessage'])}}
                @endif

                @if(session('verification_status'))
                    <div style="width: 90%" class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>
                            {{ session('verification_status') }}
                        </strong>
                    </div>
                    {{session()->forget(['verification_status'])}}
                @endif

                <div  class="d-flex flex-row justify-content-between user-container">
                    <div style="" class="d-flex flex-column left-side">
                        <livewire:template.user.personal-image />
{{--                        <livewire:template.user.payment />--}}
                        <livewire:template.user.support />
                        <livewire:template.user.logout />

                    </div>

                    <div style="" class="d-flex flex-column right-side">
                        <livewire:template.user.personal-info />
                        <livewire:template.user.payment-method-component />
                        <livewire:template.user.order-history-component />
                    </div>
                </div>
            </div>

        </section>



@endsection
