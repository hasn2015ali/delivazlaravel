@extends('public.master')

@section('title')

    {{__("userFront.Sign_in")}}
@endsection

@section('content')


        <div class="register-section ">
            <div class="container">
                <div class="d-flex flex-column justify-content-center align-items-center">
                    @if(session()->has('errorIdentification'))
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>
                                {{ session('errorIdentification') }}
                            </strong>
                        </div>
                        {{session()->forget(['errorIdentification'])}}
                    @endif

                    @error('password')
                    <div class="alert alert-danger">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    @error('email')
                    <div class="alert alert-danger">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @enderror
                    @if(session()->has('errorIdentification'))
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>
                                {{ session('errorIdentification') }}
                            </strong>
                        </div>
                        {{session()->forget(['errorIdentification'])}}
                    @endif
                </div>
                    <div class="d-flex flex-column justify-content-center align-items-center">
                        <form method="POST" action="{{ route('login',['locale'=>app()->getLocale()]) }}">
                            {{--                    @csrf--}}
                            {{ csrf_field() }}
                    <div class="d-flex flex-row justify-content-center">
                        <h2 class="address"> {{__("userFront.welcome")}}</h2>
                    </div>
                    {{--                <div class="d-flex flex-row justify-content-start">--}}
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="far fa-envelope"></i> <input class="form-control" name="email" placeholder="{{__("userFront.Email")}}">
                    </div>
{{--                    <div class="form-group d-flex flex-row justify-content-start align-items-center">--}}
{{--                        <i class="fas fa-mobile-alt"></i> <input class="form-control" placeholder="{{__("userFront.Phone")}}">--}}
{{--                    </div>--}}
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-lock"></i> <input placeholder="{{__("userFront.Password")}}" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" >

                    </div>

                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                        <button type="submit" class="btn btn-sing-up">
                            {{__('userFront.Sign_in')}}
                        </button>
                    </div>
                        </form>
                        <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                            <a href="{{ route('password.request',['locale'=>app()->getLocale()]) }}" class="btn btn-sing-in">
                                {{__('userFront.Forgot')}}
                            </a>
                        </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <p class="text">{{__('userFront.others')}}</p>
                    </div>
                    <div class=" d-flex flex-row justify-content-between align-items-center three-btn">
                        <a href="{{route('login.facebook')}}" class="btn btn-facebook">
                            <i class="fab fa-facebook-square mr-1"></i>  {{__('userFront.facebook')}}
                        </a>
{{--                        <a href="{{route('login.twitter')}}" class="btn btn-twiteer">--}}
{{--                            <i class="fab fa-twitter mr-1"></i> {{__('userFront.twiteer')}}--}}
{{--                        </a>--}}
                        <a href="{{route('login.google')}}" class="btn btn-gmail">
                            <i class="fa fa-google mr-1" aria-hidden="true"></i>
                            {{__('userFront.gmail')}}
                        </a>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <p class="text">{{__('userFront.accountDont')}}</p>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <a href="{{route('register',['locale'=>app()->getLocale()])}}" class="btn btn-sing-in">
                            {{__('userFront.Sign_Up')}}
                        </a>
                    </div>
                    {{--                </div>--}}
                </div>

            </div>

            <div class="cover">
                <img src="{{asset('/templateIMG/register.png')}}">
            </div>
        </div>



@endsection
