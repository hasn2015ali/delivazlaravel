@extends('public.master')

@section('title')

    {{__("userFront.Sign_Up")}}
@endsection

@section('content')


        <div class="register-section ">
            <div class="container">
                <form method="POST" action="{{ route('register',['locale'=>app()->getLocale()]) }}">
                    @csrf
                <div class="d-flex flex-column justify-content-center align-items-center">
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        @if(session()->has('errorIdentification'))
                            <div class="alert alert-danger">
                                <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                                <strong>
                                    {{ session('errorIdentification') }}
                                </strong>
                            </div>
                            {{session()->forget(['errorIdentification'])}}
                        @endif
                        @error('name')
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                        @error('lastName')
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                        @error('email')
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                        @error('password')
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                        @error('password_confirmation')
                        <div class="alert alert-danger">
                            <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        @enderror
                    </div>
                    <div class="d-flex flex-row justify-content-center">
                        <h2 class="address"> {{__("userFront.Registration")}}</h2>
                    </div>
                    {{--                <div class="d-flex flex-row justify-content-start">--}}
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="far fa-user"></i>  <input  placeholder="{{__("userFront.FirstName")}}" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus>

                    </div>
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="far fa-user"></i>  <input class="form-control" name="lastName" placeholder="{{__("userFront.LastName")}}">

                    </div>
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="far fa-envelope"></i> &nbsp; <i class="fas fa-mobile-alt"></i> <input placeholder="{{__("userFront.Email")}}" id="email" type="" class="form-control @error('email') is-invalid @enderror" name="email"   >

                    </div>
{{--                    <div class="form-group d-flex flex-row justify-content-start align-items-center">--}}
{{--                        <input class="form-control" placeholder="{{__("userFront.Phone")}}">--}}
{{--                    </div>--}}
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-lock"></i> <input placeholder="{{__("userFront.Password")}}" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                    </div>
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-lock"></i> <input placeholder="{{__("userFront.password_confirmation")}}" id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">

                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                       <button type="submit" class="btn btn-sing-up">
                           {{__('userFront.Sign_Up')}}
                       </button>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <p class="text">{{__('userFront.Or')}}</p>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center ">
                        <a href="{{route('Register-Shop-Or-Restaurant',['locale'=>app()->getLocale()])}}" class="btn btn-sing-up">
                            {{__('userFront.RegisterAsShopOrRestaurant')}}
                        </a>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <p class="text">{{__('userFront.Or')}}</p>
                    </div>
                    <div class="form-group d-flex flex-row justify-content-center align-items-center">
                        <a href="{{route('login',['locale'=>app()->getLocale()])}}" class="btn btn-sing-in">
                            {{__('userFront.Sign_in')}}
                        </a>
                    </div>
                    {{--                </div>--}}
                </div>
                </form>
            </div>
            <div class="cover">
                <img src="{{asset('/templateIMG/register.png')}}">
            </div>
        </div>



@endsection
