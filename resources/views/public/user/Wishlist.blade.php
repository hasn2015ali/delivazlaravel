@extends('public.master')

@section('title')

    {{__("userFront.Wishlist")}}
@endsection

@section('content')

    <livewire:template.user.wishlist-component />

    <livewire:template.category.sell-on-dlivaz :country="$country" :city="$city" />


@endsection
