@extends('public.master')

@section('title')

    {{__("userFront.RegisterShopOrRestaurant")}}
@endsection

@section('content')

    <livewire:template.user.register-shop-or-restaurant >

@endsection
