@extends('public.master')

@section('title')

    {{__("userFront.resetPassword")}}
@endsection

@section('content')

    <div class="register-section mt-5 mb-5">
        <div class="container">
            <div class="d-flex flex-column justify-content-center align-items-center">
                @error('password')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @error('password_confirmation')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @if(session('status'))
                    <div class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>
                            {{ session('status') }}
                        </strong>
                    </div>

                @endif
            </div>
            <div class="d-flex flex-row justify-content-center">
                <form method="POST" action="{{ route('password.update',['locale'=>app()->getLocale()]) }}">
                    @csrf
                    <input type="hidden" name="token" value="{{$request->token}}"/>
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="far fa-envelope"></i> <label class="mr-2 ml-2">{{$request->email}}</label> <input
                            type="hidden" class="form-control" name="email" value="{{$request->email}}">
                    </div>

                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-lock"></i> <input placeholder="{{__("userFront.Password")}}" id="password"
                                                           type="password"
                                                           class="form-control @error('password') is-invalid @enderror"
                                                           name="password" autocomplete="new-password">

                    </div>
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-lock"></i> <input placeholder="{{__("userFront.password_confirmation")}}"
                                                           id="password-confirm" type="password" class="form-control"
                                                           name="password_confirmation" autocomplete="new-password">

                    </div>


                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                        <button type="submit" class="btn btn-sing-up">
                            {{__('userFront.RestPassword')}}
                        </button>
                    </div>
                </form>
            </div>

        </div>

        <div class="cover">
            <img src="{{asset('/templateIMG/register.png')}}">
        </div>
    </div>



@endsection
