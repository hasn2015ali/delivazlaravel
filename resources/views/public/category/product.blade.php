@extends('public.master')

@section('title')
    @if( $type=="product")
    Product
    @endif
    @if( $type=="food")
        Food
    @endif
{{--        {{__('serviceProvider.Restaurant')}}--}}

@endsection

@section('content')


    @if( $type=="food")
        <livewire:template.category.product-component :type="$type" :foodID="$foodID" :country="$country" :city="$city" :serviceProvider="$serviceProvider"/>
    @endif
    @if( $type=="product")
        <livewire:template.category.product-component :type="$type" :productID="$productID" :country="$country" :city="$city" :serviceProvider="$serviceProvider"/>
    @endif
        <livewire:template.mobile >

@endsection
