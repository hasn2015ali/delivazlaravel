@extends('public.master')

@section('title')
{{--    <livewire:template.category.service-provider-title-component />--}}
{{-- <p>--}}
{{--         {{__('Vendors.Restaurants')}}--}}

{{-- </p>--}}

@endsection

@section('content')
    <script src="{{asset('js/moment-locales.js')}}"></script>
    <script>
        window.addEventListener('page-type', event => {
            // console.log(event.detail.page1);
            document.title=event.detail.page1;
            // window.pageType=event.detail.page1;
            Livewire.emitTo('template.category.service-providers-banners', 'getPageType',event.detail.page1);
            Livewire.emitTo('template.category.sell-on-dlivaz', 'getPageType',event.detail.page1);

            // let cover=document.querySelector('#cover');
            // console.log(cover);

        })
    </script>
        <div class="service-providers-section">
            <livewire:template.category.service-providers-banners :country="$country" :city="$city"/>
                <div class="container">
                    <div class="d-flex flex-row justify-content-between parent">
                        <div class="filters">
                            <livewire:template.category.service-providers-filters :country="$country" :city="$city" :shape="$shape"/>
                        </div>
                        <div class="service-providers">
                            <livewire:template.category.service-providers :country="$country" :city="$city" :shape="$shape"/>
                        </div>

                    </div>
                </div>

            <livewire:template.category.sell-on-dlivaz :country="$country" :city="$city" page="serviceProviders" />

        </div>







@endsection
