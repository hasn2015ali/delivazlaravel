@extends('public.master')

@section('title')
    {{$serviceProvider->name}}
@endsection

@section('content')


        <div class="service-providers-section">
{{--            {{dd($serviceProvider)}}--}}
            <livewire:template.category.service-provider-banners :serviceProvider="$serviceProvider"/>
            <livewire:template.category.service-provider-info :serviceProvider="$serviceProvider"/>
            <div class="container">
                <div class="d-flex flex-row justify-content-between parent">
                    <div class="filters ">
                        <livewire:template.category.service-provider-filters :serviceProvider="$serviceProvider"/>
                    </div>
                    <div class="service-providers">
                        <livewire:template.category.service-provider-items :country="$country" :city="$city" :serviceProvider="$serviceProvider" />
                    </div>

                </div>
            </div>

            <?php
            $serviceProvider->user->role_id==9?$serviceProviderType="Restaurant":$serviceProviderType="Shop" ;
//            $serviceProvider->user->role_id==10?$serviceProviderType="Shops" ;
            ?>
{{--            {{dd($serviceProvider->user->role_id)}}--}}
            <livewire:template.category.sell-on-dlivaz :country="$country" :city="$city" :serviceProviderType="$serviceProviderType"/>
        </div>




@endsection
