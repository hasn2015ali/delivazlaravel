<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item active">{{__("tax.taxes")}}</li>

                    </ol>
                    @include('livewire.modal.tax.addTax')
{{--                    @include('livewire.modal.tax.countryUp')--}}
                    @include('livewire.modal.deleteModel')
{{--                    @include('livewire.modal.translation')--}}
                    {{--start modal--}}
                    @can('is-manager')
                        <a href="#addTax" data-toggle="modal" wire:click.prevent="restValues">
                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </a>
                    @endcan

                    @if (session()->has('message'))

                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif
                <!-- The Modal -->


                    {{--                            end modal--}}

                </div>
                <div class="card-body">
                    @if(count($taxes)>0)
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("tax.Name")}}
                            </th>
                            <th>
                                {{__("tax.Type")}}
                            </th>
                            <th>
                                {{__("tax.Value")}}
                            </th>
                            <th>
                                {{__("masterControl.Action")}}
                            </th>

                            </thead>
                            <tbody>

                            @foreach($taxes as $tax)
                                <tr>
                                    <td>
                                        {{$tax->name}}
                                    </td>
                                    <td>
                                        {{$tax->type}}
                                    </td>
                                    <td>
                                        @if( $tax->type=="Ratio")
                                        {{$tax->value}}%
                                        @else
                                            {{$tax->value}}

                                        @endif
                                    </td>
                                    <td>
                                        @can('is-manager')
                                            <a href="#addTax" data-toggle="modal" wire:click.prevent="edit({{$tax}})">
                                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                                    <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                                </button>
                                            </a>
                                            <a href="#delCountry" data-toggle="modal" wire:click="callDelete({{$tax->id}})">
                                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                    <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                                </button>
                                            </a>
{{--                                            <a href="{{route('countrySetting',['locale'=>app()->getLocale(),'id'=>$tax->country_id])}}">--}}
{{--                                                <button type="button" class="btn btn-outline-info p-2 m-1">--}}
{{--                                                    <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>--}}
{{--                                                </button>--}}
{{--                                            </a>--}}
                                        @endcan
                                    </td>


                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="d-flex flex-row justify-content-center">
                            {{ $taxes->links() }}
                        </div>
                        @else
                            <div class="alert alert-info">
                                {{__("tax.NoTaxes")}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

