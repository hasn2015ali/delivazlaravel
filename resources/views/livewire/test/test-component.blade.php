<div class="user-scheduling-section">
    @push('head-scripts')
        <script src="{{asset('js/datepicker/datepicker.min.js') }}" defer></script>
        <script src="{{asset('timepicker/js/timepicker.min.js')}}"></script>
        <link href="{{asset('timepicker/css/timepicker.min.css')}}" rel="stylesheet"/>

        {{--        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
        {{--        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
    @endpush
    <div class="container">
        {{--                <livewire:template.user.userSchedule.order-scheduling-component />--}}
        <div class="user-scheduling">
            <div class="scheduling-container">
                <div class="scheduling-option">

                    <div class="address">
                        {{__('userSchedule.Book')}}
                    </div>
                    {{--                 <div class="content">--}}
                    {{--                     {{__('userSchedule.ScheduleOrderDes')}}--}}
                    {{--                 </div>--}}
                    <div class="user-input w-100">
                        <div class="d-flex justify-content-between p-relative">
                            <label>{{__('userSchedule.OrderName')}}</label>


                            <div class="my-tooltip">
                                <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M16.5 2.0625C8.52715 2.0625 2.0625 8.52715 2.0625 16.5C2.0625 24.4729 8.52715 30.9375 16.5 30.9375C24.4729 30.9375 30.9375 24.4729 30.9375 16.5C30.9375 8.52715 24.4729 2.0625 16.5 2.0625ZM16.5 28.4883C9.88066 28.4883 4.51172 23.1193 4.51172 16.5C4.51172 9.88066 9.88066 4.51172 16.5 4.51172C23.1193 4.51172 28.4883 9.88066 28.4883 16.5C28.4883 23.1193 23.1193 28.4883 16.5 28.4883Z" fill="#FAC24F"/>
                                    <path d="M14.9531 10.8281C14.9531 11.2384 15.1161 11.6318 15.4062 11.9219C15.6963 12.212 16.0897 12.375 16.5 12.375C16.9103 12.375 17.3037 12.212 17.5938 11.9219C17.8839 11.6318 18.0469 11.2384 18.0469 10.8281C18.0469 10.4179 17.8839 10.0244 17.5938 9.73432C17.3037 9.44422 16.9103 9.28125 16.5 9.28125C16.0897 9.28125 15.6963 9.44422 15.4062 9.73432C15.1161 10.0244 14.9531 10.4179 14.9531 10.8281ZM17.2734 14.4375H15.7266C15.5848 14.4375 15.4688 14.5535 15.4688 14.6953V23.4609C15.4688 23.6027 15.5848 23.7188 15.7266 23.7188H17.2734C17.4152 23.7188 17.5312 23.6027 17.5312 23.4609V14.6953C17.5312 14.5535 17.4152 14.4375 17.2734 14.4375Z" fill="#FAC24F"/>
                                </svg>
                                <div class="top">
                                    <p>  this name will appear in your cart.</p>
                                    <i></i>
                                </div>
                            </div>
                        </div>

                        <div class="input-group p-relative ">
                            <input  type="text" class="form-control">

                        </div>
                        @error('DefaultName')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="row-inputs">
                        <div class="user-input">
                            <label>{{__('userSchedule.StartDate')}}</label>
                            <div wire:ignore class="input-group date p-relative data-start-container">
                                <input id="date-schedule-start" type="text" class="form-control">
                                <div class="input-group-append" data-target="#datetimepicker3"
                                     data-toggle="datetimepicker">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>

                            </div>
                            @error('DefaultName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input "
                             x-data="{ dropDownMenu : false}">
                            <div class="d-flex justify-content-between ">
                                <label>{{__('userSchedule.Intervals')}}</label>
                                <div class="my-tooltip">
                                    <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M16.5 2.0625C8.52715 2.0625 2.0625 8.52715 2.0625 16.5C2.0625 24.4729 8.52715 30.9375 16.5 30.9375C24.4729 30.9375 30.9375 24.4729 30.9375 16.5C30.9375 8.52715 24.4729 2.0625 16.5 2.0625ZM16.5 28.4883C9.88066 28.4883 4.51172 23.1193 4.51172 16.5C4.51172 9.88066 9.88066 4.51172 16.5 4.51172C23.1193 4.51172 28.4883 9.88066 28.4883 16.5C28.4883 23.1193 23.1193 28.4883 16.5 28.4883Z" fill="#FAC24F"/>
                                        <path d="M14.9531 10.8281C14.9531 11.2384 15.1161 11.6318 15.4062 11.9219C15.6963 12.212 16.0897 12.375 16.5 12.375C16.9103 12.375 17.3037 12.212 17.5938 11.9219C17.8839 11.6318 18.0469 11.2384 18.0469 10.8281C18.0469 10.4179 17.8839 10.0244 17.5938 9.73432C17.3037 9.44422 16.9103 9.28125 16.5 9.28125C16.0897 9.28125 15.6963 9.44422 15.4062 9.73432C15.1161 10.0244 14.9531 10.4179 14.9531 10.8281ZM17.2734 14.4375H15.7266C15.5848 14.4375 15.4688 14.5535 15.4688 14.6953V23.4609C15.4688 23.6027 15.5848 23.7188 15.7266 23.7188H17.2734C17.4152 23.7188 17.5312 23.6027 17.5312 23.4609V14.6953C17.5312 14.5535 17.4152 14.4375 17.2734 14.4375Z" fill="#FAC24F"/>
                                    </svg>
                                    <div class="top">
                                        <p> your order will be automatically repeated every day</p>
                                        <i></i>
                                    </div>
                                </div>

                            </div>
                            <div class="my-dropdown-container">
                                <button type="button" class="btn btn-interval"
                                        @click="dropDownMenu=!dropDownMenu">
                                    {{$selectedIntervalsTranslated}}
                                </button>
                                <div class="my-dropdown-menu"
                                     x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="dropDownMenu"
                                     @click.away="dropDownMenu=false"
                                     x-on:close-drop-down.window="dropDownMenu = false"
                                     x-cloak>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('Daily')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.Daily")}}
                                            </p>
                                        </div>
                                    </div>

                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EveryTwoDays')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EveryTwoDays")}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EveryThreeDays')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EveryThreeDays")}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EveryFourDays')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EveryFourDays")}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EveryFiveDays')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EveryFiveDays")}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EverySixDays')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EverySixDays")}}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item-container"
                                         wire:click.prevent="selectIntervals('EveryWeek')">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2" stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            <p class="dropdown-item">
                                                {{__("userSchedule.EveryWeek")}}
                                            </p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            @error('DefaultName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="user-input">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input"
                                   wire:change.prevent="changeOngoingState"
                                   @if($ongoingState) checked="checked" @endif
                                   id="customSwitchGoing">
                            <label class="custom-control-label"
                                   for="customSwitchGoing">{{__('userSchedule.Ongoing')}}</label>
                        </div>
                    </div>
                    <div wire:loading wire:target="changeOngoingState">
                        <x-control-panel.loading/>
                    </div>


                        <div class="row-inputs ">
                            @if(!$ongoingState)
                            <div class="row-inputs">
                                <div class="user-input w-100">
                                    <label>{{__('userSchedule.RepeatUnitl')}}</label>
                                    <div wire:ignore class="input-group date p-relative data-end-container">
                                        <input id="date-schedule-end" type="text" class="form-control">
                                        <div class="input-group-append" data-target="#datetimepicker3"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                            </div>
                                        </div>

                                    </div>
                                    @error('DefaultName')
                                    <div class="alert alert-danger2">
                                        <button type="button" class="close" data-dismiss="alert">&times;
                                        </button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                            @endif
                            <div class="user-input ">
                                <label>{{__('userSchedule.DeliveryTime')}}</label>
                                <div class="form-group">
                                    <div class="input-group date" data-target-input="nearest">
                                        {{--                                            <input id="time-schedule" type="time" class="form-control" />--}}
                                        <input id="time-schedule" type="text" class="form-control bs-timepicker">
                                        <div class="input-group-append" data-target="#datetimepicker3"
                                             data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                        </div>
                                    </div>
                                </div>
                                @error('DefaultName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    <div class="row-inputs">
                        <button class="btn btn-primary-delivaz">
                            <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M38.1885 13.0043C38.0275 12.7716 37.8126 12.5815 37.5621 12.4501C37.3115 12.3188 37.0329 12.2501 36.75 12.25H12.8328L10.8132 7.40251C10.5485 6.76415 10.1001 6.21879 9.52494 5.83558C8.94982 5.45237 8.27385 5.24856 7.58275 5.25001H3.5V8.75001H7.58275L15.8847 28.6738C16.0177 28.9925 16.2421 29.2648 16.5295 29.4563C16.8169 29.6479 17.1546 29.75 17.5 29.75H31.5C32.2297 29.75 32.8825 29.2968 33.1398 28.616L38.3898 14.616C38.489 14.3511 38.5225 14.0661 38.4874 13.7854C38.4524 13.5047 38.3498 13.2366 38.1885 13.0043V13.0043Z" fill="white"/>
                                <path d="M18.375 36.75C19.8247 36.75 21 35.5747 21 34.125C21 32.6753 19.8247 31.5 18.375 31.5C16.9253 31.5 15.75 32.6753 15.75 34.125C15.75 35.5747 16.9253 36.75 18.375 36.75Z" fill="white"/>
                                <path d="M30.625 36.75C32.0747 36.75 33.25 35.5747 33.25 34.125C33.25 32.6753 32.0747 31.5 30.625 31.5C29.1753 31.5 28 32.6753 28 34.125C28 35.5747 29.1753 36.75 30.625 36.75Z" fill="white"/>
                            </svg>
                            {{__('userSchedule.AddCart')}}

                        </button>
                    </div>

                </div>


            </div>

            <div class="scheduling-items">

            </div>
        </div>

    </div>
</div>

@push('custom-scripts')

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        $('#credit-card').on('keypress change', function () {
            $(this).val(function (index, value) {
                return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            });
        });

    </script>
    <script>
        function paymentMethod() {
            return {
                card: true,
                wallet: false,
                cash: false,
                openCard() {
                    this.card = true;
                    this.wallet = false;
                    this.cash = false;
                    this.$wire.emitSelf('selectedPaymentMethod', 'card');
                },
                closeCard() {
                    this.card = false
                },
                isOpenCard() {
                    return this.card === true
                },


                openWallet() {
                    this.card = false;
                    this.wallet = true;
                    this.cash = false;
                    this.$wire.emitSelf('selectedPaymentMethod', 'wallet');
                },
                closeWallet() {
                    this.wallet = false
                },
                isOpenWallet() {
                    return this.wallet === true
                },


                openCash() {
                    this.card = false;
                    this.wallet = false;
                    this.cash = true;
                    this.$wire.emitSelf('selectedPaymentMethod', 'cash');
                },
                closeCash() {
                    this.cash = false
                },
                isOpenCash() {
                    return this.cash === true
                },
            }
        }
    </script>

    <script>
        $(document).ready(function () {
            $('#date-filter-day').datepicker();
            $('#date-filter-card').datepicker({
                format: 'MM/yyyy',
                icons: {
                    time: 'fa fa-time',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove'
                },
                startView: "months",
                minViewMode: "months",
                autoclose: true,
                container: '.datepicker-container',
            });

            $('#date-schedule').datepicker({
                format: 'mm/dd/yyyy',

                autoclose: true,
                container: '.data-container',
            });

            $('#date-schedule-start').datepicker({
                format: 'mm/dd/yyyy',

                autoclose: true,
                container: '.data-start-container',
            });
            $('#date-schedule-end').datepicker({
                format: 'mm/dd/yyyy',

                autoclose: true,
                container: '.data-end-container',
            });
        });
        $(function () {
            $('.bs-timepicker').timepicker();
        });
    </script>
@endpush
