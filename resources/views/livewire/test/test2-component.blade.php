<div style="width: 100%" class="d-flex flex-column justify-content-start align-items-start">
    {{-- <x-control-panel.loading/>--}}
    {{--    <div wire:loading wire:target="$recommended">--}}
    {{--        <x-control-panel.loading/>--}}
    {{--    </div>--}}
    <div class="d-flex flex-row justify-content-start">
        <h6 class="address">{{$type}}</h6>
    </div>

    <div style="width: 100%" class="d-flex flex-row justify-content-start flex-wrap service-provider-parent">
        {{-- Care about people's approval and you will be their prisoner. --}}
        @forelse($recommended as $restaurant)
            <script type="text/javascript">
                window.obj=[];
                window.obj2={};
                window.dayResult=null;
                window.hourResult=null;
            </script>
            <div class="service-provider ">
                <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant->id])}}">
                    <div class="img">
                        <img class=" " src="{{asset('/storage/images/avatars/'.$restaurant->avatar)}}">
                    </div>
                </a>
                <div class="d-flex flex-row justify-content-between content">
                    <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant->id])}}">
                        <h6 class="name">{{$restaurant->name}}</h6>
                    </a>
                    <button type="button" class="btn btn-white">
                        <i class="fas fa-heart active"></i>
                    </button>
                </div>
                {{--{{$restaurant->partTime->where('day_state','on')}}--}}
                @foreach($restaurant->partTime()->get()->where('day_state','on') as $day)
                    <script type="text/javascript">
                        window.obj.push('<?php echo $day->day_id; ?>');
                        {{--console.log("day_id",{{$day->day_id}});--}}

                            {{--console.log("test",{{$restaurant->id}});--}}
                            window.obj2={
                            serviceProvider:<?php echo $restaurant->id; ?>,
                            day:<?php echo $day->day_id; ?>,
                            HourStart:<?php echo $day->startTimeHour; ?>,
                            MinuteStart:<?php echo $day->startTimeMinutes; ?>,
                            HourEnd:<?php echo $day->endTimeHour; ?>,
                            MinuteEnd:<?php echo $day->endTimeMinutes; ?>,
                        }
                        // console.log( window.obj);
                        {{--window.Hours.push('<?php echo $day->day_id; ?>');--}}
                    </script>

                @endforeach
                <script>
                    let doChick={{$restaurant->state}};

                    if(doChick==1)
                    {
                        // console.log("doChick",doChick);
                        // console.log("obj",window.obj);
                        // console.log("obj2",window.obj2);
                        window.dayResult=isDayOpen( window.obj);
                        let state=document.querySelector(".state{{$restaurant->id}}");
                        // console.log("state is", state);
                        // console.log("dayResult is",   window.dayResult);
                        if( window.dayResult==true)
                        {
                            window.hourResult=checkHours(window.obj2);
                            if(window.hourResult==true)
                            {
                                let open=@json( __('serviceProvider.Open') );
                                // console.log("open",open);
                                state.classList.add('open');
                                state.textContent=open;
                            }
                            if( window.hourResult!=true)
                            {
                                state.classList.add('close');
                                state.textContent="Closed";
                                let close=@json( __('serviceProvider.Closed') );
                                state.textContent=close;
                                // console.log("Result is", window.dayResult);
                            }
                        }
                        if( window.dayResult!=true)
                        {
                            state.classList.add('close');
                            state.textContent="Closed";
                            let close=@json( __('serviceProvider.Closed') );
                            state.textContent=close;
                            // console.log("Result is", window.dayResult);
                        }
                    }

                </script>
                @if($restaurant->state=="1")
                    <div class="d-flex flex-row justify-content-between state ">
                        <p class="state{{$restaurant->id}}"> </p>
                    </div>
                @elseif($restaurant->state=="0")
                    <div class="d-flex flex-row justify-content-between state ">
                        <p class="close "> {{__('serviceProvider.Closed')}}</p>
                    </div>
                @endif

            </div>
        @empty
        @endforelse

        @forelse($notRecommended as $restaurant)
            <script type="text/javascript">
                window.objv=[];
                window.obj2v={};
                window.dayResultv=null;
                window.hourResultv=null;
            </script>
            <div class="service-provider ">
                <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant->id])}}">
                    <div class="img">
                        <img class=" " src="{{asset('/storage/images/avatars/'.$restaurant->avatar)}}">
                    </div>
                </a>
                <div class="d-flex flex-row justify-content-between content">
                    <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant->id])}}">
                        <h6 class="name">{{$restaurant->name}}</h6>
                    </a>
                    <button type="button" class="btn btn-white">
                        <i class="fas fa-heart active"></i>
                    </button>
                </div>

                {{--                {{$restaurant->partTime->where('day_state','on')}}--}}
                @foreach($restaurant->partTime->where('day_state','on') as $day)
                    <script type="text/javascript">
                        window.objv.push('<?php echo $day->day_id; ?>');
                        {{--                        console.log("day",{{$day->day_id}});--}}
                            window.obj2v={
                            serviceProvider:<?php echo $restaurant->id; ?>,
                            day:<?php echo $day->day_id; ?>,
                            HourStart:<?php echo $day->startTimeHour; ?>,
                            MinuteStart:<?php echo $day->startTimeMinutes; ?>,
                            HourEnd:<?php echo $day->endTimeHour; ?>,
                            MinuteEnd:<?php echo $day->endTimeMinutes; ?>,
                        }

                    </script>

                @endforeach

                <script>
                    let doChickv={{$restaurant->state}};

                    if(doChickv==1)
                    {
                        // console.log(doChickv);
                        window.dayResultv=isDayOpen( window.objv);
                        let statev=document.querySelector(".state{{$restaurant->id}}");
                        // console.log("state is", state);

                        if( window.dayResultv==true)
                        {
                            window.hourResultv=checkHours(window.obj2v);
                            if(window.hourResultv==true)
                            {
                                let openv=@json( __('serviceProvider.Open') );
                                // console.log("open",open);
                                statev.classList.add('open');
                                statev.textContent=openv;
                            }
                            if( window.hourResultv!=true)
                            {
                                statev.classList.add('close');
                                statev.textContent="Closed";
                                let closev=@json( __('serviceProvider.Closed') );
                                statev.textContent=closev;
                                // console.log("Result is", window.dayResult);
                            }
                        }
                        if( window.dayResultv!=true)
                        {
                            statev.classList.add('close');
                            statev.textContent="Closed";
                            let closev=@json( __('serviceProvider.Closed') );
                            statev.textContent=closev;
                            // console.log("Result is", window.dayResult);
                        }
                    }

                </script>

                @if($restaurant->state=="1")
                    <div class="d-flex flex-row justify-content-between state ">
                        <p class="state{{$restaurant->id}}"> </p>
                    </div>
                @elseif($restaurant->state=="0")
                    <div class="d-flex flex-row justify-content-between state ">
                        <p class="close "> {{__('serviceProvider.Closed')}}</p>
                    </div>
                @endif
            </div>

        @empty
        @endforelse
    </div>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.23/moment-timezone-with-data-2012-2022.min.js"></script>--}}
    <script>
        let m=moment();
        let days=new Array();
        let day=m.day()
        // if(day==0)
        // {
        //     console.log("day now",day=7);
        // }

        function isDayOpen(days,day=m.day())
        {
            if(day==0)
            {
                day=7;
                // console.log("day now",day=7);
            }
            day=day.toString();
            let dayChecked = days.includes(day);
            // console.log("days are",days);
            // console.log("day are",day);
            return dayChecked;

        }
        // checkHours();
        function checkHours(daysWithHour, day=m.day()) {

            let t1= moment().minute(daysWithHour.MinuteStart).hour(daysWithHour.HourStart);
            let t2= moment().minute(daysWithHour.MinuteEnd).hour(daysWithHour.HourEnd);
            let t3= moment();
            let r=t3.isBetween(t1, t2);
            return r;
            // console.log("daysWithHour",daysWithHour);
            // console.log("day",day);
            // console.log("daysWithHour",daysWithHour);
            // console.log("HourStart",daysWithHour.HourStart);
            // console.log("MinuteStart",daysWithHour.MinuteStart);
            // console.log("HourEnd",daysWithHour.HourEnd);
            // console.log("MinuteEnd",daysWithHour.MinuteEnd);
            // console.log("t1",t1);
            // console.log("t2",t2);
            // console.log("t3",t3);
            // console.log("r",r);


        }



    </script>

    <script type="text/javascript">
        // window.onload = function() {
        //     Livewire.emit('pageType')
        // }
    </script>

</div>
