<div class="content">
    <style>


    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        @if($type=="term")
                            <li class="breadcrumb-item active">{{__("footerPages.CC")}}  </li>
                        @elseif($type=="cookie")
                            <li class="breadcrumb-item active">{{__("footerPages.cp")}}  </li>
                        @elseif($type=="privacy")
                            <li class="breadcrumb-item active">{{__("footerPages.PP")}}  </li>
                        @elseif($type=="FAQ")
                            <li class="breadcrumb-item active">{{__("footerPages.FAQ")}}  </li>
                        @endif
                    </ol>

                    @include('livewire.modal.footerPages.add')
                    @include('livewire.modal.translation')
                    @include('livewire.modal.footerPages.up')
                    @include('livewire.modal.footerPages.termTrans')
                    @include('livewire.modal.deleteModel')

                    <div class="d-flex flex-row justify-content-between">
                        @canany(['is-manager'])
                            <a href="#addTerm" data-toggle="modal" wire:click="setCountryConfig">
                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                                </button>
                            </a>
                            @if($type!="FAQ")
                                <div class="dropdown">
                                    <button class="btn btn-outline-success dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        @if($countryName)
                                            {{$countryName}}
                                        @else
                                            {{__("footerPages.selectCountry")}}
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#"
                                           wire:click.prevent="getCities(0,'all')">
                                            {{__("footerPages.TermsForAllCountry")}}
                                        </a>
                                        @foreach($countries as $country)

                                            <a class="dropdown-item" href="#"
                                               wire:click.prevent="getCities({{ $country->country->id }}, '{{ $country->name }}')">
                                                {{$country->name}}
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endcan

                    </div>


                    <div wire:loading wire:target="getCities">
                        <x-control-panel.loading/>
                    </div>

                    {{--                    <div>--}}
                    {{--                        <button wire:click="$refresh">Reload Component</button>--}}
                    {{--                    </div>--}}

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            @if(count($countryTerms)!=0)
                                <thead class=" text-primary">
                                <th>

                                    {{__("footerPages.Address")}}
                                </th>
                                <th>
                                    {{__("footerPages.Content")}}
                                </th>
                                <th>
                                    {{__("masterControl.translated")}}
                                </th>

                                <th>
                                    {{__("footerPages.Action")}}
                                </th>
                                </thead>
                                <tbody>
                                @foreach($countryTerms as $term)
                                    <tr>
                                        <td>
                                            {{ \Illuminate\Support\Str::of($term->address)->words(6) }}

                                        </td>
                                        <td>
                                            {{ \Illuminate\Support\Str::of($term->content)->words(5) }}
                                        </td>

                                        <td>
                                            @if($languegeCount==count($term->countryTerm->translation))
                                                <label style="color: green">
                                                    <i class="fa fa-check ml-1 mr-1"
                                                       aria-hidden="true"></i>{{__("masterControl.TranslateCompleted")}}
                                                </label>
                                            @else
                                                <a href="#showTranslation" data-toggle="modal" class=""
                                                   wire:click="showTranslation({{$term->term_id}})">
                                                    <button type="button" class="btn btn-outline-info p-2 m-1">
                                                        <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                                    </button>
                                                </a>

                                            @endif
                                        </td>
                                        <td>
                                            @can('is-manager')
                                                <button wire:click="edit({{$term->term_id}})" data-toggle="modal"
                                                        data-target="#CountryTermUp"
                                                        class="btn btn-outline-success p-2 m-1">
                                                    <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                                </button>


                                                <a href="#delCountry" data-toggle="modal"
                                                   wire:click="callDelete({{$term->term_id}})">
                                                    <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                        <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                            @endcan
                                            @canany(['is-manager','is-translator'])

                                                {{--                                            <a href="{{route('restaurantFilterTranslate',['locale'=>app()->getLocale(),'id'=>$term->term_id])}}"--}}
                                                <a href="#Termtrans" data-toggle="modal"
                                                   wire:click="getAllTranslation({{$term->term_id}})"
                                                   class="btn btn-outline-primary p-2 m-1"><i
                                                        class="fa fa-language fa-2x" aria-hidden="true"></i>
                                                </a>
                                            @endcan

                                        </td>
                                @endforeach
                                </tbody>
                        </table>
                        <div class="d-flex flex-row justify-content-center">
                            {{ $countryTerms->links() }}
                        </div>
                        @else
                            <div class="alert alert-info">
                                @if($type=="term")
                                    {{__("footerPages.NoTerms")}}
                                @elseif($type=="cookie")
                                    {{__("footerPages.NoCookie")}}
                                @elseif($type=="privacy")
                                    {{__("footerPages.NoPrivacy")}}
                                @elseif($type=="FAQ")
                                    {{__("footerPages.NoFAQ")}}
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
