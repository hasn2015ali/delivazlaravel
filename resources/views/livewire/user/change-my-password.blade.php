<div class="d-flex flex-column align-items-lg-start">
    <button class="btn btn-outline-warning" data-toggle="collapse" data-target="#changeMyPassword">{{__("profile.changePassword")}}</button>

    <div style="width: 90%" wire:ignore.self id="changeMyPassword" class="collapse">
        @if($showOldPasswordFild)
    <form wire:submit.prevent="submitOldPassword">
      <div class="row">
          <div class="col-lg-10">
              <div class="form-group">
                  <label>  {{__("profile.oldPassword")}}</label>
                  <input type="password" class="form-control"   wire:model.lazy="oldPassword">
                  @error('oldPassword') <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      {{ $message }}</div>
                  @enderror
              </div>
          </div>

      </div>

        <div class="d-flex flex-row justify-content-center">
            <button class="btn btn-outline-primary">{{__("profile.Send")}}</button>
        </div>
    </form>
        @endif
        @if($showNewPasswordFild)

        <form wire:submit.prevent="submitNewPassword">
            <div class="row">
                <div class="col-lg-10">
                    <div class="form-group">
                        <label>  {{__("profile.newPassword")}}</label>
                        <input type="password" class="form-control"   wire:model.lazy="newPassword">
                        @error('newPassword') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

            </div>
            <div class="d-flex flex-row justify-content-center">
                <button class="btn btn-outline-primary">{{__("profile.Send")}}</button>
            </div>
        </form>

        @endif



    </div>

</div>
