<div class="col-md-9">
    <div class="card">
        <div class="card-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                <li class="breadcrumb-item active">{{__("user.Users")}}  </li>
                <li class="breadcrumb-item "><a href="{{route('users',['locale'=>app()->getLocale(),'type'=>$type])}}">{{ str_replace('_', ' ', $type)}}</a></li>
                <li class="breadcrumb-item active">{{__("user.Update_User")}}</li>
            </ol>
            <h5 class="title">{{__("user.update_Details")}}</h5>
            <hr style="background: #f96332; "/>
        </div>
        <div class="card-body">
            <form wire:submit.prevent="submit">
                <div class="row">
                    <div class="col-md-5 pr-1">
                        <div class="form-group">
                            <label>  {{__("user.first")}}</label>
{{--                                    {{dd($user->firstName)}}--}}
                            <input type="text" class="form-control"   wire:model.lazy="firstName">
                            @error('firstName') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <label>{{__("user.Last")}}</label>
                            <input type="text" class="form-control" placeholder="{{__("user.LastEnter")}}" wire:model.lazy="lastName">
                            @error('lastName') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                            <label>{{__("user.Father")}}</label>
                            <input  type="text" class="form-control" placeholder="{{__("user.FatherEnter")}}" wire:model.lazy="father">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <label  for="exampleInputEmail1"> {{__("user.Email_Address")}}</label>
                            <input type="email"  class="form-control" placeholder="{{__("user.Enter_Email_Address")}}" wire:model.lazy="email">
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="form-group">
                            <label>{{__("user.Phone")}}</label>
                            <input type="text" class="form-control" placeholder="{{__("user.Enter_Phone")}}" wire:model.lazy="phone">
                            @error('phone') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__("user.Address")}}</label>
                            <input type="text" class="form-control" placeholder="{{__("user.enter_address")}}"wire:model.lazy="address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label>{{__("user.Age")}}</label>
                            <input type="text" class="form-control" placeholder="{{__("user.Enter_Age")}}"wire:model.lazy="age" >
                        </div>
                    </div>
                    <div class="col-md-4 px-1">

                    </div>

                    <div class="col-md-4 px-1">
                        @if(!isset($user->provider_id))

                            <div class="form-group">
                                <label>{{__("user.password")}} </label>
                                <input type="password" class="form-control" placeholder="{{__("user.Enter_Password")}}"wire:model.lazy="password" value="Andrew">
                                @error('password') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        @endif

                    </div>

                </div>

                @if($type=='COO')
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>{{__("user.selectCountry")}}</label>
                                <select class="form-control" id="exampleFormControlSelect2" wire:model.lazy="selectedCountry">

                                    @foreach($countries as $country)
{{--                                        @if($country->country_id==$selectedCountry) selected @endif--}}
                                        <option value="{{$country->country_id}}" >{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                    </div>
                @endif

                @if($type=='Delivery_System_Team' or $type=='Delivery_Costumer_Team' or
                     $type=='Delivery_workers' or $type=='Customers' or $type=='Data_Entry_Manager')
                    <div class="row">
                        <div class="col-md-6">
                            @if ($showSelectCountry)
                            <div class="form-group">
                                <label>{{__("user.selectCountry")}}</label>
                                <select class="form-control" id="exampleFormControlSelect2" wire:change="getCities" wire:model.lazy="selectedCountry">
                                    <option value="null">---</option>
                                    @foreach($countries as $country)
                                        <option  value="{{$country->country_id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("user.selectCity")}}</label>
                                <select class="form-control" id="exampleFormControlSelect2" wire:change="getSystemManager" wire:model.lazy="selectedCity">
                                    <option value=" " >---</option>
                                    @if($cities)
                                        @foreach($cities as $city)
                                            <option value="{{$city->city_id}}">{{$city->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                @endif
                @if($type=='Delivery_workers')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("user.Select_Delivery_System_Manager")}}</label>
                                <select class="form-control" id="exampleFormControlSelect2" wire:model.lazy="Select_Delivery_Manager">
                                    <option>---</option>
                                    @if($Delivery_Managers)
                                        @foreach($Delivery_Managers as $manager)
                                            <option value="{{$manager->id}}">   {{$manager->firstName}}{{" " }}{{  $manager->fatherName}}{{" " }}{{  $manager->lastName}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-9">
                        {{--                                <div class="form-group">--}}
                        {{--                                    <label>More Details </label>--}}
                        {{--                                    <textarea rows="4" cols="80" class="form-control" placeholder="Here can be your description" value="Mike">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</textarea>--}}
                        {{--                                </div>--}}
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-outline-primary">{{__("user.Update_User")}} </button>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-10">

                </div>
            </div>

        </div>
    </div>
</div>

