<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item active">{{__("user.Users",['locale'=>app()->getLocale()])}}  </li>
                        <li class="breadcrumb-item active"> {{ str_replace('_', ' ', $type)}}</li>
{{--                        <li class="breadcrumb-item active">{{$roleId}}</li>--}}
                    </ol>

                    @include('livewire.modal.deleteModel')
                    @if($m)
                        {{$m}}
                    @else
                        <a href="{{route('addUser',['locale'=>app()->getLocale(),'type'=>$type])}}" class="btn btn-outline-success">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </a>
                    @endif

                </div>
                <div class="card-body">
                    <div  wire:loading wire:target="switchState">
                        <x-control-panel.loading />
                    </div>
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>

                                {{__("user.full_name")}}
                            </th>
                            <th>
                                {{__("user.Email")}}
                            </th>
                            <th>
                                {{__("user.Phone")}}
                            </th>
                            <th>
                                {{__("user.created_at")}}

                            </th>
                            @if($type=='COO')
                                <th>
                                    {{__("user.countrySelected")}}

                                </th>
                            @endif
                            @if($type=='Delivery_System_Team' or $type=='Delivery_Costumer_Team' or
                                $type=='Delivery_workers' or $type=='Customers' or $type=='Data_Entry_Manager')
                                <th>
                                    {{__("user.citySelected")}}

                                </th>
                            @endif
                                <th>
                                {{__("user.status")}}
                            </th>

                            <th>
                                {{__("user.Action")}}
                            </th>
                            </thead>
                            <tbody>
                            @if($users)
                            @foreach($users as $user)
                            <tr>
                                <td>
{{--                                    {{dd( $user->fatherName)}}--}}
                                    {{$user->firstName}}{{" " }}{{  $user->fatherName}}{{" " }}{{  $user->lastName}}
                                </td>
                                <td>
                                    @if(isset($user->email))
                                    {{$user->email}}
                                    @else
                                        <small>    {{__("user.cityNotAdded")}}</small>

                                        {{--                                    @elseif(isset($user->provider))--}}
{{--                                        @if($user->provider=="google")--}}
{{--                                            <small>    {{__("user.LoggedINWithGoogleAccount")}}</small>--}}
{{--                                        @elseif($user->provider=="facebook")--}}
{{--                                            <small>    {{__("user.LoggedINWithFacebookAccount")}}</small>--}}
{{--                                        @endif--}}
                                    @endif
                                </td>
                                <td>
                                    @if($user->phone!=0)

                                    {{$user->phone}}
                                    @else
                                        <small>    {{__("user.cityNotAdded")}}</small>
                                    @endif
                                </td>
                                <td>
                                    {{$user->created_at->toFormattedDateString()}}
                                </td>
{{--                                {{dd($type)}}--}}
                                @if($type=='COO')
                                    <td>
                                        {{$user->country->translation->where('code_lang',App::getlocale())->first()->name}}

                                    </td>
                                @endif
                                @if($type=='Delivery_System_Team' or $type=='Delivery_Costumer_Team' or
                                    $type=='Delivery_workers' or $type=='Customers' or $type=='Data_Entry_Manager')
                                    <td>
                                        @if(isset($user->city))
                                        {{$user->city->translation->where('code_lang',App::getlocale())->first()->name}}

                                        @else
                                           <small>    {{__("user.cityNotAdded")}}</small>
                                        @endif

                                    </td>
                                @endif
                                <td>
{{--                                    {{dd($user->status)}}--}}
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" wire:change="switchState({{$user->id}})" id="customSwitch1{{$user->id}}" @if($user->status==1) checked=" " @endif>
                                        <label class="custom-control-label" for="customSwitch1{{$user->id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{route('updateUser',['locale'=>app()->getLocale(),'id'=>$user->id,'type'=>$type])}}"  class="btn btn-outline-success p-2 m-1">
                                        <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                    </a>
                                    <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$user->id}})">
                                        <button type="button" class="btn btn-outline-danger p-2 m-1">
                                            <i class="fa fa-trash-o fa-2x"></i>
                                        </button>
                                    </a>
                                </td>



                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
