<div class="col-lg-12">
{{--    {{dd('tst')}}--}}
    <div class="card">
        <div class="card-header d-flex flex-row justify-content-center">
            <h5> {{__("user.updateWorkHours")}}</h5>


        </div>
        <hr style="background: #f96332; "/>

        <div class="card-body">
{{--            @if($type!='manager')--}}
            <div class="row">
                <div class="col-lg-12 ">
                <h6 class="user-work-address">{{__("user.userWorkType")}}</h6>
                @if($workType==0)
                   &nbsp;  &nbsp; <h6 class="user-day">{{__("user.FullTime")}}</h6>
                @elseif($workType==1)
                    &nbsp; &nbsp; <h6 class="user-day">{{__("user.PartTime")}}</h6>
                    <br/>
                       <div>
                           <h6 class="user-work-address"> {{__("user.userDay")}}</h6>&nbsp;&nbsp;
                           @foreach($daysWork as $day)
                               {{--                            {{dd($day->name)}}--}}
                               <h6 class="user-day">{{$day->days->name}}</h6>  &nbsp;
                           @endforeach
                       </div>
                        <h6 class="user-work-hour-address">{{__("user.userPartTime")}}</h6>

                        <table id="myTable" class="table">
                            <tbody>
                            @foreach($partTimesWork as $partTime)
{{--                                                                {{dd($partTimes)}}--}}
                                <tr>
                                    <td class="user-work-table">{{__("user.from")}}</td>
                                    <td class="user-day-table"> {{$partTime->workHour->startTimeHour}}{{ ": "  }} {{$partTime->workHour->startTimeMinutes}} </td>
                                    <td class="user-work-table">{{__("user.to")}}</td>
                                    <td class="user-day-table">{{$partTime->workHour->endTimeHour}}{{ ": "  }} {{$partTime->workHour->endTimeMinutes}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                @endif
                </div>
            </div>
            <div class="d-flex flex-row justify-content-center">
                <button class="btn btn-outline-warning"  data-toggle="collapse" data-target="#workHour">{{__("user.changeUserPartTime")}}</button>

            </div>


            <div wire:ignore.self id="workHour" class="collapse">
            <form wire:submit.prevent="submitPartTime">
                <div style="width: 100%" class="d-flex flex-row justify-content-center">
                    <div  wire:loading wire:target="workTypSelected">
                        <x-control-panel.loading class="m-3" />
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 ">
                            <div class="form-group">
                                <label>{{__("user.workType")}} </label>
                                @error('workType') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror

                                <div class="d-flex flex-row justify-content-lg-start " style="gap: 10px">

                                    <div style="display: inline-block"><label>{{__("user.FullTime")}} </label></div>
                                    <div class="custom-control custom-switch">

                                        <input type="radio" name="typeWork" class="custom-control-input" wire:change="hidePartTimes" wire:model="workTypSelected" value="0"  id="fullTime" checked=" ">
                                        <label  style="display: inline-block" class="custom-control-label" for="fullTime"></label>
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-lg-start " style="gap: 10px">
                                    <div style="display: inline-block"><label>{{__("user.PartTime")}} </label></div>
                                    <div class="custom-control custom-switch">

                                        <input type="radio" name="typeWork"  class="custom-control-input" wire:change="getPartTimes" wire:model="workTypSelected" value="1" id="partTime">
                                        <label  style="display: inline-block" class="custom-control-label" for="partTime"></label>
                                    </div>
                                </div>
                            </div>

                    </div>

                    <div class="col-lg-9 ">
                        @if($showPartTime)
                            <div class="d-flex flex-row justify-content-center mt-5 mb-2" style="gap: 7px">
                                @foreach($days as $day)

                                    <label>{{$day->name}} </label>
                                    <div class="custom-control custom-switch">
                                        <input value="{{$day->id}}" type="checkbox" class="custom-control-input" wire:model.defer="daySelected"  id="customSwitch1{{$day->id}}"  checked=" ">
                                        <label class="custom-control-label" for="customSwitch1{{$day->id}}"></label>
                                    </div>
                                @endforeach

                            </div>
                            <table id="myTable" class="table">
                                <tbody>
                                @foreach($partTimes as $partTime)
                                    {{--                                {{dd($partTimes)}}--}}
                                    <tr>
                                        <td>{{__("user.from")}}</td>
                                        <td> {{$partTime->startTimeHour}}{{ ": "  }} {{$partTime->startTimeMinutes}} </td>
                                        <td>{{__("user.to")}}</td>
                                        <td>{{$partTime->endTimeHour}}{{ ": "  }} {{$partTime->endTimeMinutes}}</td>
                                        <td>
                                            <div class="custom-control custom-switch">
                                                <input value="{{$partTime->id}}" type="checkbox" class="custom-control-input"  wire:model.defer="partTimeSelected" id="customSwitch12{{$partTime->id}}"  >
                                                <label class="custom-control-label" for="customSwitch12{{$partTime->id}}"></label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
                <div class="d-flex flex-row justify-content-center">
                    <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                </div>
            </form>
        </div>

{{--        @endif--}}

        </div>
    </div>
</div>
