<div class="d-flex flex-row notification-section"  x-data="{ showNotification: false }">
    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" @click="showNotification=!showNotification">
        <i class="fa fa-bell" aria-hidden="true"></i>
        <span class="badge badge-warning card-count badge-pill">{{$allNotifications}}</span>
    </a>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink"
         wire:ignore.self
         x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="showNotification"
         @click.away="showNotification=false" x-cloak>
        @if($allNotifications==0)
            <p class="dropdown-item">
                {{__("masterControl.NoNotification")}}
            </p>
        @endif
        @if($countOFNewRestaurant!=0)
            <a class="dropdown-item"
               href="{{route('restaurantRegistered',['locale'=>app()->getLocale()])}}">
                <span class="badge badge-warning card-count badge-pill">{{$countOFNewRestaurant}}</span>
                {{__("restaurant.NewRestaurants")}}
            </a>
        @endif
        @if($countOFNewVendor!=0)

            <a class="dropdown-item"
               href="{{route('vendorRegistered',['locale'=>app()->getLocale()])}}">
                <span class="badge badge-warning card-count badge-pill">{{$countOFNewVendor}}</span>
                {{__("vendor.NewVendors")}}
            </a>
        @endif


    </div>
</div>
