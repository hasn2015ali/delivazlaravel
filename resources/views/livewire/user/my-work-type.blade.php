<div class="col-lg-12">
    <div class="">
        <div class="d-flex flex-row justify-content-lg-start">
            <h5> {{__("profile.WorkHours")}}</h5>
        </div>
{{--        <hr style="background: #f96332; "/>--}}

        <div class="">
            <div class="row">
                <div class="col-lg-12 ">
                    <h6 class="user-work-address">{{__("profile.WorkHours1")}}</h6>
                    @if($workType==0)
                        &nbsp;  &nbsp; <h6 class="user-day">{{__("profile.FullTime")}}</h6>
                    @elseif($workType==1)
                        &nbsp; &nbsp; <h6 class="user-day">{{__("profile.PartTime")}}</h6>
                        <br/>
                        <div>
                            <h6 class="user-work-address"> {{__("profile.userDay")}}</h6>&nbsp;&nbsp;
                            @foreach($daysWork as $day)
                                {{--                            {{dd($day->name)}}--}}
                                <h6 class="user-day">{{$day->days->name}}</h6>  &nbsp;
                            @endforeach
                        </div>
                        <h6 class="user-work-hour-address">{{__("profile.userPartTime")}}</h6>

                        <table id="myTable" class="table">
                            <tbody>
                            @foreach($partTimesWork as $partTime)
                                {{--                                                                {{dd($partTimes)}}--}}
                                <tr>
                                    <td class="user-work-table">{{__("profile.from")}}</td>
                                    <td class="user-day-table"> {{$partTime->workHour->startTimeHour}}{{ ": "  }} {{$partTime->workHour->startTimeMinutes}} </td>
                                    <td class="user-work-table">{{__("profile.to")}}</td>
                                    <td class="user-day-table">{{$partTime->workHour->endTimeHour}}{{ ": "  }} {{$partTime->workHour->endTimeMinutes}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
