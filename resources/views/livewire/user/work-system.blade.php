<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item active">{{__("workSystem.workSystem")}}</li>
                    </ol>

                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.hours.add')
{{--                    @include('livewire.modal.day.add')--}}

                    <div class="d-flex flex-row justify-content-between">
{{--                        <a href="#addDay" data-toggle="modal">--}}
{{--                            <button type="button" class="btn btn-outline-success p-2 m-1">--}}
{{--                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>--}}
{{--                            </button>--}}
{{--                        </a>--}}

                        <a href="#addWorkHour" data-toggle="modal">
                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </a>

                    </div>


                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("workSystem.Start_Part_Time")}}
                            </th>
                            <th>
                                {{__("workSystem.End_Part_Time")}}
                            </th>

                            <th>
                                {{__("masterControl.Action")}}
                            </th>


                            </thead>

                            <tbody>
                            @foreach($partTimes as $partTime)
                                <tr>

                                    <td>
                                        {{$partTime->startTimeHour}}{{ ": "  }} {{$partTime->startTimeMinutes}}
                                    </td>
                                    <td>
                                        {{$partTime->endTimeHour}}{{ ": "  }} {{$partTime->endTimeMinutes}}
                                    </td>

                                    <td>
                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$partTime->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                    </td>

                                </tr>


                            @endforeach

                            </tbody>
                        </table>

                        <div class="d-flex flex-row justify-content-center">
                            {{ $partTimes->links() }}
                        </div>
                    </div>
{{--                    <hr>--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table id="myTable" class="table">--}}
{{--                            <thead class=" text-primary">--}}
{{--                            <th>--}}
{{--                                {{__("workSystem.day")}}--}}
{{--                            </th>--}}

{{--                            <th>--}}
{{--                                {{__("masterControl.Action")}}--}}
{{--                            </th>--}}


{{--                            </thead>--}}

{{--                            <tbody>--}}
{{--                            @foreach($days as $day)--}}
{{--                                <tr>--}}

{{--                                    <td>--}}
{{--                                        {{$day->name}}--}}
{{--                                    </td>--}}

{{--                                    <td>--}}
{{--                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$day->id}})">--}}
{{--                                            <button type="button" class="btn btn-outline-danger p-2 m-1">--}}
{{--                                                <i class="fa fa-trash-o fa-2x"></i>--}}
{{--                                            </button>--}}
{{--                                        </a>--}}
{{--                                    </td>--}}

{{--                                </tr>--}}


{{--                            @endforeach--}}

{{--                            </tbody>--}}
{{--                        </table>--}}

{{--                        <div class="d-flex flex-row justify-content-center">--}}
{{--                            {{ $days->links() }}--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>

    </div>
</div>


{{--</div>--}}
