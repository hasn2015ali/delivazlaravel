<div class="col-md-9">
    <div class="card">
        <div class="card-header">
            <h5 class="title">  {{__("profile.myprofile")}}</h5>
            <hr style="background: #f96332; "/>
        </div>
        <div class="card-body">
            <form>
                <div class="row">
                    <div class="col-md-5 pr-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.FirstName")}}  </h6>&nbsp; &nbsp;<h6 class="profile-content">{{$user->firstName}}</h6>

                        </div>
                    </div>
                    <div class="col-md-3 px-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.LastName")}} </h6> &nbsp; &nbsp;<h6 class="profile-content">{{$user->lastName}}</h6>
                        </div>
                    </div>
                    <div class="col-md-4 pl-1">
                        <div class="form-group">
                            <h6 class="profile-address" >{{__("profile.FatherName")}}  </h6>&nbsp; &nbsp;<h6 class="profile-content">{{$user->fatherName}}</h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.EmailAddress")}}  </h6> &nbsp;&nbsp;<h6 class="profile-content">{{$user->email}}</h6>
                        </div>
                    </div>
                    <div class="col-md-6 pl-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.Phone")}} </h6>&nbsp;&nbsp;<h6 class="profile-content">{{$user->phone}}</h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.Address")}} </h6>&nbsp;&nbsp;<h6 class="profile-content">{{$user->address}}</h6>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.Age")}} </h6>&nbsp;&nbsp;<h6 class="profile-content">{{$user->age}}</h6>

                        </div>
                    </div>

                </div>
                @if(!empty($user->country))
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.Country")}}&nbsp;&nbsp;
                                @foreach($user->country->translation->where('code_lang',App::getLocale()) as $trans)

                                    <h6 class="profile-content">{{$trans->name}}</h6>
                                @endforeach

                            </h6>
                        </div>
                    </div>
                    @if(!empty($user->city))
                    <div class="col-md-8 px-1">
                        <div class="form-group">
                            <h6 class="profile-address">{{__("profile.City")}}&nbsp;&nbsp;
                                @foreach($user->city->translation->where('code_lang',App::getLocale()) as $trans)

                                    <h6 class="profile-content">{{$trans->name}}</h6>
                                @endforeach
                            </h6>
                        </div>
                    </div>
                    @endif

                </div>
                @endif
{{--                        <div class="row">--}}
{{--                            <div class="col-md-9">--}}
{{--                                <div class="form-group">--}}
{{--                                    <h6>More Details:{{$user->first_name}} </h6>--}}
{{--                                    <textarea rows="4" cols="80" class="form-control" placeholder="Here can be your description" value="Mike">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</textarea>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-3">--}}
{{--                                <button type="submit" class="btn btn-outline-primary">Add User </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
            </form>
{{--            {{dd('tst')}}--}}
            @if($user->role_id!=9 and $user->role_id!=10 )
            <hr style="background: #f96332; "/>


            <div class="row">
                <div class="col-lg-12">
                    <livewire:user.my-work-type />

                </div>
            </div>
            @endif

            <hr style="background: #f96332; "/>
            <div class="row">
                <div class="col-md-10">
                    <livewire:user.change-my-password />

                </div>
            </div>

        </div>
    </div>
</div>





