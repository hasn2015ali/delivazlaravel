<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item active">{{__("user.Users")}}  </li>
                        <li class="breadcrumb-item "><a href="{{route('users',['locale'=>app()->getLocale(),'type'=>$type])}}">{{ str_replace('_', ' ', $type)}}</a></li>
                        <li class="breadcrumb-item active">{{__("user.Add_New_user")}} {{ str_replace('_', ' ', $type)}}</li>
                    </ol>
                    <h5 class="title">{{__("user.Enter_Details")}}</h5>
                    <hr style="background: #f96332; "/>
                </div>
                <div class="card-body">
                    <form wire:submit.prevent="submit">
                        <div class="row">
                            <div class="col-md-5 pr-1">
                                <div class="form-group">
                                    <label>  {{__("user.first")}}</label>
                                    <input type="text" class="form-control"  placeholder="{{__("user.firstEnter")}}" wire:model.defer="firstName">
                                    @error('firstName') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-3 px-1">
                                <div class="form-group">
                                    <label>{{__("user.Last")}}</label>
                                    <input type="text" class="form-control" placeholder="{{__("user.LastEnter")}}" wire:model.defer="lastName">
                                    @error('lastName') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 pl-1">
                                <div class="form-group">
                                    <label>{{__("user.Father")}}</label>
                                    <input  type="text" class="form-control" placeholder="{{__("user.FatherEnter")}}" wire:model.defer="father">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label  for="exampleInputEmail1"> {{__("user.Email_Address")}}</label>
                                    <input type="email"  class="form-control" placeholder="{{__("user.Enter_Email_Address")}}" wire:model.defer="email">
                                    @error('email') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 pl-1">
                                <div class="form-group">
                                    <label>{{__("user.Phone")}}</label>
                                    <input type="number" class="form-control" placeholder="{{__("user.Enter_Phone")}}" wire:model.defer="phone">
                                    @error('phone') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{__("user.Address")}}</label>
                                    <input type="text" class="form-control" placeholder="{{__("user.enter_address")}}"wire:model.defer="address" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label>{{__("user.Age")}}</label>
                                    <input type="text" class="form-control" placeholder="{{__("user.Enter_Age")}}"wire:model.defer="age" >
                                </div>
                            </div>
                            <div class="col-md-4 px-1">

                            </div>

                            <div class="col-md-4 px-1">

                                    <div class="form-group">
                                        <label>{{__("user.password")}} </label>
                                        <input type="password" class="form-control" placeholder="{{__("user.Enter_Password")}}"wire:model.defer="password" value="Andrew">
                                        @error('password') <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>

                            </div>

                        </div>

                        @if($type=='COO')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__("user.selectCountry")}}</label>
                                        <select class="form-control" id="exampleFormControlSelect2" wire:model.defer="selectedCountry">
                                            <option>---</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->country_id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($type=='Delivery_System_Team' or $type=='Delivery_Costumer_Team' or
                                 $type=='Delivery_workers' or $type=='Customers' or $type=='Data_Entry_Manager')

                            <div class="row">
                                <div class="col-md-6">
                                    @if ($showSelectCountry)
                                        <div class="form-group">
                                            <label>{{__("user.selectCountry")}}</label>
                                            <select class="form-control" id="exampleFormControlSelect2" wire:change="getCities" wire:model.defer="selectedCountry">
                                                <option>---</option>
                                                @foreach($countries as $country)
                                                    <option  value="{{$country->country_id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__("user.selectCity")}}</label>
                                        <select class="form-control" id="exampleFormControlSelect2" wire:change="getSystemManager" wire:model.defer="selectedCity">
                                            <option>---</option>
                                            @if($cities)
                                                @foreach($cities as $city)
                                                    <option value="{{$city->city_id}}">{{$city->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($type=='Delivery_workers')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__("user.Select_Delivery_System_Manager")}}</label>
                                        <select class="form-control" id="exampleFormControlSelect2" wire:model.defer="Select_Delivery_Manager">
                                            <option>---</option>
                                            @if($Delivery_Managers)
                                            @foreach($Delivery_Managers as $manager)
                                                <option value="{{$manager->id}}">   {{$manager->firstName}}{{" " }}{{  $manager->fatherName}}{{" " }}{{  $manager->lastName}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div style="width: 100%" class="d-flex flex-row justify-content-center">
                            <div  wire:loading wire:target="workType">
                                <x-control-panel.loading class="m-3" />
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-3 ">
                                @if($type!='manager')
                                    <div class="form-group">
                                        <label>{{__("user.workType")}} </label>
                                        @error('workType') <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    <div class="d-flex flex-row justify-content-lg-start " style="gap: 10px">
                                        <div style="display: inline-block"><label>{{__("user.FullTime")}} </label></div>
                                        <div class="custom-control custom-switch">

                                            <input type="radio" name="typeWork" class="custom-control-input" wire:change="hidePartTimes" wire:model.defer="workType" value="0"  id="fullTime" checked=" ">
                                            <label  style="display: inline-block" class="custom-control-label" for="fullTime"></label>
                                        </div>
                                    </div>
                                  <div class="d-flex flex-row justify-content-lg-start " style="gap: 10px">
                                      <div style="display: inline-block"><label>{{__("user.PartTime")}} </label></div>
                                      <div class="custom-control custom-switch">

                                          <input type="radio" name="typeWork"  class="custom-control-input" wire:change="getPartTimes" wire:model.defer="workType" value="1" id="partTime">
                                          <label  style="display: inline-block" class="custom-control-label" for="partTime"></label>
                                      </div>
                                  </div>
                                  </div>
                                @endif
                            </div>

                            <div class="col-lg-9 ">
                           @if($showPartTime)
                               <div class="d-flex flex-row justify-content-center mt-5 mb-2" style="gap: 7px">
                                  @foreach($days as $day)
                                       <label>{{$day->name}} </label>
                                       <div class="custom-control custom-switch">
                                           <input value="{{$day->id}}" type="checkbox" class="custom-control-input" wire:model.defer="daySelected"  id="customSwitch1{{$day->id}}"  checked=" ">
                                           <label class="custom-control-label" for="customSwitch1{{$day->id}}"></label>
                                       </div>
                                   @endforeach

                               </div>
                            <table id="myTable" class="table">
                                <tbody>
                               @foreach($partTimes as $partTime)
                                   <tr>
                                       <td>{{__("user.from")}}</td>
                                       <td> {{$partTime->startTimeHour}}{{ ": "  }} {{$partTime->startTimeMinutes}} </td>
                                       <td>{{__("user.to")}}</td>
                                       <td>{{$partTime->endTimeHour}}{{ ": "  }} {{$partTime->endTimeMinutes}}</td>
                                       <td>
                                           <div class="custom-control custom-switch">
                                               <input value="{{$partTime->id}}" type="checkbox" class="custom-control-input"  wire:model.defer="partTimeSelected" id="customSwitch{{$partTime->id}}"  >
                                               <label class="custom-control-label" for="customSwitch{{$partTime->id}}"></label>
                                           </div>
                                       </td>
                                   </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @endif

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-8 handle-input">
                                <div wire:loading wire:target="images">
                                    <x-control-panel.loading />
                                </div>
{{--                                <label>{{__("user.Add_Images")}} </label>--}}
{{--                                <input type="file" class="form-control" wire:model.defer="images" multiple>--}}
                                <label>{{__("user.Add_Images")}}
                                    <i class="fa fa-cloud-upload"></i>
                                    <input type="file" class="form-control file-upload" wire:model="images" multiple>
                                </label>

                                @error('images')
{{--                                {{dd( error('images.*') )}}--}}
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-4 handle-input">
                                <div wire:loading wire:target="personalImage">
                                    <x-control-panel.loading />
                                </div>
{{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                                <label>{{__("user.Add_Personal_Image")}}
                                    <i class="fa fa-cloud-upload"></i>
                                    <input type="file" class="form-control file-upload" wire:model.defer="personalImage" >
                                </label>
                                @error('personalImage') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
{{--                          <div>--}}
{{--                              <form id="dropzoneFrom" method="post" action="{{route('addPersonalPhoto')}}" class="dropzone">--}}
{{--                                  @csrf--}}
{{--                                  <div class="fallback">--}}
{{--                                      <input name="file" type="file" wire:model.defer="personalImage" />--}}
{{--                                  </div>--}}
{{--                              </form>--}}
{{--                          </div>--}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9">
{{--                                <input type="file" class="form-control">{{__("user.Add_Personal_Image")}} />--}}
                                {{--                                <div class="form-group">--}}
                                {{--                                    <label>More Details </label>--}}
                                {{--                                    <textarea rows="4" cols="80" class="form-control" placeholder="Here can be your description" value="Mike">Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo.</textarea>--}}
                                {{--                                </div>--}}
                            </div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-outline-primary">{{__("user.Add_User")}} </button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-10">

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</div>

