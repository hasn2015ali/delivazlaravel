<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item active">{{__("country.Countries")}}</li>

                    </ol>
                    @include('livewire.modal.country.country')
                    @include('livewire.modal.country.countryUp')
                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.translation')
                    {{--start modal--}}
                    @can('is-manager')
                    <a href="#addCountry" data-toggle="modal">
                        <button type="button" class="btn btn-outline-success p-2 m-1">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endcan
                    <script>
                        // import Swal from 'sweetalert2';

                    </script>
                    @if (session()->has('message'))

                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif
                <!-- The Modal -->


                    {{--                            end modal--}}

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("country.Name")}}
                            </th>
                            <th>
                                {{__("country.code")}}
                            </th>
                            <th>
                                {{__("masterControl.Action")}}
                            </th>
                            <th>
                                {{__("masterControl.translated")}}
                            </th>
                            <th>
                                {{__("country.Cities")}}
                            </th>
                            </thead>
                            <tbody>

                            @foreach($countries as $country)
                                <tr>
                                    <td>
                                        {{$country->name}}

                                    </td>
                                    <td>
                                        {{$country->country->code}}
                                    </td>
                                    <td>
                                        @can('is-manager')
{{--start modal--}}
                                        <a href="#CountryUp" data-toggle="modal" wire:click="edit({{$country}})">
                                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                                <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a href="#delCountry" data-toggle="modal" wire:click="callDelete({{$country->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a href="{{route('countrySetting',['locale'=>app()->getLocale(),'id'=>$country->country_id])}}">
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        @endcan

{{--                                            <a href="{{route('countryTranslate',['locale'=>app()->getLocale(),'id'=>$country->country->id])}}" class="btn btn-outline-primary p-2 m-1"><i class="fa fa-language fa-2x" aria-hidden="true"></i></a>--}}

{{--                            end modal--}}

                                        @canany([ 'is-manager','is-translator'])
                                        <a href="{{route('countryTranslate',['locale'=>app()->getLocale(),'id'=>$country->country->id])}}" class="btn btn-outline-primary p-2 m-1"><i class="fa fa-language fa-2x" aria-hidden="true"></i></a>
                                        @endcan
                                    </td>
                                    <td>
{{--                                       {{dd($country->country->translation)}}--}}
                                        @if($languegeCount==count($country->country->translation))
                                            <label style="color: green">
                                                <i class="fa fa-check ml-1 mr-1" aria-hidden="true"></i>{{__("masterControl.TranslateCompleted")}}
                                            </label>
                                        @else
                                        <a href="#showTranslation" data-toggle="modal" class="" wire:click="showTranslation({{$country->country_id}})">
{{--                                            {{__("country.Show")}}--}}
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                            </button>

                                        </a>
                                        @endif
                                    </td>
                                <td>
                                    <a class="" href="{{route('showCountryCity',['locale'=>app()->getLocale(),'id'=>$country->country->id])}}">
{{--                                        {{__("country.Show")}}--}}
                                        <button type="button" class="btn btn-outline-info p-2 m-1">
                                            <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                        </button>

                                    </a>
                                </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        <div class="d-flex flex-row justify-content-center">
                            {{ $countries->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


{{--</div>--}}
