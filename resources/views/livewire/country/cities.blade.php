<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item"><a href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a> </li>
                        <li class="breadcrumb-item active">  {{$country->translation->where('code_lang','en')->first()->name}}</li>
                        <li class="breadcrumb-item active">Cities</li>
                    </ol>

                    @include('livewire.modal.city.city')
                    @include('livewire.modal.city.cityUp')
                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.translation')
                    @include('livewire.modal.city.cityPhone')

                @can('is-manager')
                    <a href="#addCity" data-toggle="modal">
                        <button type="button" class="btn btn-outline-success p-2 m-1">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endcan
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("city.Name")}}
                            </th>
                            <th>
                                {{__("masterControl.translated")}}
                            </th>
                            @can('is-manager')
                            <th>
                                {{__("city.state")}}
                            </th>
                            @endcan
                            <th>
                                {{__("city.Phones")}}
                            </th>
                            <th>
                                {{__("city.Action")}}
                            </th>
                            </thead>
                            <tbody>
                            @foreach($cities as $city)
{{--                                @foreach($city->translation->where('code_lang','en') as $trans)--}}
                            <tr>
                                <td>

                                    {{$city->name}}
                                </td>

                                <td>
                                    @if($languegeCount==count($city->city->translation))
                                        <label style="color: green">
                                            <i class="fa fa-check ml-1 mr-1" aria-hidden="true"></i>{{__("masterControl.TranslateCompleted")}}
                                        </label>
                                    @else
                                        <a href="#showTranslation" data-toggle="modal"class="" wire:click="showTranslation({{$city->city_id}})">
{{--                                            {{__("country.Show")}}--}}
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>

                                    @endif
                                </td>

                                @canany([ 'is-manager','is-data_entry_manager'])
                                @can('is-manager')

                                <td>
                                    {{--                                    {{dd($user->status)}}--}}
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" wire:change="switchState({{$city->city_id}})" id="customSwitchCity{{$city->city_id}}" @if($city->city->state==1) checked=" " @endif>
                                        <label class="custom-control-label" for="customSwitchCity{{$city->city_id}}"></label>
                                    </div>
                                </td>
                                    @endcan
                                <td>
                                    <a href="#CityPhone" data-toggle="modal" wire:click="editPhone({{$city->city}})">
                                        <button type="button" class="btn btn-outline-info p-2 m-1">
                                            <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>
                                    @can('is-manager')
                                    <a href="#CityUp" data-toggle="modal" wire:click="edit({{$city}})">
                                        <button type="button" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </a>


                                    <a href="#delCountry" data-toggle="modal" wire:click="callDelete({{$city->city->id}})">
                                        <button type="button" class="btn btn-outline-danger p-2 m-1">
                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </a>
                                    @endcan
{{--                                        href="{{route('cityShapes',['locale'=>$locale,'id'=>$id])}}"--}}
                                    {{--                                        @canany([ 'is-manager','is-data_entry_manager'])--}}
                                        <a href="{{route('cityShapes',['locale'=>app()->getLocale(),'id'=>$city->city->id])}}">
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        @endcan
                                    @canany([ 'is-manager','is-translator'])
                                    <a href="{{route('cityTranslate',['locale'=>app()->getLocale(),'id'=>$city->city->id])}}" class="btn btn-outline-primary p-2 m-1"><i class="fa fa-language fa-2x" aria-hidden="true"></i></a>
                                    @endcan
                                </td>


                            </tr>
{{--                                @endforeach--}}
                            @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex flex-row justify-content-center">
                            {{ $cities->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
