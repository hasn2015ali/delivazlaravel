<div class="delivery-setting-section">
    <form wire:submit.prevent="saveSetting">

        <div class="row">
            <div class="col-md-7">
                <div class="form-group ">
                    <label> {{__("city.areaMin")}}</label>
                    <input type="number" class="form-control" wire:model.defer="areaMin"
                           placeholder="{{__("city.areaMinEX")}}">
                    @error('areaMin')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group ">
                    <label> {{__("city.areaMax")}}</label>
                    <input type="number" class="form-control" wire:model.defer="areaMax"
                           placeholder="{{__("city.areaMaxEX")}}">
                    @error('areaMax')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group ">
                    <label> {{__("city.priceIn")}} ({{$countryCurrency}})</label>
                    <input type="number" class="form-control" wire:model.defer="priceIn"
                           placeholder="{{__("city.priceInEX")}}" step="0.01">
                    @error('priceIn')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group ">
                    <label> {{__("city.workerRatioIN")}}</label>
                    <input type="number" class="form-control" wire:model.defer="workerRatioIN"
                           placeholder="{{__("city.workerRatioINEX")}}">
                    @error('workerRatioIN')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-7">
                <div class="form-group ">
                    <label> {{__("city.priceOut")}} ({{$countryCurrency}})</label>
                    <input type="number" class="form-control" wire:model.defer="priceOut"
                           placeholder="{{__("city.priceOutEX")}}" step="0.01">
                    @error('priceOut')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group ">
                    <label> {{__("city.workerRatioOut")}}</label>
                    <input type="number" class="form-control" wire:model.defer="workerRatioOut"
                           placeholder="{{__("city.workerRatioOutEX")}}">
                    @error('workerRatioOut')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-7">
                <label> {{__("city.SelectTax")}}</label>
                <div class="dropdown mr-3 ml-3 " id="cities_list">
                    <button id="dropdown_selecte_btn_city" type="button"
                            class="btn btn-outline-success dropdown-toggle"
                            data-toggle="dropdown">
                        @if(count($selectDeliveryTax)>0)
                            @if($selectDeliveryTax['type']=="Ratio")
                                {{$selectDeliveryTax['value']}} %
                            @else
                                {{$selectDeliveryTax['value']}} {{$countryCurrency}}
                            @endif
                        @else
                            {{__("city.SelectTax")}}
                        @endif
                    </button>
                    <div class="dropdown-menu">
                        @foreach($taxesInCountry as $tax)

                            <a href="#"
                               wire:click.prevent="sendTaxValue({{json_encode($tax)}})">
                                <p class="dropdown-item">
                                    {{$tax['value']}}
                                    @if($tax['type']=="Ratio")
                                        %
                                    @else
                                        {{$countryCurrency}}
                                    @endif
                                </p>

                            </a>
                        @endforeach

                    </div>
                </div>
                @error('selectDeliveryTax')
                <div class="alert alert-danger2">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-5">

            </div>
        </div>

        <div class="d-flex justify-content-center">
            <button type="submit" class="btn btn-outline-primary">
                {{__("masterControl.Save")}}
            </button>
        </div>

    </form>

</div>
