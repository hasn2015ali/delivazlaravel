<div>
    @include('livewire.modal.city.recommended.addRestaurant')
    @include('livewire.modal.city.recommended.upRestaurant')

    @include('livewire.modal.city.recommended.addVendor')
    @include('livewire.modal.city.recommended.upVendor')

    {{--    @include('livewire.modal.city.recommended.addVendor')--}}
    <div class="d-flex flex-row justify-content-center">
        <h6 class="address">{{__("city.Restaurants")}}</h6>
    </div>
    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRecommendedRestaurant" data-toggle="modal">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                </button>
            </a>

            <a href="#editRecommendedRestaurant" data-toggle="modal">
                <button type="button" class="btn btn-outline-info p-2 m-1">
                    <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                </button>
            </a>
        @endcan
    </div>

    <livewire:country.city-details.restaurant-component :cityID="$cityID"/>
    <hr style="background: #f96332; "/>

    <div class="d-flex flex-row justify-content-center">
        <h6 class="address">{{__("city.Vendors")}}</h6>
    </div>
    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRecommendedVendor" data-toggle="modal">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                </button>
            </a>

            <a href="#editRecommendedVendor" data-toggle="modal">
                <button type="button" class="btn btn-outline-info p-2 m-1">
                    <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                </button>
            </a>
        @endcan
    </div>
    <livewire:country.city-details.vendor-component :cityID="$cityID"/>

</div>

