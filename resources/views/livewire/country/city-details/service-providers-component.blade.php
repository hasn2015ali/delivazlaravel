<div>
    @include('livewire.modal.city.pages.addRestaurantsBanners')
    @include('livewire.modal.city.pages.upRestaurantsBanners')

    <div class="d-flex flex-row justify-content-center">
        <h6 class="address">{{__("city.RestaurantsPage")}}</h6>
    </div>
    <div class="d-flex flex-row justify-content-start">
        <h6 class="content-text">{{__("city.RestaurantsBanners")}}</h6>
    </div>
    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRestaurantsBanners" data-toggle="modal" wire:click="setTypeRestaurantBanner">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                </button>
            </a>

            <a href="#editRestaurantsBanners" data-toggle="modal" wire:click="getRestaurantBanner">
                <button type="button" class="btn btn-outline-info p-2 m-1">
                    <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                </button>
            </a>
        @endcan
    </div>

    <livewire:country.city-details.restaurants-banners-component :cityID="$cityID"/>

    <div class="d-flex flex-row justify-content-start mt-4 mb-2">
        <h6 class="content-text">{{__("city.RestaurantsCover")}}</h6>
    </div>

    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRestaurantsBanners" data-toggle="modal" wire:click="setTypeRestaurantCover">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                </button>
            </a>


        @endcan
    </div>

    @if(isset($restaurantsCover))

        <div class=""><img src="{{asset('storage/images/cover/city/restaurants/'.$restaurantsCover->name)}}"></div>

    @endif


    <hr style="background: #f96332; "/>

    <div class="d-flex flex-row justify-content-center mt-3">
        <h6 class="address">{{__("city.ShopsPage")}}</h6>
    </div>
    <div class="d-flex flex-row justify-content-start mt-2">
        <h6 class="content-text">{{__("city.ShopsBanners")}}</h6>
    </div>

    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRestaurantsBanners" data-toggle="modal" wire:click="setTypeShopsBanner">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                </button>
            </a>

            <a href="#editRestaurantsBanners" data-toggle="modal" wire:click="getShopsBanner">
                <button type="button" class="btn btn-outline-info p-2 m-1">
                    <i class="fa fa-gear  fa-2x" aria-hidden="true"></i>
                </button>
            </a>
        @endcan
    </div>

    <livewire:country.city-details.shops-banners-component :cityID="$cityID"/>


    <div class="d-flex flex-row justify-content-between">
        @canany([ 'is-manager','is-data_entry_manager'])
            <a href="#addRestaurantsBanners" data-toggle="modal" wire:click="setTypeShopCover">
                <button type="button" class="btn btn-outline-success p-2 m-1">
                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                </button>
            </a>


        @endcan
    </div>

    @if(isset($shopsCover))
        <div class=""><img src="{{asset('storage/images/cover/city/shops/'.$shopsCover->name)}}"></div>
    @endif


</div>

