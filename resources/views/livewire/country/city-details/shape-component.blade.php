<div class="table-responsive">
    <table id="myTable" class="table">
        <thead class=" text-primary">
        <tr>
            <th>{{__("city.shape")}} </th>
            <th>{{__("city.shapeIcon")}} </th>
            <th>{{__("city.shapeAddress")}} </th>
            <th>{{__("city.action")}} </th>
            <th>{{__("city.shapeState")}} </th>


        </tr>
        </thead>
        @include('livewire.modal.shape.oldContent')
        @include('livewire.modal.shape.trans')
        @for($j=1;$j<13;$j++)

        <tr>
            <td>
                {{$j}}
            </td>

            <td>
               <label class="d-flex flex-row">
                   @if ( ${"photoOld".$j} and ${"photoOld".$j}!="icon")
                       <img class="max-w-100 m-2"
                            src="@if(${"photoOld".$j}!="icon") {{asset('storage/images/shape/'.${"photoOld".$j})}} @endif">
                   @else
                       {{__("city.empty")}}
                   @endif
               </label>
            </td>

            <td>
                {{ ${"address".$j} }}
            </td>

            <td class="">
                <a href="#addContent{{$j}}" data-toggle="modal" wire:click="setShapeNumber({{$j}})">
                    <button type="button" class="btn btn-outline-success p-2 m-1">
                        <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                    </button>
                </a>
                @include('livewire.modal.shape.content')

                <a href="#addShapTrans" data-toggle="modal" wire:click="transShape({{$j}})">
                    <button type="submit"  class="btn btn-outline-primary p-2 m-1">
                        <i class="fa fa-language fa-2x" aria-hidden="true"></i>
                    </button>
                </a>
                <a href="#addShowOldContent" data-toggle="modal" wire:click="ShowOldContent({{$j}})">
                    <button type="submit"  class="btn btn-outline-info p-2 m-1">
                        <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                    </button>
                </a>

            </td>

            <td>

                <div class="form-group ">
                    <div class="custom-control custom-switch">
                        <input type="checkbox"  class="custom-control-input" wire:change="changeShapeState({{$j}})" id="customSwitchShap{{$j}}" @if(${"state".$j}==1)   checked=" " @endif>
                        <label class="custom-control-label" for="customSwitchShap{{$j}}"> </label>
                    </div>
                </div>

            </td>

        </tr>


        @endfor

    </table>
</div>
