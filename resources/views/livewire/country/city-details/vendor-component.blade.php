<div>
    {{--    @include('livewire.modal.deleteModel')--}}
    @if(count($recommendedVendors)!=0)
        <div class="owl-carousel carousel-vendor owl-theme">
            @foreach($recommendedVendors as $vendor)
                <div class="item">
                    <img  src="{{asset('storage/images/sliders/recommended/'.$vendor->photo)}}">
                    <h1>{{$vendor->serviceProvider->name}}</h1>
                    <h6>{{$vendor->content}}</h6>


                </div>
            @endforeach
        </div>
    @elseif(count($recommendedVendors)==0)
        <div class="alert alert-info">
            {{__("city.noVendor")}}
        </div>
    @endif
</div>

<script>
    $(document).ready(function(){
        $('.carousel-vendor').owlCarousel({
            autoplay:true,
            autoplayHoverPause:true,
            smartSpeed:250,
            loop:true,
            // margin:10,
            center:true,
            // // nav:true,
            dots:true,

            items:3,
            margin:0,
            stagePadding:0,
            // smartSpeed:450,
            animateOut: 'fadeOut',
            // autoWidth:true,
            // animateOut: 'slideOutDown',
            // animateIn: 'flipInX',
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:3
                }
            }
        })
    });
</script>
<style>
    .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span
    {
        background: #F96332!important;
    }
    .owl-theme .owl-dots  span
    {
        width: 3vw!important;
        height: 0.8vh!important;
    }
    /*.card-header{*/
    /*    border-radius: 15px!important;*/

    /*}*/
    /*.owl-carousel img{*/

    /*        border-radius: 15px!important;*/


    /*}*/
</style>
