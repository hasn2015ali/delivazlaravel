<div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12 handle-input">
                @error('cityBanner')
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror

                @error('serviceProviderID')
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}
                </div>
                @enderror

                <div wire:loading wire:target="cityBanner">
                    <x-control-panel.loading/>
                </div>
                @if ($cityBanner)
                    <img class="banner-loaded " src=" {{ $cityBanner->temporaryUrl() }}">
                @endif
                <label class="d-flex flex-row">
                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="cityBanner">
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group handel-select">
                    <label for="exampleFormControlSelect1">
                        {{__("city.selectTypeRestaurantOrShop")}}
                    </label>
                    <select class="form-control" id="exampleFormControlSelect1" wire:model="type">
                        <option>---</option>
                        <option value="restaurant">restaurant</option>
                        <option value="shop">shop</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group handel-select">
                    <label for="exampleFormControlSelect1">
                        {{__("city.selectRestaurantOrShop")}}
                    </label>
                    <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="serviceProviderID">
                        <option>---</option>
                        @foreach($serviceProviders as $serviceProvider)
                            <option
                                value="{{$serviceProvider['serviceProviderID']}}">{{$serviceProvider['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>


        <div class="d-flex flex-row justify-content-start">

        </div>
        <div class="form-group d-flex align-items-center flex-column">

            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
        </div>
    </form>
    @include('livewire.modal.deleteModel2')

    <div class="card-body">
        <div class="table-responsive">
            <table id="myTable" class="table">
                <thead class=" text-primary">
                <th>
                    {{__("city.Banner")}}
                </th>

                <th>
                    {{__("city.BannerState")}}
                </th>
                <th>
                    {{__("masterControl.Action")}}
                </th>


                </thead>

                <tbody>
                @foreach($banners as $banner)
                    <tr>

                        <td>
                            <img class="banner-loaded " src=" {{asset('storage/images/sliders/city/'.$banner->name)}}">

                        </td>

                        <td>
                            <div class="form-group ">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input"
                                           wire:change="changeBannerState({{$banner->id}})"
                                           id="customSwitchBa{{$banner->id}}" @if($banner->state==1) checked=" " @endif>
                                    <label class="custom-control-label" for="customSwitchBa{{$banner->id}}"> </label>
                                </div>
                            </div>

                        </td>
                        <td>
                            <a href="#delCountry2" data-toggle="modal" wire:click="deleteCall({{$banner->id}})">
                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                    <i class="fa fa-trash-o fa-2x"></i>
                                </button>
                            </a>
                        <td>

                    </tr>


                @endforeach

                </tbody>
            </table>

            <div class="d-flex flex-row justify-content-center">
                {{ $banners->links() }}
            </div>
        </div>
    </div>
</div>
