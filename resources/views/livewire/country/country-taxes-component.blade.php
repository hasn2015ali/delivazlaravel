<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <?php
                $langCode = \Illuminate\Support\Facades\App::getLocale();
                ?>
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        <li class="breadcrumb-item active"><a
                                href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a>
                        </li>
                        <li class="breadcrumb-item active"> {{$country->translation->where('code_lang',$langCode)->first()->name}}</li>

                    </ol>
                    @include('admin.country.countrySettingNav')
                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.country.editTaxes')
                    {{--                    @include('livewire.modal.country.countryUp')--}}


                    <div class="d-flex flex-column justify-content-start mt-3">
                        @can('is-manager')
                            <a href="#addTax" wire:click="resetValues" data-toggle="modal">
                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                </button>
                            </a>
                        @endcan

                        @if (session()->has('message'))

                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session('message') }}
                            </div>
                        @endif
                    </div>

                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane mt-3  active " >
                            @if(count($taxes)>0)
                            <div class="table-responsive">

                                    <table id="myTable" class="table">
                                        <thead class=" text-primary">
                                        <th>
                                            {{__("tax.Name")}}
                                        </th>
                                        <th>
                                            {{__("tax.Type")}}
                                        </th>
                                        <th>
                                            {{__("tax.Value")}}
                                        </th>
                                        <th>
                                            {{__("masterControl.Action")}}
                                        </th>

                                        </thead>
                                        <tbody>


                                        @foreach($taxes as $tax)
                                        <tr>
                                            <td>
                                                {{$tax->name}}
                                            </td>
                                            <td>
                                                {{$tax->type}}
                                            </td>
                                            <td>
                                                @if( $tax->type=="Ratio")
                                                    {{$tax->value}}%
                                                @else
                                                    {{$tax->value}}

                                                @endif
                                            </td>
                                            <td>
                                                @can('is-manager')

                                                    <a href="#delCountry" data-toggle="modal" wire:click="callDelete({{$tax->id}})">
                                                        <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                                        </button>
                                                    </a>
                                                @endcan
                                            </td>


                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>

                            </div>
                            @else
                                <div class="alert alert-info">
                                    {{__("tax.NoTaxes")}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



