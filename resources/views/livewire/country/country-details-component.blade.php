<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <?php
                $langCode = \Illuminate\Support\Facades\App::getLocale();
                ?>
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        <li class="breadcrumb-item active"><a
                                href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a>
                        </li>
                        <li class="breadcrumb-item active"> {{$country->translation->where('code_lang',$langCode)->first()->name}}</li>

                    </ol>
                    @include('admin.country.countrySettingNav')
                    @include('livewire.modal.country.addDetails')
                    {{--                    @include('livewire.modal.country.countryUp')--}}


                    <div class="d-flex flex-column justify-content-start mt-3">
                        @can('is-manager')
                            <a href="#addDetails" wire:click="setValue" data-toggle="modal">
                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                </button>
                            </a>
                        @endcan

                        @if (session()->has('message'))

                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ session('message') }}
                            </div>
                        @endif
                    </div>

                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane mt-3  active " >
                    <div class="table-responsive">
                        @if($country->currency or $country->currency_code)
                            <table id="myTable" class="table">
                                <thead class=" text-primary">
                                <th>
                                    {{__("country.currency")}}
                                </th>
                                <th>
                                    {{__("country.currencyCode")}}
                                </th>
                                <th>
                                    {{__("country.numberCode")}}
                                </th>
                                <th>
                                    {{__("country.phoneNumber")}}
                                </th>

                                </thead>
                                <tbody>


                                <tr>
                                    <td>
                                        {{$country->currency}}
                                    </td>
                                    <td>
                                        {{$country->currency_code}}
                                    </td>

                                    <td>
                                        @if($country->code_number)
                                            {{$country->code_number}}
                                        @else
                                            <small>   {{__("country.notFound")}}</small>
                                        @endif
                                    </td>

                                    <td>
                                        @if($country->phone1 and $country->phone1!=0)
                                            @if($oldIcon1)
                                                <img class="food-img-loaded "
                                                     src="{{asset('storage/images/phone/country/'.$oldIcon1)}}">
                                            @endif
                                            {{$country->phone1}}
                                        @else
                                            <small>   {{__("country.notFound")}}</small>
                                        @endif
                                    </td>


                                    {{--                                    <td>--}}
                                    {{--                                        @can('is-manager')--}}
                                    {{--                                            --}}{{--start modal--}}
                                    {{--                                            <a href="#CountryUp" data-toggle="modal" wire:click="edit({{$country}})">--}}
                                    {{--                                                <button type="button" class="btn btn-outline-success p-2 m-1">--}}
                                    {{--                                                    <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>--}}
                                    {{--                                                </button>--}}
                                    {{--                                            </a>--}}
                                    {{--                                            <a href="#delCountry" data-toggle="modal" wire:click="callDelete({{$country->id}})">--}}
                                    {{--                                                <button type="button" class="btn btn-outline-danger p-2 m-1">--}}
                                    {{--                                                    <i class="fa fa-trash fa-2x" aria-hidden="true"></i>--}}
                                    {{--                                                </button>--}}
                                    {{--                                            </a>--}}
                                    {{--                                        @endcan--}}
                                    {{--                                    </td>--}}
                                </tr>
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                {{__("country.noInfo")}}
                            </div>
                        @endif
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



