<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        <li class="breadcrumb-item "><a href="{{route('showCountries',['locale'=>app()->getLocale()])}}">{{__("country.Countries")}}</a></li>
                        <li class="breadcrumb-item active">{{$country->translation->where('code_lang','en')->first()->name}}</li>
                        <li class="breadcrumb-item "><a href="{{route('showCountryCity',['locale'=>app()->getLocale(),'id'=>$country->id])}}">{{__("city.cities")}}</a></li>
                        <li class="breadcrumb-item active">{{$city->translation->where('code_lang','en')->first()->name}}</li>
                        <li class="breadcrumb-item active">{{__("country.translate")}}</li>
                    </ol>
                    @include('livewire.modal.city.cityTranslate')
                    @include('livewire.modal.city.cityTranslateUp')
                    @include('livewire.modal.deleteModel')
                    @canany([ 'is-manager','is-translator'])

                    <a href="#translateCountry" data-toggle="modal">
                        <button type="button" class="btn btn-outline-success p-2 m-1">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endcan

                    @if (session()->has('message'))
                        <div class="container alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("city.Name")}}
                            </th>
                            <th>
                                {{__("city.translate")}}
                            </th>
                            <th>
                                {{__("city.lang")}}
                            </th>
                            <th>
                                {{__("masterControl.Action")}}
                            </th>
                            </thead>

                            <tbody>
                            @foreach($translations as $translate)
                                <tr>
                                    <td>
                                        {{$cityEn->name}}
                                    </td>
                                    <td>
                                        {{$translate->name}}
                                    </td>
                                    <td>
                                        {{$translate->lang->name}}
                                    </td>
                                    <td>
                                        @canany([ 'is-manager','is-translator'])
                                        {{--                                        start modal--}}
                                        <button wire:click="edit({{$translate}})"  data-toggle="modal" data-target="#translateCountryUp" class="btn btn-outline-success p-2 m-1">
                                            {{--                                            <button type="button" class="btn btn-outline-success p-2 m-1">--}}
                                            <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                            {{--                                            </button>--}}
                                        </button>


                                        {{--                                        start modal--}}
                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$translate->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                        @endcan
                                        {{--                                                        end modal--}}
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <div class="d-flex flex-row justify-content-center">
                            {{ $translations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


{{--</div>--}}
