<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__("subscriber.subscribers")}}  </li>
                    </ol>

                    @include('livewire.modal.deleteModel')

                    <div wire:loading wire:target="getCities">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="sendCityVal">
                        <x-control-panel.loading/>
                    </div>
                    <div class="d-flex flex-row justify-content-between">
                        @can('is-manager')
                            <div class="d-flex flex-row justify-content-start">
                                <div class="dropdown">
                                    <button class="btn btn-outline-success dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        @if($countryName)
                                            {{$countryName}}
                                        @else
                                            {{__("subscriber.selectCountry")}}
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                        @foreach($countries as $country)
                                            <a class="dropdown-item" href="#"
                                               wire:click.prevent="getCities({{ $country->country->id }}, '{{ $country->name }}')">{{$country->name}}</a>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="dropdown mr-3 ml-3 " id="cities_list">
                                    <button id="dropdown_selecte_btn_city" type="button"
                                            class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown">
                                        @if($countryName)
                                            @if($cityName)
                                                {{$cityName}}
                                            @else
                                                {{__("home.selectCity")}}
                                            @endif

                                        @else
                                            {{__("home.selectCity")}}
                                        @endif
                                    </button>
                                    <div class="dropdown-menu">
                                        @if(!empty($countryName))
                                            @foreach($cities as $item)

                                                <a href="#"
                                                   wire:click.prevent="sendCityVal({{ $item->city->id }}, '{{ $item->name }}')">
                                                    <p class="dropdown-item">{{$item->name}}</p>

                                                </a>
                                            @endforeach
                                        @else
                                            <p class="dropdown-item"><small> {{__("home.selectCountryFirst")}}</small></p>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        @endcan

                        @if($isSubcsribers)
                            <div>
                                <a href="/en/subscribers-print/{{$countryIdSub}}/{{$cityIdSub}}" class="btn btn-outline-info p-2 m-1" >
                                    <i class="fa fa-print fa-2x"></i>
                                </a>
                            </div>
                        @endif
                    </div>


                </div>
                @if($isSubcsribers)
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="myTable" class="table">
                                @if(count($subscribers)!=0)
                                    <thead class=" text-primary">
                                    <th>

                                        {{__("subscriber.name")}}
                                    </th>
                                    <th>
                                        {{__("subscriber.Email")}}
                                    </th>
                                    <th>
                                        {{__("subscriber.created_at")}}

                                    </th>


                                    <th>
                                        {{__("subscriber.Action")}}
                                    </th>
                                    </thead>

                                @endif
                                <tbody>
                                @forelse($subscribers as $subscriber)
                                    <tr>
                                        <td>
                                            {{$subscriber->name}}
                                        </td>
                                        <td>
                                            {{$subscriber->email}}
                                        </td>
                                        <td>
                                            {{(\Carbon\Carbon::parse($subscriber->created_at)->format('d/m/Y'))}}
                                        </td>


                                        <td>
                                            <a href="#delCountry" data-toggle="modal" wire:click="deleteCall( {{$subscriber->id}})">
                                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                    <i class="fa fa-trash-o fa-2x"></i>
                                                </button>
                                            </a>
                                        </td>
                        </div>
                    </div>
                    @empty
                        <div class="alert alert-info">
                            {{__("subscriber.NoSubscribers")}}
                        </div>

                    @endforelse
                @endif
            </div>
        </div>
    </div>

</div>
</div>
