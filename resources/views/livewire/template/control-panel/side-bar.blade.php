<div class="sidebar">
    <div class="open-close-container">
        <button class="btn-open-close-sideBar mt-1">
            <span class="close-sideBar">
                <i class="fas fa-angle-left"></i>
            </span>
            <span class="open-sideBar d-none">
                <i class="fas fa-angle-right"></i>
            </span>
        </button>
    </div>

{{--    {{dd($active,$active=="Basic information",$active=="Products")}}--}}
    <div class="icon-list d-none">
        <ul class="nav  ul-side-bar">
            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Basic information") active @endif" href="{{route('Basic-Info',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-info-circle"></i>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Work Times") active @endif" href="{{route('Scheduling-Time',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-clock"></i>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Banners") active @endif" href="{{route('Banners',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-image"></i>
                </a>
            </li>
{{--            <li class="ul-item  ">--}}
{{--                <a class="btn btn-navigation @if($active=="Sizes") active @endif" href="{{route('Sizes',['locale'=>app()->getLocale()])}}">--}}
{{--                    --}}{{--                    <i class="fa fa-cog" aria-hidden="true"></i>--}}
{{--                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                        <path d="M12 5H16V16H12V5ZM6 0H10V16H6V0ZM0 8H4V16H0V8Z" fill="white"/>--}}
{{--                    </svg>--}}
{{--                </a>--}}
{{--            </li>--}}
            @if($user->role_id ==9)
                <li class="ul-item  ">
                    <a class="btn btn-navigation @if($active=="Foods" or  $active=="foodSizes" or  $active=="foodMenu") active @endif" href="{{route('ControlPanel.Food',['locale'=>app()->getLocale()])}}">
                        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 20.98C0 21.54 0.45 21.99 1.01 21.99H14C14.56 21.99 15.01 21.54 15.01 20.98V20H0V20.98ZM7.5 7.99C3.75 7.99 0 10 0 14H15C15 10 11.25 7.99 7.5 7.99ZM2.62 12C3.73 10.45 6.09 9.99 7.5 9.99C8.91 9.99 11.27 10.45 12.38 12H2.62ZM0 16H15V18H0V16ZM17 4V0H15V4H10L10.23 6H19.79L18.39 20H17V22H18.72C19.56 22 20.25 21.35 20.35 20.53L22 4H17Z" fill="white"/>
                        </svg>

                    </a>
                </li>
            @elseif($user->role_id ==10)
                <li class="ul-item  ">
                    <a class="btn btn-navigation @if($active=="Products" or  $active=="foodSizes" or  $active=="foodMenu") active @endif" href="{{route('ControlPanel.Products',['locale'=>app()->getLocale()])}}">
                        <i class="fas fa-list"></i>
                    </a>
                </li>
            @endif

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Orders") active @endif" href="{{route('Orders',['locale'=>app()->getLocale()])}}">
                    <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.55 11C15.3 11 15.96 10.59 16.3 9.97L19.88 3.48C20.25 2.82 19.77 2 19.01 2H4.21L3.27 0H0V2H2L5.6 9.59L4.25 12.03C3.52 13.37 4.48 15 6 15H18V13H6L7.1 11H14.55ZM5.16 4H17.31L14.55 9H7.53L5.16 4ZM6 16C4.9 16 4.01 16.9 4.01 18C4.01 19.1 4.9 20 6 20C7.1 20 8 19.1 8 18C8 16.9 7.1 16 6 16ZM16 16C14.9 16 14.01 16.9 14.01 18C14.01 19.1 14.9 20 16 20C17.1 20 18 19.1 18 18C18 16.9 17.1 16 16 16Z" fill="white"/>
                    </svg>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Ratting && Reviews") active @endif" href="{{route('Reviews',['locale'=>app()->getLocale()])}}">
                    {{--                    <i class="fas fa-star"></i>--}}
                    <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g filter="url(#filter0_d_1753:126)">
                            <path d="M22 7.24L14.81 6.62L12 0L9.19 6.63L2 7.24L7.46 11.97L5.82 19L12 15.27L18.18 19L16.55 11.97L22 7.24ZM12 13.4V4.1L13.71 8.14L18.09 8.52L14.77 11.4L15.77 15.68L12 13.4Z" fill="white"/>
                        </g>
                        <defs>
                            <filter id="filter0_d_1753:126" x="-2" y="0" width="28" height="27" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                <feOffset dy="4"/>
                                <feGaussianBlur stdDeviation="2"/>
                                <feComposite in2="hardAlpha" operator="out"/>
                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1753:126"/>
                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1753:126" result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                </a>
            </li>


            {{--            <li class="ul-item " data-toggle="collapse" data-target="#demo">--}}
            {{--                <a class="btn btn-navigation">--}}
            {{--                    <i class="fa fa-users" aria-hidden="true"></i>--}}
            {{--                    <p>  {{__("user.Users")}}</p>--}}
            {{--                </a>--}}

            {{--                <div id="demo"--}}
            {{--                     class="collapse handel-collapse change-pointer-event  show ">--}}

            {{--                        <a class="btn btn-navigation-child"--}}
            {{--                           href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                            <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                            <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                        </a>--}}

            {{--                    <a class="btn btn-navigation-child"--}}
            {{--                       href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                        <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                        <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                    </a>--}}
            {{--                    <a class="btn btn-navigation-child"--}}
            {{--                       href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                        <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                        <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                    </a>--}}



            {{--                </div>--}}
            {{--            </li>--}}

        </ul>
    </div>
    <div class="sidebar-container">
{{--        <div >--}}
{{--            <h4 class="address"> <i class="fas fa-cogs"></i> &nbsp;{{__("RestaurantControlPanel.ControlPanel")}}</h4>--}}
{{--        </div>--}}
{{--        <hr style="width: 100%; height: 1px ;background: white; margin-bottom: 2rem;"/>--}}
        <ul class="nav  ul-side-bar">
            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Basic information") active @endif" href="{{route('Basic-Info',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-info-circle"></i>
                    <p>{{__("RestaurantControlPanel.BasicInformation")}}</p>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Work Times") active @endif" href="{{route('Scheduling-Time',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-clock"></i>
                    <p>{{__("RestaurantControlPanel.workHours")}}</p>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Banners") active @endif" href="{{route('Banners',['locale'=>app()->getLocale()])}}">
                    <i class="fas fa-image"></i>
                    <p>{{__("RestaurantControlPanel.Banners")}}</p>
                </a>
            </li>
{{--            <li class="ul-item  ">--}}
{{--                <a class="btn btn-navigation @if($active=="Sizes") active @endif" href="{{route('Sizes',['locale'=>app()->getLocale()])}}">--}}
{{--                    <i class="fa fa-cog" aria-hidden="true"></i>--}}
{{--                    <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                        <path d="M12 5H16V16H12V5ZM6 0H10V16H6V0ZM0 8H4V16H0V8Z" fill="white"/>--}}
{{--                    </svg>--}}
{{--                    <p>{{__("RestaurantControlPanel.Sizes")}}</p>--}}
{{--                </a>--}}
{{--            </li>--}}
            @if($user->role_id ==9)
                <li class="ul-item  ">
                    <a class="btn btn-navigation @if($active=="Foods" or  $active=="foodSizes" or  $active=="foodMenu") active @endif" href="{{route('ControlPanel.Food',['locale'=>app()->getLocale()])}}">
                        <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 20.98C0 21.54 0.45 21.99 1.01 21.99H14C14.56 21.99 15.01 21.54 15.01 20.98V20H0V20.98ZM7.5 7.99C3.75 7.99 0 10 0 14H15C15 10 11.25 7.99 7.5 7.99ZM2.62 12C3.73 10.45 6.09 9.99 7.5 9.99C8.91 9.99 11.27 10.45 12.38 12H2.62ZM0 16H15V18H0V16ZM17 4V0H15V4H10L10.23 6H19.79L18.39 20H17V22H18.72C19.56 22 20.25 21.35 20.35 20.53L22 4H17Z" fill="white"/>
                        </svg>

                        <p>{{__("RestaurantControlPanel.Foods")}}</p>
                    </a>
                </li>
            @elseif($user->role_id ==10)
                <li class="ul-item  ">
                    <a class="btn btn-navigation @if($active=="Products" or  $active=="foodSizes" or  $active=="foodMenu") active @endif" href="{{route('ControlPanel.Products',['locale'=>app()->getLocale()])}}">
                        <i class="fas fa-list"></i>
                        <p>{{__("RestaurantControlPanel.Products")}}</p>
                    </a>
                </li>
            @endif
            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Orders") active @endif" href="{{route('Orders',['locale'=>app()->getLocale()])}}">
                    <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M14.55 11C15.3 11 15.96 10.59 16.3 9.97L19.88 3.48C20.25 2.82 19.77 2 19.01 2H4.21L3.27 0H0V2H2L5.6 9.59L4.25 12.03C3.52 13.37 4.48 15 6 15H18V13H6L7.1 11H14.55ZM5.16 4H17.31L14.55 9H7.53L5.16 4ZM6 16C4.9 16 4.01 16.9 4.01 18C4.01 19.1 4.9 20 6 20C7.1 20 8 19.1 8 18C8 16.9 7.1 16 6 16ZM16 16C14.9 16 14.01 16.9 14.01 18C14.01 19.1 14.9 20 16 20C17.1 20 18 19.1 18 18C18 16.9 17.1 16 16 16Z" fill="white"/>
                    </svg>

                    <p>{{__("RestaurantControlPanel.Orders")}}</p>
                </a>
            </li>

            <li class="ul-item  ">
                <a class="btn btn-navigation @if($active=="Ratting && Reviews") active @endif" href="{{route('Reviews',['locale'=>app()->getLocale()])}}">
{{--                    <i class="fas fa-star"></i>--}}
                    <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g filter="url(#filter0_d_1753:1263)">
                            <path d="M22 7.24L14.81 6.62L12 0L9.19 6.63L2 7.24L7.46 11.97L5.82 19L12 15.27L18.18 19L16.55 11.97L22 7.24ZM12 13.4V4.1L13.71 8.14L18.09 8.52L14.77 11.4L15.77 15.68L12 13.4Z" fill="white"/>
                        </g>
                        <defs>
                            <filter id="filter0_d_1753:1263" x="-2" y="0" width="28" height="27" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                <feOffset dy="4"/>
                                <feGaussianBlur stdDeviation="2"/>
                                <feComposite in2="hardAlpha" operator="out"/>
                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1753:126"/>
                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1753:126" result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                    <p>{{__("RestaurantControlPanel.Ratting&&Reviews")}}</p>
                </a>
            </li>


            {{--            <li class="ul-item " data-toggle="collapse" data-target="#demo">--}}
            {{--                <a class="btn btn-navigation">--}}
            {{--                    <i class="fa fa-users" aria-hidden="true"></i>--}}
            {{--                    <p>  {{__("user.Users")}}</p>--}}
            {{--                </a>--}}

            {{--                <div id="demo"--}}
            {{--                     class="collapse handel-collapse change-pointer-event  show ">--}}

            {{--                        <a class="btn btn-navigation-child"--}}
            {{--                           href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                            <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                            <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                        </a>--}}

            {{--                    <a class="btn btn-navigation-child"--}}
            {{--                       href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                        <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                        <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                    </a>--}}
            {{--                    <a class="btn btn-navigation-child"--}}
            {{--                       href="{{route('users',['locale'=>app()->getLocale(),'type'=>'Manager'])}}">--}}
            {{--                        <i class="fas fa-user-cog" aria-hidden="true"></i>--}}
            {{--                        <p>       {{__("masterControl.Admins")}}</p>--}}

            {{--                    </a>--}}



            {{--                </div>--}}
            {{--            </li>--}}

        </ul>
    </div>

</div>
