<div class="menu-section">
    <div class="heading2">
        <div class="btn-container">
            <a class="btn btn-back-delivaz"
               @if($user->role_id==10)
               href="{{route('ControlPanel.Products',['locale'=>$locale])}}"
               @elseif($user->role_id==9)
               href="{{route('ControlPanel.Food',['locale'=>$locale])}}"
                @endif
            >
                <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                        fill="#FE2E17"/>
                </svg>
                {{__('RestaurantControlPanel.Back')}}
            </a>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    @if($user->role_id==10)
                        {{__("RestaurantControlPanel.Products")}}
                    @elseif($user->role_id==9)
                        {{__("RestaurantControlPanel.Foods")}}
                    @endif
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{__("RestaurantControlPanel.Menu")}}
                </li>
            </ol>
        </nav>
    </div>
    @if(count($menuFilters)!=0)
    <div class="content">


            <div class="list">
                <div class="d-flex flex-column justify-content-start header">
                    <p class="address">
                        {{__('RestaurantControlPanel.RestaurantMenuConfig')}}

                    </p>
                    <div class="d-flex flex-row justify-content-between w-100">
                        <div class="d-flex flex-row justify-content-start w-50">
                            <p class="address">
                                {{__('RestaurantControlPanel.FilterState')}}

                            </p>
                        </div>
                        <div class="d-flex flex-row justify-content-end w-50">
                            <p class="address">
                                {{__('RestaurantControlPanel.FilterOrder')}}
                            </p>
                        </div>
                    </div>
                </div>


                @foreach($menuFilters as $filter)
                    <?php
                    if ($this->user->role_id == 9) {
//                        dd($filter);
                        $filterID = $filter['meal_filter_id'];
                    } elseif ($this->user->role_id == 10) {
                        $filterID = $filter['product_filter_id'];

                    }
                    ?>
                    <div class="custom-switch-container d-flex flex-row w-100 ">
                        <div class="custom-control custom-switch w-50 d-flex flex-row justify-content-start">
                            <input type="checkbox" wire:model.lazy="active.{{$filterID}}" class=""
                                   id="customSwitch{{$filterID}}"
                            >
                            <label class="custom-control-label" for="customSwitch{{$filterID}}">
                                {{$filter['name']}}
                            </label>
                        </div>


                        <div class="w-50 d-flex flex-row justify-content-end">
                            <div class="user-input ">
                                {{--                        <label>{{__('RestaurantControlPanel.LastName')}}</label>--}}
                                <input type="number" class="form-control " wire:model="order.{{$filterID}}"/>

                            </div>
                        </div>
                    </div>
                @endforeach

            </div>

    </div>

    <div class="d-flex flex-row justify-content-center width-full mt-5">
        <div class="btn-container">
            <button type="button" wire:click.prevent="saveFilter" class="btn btn-save-delivaz mr-2 ml-2">
                {{__('RestaurantControlPanel.SaveChanges')}}
            </button>
            <button type="button" wire:click.prevent="cancelNewChanges" class="btn btn-cancle2-delivaz mr-2 ml-2">
                {{__('RestaurantControlPanel.Cancel')}}
            </button>
        </div>
    </div>
    @else
        <p class="address">
            @if($user->role_id==10)
                {{__('RestaurantControlPanel.NoMenuUsedProduct')}}
            @elseif($user->role_id==9)
                {{__('RestaurantControlPanel.NoMenuUsedFood')}}

            @endif

        </p>
    @endif

</div>
