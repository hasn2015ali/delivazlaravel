<div class="add-food-section">
    <div class="heading2">
        <div class="btn-container">
            <a class="btn btn-back-delivaz" href="{{route('ControlPanel.Products',['locale'=>$locale])}}">
                <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                        fill="#FE2E17"/>
                </svg>
                {{__('RestaurantControlPanel.Back')}}
            </a>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    {{__("RestaurantControlPanel.Products")}}
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{__("RestaurantControlPanel.AddNewProduct")}}
                </li>
            </ol>
        </nav>
    </div>
    <div class="add-new">
        <form wire:submit.prevent="addNewFood">
            <div class="food-info ">
                <div class="d-flex flex-column user-input2">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.ProductName')}}</label>
                        <input class="form-control " wire:model.defer="productName"/>
                        @error('productName')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="user-input ">
                        <label>{{__('RestaurantControlPanel.productDetails')}}</label>
                        <textarea class="form-control" id="exampleFormControlTextareaUID"
                                  wire:model.defer="productDetails"
                                  rows="6">
                        </textarea>

                        @error('productDetails')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div wire:loading wire:target="productPhoto">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-column user-input2 align-items-end">
                    <div class="user-input d-flex justify-content-end width-full">
                        <div class="input-file-container">
                            @if($productPhoto)
                                <div class="img">
                                    <img src="{{ $productPhoto->temporaryUrl() }}"/>
                                </div>
                            @endif
                            <input type="file" class="form-control banner-input"
                                   wire:model.defer="productPhoto"/>

                            <div class="input-handel  @if($productPhoto) back-ground-transparent @endif">
                                <div class="items">
                                    <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                            fill="#FE2E17"/>
                                    </svg>
                                    <p class="text1">{{__('RestaurantControlPanel.DragProductPhoto')}}
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>

                    @error('productPhoto')
                    <div class="alert alert-danger2 w-50">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                    <div wire:loading wire:target="otherProductPhoto1">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="otherProductPhoto2">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="otherProductPhoto3">
                        <x-control-panel.loading/>
                    </div>
                    <div class="food-info flex-column other-photo">
                        <label>{{__('RestaurantControlPanel.OtherProductPhoto')}}</label>
                        <div class="other-photo">

                            <div class="user-input d-flex justify-content-end">
                                <div class="input-file-container">
                                    @if($otherProductPhoto1)
                                        <div class="img">
                                            <img src="{{ $otherProductPhoto1->temporaryUrl() }}"/>

                                        </div>
                                    @endif

                                    <input type="file" class="form-control banner-input"
                                           wire:model.defer="otherProductPhoto1"/>
                                    <div class="input-handel @if($otherProductPhoto1) back-ground-transparent @endif">
                                        <div class="items">
                                            <svg width="28" height="20" viewBox="0 0 28 20" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22.575 7.71268C21.7817 3.68768 18.2467 0.666016 14 0.666016C10.6283 0.666016 7.7 2.57935 6.24167 5.37935C2.73 5.75268 0 8.72768 0 12.3327C0 16.1943 3.13833 19.3327 7 19.3327H22.1667C25.3867 19.3327 28 16.7193 28 13.4993C28 10.4193 25.6083 7.92268 22.575 7.71268ZM16.3333 11.166V15.8327H11.6667V11.166H8.16667L13.5917 5.74102C13.825 5.50768 14.1867 5.50768 14.42 5.74102L19.8333 11.166H16.3333Z"
                                                    fill="#FE2E17"/>
                                            </svg>

                                        </div>
                                    </div>
                                </div>
                                <div class="input-file-container">
                                    @if($otherProductPhoto2)
                                        <div class="img">
                                            <img src="{{ $otherProductPhoto2->temporaryUrl() }}"/>
                                        </div>
                                    @endif
                                    <input type="file" class="form-control banner-input"
                                           wire:model.defer="otherProductPhoto2"/>
                                    <div class="input-handel @if($otherProductPhoto2) back-ground-transparent @endif">
                                        <div class="items">
                                            <svg width="28" height="20" viewBox="0 0 28 20" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22.575 7.71268C21.7817 3.68768 18.2467 0.666016 14 0.666016C10.6283 0.666016 7.7 2.57935 6.24167 5.37935C2.73 5.75268 0 8.72768 0 12.3327C0 16.1943 3.13833 19.3327 7 19.3327H22.1667C25.3867 19.3327 28 16.7193 28 13.4993C28 10.4193 25.6083 7.92268 22.575 7.71268ZM16.3333 11.166V15.8327H11.6667V11.166H8.16667L13.5917 5.74102C13.825 5.50768 14.1867 5.50768 14.42 5.74102L19.8333 11.166H16.3333Z"
                                                    fill="#FE2E17"/>
                                            </svg>

                                        </div>

                                    </div>
                                </div>
                                <div class="input-file-container">
                                    @if($otherProductPhoto3)
                                        <div class="img">
                                            <img src="{{ $otherProductPhoto3->temporaryUrl() }}"/>

                                        </div>
                                    @endif
                                    <input type="file" class="form-control banner-input"
                                           wire:model.defer="otherProductPhoto3"/>
                                    <div class="input-handel @if($otherProductPhoto3) back-ground-transparent @endif">
                                        <div class="items">
                                            <svg width="28" height="20" viewBox="0 0 28 20" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22.575 7.71268C21.7817 3.68768 18.2467 0.666016 14 0.666016C10.6283 0.666016 7.7 2.57935 6.24167 5.37935C2.73 5.75268 0 8.72768 0 12.3327C0 16.1943 3.13833 19.3327 7 19.3327H22.1667C25.3867 19.3327 28 16.7193 28 13.4993C28 10.4193 25.6083 7.92268 22.575 7.71268ZM16.3333 11.166V15.8327H11.6667V11.166H8.16667L13.5917 5.74102C13.825 5.50768 14.1867 5.50768 14.42 5.74102L19.8333 11.166H16.3333Z"
                                                    fill="#FE2E17"/>
                                            </svg>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    @error('otherProductPhoto1')
                    <div class="alert alert-danger2 w-50">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                    @error('otherProductPhoto2')
                    <div class="alert alert-danger2 w-50">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                    @error('otherProductPhoto3')
                    <div class="alert alert-danger2 w-50">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
            <div class="food-info">
                <div class="colum-1">
                    <div class="user-input">
                        <label>
                            {{__('RestaurantControlPanel.ProductWeight')}}
                            <small>
                                {{__('RestaurantControlPanel.gram')}}
                            </small>
                        </label>
                        <input type="number" step="0.01" class="form-control " wire:model.defer="productWeight"/>
                        @error('productWeight')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="food-info flex-column">
                        <div wire:loading wire:target="removeSize">
                            <x-control-panel.loading/>
                        </div>
                        <div wire:loading wire:target="setSelectedSize">
                            <x-control-panel.loading/>
                        </div>
                        <div wire:loading wire:target="addAdditionalSize">
                            <x-control-panel.loading/>
                        </div>




                        <div class="add-size-section"
                             x-data="{ isSize: @entangle('isSize')}">
                            @include('livewire.modal.template.controlPanel.addFood.sizes.addNewSize')
                            @if(count($newSizeStored)>0)
                                @foreach($newSizeStored as $new=>$size)
                                    {{--                            @if($new!=1)--}}
                                    <div class="new-size">
                                        <div class="name">
                                            <svg width="12" height="8" viewBox="0 0 12 8" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M3.6 1.33333C2.25137 1.33333 1.2 2.50152 1.2 4C1.2 5.49848 2.25137 6.66667 3.6 6.66667H4.8C5.13137 6.66667 5.4 6.96514 5.4 7.33333C5.4 7.70152 5.13137 8 4.8 8H3.6C1.58863 8 0 6.23486 0 4C0 1.76514 1.58863 0 3.6 0H4.8C5.13137 0 5.4 0.298477 5.4 0.666667C5.4 1.03486 5.13137 1.33333 4.8 1.33333H3.6ZM6.6 0.666667C6.6 0.298477 6.86863 0 7.2 0H8.4C10.4114 0 12 1.76514 12 4C12 6.23486 10.4114 8 8.4 8H7.2C6.86863 8 6.6 7.70152 6.6 7.33333C6.6 6.96514 6.86863 6.66667 7.2 6.66667H8.4C9.74863 6.66667 10.8 5.49848 10.8 4C10.8 2.50152 9.74863 1.33333 8.4 1.33333H7.2C6.86863 1.33333 6.6 1.03486 6.6 0.666667ZM3 4C3 3.63181 3.26863 3.33333 3.6 3.33333H8.4C8.73137 3.33333 9 3.63181 9 4C9 4.36819 8.73137 4.66667 8.4 4.66667H3.6C3.26863 4.66667 3 4.36819 3 4Z"
                                                    fill="#2CC990"/>
                                            </svg>

                                            {{$selectedSizeName[$new]}}
                                        </div>
                                        <div class="price">
                                            {{$price[$new]}}&nbsp;
                                            {{$currencyCode}}
                                        </div>

                                        @if(isset($sizeWeight[$new]) and $sizeWeight[$new]!="" and $sizeWeight[$new]!=null)
                                            <div class="price">
                                                {{$sizeWeight[$new]}}
                                                <small>
                                                    {{__('RestaurantControlPanel.gram')}}
                                                </small>
                                            </div>
                                        @endif

                                        <div class="btn-container">
                                            <a href="#" class="btn btn-close"
                                               wire:click.prevent="removeSize('{{$new}}')">
                                                <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                                        fill="#FC6042"/>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    {{--                            @endif--}}
                                @endforeach
                            @endif
                            <div class="sizes">
                                <?php $i = 1; ?>
                                <div class="d-flex justify-content-between width-full align-items-end">
                                    <div class="user-input">
                                        <label>
                                            {{__('RestaurantControlPanel.ProductSizeDefault')}}
                                            @if(isset($selectedSizeName[$i]))
                                                <span class="name">
                                                        {{$selectedSizeName[$i]}}
                                                    </span>
                                                <button type="button" class="btn btn-close"
                                                        wire:click.prevent="removeSize(1)">
                                                    <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                                            fill="#FC6042"/>
                                                    </svg>
                                                </button>
                                            @endif
                                        </label>
                                        <div class="dropdown ">
                                            <button type="button" class="btn dropdown-toggle"
                                                    data-toggle="dropdown">
                                                @if(isset($selectedSizeName[$i]))
                                                    {{$selectedSizeName[$i]}}
                                                @else
                                                    {{__('RestaurantControlPanel.SelectSize')}}
                                                @endif
                                            </button>

                                            <div class="dropdown-menu">
                                                @if(!isset($selectedSize[$i]))
                                                    @foreach($sizes as $key=> $size)
                                                        <a href="#"
                                                           wire:click.prevent="setSelectedSize({{json_encode($size)}},{{$i}})">
                                                            <p class="dropdown-item">
                                                                <svg width="24" height="24" viewBox="0 0 24 24"
                                                                     fill="none"
                                                                     xmlns="http://www.w3.org/2000/svg">
                                                                    <path
                                                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                        stroke="#209F84" stroke-width="2"
                                                                        stroke-linecap="round"
                                                                        stroke-linejoin="round"/>
                                                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                          stroke-width="2"
                                                                          stroke-linecap="round"
                                                                          stroke-linejoin="round"/>
                                                                </svg>
                                                                {{$size['name']}}
                                                            </p>
                                                        </a>
                                                    @endforeach
                                                @else
                                                    <p class="dropdown-item">
                                                        <small>
                                                            {{__('RestaurantControlPanel.SelectSizeNotAvailable')}}
                                                        </small>
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        @error('selectedSize')
                                        <div class="alert alert-danger2">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>


                                    <div class="btn-container">
                                        <a href="#" class="btn btn-edit-delivaz @if(count($sizes)==0) disabled @endif"
                                           wire:click.prevent="addAdditionalSize()">
                                            {{__('RestaurantControlPanel.AddNewSize')}}
                                        </a>
                                    </div>
                                </div>
                                @if($i!=1 and $addNewSize!=false)
                                    <div class="btn-container">
                                        <button class="btn btn-save-delivaz"
                                                wire:click.prevent="saveSize({{$i}})">
                                            {{__('RestaurantControlPanel.SaveChanges')}}
                                        </button>
                                        <button class="btn btn-cancle2-delivaz"
                                                wire:click.prevent="cancelSize({{$i}})">
                                            {{__('RestaurantControlPanel.Cancel')}}
                                        </button>
                                    </div>
                                @endif
                            </div>

                        </div>


                    </div>

                </div>
                <div class="colum-2">

                    <div class="user-input-2">

                        <label>
                            {{__('RestaurantControlPanel.ProductPrice')}}
                            <small>
                                {{__('RestaurantControlPanel.includedTaxes')}}
                            </small>
                        </label>
                        <input class="form-control " type="number" step="0.01"
                               wire:model.defer="price.{{$i}}"/>
                        @error('price')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                        @error('price.'.$i)
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div wire:loading wire:target="addAddition">
                        <x-control-panel.loading/>
                    </div>
                    <div class="user-input-2 additions "
                         x-data="{ openAdditionToast: @entangle('openAdditionToast')}">
                        @include('livewire.modal.template.controlPanel.addition.addition')
                        <div class="">
                            <div class="header-2">
                                <button type="button" class="btn btn-edit-delivaz" wire:click.prevent="addAddition()">
                                    {{__('RestaurantControlPanel.AddAddition')}}
                                </button>
                            </div>

                        </div>

                    </div>
                    @if(count($storedAddition)>0)
                        <?php
                        if($price[1]==null or $price[1]==""){$price[1]=0;}
                        ?>
                        @foreach($storedAddition as $add)

                            <div class="d-flex flex-column additions-stored width-full">
                                <div class="d-flex justify-content-start width-full">
                                    <label>{{__('RestaurantControlPanel.ProductAddition')}}</label>

                                </div>
                                <div class="addition">
                                    <div class="img">
                                        <img src="{{ $additionPhoto[$add]->temporaryUrl() }}"/>
                                    </div>
                                    <div class="name">
                                        {{$additionName[$add]}}
                                    </div>
                                    <div class="price">
                                        {{$additionPrice[$add]}}&nbsp;{{$currencyCode}}
                                        {{__('RestaurantControlPanel.AndFullPrice')}}
                                        {{$price[1]}}&nbsp;{{$currencyCode." = "}}
                                        {{$price[1]+$additionPrice[$add]}}&nbsp;{{$currencyCode}}
                                    </div>
                                    <div class="btn-container">
                                        <button type="button" class="btn btn-close"
                                                wire:click.prevent="removeAddition({{$add}})">
                                            <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                                    fill="#FC6042"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    @endif
                </div>
            </div>


            <div class="food-info flex-column mt-4 mb-4 ">
                <label>
                    {{__('RestaurantControlPanel.SelectOneFilter')}}
                </label>
                @error('allFoodFiltersSelected')
                <div class="alert alert-danger2">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <div class=" d-flex flex-row flex-wrap  justify-content-start width-full">
                    @foreach($allProductFilters as $shopFilter)
                        @foreach($shopFilter as $productFilter)

                            <div class="filter-state">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" value="{{$productFilter['product_filter_id']}}"
                                           wire:model.lazy="allProductFiltersSelected"
                                           id="customSwitch{{$productFilter['product_filter_id']}}">
                                    <label class="custom-control-label"
                                           for="customSwitch{{$productFilter['product_filter_id']}}">
                                        {{$productFilter['name']}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @endforeach


                </div>

            </div>

            <div class="food-info">
                <div class="colum-1">
                    <div class="user-input">
                        <label>
                            {{__('RestaurantControlPanel.ProductTax')}}
                            @if(count($serviceProviderTax)==0)

                                <small>
                                    {{__('RestaurantControlPanel.taxNoteShop')}}
                                </small>
                            @endif
                        </label>
                        <div class="dropdown " id="cities_list">
                            <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                    data-toggle="dropdown">
                                @if(count($selectedProductTax)>0)
                                    @if($selectedProductTax['type']=="Ratio")
                                        {{$selectedProductTax['value']}} %
                                    @else
                                        {{$selectedProductTax['value']}} {{$currencyCode}}
                                    @endif
                                @else
                                    {{__("RestaurantControlPanel.SelectTax")}}
                                @endif
                            </button>
                            <div class="dropdown-menu">

                                @foreach($taxesInCountry as $tax)
                                    <a href="#"
                                       wire:click.prevent="sendTaxValue({{json_encode($tax)}})">
                                        <div class="item-container">
                                            <div class="d-flex flex-row item align-items-center">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                    <path d="M22 4L12 14.01L9 11.01" stroke-width="2"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>

                                                <p class="dropdown-item">
                                                    {{$tax['value']}}
                                                    @if($tax['type']=="Ratio")
                                                        %
                                                    @else
                                                        {{$currencyCode}}
                                                    @endif
                                                </p>

                                            </div>

                                        </div>
                                    </a>
                                @endforeach

                            </div>
                        </div>
                        @error('selectedProductTax')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                </div>
            </div>

            <div class="d-flex flex-row justify-content-center width-full">
                <div wire:loading wire:target="addNewFood">
                    <x-control-panel.loading/>
                </div>
            </div>
            <div class="d-flex flex-row justify-content-center width-full">
                <button type="submit" class="btn btn-save-delivaz">
                    {{__('RestaurantControlPanel.SaveChanges')}}
                </button>
            </div>

        </form>
    </div>
</div>
