<div class="add-food-section">
    <div class="heading2">
        <div class="btn-container">
            <a class="btn btn-back-delivaz" href="{{route('ControlPanel.Food',['locale'=>$locale])}}">
                <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                        fill="#FE2E17"/>
                </svg>
                {{__('RestaurantControlPanel.Back')}}
            </a>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    {{__("RestaurantControlPanel.Foods")}}
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{__("RestaurantControlPanel.Update")}}&nbsp;{{$food->translation[0]->name}}
                </li>
            </ol>
        </nav>
    </div>

    <div class="basic-food-info"
         x-data="{ editBasicInfo: @entangle('editBasicInfo')}">
        <div
            x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="!editBasicInfo">
            <div class="d-flex flex-row justify-content-between food-heading">
                <div class="heading1 d-flex">
                    <div class="wide"></div>
                    <div class="thin"></div>
                    <h2 class="address2">{{__('RestaurantControlPanel.DefaultMeal')}}</h2>
                    <p class="text-sm">{{__('RestaurantControlPanel.ViewEditBasicMeal')}}</p>
                </div>

            </div>

            <div class="items mt-4">
                <div class="food-info ">
                    <div class="d-flex flex-column user-input2">
                        <div class="user-input">
                            <label>{{__('RestaurantControlPanel.foodName')}}</label>
                            <p class="text1">{{$food->translation[0]->name}}</p>
                        </div>
                        <div class="user-input ">
                            <label>{{__('RestaurantControlPanel.foodDetails')}}</label>
                            <p class="text1">{{$food->translation[0]->content}}</p>
                        </div>
                    </div>

                    <div class="d-flex flex-column user-input2 align-items-end">
                        <div class="user-input d-flex justify-content-end width-full">
                            <div class="input-file-container">
                                @if($food->photo)
                                    <div class="img">
                                        <img class="img " src="{{asset('storage/images/food/'.$food->photo)}}"/>

                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
                <div class="food-info">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.FoodWeight')}}</label>
                        <p class="text1">{{$food->weight}}
                            <small>
                                {{__('RestaurantControlPanel.gram')}}
                            </small>
                        </p>
                    </div>
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.FoodPrice')}}</label>
                        <p class="text1">
                            {{$food->price}}
                        </p>
                    </div>
                </div>

                <div class="food-info">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.FoodSizeDefault')}}</label>
                        <p class="text1">{{$food->size}}

                        </p>
                    </div>
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.foodTime')}}</label>
                        <p class="text1">
                            {{__('RestaurantControlPanel.Hours')}}:&nbsp;
                            {{$food->time_hour}}&nbsp;
                            {{__('RestaurantControlPanel.Minutes')}}:&nbsp;
                            {{$food->time_minutes}}
                        </p>

                    </div>
                </div>

                <div class="food-info">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.FoodTax')}}</label>
                        @if(count($selectedFoodTax)!=0)
                        <p class="text1">
                            {{$selectedFoodTax['value']}}
                            @if($selectedFoodTax['type']=="Ratio")
                                %
                            @else
                                {{$currencyCode}}
                            @endif
                        </p>
                        @else
                            <p class="text1">
                                {{__('RestaurantControlPanel.NoSelectedTax')}}
                            </p>
                        @endif
                    </div>
                    <div class="user-input">

                    </div>
                </div>

                <div class="d-flex justify-content-end width-full">
                    <div class="btn-container">
                        <div wire:loading wire:target="openEditBasicInfo">
                            <x-control-panel.loading/>
                        </div>
                        <button class="btn btn-edit-delivaz"
                                wire:click.prevent="openEditBasicInfo">{{__('RestaurantControlPanel.Edit')}}</button>
                    </div>
                </div>
            </div>
        </div>

        <div
            x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="editBasicInfo"
            x-cloak>
            <div class="food-info ">
                <div class="d-flex flex-column user-input2">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.foodName')}}</label>
                        <input class="form-control " wire:model.defer="foodName"/>
                        @error('foodName')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="user-input ">
                        <label>{{__('RestaurantControlPanel.foodDetails')}}</label>
                        <textarea class="form-control" id="exampleFormControlTextareaUID"
                                  wire:model.defer="foodDetails"
                                  rows="6">
                            {{$foodDetails}}
                        </textarea>

                        @error('foodDetails')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div wire:loading wire:target="foodPhoto">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-column user-input2 align-items-end">
                    <div class="user-input d-flex justify-content-end width-full">
                        <div class="input-file-container">
                            {{--                                <img src="{{asset('/images/avatars/')}}"/>--}}
                            @if($foodPhoto)
                                <div class="img">
                                    <img src="{{ $foodPhoto->temporaryUrl() }}"/>
                                </div>
                            @elseif($oldFoodPhoto)
                                <div class="img">
                                    <img class="img " src="{{asset('storage/images/food/'.$oldFoodPhoto)}}"/>
                                </div>
                            @endif
                            <input type="file" class="form-control banner-input"
                                   wire:model.defer="foodPhoto"/>

                            <div class="input-handel  @if($foodPhoto or $oldFoodPhoto) back-ground-transparent @endif">
                                <div class="items">
                                    <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                            fill="#FE2E17"/>
                                    </svg>

                                    {{--                                        <i class="fas fa-cloud-upload-alt"></i>--}}
                                    <p class="text1">{{__('RestaurantControlPanel.DragFoodPhoto')}}
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>

                    @error('foodPhoto')
                    <div class="alert alert-danger2 w-50">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror

                </div>
            </div>

            <div class="food-info">
                <div class="user-input">
                    <label>
                        {{__('RestaurantControlPanel.FoodWeight')}}
                        <small>
                            {{__('RestaurantControlPanel.gram')}}
                        </small>
                    </label>
                    <input type="number" step="0.01" class="form-control " wire:model.defer="foodWeight"/>
                    @error('foodWeight')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
                <div class="user-input">
                    <label>{{__('RestaurantControlPanel.FoodPrice')}}</label>
                    <input class="form-control " type="number" step="0.01"
                           wire:model.defer="defaultPrice"/>
                    @error('defaultPrice')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>

            <div wire:loading wire:target="removeSize">
                <x-control-panel.loading/>
            </div>
            <div wire:loading wire:target="setSelectedSize">
                <x-control-panel.loading/>
            </div>
            <div class="food-info">
                <div class="user-input  ">
                    <label>
                        {{__('RestaurantControlPanel.FoodSizeDefault')}}
                        @if(isset($defaultSelectedSizeName) and $defaultSelectedSizeName!= null)
                            <span class="name">
                                {{$defaultSelectedSizeName}}
                            </span>
                            <button type="button" class="btn btn-close"
                                    wire:click.prevent="removeSize()">
                                <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                        fill="#FC6042"/>
                                </svg>
                            </button>
                        @endif
                    </label>
                    <div class="dropdown w-50">
                        <button type="button" class="btn dropdown-toggle"
                                data-toggle="dropdown">
                            @if(isset($defaultSelectedSizeName))
                                {{$defaultSelectedSizeName}}
                            @else
                                {{__('RestaurantControlPanel.SelectSize')}}
                            @endif
                        </button>

                        <div class="dropdown-menu">
                            @if(!isset($defaultSelectedSizeName))
                                @foreach($sizes as $key=> $size)
                                    <a href="#"
                                       wire:click.prevent="setSelectedSize({{json_encode($size)}},{{1}})">
                                        <p class="dropdown-item">
                                            <svg width="24" height="24" viewBox="0 0 24 24"
                                                 fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2"
                                                    stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                      stroke-width="2"
                                                      stroke-linecap="round"
                                                      stroke-linejoin="round"/>
                                            </svg>
                                            {{$size['name']}}
                                        </p>
                                    </a>
                                @endforeach
                            @else
                                <p class="dropdown-item">
                                    <small>
                                        {{__('RestaurantControlPanel.SelectSizeNotAvailable')}}
                                    </small>
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="user-input d-flex align-items-end">
                    <div class="d-flex flex-row width-full">
                        <div class="d-flex flex-row width-40 align-items-end">
                            <label>{{__('RestaurantControlPanel.foodTime')}}</label>
                        </div>

                        <div class="d-flex flex-column width-30">
                            <label>{{__('RestaurantControlPanel.Hours')}}</label>

                            <div class="dropdown " id="cities_list">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(isset($foodHour))
                                        {{$foodHour}}
                                    @else
                                        00
                                    @endif
                                </button>

                                <div class="dropdown-menu">
                                    @foreach($hoursInDay as $key=> $hour)
                                        <a href="#"
                                           wire:click.prevent="setFoodHour('{{$hour}}')">
                                            <p class="dropdown-item">{{$hour}}</p>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-column width-30">
                            <label>{{__('RestaurantControlPanel.Minutes')}}</label>

                            <div class="dropdown mr-2 ml-2" id="cities_list">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(isset($foodMinute))
                                        {{$foodMinute}}
                                    @else
                                        00
                                    @endif
                                </button>
                                <div class="dropdown-menu">

                                    @foreach($minutesInHour as $key=> $minute)
                                        <a href="#"
                                           wire:click.prevent="setFoodMinute('{{$minute}}')">
                                            <p class="dropdown-item">{{$minute}}</p>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="food-info">
                <div class="user-input  ">
                    <label>
                        {{__('RestaurantControlPanel.FoodTax')}}
                    </label>
                    <div class="dropdown " id="cities_list">
                        <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                data-toggle="dropdown">
                            @if(count($selectedFoodTax)>0)
                                @if($selectedFoodTax['type']=="Ratio")
                                    {{$selectedFoodTax['value']}} %
                                @else
                                    {{$selectedFoodTax['value']}} {{$currencyCode}}
                                @endif
                            @else
                                {{__("RestaurantControlPanel.SelectTax")}}
                            @endif
                        </button>
                        <div class="dropdown-menu">

                            @foreach($taxesInCountry as $tax)
                                <a href="#"
                                   wire:click.prevent="sendTaxValue({{json_encode($tax)}})">
                                    <div class="item-container">
                                        <div class="d-flex flex-row item align-items-center">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                    stroke-linejoin="round"/>
                                                <path d="M22 4L12 14.01L9 11.01" stroke-width="2"
                                                      stroke-linecap="round" stroke-linejoin="round"/>
                                            </svg>

                                            <p class="dropdown-item">
                                                {{$tax['value']}}
                                                @if($tax['type']=="Ratio")
                                                    %
                                                @else
                                                    {{$currencyCode}}
                                                @endif
                                            </p>

                                        </div>

                                    </div>
                                </a>
                            @endforeach

                        </div>
                    </div>
                    @error('selectedFoodTax')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>

            </div>


            <div class="d-flex justify-content-center">
                <div class="btn-container">
                    <div wire:loading wire:target="saveBasicInfo">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="cancelBasicInfo">
                        <x-control-panel.loading/>
                    </div>
                    <button class="btn btn-save-delivaz"
                            wire:click.prevent="saveBasicInfo()">
                        {{__('RestaurantControlPanel.SaveChanges')}}
                    </button>
                    <button class="btn btn-cancle2-delivaz "
                            wire:click.prevent="cancelBasicInfo()">
                        {{__('RestaurantControlPanel.Cancel')}}
                    </button>
                </div>
            </div>

        </div>

    </div>
    <div class="food-addition-photo "
         x-data="{ openDeleteToast: @entangle('openDeleteToast'),openNewPhoto: @entangle('openNewPhoto')}">
        @include('livewire.modal.template.controlPanel.deleteWindow')
        @include('livewire.modal.template.controlPanel.updateFood.addNewPhoto')

        <div class="d-flex flex-row justify-content-between food-heading">

            <div class="heading1 d-flex">
                <div class="wide"></div>
                <div class="thin"></div>
                <h2 class="address2">{{__('RestaurantControlPanel.AdditionalPhoto')}}</h2>
                <p class="text-sm">{{__('RestaurantControlPanel.ViewAdditionalPhoto')}}</p>
            </div>

        </div>
        <div wire:loading wire:target="removeAdditionalPhoto">
            <x-control-panel.loading/>
        </div>
        <div class="addition-photos mt-4">
            @if(count($food->photos)>0)
                <?php $i = 1;?>
                @foreach($food->photos as $photo)
                    <div class="photo">
                        <div class="img">
                            <img class="img " src="{{asset('storage/images/food/'.$photo->name)}}"/>
                        </div>
                        <div class="footer">
                            <p class="text1">
                                {{__('RestaurantControlPanel.Photo')}}&nbsp; {{$i}}
                            </p>
                            <button type="button" class="btn btn-delete-delivaz"
                                    wire:click.prevent="removeAdditionalPhoto({{$photo->id}})">
                                <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                        fill="#ffffff"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <?php $i++;?>
                @endforeach
            @else
                <p class="text2">
                    {{__('RestaurantControlPanel.NoAdditionPhoto')}}
                </p>
            @endif
        </div>
        <div class="d-flex justify-content-end width-full">
            <div class="btn-container">
                <button class="btn btn-edit-delivaz" wire:click.prevent="openAdditionPhotoToast">
                    {{__('RestaurantControlPanel.AddAdditionalPhoto')}}
                </button>
            </div>
        </div>
    </div>


    <div class="food-sizes "
         x-data="{ openDeleteToast: @entangle('openDeleteToast'),
         openNewSize: @entangle('openNewSize')}">
        @include('livewire.modal.template.controlPanel.deleteWindow')
        @include('livewire.modal.template.controlPanel.updateFood.addNewSize')

        <div class="d-flex flex-row justify-content-between food-heading">
            <div class="heading1 d-flex">
                <div class="wide"></div>
                <div class="thin"></div>
                <h2 class="address2">{{__('RestaurantControlPanel.AdditionalMealSize')}}</h2>
                <p class="text-sm">{{__('RestaurantControlPanel.ViewAdditionalMealSize')}}</p>
            </div>
        </div>
        <div class="mt-4 sizes-list">
            @if(count($food->sizes)>0)
                @foreach($food->sizes as $size)
                    <div class="new-size">
                        <div class="name">
                            <svg width="12" height="8" viewBox="0 0 12 8" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M3.6 1.33333C2.25137 1.33333 1.2 2.50152 1.2 4C1.2 5.49848 2.25137 6.66667 3.6 6.66667H4.8C5.13137 6.66667 5.4 6.96514 5.4 7.33333C5.4 7.70152 5.13137 8 4.8 8H3.6C1.58863 8 0 6.23486 0 4C0 1.76514 1.58863 0 3.6 0H4.8C5.13137 0 5.4 0.298477 5.4 0.666667C5.4 1.03486 5.13137 1.33333 4.8 1.33333H3.6ZM6.6 0.666667C6.6 0.298477 6.86863 0 7.2 0H8.4C10.4114 0 12 1.76514 12 4C12 6.23486 10.4114 8 8.4 8H7.2C6.86863 8 6.6 7.70152 6.6 7.33333C6.6 6.96514 6.86863 6.66667 7.2 6.66667H8.4C9.74863 6.66667 10.8 5.49848 10.8 4C10.8 2.50152 9.74863 1.33333 8.4 1.33333H7.2C6.86863 1.33333 6.6 1.03486 6.6 0.666667ZM3 4C3 3.63181 3.26863 3.33333 3.6 3.33333H8.4C8.73137 3.33333 9 3.63181 9 4C9 4.36819 8.73137 4.66667 8.4 4.66667H3.6C3.26863 4.66667 3 4.36819 3 4Z"
                                    fill="#2CC990"/>
                            </svg>
                            {{--                        {{dd($size)}}--}}
                            {{$size->size}}
                        </div>
                        <div class="price">

                            {{$size->price}}&nbsp;
                            &nbsp;{{$currencyCode}}
                        </div>
                        <div class="price">
                            @if($size->weight!=0)
                                {{$size->weight}}&nbsp;
                                {{__('RestaurantControlPanel.gram')}}
                            @endif

                        </div>
                        <div class="name">
                            @if($size->content!=null)
                                {{$size->content}}
                            @endif
                        </div>
                        <div class="btn-container">
                            <button type="button" class="btn btn-edit-delivaz"
                                    wire:click.prevent="updateSize({{$size->id}})">
                                <i class="fas  fa-edit"></i>
                            </button>
                            <button type="button" class="btn btn-delete-delivaz"
                                    wire:click.prevent="callDeleteSize({{$size->id}})">
                                <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                        fill="#ffffff"/>
                                </svg>
                            </button>

                        </div>
                    </div>
                @endforeach
            @else
                <p class="text2">
                    {{__('RestaurantControlPanel.NoFoodSizes')}}
                </p>
            @endif
            <div class="d-flex justify-content-end width-full mt-4">
                <div class="btn-container">
                    <button class="btn btn-edit-delivaz @if(count($sizes)==0) disabled @endif"
                            wire:click.prevent="openNewSize()">
                        {{__('RestaurantControlPanel.AddNewSize')}}
                    </button>
                </div>
            </div>
        </div>

    </div>


    <div class="food-additions "
         x-data="{ openDeleteToast: @entangle('openDeleteToast'),
         openNewAddition: @entangle('openNewAddition')}">
        @include('livewire.modal.template.controlPanel.deleteWindow')
        @include('livewire.modal.template.controlPanel.updateFood.addNewAddition')
        <div class="d-flex flex-row justify-content-between food-heading">

            <div class="heading1 d-flex">
                <div class="wide"></div>
                <div class="thin"></div>
                <h2 class="address2">{{__('RestaurantControlPanel.MealAddition')}}</h2>
                <p class="text-sm">{{__('RestaurantControlPanel.ViewMealAddition')}}</p>
            </div>

        </div>

        <div class="d-flex flex-column food-additions-container w-100 mt-4">
            @if(count($food->additions )>0)
                <div class="table-responsive-xl ">
                    <table id="myTable" class="table">
                        <tbody>
                        <tr class="addition">
                            <td class="img align-middle">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.Photo")}}
                                </p>
                            </td>
                            <td class="width-14 align-middle">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.AdditionName")}}
                                </p>
                            </td>

                            <td class="width-14 align-middle ">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.AdditionDetails")}}
                                </p>
                            </td>

                            <td class="width-14 align-middle">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.AdditionWeight")}}
                                </p>
                            </td>

                            <td class="width-14 align-middle">
                                <p class="text3">
                                    {{__('RestaurantControlPanel.Price')}}
                                </p>
                            </td>

                            <td class="width-14 align-middle">
                                <p class="text3">
                                    {{__('RestaurantControlPanel.TotalPrice')}}
                                </p>
                            </td>

                            <td class="width-14 align-middle ">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.Action")}}
                                </p>
                            </td>

                        </tr>

                        @foreach($food->additions as $addition)
                            <tr class="addition">
                                <td class="img align-middle width-14 ">
                                    <img class="img "
                                         src="{{asset('storage/images/food/addition/'.$addition->photo)}}"/>
                                </td>
                                <td class="align-middle width-14">
                                    {{$addition->name}}

                                </td>
                                <td class="align-middle width-14">
                                    {{$addition->content}}
                                </td>
                                <td class="align-middle width-14">
                                    {{$addition->weight}}
                                    <small>
                                        {{__('RestaurantControlPanel.gram')}}
                                    </small>
                                </td>
                                <td class="align-middle width-14">
                                    {{$addition->price}}&nbsp;{{$currencyCode}}
                                </td>
                                <td class="align-middle width-14">
                                    {{$addition->price+$food->price}}&nbsp;{{$currencyCode}}
                                </td>
                                <td class=" align-middle width-14">
                                    <div class="btn-container">
                                        <button type="button" class="btn btn-edit-delivaz"
                                                wire:click.prevent="updateAddition({{$addition->id}})">
                                            <i class="fas  fa-edit"></i>
                                        </button>
                                        <button type="button" class="btn btn-delete-delivaz"
                                                wire:click.prevent="callDeleteAddition({{$addition->id}})">
                                            <svg width="9" height="8" viewBox="0 0 9 8" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M0.308058 0.646447C0.552136 0.451184 0.947864 0.451184 1.19194 0.646447L4.5 3.29289L7.80806 0.646447C8.05214 0.451184 8.44786 0.451184 8.69194 0.646447C8.93602 0.841709 8.93602 1.15829 8.69194 1.35355L5.38388 4L8.69194 6.64645C8.93602 6.84171 8.93602 7.15829 8.69194 7.35355C8.44786 7.54882 8.05214 7.54882 7.80806 7.35355L4.5 4.70711L1.19194 7.35355C0.947864 7.54882 0.552136 7.54882 0.308058 7.35355C0.0639806 7.15829 0.0639806 6.84171 0.308058 6.64645L3.61612 4L0.308058 1.35355C0.0639806 1.15829 0.0639806 0.841709 0.308058 0.646447Z"
                                                    fill="#ffffff"/>
                                            </svg>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <p class="text2">
                    {{__('RestaurantControlPanel.NoFoodAdditions')}}
                </p>
            @endif

            <div class="d-flex justify-content-end w-100">
                <div class="btn-container">
                    <button class="btn btn-edit-delivaz" wire:click.prevent="openAdditionToast">
                        {{__('RestaurantControlPanel.AddAddition')}}
                    </button>
                </div>
            </div>
        </div>

    </div>


    <div class="food-filters "
         x-data="{ openEditFilters: @entangle('openEditFilters')}">
        <div class="d-flex flex-row justify-content-between food-heading">

            <div class="heading1 d-flex">
                <div class="wide"></div>
                <div class="thin"></div>
                <h2 class="address2">{{__('RestaurantControlPanel.MealFilters')}}</h2>
                <p class="text-sm">{{__('RestaurantControlPanel.ViewMealFilters')}}</p>
            </div>

        </div>


        <div class="filters-list mt-4"
             x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="!openEditFilters"
             >
            @foreach($food->filters as $mailFilter)
                <p class="text1 mb-2">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                            stroke="#209F84" stroke-width="2" stroke-linecap="round"
                            stroke-linejoin="round"/>
                        <path d="M22 4L12 14.01L9 11.01" stroke-width="2" stroke-linecap="round" stroke="#209F84"
                              stroke-linejoin="round"/>
                    </svg>
                    &nbsp;
                    {{$mailFilter->translation->where('code_lang',$locale)->first()->name}}
                </p>
            @endforeach
        </div>

        <div
            x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openEditFilters"
            x-cloak>
            <div class="food-info flex-column mt-4 mb-4 ">
                <label>
                    {{__('RestaurantControlPanel.SelectOneFilter')}}
                </label>
                @error('allFoodFiltersSelected')
                <div class="alert alert-danger2">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <div class=" d-flex flex-row flex-wrap  justify-content-start width-full">
                    @foreach($allFoodFilters as $restaurantFilter)
                        @foreach($restaurantFilter as $foodFilter)

                            <div class="filter-state">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" value="{{$foodFilter['meal_filter_id']}}"
                                           wire:model.lazy="allFoodFiltersSelected"
                                           id="customSwitch{{$foodFilter['meal_filter_id']}}">
                                    <label class="custom-control-label"
                                           for="customSwitch{{$foodFilter['meal_filter_id']}}">
                                        {{$foodFilter['name']}}
                                    </label>
                                </div>
                            </div>
                        @endforeach
                    @endforeach


                </div>

            </div>
            <div class="d-flex justify-content-center w-100">
                <div class="btn-container ">
                    <button class="btn btn-save-delivaz"
                            wire:click.prevent="saveNewFilters()">
                        {{__('RestaurantControlPanel.SaveChanges')}}
                    </button>
                    <button class="btn btn-cancle2-delivaz"
                            wire:click.prevent="cancelNewFilters()">
                        {{__('RestaurantControlPanel.Cancel')}}
                    </button>
                </div>
            </div>
        </div>

        <div class="d-flex flex-row justify-content-end w-100">
            <div class="btn-container">
                <button class="btn btn-edit-delivaz @if($openEditFilters) disabled @endif" wire:click.prevent="editFilters">
                    {{__('RestaurantControlPanel.Edit')}}
                </button>
            </div>
        </div>
        <div wire:loading wire:target="allFoodFiltersSelected">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="editFilters">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="cancelNewFilters">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="saveNewFilters">
            <x-control-panel.loading/>
        </div>
    </div>


</div>
