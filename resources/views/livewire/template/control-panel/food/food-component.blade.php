<div class="food-section">
    <div class="heading1 d-flex">
        <div class="wide"></div>
        <div class="thin"></div>
        <h2 class="address2">{{__('RestaurantControlPanel.Foods')}} </h2>
    </div>
    <div class="d-flex flex-row justify-content-between">
        <div class="food-list">
            <div class="item">
                <div class="icon">
                    <svg width="21" height="18" viewBox="0 0 21 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M8.33203 4.66602H5.66536V9.99935H0.332031V12.666H5.66536V17.9993H8.33203V12.666H13.6654V9.99935H8.33203V4.66602ZM14.332 2.10602V4.53268L17.6654 3.86602V17.9993H20.332V0.666016L14.332 2.10602Z"
                            fill="black"/>
                    </svg>
                </div>
                <div class="text">
                    <p>
                        {{__("RestaurantControlPanel.AddFood")}}

                    </p>
                </div>
                <div class="btn-container">
                    <a class="btn btn-edit-delivaz"
                       href="{{route('AddNewFood',['locale'=>$locale])}}">
                        {{__('RestaurantControlPanel.Add')}}
                    </a>
                </div>

            </div>
            <div class="item">
                <div class="icon">
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M8.33203 21.6673H13.6654V0.333984H8.33203V21.6673ZM0.332031 21.6673H5.66536V11.0007H0.332031V21.6673ZM16.332 7.00065V21.6673H21.6654V7.00065H16.332Z"
                            fill="black"/>
                    </svg>

                </div>
                <div class="text">
                    <p>
                        {{__("RestaurantControlPanel.MySizes")}}
                    </p>
                </div>
                <div class="btn-container">
                    <a class="btn btn-edit-delivaz" href="{{route('Sizes',['locale'=>$locale])}}">
                        {{__('RestaurantControlPanel.Edit')}}
                    </a>
                </div>

            </div>
            <div class="item">
                <div class="icon">
                    <i class="fas fa-list"></i>
                </div>
                <div class="text">
                    <p>
                        {{__("RestaurantControlPanel.MyMenu")}}
                    </p>
                </div>


                <div class="btn-container">
                    <a class="btn btn-edit-delivaz" href="{{route('EditMenu',['locale'=>$locale])}}">
                        {{__('RestaurantControlPanel.Edit')}}
                    </a>
                </div>

            </div>
        </div>

    </div>
    <div class="food"
         x-data="{isFilterList: @entangle('isFilterList'),openDeleteToast: @entangle('openDeleteToast')}">

        <div class="d-flex justify-content-end">
            <div class="my-dropdown-container " id="cities_list">
                <button id="dropdown_selecte_btn" type="button" class="btn btn-drop-down "
                        @click.prevent="$wire.openFilterList()">
                    @if($MealFilterName)
                        {{$MealFilterName}}
                    @else
                        {{__("RestaurantControlPanel.SelectFilter")}}
                    @endif

                </button>
                <div class="my-dropdown-menu"
                     x-show.transition.in.duration.50ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="isFilterList"
                     @click.away="$wire.closeFilterList()"
                     x-cloak>

                    <div class="item-container">
                        <div class="d-flex flex-row item align-items-center">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                    stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round"/>
                                <path d="M22 4L12 14.01L9 11.01" stroke-width="2" stroke-linecap="round"
                                      stroke-linejoin="round"/>
                            </svg>
                            <a href="#"
                               class="width-full"
                               wire:click.prevent="getFoodForThisFilter(0)">
                                <p class="dropdown-item"> {{__("restaurant.allMealsFilters")}}</p>
                            </a>
                        </div>
                    </div>
                    @foreach($allFoodFilters as $restaurantFilter)
                        @foreach($restaurantFilter as $mailFilter)
                            <div class="item-container">
                                <div class="d-flex flex-row item align-items-center">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                            stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                            stroke-linejoin="round"/>
                                        <path d="M22 4L12 14.01L9 11.01" stroke-width="2" stroke-linecap="round"
                                              stroke-linejoin="round"/>
                                    </svg>
                                    <a href="#"
                                       class="width-full"
                                       wire:click.prevent="getFoodForThisFilter({{$mailFilter['id']}})">
                                        <p class="dropdown-item">{{$mailFilter['name']}}</p>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
            </div>
        </div>
        <div wire:loading wire:target="getFoodForThisFilter">
            <x-control-panel.loading/>
        </div>
        @if(count($foods)>0)
            <div class="table-responsive-xl">
                @include('livewire.modal.template.controlPanel.deleteWindow')
                <table id="myTable" class="table">
                    <div class="d-flex flex-row table-header">
                        <div class="width-20">
                            <p class="text3">
                                {{__("RestaurantControlPanel.FoodName")}}
                            </p>
                        </div>
                        <div class="width-20">
                            <p class="text3">
                                {{__("RestaurantControlPanel.Photo")}}
                            </p>
                        </div>

                        <div class="width-20 d-flex">
                            <p class="text3">
                                {{__("RestaurantControlPanel.Details")}}
                            </p>
                        </div>
                        <div class="width-20">
                            <p class="text3">
                                {{__("RestaurantControlPanel.FoodPrice")}}
                            </p>
                        </div>
                        <div class="width-20 d-flex justify-content-center">
                            <p class="text3">
                                {{__("RestaurantControlPanel.Action")}}
                            </p>
                        </div>
                    </div>
                    <tbody>


                    @foreach($foods as $food)
                        <tr>

                            <td class="width-20 align-middle">
                                <div class="banner-table ">
                                    <p class="text1">
                                        {{$food->translation[0]->name}}
                                    </p>
                                </div>
                            </td>
                            <td class="width-20 align-middle">
                                @if($food->photo)
                                    <div class="img">
                                        <img class="img " src="{{asset('storage/images/food/'.$food->photo)}}"/>

                                    </div>
                                @else
                                    {{__("RestaurantControlPanel.empty")}}
                                @endif
                            </td>
                            <td class="width-20 align-middle">
                                <div class="banner-table ">
                                    <p class="text1">
                                        {{ \Illuminate\Support\Str::of($food->translation[0]->content)->words(3) }}
                                    </p>
                                </div>
                            </td>
                            <td class="width-20 align-middle">
                                <p class="text1">
                                    {{$food->price}} {{$currencyCode}}
                                </p>
                            </td>
                            <td class="width-20 align-middle">
                                <div class="d-flex flex-row justify-content-end">
                                    <a href="{{route('UpdateFood',['locale'=>$locale,'id'=>$food->id])}}"
                                       class="d-flex align-items-center btn btn-edit-delivaz mr-2 ml-2">
                                        {{__("RestaurantControlPanel.Edit")}}
                                    </a>
                                    <button type="button" class="btn btn-delete-delivaz mr-2 ml-2"
                                            wire:click.prevent="deleteCall({{$food->id}})">
                                        {{__("RestaurantControlPanel.Delete")}}
                                    </button>
                                </div>
                            <td>

                        </tr>

                    @endforeach

                    </tbody>
                </table>

                <div class="d-flex flex-row justify-content-center">
                    {{ $foods->links() }}
                </div>

            </div>
        @else
            <div class="d-flex flex-row justify-content-start">
                <p class="text2">
                    {{__("RestaurantControlPanel.noFoodAdded")}}
                </p>
            </div>
        @endif
    </div>


</div>
