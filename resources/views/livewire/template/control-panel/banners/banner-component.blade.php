<div class="banner-section">
    <div class="banner">
        <div class="heading1 d-flex">
            <div class="wide"></div>
            <div class="thin"></div>
            <h2 class="address2">{{__('RestaurantControlPanel.MyBanners')}}</h2>
        </div>

        @if ($serviceProviderBanner)
            <div class="banner-loaded ">
                <img src=" {{ $serviceProviderBanner->temporaryUrl() }}">
            </div>
        @endif
        <form wire:submit.prevent="submit">
            @if (!$serviceProviderBanner)

                <div class="image-section">
                    <div wire:loading wire:target="serviceProviderBanner">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="RemoveCurrentServiceProviderBanner">
                        <x-control-panel.loading/>
                    </div>
                    <label>{{__('RestaurantControlPanel.AddNewBanner')}}</label>
                    <div class="input-file-container">
                        <input type="file" class="form-control banner-input"
                               wire:model.defer="serviceProviderBanner"/>
                        <div class="input-handel">
                            <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                    fill="#FE2E17"/>
                            </svg>

                            <p class="text1">{{__('RestaurantControlPanel.DragHereBanners')}}
                            </p>
                        </div>
                    </div>

                    @error('serviceProviderBanner')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            @endif

            <div class="d-flex flex-row justify-content-center width-full btn-group mt-2">

                @if ($serviceProviderBanner)
                    <div class="btn-container">
                        <button type="submit" class="btn btn-save-delivaz">
                            {{__('RestaurantControlPanel.SaveChanges')}}
                        </button>
                    </div>
                    <div class="btn-container ml-2 mr-2">
                        <button type="submit" class="btn btn-cancle2-delivaz "
                                wire:click.prevent="RemoveCurrentServiceProviderBanner">
                            {{__('RestaurantControlPanel.Cancel')}}
                        </button>
                    </div>
                @endif

            </div>
        </form>
        {{--        @include('livewire.modal.template.controlPanel.deleteModel')--}}

        <div class="card-body"
             x-data="{ openDeleteToast: @entangle('openDeleteToast'),openPreviewToast: @entangle('openPreviewToast')}">
            @include('livewire.modal.template.controlPanel.deleteWindow')
            @include('livewire.modal.template.controlPanel.previewWindow')
            @if(count($banners)!=0)

                <div class="table-responsive-xl">
                    <table id="myTable" class="table">
                        <div class="d-flex flex-row table-header">
                            <div class="width-20">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.Banner")}}
                                </p>
                            </div>
                            <div class="width-50">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.BannerState")}}
                                </p>
                            </div>
                            <div class="width-30 d-flex justify-content-center">
                                <p class="text3">
                                    {{__("RestaurantControlPanel.Action")}}
                                </p>
                            </div>
                        </div>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($banners as $banner)
                            <tr>

                                <td class="width-20 align-middle">
                                    <div class="banner-table ">
                                        <p class="text1">
                                            {{__("RestaurantControlPanel.Banner")}} {{$i}}
                                        </p>
                                        {{--                                    <img src=" {{asset($imageFolders.$banner->name)}}"/>--}}
                                    </div>
                                </td>
                                <td class="width-50 align-middle">
                                    @if($banner->state==1)
                                        <p class="text1">
                                            {{__("RestaurantControlPanel.BannerIsActive")}}
                                        </p>
                                    @else
                                        <p class="text1">
                                            {{__("RestaurantControlPanel.BannerIsNotActive")}}
                                        </p>
                                    @endif
                                </td>
                                <td class="width-30 align-middle">

                                    <div class="d-flex flex-row justify-content-end">

                                        <button type="button" class="btn btn-edit-delivaz"
                                                wire:click.prevent="openPreviewToast('{{$i}}','{{$banner->name}}')">
                                            {{__("RestaurantControlPanel.Preview")}}
                                        </button>
                                        <button type="button" class="btn btn-delete-delivaz"
                                                wire:click.prevent="deleteCall({{$banner->id}})">
                                            {{__("RestaurantControlPanel.Delete")}}
                                        </button>
                                    </div>
                                <td>

                            </tr>

                            <?php $i++; ?>
                        @endforeach

                        </tbody>
                    </table>

                    <div class="d-flex flex-row justify-content-center">
                        {{ $banners->links() }}
                    </div>
                    @else
                        <div class="d-flex flex-row justify-content-start">
                            <p class="text2">
                                {{__("RestaurantControlPanel.noBanners")}}
                            </p>
                        </div>
                    @endif
                </div>
        </div>


    </div>
</div>
