<div class="order-section">
    <livewire:template.control-panel.order.order-info-component :user="$user"/>

    <div class="order-list">
        <div class="heading1 d-flex">
            <div class="wide"></div>
            <div class="thin"></div>
            <h2 class="address2">{{__('RestaurantControlPanel.RecentOrderRequest')}}</h2>
{{--            @if($serviceProvider->type=="res")--}}
                <p class="text-sm">{{__('RestaurantControlPanel.Lorem')}}</p>
{{--            @endif--}}
{{--            @if($serviceProvider->type=="ven")--}}
{{--                <p class="text-sm">{{__('RestaurantControlPanel.SetTimesForYourWorkShop')}}</p>--}}
{{--            @endif--}}
        </div>

{{--        <div class="table-responsive-xl orders">--}}
{{--            @include('livewire.modal.template.controlPanel.deleteWindow')--}}
{{--            <table id="myTable" class="table">--}}

{{--                <tbody>--}}
              <div class="orders">
                  <div class="w-100 d-flex order-row">
                      <div class="w-10">
                          <div class="img">
                              <img  src="{{asset('storage/images/food/16254939846.jpg')}}"/>
                          </div>
                      </div>
                      <div class="width-30 two-item">
                          <div class="item1">
                              <p class="text3">
                                  Tuna soup spinach with himalaya salt
                              </p>
                          </div>
                          <div class="item2">
                              <p class="text3">
                                  #0010235
                              </p>
                          </div>
                      </div>

                      <div class="w-20 two-item">
                          <div class="item1">
                              <p class="text3">
                                  Jimmy Kueai
                              </p>
                          </div>

                          <div class="item2">
                              South Corner St.
                              41256 London
                          </div>
                      </div>
                      <div class="w-10 one-item">
                          <p class="text3">
                              $7.4
                          </p>
                      </div>
                      <div class="width-5 one-item">
                          <p class="text3">
                              x3
                          </p>
                      </div>
                      <div class="width-20 one-item">
                          <p class="text3">
                              PENDING
                          </p>
                      </div>
                      <div class="width-5 one-item">
                          <svg width="32" height="24" viewBox="0 0 32 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M15.9282 13C16.6587 13 17.2509 12.5523 17.2509 12C17.2509 11.4477 16.6587 11 15.9282 11C15.1977 11 14.6055 11.4477 14.6055 12C14.6055 12.5523 15.1977 13 15.9282 13Z" stroke="#2E2E2E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                              <path d="M15.9282 6C16.6587 6 17.2509 5.55228 17.2509 5C17.2509 4.44772 16.6587 4 15.9282 4C15.1977 4 14.6055 4.44772 14.6055 5C14.6055 5.55228 15.1977 6 15.9282 6Z" stroke="#2E2E2E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                              <path d="M15.9282 20C16.6587 20 17.2509 19.5523 17.2509 19C17.2509 18.4477 16.6587 18 15.9282 18C15.1977 18 14.6055 18.4477 14.6055 19C14.6055 19.5523 15.1977 20 15.9282 20Z" stroke="#2E2E2E" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg>
                      </div>
                  </div>
              </div>


            <div class="d-flex flex-row justify-content-center">
{{--                {{ $foods->links() }}--}}
            </div>

        </div>


    </div>

</div>
