<div class="work-time-section">

    <div class="work-time">
        <div class="work-time-header">
            <div class="heading1 d-flex">
                <div class="wide"></div>
                <div class="thin"></div>
                <h2 class="address2">{{__('RestaurantControlPanel.WorkTimes')}}</h2>
                @if($serviceProvider->type=="res")
                    <p class="text-sm">{{__('RestaurantControlPanel.SetTimesForYourWorkRestaurant')}}</p>
                @endif
                @if($serviceProvider->type=="ven")
                    <p class="text-sm">{{__('RestaurantControlPanel.SetTimesForYourWorkShop')}}</p>
                @endif
            </div>

            <div class="state">

                <div class="custom-control custom-switch">
                    <input type="checkbox" wire:change="changeServiceProviderState" class="" id="customSwitch1"
                           @if($serviceProvider->state==1) checked @endif>
                    <label class="custom-control-label" for="customSwitch1">
                        @if($serviceProvider->type=="res")
                            @if($serviceProvider->state==1)
                                {{__('RestaurantControlPanel.RestaurantOn')}}
                            @else
                                {{__('RestaurantControlPanel.RestaurantOff')}}
                            @endif
                        @endif
                        @if($serviceProvider->type=="ven")

                            @if($serviceProvider->state==1)
                                {{__('RestaurantControlPanel.ShopOn')}}
                            @else
                                {{__('RestaurantControlPanel.ShopOff')}}
                            @endif
                        @endif

                    </label>
                </div>
            </div>
        </div>


        <div wire:loading wire:target="changeServiceProviderState">
            <x-control-panel.loading/>
        </div>

        <div wire:loading wire:target="saveDayWork">
            <x-control-panel.loading/>
        </div>
        <div class="table-responsive-xl">
            <table class="table">
                <thead>
                <tr>
                    <th>
                    {{--                            <h4 class="table-address">{{__("RestaurantControlPanel.Day")}}</h4></th>--}}
                    <td>

                    </td>
                    <th>
                        <h4 class="table-address">{{__("RestaurantControlPanel.StartTime")}}</h4>
                        <div class="">
                            <span class="hour">{{__("RestaurantControlPanel.Hours")}}</span>
                            <span class="minutes">{{__("RestaurantControlPanel.Minutes")}}</span>
                        </div>
                        {{--                        <small>{{__("RestaurantControlPanel.format")}}</small>--}}
                    </th>
                    {{--                        <th><h4 class="table-address">{{__("RestaurantControlPanel.To")}}</h4></th>--}}
                    <th>
                        <h4 class="table-address">{{__("RestaurantControlPanel.EndTime")}}</h4>
                        <div class="">
                            <span class="hour">{{__("RestaurantControlPanel.Hours")}}</span>
                            <span class="minutes">{{__("RestaurantControlPanel.Minutes")}}</span>
                        </div>
                        {{--                        <small>{{__("RestaurantControlPanel.format")}}</small>--}}
                    </th>
                    <th>
                        {{--                            <h4 class="table-address">{{__("RestaurantControlPanel.Action")}}</h4>--}}
                    </th>

                </tr>
                </thead>
                <tbody>
                @foreach($days as $keyForDay=>$day)
                    <?php $keyForDay = $keyForDay + 1;?>
                    <tr>
                        <form wire:submit.prevent="saveDayWork({{$keyForDay}})">
                            <td class="table-address">

                                {{$day}}
                            </td>
                            <td>

                                <div class="d-flex flex-row justify-content-center align-items-center width-full">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="" wire:change="setDayState({{$keyForDay}})"
                                               id="customSwitch{{$day}}"
                                               @if($workTimeDays[$keyForDay]=="on") checked @endif>
                                        <label class="custom-control-label" for="customSwitch{{$day}}">

                                        </label>
                                    </div>
                                </div>
                            </td>

                            <td>
                                {{--                                @if($key==0)--}}
                                {{--                                    Hour--}}
                                {{--                                @endif--}}
                                <div class="d-flex flex-row">
                                    <div class="dropdown " id="cities_list">
                                        <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                                data-toggle="dropdown">
                                            @if(isset($startHour[$keyForDay]))
                                                {{$startHour[$keyForDay]}}
                                            @else
                                                00
                                            @endif
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="#"
                                               wire:click.prevent="setStartHour('{{$keyForDay}}', 'off')">
                                                <p class="dropdown-item">{{__("RestaurantControlPanel.Off")}}</p>
                                            </a>

                                            @foreach($hoursInDay as $key=> $hour)
                                                <a href="#"
                                                   wire:click.prevent="setStartHour('{{$keyForDay}}', '{{$hour}}')">
                                                    <p class="dropdown-item">{{$hour}}</p>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <span class="mt-2">   &nbsp;&nbsp;</span>
                                    <div class="dropdown " id="cities_list">
                                        <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                                data-toggle="dropdown">
                                            @if(isset($startMinute[$keyForDay]))
                                                {{$startMinute[$keyForDay]}}
                                            @else
                                                00
                                            @endif
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="#"
                                               wire:click.prevent="setStartMinute('{{$keyForDay}}', 'off')">
                                                <p class="dropdown-item">{{__("RestaurantControlPanel.Off")}}</p>
                                            </a>
                                            @foreach($minutesInHour as $key=> $minute)
                                                <a href="#"
                                                   wire:click.prevent="setStartMinute('{{$keyForDay}}', '{{$minute}}')">
                                                    <p class="dropdown-item">{{$minute}}</p>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </td>

                            <td>
                                <div class="d-flex flex-row">
                                    <div class="dropdown " id="cities_list">
                                        <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                                data-toggle="dropdown">
                                            @if(isset($endHour[$keyForDay]))
                                                {{$endHour[$keyForDay]}}
                                            @else
                                                00
                                            @endif
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="#"
                                               wire:click.prevent="setEndHour('{{$keyForDay}}', 'off')">
                                                <p class="dropdown-item">{{__("RestaurantControlPanel.Off")}}</p>
                                            </a>
                                            @foreach($hoursInDay as $key=> $hour)
                                                <a href="#"
                                                   wire:click.prevent="setEndHour('{{$keyForDay}}', '{{$hour}}')">
                                                    <p class="dropdown-item">{{$hour}}</p>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                    <span class="mt-2">   &nbsp;&nbsp;</span>
                                    <div class="dropdown " id="cities_list">
                                        <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                                data-toggle="dropdown">
                                            @if(isset($endMinute[$keyForDay]))
                                                {{$endMinute[$keyForDay]}}
                                            @else
                                                00
                                            @endif
                                        </button>
                                        <div class="dropdown-menu">
                                            <a href="#"
                                               wire:click.prevent="setEndMinute('{{$keyForDay}}', 'off')">
                                                <p class="dropdown-item">{{__("RestaurantControlPanel.Off")}}</p>
                                            </a>
                                            @foreach($minutesInHour as $key=> $minute)
                                                <a href="#"
                                                   wire:click.prevent="setEndMinute('{{$keyForDay}}', '{{$minute}}')">
                                                    <p class="dropdown-item">{{$minute}}</p>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="">
                                    <button type="submit" class="btn btn-save ">
                                        <i class="fa fa-save " aria-hidden="true"></i>
                                    </button>
                                </div>
                            </td>
                        </form>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


    </div>
</div>
