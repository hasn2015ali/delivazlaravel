<div class="size-section">
    <div class="heading2">
        <div class="btn-container">
            <a class="btn btn-back-delivaz"
               @if($user->role_id==10)
               href="{{route('ControlPanel.Products',['locale'=>$locale])}}"
               @elseif($user->role_id==9)
               href="{{route('ControlPanel.Food',['locale'=>$locale])}}"
                @endif
            >
                <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                        fill="#FE2E17"/>
                </svg>
                {{__('RestaurantControlPanel.Back')}}
            </a>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    @if($user->role_id==10)
                        {{__("RestaurantControlPanel.Products")}}
                    @elseif($user->role_id==9)
                        {{__("RestaurantControlPanel.Foods")}}
                    @endif
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    {{__("RestaurantControlPanel.Sizes")}}
                </li>
            </ol>
        </nav>
    </div>
    <form wire:submit.prevent="submit">
        <div class="info">
            <div class="user-input">
                <label>{{__('RestaurantControlPanel.Size')}}</label>
                <input class="form-control " wire:model.defer="sizeName" placeholder="{{__('RestaurantControlPanel.SizeEX')}}"/>
                @error('sizeName')
                <div class="alert alert-danger2">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
            </div>

            <div class="user-input mt-2">
                <button type="submit" class="btn btn-save-delivaz">{{__('RestaurantControlPanel.SaveChanges')}}</button>
            </div>

        </div>
    </form>

    <div class="size"
         x-data="{ openDeleteToast: @entangle('openDeleteToast'),openEditToast: @entangle('openEditToast')}">

    @if(count($sizes)!=0)
            @include('livewire.modal.template.controlPanel.sizes.editWindow')
            @include('livewire.modal.template.controlPanel.deleteWindow')
        <div class="table-responsive-xl">
            <table id="myTable" class="table">
                <div class="d-flex flex-row table-header">
                    <div class="width-70">
                        <p class="text3">
                            {{__("RestaurantControlPanel.Size")}}
                        </p>
                    </div>

                    <div class="width-30 d-flex justify-content-center">
                        <p class="text3">
                            {{__("RestaurantControlPanel.Action")}}
                        </p>
                    </div>
                </div>
                <tbody>
                @foreach($sizes as $size)
                    <tr>

                        <td class="width-20 align-middle">
                            <div class="banner-table ">
                                <p class="text1">
                                    {{$size->name}}
                                </p>
                            </div>
                        </td>
                        <td class="width-50 align-middle">

                        </td>
                        <td class="width-30 align-middle">

                            <div class="d-flex flex-row justify-content-end">
                                <button type="button" class="btn btn-edit-delivaz mr-2 ml-2"
                                        wire:click.prevent="openEditToast({{$size->id}})">
                                    {{__("RestaurantControlPanel.Edit")}}
                                </button>

                                <button type="button" class="btn btn-delete-delivaz mr-2 ml-2"
                                        wire:click.prevent="deleteCall({{$size->id}})">
                                    {{__("RestaurantControlPanel.Delete")}}
                                </button>
                            </div>
                        <td>

                    </tr>

                @endforeach

                </tbody>
            </table>

            <div class="d-flex flex-row justify-content-center">
                {{ $sizes->links() }}
            </div>
            @else
                <div class="d-flex flex-row justify-content-start">
                    <p class="text2">
                        {{__("RestaurantControlPanel.noSizeAdded")}}
                    </p>
                </div>
            @endif
        </div>

    </div>
</div>
