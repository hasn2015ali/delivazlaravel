<div x-data="{ showNotification: false }" class="notification-container">
    <a class="nav-link dropdown-toggle btn-user-notification  gap-10"
       @click="showNotification=!showNotification">
        <i class="far fa-bell" aria-hidden="true"></i>
        <span class="badge badge-warning card-count badge-pill">1{{$count}}</span>
    </a>

    <div class="notification-list"
         x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="showNotification"
         @click.away="showNotification=false" x-cloak>
        @if($count==0)
            <div class="item-container">
                <div class="dropdown-item">
                    {{__("RestaurantControlPanel.NoNotification")}}
                </div>
            </div>
        @endif
        <div class="item-container">
            <a class="dropdown-item"
               href="{{ route('Verify-My-Email',['locale'=>app()->getLocale()]) }}">
                {{__("userFront.verifyMyEmail")}}
            </a>
        </div>
        <div class="item-container">
            <a class="dropdown-item"
               href="{{ route('Verify-My-Phone',['locale'=>app()->getLocale()]) }}">
                {{__("userFront.PhoneMyVerification")}}
            </a>
        </div>


    </div>
</div>
