<div class="header-section">
    <div class="second-header">
        <div class="container">
            <div class="log">
                <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}">
                    <img loading="lazy" src="{{asset('templateIMG/logo1.png')}}" class="img-logo">
                </a>
            </div>



            <div class="header-container">
                <div class="logout">
                    <div class="header-address">
                        <h4 class="address">
                            @if($active=="Foods")

                                {{__("RestaurantControlPanel.Foods")}}

                            @elseif($active=="Basic information")
                                {{__("RestaurantControlPanel.BasicInformation")}}

                            @elseif($active=="Work Times")
                                {{__("RestaurantControlPanel.workHours")}}

                            @elseif($active=="Banners")
                                {{__("RestaurantControlPanel.Banners")}}

                            @elseif($active=="Orders")
                                {{__("RestaurantControlPanel.Orders")}}
                            @elseif($active=="productSizes")
                                {{__("RestaurantControlPanel.Products")}}
                            @elseif($active=="foodSizes")
                                {{__("RestaurantControlPanel.Foods")}}
                            @elseif($active=="productMenu")
                                {{__("RestaurantControlPanel.Products")}}
                            @elseif($active=="foodMenu")
                                {{__("RestaurantControlPanel.Foods")}}
                            @elseif($active=="Ratting && Reviews")
                                {{__("RestaurantControlPanel.Ratting&&Reviews")}}

                            @endif

                        </h4>
                    </div>
                    <livewire:template.control-panel.service-provider-notification />
                    <div class="logout-container" x-data="{ showLogout: false }">
                        <a class="nav-link dropdown-toggle btn-user-menu  gap-10"
                           @click="showLogout=!showLogout">
                            {{$user->firstName}}
                            <div class="personal-image">
                                @if($serviceProviderPhoto->avatar)
                                    <img src="{{asset('storage/images/avatars/'.$serviceProviderPhoto->avatar)}}">
                                @else
                                    <img src="{{asset('storage/images/avatars/person240.png')}}">
                                @endif
                            </div>
                        </a>
                        <div class="logout-list "
                             x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="showLogout"
                             @click.away="showLogout=false" x-cloak>
                            <div class="item-container">
                                <div class="item">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M19 5V7H15V5H19ZM9 5V11H5V5H9ZM19 13V19H15V13H19ZM9 17V19H5V17H9ZM21 3H13V9H21V3ZM11 3H3V13H11V3ZM21 11H13V21H21V11ZM11 15H3V21H11V15Z" fill="#3E4954"/>
                                    </svg>
                                    <a class="dropdown-item"
                                       href="{{ route('Dashboard',['locale'=>app()->getLocale()]) }}">
                                        {{__("RestaurantControlPanel.Dashboard")}}
                                    </a>
                                </div>

                            </div>
                            <div class="item-container">
                                <div class="item">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g opacity="0.8">
                                            <path d="M7 7L8.41 8.41L5.83 11H16V13H5.83L8.41 15.58L7 17L2 12L7 7ZM20 5H12V3H20C21.1 3 22 3.9 22 5V19C22 20.1 21.1 21 20 21H12V19H20V5Z" fill="#323232"/>
                                        </g>
                                    </svg>
                                    <a class="dropdown-item" href="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{__("RestaurantControlPanel.Logout")}}
                                    </a>
                                </div>

                            </div>



                            <form id="logout-form" action="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                                  method="POST">
                                @csrf
                            </form>
                        </div>
                    </div>





                </div>
                <div class="mobile-btn-container">
                    <button class="btn-menu-mobile mt-1">
                        <span class="close-icon-header"> <i class="fas fa-times"></i></span>
                        <span class="open-icon-header show-on-mobil"><i style="" class="fa fa-bars" aria-hidden="true"></i></span>
                    </button>
                </div>

            </div>
        </div>

    </div>

</div>
