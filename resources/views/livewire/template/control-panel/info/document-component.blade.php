<div class="service-provider-info">
    <div class="info-container">
        <div
            x-show.transition.in.duration.1500ms.origin.top.left.scale.30.out.duration.1ms.origin.top.right.scale.30="isDocuments"
            x-cloak>

            <div class="heading2">
                <div class="btn-container">
                    <button class="btn btn-back-delivaz" @click="closeServiceProviderDocuments">
                        <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                                fill="#FE2E17"/>
                        </svg>
                        {{__('RestaurantControlPanel.Back')}}
                    </button>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            {{__("RestaurantControlPanel.BasicInformation")}}
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">
                            {{__("RestaurantControlPanel.Documents")}}
                        </li>
                    </ol>
                </nav>
            </div>
            <div class="documents-section">

                @if ($serviceProviderDocument)
                    <div class="banner-loaded ">
                        <img src=" {{ $serviceProviderDocument->temporaryUrl() }}">
                    </div>
                @endif
                <form wire:submit.prevent="submitDocument">
                    @if (!$serviceProviderDocument)

                        <div class="image-section">
                            <div wire:loading wire:target="serviceProviderDocument">
                                <x-control-panel.loading/>
                            </div>

                            <label>{{__('RestaurantControlPanel.AddNewDocument')}}</label>
                            <div class="input-file-container">
                                <input type="file" class="form-control banner-input"
                                       wire:model.defer="serviceProviderDocument"/>
                                <div class="input-handel">
                                    <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                            fill="#FE2E17"/>
                                    </svg>

                                    <p class="text1">{{__('RestaurantControlPanel.DragHereDocument')}}
                                    </p>
                                </div>
                            </div>

                            @error('serviceProviderDocument')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    @endif

                    <div class="d-flex flex-row justify-content-center width-full btn-group mt-2">

                        @if ($serviceProviderDocument)
                            <div class="btn-container">
                                <button type="submit" class="btn btn-save-delivaz">
                                    {{__('RestaurantControlPanel.SaveChanges')}}
                                </button>
                            </div>

                            <div class="btn-container ml-2 mr-2">
                                <button type="submit" class="btn btn-cancle2-delivaz "
                                        wire:click.prevent="RemoveCurrentServiceProviderDocument">
                                    {{__('RestaurantControlPanel.Cancel')}}
                                </button>
                            </div>
                        @endif

                    </div>
                </form>
                <div wire:loading wire:target="RemoveCurrentServiceProviderDocument">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="CancelDelete">
                    <x-control-panel.loading/>
                </div>
                <div class=""
                     x-data="{ openDeleteToast: @entangle('openDeleteToast'),openPreviewToast: @entangle('openPreviewToast')}">
                    @include('livewire.modal.template.controlPanel.deleteWindow')
                    @include('livewire.modal.template.controlPanel.previewWindow')

                    @if(count($documents)!=0)
                        <div class="table-responsive-xl">
                            <table id="myTable" class="table">
                                <div class="d-flex flex-row table-header">
                                    <div class="w-75">
                                        <p class="text3">
                                            {{__("RestaurantControlPanel.Documents")}}
                                        </p>
                                    </div>
                                    <div class="w-25 d-flex justify-content-center">
                                        <p class="text3">
                                            {{__("RestaurantControlPanel.Action")}}
                                        </p>
                                    </div>
                                </div>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($documents as $doc)
                                    <tr>

                                        <td class="align-middle">
                                            <p class="text2"> {{__("RestaurantControlPanel.Document")}} {{$i}}</p>
                                            @if($doc->deleteByOwner==1)
                                                <small>
                                                    {{__("RestaurantControlPanel.underReview")}}
                                                </small>
                                            @endif
                                        </td>

                                        <td class="align-middle">
                                            <div class="d-flex flex-row justify-content-end">
                                                <button type="button" class="btn btn-edit-delivaz"
                                                        wire:click.prevent="openPreviewToast('{{$i}}','{{$doc->name}}')">
                                                    {{__("RestaurantControlPanel.Preview")}}
                                                </button>
                                                @if($doc->deleteByOwner==0)
                                                    <button type="button" class="btn btn-delete-delivaz"
                                                            wire:click.prevent="deleteCall({{$doc->id}})">
                                                        {{__("RestaurantControlPanel.Delete")}}
                                                        {{--                                                <i class="fa fa-trash-o "></i>--}}
                                                    </button>
                                                @elseif($doc->deleteByOwner==1)
                                                    <button type="button" class="btn btn-cancle2-delivaz"
                                                            wire:click.prevent="CancelDelete('{{$doc->id}}')">
                                                        {{__("RestaurantControlPanel.CancelDelete")}}
                                                    </button>
                                                @endif

                                            </div>
                                        <td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="d-flex flex-row justify-content-center">
                            {{ $documents->links() }}
                        </div>
                    @else
                        <div class="d-flex flex-row justify-content-start">
                            <p class="text2">
                                {{__("RestaurantControlPanel.noDocuments")}}
                            </p>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
