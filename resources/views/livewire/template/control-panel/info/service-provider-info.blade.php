<div class="service-provider-info">
    <div class="">
        <div class="info-container">
            <div
                x-show.transition.in.duration.1500ms.origin.top.left.scale.30.out.duration.1ms.origin.top.right.scale.30="isServiceProviderInfo"
                x-cloak wire:ignore.self>
                <div class="heading2">
                    <div class="btn-container">
                        <button class="btn btn-back-delivaz" @click="closeServiceProviderInfo()">
                            {{--                            <i class="fas fa-long-arrow-alt-left"></i>--}}
                            <svg width="24" height="12" viewBox="0 0 24 12" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z"
                                    fill="#FE2E17"/>
                            </svg>
                            {{__('RestaurantControlPanel.Back')}}
                        </button>
                    </div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                {{__("RestaurantControlPanel.BasicInformation")}}
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{__("RestaurantControlPanel.RestaurantInformation")}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <div wire:loading wire:target="mount">
                    <x-control-panel.loading/>
                </div>
                <form wire:submit.prevent="updateInfo">
                    <div class="info ">
                        <div class="d-flex flex-column user-input2">
                            <div class="user-input">
                                <label>{{__('RestaurantControlPanel.DefaultName')}}</label>
                                <input class="form-control " wire:model.defer="DefaultName"/>
                                @error('DefaultName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="user-input ">
                                <label>{{__('RestaurantControlPanel.EnglishName')}}</label>
                                <input class="form-control" wire:model.defer="EnglishName"/>
                                @error('EnglishName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div wire:loading wire:target="photo">
                            <x-control-panel.loading/>
                        </div>
                        <div class="user-input d-flex justify-content-end">
                            <div class="input-file-container">
                                {{--                                <img src="{{asset('/images/avatars/')}}"/>--}}
                                @if($photo)
                                    <div class="img">
                                        <img src="{{ $photo->temporaryUrl() }}"/>

                                    </div>
                                @endif
                                @if(!isset($photo) and isset($oldAvatar) )
                                    <div class="img">
                                        <img src="{{asset('storage/images/avatars/'.$oldAvatar)}}"/>

                                    </div>
                                @endif
                                <input type="file" class="form-control banner-input"
                                       wire:model.defer="photo"/>

                                <div class="input-handel  @if($photo) back-ground-transparent @endif">
                                    <div class="items">
                                        <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                                fill="#FE2E17"/>
                                        </svg>

                                        {{--                                        <i class="fas fa-cloud-upload-alt"></i>--}}
                                        <p class="text1">{{__('RestaurantControlPanel.DragHereLogo')}}
                                        </p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="info">
                        <div class="d-flex flex-column width-full text-area-container">
                            <label>{{__('RestaurantControlPanel.EnterFullAddress')}}</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="address"
                                      rows="3">{{$address}}</textarea>
                            @error('address')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="info">
                        <div class="user-input">
                            @if($validPhone1)

                            @endif
                            <label>{{__('RestaurantControlPanel.FirstPhoneNumber')}}</label>
                            &nbsp;
                            @if($validPhone1)
                                {{$validPhone1}}
                                &nbsp;
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            @endif
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="">+{{$codeNumber}}</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       wire:model.lazy="phone1">
                            </div>
                            @error('phone1')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input">
                            @if($validPhone2)


                            @endif
                            <label>{{__('RestaurantControlPanel.SecondPhoneNumber')}}</label>
                            &nbsp;
                            @if($validPhone2)
                                &nbsp;
                                {{$validPhone2}}
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            @endif
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="">+{{$codeNumber}}</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup"
                                       wire:model.lazy="phone2">
                            </div>
                            @error('phone2')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="info">
                        <div class="user-input">
                            <label>{{__('RestaurantControlPanel.MinimumOrderAmount')}}</label>
                            <input class="form-control" wire:model.defer="minimumOrderAmount"/>
                            @error('minimumOrderAmount')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input">
                            <label>{{__('RestaurantControlPanel.DeliveryMode')}}</label>
                            <div class="dropdown " id="cities_list">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if($selectDeliveryMode)
                                        @if($selectDeliveryMode=="self")
                                            Self Pick-Up
                                        @else
                                            {{ucfirst($selectDeliveryMode)}}
                                        @endif
                                    @else
                                        {{__("RestaurantControlPanel.SelectDeliveryMode")}}
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    <a href="#"
                                       wire:click.prevent="sendDeliveryModeValue('delivery')">
                                        <div class="item-container">
                                            <div class="d-flex flex-row item align-items-center">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                    <path d="M22 4L12 14.01L9 11.01" stroke-width="2"
                                                          stroke-linecap="round"
                                                          stroke-linejoin="round"/>
                                                </svg>

                                                <p class="dropdown-item">Delivery</p>

                                            </div>

                                        </div>
                                    </a>
                                    <a href="#"
                                       wire:click.prevent="sendDeliveryModeValue('self')">
                                        <div class="item-container">
                                            <div class="d-flex flex-row item align-items-center">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>


                                                <p class="dropdown-item">Self Pick-Up</p>

                                            </div>

                                        </div>
                                    </a>
                                    <a href="#"
                                       wire:click.prevent="sendDeliveryModeValue('both')">
                                        <div class="item-container">
                                            <div class="d-flex flex-row item align-items-center">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                     xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                        stroke-linejoin="round"/>
                                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                                          stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>


                                                <p class="dropdown-item">Both</p>

                                            </div>

                                        </div>

                                    </a>
                                </div>
                            </div>
                            @error('selectDeliveryMode')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="info">

                        <div class="user-input">
                            <label>{{__('RestaurantControlPanel.SelectTax')}}</label>
                            <div class="dropdown " id="cities_list">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(count($selectedTax)>0)
                                        @if($selectedTax['type']=="Ratio")
                                            {{$selectedTax['value']}} %
                                        @else
                                            {{$selectedTax['value']}} {{$countryCurrency}}
                                        @endif
                                    @else
                                        {{__("RestaurantControlPanel.SelectTax")}}
                                    @endif
                                </button>
                                <div class="dropdown-menu">

                                    @foreach($taxesInCountry as $tax)
                                        <a href="#"
                                           wire:click.prevent="sendTaxValue({{json_encode($tax)}})">
                                            <div class="item-container">
                                                <div class="d-flex flex-row item align-items-center">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
                                                        <path
                                                            d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                            stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                                            stroke-linejoin="round"/>
                                                        <path d="M22 4L12 14.01L9 11.01" stroke-width="2"
                                                              stroke-linecap="round" stroke-linejoin="round"/>
                                                    </svg>

                                                    <p class="dropdown-item">
                                                        {{$tax['value']}}
                                                        @if($tax['type']=="Ratio")
                                                            %
                                                        @else
                                                            {{$countryCurrency}}
                                                        @endif
                                                    </p>

                                                </div>

                                            </div>
                                        </a>
                                    @endforeach

                                </div>
                            </div>
                            @error('selectDeliveryMode')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="d-flex flex-row justify-content-center width-full mt-5">
                        <div class="btn-container">
                            <button type="submit" class="btn btn-save-delivaz">
                                {{__('RestaurantControlPanel.SaveChanges')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>


    </div>
    <script>

    </script>
</div>
