<div class="service-provider-info">
    <div class="">
        <div class="info-container">

            <div
                x-show.transition.in.duration.1500ms.origin.top.left.scale.30.out.duration.1ms.origin.top.right.scale.30="isPassword" x-cloak>

                <div class="heading2">
                    <div class="btn-container">
                        <button class="btn btn-back-delivaz" @click="closePassword()">
                            {{--                            <i class="fas fa-long-arrow-alt-left"></i>--}}
                            <svg width="24" height="12" viewBox="0 0 24 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.274969 5.14888C0.27525 5.1486 0.275484 5.14827 0.275812 5.14799L5.17444 0.272997C5.54142 -0.0922061 6.135 -0.090847 6.5003 0.276184C6.86555 0.643168 6.86414 1.23675 6.49716 1.60199L3.20822 4.87499H23.0625C23.5803 4.87499 24 5.29471 24 5.81249C24 6.33027 23.5803 6.74999 23.0625 6.74999H3.20827L6.49711 10.023C6.86409 10.3882 6.8655 10.9818 6.50025 11.3488C6.13495 11.7159 5.54133 11.7171 5.17439 11.352L0.275764 6.47699C0.275483 6.47671 0.27525 6.47638 0.274921 6.4761C-0.0922505 6.10963 -0.0910778 5.51413 0.274969 5.14888Z" fill="#FE2E17"/>
                            </svg>
                            {{__('RestaurantControlPanel.Back')}}
                        </button>
                    </div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                {{__("RestaurantControlPanel.BasicInformation")}}
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{__("RestaurantControlPanel.ChangePassword")}}
                            </li>
                        </ol>
                    </nav>
                </div>
                <div wire:loading wire:target="submitOldPassword">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="submitNewPassword">
                    <x-control-panel.loading/>
                </div>
                    @if($showOldPasswordFild)
                    <form wire:submit.prevent="submitOldPassword">
                        <div class="info">
                            <div class="user-input">
                                <label>{{__('RestaurantControlPanel.OldPassword')}}</label>
                                <input type="password" class="form-control" wire:model.defer="oldPassword"/>
                                @error('minimumOrderAmount')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="d-flex flex-row justify-content-center width-full">
                            <div class="btn-container">
                                <button type="submit" class="btn btn-save-delivaz">
                                    {{__('RestaurantControlPanel.Submit')}}
                                </button>
                            </div>

                        </div>
                    </form>
                    @endif

                    @if($showNewPasswordFild)
                            <form wire:submit.prevent="submitNewPassword">
                        <div class="info">
                            <div class="user-input">
                                <label>{{__('RestaurantControlPanel.NewPassword')}}</label>
                                <input type="password" class="form-control" wire:model.defer="newPassword"/>
                                @error('newPassword')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                                <div class="info">
                                    <div class="user-input">
                                        <label>{{__('RestaurantControlPanel.NewPasswordConformation')}}</label>
                                        <input type="password" class="form-control" wire:model.defer="newPasswordConformation"/>
                                        @error('newPassword')
                                        <div class="alert alert-danger2">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-row justify-content-center width-full">
                                    <div class="btn-container">
                                        <button type="submit" class="btn btn-save-delivaz">
                                            {{__('RestaurantControlPanel.SaveChanges')}}
                                        </button>
                                    </div>

                                </div>
                            </form>
                    @endif



            </div>
        </div>
    </div>
    </div>
</div>
