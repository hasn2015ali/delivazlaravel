<div
    wire:ignore.self
    x-show.transition.in.duration.1500ms.origin.top.left.scale.30.out.duration.1ms.origin.top.right.scale.30="isList">
    <div class="heading1 d-flex">
        {{--        <svg width="9" height="55" viewBox="0 0 9 55" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
        {{--            <rect x="3" y="4" width="3" height="51" rx="1.5" fill="#FFCBDE"/>--}}
        {{--            <rect width="9" height="25" rx="4.5" fill="#FE2E17"/>--}}
        {{--        </svg>--}}

        <div class="wide"></div>
        <div class="thin"></div>
        <div class="owner d-flex flex-row">
            <div class="img">
                <img src="{{asset('templateIMG/control_panel/hand.png')}}">
            </div>
            <h2 class="address1">{{$user->firstName}}&nbsp;{{$user->lastName}} </h2>
        </div>
        <h2 class="address2"> {{$pageName}}</h2>
    </div>


    <div class="list-items">
        <div class="item">
            <div class="icon">
                <svg width="32" height="31" viewBox="0 0 32 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M4 30.5H22.75C23.7442 30.4989 24.6974 30.1035 25.4004 29.4004C26.1035 28.6974 26.4989 27.7442 26.5 26.75V16.75C26.5 16.4185 26.3683 16.1005 26.1339 15.8661C25.8995 15.6317 25.5815 15.5 25.25 15.5C24.9185 15.5 24.6005 15.6317 24.3661 15.8661C24.1317 16.1005 24 16.4185 24 16.75V26.75C23.9997 27.0814 23.8679 27.3992 23.6336 27.6336C23.3992 27.8679 23.0814 27.9997 22.75 28H4C3.66857 27.9997 3.3508 27.8679 3.11645 27.6336C2.88209 27.3992 2.7503 27.0814 2.75 26.75V9.25C2.7503 8.91857 2.88209 8.6008 3.11645 8.36645C3.3508 8.13209 3.66857 8.0003 4 8H15.25C15.5815 8 15.8995 7.8683 16.1339 7.63388C16.3683 7.39946 16.5 7.08152 16.5 6.75C16.5 6.41848 16.3683 6.10054 16.1339 5.86612C15.8995 5.6317 15.5815 5.5 15.25 5.5H4C3.00577 5.50109 2.05258 5.89653 1.34956 6.59956C0.646531 7.30258 0.251092 8.25577 0.25 9.25V26.75C0.251092 27.7442 0.646531 28.6974 1.34956 29.4004C2.05258 30.1035 3.00577 30.4989 4 30.5Z"
                        fill="#EA4989"/>
                    <path
                        d="M25.25 0.5C24.0139 0.5 22.8055 0.866556 21.7777 1.55331C20.7499 2.24007 19.9488 3.21619 19.4758 4.35823C19.0027 5.50027 18.8789 6.75693 19.1201 7.96931C19.3613 9.1817 19.9565 10.2953 20.8306 11.1694C21.7047 12.0435 22.8183 12.6388 24.0307 12.8799C25.2431 13.1211 26.4997 12.9973 27.6418 12.5242C28.7838 12.0512 29.7599 11.2501 30.4467 10.2223C31.1334 9.19451 31.5 7.98613 31.5 6.75C31.498 5.093 30.8389 3.50442 29.6673 2.33274C28.4956 1.16106 26.907 0.501952 25.25 0.5ZM25.25 10.5C24.5083 10.5 23.7833 10.2801 23.1666 9.86801C22.5499 9.45596 22.0693 8.87029 21.7855 8.18506C21.5016 7.49984 21.4274 6.74584 21.5721 6.01841C21.7167 5.29098 22.0739 4.6228 22.5983 4.09835C23.1228 3.5739 23.791 3.21675 24.5184 3.07206C25.2458 2.92736 25.9998 3.00162 26.6851 3.28545C27.3703 3.56928 27.9559 4.04993 28.368 4.66661C28.7801 5.2833 29 6.00832 29 6.75C28.9989 7.74423 28.6035 8.69742 27.9004 9.40044C27.1974 10.1035 26.2442 10.4989 25.25 10.5Z"
                        fill="#EA4989"/>
                    <path
                        d="M6.5 13H12.75C13.0815 13 13.3995 12.8683 13.6339 12.6339C13.8683 12.3995 14 12.0815 14 11.75C14 11.4185 13.8683 11.1005 13.6339 10.8661C13.3995 10.6317 13.0815 10.5 12.75 10.5H6.5C6.16848 10.5 5.85054 10.6317 5.61612 10.8661C5.3817 11.1005 5.25 11.4185 5.25 11.75C5.25 12.0815 5.3817 12.3995 5.61612 12.6339C5.85054 12.8683 6.16848 13 6.5 13Z"
                        fill="#EA4989"/>
                    <path
                        d="M5.25 16.75C5.25 17.0815 5.3817 17.3995 5.61612 17.6339C5.85054 17.8683 6.16848 18 6.5 18H17.75C18.0815 18 18.3995 17.8683 18.6339 17.6339C18.8683 17.3995 19 17.0815 19 16.75C19 16.4185 18.8683 16.1005 18.6339 15.8661C18.3995 15.6317 18.0815 15.5 17.75 15.5H6.5C6.16848 15.5 5.85054 15.6317 5.61612 15.8661C5.3817 16.1005 5.25 16.4185 5.25 16.75Z"
                        fill="#EA4989"/>
                </svg>
                {{--                <i class="fa fa-file-text-o"></i>--}}
            </div>
            <div class="text">
                <p>
                    @if($user->role_id ==9)
                    {{__("RestaurantControlPanel.RestaurantInformation")}}
                    @elseif($user->role_id ==10)
                        {{__("RestaurantControlPanel.ShopInformation")}}

                    @endif
                </p>
            </div>
            <div class="btn-container">
                <button class="btn btn-edit-delivaz" @click="openServiceProviderInfo()">
                    {{__('RestaurantControlPanel.Edit')}}
                </button>
            </div>

        </div>
        <div class="item">
            <div class="icon">
                <svg width="32" height="31" viewBox="0 0 32 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M4 30.5H22.75C23.7442 30.4989 24.6974 30.1035 25.4004 29.4004C26.1035 28.6974 26.4989 27.7442 26.5 26.75V16.75C26.5 16.4185 26.3683 16.1005 26.1339 15.8661C25.8995 15.6317 25.5815 15.5 25.25 15.5C24.9185 15.5 24.6005 15.6317 24.3661 15.8661C24.1317 16.1005 24 16.4185 24 16.75V26.75C23.9997 27.0814 23.8679 27.3992 23.6336 27.6336C23.3992 27.8679 23.0814 27.9997 22.75 28H4C3.66857 27.9997 3.3508 27.8679 3.11645 27.6336C2.88209 27.3992 2.7503 27.0814 2.75 26.75V9.25C2.7503 8.91857 2.88209 8.6008 3.11645 8.36645C3.3508 8.13209 3.66857 8.0003 4 8H15.25C15.5815 8 15.8995 7.8683 16.1339 7.63388C16.3683 7.39946 16.5 7.08152 16.5 6.75C16.5 6.41848 16.3683 6.10054 16.1339 5.86612C15.8995 5.6317 15.5815 5.5 15.25 5.5H4C3.00577 5.50109 2.05258 5.89653 1.34956 6.59956C0.646531 7.30258 0.251092 8.25577 0.25 9.25V26.75C0.251092 27.7442 0.646531 28.6974 1.34956 29.4004C2.05258 30.1035 3.00577 30.4989 4 30.5Z"
                        fill="#EA4989"/>
                    <path
                        d="M25.25 0.5C24.0139 0.5 22.8055 0.866556 21.7777 1.55331C20.7499 2.24007 19.9488 3.21619 19.4758 4.35823C19.0027 5.50027 18.8789 6.75693 19.1201 7.96931C19.3613 9.1817 19.9565 10.2953 20.8306 11.1694C21.7047 12.0435 22.8183 12.6388 24.0307 12.8799C25.2431 13.1211 26.4997 12.9973 27.6418 12.5242C28.7838 12.0512 29.7599 11.2501 30.4467 10.2223C31.1334 9.19451 31.5 7.98613 31.5 6.75C31.498 5.093 30.8389 3.50442 29.6673 2.33274C28.4956 1.16106 26.907 0.501952 25.25 0.5ZM25.25 10.5C24.5083 10.5 23.7833 10.2801 23.1666 9.86801C22.5499 9.45596 22.0693 8.87029 21.7855 8.18506C21.5016 7.49984 21.4274 6.74584 21.5721 6.01841C21.7167 5.29098 22.0739 4.6228 22.5983 4.09835C23.1228 3.5739 23.791 3.21675 24.5184 3.07206C25.2458 2.92736 25.9998 3.00162 26.6851 3.28545C27.3703 3.56928 27.9559 4.04993 28.368 4.66661C28.7801 5.2833 29 6.00832 29 6.75C28.9989 7.74423 28.6035 8.69742 27.9004 9.40044C27.1974 10.1035 26.2442 10.4989 25.25 10.5Z"
                        fill="#EA4989"/>
                    <path
                        d="M6.5 13H12.75C13.0815 13 13.3995 12.8683 13.6339 12.6339C13.8683 12.3995 14 12.0815 14 11.75C14 11.4185 13.8683 11.1005 13.6339 10.8661C13.3995 10.6317 13.0815 10.5 12.75 10.5H6.5C6.16848 10.5 5.85054 10.6317 5.61612 10.8661C5.3817 11.1005 5.25 11.4185 5.25 11.75C5.25 12.0815 5.3817 12.3995 5.61612 12.6339C5.85054 12.8683 6.16848 13 6.5 13Z"
                        fill="#EA4989"/>
                    <path
                        d="M5.25 16.75C5.25 17.0815 5.3817 17.3995 5.61612 17.6339C5.85054 17.8683 6.16848 18 6.5 18H17.75C18.0815 18 18.3995 17.8683 18.6339 17.6339C18.8683 17.3995 19 17.0815 19 16.75C19 16.4185 18.8683 16.1005 18.6339 15.8661C18.3995 15.6317 18.0815 15.5 17.75 15.5H6.5C6.16848 15.5 5.85054 15.6317 5.61612 15.8661C5.3817 16.1005 5.25 16.4185 5.25 16.75Z"
                        fill="#EA4989"/>
                </svg>
            </div>
            <div class="text">
                <p>
                    {{__("RestaurantControlPanel.OwnerInformation")}}
                </p>
            </div>
            <div class="btn-container">
                <button class="btn btn-edit-delivaz" @click="openOwner">
                    {{__('RestaurantControlPanel.Edit')}}
                </button>
            </div>

        </div>
        <div class="item">
            <div class="icon">
                <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M31.6667 8.33333V31.6667H8.33333V8.33333H31.6667ZM31.6667 5H8.33333C6.5 5 5 6.5 5 8.33333V31.6667C5 33.5 6.5 35 8.33333 35H31.6667C33.5 35 35 33.5 35 31.6667V8.33333C35 6.5 33.5 5 31.6667 5ZM23.5667 19.7667L18.5667 26.2167L15 21.9L10 28.3333H30L23.5667 19.7667Z" fill="#EA4989"/>
                </svg>
            </div>
            <div class="text">
                <p>
                    {{__("RestaurantControlPanel.Documents")}}
                </p>
            </div>
            <div class="btn-container">
                <button class="btn btn-edit-delivaz" @click="openServiceProviderDocuments()">
                    {{__('RestaurantControlPanel.Edit')}}
                </button>
            </div>

        </div>
        <div class="item">
            <div class="icon">
                <svg width="28" height="36" viewBox="0 0 28 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M24.0013 12.3327H22.3346V8.99935C22.3346 4.39935 18.6013 0.666016 14.0013 0.666016C9.4013 0.666016 5.66797 4.39935 5.66797 8.99935H9.0013C9.0013 6.23268 11.2346 3.99935 14.0013 3.99935C16.768 3.99935 19.0013 6.23268 19.0013 8.99935V12.3327H4.0013C2.16797 12.3327 0.667969 13.8327 0.667969 15.666V32.3327C0.667969 34.166 2.16797 35.666 4.0013 35.666H24.0013C25.8346 35.666 27.3346 34.166 27.3346 32.3327V15.666C27.3346 13.8327 25.8346 12.3327 24.0013 12.3327ZM24.0013 32.3327H4.0013V15.666H24.0013V32.3327ZM14.0013 27.3327C15.8346 27.3327 17.3346 25.8327 17.3346 23.9993C17.3346 22.166 15.8346 20.666 14.0013 20.666C12.168 20.666 10.668 22.166 10.668 23.9993C10.668 25.8327 12.168 27.3327 14.0013 27.3327Z"
                        fill="#EA4989"/>
                </svg>

                {{--                <i class="fas fa-unlock"></i>--}}
            </div>
            <div class="text">
                <p>
                    {{__("RestaurantControlPanel.ChangePassword")}}
                </p>
            </div>
            <div class="btn-container">
                <button class="btn btn-edit-delivaz" @click="openPassword">
                    {{__('RestaurantControlPanel.Edit')}}
                </button>
            </div>

        </div>
    </div>
</div>
