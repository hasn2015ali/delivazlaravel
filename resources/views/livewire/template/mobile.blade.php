<div style=" " class="mobile-section ">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-lg-6 col-sm-10 col-xs-8 mobile-info">
                <div class="d-flex flex-row justify-content-start">
                    <p class="text1">{{__("home.Pick")}}&nbsp;{{__("home.your")}}&nbsp;{{__("home.Order")}}&nbsp;{{__("home.from")}}&nbsp;{{__("home.A")}}&nbsp;{{__("home.to")}}&nbsp;{{__("home.Z")}}</p>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <p class="text2"><span class="delivaz">{{__("home.delivaz")}} </span> <span class="app">{{__("home.app")}}</span></p>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <p class="text3">
                        {{__("home.lorem")}}
                    </p>

                </div>
                <div class="d-flex flex-row justify-content-between btn-social-container">
                   <button class="btn btn-social d-flex flex-row justify-content-around">
                       <i class="fab fa-apple"></i>
                       <div class="d-flex flex-column  justify-content-start align-items-start">
                           <p>{{__("home.available_on")}}</p>
                           <h6>{{__("home.Apple_Store")}}</h6>
                       </div>
                   </button>
                    <button class="btn btn-social d-flex flex-row justify-content-around">
                        <i class="fab fa-google-play"></i>
                        <div class="d-flex flex-column justify-content-start align-items-start ">
                            <p>{{__("home.get_it_on")}}</p>
                            <h6>{{__("home.google_play")}}</h6>
                        </div>
                    </button>
                    <a id="save" class="btn btn-social d-flex flex-row justify-content-around desktop" >
                        <i class="fas fa-download"></i>
                        <div class="d-flex flex-column  justify-content-start align-items-start">
                            <p>{{__("home.Save_the_page")}}</p>
                            <h6>{{__("home.to_desktop")}}</h6>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6 photo-mobile">
{{--                <div class="parent">--}}
{{--                    <div class="circle">--}}

{{--                    </div>--}}
{{--                    <div class="photo1">--}}
{{--                        <img src="{{asset('templateIMG/1.png')}}">--}}
{{--                    </div>--}}
{{--                    <div class="photo2">--}}
{{--                        <img src="{{asset('templateIMG/2.png')}}">--}}
{{--                    </div>--}}

{{--                </div>--}}

            </div>
        </div>
    </div>


    <div style="" class="parent">
        <div class="circle">

        </div>
        <div class="photo1">
            <img  loading="lazy" src="{{asset('templateIMG/1.png')}}">
        </div>
        <div class="photo2">
            <img  loading="lazy" src="{{asset('templateIMG/2.png')}}">
        </div>

    </div>

</div>
