<div class="food-addition ">
    <div class="img">
        <img src="{{asset($foldersForAddition.$addition['photo'])}}">
    </div>
    <div class="detail">
        <p class="content"> {{$addition['weight']}} g</p>
        <p class="addition-price"> {{$priceAfterCommission}} {{$currencyCode}}</p>
    </div>
    <div class="address ">
        {{$addition['name']}}
    </div>
    <div class="content">
        {{$addition['content']}}
    </div>
    <div class="d-flex flex-row justify-content-end btn-s-qtn">
        <button class="btn btn-qtn @if($qtn==0) disable @endif"
                wire:click='minusQuantity'>
            <i class="fas fa-minus"></i></button>
        <p class="qtn">
            {{$qtn}}
        </p>
        <button class="btn btn-qtn"
                wire:click="plusQuantity">
            <i class="fas fa-plus"></i></button>
    </div>
    <div wire:loading wire:target="minusQuantity">
        <x-control-panel.loading/>
    </div>
    <div wire:loading wire:target="plusQuantity">
        <x-control-panel.loading/>
    </div>
</div>

