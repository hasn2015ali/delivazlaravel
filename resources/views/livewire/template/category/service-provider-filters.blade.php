<div class="d-flex flex-column justify-content-center align-items-center list">


    @if($mealFilters)
        <a class="btn btn-filter @if($activeFilter==$filterKey) active @endif "
           wire:click.prevent="getProductByFilter('0','res','0','all')"
        >
            {{__('serviceProvider.AllMeals')}}
        </a>
        @foreach($mealFilters as $key=>$filter)
            {{--            {{dd($filterKey,$key,"$key"==$filterKey)}}--}}
            {{--            <div>--}}
            {{--                <a class="btn btn-filter @if($key==0) active @endif "--}}
            {{--                   onclick="sendEmitEvent({{$filter->meal_filter_id}},type='res','{{$filter->name}}')"--}}
            {{--                   data-toggle="collapse" data-target="#demo">--}}
            {{--                    {{$filter->name}}--}}
            {{--                </a>--}}
            {{--            <a class="btn btn-filter @if($key==0) active @endif "--}}
            {{--               onclick="sendEmitEvent({{$filter->filter_id}},type='res','{{$filter->mailFilterTrans->where('code_lang',$langCode)->first()->name}}')"--}}
            {{--               data-toggle="collapse" data-target="#demo">--}}
            {{--                {{$filter->mailFilterTrans->where('code_lang',$langCode)->first()->name}}--}}
            {{--            </a>--}}

            @if(isset($filter->mailFilterTrans->where('code_lang',$langCode)->first()->name))
                <a class="btn btn-filter @if("$key"==$filterKey) active @endif "
                   wire:click.prevent="getProductByFilter({{$filter->filter_id}},'res','{{$filter->mailFilterTrans->where('code_lang',$langCode)->first()->name}}','{{$key}}')"
                >
                    {{$filter->mailFilterTrans->where('code_lang',$langCode)->first()->name}}
                </a>
            @endif

            {{--            </div>--}}
        @endforeach
    @endif

    @if($productFilters)
        <a class="btn btn-filter @if($activeFilter==$filterKey) active @endif "
           wire:click.prevent="getProductByFilter('0','ven','0','all')"
        >
            {{__('serviceProvider.AllProducts')}}
        </a>
        @foreach($productFilters as $key=>$filter)
            {{--                {{dd($filter->product_filter_id)}}--}}
            {{--            <div>--}}
            {{--                <a class="btn btn-filter @if($key==0) active @endif "--}}
            {{--                   onclick="sendEmitEvent({{$filter->filter_id}},type='ven','{{$filter->productFilterTrans->where('code_lang',$langCode)->first()->name}}')"--}}
            {{--                   data-toggle="collapse" data-target="#demo">--}}
            {{--                    {{$filter->productFilterTrans->where('code_lang',$langCode)->first()->name}}--}}
            {{--                </a>--}}

            @if(isset($filter->mailFilterTrans->where('code_lang',$langCode)->first()->name))
                <a class="btn btn-filter @if("$key"==$filterKey) active @endif "
                   wire:click.prevent="getProductByFilter({{$filter->filter_id}},type='ven','{{$filter->productFilterTrans->where('code_lang',$langCode)->first()->name}}','{{$key}}')"
                >
                    {{$filter->productFilterTrans->where('code_lang',$langCode)->first()->name}}
                </a>
            @endif

            {{--            </div>--}}
        @endforeach
    @endif

    <script type="text/javascript">
        window.onload = function () {
            {{--Livewire.emitTo('template.category.service-provider-items', 'getItemByFilter', '{{$firstFilterID}}', '{{$filterType}}','{{$firstFilterName}}');--}}
        }

        // function sendEmitEvent(filterId,type, filterName) {
        //     // console.log(filterId,type,filterName);
        //     Livewire.emitTo('template.category.service-provider-items', 'getItemByFilter', filterId,type,filterName);
        //
        //
        // }
    </script>

</div>
