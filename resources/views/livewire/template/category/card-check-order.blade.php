<div class="user-cart-check-toast"
     x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="cardToast"
     @click.away="cardToast=false" x-data="{ cardToast: @entangle('cardToast') }" x-cloak>
    <div class="cart-header d-flex flex-row justify-content-between ">
        <div class="mt-1">
            <h4 class="toast-address">{{__("userFront.order_confirmation")}}</h4>
        </div>
        <div>
            <button class="btn btn-close" @click="cardToast=false"><i class="fas fa-times "></i></button>
        </div>
    </div>
    <div class="cart-content">

        {{__("userFront.check")}}
        <div class="m-5">
            <div wire:loading wire:target="deleteOldOrder">
                <x-control-panel.loading/>
            </div>
            <div wire:loading wire:target="cancelNewOrder">
                <x-control-panel.loading/>
            </div>
        </div>

        <div class="d-flex flex-row justify-content-between btn-container">
            <button class="btn btn-yes" wire:click="deleteOldOrder">
                {{__("userFront.Yes")}}
            </button>
            <button class="btn btn-no" wire:click="cancelNewOrder">
                {{__("userFront.No")}}
            </button>

        </div>

    </div>


    {{-- Stop trying to control. --}}
</div>
