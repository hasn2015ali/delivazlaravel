<div class="d-flex flex-row sell-on-delivaz-section" style="width: 100%">
    <div class="cover">
        @if($cover)
            @if($folders)
                <img width="800"
                     height="315"
                     loading="lazy"
                     src="{{asset($folders.$cover)}}">
                <div class="content d-flex flex-column justify-content-start">

                    <h6 class="address">  {{__('serviceProvider.SellOnDelivaz')}}</h6>
                    <p class="text">{{__('serviceProvider.Lorem')}}</p>

                    <a href="{{route('Register-Shop-Or-Restaurant',['locale'=>app()->getLocale()])}}" class="btn btn-sell-on-delivaz">
                        {{__('serviceProvider.Getstarted')}}
                    </a>
                </div>
            @endif
{{--        @else--}}
{{--            <img width="800"--}}
{{--                 height="315"--}}
{{--                 loading="lazy"--}}
{{--                 src="{{asset('storage/images/pages/serviceProvidersPage/1.png')}}">--}}
{{--            <div class="content d-flex flex-column justify-content-start">--}}

{{--                <h6 class="address">  {{__('serviceProvider.SellOnDelivaz')}}</h6>--}}
{{--                <p class="text">{{__('serviceProvider.Lorem')}}</p>--}}

{{--                <a href="#" class="btn btn-sell-on-delivaz">--}}
{{--                    {{__('serviceProvider.Getstarted')}}--}}
{{--                </a>--}}
{{--            </div>--}}
        @endif

    </div>
</div>
