<div class="product-section ">
    @include('livewire.template.category.card-check-order')
    <div class="container ">
        <div class="parent d-flex flex-wrap">
            <livewire:template.category.product-photo-component
                :photos="$allProductPhoto"
                :itemPhoto="$item->photo"
                :folders="$folders"/>
            <div class="d-flex flex-column right-side">
                <div wire:loading wire:target="addToWishlist">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <h4 class="address">
                        {{$item->translation->where('code_lang','en')->first()->name}}
                    </h4>
                    <div class="btn-favourite-container">
                        <button class="btn btn-favourite @if($isFavourite) btn-favourite-active @endif "
                                wire:click.prevent="addToWishlist">
                            <i class="fas fa-heart"></i>
                        </button>
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-start align-items-center weight">
                    @if(isset($item->weight))
                        <svg width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="3" cy="3" r="3" fill="#FF9126"/>
                        </svg>
                        <p class="">{{$item->weight}} g</p>
                    @endif
                    @if(isset($item->time_hour) and $item->time_hour!="00" and $item->time_hour=!"0")
                        <svg width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="3" cy="3" r="3" fill="#FF9126"/>
                        </svg>
                        <p class="">{{$item->time_hour}} hour</p>
                    @endif
                    @if(isset($item->time_minutes) and $item->time_minutes!="00" and $item->time_minutes!="0")
                        <svg width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="3" cy="3" r="3" fill="#FF9126"/>
                        </svg>
                        <p class="">{{$item->time_minutes}} min</p>
                    @endif
                </div>

                <div class="d-flex flex-row justify-content-start">
                    <p class="content">
                        {{$item->translation->where('code_lang','en')->first()->content}}
                    </p>
                </div>

                <div wire:loading wire:target="selectSize">
                    <x-control-panel.loading/>
                </div>

                @if(count($item->sizes)!=0 )
                    <div class="d-flex flex-row justify-content-start">
                        <h4 class="address mb-3">
                            {{__('serviceProvider.Sizes')}}
                        </h4>
                    </div>

                    <div class="d-flex flex-wrap justify-content-start sizes">
                        @if($item->size)
                            <?php
                            $defaultPrice=$this->calculatePriceWithCommission( $item->price,$delivaz_commission)
                            ?>
                            <div class="btn-size @if($selectedSize==0) active @endif"
                                 wire:click="selectSize(0,{{$defaultPrice}},'{{$item->size}}')">
                                <p class="size-name">
                                    {{$item->size}}
                                </p>
                                <p class="size-weight">
                                    {{$item->weight}} g
                                </p>
                                <div class="size-price">
                                    {{$defaultPrice}} {{$currencyCode}}
                                </div>
                            </div>
                        @endif
                        @foreach($item->sizes as $index=> $size)
                            <?php
                                $sizePrice=$this->calculatePriceWithCommission( $size->price,$delivaz_commission)
                                ?>
                            <div class="btn-size @if($selectedSize==$index+1) active @endif"
                                 wire:click="selectSize({{$index+1}},{{$sizePrice}},'{{$size->size}}')">
                                <p class="size-name">
                                    {{$size->size}}
                                </p>
                                <p class="size-weight">
                                    {{$size->weight}} g
                                </p>
                                <div class="size-price">
                                    {{$sizePrice}} {{$currencyCode}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    @if($item->size)
                        <?php
                        $defaultPrice=$this->calculatePriceWithCommission( $item->price,$delivaz_commission)
                        ?>
                        <div class="d-flex flex-row justify-content-start">
                            <h4 class="address mb-3">
                                @if($itemType == "res")
                                    {{__('serviceProvider.DefaultSizeFood')}}
                                @elseif($itemType == "ven")
                                    {{__('serviceProvider.DefaultSizeProduct')}}
                                @endif
                            </h4>
                        </div>
                        <div class="btn-size active">
                            <p class="size-name">
                                {{$item->size}}
                            </p>
                            <p class="size-weight">
                                {{$item->weight}} g
                            </p>
                            <div class="size-price">
                                {{$defaultPrice}} {{$currencyCode}}
                            </div>
                        </div>
                    @endif
                @endif

                <div class="d-flex flex-row justify-content-start mt-3">
                    <div class="d-flex flex-row justify-content-end btn-s-qtn">
                        <button class="btn btn-qtn @if($ProductQtn==1 or $disabledAdd) disable @endif"
                                wire:click='minusProductQuantity'>
                            <i class="fas fa-minus"></i></button>
                        <p class="address">{{$ProductQtn}}</p>
                        <button class="btn btn-qtn @if($disabledAdd) disable @endif"
                                wire:click="plusProductQuantity">
                            <i class="fas fa-plus"></i></button>
                    </div>

                    <h4 class="address">
                        {{__('serviceProvider.Price')}}
                    </h4>
                    <h4 class="address ml-3 mr-3">

                        {{$itemPrice*$ProductQtn}} {{$currencyCode}}
                    </h4>
                </div>


            </div>
        </div>


        @if(count($item->additions)!=0)

            <div class="Additional-Item">
                <div class="d-flex flex-row justify-content-start mb-3">
                    <h4 class="address">
                        {{__('serviceProvider.Additional_Item')}}
                    </h4>
                </div>

                <div class="d-flex flex-row justify-content-start flex-wrap food-addition-parent  width-full">
                    @foreach($item->additions as $key =>$addition)

                        <?php
                        $y = rand(100, 1000000);
                        $addition = $addition->toArray();
                        ?>
                        <livewire:template.category.product-addition-component
                            :addition="$addition"
                            :foldersForAddition="$foldersForAddition"
                            :currencyCode="$currencyCode"
                            :delivaz_commission="$delivaz_commission"
                            :key="'product-addition-component'.$y.$addition['id']"/>


                    @endforeach

                </div>
                @endif
                <div wire:loading wire:target="addToBasket">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-row justify-content-center mt-3">
                    @if($disabledAdd)
                        <div class="added-item-cart">

                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                      d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                                      fill="#FFFFFF"/>
                            </svg>
                            {{__('serviceProvider.basketAdded')}}
                        </div>

                    @else

                        <button class="btn btn-basket " wire:click.prevent="addToBasket" >
                            {{__('serviceProvider.Add_basket')}}
                        </button>
                    @endif

                </div>

                <livewire:template.category.product-related-component :type="$type"
                                                                      :item="$item"
                                                                      :serviceProvider="$serviceProvider"
                                                                      :country="$country"
                                                                      :city="$city"/>

            </div>
    </div>
</div>
