<div>
    <button type="button" class="btn btn-white" wire:click.prevent="addToWishlist">
        <i class="fas fa-heart @if(in_array($item['id'], $itemsID)) active @endif"></i>
    </button>
</div>
