<div class="d-flex flex-column justify-content-center align-items-center list">

    @foreach($shapeContent as $key=>$content)
        <?php
        $trans=null;
        $translation = $content->filter->translation;
        $transLang = \Illuminate\Support\Facades\App::getLocale();
        $EnTrans = $translation->where('code_lang', 'en')->first()->name;
        if($translation->where('code_lang', $transLang)->first())
            {
                $trans = $translation->where('code_lang', $transLang)->first()->name;
            }

        ?>
{{--        <div>--}}
{{--            <a class="btn btn-filter @if($key==0) active @endif" wire:click="$emitTo('template.category.service-providers', 'getServiceProviderByFilter','{{$content->filter->id}}')" >--}}
                <a class="btn btn-filter @if($key==0) active @endif" onclick="sendEmitEvent({{$content->filter->id}})" >

                @if($trans)
                    {{$trans}}
                @else
                    {{$EnTrans}}
                @endif
            </a>
{{--        </div>--}}
    @endforeach

        <script type="text/javascript">
            let now = moment();
            let nowDay = now.day();
            let nowHour=now.hour();
            let nowMinute=now.minute();
            // console.log("day",day);
            // console.log("hour",nowHour);
            // console.log("nowMinute",nowMinute);

            function sendEmitEvent(filterId)
            {
                let now = moment();
                let nowDay = now.day();
                let nowHour=now.hour();
                let nowMinute=now.minute();
                Livewire.emitTo('template.category.service-providers', 'setCurrentDay',nowDay,nowHour,nowMinute);
                Livewire.emitTo('template.category.service-providers', 'getServiceProviderByFilter',filterId);
            }
            window.onload = function() {
                Livewire.emitTo('template.category.service-providers', 'setCurrentDay',nowDay,nowHour,nowMinute);
                Livewire.emitTo('template.category.service-providers', 'getServiceProviderByFilter','{{$firstFilterID}}');
                Livewire.emitTo('template.category.service-providers', 'pageType');

            }
        </script>
</div>
