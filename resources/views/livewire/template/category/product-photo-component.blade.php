<div class="d-flex flex-column left-side">

{{--    @if(count($photos)!=0)--}}
        <div class=" @if (count($photos)==0) d-none @endif"
             x-data="{ image: 'imageD'}">
            <div class="img-container">
                <img class="img" src="{{asset($folders.$itemPhoto)}}"
                     x-show.transition.in.duration.1000ms="image === 'imageD'">
                @foreach($photos as $key =>$img)
                    <img class="img" src="{{$img}}"
                         x-show.transition.in.duration.1000ms="image === 'image{{$key}}'" x-cloak>
                @endforeach
            </div>
            <div class="d-flex flex-wrap justify-content-start small-imgs
                     @if(count($photos)>2) owl-carousel food-banners owl-theme @endIf">
                @foreach($photos as $key =>$img)

                    <div class="small-img item">
                        <a class="link" href="#" @click.prevent="image = 'image{{$key}}'">
                            <img src="{{$img}}">
                        </a>
                    </div>

                @endforeach
            </div>
        </div>

{{--    @else--}}
        <div class="d-flex flex-column @if (count($photos)!=0) d-none @endif ">
            <div class="img-container">
                <img class="img" src="{{asset($folders.$itemPhoto)}}">
            </div>
        </div>
{{--    @endif--}}

        <script>


            $(document).ready(function () {
                $('.food-banners').owlCarousel({
                    autoplay: true,
                    autoplayHoverPause: true,
                    smartSpeed: 250,
                    loop: true,
                    // margin:10,
                    center: true,
                    // // nav:true,
                    dots: true,

                    // items:1,
                    margin: 0,
                    stagePadding: 0,
                    // smartSpeed:450,
                    animateOut: 'fadeOut',
                    // autoWidth:true,
                    // animateOut: 'slideOutDown',
                    // animateIn: 'flipInX',
                    responsive: {
                        0: {
                            items: 2
                        },

                        1000: {
                            items: 3
                        }
                    }
                })
            });


        </script>

</div>
