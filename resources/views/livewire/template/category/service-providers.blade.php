<div style="width: 100%" class="d-flex flex-column justify-content-start align-items-start">
    <div wire:loading>
        <x-control-panel.loading/>
    </div>
    <div class="d-flex flex-row justify-content-start">
        <div>
            <h6 class="address">
                @if($pageType=="Restaurants")
                    {{$type}}
                @else
                    {{str_replace('-', ' ', $shape)}}

                @endif
            </h6>
        </div>
        <div class="mobil-version" wire:ignore>
            <button class="btn-menu-filter mt-1">
                <span class="close-icon"> <i class="fas fa-times"></i></span>
                <span class="open-icon show-on-mobil"><i style="" class="fa fa-bars" aria-hidden="true"></i></span>
            </button>
        </div>
    </div>

    <div style="width: 100%" class="d-flex flex-row justify-content-start flex-wrap service-provider-parent">

        @if($filterWithRecommended)

            @foreach($filterWithRecommended[0]['service_provider'] as $restaurant)
                <?php
                $obj = [];
                $HourStart = null;
                $MinuteStart = null;
                $HourEnd = null;
                $MinuteEnd = null;
                ?>
                <div class="service-provider ">

                    @if($filterWithRecommended[0]['type']==1)
                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">
                            @elseif($filterWithRecommended[0]['type']==2)
                                <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">

                                    @endif
                                    <div class="img">
                                        <img width="270" height="200" loading="lazy" class=""
                                             src="{{asset('/storage/images/avatars/'.$restaurant['avatar'])}}">
                                    </div>
                                </a>
                                <div class="d-flex flex-row justify-content-between content">
                                    @if($filterWithRecommended[0]['type']==1)
                                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">
                                            @else
                                                <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">

                                                    @endif
                                                    <h6 class="name">{{$restaurant['name']}}</h6>
                                                </a>
                                </div>
                                @foreach($restaurant['part_time'] as $day)
                                    <?php
                                    array_push($obj, $day['day_id']);
                                    if ($day['day_id'] == $currentDay) {
                                        $HourStart = $day['startTimeHour'];
                                        $MinuteStart = $day['startTimeMinutes'];
                                        $HourEnd = $day['endTimeHour'];
                                        $MinuteEnd = $day['endTimeMinutes'];
                                    }
                                    ?>
                                @endforeach
                                @if($restaurant['state']=="1")
                                    @if(in_array($currentDay,$obj))
                                        <?php

                                        $now = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $currentHour . ':' . $currentMinute);
                                        $begintime = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $HourStart . ':' . $MinuteStart);
                                        $endtime = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $HourEnd . ':' . $MinuteEnd);
                                        if($now >= $begintime && $now <= $endtime){

                                        ?>
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="open "> {{__('serviceProvider.Open')}}</p>
                                        </div>
                                        <?php
                                        } else {
                                        // not between times
                                        //                                echo "not";
                                        ?>
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                        </div>
                                        <?php
                                        }
                                        ?>

                                    @else
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                        </div>
                                    @endif
                                @elseif($restaurant['state']=="0")
                                    <div class="d-flex flex-row justify-content-between state ">
                                        <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                    </div>
                    @endif

                </div>
            @endforeach
        @endif

        @if($filterWithNotRecommended)

            @foreach($filterWithNotRecommended[0]['service_provider'] as $restaurant)
                <?php
                $objNot = [];
                $HourStartNot = null;
                $MinuteStartNot = null;
                $HourEndNot = null;
                $MinuteEnd = null;
                ?>
                <div class="service-provider ">
                    @if($filterWithRecommended[0]['type']==1)
                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">
                            @elseif($filterWithRecommended[0]['type']==2)
                                <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">

                                    @endif
                                    <div class="img">
                                        {{--                            {{dd($restaurant['avatar'])}}--}}
                                        @if($restaurant['avatar']==null)
                                            <?php $restaurant['avatar'] = "person.png"; ?>
                                        @endif
                                        <img width="270" height="200" loading="lazy" class=""
                                             src="{{asset('/storage/images/avatars/'.$restaurant['avatar'])}}">
                                    </div>
                                </a>
                                <div class="d-flex flex-row justify-content-between content">
                                    @if($filterWithRecommended[0]['type']==1)
                                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">
                                            @elseif($filterWithRecommended[0]['type']==2)
                                                <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant['slug']])}}">

                                                    @endif
                                                    <h6 class="name">{{$restaurant['name']}}</h6>
                                                </a>
                                        {{--                        <button type="button" class="btn btn-white">--}}
                                        {{--                            <i class="fas fa-heart active"></i>--}}
                                        {{--                        </button>--}}
                                </div>
                                {{--                        {{dd($restaurant['part_time'])}}--}}
                                @foreach($restaurant['part_time'] as $day)
                                    <?php
                                    array_push($objNot, $day['day_id']);
                                    if ($day['day_id'] == $currentDay) {
                                        $HourStartNot = $day['startTimeHour'];
                                        $MinuteStartNot = $day['startTimeMinutes'];
                                        $HourEndNot = $day['endTimeHour'];
                                        $MinuteEndNot = $day['endTimeMinutes'];
                                    }
                                    ?>
                                @endforeach
                                @if($restaurant['state']=="1")

                                    @if(in_array($currentDay,$objNot))
                                        <?php
                                        $nowNot = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $currentHour . ':' . $currentMinute);
                                        $begintimeNot = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $HourStartNot . ':' . $MinuteStartNot);
                                        $endtimeNot = new DateTime($year . '-' . $month . '-' . $day1 . ' ' . $HourEndNot . ':' . $MinuteEndNot);
                                        if($nowNot >= $begintimeNot && $nowNot <= $endtimeNot){
                                        ?>
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="open "> {{__('serviceProvider.Open')}}</p>
                                        </div>
                                        <?php
                                        } else {
                                        ?>
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                        </div>
                                        <?php
                                        }
                                        ?>
                                    @else
                                        <div class="d-flex flex-row justify-content-between state ">
                                            <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                        </div>
                                    @endif
                                @elseif($restaurant['state']=="0")
                                    <div class="d-flex flex-row justify-content-between state ">
                                        <p class="close "> {{__('serviceProvider.Closed')}}</p>
                                    </div>
                    @endif
                </div>
            @endforeach
        @endif

    </div>

</div>
