<div class="container info-section">
    <div class="d-flex flex-row justify-content-start p-relative container-child">
        <div class="left-side">

        </div>
        <div class="avatar">
            <img src="{{asset('storage/images/avatars/'.$photo)}}">
        </div>
        <div class="info gap-5">
            <div>
                <h2 class="address">
                    {{$serviceProvider->name}}
                </h2>
            </div>
            <div class="d-flex flex-row align-items-center align-content-center">
                {{--                <p class="close "> {{__('serviceProvider.Closed')}}</p>--}}
                @if(!$closedToday)
                    <div class="active-working "></div>

                    <p class="working m-1">

                        {{__('serviceProvider.Working_from')}}
                        {{$startTime}}
                        {{__('serviceProvider.To')}}

                        {{$endTime}}
                    </p>
                @else
                    <p class="close "> {{__('serviceProvider.ClosedToday')}}</p>

                @endif
            </div>
            <div>
                @if($address)
                    <p class="text1">
                        <i class="fa fa-map-marker" aria-hidden="true"></i> {{$address}}
                    </p>
                @else
                    <div style="width: 100% ;height: 50px">

                    </div>
                @endif
            </div>

        </div>
    </div>

    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    <script src="{{asset('js/moment-locales.js')}}"></script>
    <script type="text/javascript">
        let now = moment();
        let nowDay = now.day();
        let nowHour = now.hour();
        let nowMinute = now.minute();

        // console.log('now'+now);

        function sendCurrentDate() {
            // console.log('now'+now);
            Livewire.emit('setCurrentDay', nowDay, nowHour, nowMinute);

        }

        window.addEventListener('load', (event) => {
            // console.log('page is fully loaded');
            sendCurrentDate();
        });
    </script>
</div>
