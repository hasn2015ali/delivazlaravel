<div style="width: 100%" class="d-flex flex-column justify-content-start align-items-start">
    <div wire:loading >
        <x-control-panel.loading/>
    </div>
    <div class="d-flex flex-row justify-content-start">
{{--        <div>--}}
{{--            <h6 class="address">{{$firstFilter}}</h6>--}}
{{--        </div>--}}
        <div class="mobil-version" wire:ignore>
            <button class="btn-menu-filter mt-1">
                <span class="close-icon"> <i class="fas fa-times"></i></span>
                <span class="open-icon show-on-mobil"><i style="" class="fa fa-bars" aria-hidden="true"></i></span>
            </button>
        </div>


    </div>
    {{--    {{dd($productByFilters,$test,$filterType)}}--}}
{{--    @if(isset($itemsByFilters))--}}
{{--        <div class="d-flex flex-row justify-content-center width-full">--}}
{{--            <div class="items-pagination">--}}
{{--                {{ $itemsByFilters->links() }}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}
    <div class="d-flex flex-row justify-content-start flex-wrap service-provider-parent  width-full">
        @if(isset($itemsByFilters))
            @foreach($itemsByFilters as $itemByFilter)
                <div class="service-provider ">
                    <div>
                        @if($filterType == "res")
                            <a href="{{route('Food',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$this->serviceProvider->slug,'foodID'=>$itemByFilter->id])}}">
                                @elseif($filterType == "ven")
                                    <a href="{{route('Product',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$this->serviceProvider->slug,'productID'=>$itemByFilter->id])}}">

                                        @endif
                                        <div class="img">
                                            <img width="270" height="175" loading="lazy" class=" " src="{{asset($folders.$itemByFilter->photo)}}">
                                        </div>
                                    </a>
                                    <div class="d-flex flex-row justify-content-between content">
                                        @if($filterType == "res")
                                            <a href="{{route('Food',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$this->serviceProvider->slug,'foodID'=>$itemByFilter->id])}}">
                                                @elseif($filterType == "ven")
                                                    <a href="{{route('Product',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$this->serviceProvider->slug,'productID'=>$itemByFilter->id])}}">

                                                        @endif
                                                        <h6 class="name-item">{{$itemByFilter->translation->where('code_lang','en')->first()->name}}</h6>
                                                    </a>
                                                    <?php
                                                    $y=rand(100,1000000);
                                                    ?>
                                                    <livewire:template.category.add-to-wish-list-component
                                                        :filterType="$filterType"
                                                        :item="$itemByFilter"
                                                        :country="$country"
                                                        :city="$city"
                                                        :key="'add-to-wish-list-component'.$y.$itemByFilter->id"/>

                                    </div>

                                    <div class="d-flex flex-row justify-content-start content">
                                        <p class="content-item">
                                            {{ Illuminate\Support\Str::limit($itemByFilter->translation->where('code_lang','en')->first()->content, 50) }}
                                            {{--                                        {{ \Illuminate\Support\Str::of($itemByFilter->translation->where('code_lang','en')->first()->content)->words(10) }}--}}
                                        </p>

                                    </div>

                            <?php
                            $x=rand(1,10000);
                            ?>
                    </div>

                                <div>
{{--                                    {{$itemByFilter}}--}}
                                    <livewire:template.category.add-to-basket-component
                                        :serviceProvider="$serviceProvider"
                                        :filterType="$filterType"
                                        :delivazCommission="$delivazCommission"
                                        :item="$itemByFilter"
                                        :currencyCode="$currencyCode"
                                        :key="'add-to-basket-component'.$itemByFilter->id.$x"/>
                                    {{--                                 @livewire('template.category.add-to-basket-component',--}}
                                    {{--                                 ['filterType' => $filterType,'item'=>$itemByFilter,'currencyCode'=>$currencyCode])--}}
                                </div>
                </div>
            @endforeach
        @endif


    </div>

{{--    @if(isset($itemsByFilters))--}}
{{--        <div class="d-flex flex-row justify-content-center width-full">--}}
{{--            <div class="items-pagination">--}}
{{--                {{ $itemsByFilters->links() }}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

</div>
