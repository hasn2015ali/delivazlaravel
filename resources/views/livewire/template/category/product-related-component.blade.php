<div>
    <div class="Related-Item">
        <div class="d-flex flex-row justify-content-start mb-3">
            <h4 class="address">
                {{__('serviceProvider.Related_Item')}}
            </h4>
        </div>
        <div class="slider">
            <div class="owl-carousel related-item-banners owl-theme">
                @if($relatedItems)
                    @foreach($relatedItems as $banner)
                        @if($type == "food")
                            <a href="{{route('Food',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProvider,'foodID'=>$banner->id])}}">
                                @elseif($type == "product")
                                    <a href="{{route('Product',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProvider,'productID'=>$banner->id])}}">

                                        @endif
                                        <div class="item gap-10">
                                            <img src="{{asset($folders.$banner->photo)}}">
                                            <h4 class="address mt-3"> {{$banner->name}}</h4>
                                        </div>
                                    </a>
                            @endforeach
                        @endif
            </div>

            <div class="img-next">
                <img class="" src="{{asset('templateIMG/OWEL/left.png')}}">
            </div>

            <div class="img-prev">
                <img class="" src="{{asset('templateIMG/OWEL/rigth.png')}}">
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
            $('.related-item-banners').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: false,
                nav: true,
                navText: [$('.img-prev'), $('.img-next')],
                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    900: {
                        items: 2
                    },
                    1200: {
                        items: 3
                    }
                }
            })
        });
    </script>
    <style>
        .owl-carousel .item {
            /*display: block;*/
            /*width: calc(150px + 15vw);*/
            /*min-width: 250px;*/
        }

        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: 3vw !important;
            height: 0.8vh !important;
        }

        /*.card-header{*/
        /*    border-radius: 15px!important;*/

        /*}*/
        /*.owl-carousel img{*/

        /*        border-radius: 15px!important;*/


        /*}*/
    </style>
</div>
