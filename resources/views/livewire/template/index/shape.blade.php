<div class="shape-section mt-5">
    <div class="container">
        <div class="d-flex flex-column align-items-center justify-content-center shape-12 ">


            <div class="d-flex flex-row justify-content-center sex-shape gap-0-10">
                  <a href="#" wire:click.prevent="submit">
                      <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center ">
{{--                          <div class="css-sprite-1"></div>--}}
                          <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/1.png')}}">
                          <p class="text">{{__("home.Restaurants")}}</p>
                      </div>
                  </a>

                  <a href="#" wire:click.prevent="submit">
                      <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                          <div class="css-sprite-2"></div>--}}
                          <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/2.png')}}">
                          <p class="text">{{__("home.Coffee_Sweets")}}</p>
                      </div>
                  </a>
                  <a href="#" wire:click.prevent="submit">
                      <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                          <div class="css-sprite-3"></div>--}}
                          <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/3.png')}}">
                          <p class="text">{{__("home.Supermarkets")}}</p>

                      </div>
                  </a>
                   <a href="#" wire:click.prevent="submit">
                       <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                           <div class="css-sprite-4"></div>--}}
                           <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/4.png')}}">
                           <p class="text">{{__("home.meats")}}</p>

                       </div>
                   </a>
                   <a href="#" wire:click.prevent="submit">
                       <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                           <div class="css-sprite-5"></div>--}}
                           <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/5.png')}}">
                           <p class="text">{{__("home.Water")}}</p>

                       </div>
                   </a>
                   <a href="#" wire:click.prevent="submit">
                       <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                           <div class="css-sprite-6"></div>--}}
                           <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/6.png')}}">
                           <p class="text">{{__("home.Pharmacies")}}</p>
                       </div>
                   </a>


            </div>
            <div class="d-flex flex-row justify-content-center sex-shape  gap-0-10">
                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-7"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/7.png')}}">
                            <p class="text">{{__("home.Flowers_Shops")}}</p>
                        </div>
                    </a>

                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-8"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/8.png')}}">
                            <p class="text">{{__("home.Delivaz_Kids")}}</p>
                        </div>
                    </a>
                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-9"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/9.png')}}">
                            <p class="text">{{__("home.DigitalServices")}}</p>
                        </div>
                    </a>
                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-10"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/10.png')}}">
                            <p class="text">{{__("home.Discounts")}}</p>
                        </div>
                    </a>
                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-11"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/11.png')}}">
                            <p class="text">{{__("home.Delivaz_Boxes")}}</p>
                        </div>
                    </a>
                    <a href="#" wire:click.prevent="submit">
                        <div class="shape gap-shape d-flex flex-column align-items-center justify-content-center">
{{--                            <div class="css-sprite-12"></div>--}}
                            <img width="55" height="55" loading="lazy" src="{{asset('/templateIMG/shapes/12.png')}}">
                            <p class="text">{{__("home.Express_Delivery")}}</p>
                        </div>
                    </a>
                </div>
        </div>

    </div>
</div>
