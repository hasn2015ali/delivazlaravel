<div class="form-group gap-10 search-container" x-data="{ open: @entangle('showDropdown') }">
    <i class="fas fa-search"></i>
    <input type="text" class="form-control search"
           wire:model.debounce.1200ms="query"
           wire:keydown.escape="resetSearchResult"
           wire:keydown.Arrow-up="decrementHighlight"
           wire:keydown.Arrow-down="incrementHighlight"
           wire:keydown.enter="selectResult"/>

    <div wire:loading wire:target="query" class="search-loading">
        <h4 class="address">  {{__("home.Searching")}}</h4>
    </div>

    <div class="search-result" x-show="open" @click.away="$wire.closeAndResetSearchResult()" x-cloak>

        {{--        <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$vendor->serviceProvider->slug])}}">--}}

        {{--        </a>--}}

        <?php $counter = 0;  ?>
        @if(count($allResult)!=0)
            <div>
{{--                <a class="btn-link {{$highlightIndex===$highlightIndexForAllPage ? 'on-hover':''}}"--}}
{{--                   href="#">--}}
{{--                    {{__("home.SeeAllResult")}}--}}
{{--                </a>--}}
                @foreach($allResult as $k1=>$res)
                    <?php $counter = $k1;  ?>
{{--{{$highlightIndex}}{{$counter}}--}}
{{--                @if($counter===-1)--}}

{{--                    @endif--}}
                    @if($k1 < $this->x1)
                        @if($k1==$r)
                            <h4 class="address">
                                {{__("home.Restaurants1")}}
                            </h4>
                        @endif
                        <a class="btn-link {{$highlightIndex===$counter ? 'on-hover':''}}"
                           href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$res['country']['translation'][0]['name'],'city'=>$res['city']['translation'][0]['name'],'serviceProvider'=>$res['slug']])}}">
                            {{$res['name']}}
                        </a>
                    @endif

                        @if($k1 >= $this->x1 and $k1 < $this->x2)
                            @if($k1==$f)
                                <h4 class="address">
                                    {{__("home.Foods")}}
                                </h4>
                            @endif
                                <a class="btn-link {{$highlightIndex===$counter ? 'on-hover':''}}"
                                   href="{{route('Food',['locale'=>app()->getLocale(),
                                'country'=>$res['food']['service_provider']['country']['translation'][0]['name'],
                                'city'=>$res['food']['service_provider']['city']['translation'][0]['name'],
                                'serviceProvider'=>$res['food']['service_provider']['slug'],
                                'foodID'=>$res['food_id']])}}">
                                    {{$res['name']}}
                                </a>
                        @endif

                        @if($k1 >= $this->x2 and $k1 < $this->x3)
                            @if($k1==$s)
                                <h4 class="address">
                                    {{__("home.Shops")}}
                                </h4>
                            @endif

                            <a class="btn-link {{$highlightIndex===$counter ? 'on-hover':''}}"
                               href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$res['country']['translation'][0]['name'],'city'=>$res['city']['translation'][0]['name'],'serviceProvider'=>$res['slug']])}}">
                                {{$res['name']}}
                            </a>
                        @endif

                        @if($k1 >= $this->x3 and $k1 < $this->x4)
                            @if($k1==$p)
                                <h4 class="address">
                                    {{__("home.Products")}}
                                </h4>
                            @endif
                            <a class="btn-link {{$highlightIndex===$counter ? 'on-hover':''}}"
                               href="{{route('Product',['locale'=>app()->getLocale(),
                                'country'=>$res['product']['service_provider']['country']['translation'][0]['name'],
                                'city'=>$res['product']['service_provider']['city']['translation'][0]['name'],
                                'serviceProvider'=>$res['product']['service_provider']['slug'],
                                'productID'=>$res['product_id']])}}">
                                {{$res['name']}}
                            </a>
                        @endif
                @endforeach
            </div>
        @endif

        @if($length<3)

            <h4 class="text-small">  {{__("home.SearchingTypeMoreLetters")}}</h4>
        @else
            @if(count($restaurants)==0 and count($foods)==0 and count($shops)==0 and count($products)==0)
                <h4 class="address">
                    {{__("home.NoSearchResult")}}
                </h4>
            @endif
        @endif

        <script>
            let startingY = 0;
            let search_result=document.querySelector(".search-result");

            window.addEventListener('scroll-start', event => {
                // console.log('jjj');
                startingY = 0;
                search_result.scrollTo(0, startingY);
            })
            window.addEventListener('scroll-down', event => {
              // console.log('fff');
                let h = search_result.scrollHeight;
                let allResultCount=event.detail.count;
                let step=h / allResultCount * 0.90;
                // console.log("step",step);

                if(event.detail.direction=="down")
                {
                    startingY += step;
                    search_result.scrollTo(0, startingY);
                }

                if(event.detail.direction=="top")
                {
                    startingY = 0;

                    search_result.scrollTo(0, startingY);
                    // search_result.scrollTo(0, 0);

                }

            })

            window.addEventListener('scroll-up', event => {
                let h = search_result.scrollHeight;
                let allResultCount=event.detail.count;
                let step=h / allResultCount *1.25;
                if(event.detail.direction=="top")
                {
                    // console.log('after', startingY);
                    startingY -= step;
                    // console.log('before', startingY);

                    search_result.scrollTo(0, startingY);
                }

                if(event.detail.direction=="down")
                {
                     startingY = search_result.scrollHeight;
                    // console.log("height",height);
                    search_result.scrollTo(0, startingY);
                    // search_result.scrollTop = search_result.scrollHeight;
                }

            })

        </script>

    </div>
</div>
