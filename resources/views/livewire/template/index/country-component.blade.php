<div class="text-section">
    <div class="container">
        <div class="d-flex flex-row justify-content-center ">
            <div class="address">
                <h2 class="">{{__("home.Pick")}}&nbsp;{{__("home.your")}}&nbsp;{{__("home.Order")}}&nbsp;{{__("home.from")}}&nbsp;{{__("home.A")}}&nbsp;{{__("home.to")}}&nbsp;{{__("home.Z")}}</h2>
            </div>
        </div>
        <div class="d-flex flex-row justify-content-center mt-1">
            <h2 class="address2">{{__("home.choose_your_country_and_your_city")}}</h2>
        </div>

        <div class="d-flex justify-content-center mt-3">
            <div wire:loading wire:target="getCities">
                <x-control-panel.loading/>
            </div>
            <div class="dropdown gap-15 btn-group "
                 wire:loading.remove wire:target="getCities">
                <button  id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                         data-toggle="dropdown" data-display="static" aria-expanded="false">
                    @if(empty($countrySelected))
                        <small>    {{__("home.selectCountry")}}</small>
                        {{--                        {{$countries[0]->name}}--}}
                    @else
                        {{$countrySelected }}
                    @endif
                </button>
                <div class="dropdown-menu dropdown-menu-lg-right" id="countryDropdown">
                    <div class="input-container">
                        <input class="input-filter"
                               type="text" placeholder="{{__("home.search")}}" id="countryInput" onkeyup="filterCountryFunction()">
                    </div>
                    @foreach($countries as $item)
                        <button type="button" id="country-option" class="dropdown-item"
                          wire:click.prevent="getCities({{$item}})">{{$item->name}}
                        </button>
                    @endforeach

                </div>
            </div>


            <div class="dropdown gap-15" id="cities_list"
                 wire:loading.remove wire:target="getCities">
                <button id="dropdown_selecte_btn_city" type="button" class="btn dropdown-toggle"
                        data-toggle="dropdown" data-display="static" aria-expanded="false">
                    @if($countrySelected)
                        @if($checkedCity)
                            {{$checkedCity->name}}
                        @else
                            <small> {{__("home.selectCity")}}</small>
                        @endif

                    @else
                        <small> {{__("home.selectCity")}}</small>
                    @endif
                </button>
                <div class="dropdown-menu dropdown-menu-lg-right" id="cityDropdown">
                    <div class="input-container">
                        <input class="input-filter"

                               type="text" placeholder="{{__("home.search")}}" id="cityInput" onkeyup="filterCityFunction()">
                    </div>
                    @if(!empty($countrySelected))

                        @foreach($cities as $item)
{{--                            <a href="{{route('index',['locale'=>$locale,'country'=>$countryNameInURL,'city'=>])}}">--}}
                                <button type="button" class="dropdown-item"
                                   wire:click.prevent="goToSelectedCity({{$item}})"
                                >{{$item->name}}</button>
{{--                            </a>--}}
                        @endforeach
                    @else
                        <p class="dropdown-item"><small> {{__("home.selectCountryFirst")}}</small></p>
                    @endif
                </div>
            </div>

        </div>
    </div>


    <script>

        window.addEventListener('country-updated', event => {

            var app_url = "{{url('/')}}/{{app()->getLocale()}}/" + event.detail.country;
            window.history.pushState({ },"", app_url);
        })

    </script>
</div>

