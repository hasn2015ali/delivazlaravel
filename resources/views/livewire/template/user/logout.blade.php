<div>
    <div class="d-flex flex-row justify-content-start">
        <a href="{{ route('logout',['locale'=>app()->getLocale()]) }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
           class="btn btn-header">
            <i class="fas fa-sign-out-alt"></i>{{__("userFront.Logout")}}
        </a>

        <form id="logout-form" action="{{ route('logout',['locale'=>app()->getLocale()]) }}"
              method="POST">
            @csrf
        </form>
    </div>
</div>
