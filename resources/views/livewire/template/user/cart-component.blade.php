<div class="icon" x-data="{ isCart: false }">
    <a href="{{ route('My-Wishlist',['locale'=>app()->getLocale()]) }}">
        <button type="button" class="btn btn-white">
            <i class="fas fa-heart"></i><span
                class="badge badge-primary card-count badge-pill">{{ count($wishes)}}</span>
        </button>
    </a>

    <button type="button" class="btn btn-white" @click="isCart=!isCart">
        <i style="color: #757575;" class="fas fa-shopping-cart"></i><span
            class="badge badge-primary card-count badge-pill">{{count($cartContent['products'])}}</span>
    </button>
    <div class="user-cart"
         x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="isCart"
         @click.away="isCart=false" x-cloak>
        <div class="cart-header d-flex flex-row justify-content-between ">
            <div class="mt-1">
                <h4 class="address">{{__("userFront.CartDetails")}}</h4>
            </div>
            <div>
                <button class="btn btn-close" @click="isCart=false"><i class="fas fa-times "></i></button>
            </div>
        </div>
        <div class="cart-content">
            <div wire:loading wire:target="removeFromCart">
                <x-control-panel.loading/>
            </div>

            <div wire:loading wire:target="plusQuantity">
                <x-control-panel.loading/>
            </div>

            <div wire:loading wire:target="minusQuantity">
                <x-control-panel.loading/>
            </div>
            @if(count($cartContent['products']) > 0)
                @foreach($cartContent['products'] as $item)
                    <div class=" row mb-3">
                        <div class="col-3">
                            <div class="img">
                                @if($item['type']=="res")

                                    <img class=" " src="{{asset($foldersForFood.$item['photo'])}}">
                                @elseif($item['type']=="ven")
                                    <img class=" " src="{{asset($foldersForProduct.$item['photo'])}}">
                                @endif
                            </div>
                        </div>
                        <div class="col-5">

                            <div class="d-flex flex-column justify-content-start align-items-start">
                                <div class="address1">
                                    {{ $item['name'] }}
                                </div>
                                <div class="content">
                                    {{ $item['size'] }}
                                </div>
                                @if(isset($item['additions']['additions']))
                                    @if(count($item['additions']['additions'])!=0)

                                        @foreach($item['additions']['additions'] as $add)
                                            <div class="content">
                                                {{--                                                {{dd($add)}}--}}
                                                {{$add['quantity']}}
                                                <i class="fas fa-times "></i>
                                                {{ $add['name']}}
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                                <div class="content">
                                    {{ $item['quantity'] }}
                                    <i class="fas fa-times "></i> {{ $item['price'] }} {{$currencyCode}}
                                </div>


                            </div>
                        </div>
                        <div class="col-2">
                            <div class="d-flex flex-row justify-content-end btn-s-qtn">
                                <button class="btn btn-qtn @if( $item['quantity'] ==1) disable @endif"
                                        wire:click="minusQuantity('{{ $item['id'] }}')">
                                    <i class="fas fa-minus"></i></button>
                                {{--                    <p class="qtn"></p>--}}
                                <button class="btn btn-qtn" wire:click="plusQuantity('{{ $item['id'] }}')">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-2">

                            <div class="d-flex flex-row justify-content-end ">
                                {{--                    {{ $item['id'] }}--}}
                                <button class="btn btn-delete "
                                        wire:click.prevent="removeFromCart('{{ $item['id'] }}')"><i
                                        class="fas fa-times  "></i></button>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="d-flex flex-row justify-content-between ml-3 mr-3 mt-5">
                    <div>
                        <h4 class="address2">{{__("userFront.Subtotal")}}</h4>

                    </div>
                    <div>
                        <h4 class="address3"> {{$total}} {{$currencyCode}}</h4>

                    </div>

                </div>

                <div class="d-flex flex-row justify-content-center mt-3 mb-5">
                    <a href="{{route('user.checkout',['locale'=>app()->getLocale()])}}"  class="btn btn-check-out">
                        {{__("userFront.Checkout")}}
                    </a>
                </div>
            @else
                <div class="d-flex flex-row justify-content-center">
                    <h4 class="address2">{{__("userFront.CartEmpty")}}</h4>
                </div>
            @endif
        </div>

    </div>

</div>



