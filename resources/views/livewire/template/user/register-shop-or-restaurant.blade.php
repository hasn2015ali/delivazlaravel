<div class="register-service-provider-section">
    <div class="container">
        <form wire:submit.prevent="submit">
            <div class="register-service-provider">
                <div class="clip-path">

                </div>
                <div class="header">
                    <h4 class="address">   {{__('serviceProvider.RegisterNewOne')}}</h4>
                </div>
                <div class="content">
                    <div class="info">
                        <div class="owner-name">
                            <div class="dropdown ">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(empty($type))
                                        {{__("serviceProvider.SelectRestaurantOrShop")}}
                                    @else
                                        {{$type=="res"?__("serviceProvider.Restaurant"):__("serviceProvider.Shop") }}
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    <a href="#"
                                       wire:click.prevent="$set('type', 'res')">
                                        <p class="dropdown-item">{{__("serviceProvider.Restaurant")}}</p>
                                    </a>
                                    <a href="#"
                                       wire:click.prevent="$set('type', 'ven')">
                                        <p class="dropdown-item">{{__("serviceProvider.Shop")}}</p>
                                    </a>
                                </div>
                            </div>
                            @error('type')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="owner-name">
                            <div wire:loading wire:target="getCities">
                                <x-control-panel.loading/>
                            </div>
                            <div class="dropdown ">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(empty($countryName))
                                        {{__("serviceProvider.SelectCountry")}}
                                    @else
                                        {{$countryName }}
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    @foreach($countries as $item)
                                        <a href="#"
                                           wire:click.prevent="getCities({{ $item->country_id }},'{{$item->name}}')">
                                            <p class="dropdown-item">{{$item->name}}</p>
                                        </a>
                                    @endforeach
                                </div>
                            </div>

                            @error('selectedCountry')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                        <div class="owner-name">
                            <div class="dropdown " id="cities_list">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if($selectedCountry)
                                        @if($cityName)
                                            {{$cityName}}
                                        @else
                                            {{__("serviceProvider.SelectCity")}}
                                        @endif

                                    @else
                                        {{__("serviceProvider.SelectCity")}}
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    @if(!empty($selectedCountry))
                                        @foreach($cities as $item)
                                            <a href="#"
                                               wire:click.prevent="sendCityVal({{ $item->city->id }}, '{{ $item->name }}')">
                                                <p class="dropdown-item">{{$item->name}}</p>
                                            </a>
                                        @endforeach
                                    @else
                                        <p class="dropdown-item">
                                            <small> {{__("serviceProvider.selectCountryFirst")}}</small></p>
                                    @endif
                                </div>
                            </div>
                            @error('selectedCity')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <div class="info">
                        <div class="user-input">
                            <label>{{__('serviceProvider.DefaultName')}}</label>
                            <input class="form-control" wire:model.defer="DefaultName"/>
                            @error('DefaultName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input">
                            <label>{{__('serviceProvider.EnglishName')}}</label>
                            <input class="form-control" wire:model.defer="EnglishName"/>
                            @error('firstName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>


                    <div class="info">
                        <div class="user-input">
                            <label>{{__('serviceProvider.EnterFullAddress')}}</label>
                            <textarea wire:model.defer="address" class="form-control" rows="3"></textarea>
                            @error('address')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="user-input">

                            <div class="d-flex flex-row image-section">
                                <div class="d-flex flex-column ">
                                    <div wire:loading wire:target="photo">
                                        <x-control-panel.loading/>
                                    </div>
                                    <label>{{__('serviceProvider.AddImage')}}</label>
                                    <div class="input-file-container">
                                        <input type="file" class="form-control personal-image-input"
                                               wire:model.defer="photo"/>
                                        @error('photo')
                                        <div class="alert alert-danger2">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                        <div class="input-handel">
                                            <i class="fas fa-image fa-2x"></i>
                                            <p class="text1">{{__('serviceProvider.DragHere')}} <span
                                                    class="brows">{{__('serviceProvider.browse')}}</span> {{__('serviceProvider.toUpload')}}
                                            </p>
                                        </div>
                                    </div>

                                </div>
                                @if ($photo)
                                    <div class="img">

                                        <img src="  {{ $photo->temporaryUrl() }}">

                                    </div>
                                @endif
                            </div>

                        </div>
                    </div>

                    <div class="d-flex flex-row justify-content-center mt-3 mb-3">
                        <h4 class="address">   {{__('serviceProvider.OwnerInformation')}}</h4>
                    </div>

                    <div class="info">
                        <div class="owner-name">
                            <label>{{__('serviceProvider.FirstName')}}</label>
                            <input class="form-control" wire:model.defer="FirstName"/>
                            @error('FirstName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="owner-name">
                            <label>{{__('serviceProvider.LastName')}}</label>
                            <input class="form-control" wire:model.defer="LastName"/>
                            @error('LastName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                        <div class="owner-name">
                            <label>{{__('serviceProvider.FathersName')}}</label>
                            <input class="form-control" wire:model.defer="FathersName"/>
                            @error('FathersName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                    </div>


                    <div class="info">
                        <div class="user-input">
                            <label>{{__('serviceProvider.Phone')}}</label>
                            <input class="form-control" wire:model.defer="Phone"/>
                            @error('Phone')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input">
                            <label>{{__('serviceProvider.EnterEmail')}}</label>
                            <input class="form-control" wire:model.defer="Email"/>
                            @error('Email')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="info">
                        <div class="user-input">
                            <label>{{__('serviceProvider.NewPassword')}}</label>
                            <input type="password" class="form-control" wire:model.defer="Password"/>
                            @error('Password')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                        <div class="user-input">
                            <label>{{__('serviceProvider.PasswordConfirmation')}}</label>
                            <input type="password" class="form-control" wire:model.defer="PasswordConfirmation"/>
                            @error('PasswordConfirmation')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                </div>

            </div>

            <div class="d-flex flex-column justify-content-center align-items-center mb-5">
                <div class="submit">
                    <button type="submit" class="btn btn-primary-delivaz"> {{__('serviceProvider.Register')}}</button>
                </div>
                <div wire:loading wire:target="submit">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-row justify-content-center align-items-center">
                    <p class="text mb-0">{{__('userFront.Or')}}</p>
                </div>
                <div class="cancle">
                    <a href="{{route('login',['locale'=>app()->getLocale()])}}" class="btn btn-cancle-delivaz">
                        {{__('userFront.Sign_in')}}
                    </a>
                </div>
                <div class="d-flex flex-row justify-content-center align-items-center">
                    <p class="text mb-0">{{__('userFront.Or')}}</p>
                </div>
                <div class="submit">
                    <a href="{{route('register',['locale'=>app()->getLocale()])}}"
                       class="btn btn-primary-delivaz"> {{__('serviceProvider.RegisterAsPerson')}}</a>
                </div>
            </div>
        </form>
    </div>


</div>
