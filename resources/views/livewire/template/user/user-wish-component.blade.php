<tr>
    <td>

        @if($itemType== "res")
            <a href="{{route('Food',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProviderSlug,'foodID'=>$item->id])}}">
                @elseif($itemType== "ven")
                    <a href="{{route('Product',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProviderSlug,'productID'=>$item->id])}}">

                        @endif
                        <div class="img">
                            <img class=" " src="{{asset($folders.$item->photo)}}">
                        </div>
                    </a>
    </td>
    <td>
        @if($itemType== "res")
            <a href="{{route('Food',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProviderSlug, 'foodID'=>$item->id])}}">
                @elseif($itemType== "ven")
                    <a href="{{route('Product',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$serviceProviderSlug, 'productID'=>$item->id])}}">

                        @endif
                        <strong>
                            {{$item->translation->where('code_lang','en')->first()->name}}
                        </strong>
                        {{ \Illuminate\Support\Str::of($item->translation->where('code_lang','en')->first()->content)->words(3) }}

                        {{$item->translation->where('code_lang','en')->first()->content}}
                    </a>
    </td>
    <td>
        <p class="price">
            {{$priceWithCommission}} {{$wish->country->currency_code}}
        </p>
    </td>
    <td>
        <div wire:loading wire:target="addToBasket">
            <x-control-panel.loading/>
        </div>
        <a href="#" class="btn btn-add-basket
           @if(in_array($this->itemIdWithType,$this->itemIdsInCart)) btn-added-basket @endif"
           wire:loading.remove wire:target="addToBasket"
           wire:click.prevent="addToBasket()">

            @if(in_array($this->itemIdWithType,$this->itemIdsInCart))
                <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                          fill="#FFFFFF"/>
                </svg>

                {{__("userFront.Added_basket")}}

            @else
                <i class="fas fa-shopping-cart ml-1 mr-1"></i>
                {{__("userFront.Add_basket")}}

            @endif
        </a>
    </td>
    <td>
        <div wire:loading wire:target="removeWish">
            <x-control-panel.loading/>
        </div>

        <a href="#" class="btn btn-delete"
           wire:loading.remove wire:target="removeWish"
           wire:click.prevent="removeWish()">
            <i class="fa fa-trash-o ml-1 mr-1"></i> {{__("userFront.Delete")}}
        </a>

    </td>

</tr>
