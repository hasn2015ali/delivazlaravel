<div class="user-payment-method d-flex flex-column justify-content-start align-items-start">
    <div>
        <h4 class="address">{{__("userFront.Delivaz_Wallet")}}</h4>
    </div>
    <div class="balance">
        <p class="content"><i class="fas fa-wallet"></i>&nbsp;&nbsp; $40.43 Balance</p>
    </div>

    <div class=" d-flex flex-column justify-content-start align-items-start" x-data="{ toggle1: false }">
        {{-- In work, do what you enjoy. --}}
        <div style="width: 100%"
             x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="!toggle1">
            <div>
                <h4 class="address">{{__("userFront.new_Payment")}}</h4>
            </div>
            <div>
                <h4 class="address3">{{__("userFront.Credit_or_debit_card")}}</h4>
            </div>
            <div class="d-flex flex-row justify-content-between parent">
                <div class="right-side">
                    <p class="content">
                        {{__("userFront.Delivaz_accepts")}}
                    </p>
                </div>

                <div class="d-flex flex-row justify-content-start left-side">
                    <div class="gap-10">
                        <i style="color: #1A1F71;" class="fab fa-cc-visa"></i>
                    </div>
                    <div class="gap-10">
                        <i style="color: #EB001B;" class="fab fa-cc-mastercard"></i>
                    </div>
                    <div class="gap-10">
                        <i style="color: #31B1F0;" class="fa fa-cc-paypal" aria-hidden="true"></i>
                    </div>
                </div>


            </div>

            <div>
                <button class="btn btn-add" @click="toggle1 = !toggle1">{{__("userFront.Debit_or_Credit")}}</button>
            </div>
        </div>
        <div style="width: 100%"
             x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="toggle1"
             x-cloak>

            <div class="d-flex flex-row justify-content-start set-margin">
                {{--            <h4 class="address2 ">{{__("userFront.AddPaymentCard")}}</h4>--}}
                <div>
                    <h4 class="address ">{{__("userFront.AddPaymentCard")}}</h4>
                </div>
                <div>
                    <button type="button" class="btn btn-Cancel ml-3 mr-3 " @click="toggle1 = !toggle1">
                        {{__("userFront.Cancel")}}
                    </button>
                </div>
            </div>


            <div class="d-flex flex-row justify-content-between parent">
                <div class="d-flex flex-column justify-content-start align-items-start pay-card right-side">
                    <form>
                        <div class="form-group d-flex flex-row justify-content-start user-input2">
                            <input class="form-control" placeholder="{{__("userFront.NameofCard")}}"/>
                        </div>

                        <div class="form-group d-flex flex-row justify-content-start user-input2">
                            <input class="form-control" placeholder="{{__("userFront.CardNumber")}}"/>
                        </div>

                        <div class="d-flex flex-row justify-content-start user-input2 form-group datepicker-container">
                            <div class="form-group d-flex flex-row justify-content-start input-group date datepicker"
                                 id='table'>
                                <input class="form-control date-input" placeholder="{{__("userFront.MonthYear")}}"/>
                                <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar">

                         </span>
                    </span>
                            </div>
                            <div class="form-group d-flex flex-row justify-content-start second-child ">
                                <input class="form-control" placeholder="{{__("userFront.CVC")}}"/>
                            </div>

                        </div>

                        <div class="form-group d-flex flex-row justify-content-start handel-select ">
                            {{--                        <i class="fas fa-flag"></i>--}}
                            <select class="form-control  toggle-enabl">
                                <option value="off" hidden>{{__("userFront.Country")}}</option>
                            </select>
                        </div>


                    </form>
                </div>

                <div class="d-flex flex-column left-side">
                    <div>
                        <p class="content">
                            {{__("userFront.Delivaz_accepts")}}
                        </p>
                    </div>
                    <div class="d-flex flex-row">
                        <div class="gap-10">
                            <i style="color: #1A1F71;" class="fab fa-cc-visa"></i>
                        </div>
                        <div class="gap-10">
                            <i style="color: #EB001B;" class="fab fa-cc-mastercard"></i>
                        </div>
                        <div class="gap-10">
                            <i style="color: #31B1F0;" class="fa fa-cc-paypal" aria-hidden="true"></i>
                        </div>
                    </div>

                </div>
            </div>

            <label class="checkbox-container">{{__("userFront.default_payment")}}
                <input type="checkbox">
                <span class="checkmark"></span>
            </label>


            <div style="width: 100%" class="form-group d-flex flex-row justify-content-center">
                <button type="submit" class="btn btn-save">{{__("userFront.Add_Card")}}</button>
            </div>
        </div>

    </div>

</div>
