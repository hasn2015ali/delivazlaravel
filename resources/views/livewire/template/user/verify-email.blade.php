<div>
    <div class="register-section section-verification ">
        <div class="container ">
            <div class="d-flex flex-column justify-content-center align-items-center">
                <div  wire:loading wire:target="verifyEmail">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="sendEmailAnotherVerifyCode">
                    <x-control-panel.loading />
                </div>

                @error('code')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @if(session('status'))
                    <div class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>
                            {{ session('status') }}
                        </strong>
                    </div>

                @endif

                @if(session('verification_status'))
                    <div class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        <strong>
                            {{ session('verification_status') }}
                        </strong>
                    </div>
                    {{session()->forget(['verification_status'])}}
                @endif
            </div>
            <div class="d-flex flex-row justify-content-center align-content-center">
                {{--                <div class="alert alert-info">--}}
                {{--                    {{__("userFront.verifyEmailMessage")}}--}}
                {{--                </div>--}}
            </div>
            <div class="d-flex flex-row justify-content-center align-content-center">
                <form wire:submit.prevent="SubmitVerifyEmail">
                    @csrf
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <input class="form-control" wire:model.defer="code" placeholder="{{__("userFront.EnterEmailVerification")}}">
                    </div>


                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                        <button type="submit" class="btn btn-sing-up">
                            {{__('userFront.emailVerifyCode')}}
                        </button>
                    </div>

                </form>
            </div>

            <div class="d-flex flex-row justify-content-center align-items-center mt-2">

                <form wire:submit.prevent="sendEmailAnotherVerifyCode">
                    @csrf

                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                        <button type="submit" class="btn btn-sing-up">
                            {{__('userFront.SendAnotherVerificationCode')}}
                        </button>
                    </div>
                </form>
            </div>

        </div>
        {{--   <div class="container">--}}
        <div class="cover mt-5">
            <img src="{{asset('/templateIMG/register.png')}}">
        </div>
        {{--   </div>--}}

    </div>
</div>
<style>
    .section-verification .form-group{
        width: calc(100px + 25vw)!important;
    }
    .section-verification{
        /*margin-top: calc(30px + 4vw);*/
        margin-bottom: calc(30px + 4vw);
    }
    .btn-outline-warning:hover{
        color: white!important;
        transform: translateY(-3px);
        transition: transform 1000ms;

    }
    .section-verification .cover{
        width: 100%;
    }
    .section-verification .cover img{
        width: 100%;
    }
</style>
