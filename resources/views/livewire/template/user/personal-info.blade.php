<div class="d-flex flex-column user-info">
    <div x-data="{isShowing: @entangle('isShowing') }">
        <div class="d-flex flex-row justify-content-start">
            <div>
                <h4 class="address">{{__("userFront.PersonalInformation")}}</h4>
            </div>

            <div>
                {{--            btn-edit--}}
                <button :class="{ 'btn-edit': !isShowing ,'btn-Cancel': isShowing}" class="btn btn-edit-Cancel "
                        wire:click="toggleIsShowing()">
                    <i class="fa fa-edit " aria-hidden="true" x-show="!isShowing"></i>
                    <span x-show="!isShowing"> {{__("userFront.Edit")}}</span>
                    <span x-show="isShowing" x-cloak> {{__("userFront.Cancel")}}</span>
                </button>
            </div>
        </div>
        <div :class="{ 'd-flex flex-column': !isShowing }" class=""
             x-show.transition.in.duration.800ms.origin.center.center.scale.120.out.duration.10ms.origin.bottom.left.scale.95="!isShowing">
            @if(session()->has('emailMessage'))
                <div style="width: 90%" class="alert alert-info">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    {{ session('emailMessage') }}
                    <a class="dropdown-item" href="{{ route('Verify-My-Email',['locale'=>app()->getLocale()]) }}">
                        <strong>{{__("userFront.verifyMyEmail")}}</strong>
                    </a>
                </div>
                {{session()->forget(['emailMessage'])}}
            @endif
            @if(session()->has('mobileMessage'))
                <div style="width: 90%" class="alert alert-info">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    {{ session('mobileMessage') }}
                    <a class="dropdown-item" href="{{ route('Verify-My-Phone',['locale'=>app()->getLocale()]) }}">
                        <strong> {{__("userFront.PhoneMyVerification")}} </strong>
                    </a>
                </div>
                {{session()->forget(['mobileMessage'])}}
            @endif
            <div class="d-flex flex-row justify-content-start">
                <div class="user-title">
                    <h6 class="content">{{__("userFront.Name")}}</h6>
                </div>

                <div>
                    <p class="content">{{$user->firstName}} &nbsp; {{$user->fatherName}} &nbsp; {{$user->lastName}}</p>
                </div>
            </div>
            @if($user->email)
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.Email1")}}</h6>
                    </div>

                    <div>
                        <p class="content">{{$user->email}}</p>
                    </div>
                </div>
            @endif
            @if($user->phone)
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.phone1")}}</h6>
                    </div>

                    <div>
                        <p class="content">
{{--                            +{{$countryCodeNumber.$user->phone}}--}}
                            {{$phoneNumberFormatedByRegion}}
                        </p>
                    </div>
                </div>
            @endif
            @if($user->address)
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.Address")}}</h6>
                    </div>

                    <div>
                        <p class="content">{{$user->address}}</p>
                    </div>
                </div>
            @endif
        </div>

        <div :class="{ 'd-flex flex-column': isShowing }"
             x-show.transition.in.duration.800ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="isShowing"
             x-cloak>
            <div wire:loading wire:target="updateInfo">
                <x-control-panel.loading/>
            </div>
            <form wire:submit.prevent="updateInfo">
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.FirstName1")}}</h6>
                    </div>

                    <div class="user-input">
                        <input class="form-control" wire:model.defer="firstName"/>
                        @error('firstName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.FatherName")}}</h6>
                    </div>

                    <div class="user-input">
                        <input class="form-control" wire:model.defer="fatherName"/>
                        @error('fatherName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.LastName1")}}</h6>
                    </div>

                    <div class="user-input">
                        <input class="form-control" wire:model.defer="lastName"/>
                        @error('lastName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.Email1")}}</h6>
                    </div>

                    <div class="user-input">
                        <input class="form-control" wire:model.defer="email" type="email"/>
                        @error('email')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.phone1")}}</h6>
                    </div>

                    <div class="user-input">

                        <div class="d-flex flex-row justify-content-start phone-input">
                            <div class="country-code">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        +{{$countryCodeNumber}}
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach($allCountriesCodes as $code)
{{--                                            {{dd($code->code_number)}}--}}
                                        <a class="dropdown-item" wire:click.prevent="$set('countryCodeNumber','{{$code['code_number']}}')" href="#">+{{$code['code_number']}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <input class="form-control" wire:model.lazy="phone1" type="number"/>
                        </div>
                        @error('phone1')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-start">
                    <div class="user-title">
                        <h6 class="content">{{__("userFront.Address")}}</h6>
                    </div>

                    <div class="user-input">
                        <textarea wire:model.defer="address" class="form-control" rows="2"></textarea>
                        @error('address')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex flex-row justify-content-center align-items-center mt-4">
                    <button type="submit" class="btn btn-save">
{{--                        <i class="fa fa-edit " aria-hidden="true"></i> --}}
                        {{__("userFront.SaveChanges")}}
                    </button>
                </div>

            </form>

        </div>
    </div>
    <livewire:template.user.change-password-component/>
</div>
