<div>
    <div class="register-section ">
        <div class="container">
            <div class="d-flex flex-column justify-content-center align-items-center">
                <div  wire:loading wire:target="submitIdentification">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="sendAnotherCode">
                    <x-control-panel.loading />
                </div>
                @error('email')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @error('code')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @error('password')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror
                @error('passwordConfirmation')
                <div class="alert alert-danger">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>{{ $message }}</strong>
                </div>
                @enderror

                @if(session()->has('resetPasswordState'))
                    <div style="width: 90%" class="alert alert-info">
                        <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                        {{ session('resetPasswordState') }}

                    </div>
                    {{session()->forget(['resetPasswordState'])}}
                @endif

            </div>
            <div class="d-flex flex-row justify-content-center">
                @if($identificationShow)
                <form wire:submit.prevent="submitIdentification">
                    {{--                        <form method="POST" action="{{route('password.email',['locale'=>app()->getLocale()]) }}">--}}
                    @csrf
                    <div class="form-group d-flex flex-row justify-content-start align-items-center">
                        <i class="fas fa-mobile-alt"></i>  <i class="far fa-envelope ml-1 mr-1"></i> <input class="form-control"  wire:model.defer="email" placeholder="{{__("userFront.Email")}}">
                    </div>


                    <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                        <button type="submit" class="btn btn-sing-up">
                            {{__('userFront.submit')}}
                        </button>
                    </div>
                </form>
                    @endif

                    @if($codShow)
                      <div class="d-flex flex-column justify-content-center align-items-center">
                          <div class="d-flex flex-row justify-content-center">
                              <form wire:submit.prevent="submitCode">
                                  {{--                        <form method="POST" action="{{route('password.email',['locale'=>app()->getLocale()]) }}">--}}
                                  @csrf
                                  <div class="form-group d-flex flex-row justify-content-start align-items-center">
                                      <i class="fas fa-lock"></i> <input class="form-control"  wire:model.defer="code" placeholder="{{__("userFront.Enter_Verification_Code")}}">
                                  </div>


                                  <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                                      <button type="submit" class="btn btn-sing-up">
                                          {{__('userFront.verifyCode')}}
                                      </button>
                                  </div>
                              </form>
                          </div>
                          <div class="d-flex flex-row justify-content-center">
                              <form wire:submit.prevent="sendAnotherCode">
                                  {{--                        <form method="POST" action="{{route('password.email',['locale'=>app()->getLocale()]) }}">--}}
                                  @csrf
                                  <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                                      <button type="submit" class="btn btn-sing-up">
                                          {{__('userFront.sendAnotherCode')}}
                                      </button>
                                  </div>
                              </form>
                          </div>


                      </div>
                    @endif
                    @if($newPasswordShow)
                        <form wire:submit.prevent="submitNewPassword">
                            {{--                        <form method="POST" action="{{route('password.email',['locale'=>app()->getLocale()]) }}">--}}
                            @csrf
                            <div class="form-group d-flex flex-row justify-content-start align-items-center">
                                <i class="fas fa-lock"></i> <input class="form-control"  type="password" wire:model.defer="password" placeholder="{{__("userFront.Password")}}">
                            </div>
                            <div class="form-group d-flex flex-row justify-content-start align-items-center">
                                <i class="fas fa-lock"></i> <input class="form-control" type="password"  wire:model.defer="passwordConfirmation" placeholder="{{__("userFront.password_confirmation")}}">
                            </div>


                            <div class="form-group d-flex flex-row justify-content-center align-items-center mt-2">
                                <button type="submit" class="btn btn-sing-up">
                                    {{__('userFront.RestPassword')}}
                                </button>
                            </div>
                        </form>
                    @endif

            </div>

        </div>

        <div class="cover">
            <img src="{{asset('/templateIMG/register.png')}}">
        </div>
    </div>
</div>
