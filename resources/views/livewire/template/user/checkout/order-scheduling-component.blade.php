<div class="scheduling-section-container">

    <div class="scheduling-option active-option">
        <div class="price">
            {{__('checkout.Free')}}
        </div>
        <div class="address">
            {{__('checkout.door')}}
        </div>
        <div class="content">
            {{__('checkout.doorDes')}}
        </div>
    </div>

    <div class="scheduling-option">
        <div class="price">
            $20
        </div>
        <div class="address">
            {{__('checkout.Fast')}}
        </div>
        <div class="content">
            {{__('checkout.FastDes')}}
        </div>
    </div>


    <div class="scheduling-option">
        <div class="price">
            $20
        </div>
        <div class="address">
            {{__('checkout.ScheduleOrder')}}
        </div>
        <div class="content">
            {{__('checkout.ScheduleOrderDes')}}
        </div>


    </div>

{{--    <div class="scheduling-setting">--}}
{{--        <div class="address">--}}
{{--            {{__('checkout.ScheduleSetting')}}--}}
{{--        </div>--}}
{{--        <div class="my-dropdown-container" x-data="{ dropDownMenu: false }">--}}
{{--            <button class="btn btn-option-delivaz dropdown-toggle" @click="dropDownMenu=!dropDownMenu">--}}
{{--                {{__('checkout.ScheduleType')}}--}}

{{--            </button>--}}
{{--            <div class="my-dropdown-menu"--}}
{{--                 x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="dropDownMenu"--}}
{{--                 @click.away="dropDownMenu=false" x-cloak>--}}
{{--                <div class="item-container" wire:click.prevent="goToAddNewAddress">--}}
{{--                    <div class="dropdown-item">--}}
{{--                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                            <path d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986" stroke="#209F84" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                            <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                        </svg>--}}
{{--                        {{__("checkout.OneTime")}}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="item-container" wire:click.prevent="deleteCall()">--}}
{{--                    <div class="dropdown-item">--}}
{{--                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">--}}
{{--                            <path d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986" stroke="#209F84" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                            <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>--}}
{{--                        </svg>--}}
{{--                        {{__("checkout.MoreThanOneTime")}}--}}
{{--                    </div>--}}

{{--                </div>--}}



{{--            </div>--}}
{{--            <div class="user-input datepicker-container2 ">--}}
{{--                <label>{{__('checkout.SelectDate')}}</label>--}}
{{--                <div class="form-group d-flex flex-row justify-content-start input-group date datepicker"--}}
{{--                    >--}}
{{--                    <input id="date-filter-day" type="text" class="form-control filter-value datepicker-field" data-filter-value="day">--}}
{{--                    <span class="input-group-addon">--}}
{{--                                     <span class="glyphicon glyphicon-calendar">--}}

{{--                                     </span>--}}
{{--                                 </span>--}}
{{--                </div>--}}
{{--                @error('DefaultName')--}}
{{--                <div class="alert alert-danger2">--}}
{{--                    <button type="button" class="close" data-dismiss="alert">&times;</button>--}}
{{--                    {{ $message }}</div>--}}
{{--                @enderror--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
