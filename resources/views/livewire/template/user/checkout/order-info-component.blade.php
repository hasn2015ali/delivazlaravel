<div class="">
    <h2 class="address">  {{__("checkout.OrderInfo")}} </h2>

    <div class="list2">
        <div class="item">
            <div class="content">
                {{__("checkout.Subtotal")}}
            </div>
            <div class="price">
                {{$currency_code}}  {{number_format($totalForItemCart, 2, '.', ',')}}
            </div>
        </div>

        <div class="item">
            <div class="content">
                {{__("checkout.DeliveryCharge")}}
            </div>
            <div class="price">
                {{$currency_code}}  {{number_format($deliveryCharge, 2, '.', ',')}}
            </div>
        </div>

        @if($showCashCost)
            <div class="item">
                <div class="content">
                    {{__("checkout.Cash")}}
                </div>
                <div class="price">
                    {{$currency_code}}  {{number_format($cashCost, 2, '.', ',')}}
                </div>
            </div>
        @endif
        @if($showAtDoorCost)
            <div class="item">
                <div class="content">
                    {{__("checkout.door")}}
                </div>
                <div class="price">
                    {{$currency_code}}  {{number_format($atDoorCost, 2, '.', ',')}}
                </div>
            </div>
        @endif
        @if($showASAPCost)
            <div class="item">
                <div class="content">
                    {{__("checkout.ASAP")}}
                </div>
                <div class="price">
                    {{$currency_code}}  {{number_format($asapCost, 2, '.', ',')}}
                </div>
            </div>
        @endif
        @if($showScheduleCost)
            <div class="item">
                <div class="content">
                    {{__("checkout.ScheduleOrder")}}
                </div>
                <div class="price">
                    {{$currency_code}}  {{number_format($scheduleCost, 2, '.', ',')}}
                </div>
            </div>
        @endif

        {{--        <div class="item">--}}
        {{--            <div class="content">--}}
        {{--                {{__("checkout.Tax")}}--}}
        {{--            </div>--}}
        {{--            <div class="price">--}}
        {{--                $4.00--}}
        {{--            </div>--}}
        {{--        </div>--}}

    </div>
    <hr/>
    <div class="list3">
        <div class="address2">
            {{__("checkout.OrderTotal")}}
        </div>
        <div class="price">
            {{--            $totalOrder--}}
            {{number_format($totalOrder, 2, '.', ',')}} {{$currency_code}}
        </div>
    </div>
    @if($step=="payment")
        <hr/>
        @if( isset($storedAddress['type']) and  $storedAddress['type']=="me")
            <h2 class="address"> {{$storedAddress['firstName']}} {{$storedAddress['lastName']}} </h2>
            <h2 class="address">
                {{$storedAddress['buildingName']}}
                {{$storedAddress['addressOnMap']}}
            </h2>
            <h2 class="address"> {{$storedAddress['mobileNumber']}} </h2>
        @endif
        @if( isset($storedAddress['type']) and $storedAddress['type']=="to")
            <h2 class="address"> {{$storedAddress['firstNameRecipient']}} {{$storedAddress['lastNameRecipient']}} </h2>
            <h2 class="address">
                {{$storedAddress['buildingNameRecipient']}}
                {{$storedAddress['addressOnMap']}}
            </h2>
            <h2 class="address"> {{$storedAddress['mobileNumberRecipient']}} </h2>
        @endif

{{--        <div class="d-flex justify-content-center w-100 btn-container mt-3">--}}
{{--            <div wire:loading wire:target="placeOrder">--}}
{{--                <x-control-panel.loading/>--}}
{{--            </div>--}}
{{--            <button class="btn btn-primary-gradient"--}}
{{--                    wire:loading.remove wire:target="placeOrder"--}}
{{--                    type="submit">--}}
{{--                {{__("checkout.PLACEORDER")}}--}}
{{--            </button>--}}
{{--        </div>--}}
    @elseif($step=="orderSummery")
        <div class="d-flex justify-content-center w-100 btn-container mt-5">
            @if(isset($countryName) and  isset($cityName))
            <a href="{{route('index',['locale'=>$locale,'country'=>$countryName,'city'=>$cityName])}}" class="btn btn-primary-gradient">
                {{__("checkout.ContinueShopping")}}
            </a>
            @endif
        </div>
    @endif

</div>
