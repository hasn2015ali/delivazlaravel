{{--<div class="d-flex flex-row">--}}
    @if($type=="me")
    <div class="user-address">
        <div class="address-container">
            <div class="header">
                {{__("checkout.PrimaryAddress")}}
                {{$address['addressName']}}
            </div>
            <div class="content">
                <div class="address-type ">
                    <h2 wire:loading.remove wire:target="editUserAddress">
                        {{$address['addressName']}}
                    </h2>
                    <div wire:loading wire:target="editUserAddress">
                        <x-control-panel.loading/>
                    </div>
                    <div class="my-dropdown-container"
                         x-data="{ dropDownMenu : false, openDeleteToast : false }">
                        @php
                            $y=rand(100,1000000);
                        @endphp
                        <livewire:template.action.delete-component
                            :itemID="$address['id']"
                            modelName="App\Models\OrderAddress"
                            :key="'delete-address-component'.$y.$address['id']"/>
                        <button class="btn btn-option-white" @click="dropDownMenu=!dropDownMenu">
                            <svg width="30" height="8" viewBox="0 0 30 8" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <circle cx="4" cy="4" r="4" fill="#FAC24F"/>
                                <circle cx="15" cy="4" r="4" fill="#FAC24F"/>
                                <circle cx="26" cy="4" r="4" fill="#FAC24F"/>
                            </svg>
                        </button>
                        <div class="my-dropdown-menu"
                             x-show.transition.in.duration.300ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="dropDownMenu"
                             @click.away="dropDownMenu=false"
                             x-on:close-drop-down.window="dropDownMenu = false"
                             x-cloak>
                            <div class="item-container"
                                 wire:click.prevent="editUserAddress({{$address['id']}},'me')">
                                <div class="dropdown-item">
                                    {{__("checkout.Edit")}}
                                </div>
                            </div>
                            <div class="item-container" @click="openDeleteToast=true">
                                <div class="dropdown-item">
                                    {{__("checkout.Remove")}}
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

                <div class="detail">
                    <div class="left">
                        <h6> {{__("checkout.Name")}}</h6>
                    </div>
                    <div class="right">
                        <p>  {{$address['firstName']}} {{$address['lastName']}}</p>
                    </div>
                </div>

                <div class="detail">
                    <div class="left">
                        <h6> {{__("checkout.Address")}}</h6>
                    </div>
                    <div class="right">
                        <p>
                            {{$address['buildingName']}}
                            {{$address['addressOnMap']}}
                        </p>
                    </div>
                </div>

                <div class="detail">
                    <div class="left">
                        <h6> {{__("checkout.PhoneNumber")}}</h6>
                    </div>
                    <div class="right">
                        <p>{{$address['mobileNumber']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div wire:loading wire:target="setAddressSelect">
                <x-control-panel.loading/>
            </div>
            <a class="select-address @if($address['id']."me"==$activeAddress) active-address @endif"
               wire:loading.remove wire:target="setAddressSelect"
               wire:click.prevent="setAddressSelect('me',{{json_encode($address)}})">
                @if($address['id']."me"==$activeAddress)
                    {{__("checkout.AddressSelected")}}
                    <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                              d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                              fill="#FFFFFF"/>
                    </svg>
                @else
                    {{__("checkout.SelectAddress")}}
                @endif
            </a>
        </div>
    </div>
    @elseif($type=="to")
        <div class="user-address">
            <div class="address-container">
                <div class="header">
                    {{__("checkout.PrimaryAddress")}}
                    {{$address['addressName']}}

                </div>

                <div class="content">
                    <div class="address-type ">
                        <h2
                            wire:loading.remove wire:target="editUserAddress">
                            {{$address['addressName']}}
                        </h2>
                        <div wire:loading wire:target="editUserAddress">
                            <x-control-panel.loading/>
                        </div>
                        <div class="my-dropdown-container" x-data="{ dropDownMenu: false ,openDeleteToast: false}">
                            @php
                                $y=rand(100,1000000);

                            @endphp
                            <livewire:template.action.delete-component
                                :itemID="$address['id']"
                                modelName="App\Models\OrderToAddress"
                                :key="'delete-address-component'.$y.$address['id']"/>

                            @include('livewire.modal.template.controlPanel.deleteWindow')
                            <button class="btn btn-option-white" @click="dropDownMenu=!dropDownMenu">
                                <svg width="30" height="8" viewBox="0 0 30 8" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="4" cy="4" r="4" fill="#FAC24F"/>
                                    <circle cx="15" cy="4" r="4" fill="#FAC24F"/>
                                    <circle cx="26" cy="4" r="4" fill="#FAC24F"/>
                                </svg>
                            </button>
                            <div class="my-dropdown-menu"
                                 x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="dropDownMenu"
                                 @click.away="dropDownMenu=false"
                                 x-on:close-drop-down.window="dropDownMenu = false"
                                 x-cloak>
                                <div class="item-container"
                                     wire:click.prevent="editUserAddress({{$address['id']}},'to')">
                                    <div class="dropdown-item">
                                        {{__("checkout.Edit")}}
                                    </div>
                                </div>
                                <div class="item-container" @click="openDeleteToast=true">
                                    <div class="dropdown-item">
                                        {{__("checkout.Remove")}}
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="detail">
                        <div class="left">
                            <h6> {{__("checkout.NameRecipient")}}</h6>
                        </div>
                        <div class="right">
                            <p>  {{$address['firstNameRecipient']}} {{$address['lastNameRecipient']}}</p>
                        </div>
                    </div>

                    <div class="detail">
                        <div class="left">
                            <h6> {{__("checkout.RecipientAddress")}}</h6>
                        </div>
                        <div class="right">
                            <p>
                                {{$address['buildingNameRecipient']}}
                                {{$address['addressOnMap']}}
                            </p>
                        </div>
                    </div>

                    <div class="detail">
                        <div class="left">
                            <h6> {{__("checkout.RecipientPhoneNumber")}}</h6>
                        </div>
                        <div class="right">
                            <p>{{$address['mobileNumberRecipient']}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div wire:loading wire:target="setAddressSelect">
                    <x-control-panel.loading/>
                </div>
                <a href="#" class="select-address @if($address['id']."to"==$activeAddress) active-address @endif"
                   wire:loading.remove wire:target="setAddressSelect"
                   wire:click.prevent="setAddressSelect('to',{{json_encode($address)}})">
                    @if($address['id']."to"==$activeAddress)
                        {{__("checkout.AddressSelected")}}
                        <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                                  fill="#FFFFFF"/>
                        </svg>
                    @else
                        {{__("checkout.SelectAddress")}}
                    @endif
                </a>

            </div>
        </div>
    @endif

{{--</div>--}}
