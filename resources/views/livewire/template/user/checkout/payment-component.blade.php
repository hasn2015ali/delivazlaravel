<div x-data="paymentMethod()">
    @push('head-scripts')
        <script src="{{asset('js/datepicker/datepicker.min.js') }}" defer></script>
        <script src="{{asset('timepicker/js/timepicker.min.js')}}" defer></script>
        <link href="{{asset('timepicker/css/timepicker.min.css')}}" rel="stylesheet"/>

        {{--        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
        {{--        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
    @endpush
    <form wire:submit.prevent="placeOrder">
        <div class="d-flex flex-row payment-section w-100 ">
            <div class="left-side">
                <div class="payment-method @if(!$isUserLogin or isset($addressSelected['type']) and $addressSelected['type']=="to") two-payment-method @endif">
                    <button :class="{ 'active': card }" class="btn " @click.prevent="openCard">
                        {{__('checkout.Card')}}
                    </button>

                    @if(isset($addressSelected['type']) and $addressSelected['type']!="to")
                        <button :class="{ 'active': cash }" class="btn " @click.prevent="openCash">
                            {{__('checkout.CashDelivery')}}
                        </button>
                    @endif
                    @if($isUserLogin)
                    <button :class="{ 'active': wallet }" class="btn " @click.prevent="openWallet">
                        {{__('checkout.DelivazWallet')}}
                    </button>
                    @endif
                </div>

                <div class="card-details"
                     x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="isOpenCard()">
                    <div class="one-row">
                        <div class="user-input">
                            <label>{{__('checkout.CardNumber')}}</label>
                            <input id="credit-card" class="form-control" wire:model.defer="DefaultName"
                                   placeholder="---- ---- ---- ----"/>
                            @error('DefaultName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="one-row">

                        <div class="row-inputs">
                            <div class="user-input datepicker-container place-holder-centered">
                                <label>{{__('checkout.ExpieryDate')}}</label>
                                <div
                                    class="form-group d-flex flex-row justify-content-start input-group date datepicker"
                                >
                                    <input id="date-filter-card" type="text"
                                           class="form-control filter-value datepicker-field"
                                           data-filter-value="card" placeholder="{{__("userFront.MonthYear")}}">
                                    {{--                                <input class="form-control date-input" id='table' placeholder="{{__("userFront.MonthYear")}}"/>--}}
                                    <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar">

                                     </span>
                                 </span>
                                </div>
                                @error('DefaultName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="user-input place-holder-centered">
                                <label>{{__('checkout.CVV')}}</label>
                                <input class="form-control" wire:model.defer="DefaultName"
                                       placeholder="{{__('checkout.Code')}}"/>
                                @error('DefaultName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                </div>

                @if($isUserLogin)
                    <div class="delivaz-wallet"
                         x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="isOpenWallet()">

                        @if($balanceInWallet!=0)
                            <div class="wallet-success">
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M16 8C16 10.1217 15.1571 12.1566 13.6569 13.6569C12.1566 15.1571 10.1217 16 8 16C5.87827 16 3.84344 15.1571 2.34315 13.6569C0.842855 12.1566 0 10.1217 0 8C0 5.87827 0.842855 3.84344 2.34315 2.34315C3.84344 0.842855 5.87827 0 8 0C10.1217 0 12.1566 0.842855 13.6569 2.34315C15.1571 3.84344 16 5.87827 16 8ZM12.03 4.97C11.9586 4.89882 11.8735 4.84277 11.7799 4.80522C11.6863 4.76766 11.5861 4.74936 11.4853 4.75141C11.3845 4.75347 11.2851 4.77583 11.1932 4.81717C11.1012 4.85851 11.0185 4.91797 10.95 4.992L7.477 9.417L5.384 7.323C5.24183 7.19052 5.05378 7.1184 4.85948 7.12183C4.66518 7.12525 4.47979 7.20397 4.34238 7.34138C4.20497 7.47879 4.12625 7.66418 4.12283 7.85848C4.1194 8.05278 4.19152 8.24083 4.324 8.383L6.97 11.03C7.04128 11.1012 7.12616 11.1572 7.21958 11.1949C7.313 11.2325 7.41305 11.2509 7.51375 11.2491C7.61444 11.2472 7.71374 11.2251 7.8057 11.184C7.89766 11.1429 7.9804 11.0837 8.049 11.01L12.041 6.02C12.1771 5.8785 12.2523 5.68928 12.2504 5.49296C12.2485 5.29664 12.1698 5.10888 12.031 4.97H12.03Z"
                                        fill="#009C06"/>
                                </svg>
                                <p>
                                    {{__('checkout.sufficientFunds')}}
                                </p>
                            </div>

                        @else
                            <div class="wallet-error">
                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                <p>
                                    {{__('checkout.NotsufficientFunds')}}
                                </p>
                            </div>


                            <hr/>
                            <div class="account">
                                <p>  {{__('checkout.WalletBalance')}}</p>
                                <p>${{$balanceInWallet}}</p>
                            </div>
                        @endif

                    </div>
                @endif
                <div class="delivaz-cash"
                     x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="isOpenCash()">

                </div>

                <div class="scheduling-section">
                    {{--                <livewire:template.user.checkout.order-scheduling-component />--}}
                    <div class="scheduling-section-container">

                        @if($atDoorOption)
                            <div class="scheduling-option ">
                                <div class="price">
                                    @if($atDoorCost!=0)
                                        {{$atDoorCost}}&nbsp;{{$currency_code}}
                                    @else
                                        {{__('checkout.Free')}}
                                    @endif
                                </div>
                                <div class="address">
                                    {{__('checkout.door')}}
                                </div>
                                <div class="content">
                                    <p>
                                        {{__('checkout.doorDes')}}
                                    </p>
                                    <button type="button"
                                            wire:loading.remove wire:target="setDoorSelected"
                                            wire:click.prevent="setDoorSelected"
                                            class="btn btn-outline-primary-delivaz @if($atDoorSelected) btn-outline-primary-active @endif">
                                        @if($atDoorSelected)
                                            {{__('checkout.Selected')}}
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                                                      fill="#FFFFFF"/>
                                            </svg>
                                        @else
                                            {{__('checkout.Select')}}
                                        @endif
                                    </button>
                                    <div wire:loading wire:target="setDoorSelected">
                                        <x-control-panel.loading/>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @if($isUserLogin)
                            <div class="scheduling-option">
                                <div class="price">
                                    @if($asapCost!=0)
                                        {{$asapCost}}&nbsp;{{$currency_code}}
                                    @else
                                        {{__('checkout.Free')}}
                                    @endif
                                </div>
                                <div class="address">
                                    {{__('checkout.ASAP')}}
                                </div>
                                <div class="content">
                                    <p>
                                        {{__('checkout.ASAPDESC')}}
                                    </p>
                                    <button type="button"
                                            wire:loading.remove wire:target="setInDaySelected"
                                            wire:click.prevent="setInDaySelected"
                                            class="btn btn-outline-primary-delivaz @if($inDaySelected) btn-outline-primary-active @endif">
                                        @if($inDaySelected)
                                            {{__('checkout.Selected')}}
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                                                      fill="#FFFFFF"/>
                                            </svg>
                                        @else
                                            {{__('checkout.Select')}}
                                        @endif
                                    </button>
                                    <div wire:loading wire:target="setInDaySelected">
                                        <x-control-panel.loading/>
                                    </div>
                                </div>
                            </div>


                            <div class="scheduling-option">
                                <div class="price">
                                    @if($scheduleCost!=0)
                                    {{$scheduleCost}}&nbsp;{{$currency_code}}
                                    @else
                                        {{__('checkout.Free')}}
                                    @endif
                                </div>
                                <div class="address">
                                    {{__('checkout.ScheduleOrder')}}
                                </div>
                                <div class="content">
                                    {{__('checkout.ScheduleOrderDes')}}
                                </div>
                                <div class="row-inputs">
                                    <div class="user-input">
                                        <label>{{__('checkout.Date')}}</label>
                                        <div class="input-group date p-relative data-container">
                                            <input id="date-schedule" type="text" class="form-control">
                                            <div class="input-group-append" data-target="#datetimepicker3"
                                                 data-toggle="datetimepicker">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </div>
                                            </div>

                                        </div>
                                        @error('DefaultName')
                                        <div class="alert alert-danger2">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="user-input ">
                                        <label>{{__('checkout.Time')}}</label>
                                        <div class="form-group">
                                            <div class="input-group date" data-target-input="nearest">
                                                {{--                                            <input id="time-schedule" type="time" class="form-control" />--}}
                                                <input id="time-schedule" type="text"
                                                       class="form-control bs-timepicker">
                                                <div class="input-group-append" data-target="#datetimepicker3"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                        @error('DefaultName')
                                        <div class="alert alert-danger2">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            {{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row-inputs justify-content-start">
                                    <div class="user-input">
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input"
                                                   wire:change.prevent="changeRepeatOrderState"
                                                   @if($repeatOrderState) checked="checked" @endif
                                                   id="customSwitch1">
                                            <label class="custom-control-label"
                                                   for="customSwitch1">{{__('checkout.RepeatOrder')}}</label>
                                        </div>
                                    </div>
                                    <div wire:loading wire:target="changeRepeatOrderState">
                                        <x-control-panel.loading/>
                                    </div>
                                </div>


                                @if($repeatOrderState)
                                    <div class="row-inputs">
                                        <div class="user-input">
                                            <label>{{__('checkout.StartDate')}}</label>
                                            <div wire:ignore class="input-group date p-relative data-start-container">
                                                <input id="date-schedule-start" type="text" class="form-control">
                                                <div class="input-group-append" data-target="#datetimepicker3"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </div>
                                                </div>

                                            </div>
                                            @error('DefaultName')
                                            <div class="alert alert-danger2">
                                                <button type="button" class="close" data-dismiss="alert">&times;
                                                </button>
                                                {{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="user-input "
                                             x-data="{ dropDownMenu : false}">
                                            <label>{{__('checkout.Intervals')}}</label>
                                            <div class="my-dropdown-container">
                                                <button type="button" class="btn btn-interval"
                                                        @click="dropDownMenu=!dropDownMenu">
                                                    {{$selectedIntervalsTranslated}}
                                                </button>
                                                <div class="my-dropdown-menu"
                                                     x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="dropDownMenu"
                                                     @click.away="dropDownMenu=false"
                                                     x-on:close-drop-down.window="dropDownMenu = false"
                                                     x-cloak>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('Daily')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.Daily")}}
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EveryTwoDays')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EveryTwoDays")}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EveryThreeDays')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EveryThreeDays")}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EveryFourDays')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EveryFourDays")}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EveryFiveDays')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EveryFiveDays")}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EverySixDays')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EverySixDays")}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="item-container"
                                                         wire:click.prevent="selectIntervals('EveryWeek')">
                                                        <div class="d-flex flex-row item align-items-center">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                <path
                                                                    d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                                    stroke="#209F84" stroke-width="2"
                                                                    stroke-linecap="round"
                                                                    stroke-linejoin="round"/>
                                                                <path d="M22 4L12 14.01L9 11.01" stroke="#209F84"
                                                                      stroke-width="2" stroke-linecap="round"
                                                                      stroke-linejoin="round"/>
                                                            </svg>
                                                            <p class="dropdown-item">
                                                                {{__("checkout.EveryWeek")}}
                                                            </p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            @error('DefaultName')
                                            <div class="alert alert-danger2">
                                                <button type="button" class="close" data-dismiss="alert">&times;
                                                </button>
                                                {{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row-inputs justify-content-start">
                                        <div class="user-input">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" class="custom-control-input"
                                                       wire:change.prevent="changeOngoingState"
                                                       @if($ongoingState) checked="checked" @endif
                                                       id="customSwitchGoing">
                                                <label class="custom-control-label"
                                                       for="customSwitchGoing">{{__('checkout.Ongoing')}}</label>
                                            </div>
                                        </div>
                                        <div wire:loading wire:target="changeOngoingState">
                                            <x-control-panel.loading/>
                                        </div>
                                    </div>

                                    @if(!$ongoingState)
                                        <div class="row-inputs">
                                            <div class="user-input w-100">
                                                <label>{{__('checkout.EndDate')}}</label>
                                                <div wire:ignore class="input-group date p-relative data-end-container">
                                                    <input id="date-schedule-end" type="text" class="form-control">
                                                    <div class="input-group-append" data-target="#datetimepicker3"
                                                         data-toggle="datetimepicker">
                                                        <div class="input-group-text">
                                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                                        </div>
                                                    </div>

                                                </div>
                                                @error('DefaultName')
                                                <div class="alert alert-danger2">
                                                    <button type="button" class="close" data-dismiss="alert">&times;
                                                    </button>
                                                    {{ $message }}</div>
                                                @enderror
                                            </div>

                                        </div>
                                    @endif
                                @endif
                                <div class="user-input">
                                    <button type="button"
                                            wire:loading.remove wire:target="setScheduleSelected"
                                            wire:click.prevent="setScheduleSelected"
                                            class="btn btn-outline-primary-delivaz @if($scheduleSelected) btn-outline-primary-active @endif">
                                        @if($scheduleSelected)
                                            {{__('checkout.Selected')}}
                                            <svg width="22" height="22" viewBox="0 0 22 22" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M11 0C4.925 0 0 4.925 0 11C0 17.075 4.925 22 11 22C17.075 22 22 17.075 22 11C22 4.925 17.075 0 11 0ZM15.768 9.14C15.8558 9.03964 15.9226 8.92274 15.9646 8.79617C16.0065 8.6696 16.0227 8.53591 16.0123 8.40298C16.0018 8.27005 15.9648 8.14056 15.9036 8.02213C15.8423 7.90369 15.758 7.79871 15.6555 7.71334C15.5531 7.62798 15.4346 7.56396 15.3071 7.52506C15.1796 7.48616 15.0455 7.47316 14.9129 7.48683C14.7802 7.50049 14.6517 7.54055 14.5347 7.60463C14.4178 7.66872 14.3149 7.75554 14.232 7.86L9.932 13.019L7.707 10.793C7.5184 10.6108 7.2658 10.51 7.0036 10.5123C6.7414 10.5146 6.49059 10.6198 6.30518 10.8052C6.11977 10.9906 6.0146 11.2414 6.01233 11.5036C6.01005 11.7658 6.11084 12.0184 6.293 12.207L9.293 15.207C9.39126 15.3052 9.50889 15.3818 9.63842 15.4321C9.76794 15.4823 9.9065 15.505 10.0453 15.4986C10.184 15.4923 10.32 15.4572 10.4444 15.3954C10.5688 15.3337 10.6791 15.2467 10.768 15.14L15.768 9.14Z"
                                                      fill="#FFFFFF"/>
                                            </svg>
                                        @else
                                            {{__('checkout.Select')}}
                                        @endif
                                    </button>
                                    <div wire:loading wire:target="setScheduleSelected">
                                        <x-control-panel.loading/>
                                    </div>
                                </div>

                            </div>
                        @endif

                    </div>

                </div>

            </div>
            <div class="right-side">
                <livewire:template.user.checkout.order-info-component
                    :totalForItemCart="$totalForItemCart"
                    :cashCost="$cashCost"
                    :atDoorCost="$atDoorCost"
                    :asapCost="$asapCost"
                    :scheduleCost="$scheduleCost"
                    :deliveryCharge="$deliveryCharge"
                    :country="$country"
                    :cityName="$cityName"
                    :countryName="$countryName"
                    step="payment"
                    :itemsInCart="$itemsInCart"/>
                <div class="d-flex justify-content-center w-100 btn-container mt-3">
                    <div wire:loading wire:target="placeOrder">
                        <x-control-panel.loading/>
                    </div>
                    <button class="btn btn-primary-gradient"
                            wire:loading.remove wire:target="placeOrder"
                            type="submit">
                        {{__("checkout.PLACEORDER")}}
                    </button>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-4">
            {{--        <button class="btn btn-primary-gradient" type="submit">--}}
            {{--            {{__("checkout.PLACEORDER")}}--}}
            {{--        </button>--}}
        </div>
    </form>
    @push('custom-scripts')
        <script>
            $('#credit-card').on('keypress change', function () {
                $(this).val(function (index, value) {
                    return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                });
            });

        </script>
        <script>
            function paymentMethod() {
                return {
                    card: true,
                    wallet: false,
                    cash: false,
                    openCard() {
                        this.card = true;
                        this.wallet = false;
                        this.cash = false;
                        this.$wire.emitSelf('selectedPaymentMethod', 'card');
                        this.$wire.emit('removeCost','cash');
                    },
                    closeCard() {
                        this.card = false
                    },
                    isOpenCard() {
                        return this.card === true
                    },


                    openWallet() {
                        this.card = false;
                        this.wallet = true;
                        this.cash = false;
                        this.$wire.emitSelf('selectedPaymentMethod', 'wallet');
                        this.$wire.emit('removeCost','cash');
                    },
                    closeWallet() {
                        this.wallet = false
                    },
                    isOpenWallet() {
                        return this.wallet === true
                    },


                    openCash() {
                        this.card = false;
                        this.wallet = false;
                        this.cash = true;
                        this.$wire.emitSelf('selectedPaymentMethod', 'cash');
                        this.$wire.emit('addNewCost','cash');
                    },
                    closeCash() {
                        this.cash = false
                    },
                    isOpenCash() {
                        return this.cash === true
                    },
                }
            }
        </script>

        <script>
            $(document).ready(function () {
                $('#date-filter-day').datepicker();
                $('#date-filter-card').datepicker({
                    format: 'MM/yyyy',
                    icons: {
                        time: 'fa fa-time',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-screenshot',
                        clear: 'fa fa-trash',
                        close: 'fa fa-remove'
                    },
                    startView: "months",
                    minViewMode: "months",
                    autoclose: true,
                    container: '.datepicker-container',
                });

                $('#date-schedule').datepicker({
                    format: 'mm/dd/yyyy',

                    autoclose: true,
                    container: '.data-container',
                });

                $('#date-schedule-start').datepicker({
                    format: 'mm/dd/yyyy',

                    autoclose: true,
                    container: '.data-start-container',
                });
                $('#date-schedule-end').datepicker({
                    format: 'mm/dd/yyyy',

                    autoclose: true,
                    container: '.data-end-container',
                });
            });
            $(function () {
                $('.bs-timepicker').timepicker();
            });
        </script>
    @endpush
</div>
