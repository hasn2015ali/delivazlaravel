<div
    x-data="{showGiftSection: @entangle('showGiftSection')}">

    <div class="edit-new-address-container">
        <div class="map">

            <livewire:template.user.checkout.map-component/>
            @error('addressOnMap')
            <div class="alert alert-danger2">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ $message }}
            </div>
            @enderror
        </div>


        <div class="info">
            <form wire:submit.prevent="addNewAddress">
                <div class="inputs one-row">
                    <div class="labels">
                        <div class="left">
                            <label>{{__('checkout.AddressName')}}</label>
                        </div>
                        <div class="right">
                            @if($userId!=0)
                                @if(!$updateAddress)
                                    <div wire:loading wire:target="changeGiftState">
                                        <x-control-panel.loading/>
                                    </div>
                                    <label

                                        class="explaine" for="edit_extra_block">{{__('checkout.SendGift')}}</label>
                                    <div

                                        class="custom-control custom-switch d-inline" id="sw">
                                        <input type="checkbox" class="custom-control-input" id="edit_extra_block"
                                               name="extra_block" value=""
                                               wire:change.prevent="changeGiftState"
                                               @if($showGiftSection==true) checked="" @endif>
                                        <label class="custom-control-label" for="edit_extra_block"></label>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="user-input">

                        <input class="form-control" wire:model.defer="addressName"/>
                        @error('addressName')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class=""
                     x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="!showGiftSection">
                    <div class="inputs one-row">

                        <div class="row-inputs">
                            <div class="user-input">
                                {{--                                {{dd($firstName)}}--}}
                                <label>{{__('checkout.FirstName')}}</label>
                                <input class="form-control" wire:model.defer="firstName"/>
                                @error('firstName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="user-input">
                                <label>{{__('checkout.LastName')}}</label>
                                <input class="form-control" wire:model.defer="lastName"/>
                                @error('lastName')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="inputs one-row">
                        <div class="user-input">
                            @if($validateMobileNumber)
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                &nbsp;
                                <label>{{__('checkout.PhoneValid')}}</label>

                                {{$validateMobileNumber}}
                            @else
                                <label>{{__('checkout.MobileNumber')}}</label>
                            @endif

                            <div class="mobile-number">
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle"
                                            data-toggle="dropdown" data-display="static" aria-expanded="false">
                                        @if($selectedCountryCode)
                                            {{$selectedCountryCode}}
                                        @else
                                            {{__('checkout.SelectCountryCode')}}
                                        @endif
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-lg-right" id="countryCodesDropdown">
                                        <div class="input-container">
                                            <input class="input-filter"
                                                   type="text" placeholder="{{__("checkout.searchNumber")}}"
                                                   id="countryCodeInput" onkeyup="filterCountryCodeFunction()">
                                        </div>
                                        @if(count($countriesCode)!=0)
                                            @foreach($countriesCode as $item)
                                                <p class="dropdown-item"
                                                   wire:click.prevent="setCountryCode({{$item['code_number']}})">
                                                    +{{$item['code_number']}}
                                                </p>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                                <input class="form-control" wire:model.lazy="mobileNumber"/>

                            </div>
                            @error('mobileNumber')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            @error('selectedCountryCode')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            <div wire:loading wire:target="setCountryCode">
                                <x-control-panel.loading/>
                            </div>
                        </div>

                    </div>

                    <div class="inputs one-row">
                        <div class="user-input w-100">
                            <label>{{__('checkout.BuildingName')}}</label>
                            <input class="form-control" wire:model.defer="buildingName"/>
                            @error('buildingName')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="inputs one-row">
                        <div class="w-100">
                            <label>{{__('checkout.NearestLandmark')}}</label>
                            <input class="form-control" wire:model.defer="nearestLandmark"/>
                            @error('nearestLandmark')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>

                    <div class="inputs one-row">

                        <div class="row-inputs">
                            <div class="user-input">
                                <div class="form-group mb-0">
                                    <label>
                                        {{__("checkout.Addresslabel")}}
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" wire:model.defer="addressType"
                                               value="Home" class="form-check-input"
                                               name="optradio"> {{__("checkout.Home")}}
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" wire:model.defer="addressType"
                                               value="Work" class="form-check-input"
                                               name="optradio"> {{__("checkout.Work")}}
                                    </label>
                                </div>
                            </div>
                            <div class="user-input d-flex flex-column align-items-end justify-content-end">
                                <div class="d-flex justify-content-center ">
                                    <button
                                        wire:loading.remove wire:target="addNewAddress"
                                        type="submit" class="btn btn-primary-gradient">
                                        {{__("checkout.Confirm")}}
                                    </button>
                                    <div wire:loading wire:target="addNewAddress">
                                        <x-control-panel.loading/>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class=""
                     x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="showGiftSection">

                    <div class="inputs one-row">

                        <div class="row-inputs">
                            <div class="user-input">
                                <label>{{__('checkout.FirstNameRecipient')}}</label>
                                <input class="form-control" wire:model.defer="firstNameRecipient"/>
                                @error('firstNameRecipient')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="user-input">
                                <label>{{__('checkout.LastNameRecipient')}}</label>
                                <input class="form-control" wire:model.defer="lastNameRecipient"/>
                                @error('lastNameRecipient')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="inputs one-row">
                        <div class="user-input">
                            @if($validateRecipientMobileNumber)
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                &nbsp;
                                <label>{{__('checkout.RecipientMobileNumberPhoneValid')}}</label>

                                {{$validateRecipientMobileNumber}}
                            @else
                                <label>{{__('checkout.RecipientMobileNumber')}}</label>
                            @endif

                            <div class="mobile-number">
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle"
                                            data-toggle="dropdown" data-display="static" aria-expanded="false">
                                        @if($selectedCountryCodeForRecipient)
                                            {{$selectedCountryCodeForRecipient}}
                                        @else
                                            {{__('checkout.SelectCountryCode')}}
                                        @endif

                                    </button>
                                    <div class="dropdown-menu dropdown-menu-lg-right" id="countryCodesDropdown">
                                        <div class="input-container">
                                            <input class="input-filter" type="text"
                                                   placeholder="{{__("checkout.searchNumber")}}" id="countryCodeInput"
                                                   onkeyup="filterCountryCodeFunction()">
                                        </div>
                                        @if(count($countriesCode)!=0)
                                            @foreach($countriesCode as $item)
                                                <p class="dropdown-item"
                                                   wire:click.prevent="setCountryCodeForRecipient({{$item['code_number']}})">
                                                    +{{$item['code_number']}}
                                                </p>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                                <input class="form-control" wire:model.lazy="recipientMobileNumber"/>

                            </div>
                            @error('recipientMobileNumber')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            @error('selectedCountryCode')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            <div wire:loading wire:target="setCountryCodeForRecipient">
                                <x-control-panel.loading/>
                            </div>
                        </div>

                    </div>

                    <div class="inputs one-row">
                        <div class="user-input w-100">
                            <label>{{__('checkout.RecipientsBuilding')}}</label>
                            <input class="form-control" wire:model.defer="recipientsBuilding"/>
                            @error('recipientsBuilding')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="inputs one-row">
                        <div class="w-100">
                            <label>{{__('checkout.NearestLandmark')}}</label>
                            <input class="form-control" wire:model.defer="nearestLandmark"/>
                            @error('nearestLandmark')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="inputs one-row">

                        <div class="row-inputs">
                            <div class="user-input">
                                <label>{{__('checkout.FirstNameSender')}}</label>
                                <input class="form-control" wire:model.defer="firstNameSender"/>
                                @error('firstNameSender')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="user-input">
                                <label>{{__('checkout.LastNameSender')}}</label>
                                <input class="form-control" wire:model.defer="lastNameSender"/>
                                @error('lastNameSender')
                                <div class="alert alert-danger2">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="inputs one-row">
                        <div class="user-input">
                            @if($validateSenderMobileNumber)
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                        stroke="#209F84" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"/>
                                    <path d="M22 4L12 14.01L9 11.01" stroke="#209F84" stroke-width="2"
                                          stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                &nbsp;
                                <label>{{__('checkout.SenderMobileNumberPhoneValid')}}</label>

                                {{$validateSenderMobileNumber}}
                            @else
                                <label>{{__('checkout.SenderMobileNumber')}}</label>
                            @endif

                            <div class="mobile-number">
                                <div class="dropdown">
                                    <button type="button" class="btn dropdown-toggle"
                                            data-toggle="dropdown" data-display="static" aria-expanded="false">
                                        @if($selectedCountryCode)
                                            {{$selectedCountryCode}}
                                        @else
                                            {{__('checkout.SelectCountryCode')}}
                                        @endif

                                    </button>
                                    <div class="dropdown-menu dropdown-menu-lg-right" id="countryCodesDropdown">
                                        <div class="input-container">
                                            <input class="input-filter" type="text"
                                                   placeholder="{{__("checkout.searchNumber")}}" id="countryCodeInput"
                                                   onkeyup="filterCountryCodeFunction()">
                                        </div>
                                        @if(count($countriesCode)!=0)
                                            @foreach($countriesCode as $item)
                                                <p class="dropdown-item"
                                                   wire:click.prevent="setCountryCode({{$item['code_number']}})">
                                                    +{{$item['code_number']}}
                                                </p>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>
                                <input class="form-control" wire:model.lazy="senderMobileNumber"/>
                            </div>
                            @error('senderMobileNumber')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            @error('selectedCountryCode')
                            <div class="alert alert-danger2">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            <div wire:loading wire:target="setCountryCode">
                                <x-control-panel.loading/>
                            </div>
                        </div>

                    </div>

                    <section class="user-input d-flex flex-column align-items-center justify-content-center">
                        <div class="d-flex justify-content-center ">
                            <button
                                wire:loading.remove wire:target="addNewAddress"
                                type="submit" class="btn btn-primary-gradient">
                                {{__("checkout.Confirm")}}
                            </button>
{{--                            <input--}}
{{--                                wire:loading.remove wire:target="addNewAddress"--}}
{{--                                type="submit" class="btn btn-primary-gradient" value="{{__("checkout.Confirm")}}" />--}}
{{--                           --}}

                            <div wire:loading wire:target="addNewAddress">
                                <x-control-panel.loading/>
                            </div>
                        </div>

                    </section>
                </div>

            </form>

        </div>

    </div>


</div>
