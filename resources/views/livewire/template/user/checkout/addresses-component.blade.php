<div>
    <div class="addresses-list">

        @if(count($myAddresses)>0)
            @foreach($myAddresses as $address)
                @php
                    $y=rand(100,100000);
                @endphp
                <livewire:template.user.checkout.address-component
                    :address="$address"
                    type="me"
                    :activeAddress="$activeAddress"
                    :key="'address-component'.$y.$address['id']"/>
            @endforeach
        @endif

        @if(count($myAddressesTo)>0)
            @foreach($myAddressesTo as $address)
                    @php
                        $y=rand(100,100000);
                    @endphp
                    <livewire:template.user.checkout.address-component
                        :address="$address"
                        :activeAddress="$activeAddress"
                        type="to"
                        :key="'address-component'.$y.$address['id']"/>
            @endforeach
        @endif

            <div  wire:loading wire:target="goToAddNewAddress">
                <x-control-panel.loading/>
            </div>
        <section class="add-new-address"
                 wire:loading.remove wire:target="goToAddNewAddress"
                 wire:click.prevent="goToAddNewAddress">

                <div class="svg-container">
                    <svg width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M58 32.1429C58 32.6951 57.5523 33.1429 57 33.1429H34.1429C33.5906 33.1429 33.1429 33.5906 33.1429 34.1429V57C33.1429 57.5523 32.6951 58 32.1429 58H25.8571C25.3049 58 24.8571 57.5523 24.8571 57V34.1429C24.8571 33.5906 24.4094 33.1429 23.8571 33.1429H1C0.447716 33.1429 0 32.6951 0 32.1429V25.8571C0 25.3049 0.447715 24.8571 1 24.8571H23.8571C24.4094 24.8571 24.8571 24.4094 24.8571 23.8571V1C24.8571 0.447716 25.3049 0 25.8571 0H32.1429C32.6951 0 33.1429 0.447715 33.1429 1V23.8571C33.1429 24.4094 33.5906 24.8571 34.1429 24.8571H57C57.5523 24.8571 58 25.3049 58 25.8571V32.1429Z"
                            fill="#FAC24F"/>
                    </svg>
                </div>

                <p>
                    {{__("checkout.AddAddress")}}
                </p>
        </section>

    </div>



</div>
