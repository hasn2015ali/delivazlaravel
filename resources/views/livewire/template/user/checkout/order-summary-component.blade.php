<div class="order-summery-container">
    <div class="left-side">

        <div class="checked-address">
            <div class="one-row">
                <div class="left">

                    <p class="active">
                        {{__('checkout.Deliverto')}}
                    </p>
                </div>
                <div class="right">
                    <p>
                       @if(isset($storedAddress['addressName']))
                            {{$storedAddress['addressName']}}
                        @endif
                    </p>
                </div>
            </div>
            <div class="one-row">
                <div class="left">

                    <p>
                        {{__('checkout.Name')}}
                    </p>
                </div>
                <div class="right">
                    <p>
                        @if( isset($storedAddress['firstName']) and $storedAddress['type']=="me")
                            {{$storedAddress['firstName']}}
                            {{$storedAddress['lastName']}}
                        @elseif( isset($storedAddress['firstNameRecipient']))
                            {{$storedAddress['firstNameRecipient']}}
                            {{$storedAddress['lastNameRecipient']}}

                        @endif
                    </p>
                </div>
            </div>
            <div class="one-row">
                <div class="left">

                    <p>
                        {{__('checkout.Address')}}
                    </p>
                </div>
                <div class="right">
                    <p>
                        @if(isset($storedAddress['type']) and  $storedAddress['type']=="me")
                            {{$storedAddress['buildingName']}}
                            {{$storedAddress['addressOnMap']}}
                        @elseif(isset($storedAddress['buildingNameRecipient']) )
                            {{$storedAddress['buildingNameRecipient']}}
                            {{$storedAddress['addressOnMap']}}
                        @endif
                    </p>
                </div>
            </div>
            <div class="one-row">
                <div class="left">

                    <p>
                        {{__('checkout.PhoneNumber')}}
                    </p>
                </div>
                <div class="right">
                    <p>

                        @if(isset($storedAddress['type']) and  $storedAddress['type']=="me")
                            {{$storedAddress['mobileNumber']}}

                        @elseif(isset($storedAddress['mobileNumberRecipient']))
                            {{$storedAddress['mobileNumberRecipient']}}
                        @endif
                    </p>
                </div>
            </div>
        </div>
        <div class="order-info">
            <div>
                <h2 class="address"> {{__('checkout.OrderDetails')}}</h2>
            </div>

            @if(count($itemsInCart['products']) > 0)
                @foreach($itemsInCart['products'] as $item)
                    <div class="one-row">
                        <div class="left">
                            @if($item['type']=="res")

                                <img class=" " src="{{asset($foldersForFood.$item['photo'])}}">
                            @elseif($item['type']=="ven")
                                <img class=" " src="{{asset($foldersForProduct.$item['photo'])}}">
                            @endif
                        </div>
                        <div class="medium">

                            <p>  {{ $item['name'] }}</p>
                            <p>{{ $item['size'] }}</p>
                            @if(isset($item['additions']['additions']))
                                @if(count($item['additions']['additions'])!=0)

                                    @foreach($item['additions']['additions'] as $add)
                                        <p> {{$add['quantity']}} &nbsp;&nbsp; x
                                            {{ $add['name']}} </p>
                                    @endforeach
                                @endif
                            @endif
                            <p class="price">
                                {{ $item['price'] }} {{$currency_code}}
                            </p>

                        </div>
                        <div class="right">
                            x{{ $item['quantity'] }}
                        </div>
                    </div>
                @endforeach
            @endif


        </div>
    </div>
    <div class="right-side">
        <livewire:template.user.checkout.order-info-component
            :totalForItemCart="$totalForItemCart"
            :cashCost="$cashCost"
            :atDoorCost="$atDoorCost"
            :asapCost="$asapCost"
            :scheduleCost="$scheduleCost"
            :deliveryCharge="$deliveryCharge"
            step="orderSummery"
            :country="$country"
            :cityName="$cityName"
            :countryName="$countryName"
            :itemsInCart="$itemsInCart"/>
    </div>

</div>
