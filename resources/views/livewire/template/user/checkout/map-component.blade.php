<div class="d-flex w-100 height-full" wire:ignore>
{{--    <livewire:template.user.checkout.search-component/>--}}

        <input id="search" class="controls  search-on-map" type="text" placeholder="{{__('checkout.Search_name')}}" >
    <button id="get-current-location-btn" class="btn btn-position">
        <svg width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M19 12.6666C15.5009 12.6666 12.6667 15.5008 12.6667 18.9999C12.6667 22.4991 15.5009 25.3333 19 25.3333C22.4992 25.3333 25.3334 22.4991 25.3334 18.9999C25.3334 15.5008 22.4992 12.6666 19 12.6666ZM33.155 17.4166C32.7964 14.2058 31.3566 11.2124 29.0721 8.92785C26.7876 6.64335 23.7942 5.20356 20.5834 4.84492V1.58325H17.4167V4.84492C14.2059 5.20356 11.2125 6.64335 8.92797 8.92785C6.64347 11.2124 5.20368 14.2058 4.84504 17.4166H1.58337V20.5833H4.84504C5.20368 23.7941 6.64347 26.7875 8.92797 29.072C11.2125 31.3565 14.2059 32.7963 17.4167 33.1549V36.4166H20.5834V33.1549C23.7942 32.7963 26.7876 31.3565 29.0721 29.072C31.3566 26.7875 32.7964 23.7941 33.155 20.5833H36.4167V17.4166H33.155ZM19 30.0833C12.8725 30.0833 7.91671 25.1274 7.91671 18.9999C7.91671 12.8724 12.8725 7.91658 19 7.91658C25.1275 7.91658 30.0834 12.8724 30.0834 18.9999C30.0834 25.1274 25.1275 30.0833 19 30.0833Z"
                fill="#FDFDFD"/>
        </svg>
    </button>
    <div id="map"></div>
    @push('custom-scripts')
        <script async
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASHH_XFUlrFFzTj1vUn97TYpl3TOVAwHk&libraries=places&callback=initMap">
        </script>
        <script>
            {{--var translation = <?php echo __('checkout.LocationFoundOnMap') ?>;--}}
            {{--let message=@lang('checkout.LocationFoundOnMap')--}}
            {{--console.log(message);--}}

            let mapProp,map, infoWindow,geocoder,marker;
            let input;
            setTimeout( initializeInput()  , 2000);
            //
            // let input = $('#search')[0];
            function initializeInput() {
                 input = $('#search')[0];
                // console.log(input);
            }

            function initialize() {
                 mapProp = {
                     center: {lat: -33.8688, lng: 151.2195},
                     zoom: 13
                };
                map = new google.maps.Map(document.getElementById('map'), mapProp);
            };

            function initMap() {
                initialize();
                // map = new google.maps.Map(document.getElementById('map'), {
                //     center: {lat: -33.8688, lng: 151.2195},
                //     zoom: 13
                // });
                 // input = document.querySelector('#search');
                 // input = $('#search')[0];
                geocoder = new google.maps.Geocoder();
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                infoWindow = new google.maps.InfoWindow();
                 marker = new google.maps.Marker({
                    map: map,
                    anchorPoint: new google.maps.Point(0, -29)
                });

                autocomplete.addListener('place_changed', function () {
                    infoWindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("Autocomplete's returned place contains no geometry");
                        return;
                    }

// If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }
                    marker.setIcon(({
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(35, 35)
                    }));
                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);
                    console.log("place.geometry.location",place.geometry.location);
                    // console.log(place.icon);
                    Livewire.emit('setAddressOnMap',input.value);

                });


                //get current Location

                let locationButton = document.querySelector('#get-current-location-btn');
                locationButton.addEventListener("click", () => {
                    // Try HTML5 geolocation.
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                                const pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude,
                                };

                                // console.log('pos',pos);
                                getReverseGeocodingData(position.coords.latitude,position.coords.longitude,pos);
                                // infoWindow.setPosition(pos);
                                //
                                // infoWindow.setContent("Location found Here");
                                // infoWindow.open(map);
                                map.setCenter(pos);
                                // const image =
                                //     "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png";
                                // marker.setIcon(({
                                //     url: image,
                                //     title: "Hello World!",
                                //     size: new google.maps.Size(71, 71),
                                //     origin: new google.maps.Point(0, 0),
                                //     anchor: new google.maps.Point(17, 34),
                                //     scaledSize: new google.maps.Size(35, 35)
                                // }));

                                // marker.setIcon(({
                                //     // url: pos.icon,
                                //     size: new google.maps.Size(71, 71),
                                //     origin: new google.maps.Point(0, 0),
                                //     anchor: new google.maps.Point(17, 34),
                                //     scaledSize: new google.maps.Size(35, 35)
                                // }));
                                // marker.setPosition(pos);
                                // marker.setVisible(true);
                            },
                            () => {
                                handleLocationError(true, infoWindow, map.getCenter());
                            }
                        );
                    } else {
                        // Browser doesn't support Geolocation
                        handleLocationError(false, infoWindow, map.getCenter());
                    }
                });

            }


            //get current Location

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(
                    browserHasGeolocation
                        ? "Error: The Geolocation service failed."
                        : "Error: Your browser doesn't support geolocation."
                );
                infoWindow.open(map);
            }


            // get address place from lan
            function getReverseGeocodingData(lat, lng,pos) {
                // console.log("get address place from lan");

                var latlng = new google.maps.LatLng(lat, lng);
                // This is making the Geocode request
                //  geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng },  (results, status) =>{
                    if (status !== google.maps.GeocoderStatus.OK) {
                        alert(status);
                    }
                    // This is checking to see if the Geoeode Status is OK before proceeding
                    if (status == google.maps.GeocoderStatus.OK) {
                        // console.log(results);
                        var address = (results[0].formatted_address);
                        Livewire.emit('setAddressOnMap',address);
                        marker.setIcon(null);
                        marker.setTitle(address);
                        marker.setPosition(pos);
                        marker.setVisible(true);
                        input.value = address;
                        // infoWindow.setPosition(pos);
                        //
                        // infoWindow.setContent(address);
                        // infoWindow.open(map);
                        // console.log(address);

                    }
                });
            }


            // update address Get the geographic coordinates for an address or location.

            function geocode(request) {
                // clear();
                console.log(request);
                marker.setMap(null);
                console.log("Get the geographic coordinates for an address or location");
                geocoder.geocode({ 'address': request }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                        });
                    } else
                        alert("we Cannot Find old Location on map Please select Your new location");

                });
            }

            // window.initMap = initMap;
            setTimeout(function(){window.initMap = initMap;},'2000');


            window.addEventListener('reset-map', event => {

                // console.log("kkk");
                // setTimeout(function(){initMap()},'2000');
                initMap();
                input.value = "";
                // window.initMap = initMap;
            })

            window.addEventListener('go-old-address', event => {
                // initMap();
                // console.log(event.detail.oldAddress);
                // initialize();
                geocode(event.detail.oldAddress);
                input.value = "";
                // alert('Name updated to: ' + event.detail.oldAddress);
            })

        </script>
    @endpush
</div>
