<div class="wishlist-section">
    <div class="container">
        <div class="d-flex flex-column wishlist">
            <div class="header">
                {{__("userFront.Wishlist")}}
            </div>
            <div class="d-flex flex-column justify-content-center align-items-center">
                {{--               <p class="content empty">--}}
                {{--                   {{__("userFront.Wishlist_empty")}}--}}
                {{--               </p>--}}


                @if(count($wishes)!=0)
                    <table class="table table-borderless table-responsive-lg">
                        <thead>
                        <tr>
                            <th><h4 class="table-address">{{__("userFront.Image")}}</h4></th>
                            <th><h4 class="table-address">{{__("userFront.Product_Name")}}</h4></th>
                            <th><h4 class="table-address">{{__("userFront.Price")}}</h4></th>
                            <th><h4 class="table-address">{{__("userFront.Add_basket")}}</h4></th>
                            <th><h4 class="table-address">{{__("userFront.Action")}}</h4></th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach($wishes as $wish)
                            <?php
//                            $country = $wish->country->translation[0]->name;
//                            $city = $wish->city->translation[0]->name;
//                            $delivazCommissin=$wish->serviceProvider->delivaz_commission;
                            ?>
                            @if($wish->type == "res")
                                <?php
                                $folders = "storage/images/food/";
                                $item = $wish->food;
                                $serviceProviderSlug = $wish->serviceProvider->slug;
                                ?>
                            @endif
                            @if($wish->type == "ven")
                                <?php
                                $folders = "storage/images/product/";
                                $item = $wish->product;
                                $serviceProviderSlug = $wish->serviceProvider->slug;
                                ?>
                            @endif


                            <?php
                            $y=rand(100,1000000);
                            ?>
                            <livewire:template.user.user-wish-component
                                :wish="$wish"
                                :folders="$folders"
                                :userID="$userID"
                                :item="$item"
                                :serviceProviderSlug="$serviceProviderSlug"
                                :key="'user-wish-component'.$y.$item->id"/>

                        @endforeach

                        </tbody>
                    </table>
                    @if(isset($wishes))
                        <div class="d-flex flex-row justify-content-center width-full">
                            <div class="items-pagination">
                                {{ $wishes->links() }}
                            </div>
                        </div>
                    @endif
                @else
                    <div class="d-flex flex-row justify-content-center">
                        <h4 style="margin-top: 4rem"
                            class="table-address ">{{__("userFront.YourWishlistIsEmpty")}} </h4>
                    </div>

                @endif
            </div>
        </div>
    </div>
</div>
