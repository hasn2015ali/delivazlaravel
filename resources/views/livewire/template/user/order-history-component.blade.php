<div class="user-order-history d-flex flex-column justify-content-start align-items-start" x-data="{ toggle1: false }">
    <div>
        <h4 class="address">{{__("userFront.Order_History")}}</h4>
    </div>
    <div>
        <p class="content">{{__("userFront.Oops")}}</p>
    </div>
    <div>
        <h4 class="address3">{{__("userFront.Past_Orders")}}</h4>
    </div>
    <div>
        <p class="danger">{{__("userFront.no_past_orders")}}</p>
    </div>

    <div>
        <p class="success">{{__("userFront.Complete")}} 05</p>
    </div>

    <div class="d-flex flex-wrap old-orders">
        <div class="d-flex flex-column justify-content-start old-order">
            <div class="img">
                <img class=" " src="{{asset('storage/images/avatars/1.jpeg')}}">
            </div>
            <div class="d-flex flex-row justify-content-start content">
                <h6 class="name-item">Food Name</h6>

            </div>
        </div>

        <div class="d-flex flex-column justify-content-start old-order">
            <div class="img">
                <img class=" " src="{{asset('storage/images/avatars/1.jpeg')}}">
            </div>
            <div class="d-flex flex-row justify-content-start content">
                <h6 class="name-item">Food Name</h6>

            </div>
        </div>

        <div class="d-flex flex-column justify-content-start old-order">
            <div class="img">
                <img class=" " src="{{asset('storage/images/avatars/1.jpeg')}}">
            </div>
            <div class="d-flex flex-row justify-content-start content">
                <h6 class="name-item">Food Name</h6>

            </div>
        </div>

        <div class="d-flex flex-column justify-content-start old-order">
            <div class="img">
                <img class=" " src="{{asset('storage/images/avatars/1.jpeg')}}">
            </div>
            <div class="d-flex flex-row justify-content-start content">
                <h6 class="name-item">Food Name</h6>

            </div>
        </div>

        <div class="d-flex flex-column justify-content-start old-order">
            <div class="img">
                <img class=" " src="{{asset('storage/images/avatars/1.jpeg')}}">
            </div>
            <div class="d-flex flex-row justify-content-start content">
                <h6 class="name-item">Food Name</h6>

            </div>
        </div>

    </div>
</div>
