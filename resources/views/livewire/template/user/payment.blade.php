<div class="d-flex flex-column user-payment">
    <div class="d-flex flex-row justify-content-start">
        <h4 class="header">{{__("userFront.Payment")}}</h4>
    </div>

    <div class="d-flex flex-row justify-content-start">
        <a class="btn btn-link"><i class="fas fa-wallet"></i>{{__("userFront.DelivazWallet")}}</a>
    </div>

    <div class="d-flex flex-row justify-content-start">
        <a class="btn btn-link"><i class="fas fa-credit-card"></i>{{__("userFront.AddCard")}}</a>
    </div>

    <div class="d-flex flex-row justify-content-start">
        <a class="btn btn-link"><i class="fas fa-key"></i>{{__("userFront.PaymentMethod")}}</a>
    </div>
</div>
