<div class="user-wallet d-flex flex-column justify-content-start align-items-start">
    {{-- In work, do what you enjoy. --}}
    <div>
        <h4 class="address">{{__("userFront.Delivaz_Wallet")}}</h4>
    </div>
    <div class="balance">
        <p class="content">$40.43 Balance</p>
    </div>
    <h4 class="address2">{{__("userFront.AddPaymentCard")}}</h4>

    <div  class="d-flex flex-column justify-content-start align-items-start pay-card">
        <form>
            <div class="form-group d-flex flex-row justify-content-start">
                <i class="fas fa-user"></i><input class="form-control" placeholder="{{__("userFront.NameofCard")}}"/>
            </div>

            <div class="form-group d-flex flex-row justify-content-start">
                <i class="fas fa-credit-card"></i> <input class="form-control" placeholder="{{__("userFront.CardNumber")}}"/>
            </div>

            <div class="d-flex flex-row justify-content-start">
                <div class="form-group d-flex flex-row justify-content-start input-group date datepicker" id='table'>
                    <i class="far fa-calendar-alt"></i>  <input  class="form-control " placeholder="{{__("userFront.MonthYear")}}"/>
                    <span class="input-group-addon">
                         <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <div class="form-group d-flex flex-row justify-content-start second-child">
                    <i class="fas fa-lock"></i> <input class="form-control" placeholder="{{__("userFront.CVC")}}"/>
                </div>

            </div>

            <div class="form-group d-flex flex-row justify-content-start handel-select">
                <i class="fas fa-flag"></i>
                <select  class="form-control  toggle-enabl" >
                    <option  value="off" hidden>{{__("userFront.Country")}}</option>
{{--                    @for($m=0;$m<60;$m++)--}}
{{--                        <option value="{{$m}}">{{$m}}</option>--}}
{{--                    @endfor--}}
                </select>
            </div>

            <div class="form-group d-flex flex-row justify-content-start">
                <button type="submit" class="btn btn-save">{{__("userFront.Save")}}</button>
            </div>



        </form>
    </div>

</div>
