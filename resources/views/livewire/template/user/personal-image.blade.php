<div class="personal-image" x-data="{ isShowing2: false }">
    <div class="d-flex flex-column justify-content-center align-items-center">
        <div class="img">
            @if ($photo)
                <img src="  {{ $photo->temporaryUrl() }}">
            @elseif($user->avatar!=null)
                <img src="{{asset('storage/images/avatars/'.$user->avatar)}}">
            @else
                <img src="{{asset('storage/images/avatars/person.png')}}">
            @endif

              <div class="btn-hover">
                  <button :class="{ 'btn-edit': !isShowing2 ,'btn-Cancel': isShowing2}" class="btn  " @click="isShowing2 = !isShowing2">
                      <i  class="fa fa-edit " aria-hidden="true"  x-show="!isShowing2"></i>
                      <span  x-show="!isShowing2"> {{__("userFront.Edit")}}</span>
                      <span  x-show="isShowing2" x-cloak> {{__("userFront.Cancel")}}</span>
                  </button>
              </div>
        </div>

        <div >
          <h4 class="address">{{$user->firstName}}</h4>
        </div>
        <form wire:submit.prevent="changePhoto">
            <div class="d-flex flex-column justify-content-center">
                <div wire:ignore class="handle-input"
                     x-show.transition.in.duration.800ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="isShowing2"
                     x-cloak>
                    <div  wire:loading wire:target="photo">
                        <x-control-panel.loading />
                    </div>
                    <label class="d-flex flex-row">
                        <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="photo" >
                    </label>
                    @error('photo') <span class="error">{{ $message }}</span> @enderror
                </div>

                <div class="d-flex flex-row justify-content-center">
                    <div wire:ignore
                         x-show.transition.in.duration.800ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="isShowing2"
                         x-cloak>
                        <button type="submit" class="btn btn-edit">  <i class="fa fa-edit " aria-hidden="true"></i> {{__("userFront.Edit")}}</button>
                    </div>
                </div>

            </div>
        </form>
    </div>

</div>
