<div x-data="{isPassword: @entangle('isPassword') }">

    <div class="d-flex flex-row justify-content-start">
        <div>
            <h4 class="address">{{__("userFront.changeMyPassword")}}</h4>
        </div>

        <div>
            <button :class="{ 'btn-edit': !isPassword ,'btn-Cancel': isPassword}" class="btn btn-edit-Cancel "
                    wire:click="toggleIsPassword()">
                <i class="fa fa-edit " aria-hidden="true" x-show="!isPassword"></i>
                <span x-show="!isPassword"> {{__("userFront.Edit")}}</span>
                <span x-show="isPassword" x-cloak> {{__("userFront.Cancel")}}</span>
            </button>
        </div>
    </div>

    <div
        x-show.transition.in.duration.800ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="isPassword"
        x-cloak>
        <div wire:loading wire:target="submitOldPassword">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="submitNewPassword">
            <x-control-panel.loading/>
        </div>
        @if($showOldPasswordFild)
            <form wire:submit.prevent="submitOldPassword">
                <div class="info">
                    <div class="user-input">
                        <label>{{__('userFront.OldPassword')}}</label>
                        <input type="password" class="form-control" wire:model.defer="oldPassword"/>
                        @error('oldPassword')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-center align-items-center mt-4">
                    <div class="btn-container">
                        <button type="submit" class="btn btn-primary-delivaz">
{{--                            <i class="fa fa-edit " aria-hidden="true"></i>--}}
                            {{__("userFront.Submit")}}
                        </button>
                    </div>
                </div>
            </form>
        @endif

        @if($showNewPasswordFild)
            <form wire:submit.prevent="submitNewPassword">
                <div class="info">
                    <div class="user-input">
                        <label>{{__('userFront.NewPassword')}}</label>
                        <input type="password" class="form-control" wire:model.defer="newPassword"/>
                        @error('newPassword')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="info">
                    <div class="user-input">
                        <label>{{__('userFront.NewPasswordConformation')}}</label>
                        <input type="password" class="form-control" wire:model.defer="newPasswordConformation"/>
                        @error('newPasswordConformation')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="d-flex flex-row justify-content-center width-full mt-3">
                    <div class="btn-container">
                        <button type="submit" class="btn btn-primary-delivaz">
{{--                            <i class="fa fa-edit " aria-hidden="true"></i>--}}
                            {{__("userFront.SaveChanges")}}
                        </button>
                    </div>
                </div>
            </form>
        @endif
    </div>
</div>
