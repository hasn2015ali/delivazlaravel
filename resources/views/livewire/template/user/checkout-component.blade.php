<div class="checkout-section"
     x-data="{showAddressSection: @entangle('showAddressSection'),
      showAddNewAddressSection: @entangle('showAddNewAddressSection'),
      showPaymentSection: @entangle('showPaymentSection'),
      showOrderSummerySection: @entangle('showOrderSummerySection')}">
    <div class="step-section">
        <div class="container">
            <div class="progress-pointer">
                <div :class="{ 'active': showAddressSection || showAddNewAddressSection,
                 'ready': showPaymentSection || showOrderSummerySection }"
                     class="content ">
                    @if($showPaymentSection || $showOrderSummerySection)
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16 8C16 10.1217 15.1571 12.1566 13.6569 13.6569C12.1566 15.1571 10.1217 16 8 16C5.87827 16 3.84344 15.1571 2.34315 13.6569C0.842855 12.1566 0 10.1217 0 8C0 5.87827 0.842855 3.84344 2.34315 2.34315C3.84344 0.842855 5.87827 0 8 0C10.1217 0 12.1566 0.842855 13.6569 2.34315C15.1571 3.84344 16 5.87827 16 8ZM12.03 4.97C11.9586 4.89882 11.8735 4.84277 11.7799 4.80522C11.6863 4.76766 11.5861 4.74936 11.4853 4.75141C11.3845 4.75347 11.2851 4.77583 11.1932 4.81717C11.1012 4.85851 11.0185 4.91797 10.95 4.992L7.477 9.417L5.384 7.323C5.24183 7.19052 5.05378 7.1184 4.85948 7.12183C4.66518 7.12525 4.47979 7.20397 4.34238 7.34138C4.20497 7.47879 4.12625 7.66418 4.12283 7.85848C4.1194 8.05278 4.19152 8.24083 4.324 8.383L6.97 11.03C7.04128 11.1012 7.12616 11.1572 7.21958 11.1949C7.313 11.2325 7.41305 11.2509 7.51375 11.2491C7.61444 11.2472 7.71374 11.2251 7.8057 11.184C7.89766 11.1429 7.9804 11.0837 8.049 11.01L12.041 6.02C12.1771 5.8785 12.2523 5.68928 12.2504 5.49296C12.2485 5.29664 12.1698 5.10888 12.031 4.97H12.03Z"
                                fill="#FAC24F"/>
                        </svg>
                    @endif
                    {{__("checkout.ShippmentDetails")}}
                </div>

                <div class="line">

                </div>
                <div :class="{ 'active': showPaymentSection,
                 'ready': showOrderSummerySection }"
                     class="content">

                    @if($showOrderSummerySection)
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M16 8C16 10.1217 15.1571 12.1566 13.6569 13.6569C12.1566 15.1571 10.1217 16 8 16C5.87827 16 3.84344 15.1571 2.34315 13.6569C0.842855 12.1566 0 10.1217 0 8C0 5.87827 0.842855 3.84344 2.34315 2.34315C3.84344 0.842855 5.87827 0 8 0C10.1217 0 12.1566 0.842855 13.6569 2.34315C15.1571 3.84344 16 5.87827 16 8ZM12.03 4.97C11.9586 4.89882 11.8735 4.84277 11.7799 4.80522C11.6863 4.76766 11.5861 4.74936 11.4853 4.75141C11.3845 4.75347 11.2851 4.77583 11.1932 4.81717C11.1012 4.85851 11.0185 4.91797 10.95 4.992L7.477 9.417L5.384 7.323C5.24183 7.19052 5.05378 7.1184 4.85948 7.12183C4.66518 7.12525 4.47979 7.20397 4.34238 7.34138C4.20497 7.47879 4.12625 7.66418 4.12283 7.85848C4.1194 8.05278 4.19152 8.24083 4.324 8.383L6.97 11.03C7.04128 11.1012 7.12616 11.1572 7.21958 11.1949C7.313 11.2325 7.41305 11.2509 7.51375 11.2491C7.61444 11.2472 7.71374 11.2251 7.8057 11.184C7.89766 11.1429 7.9804 11.0837 8.049 11.01L12.041 6.02C12.1771 5.8785 12.2523 5.68928 12.2504 5.49296C12.2485 5.29664 12.1698 5.10888 12.031 4.97H12.03Z"
                                fill="#FAC24F"/>
                        </svg>

                    @endif
                    {{__("checkout.Payment")}}
                </div>
                <div class="line">

                </div>
                <div :class="{ 'active': showOrderSummerySection}"
                     class="content">
                    {{__("checkout.OderSummury")}}
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="user-addresses"
             x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="showAddressSection">
            <livewire:template.user.checkout.addresses-component/>

            <div class="d-flex justify-content-center mt-5">
                <div wire:loading wire:target="goToPayment">
                    <x-control-panel.loading/>
                </div>
                <a href="#" class="btn btn-primary-gradient"
                   wire:loading.remove wire:target="goToPayment"
                   wire:click.prevent="goToPayment">
                    {{__("checkout.Next")}}
                </a>
            </div>

        </div>


        <div class="edit-new-address"
             x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="showAddNewAddressSection" x-cloak>
            <div class="btn-container">
                <div wire:loading wire:target="goBackToAddresses">
                    <x-control-panel.loading/>
                </div>
                <a href="#" class="btn btn-back"
                   wire:loading.remove wire:target="goBackToAddresses"
                   wire:click.prevent="goBackToAddresses">
                    <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M11.0629 2.47711C11.4534 2.08658 11.4534 1.45342 11.0629 1.06289L10.7071 0.707107C10.3166 0.316582 9.68342 0.316582 9.29289 0.707107L0.707106 9.29289C0.316582 9.68342 0.316583 10.3166 0.707107 10.7071L9.29289 19.2929C9.68342 19.6834 10.3166 19.6834 10.7071 19.2929L11.0629 18.9371C11.4534 18.5466 11.4534 17.9134 11.0629 17.5229L4.24711 10.7071C3.85658 10.3166 3.85658 9.68342 4.24711 9.29289L11.0629 2.47711Z"
                            fill="black"/>
                    </svg>
                    <span>{{__("checkout.BackAddresses")}}</span>
                </a>
            </div>

            <livewire:template.user.checkout.add-new-address-component/>
        </div>

        <div class="payment-section"
             x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="showPaymentSection" x-cloak>

            <div class="btn-container">
                <div wire:loading wire:target="goBackToAddresses">
                    <x-control-panel.loading/>
                </div>
                <button class="btn btn-back"
                        wire:loading.remove wire:target="goBackToAddresses"
                        wire:click.prevent="goBackToAddresses">
                    <svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M11.0629 2.47711C11.4534 2.08658 11.4534 1.45342 11.0629 1.06289L10.7071 0.707107C10.3166 0.316582 9.68342 0.316582 9.29289 0.707107L0.707106 9.29289C0.316582 9.68342 0.316583 10.3166 0.707107 10.7071L9.29289 19.2929C9.68342 19.6834 10.3166 19.6834 10.7071 19.2929L11.0629 18.9371C11.4534 18.5466 11.4534 17.9134 11.0629 17.5229L4.24711 10.7071C3.85658 10.3166 3.85658 9.68342 4.24711 9.29289L11.0629 2.47711Z"
                            fill="black"/>
                    </svg>
                    <span>{{__("checkout.BackShipping")}}</span>
                </button>
            </div>

            <livewire:template.user.checkout.payment-component
                :totalForItemCart="$totalForItemCart"
                :cashCost="$cashCost"
                :atDoorCost="$atDoorCost"
                :asapCost="$asapCost"
                :scheduleCost="$scheduleCost"
                :deliveryCharge="$deliveryCharge"
                :country="$country"
                :cityName="$cityName"
                :countryName="$countryName"
                :itemsInCart="$itemsInCart"/>
            <div wire:loading wire:target="goToOrderSummery">
                <x-control-panel.loading/>
            </div>
        </div>

        <div class="order-summery-section"
             x-show.transition.in.duration.800ms.opacity.out.duration.10ms.opacity="showOrderSummerySection" x-cloak>

            <livewire:template.user.checkout.order-summary-component
                :totalForItemCart="$totalForItemCart"
                :cashCost="$cashCost"
                :atDoorCost="$atDoorCost"
                :asapCost="$asapCost"
                :scheduleCost="$scheduleCost"
                :deliveryCharge="$deliveryCharge"
                :country="$country"
                :cityName="$cityName"
                :countryName="$countryName"
                :itemsInCart="$itemsInCart"/>
        </div>

    </div>
    @push('custom-scripts')
        <script>
            Livewire.on('goBackToAddresses', () => {
                // console.log('goBackToAddresses');
            @this.set('showAddressSection', true);
            @this.set('showAddNewAddressSection', false);
            @this.set('showPaymentSection', false);
            @this.set('showOrderSummerySection', false);
                window.scrollTo(0, 0);
            })
        </script>
    @endpush
</div>
