<div class="anything-section"
     x-data="{ openPhotoToast: @entangle('openPhotoToast'),
      less: @entangle('less'),
      between: @entangle('between'),
      between500: @entangle('between500'),
      between800: @entangle('between800'),}">
    @include('livewire.template.any-thing.add-photo-toast')

    <div class="container">
        <div class="row any-thing-container">
            <div class="col-lg-4 left-side">

                <div class="d-flex flex-row justify-content-start">
                    <h4 class="address">
                        {{__('anyThing.ExpressDelivery')}}
                    </h4>
                </div>
                <div class="img">
                    <img src="{{asset('/templateIMG/anyThing/1.png')}}">
                </div>
            </div>
            <div class="col-lg-8 rigth-side">
                <div class="d-flex flex-column justify-content-start">
                    <h6 class="text-1">
                        {{__('anyThing.What_we_deliver')}}
                    </h6>
                    <p class="text-2">
                        {{__('anyThing.It_can_anything')}}
                    </p>
                </div>
                <div class="d-flex flex-row justify-content-start text-area-container">
                    <textarea wire:model.defer="anyThingText" class="form-control" rows="3"></textarea>
                    @error('address')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}
                    </div>
                    @enderror
                    <div class="btn-container d-flex flex-row align-items-center justify-content-end">
                        <button class="btn btn-link-delivaz"
                                @click="openPhotoToast=true"> {{__('anyThing.Attach_photo')}}</button>
                        @if(!$confrimed)
                            <i class="fas fa-image fa-2x"></i>
                        @endif
                        @if ($photo and $confrimed)
                            <div class="img">
                                <img src="  {{ $photo->temporaryUrl() }}">
                            </div>
                        @endif
                    </div>

                </div>

                <div class="d-flex flex-column justify-content-start">
                    <h6 class="text-1">
                        {{__('anyThing.EstimatedProductCost')}}
                    </h6>
                    <p class="text-3">
                        {{__('anyThing.Select_an_option')}}
                    </p>
                </div>
                <div wire:loading wire:target="setLessActive">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="setBetweenActive">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="setBetween500Active">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="setBetween800Active">
                    <x-control-panel.loading/>
                </div>
                <div class="d-flex flex-wrap justify-content-start btn-container">
                    <a href="#" class="btn btn-option" :class="less ? 'btn-option-active ' : ''"
                       @click.prevent="$wire.setLessActive()">
                        {{__('anyThing.Less_than')}}
                        <br/>
                        {{__('anyThing.150_UAH')}} &nbsp;&nbsp;{{$currencyCode}}
                    </a>
                    <a href="#" class="btn btn-option" :class="between ? 'btn-option-active ' : ''"
                       @click.prevent="$wire.setBetweenActive()">
                        {{__('anyThing.Between')}}
                        <br/>
                        {{__('anyThing.150_500_UAH')}} &nbsp;&nbsp;{{$currencyCode}}
                    </a>
                    <a href="#" class="btn btn-option" :class="between500 ? 'btn-option-active ' : ''"
                       @click.prevent="$wire.setBetween500Active()">
                        {{__('anyThing.Between')}}
                        <br/>
                        {{__('anyThing.500_800_UAH')}} &nbsp;&nbsp;{{$currencyCode}}
                    </a>
                    <a href="#" class="btn btn-option" :class="between800 ? 'btn-option-active ' : ''"
                       @click.prevent="$wire.setBetween800Active()">
                        {{__('anyThing.Between')}}
                        <br/>
                        {{__('anyThing.800_1000_UAH')}} &nbsp;&nbsp;{{$currencyCode}}
                    </a>
                </div>

                <div class="d-flex flex-column justify-content-start">
                    <h6 class="text-1">
                        {{__('anyThing.Where_From')}}
                    </h6>
                    <p class="text-3">
                        {{__('anyThing.Add_an_Address')}}
                    </p>
                </div>
                <div class="d-flex flex-row justify-content-start user-input align-items-center">
                    <i class="fas fa-flag"></i> <input type="text" class="form-control "
                                                       placeholder=" {{__('anyThing.addressEX')}}"
                                                       wire:model.defer=""/>
                    @error('firstName')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>

                <div class="d-flex flex-column justify-content-start">
                    <h6 class="text-1">
                        {{__('anyThing.WhereTo')}}
                    </h6>
                    <p class="text-3">
                        {{__('anyThing.Add_an_Address')}}
                    </p>
                </div>
                <div class="d-flex flex-row justify-content-start user-input align-items-center">
                    <i class="fas fa-flag"></i> <input type="text" class="form-control "
                                                       placeholder=" {{__('anyThing.addressEX')}}"
                                                       wire:model.defer=""/>
                    @error('firstName')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
                <div class="user-info">
                    <div class="d-flex flex-column justify-content-start mb-2">
                        <h6 class="text-1">
                            {{__('anyThing.CustomerInformation')}}
                        </h6>

                    </div>
                    <div wire:loading wire:target="setSlot">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="setDay">
                        <x-control-panel.loading/>
                    </div>

                    <div class="d-flex flex-row justify-content-start user-input align-items-center">
                        <i class="fas fa-user"></i>
                        <input type="text"
                               class="form-control "
                               placeholder=" {{__('anyThing.FirstName')}}"
                               wire:model.defer=""/>
                        @error('firstName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="d-flex flex-row justify-content-start user-input align-items-center">
                        <i class="fa fa-phone fa-rotate-90"></i>
                        <input type="text"
                               class="form-control "
                               placeholder=" {{__('anyThing.Mobile_Number')}}"
                               wire:model.defer=""/>
                        @error('firstName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="d-flex flex-row justify-content-start user-input align-items-center">
                        <i class="fas fa-clock"></i>
                        {{--                        <input type="text"--}}
                        {{--                               class="form-control "--}}
                        {{--                               placeholder=" {{__('anyThing.ASAP')}}"--}}
                        {{--                               wire:model.defer=""/>--}}
                        <div class="dropdown ">
                            <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                    data-toggle="dropdown">
                                @if($selectedDay)
                                {{$selectedDay}}
                                @else
                                {{__('anyThing.ScheduleOrder')}}
                                @endif
                            </button>
                            <div class="dropdown-menu">
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item"  wire:click="setDay('ASAP')">  {{__('anyThing.ASAP')}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('Today')">  {{__('anyThing.Today')}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('Tomorrow')">  {{__('anyThing.Tomorrow')}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('{{$nextTwo}}')"> {{$nextTwo}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('{{$nextThree}}')"> {{$nextThree}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('{{$nextFour}}')">  {{$nextFour}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('{{$nextFive}}')">  {{$nextFive}}</p>
                                </a>
                                <a href="#"
                                   wire:click.prevent="">
                                    <p class="dropdown-item" wire:click="setDay('{{$nextSex}}')">  {{$nextSex}}</p>
                                </a>

                            </div>
                        </div>
                        @error('firstName')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    @if($showSlots)
                        <div class="d-flex flex-row justify-content-start user-input align-items-center">
                            <i class="fas fa-clock"></i>
                            <div class="dropdown ">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if($selectedSlot)
                                    {{$selectedSlot}}
                                    @else
                                        {{__('anyThing.SelectStartTime')}}

                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    @foreach($daySlots as $key=>$slot)
                                    <a href="#"
                                       wire:click.prevent="setSlot('{{$slot}}')">
                                        <p class="dropdown-item">  {{$slot}}</p>
                                    </a>
                                    @endforeach

                                </div>
                            </div>
                            @error('firstName')
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                </div>

                <div class="map">
                    <div class="img">
                        <img src="{{asset('/templateIMG/anyThing/map1.png')}}">
                    </div>
                </div>
                <div class="Place-Order d-flex flex-column align-items-start justify-content-start">
                    <button class="btn btn-primary-delivaz">
                        {{__('anyThing.PlaceOrder')}}
                    </button>
                    <p class="text-3">
                        {{__('anyThing.Place')}}
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>
