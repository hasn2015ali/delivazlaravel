<div class="user-cart-check-toast"
     x-show.transition.in.duration.1500ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openPhotoToast"
     @click.away="$wire.closeToast()" x-cloak>
    <div class="cart-header d-flex flex-row justify-content-between ">
        <div class="mt-1">
            <h4 class="toast-address">{{__("anyThing.addPhoto")}}</h4>
        </div>
        <div>
            <button class="btn btn-close" @click="$wire.closeToast()"><i class="fas fa-times "></i></button>
        </div>
    </div>
    <div class="cart-content">

        <div class="user-input">

            <div class="d-flex flex-row image-section">
                <div class="d-flex flex-column ">
                    <div wire:loading wire:target="photo">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="removePhoto">
                        <x-control-panel.loading/>
                    </div>

{{--                    <label>{{__('anyThing.AddImage')}}</label>--}}
                    <div class="input-file-container">
                        <input type="file" class="form-control personal-image-input"
                               wire:model.defer="photo"/>
                        @error('photo')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                        <div class="input-handel">
                            <i class="fas fa-image fa-2x"></i>
                            <p class="text1">{{__('anyThing.DragHere')}} <span
                                    class="brows">{{__('anyThing.browse')}}</span> {{__('anyThing.toUpload')}}
                            </p>
                        </div>
                    </div>

                </div>
                @if ($photo)
                    <div class="img">
                        <img src="  {{ $photo->temporaryUrl() }}">
                    </div>
                @endif
            </div>

        </div>



        <div class="d-flex flex-row justify-content-between btn-container">
            <button class="btn btn-yes"  wire:click="addPhoto">
                {{__("anyThing.add")}}
            </button>
            <button class="btn btn-no" wire:click="removePhoto">
                {{__("anyThing.remove")}}
            </button>

        </div>

    </div>


    {{-- Stop trying to control. --}}
</div>
