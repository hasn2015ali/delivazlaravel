<div class="city-restaurant-banners-section mt-5">
    {{--    <img  src="{{asset('storage/images/sliders/city/city.png')}}">--}}

    <div class="container">

        @if(count($restaurantBanners)!=0)
        <div class="d-flex flex-row justify-content-center">
            <h2 class="address mb-3">
                {{__("city.bestRestaurant")}} {{$city}}
            </h2>
        </div>
        <div class="owl-carousel city-restaurant-banners owl-theme">

                @foreach($restaurantBanners as $restaurant)
{{--                    {{dd($restaurant->serviceProvider->slug)}}--}}
                    <div class="item gap-10">
                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$restaurant->serviceProvider->slug])}}">
                            <img src="{{asset('storage/images/sliders/recommended/'.$restaurant->photo)}}">
                        </a>
                        <h2 class="address2"> {{$restaurant->serviceProvider->name}}</h2>
                        <p class="content">{{$restaurant->content}}</p>
                    </div>
                @endforeach

        </div>
        @endif
    </div>
    <script>
        $(document).ready(function () {
            $('.city-restaurant-banners').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: true,

                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    500: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            })
        });
    </script>
    <style>
        .owl-carousel .item {
            /*display: block;*/
            /*width: calc(150px + 15vw);*/
            /*min-width: 250px;*/
        }

        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: 3vw !important;
            height: 0.8vh !important;
        }

        /*.card-header{*/
        /*    border-radius: 15px!important;*/

        /*}*/
        /*.owl-carousel img{*/

        /*        border-radius: 15px!important;*/


        /*}*/
    </style>
</div>

