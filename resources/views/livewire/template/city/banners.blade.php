<div class="city-banners-section ">
    {{--    <img  src="{{asset('storage/images/sliders/city/city.png')}}">--}}

    <div class="container-fluid ">
        <div class="owl-carousel city-banners owl-theme">
            @if($cityBanners)
                @foreach($cityBanners as $banner)
                    @if($banner->type=="restaurant")
                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$banner->serviceProvider->slug])}}">
                            @elseif($banner->type=="shop")
                                <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$banner->serviceProvider->slug])}}">
                                    @endif
                                    <div class="item">
                                        <img width="1000" height="315" loading="lazy"
                                            src="{{asset('storage/images/sliders/city/'.$banner->name)}}"></div>
                                </a>
                        @endforeach
                    @endif
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.city-banners').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: true,

                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 1
                    },
                    1000: {
                        items: 1
                    }
                }
            })
        });
    </script>
    <style>
        .owl-carousel .item {
            z-index: 100 !important;
        }

        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: 3vw !important;
            height: 0.8vh !important;
        }

        /*.card-header{*/
        /*    border-radius: 15px!important;*/

        /*}*/
        /*.owl-carousel img{*/

        /*        border-radius: 15px!important;*/


        /*}*/
    </style>
</div>

