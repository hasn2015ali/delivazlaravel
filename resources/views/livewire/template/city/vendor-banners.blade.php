<div class="city-restaurant-banners-section mt-5">
    {{--    <img  src="{{asset('storage/images/sliders/city/city.png')}}">--}}

    <div class="container">
        @if(count($vendorBanners)!=0)
        <div class="d-flex flex-row justify-content-center">
            <h2 class="address mb-3">
                {{__("city.bestVendors")}} {{$city}}
            </h2>
        </div>
        <div class="owl-carousel city-vendor-banners owl-theme">

                @foreach($vendorBanners as $vendor)
                    <div class="item gap-10">
                        <a href="{{route('Shop',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$vendor->serviceProvider->slug])}}">

                            <img src="{{asset('storage/images/sliders/recommended/'.$vendor->photo)}}">
                        </a>
                        <h2 class="address2"> {{$vendor->serviceProvider->name}}</h2>
                        <p class="content">{{$vendor->content}}</p>
                    </div>
                @endforeach

        </div>
        @endif
    </div>
    <script>
        $(document).ready(function () {
            $('.city-vendor-banners').owlCarousel({
                autoplay: true,
                autoplayHoverPause: true,
                smartSpeed: 250,
                loop: true,
                // margin:10,
                center: true,
                // // nav:true,
                dots: true,
                center: true,

                // items:1,
                margin: 0,
                stagePadding: 0,
                // smartSpeed:450,
                animateOut: 'fadeOut',
                // autoWidth:true,
                // animateOut: 'slideOutDown',
                // animateIn: 'flipInX',
                responsive: {
                    0: {
                        items: 1
                    },
                    500: {
                        items: 2
                    },
                    1000: {
                        items: 3
                    }
                }
            })
        });
    </script>
    <style>

        .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
            background: #F96332 !important;
        }

        .owl-theme .owl-dots span {
            width: calc(9px + 2.5vw) !important;
            height: calc(3px + 0.1vw) !important;
        }

        /*.card-header{*/
        /*    border-radius: 15px!important;*/

        /*}*/
        /*.owl-carousel img{*/

        /*        border-radius: 15px!important;*/


        /*}*/
    </style>
</div>

