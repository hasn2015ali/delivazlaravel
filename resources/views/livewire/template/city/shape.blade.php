<div class="shape-section mt-5" x-data="dropdownShape()" @open-shape.window="show = true">
    <?php
    $lang = \Illuminate\Support\Facades\App::getLocale();
    ?>
    <div class="container">
        {{--        <div class="d-flex flex-column align-items-center justify-content-center shape-12 ">--}}
        <div class="d-flex flex-wrap align-items-center justify-content-center shape-12  ">

            @foreach($cityShape as $shape)
                <?php $transEN= $shape->translation->where('code_lang','en')->first()->trans;
//                      $slug=\Illuminate\Support\Str::slug($transEN);
                      $slug= str_replace(' ', '-', $transEN);
//                      echo $transEN."<br/>".$slug;
                  ?>

                @if($shape->state!=0)

                    @if($shape->url=="anything")
                        <a class="gap-0-10"
                           href="{{route('AnyThing',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city])}}">
                    @elseif($shape->url==null)

                        @if($shape->content[0]['type']=="filter")
{{--                                    {{ url('/home_services_under_department/'.$data->name) }--}}
                                     <a class="gap-0-10" href="{{url(app()->getLocale().'/'.$country.'/'.$city.'/'.$slug)}}" >
{{--                                         <a class="gap-0-10" href="{{route('Items-In',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'shape'=>$slug])}}" >--}}

                        @elseif($shape->content[0]['type']=="restaurant")
{{--                                     $wire.emit('openShape', id);--}}
{{--                                <a href="#" class="open-model gap-0-10"   x-on:click="open()">--}}
                                     <a href="#" class="open-model gap-0-10" wire:click.prevent="openShape({{$shape->id}})">

                                     @endif
                    @else
                         <a class="gap-0-10" href="{{$shape->url}}">

                    @endif
                    <div
                        class="shape d-flex flex-column align-items-center justify-content-center">
                        <img src="{{asset('/storage/images/shape/'.$shape->icon)}}">
                        <p class="text">
                            @if($shape->translation->where('code_lang',$lang)->first())
                                {{$shape->translation->where('code_lang',$lang)->first()->trans}}
                            @else
                                {{$transEN}}

                            @endif

                        </p>
                    </div>
                    </a>
                @endif

            @endforeach




        </div>


    </div>

    <div class="shape-modle"
         x-show.transition.in.duration.1500ms.origin.center.center.scale.50.out.duration.1200ms.origin.center.center.scale.30="isOpen()"
         @click="close()" @keydown.escape="close()" x-cloak>
        <div class="d-flex flex-column align-items-center justify-content-center Restaurants">

            <span class="close close-shape-model" @click="close()">&times;</span>
            <div class="d-flex flex-row flex-wrap shape-container justify-content-center align-items-center">

{{--                @if($restaurantInShape)--}}
{{--                    @foreach($restaurantInShape as $res)--}}
{{--                        <a href="{{route('Restaurant',['locale'=>app()->getLocale(),'country'=>$country,'city'=>$city,'serviceProvider'=>$res->serviceProvider->id])}}">--}}

{{--                            <div class="Restaurants-shape  d-flex flex-column align-items-center justify-content-center">--}}

{{--                                    --}}{{--                            <img src="{{asset('/templateIMG/shapes/Express_Delivery.png')}}">--}}
{{--                                    <p class="text">{{$res->serviceProvider->name}}</p>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                    @endforeach--}}
{{--                @endif--}}


            </div>

        </div>

    </div>

    <script >
        function dropdownShape() {
            // let shape_modal = document.querySelector(".shape-modle");
            return {
                show: false,
                open() {

                    this.show = true;

                    // shape_modal.style.display = "block";
                },
                close() {
                    this.show = false
                },
                isOpen() {
                    return this.show === true
                },
            }
        }



    </script>
    <style>
        [x-cloak] {
            display: none !important;
        }
    </style>

</div>
