<div class="header-section ">


    <div class="header-top">
        <div class="container">
            <div class="d-flex flex-row justify-content-between pt-3 pb-2 first">
                <div class="d-flex flex-row justify-content-end ">
                    <div class="gap-15">
                        <i class="fa fa-phone fa-rotate-90" aria-hidden="true"></i>
                        +123 123 123
                    </div>
                    <div class="gap-15">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        support@delivaz.com
                    </div>

                </div>
                <div style="" class="d-flex flex-row justify-content-start ">
                    <div class="gap-15">

                        @if(isset($city))  <i class="fas fa-map-marker-alt"></i> {{$city}} @endif
                        @if(isset($country) && !isset($city))  <i class="fas fa-map-marker-alt"></i> {{$country}} @endif
                    </div>
                    <div class="dropdown trace-order gap-15">
                        <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-truck" aria-hidden="true"></i>
                            {{__("home.Track_Your_Order")}}
                        </button>
                        <div class="dropdown-menu trace-order-menu">
                            <div class="trace-order-container">
                                <div class="shape"></div>

                                <div class="d-flex flex-column justify-content-start parent">

                                    <div class="d-flex flex-row justify-content-start ">
                                        <h4 class="address">
                                            {{__("userFront.Track_my_order")}}
                                        </h4>
                                    </div>

                                    <div class="d-flex flex-row justify-content-start">
                                        <div class="form-group">
                                            <label class="text">  {{__("userFront.Enter_your_Phone_Number")}}</label>
                                            <input class="form-control">
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row justify-content-start">
                                        <div class="form-group">
                                            <label class="text">  {{__("userFront.Your_Order_Number")}}</label>
                                            <input class="form-control">
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row justify-content-start">
                                        <div class="form-group">
                                            <button type="submit"
                                                    class="btn btn-warning pr-5 pl-5 pt-1 pb-1">{{__("userFront.Send")}}</button>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-row justify-content-start">
                                        <div class="form-group">
                                            <label class="text">  {{__("userFront.For_any_other_inquires")}}
                                                <span class="number">{{__("userFront.number")}}</span>
                                            </label>
                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>


                    <div class="seprator gap-15">
                        |
                    </div>
                    <div class="dropdown gap-15">
                        <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                            $ Dollar (US)
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Link 1</a>
                            <a class="dropdown-item" href="#">Link 2</a>
                            <a class="dropdown-item" href="#">Link 3</a>
                        </div>
                    </div>

                    <div class="dropdown gap-15">
                        <button type="button" class="btn  dropdown-toggle" data-toggle="dropdown">
                            En
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item"
                               href="{{str_replace("/".app()->getLocale(),"/en", url()->current() )}}">English</a>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="second-header mt-2">
        <div class="container">
            <div style="position: relative" class="d-flex flex-row justify-content-between">
                <div class="log">
                    <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}">

                        <img loading="lazy" src="{{asset('templateIMG/logo1.png')}}" class="img-logo">
                    </a>
                </div>

                <div class="other mt-3">
                    <livewire:template.index.search-component/>


                    @guest

                        <a href="{{route('register',['locale'=>app()->getLocale()])}}" class="btn btn-main gap-10 ">
                            {{__("home.join_now")}}
                        </a>

                        <a href="{{route('login',['locale'=>app()->getLocale()])}}" class="btn btn-main gap-10">
                            <i class="fas fa-user-circle"></i> {{__("home.sign_in")}}
                        </a>
                    @else


                        <a class="nav-link dropdown-toggle btn-user-menu  gap-10" id="navbarDropdownMenuLink1"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-circle mr-1 ml-1"></i>
                            {{auth()->user()->firstName}}
                            @if($counter)
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                <span class="badge badge-warning card-count badge-pill">{{$counter}}</span>
                            @endif


                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink1">
                            <a class="dropdown-item"
                               href="{{ route('Dashboard',['locale'=>app()->getLocale()]) }}">
                                {{__("userFront.Dashboard")}}
                            </a>
                            @if(auth()->user()->role_id==1||auth()->user()->role_id==9||auth()->user()->role_id==10)

                                @if(isset(auth()->user()->email) && !auth()->user()->email_verified_at)
                                    <a class="dropdown-item"
                                       href="{{ route('Verify-My-Email',['locale'=>app()->getLocale()]) }}">
                                        {{__("userFront.verifyMyEmail")}}
                                    </a>
                                @endif
                            @endif

                            @if(auth()->user()->role_id==1||auth()->user()->role_id==9||auth()->user()->role_id==10)

                                @if(isset(auth()->user()->phone))
                                    @if(auth()->user()->phone!=0  && !auth()->user()->phone_verified)
                                        <a class="dropdown-item"
                                           href="{{ route('Verify-My-Phone',['locale'=>app()->getLocale()]) }}">
                                            {{__("userFront.PhoneMyVerification")}}
                                        </a>
                                    @endif
                                @endif
                            @endif

                            <a class="dropdown-item" href="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{__("userFront.Logout")}}
                            </a>

                            <form id="logout-form" action="{{ route('logout',['locale'=>app()->getLocale()]) }}"
                                  method="POST">
                                @csrf
                            </form>

                        </div>
                    @endguest

                    <livewire:template.user.cart-component :country="$country" :city="$city"/>


                </div>

                <button class="btn-menu-mobile mt-1">
                    <span class="close-icon-header"> <i class="fas fa-times"></i></span>
                    <span class="open-icon-header show-on-mobil"><i style="" class="fa fa-bars" aria-hidden="true"></i></span>
                </button>
            </div>
        </div>

    </div>

</div>
