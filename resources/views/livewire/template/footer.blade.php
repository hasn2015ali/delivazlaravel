<div class="footer-section ">
    <div class="container">
        <div class="d-flex flex-row footer-parent ">


            <div class="d-flex flex-row  left-side">
                <div class="d-flex flex-row " style="width: 100%">


                    <div class="d-flex flex-column align-items-start contact-info mr-4">
                        <div class="log">
                            <a href="/{{\Illuminate\Support\Facades\App::getLocale()}}">
                                <img loading="lazy" src="{{asset('templateIMG/logo1.png')}}" class="img-logo">
                            </a>
                        </div>
                        <div class="d-flex flex-row justify-content-start mt-3">
                            <p class="text ">
                                {{__("home.lorem")}}
                            </p>
                        </div>
                        <div class="d-flex flex-row justify-content-start mt-3">
                            <a href="#" class="btn btn-youtube">
                                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                            </a>

                            <a href="#" class="btn">
                                <i class="fab fa-linkedin-in"></i>
                            </a>

                            <a href="#" class="btn">
                                <i class="fab fa-twitter"></i>
                            </a>

                            <a href="#" class="btn">
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="btn">
                                <i class="fa fa-instagram" aria-hidden="true"></i>

                            </a>
                        </div>
                    </div>
                    <div class="img-dash ">
                        <img loading="lazy" src="{{asset('/templateIMG/footer/dish1.png')}}">

                    </div>
                </div>
            </div>
            <div class="d-flex flex-row right-side">
                <div style="" class="d-flex flex-column first-child">

                    <div class="d-flex flex-row justify-content-start">
                        <p class="text1"> {{__("home.newsLetter")}} </p>
                    </div>
                    <div class="d-flex flex-row justify-content-start">
                        <p class="text2">
                            {{__("home.subscribe_and_get_our_lastest_offer")}}
                            {{__("home.and_stay_updated")}}
                        </p>
                    </div>
                    <div wire:loading wire:target="getCities">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="submitSubscriber">
                        <x-control-panel.loading/>
                    </div>
                    <form wire:submit.prevent="submitSubscriber">
                        <div class="d-flex flex-row justify-content-start country">

                            <div class="dropdown ">
                                <button id="dropdown_selecte_btn" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if(empty($selectedCountry))
                                        <small>    {{__("home.selectCountry")}}</small>

                                    @else
                                        {{$selectedCountry }}
                                    @endif
                                </button>
                                <div class="dropdown-menu">


                                    @foreach($countries as $item)

                                        <a href="#"
                                           wire:click.prevent="getCities({{ $item->country_id }}, '{{ $item->name }}')">
                                            <p class="dropdown-item">{{$item->name}}</p>
                                        </a>
                                    @endforeach


                                </div>
                            </div>


                            <div class="dropdown " id="cities_list">
                                <button id="dropdown_selecte_btn_city" type="button" class="btn dropdown-toggle"
                                        data-toggle="dropdown">
                                    @if($selectedCountry)
                                        @if($selectedCity)
                                            {{$selectedCity}}
                                        @else
                                            <small> {{__("home.selectCity")}}</small>
                                        @endif

                                    @else
                                        <small> {{__("home.selectCity")}}</small>
                                    @endif
                                </button>
                                <div class="dropdown-menu">
                                    @if(!empty($selectedCountry))
                                        {{--                                        {{dd($cities)}}--}}
                                        {{--                                        <p class="dropdown-item"  >---</p>--}}

                                        @foreach($cities as $item)

                                            <a href="#"
                                               wire:click.prevent="sendCityVal({{ $item->city->id }}, '{{ $item->name }}')">
                                                <p class="dropdown-item">{{$item->name}}</p>

                                            </a>
                                        @endforeach
                                    @else
                                        <p class="dropdown-item"><small> {{__("home.selectCountryFirst")}}</small></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <div class="form-group">
                                <input wire:model.defer="userName" class="form-control"
                                       placeholder="{{__("home.type_your_name")}}">
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-start">
                            <div class="form-group">
                                <input type="email" wire:model.defer="email" class="form-control"
                                       placeholder="{{__("home.type_your_e-mail")}}">

                            </div>
                        </div>


                        <div class="d-flex flex-row justify-content-start subscribe-container">
                            <button type="submit" class="btn subscribe">{{__("home.subscribe")}}</button>
                        </div>

                    </form>
                </div>


            </div>
        </div>

    </div>


    <div class="section-svg">
        <div class="svg-content">
            <div class="container">
                <hr style="background: #fff"/>
                <div class="d-flex flex-row parent">
                    <div class="d-flex flex-column justify-content-start align-items-start first-child">
                        <div class="d-flex flex-row justify-content-start gap-0-10">
                            <h6 class="address1">  {{__("home.links")}}</h6>
                        </div>
                        <div class="d-flex flex-row justify-content-start gap-0-5">
                        <span class="text1 gap-5">
                            <a class="btn-link" href="{{route('About-Us',['locale'=>app()->getLocale()])}}">
                                {{__("home.about")}}
                            </a>
                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                            {{__("home.sustainability")}}
                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                                @if($country==null)
                                    <a class="btn-link" href="/{{app()->getLocale()}}/Terms-Conditions">
                                @elseif($country!=null)
                                            <a class="btn-link"
                                               href="/{{app()->getLocale()}}/{{$country}}/Terms-Conditions">
                                @endif

                                                {{__("home.accessibilty")}}
                            </a>
                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                                         <a class="btn-link"
                                            href="{{route('Register-Shop-Or-Restaurant',['locale'=>app()->getLocale()])}}">
                             {{__("home.coustomer_care")}}
                            </a>

                        </span>


                        </div>
                        {{--                        {{dd($country)}}--}}
                        <div class="d-flex flex-row justify-content-start gap-0-5">
                        <span class="text1 gap-5">
                                 <a class="btn-link"
                                    href="{{route('FAQ',['locale'=>app()->getLocale()])}}">
                              {{__("home.FAQ")}}
                            </a>

                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                                              @if($country==null)
                                    <a class="btn-link" href="/{{app()->getLocale()}}/Privacy-Policy">
                                @elseif($country!=null)
                                            <a class="btn-link"
                                               href="/{{app()->getLocale()}}/{{$country}}/Privacy-Policy">
                                @endif
                                                {{__("home.shipping_and_returns")}}
                            </a>

                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                                                  @if($country==null)
                                    <a class="btn-link" href="/{{app()->getLocale()}}/Cookies-Policy">
                                @elseif($country!=null)
                                            <a class="btn-link"
                                               href="/{{app()->getLocale()}}/{{$country}}/Cookies-Policy">
                                @endif
                                                {{__("home.store_policy")}}
                            </a>

                        </span>
                            <span class="gap-5 text1">|</span>
                            <span class="text1 gap-5">
                            {{__("home.store_locator")}}
                        </span>

                        </div>

                    </div>

                    <div class="d-flex flex-column justalign-items-start align-items-start second-child">
                        <div class="d-flex flex-row justify-content-start gap-0-10 pull-right1">
                            <h6 class="address1"> {{__("home.contact")}}</h6>
                        </div>
                        <div class="d-flex flex-row justify-content-center gap-0-5 pull-right1">
                            <div class="gap-10 text1">
                                <i class="fas fa-phone-alt"></i>
                            </div>
                            <div class="gap-10">
                                <p class="text1">+ 123 456 789</p>
                            </div>

                            <div class="btn-chat gap-25">
                                <button class="btn text1"> {{__("home.LIVE_CHAT")}} </button>
                            </div>

                        </div>


                    </div>

                </div>

            </div>
        </div>
        <svg viewBox="0 0 450 120" preserveAspectRatio="none">
            <defs>
                <linearGradient id="gradient">
                    <!-- <stop offset="0%" stop-color="#f1c144" />
                    <stop offset="100%" stop-color="#fF5034" /> -->
                    <stop offset="0%" stop-color="rgba(241, 195, 67,1)"/>
                    <stop offset="100%" stop-color="rgba(240, 75, 40,1)"/>

                </linearGradient>
            </defs>
            <path d="M-0.84,1.48 C104.68,48.84 202.88,36.02 501.41,2.47 L500.00,150.00 L0.00,150.00 Z"
                  style="stroke: none;" fill="url(#gradient)"></path>
        </svg>
    </div>
    <div class="all-right">
        <div class="container">
            <div class="d-flex flex-row parent" style="width: 100%">
                <div class="d-flex flex-row justify-content-start align-items-start first">
                    <p class="text1"> DELIVAZ - &copy;<script>document.write(new Date().getFullYear())</script> {{__("home.all_rights_reserved")}}
                    </p>
                </div>
                <div class="d-flex flex-row justify-content-center second">
                    {{--                    <div class="gap-10">--}}
                    {{--                        <p class="text1">--}}
                    {{--                            {{__("home.Payment")}}--}}
                    {{--                        </p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="gap-10">--}}
                    {{--                        <i style="color: #1A1F71;" class="fab fa-cc-visa"></i>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="gap-10">--}}
                    {{--                        <i style="color: #EB001B;" class="fab fa-cc-mastercard"></i>--}}
                    {{--                    </div>--}}
                    {{--                    <div class="gap-10">--}}
                    {{--                        <i style="color: #31B1F0;" class="fa fa-cc-paypal" aria-hidden="true"></i>--}}
                    {{--                    </div>--}}
                    <div class="payment-icon gap-5">
                        <i class="fab fa-google-pay "></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_1.png')}}">--}}
                    </div>
                    <div class="payment-icon gap-5">
                        <i class="fab fa-apple-pay "></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_2.png')}}">--}}
                    </div>
                    <div class="payment-icon gap-5">
                        <i class="fab fa-cc-visa "></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_3.png')}}">--}}
                    </div>
                    <div class="payment-icon gap-5">
                        <i class="fab fa-cc-mastercard "></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_4.png')}}">--}}
                    </div>
                    <div class="payment-icon gap-5">
                        {{--                        <i class="fa-regular fa-wallet"></i>--}}
                        <i class="fas fa-wallet"></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_5.png')}}">--}}
                    </div>
                    <div class="payment-icon gap-5">
                        <i class="far fa-money-bill-alt "></i>
                        {{--                        <img src="{{asset('templateIMG/payment_icon/pay_5.png')}}">--}}
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>
