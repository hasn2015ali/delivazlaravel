<div class="terms-section">
    <div class="container">
        <div class="d-flex flex-row justify-content-start">
            <h4 class="main-address">
                {{__("footerPagesFront.Cookies_Policy")}}

            </h4>
        </div>

        @foreach($cookies as $cooke)

            <div class="d-flex flex-column align-items-start justify-content-start">
                <div class="d-flex flex-row justify-content-start">
                    <h4 class="second-address">
                        {{$cooke->address}}
                    </h4>
                </div>

                <div class="d-flex flex-row justify-content-start">
                    <p class="text">
                        {{$cooke->content}}
                    </p>
                </div>

            </div>
        @endforeach


    </div>

</div>
