<div>
    <section class="faq-section">

        <div class=" imges">
            <h2 class="address">FAQ</h2>

            <div class="img1">
                <img src="{{asset('storage/images/additions/faq/1.png')}}">
            </div>

            <div class="img2">
                <img src="{{asset('storage/images/additions/faq/1.png')}}">
            </div>

            <div class="img3">
                <img src="{{asset('storage/images/additions/faq/1.png')}}">
            </div>

            <div class="img4">
                <img src="{{asset('storage/images/additions/faq/1.png')}}">
            </div>

        </div>


        <div class="content">
            <div class="container">
                <div style="width: 90%; margin: auto" class="d-flex flex-column ">
                    @foreach($cookies as $key=>$cooke)
                    <div class="d-flex flex-column parent">
                        <div class="d-flex flex-row justify-content-between  @if($key==0) active @endif">
                            <a class="btn btn-faq " data-toggle="collapse" data-target="#demo{{$key}}">
                                {{$cooke->address}}
                            </a>

                            <i class="fas fa-arrow-down"></i>
                            <i class="fas fa-arrow-up"></i>
                        </div>
                        <div id="demo{{$key}}" class="collapse text  @if($key==0) show @endif ">
                            {{$cooke->content}}
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>



    </section>
</div>
