<div>
    @include('livewire.modal.deleteModel2')
    @include('livewire.modal.restaurant.editRegistration')

    <div class="d-flex flex-row justify-content-start">
        <h4 class="address">
            {{__("restaurant.NewRestaurants")}}
        </h4>
    </div>
    <div class="table-responsive">
        <table id="myTable" class="table">
            <thead class=" text-primary">
            <th>

                {{__("restaurant.name")}}
            </th>
            <th>
                {{__("restaurant.OwnerName")}}
            </th>
            <th>
                {{__("restaurant.Country")}}
            </th>
            <th>
                {{__("restaurant.City")}}
            </th>
            <th>
                {{__("restaurant.created_at")}}
            </th>


            <th>
                {{__("restaurant.Action")}}
            </th>
            </thead>
            <tbody>


            @forelse($newRestaurants as $restaurant)
                <tr>
                    <td>
                        {{$restaurant->name}}
                    </td>
                    <td>
                        {{$restaurant->user->firstName}} &nbsp; {{$restaurant->user->fatherName}}&nbsp; {{$restaurant->user->lastName}}
                    </td>
                    <td>
                        {{$restaurant->country->translation->where('code_lang',\Illuminate\Support\Facades\App::getLocale())->first()->name}}
                    </td>
                    <td>
                        {{$restaurant->city->translation->where('code_lang',\Illuminate\Support\Facades\App::getLocale())->first()->name}}
                    </td>
                    <td>
                        {{--                                            {{$restaurant->created_at}}--}}
                        {{(\Carbon\Carbon::parse($restaurant->created_at)->format('d/m/Y'))}}

                    </td>


                    <td>
                        <a href="#editRegistration" data-toggle="modal"
                           wire:click="updateCall({{$restaurant->id}})"
                           class="btn btn-outline-warning p-2 m-1">
                            {{--                                                {{__("restaurant.restaurantSettings")}}--}}
                            <i class="fas fa-edit fa-2x"></i>

                        </a>
                        <a href="#delCountry2" data-toggle="modal"
                           wire:click="deleteCall({{$restaurant->id}})">
                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                <i class="fa fa-trash-o fa-2x"></i>
                            </button>
                        </a>
                    </td>



            @empty
                <div class="d-flex flex-row justify-content-center alert alert-info">
                    {{__("restaurant.NoNewRestaurants")}}
                </div>

            @endforelse

            </tbody>
        </table>
        <div class="d-flex flex-row justify-content-center">

            @if($newRestaurants)
            {{ $newRestaurants->links() }}
            @endif
        </div>

    </div>
</div>
