<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__("restaurant.restaurant",['locale'=>app()->getLocale()])}}  </li>

                        {{--                        <li class="breadcrumb-item active">{{$roleId}}</li>--}}
                    </ol>

                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.restaurant.add')
                    <div wire:loading wire:target="getCities">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getRestaurantsByCity">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getNewRestaurants">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getRestaurantsInMyCity">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="switchState">
                        <x-control-panel.loading/>
                    </div>

                    <div class="d-flex flex-row justify-content-between">
                        @canany(['is-manager','is-data_entry_manager'])
                            <a href="#addRestaurant" data-toggle="modal">
                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                                </button>
                            </a>
                            @canany(['is-manager','is-data_entry_manager'])
                                <div class="d-flex flex-row justify-content-start">
{{--                                    @if($countOFNewRestaurant!=0)--}}
                                        <div class="mr-3 ml-3">
                                            <button type="button" class="btn btn-outline-primary d-flex flex-row "
                                                    wire:click="getNewRestaurants">
                                                <livewire:category.new-restuarnt-count-component />
                                            </button>
                                        </div>
{{--                                    @endif--}}
                                    @can('is-data_entry_manager')
                                        <div class="mr-3 ml-3">
                                            <button type="button" class="btn btn-outline-success "
                                                    wire:click="getRestaurantsInMyCity">
                                                {{__("restaurant.RestaurantsInMyCity")}}
                                            </button>
                                        </div>
                                    @endcan
                                    @can('is-manager')
                                        <div class="dropdown">
                                            <button class="btn btn-outline-success dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                @if($countryName)
                                                    {{$countryName}}
                                                @else
                                                    {{__("subscriber.selectCountry")}}
                                                @endif
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                                @foreach($countries as $country)
                                                    <a class="dropdown-item" href="#"
                                                       wire:click.prevent="getCities({{ $country->country_id }}, '{{ $country->name }}')">{{$country->name}}</a>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="dropdown mr-3 ml-3 " id="cities_list">
                                            <button id="dropdown_selecte_btn_city" type="button"
                                                    class="btn btn-outline-success dropdown-toggle"
                                                    data-toggle="dropdown">
                                                @if($countryName)
                                                    @if($cityName)
                                                        {{$cityName}}
                                                    @else
                                                        {{__("home.selectCity")}}
                                                    @endif

                                                @else
                                                    {{__("home.selectCity")}}
                                                @endif
                                            </button>
                                            <div class="dropdown-menu">
                                                @if(!empty($countryName))
                                                    @foreach($cities as $item)

                                                        <a href="#"
                                                           wire:click.prevent="getRestaurantsByCity({{ $item->city_id }}, '{{ $item->name }}')">
                                                            <p class="dropdown-item">{{$item->name}}</p>

                                                        </a>
                                                    @endforeach
                                                @else
                                                    <p class="dropdown-item">
                                                        <small> {{__("home.selectCountryFirst")}}</small></p>
                                                @endif
                                            </div>
                                        </div>
                                    @endcan

                                </div>
                            @endcan
                        @endcan
                    </div>

                </div>
                <div class="card-body">
                    {{--                    {{dd( count($restaurantsInCity))}}--}}
                    @if(!$showNewRestaurant)
                        @if(count($restaurantsInCity)!=0 )
                            @can('is-manager')
                                @if($cityName)
                                    <h4 class="address">
                                        {{__("restaurant.restaurantsIn")}} {{$cityName}}
                                    </h4>
                                @endif
                            @endcan
                            <div class="table-responsive">
                                <table id="myTable" class="table">
                                    <thead class=" text-primary">
                                    <th>

                                        {{__("restaurant.name")}}
                                    </th>
                                    <th>
                                        {{--                                    {{__("restaurant.Email")}}--}}
                                        {{__("restaurant.Address")}}

                                    </th>
                                    <th>
                                        {{__("restaurant.Phone")}}
                                    </th>

                                    <th>
                                        {{__("restaurant.created_at")}}

                                    </th>
                                    @can('is-manager')
                                    <th>
                                        {{__("restaurant.status")}}
                                    </th>
                                    @endcan

                                    <th>
                                        {{__("restaurant.Action")}}
                                    </th>
                                    </thead>
                                    <tbody>


                                    @foreach($restaurantsInCity as $restaurant)
                                        <tr>
                                            <td>
                                                {{$restaurant->name}}
                                            </td>
                                            <td>
                                                {{--                                            {{$restaurant->user->email}}--}}
                                                {{--                                            {{$restaurant->userEmail}}--}}
                                                {{--                                            {{$restaurant->address}}--}}
                                                {{ \Illuminate\Support\Str::of($restaurant->address)->words(5) }}

                                            </td>
                                            <td>
                                                {{$restaurant->phone1}}
                                            </td>
                                            <td>
                                                {{--                                            {{$restaurant->created_at}}--}}
                                                {{(\Carbon\Carbon::parse($restaurant->created_at)->format('d/m/Y'))}}

                                            </td>
                                            @can('is-manager')
                                            <td>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input"
                                                           wire:change="switchState({{$restaurant->serviceProviderID}})"
                                                           id="customSwitchUser2{{$restaurant->serviceProviderID}}"
                                                           @if($restaurant->userStatus==1) checked=" " @endif>
                                                    <label class="custom-control-label" for="customSwitchUser2{{$restaurant->serviceProviderID}}"></label>
                                                </div>
                                            </td>
                                            @endcan

                                            <td>
                                                <a href="{{route('restaurantDetails',['locale'=>app()->getLocale(),'id'=>$restaurant->serviceProviderID])}}"
                                                   class="btn btn-outline-info p-2 m-1">
                                                    <i class="fa fa-cog fa-2x" aria-hidden="true"></i>
                                                </a>
                                                <a href="#delCountry" data-toggle="modal"
                                                   wire:click="deleteCall({{$restaurant->serviceProviderID}})">
                                                    <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                        <i class="fa fa-trash-o fa-2x"></i>
                                                    </button>
                                                </a>
                                            </td>



                                    @endforeach

                                    </tbody>
                                </table>
                                <div class="d-flex flex-row justify-content-center">

                                    {{--                            @if($restaurantsInCity)--}}
                                    {{ $restaurantsInCity->links() }}
                                    {{--                            @endif--}}
                                </div>

                            </div>

                        @else
                            @if($selectedCityCheck)
                                <div class="alert alert-info">
                                    {{$selectedCityCheck}}
                                </div>
                            @else
                                <div class="alert alert-info">
                                    {{__("restaurant.NoRestaurants")}}
                                </div>
                            @endif

                        @endif
                    @else

                        <livewire:category.new-restuarnt-component :allRestaurantFilter="$allRestaurantFilter"/>
                    @endif

                </div>
            </div>
        </div>

    </div>
</div>
