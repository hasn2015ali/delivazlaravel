<div>
    <div class="d-flex flex-row justify-content-center">
        &nbsp; &nbsp; <h6 class="user-day">{{__("vendor.oldFilters")}}</h6>
    </div>
    <div class="row p-3" >
        @foreach($selectedFiltersForVendor as $filter)
            <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                <h6 class="user-work-address">{{$filter->translation->where('code_lang',$langCode)->first()->name}} </h6>
            </div>
        @endforeach

    </div>
    <hr style="background: #f96332; "/>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group d-flex flex-row justify-content-center">
                    <h6 class="user-day">{{__("vendor.updateFilter")}}</h6>
                    @error('selectedVendorFilters') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group ">
                    <label>{{__("vendor.selectNewFilters")}}</label>
                    @error('newSelectedFilters') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row p-3" >
            @foreach($allVendorFilter as $filter)
                <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                    <label>{{$filter->name}} </label>
                    <div class="custom-control custom-switch ">
                        <input type="checkbox" wire:model.defer="newSelectedFilters" value="{{$filter->id}}" class="custom-control-input"  id="customSwitch1{{$filter->id}}" checked=" " >
                        <label class="custom-control-label" for="customSwitch1{{$filter->id}}"></label>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="form-group d-flex align-items-center flex-column">

            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
        </div>
    </form>
</div>
