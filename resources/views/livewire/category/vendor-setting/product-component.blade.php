<div class="row">
    <div class="col-md-12">
        <div class="">
            <div class="card-header">


                @include('livewire.modal.deleteModel')
                @include('livewire.modal.product.add')
                @include('livewire.modal.product.productFilter')
                @include('livewire.modal.product.productConfig')
                @include('livewire.modal.product.size')
                @include('livewire.modal.product.productSize')
                @include('livewire.modal.product.Additions')

                <div  wire:loading wire:target="getProductForThisFilter">
                    <x-control-panel.loading />
                </div>
                <div class="d-flex flex-row justify-content-between">
                    @canany(['is-manager','is-data_entry_manager'])
                        <a href="#addProduct" data-toggle="modal" wire:click.prevent="restFilde()">
                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </a>

                        <a href="#addSize" data-toggle="modal"  >
                            <button type="button" class="btn btn-outline-info p-3 m-1">
                                <span style="font-size: 16px"> {{__("vendor.Sizes")}}</span>
                            </button>
                        </a>
                        <div class="dropdown">
                            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if($ProductFilterName)
                                    {{$ProductFilterName}}
                                @else
                                    {{__("vendor.allProductsFilters")}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#" wire:click.prevent="getProductForThisFilter(0)" > {{__("vendor.allProductsFilters")}}</a>

                                {{--                                {{dd($allProductFilters)}}--}}
                                @if(count($allProductFilters[0])!=0)

                                    @foreach($allProductFilters as $vendorFilter)
                                        @foreach($vendorFilter as $productFilter)
                                            {{--                                    <a class="dropdown-item" href="{{route('mealsFilters',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}" >{{$filter->name}}</a>--}}
                                            <a class="dropdown-item" href="#" wire:click.prevent="getProductForThisFilter({{$productFilter['id']}})" >{{$productFilter['name']}}</a>

                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>


                    @endcan

                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="d-flex flex-row justify-content-center">
                        <div  wire:loading wire:target="editProductFilter">
                            <x-control-panel.loading />
                        </div>
                    </div>
                    <table id="myTable" class="table">
                        <thead class=" text-primary">
                        <th>

                            {{__("vendor.productName")}}
                        </th>
                        <th>
                            {{__("vendor.productPhoto")}}

                        </th>
                        <th>
                            {{__("vendor.productContent")}}
                        </th>
                        <th>
                            {{__("vendor.productPrice")}}
                        </th>

                        <th>
                            {{__("vendor.Sizes")}}
                        </th>
                        <th>
                            {{__("vendor.productFilters")}}
                        </th>
                        <th>
                            {{__("vendor.Action")}}
                        </th>
                        </thead>
                        <tbody>
                        @if($products)
                            @foreach($products as $product)
                                <tr>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($product->name)->words(2) }}
                                        {{--                                {{$product->name}}--}}
                                    </td>
                                    <td >
                                        @if($product->product->photo)
                                            <img class="food-img-view "  src="{{asset('storage/images/product/'.$product->product->photo)}}"/>
                                        @else
                                            {{__("vendor.empty")}}
                                        @endif
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($product->content)->words(1) }}
                                    </td>
                                    <td>
                                        {{$product->product->price}}
                                    </td>

                                    <td>
                                        <button   data-toggle="modal" data-target="#addProductSize" wire:click="editProductSize({{$product->product_id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <button   data-toggle="modal" data-target="#productFilter" wire:click="editProductFilter({{$product->product_id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <a href="#addAdditions" data-toggle="modal" wire:click="editProductAddition({{$product->product_id}})">
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button   data-toggle="modal" data-target="#productConfig" wire:click="editProduct({{$product->product_id}})" class="btn btn-outline-info p-2 m-1">

                                            <i class="fa fa-cog fa-2x" aria-hidden="true"></i>

                                        </button>
                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$product->product_id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                    </td>



                            @endforeach
                        @endif

                        @if($productByFilters)
                            {{--                            {{ dd($productByFilters) }}--}}
                            @foreach($productByFilters as $product)
                                <tr>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($product->translation->where('code_lang',App::getlocale())->first()->name)->words(2) }}
                                        {{--                                        {{$product->translation->where('code_lang',App::getlocale())->first()->name}}--}}
                                    </td>
                                    <td >
                                        @if($product->photo)
                                            <img class="food-img-view "  src="{{asset('storage/images/product/'.$product->photo)}}"/>
                                        @else
                                            {{__("vendor.empty")}}
                                        @endif
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($product->translation->where('code_lang',App::getlocale())->first()->content)->words(1) }}
                                    </td>
                                    <td>
                                        {{$product->price}}
                                    </td>

                                    <td>
                                        <button   data-toggle="modal" data-target="#addProductSize" wire:click="editProductSize({{$product->id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>

                                    <td>
                                        <button   data-toggle="modal" data-target="#productFilter" wire:click="editProductFilter({{$product->id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <a href="#addAdditions" data-toggle="modal" wire:click="editProductAddition({{$product->id}})">
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button   data-toggle="modal" data-target="#productConfig" wire:click="editProduct({{$product->id}})" class="btn btn-outline-info p-2 m-1">

                                            <i class="fa fa-cog fa-2x" aria-hidden="true"></i>

                                        </button>
                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$product->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                    </td>



                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="d-flex flex-row justify-content-center">
                        @if($products)
                            {{ $products->links() }}
                        @endif
                        @if($productByFilters)
                            {{ $productByFilters->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
