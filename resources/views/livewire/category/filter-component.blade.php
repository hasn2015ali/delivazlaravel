<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        @if($type==1)
                            <li class="breadcrumb-item active">{{__("filter.filterRestaurant")}}</li>
                        @elseif($type==2)
                            <li class="breadcrumb-item active">{{__("filter.filterVendors")}}</li>
                        @endif
                    </ol>
                    @include('livewire.modal.filter.add')
                    @include('livewire.modal.translation')
                    @include('livewire.modal.filter.up')
                    @include('livewire.modal.deleteModel')

                    <div class="d-flex flex-row justify-content-between">
                        @can('is-manager')
                        <a href="#addFilter" data-toggle="modal">
                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </a>
                        @endcan

                    </div>

                    @if (session()->has('message'))
                        <div class="container alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("filter.Name")}}
                            </th>

                            <th>
                                {{__("masterControl.translated")}}
                            </th>
                            <th>
                                {{__("masterControl.Action")}}
                            </th>

                            <th>
                                @if($type==1)
                                {{__("filter.showMealsFilters")}}
                                @elseif($type==2)
                                {{__("filter.showProductsFilters")}}
                                @endif
                            </th>
                            </thead>

                            <tbody>
                            @foreach($filters as $filter)
                                <tr>

                                    <td>
                                        {{$filter->name}}
                                    </td>

                                    <td>
                                        @if($languegeCount==count($filter->filter->translation))
                                            <label style="color: green">
                                                <i class="fa fa-check ml-1 mr-1" aria-hidden="true"></i>{{__("masterControl.TranslateCompleted")}}
                                            </label>
                                        @else
                                            <a href="#showTranslation" data-toggle="modal"class="" wire:click="showTranslation({{$filter->filter_id}})"> <button type="button" class="btn btn-outline-info p-2 m-1">
                                                    <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                                </button></a>

                                        @endif
                                    </td>
                                    <td>
                                        @can('is-manager')
                                        <button wire:click="edit({{$filter}})"  data-toggle="modal" data-target="#filterUp" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                        </button>


                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$filter->filter_id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                        @endcan
                                        @canany(['is-manager','is-translator'])
                                        @if($type==1)
                                            <a href="{{route('restaurantFilterTranslate',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}" class="btn btn-outline-primary p-2 m-1"><i class="fa fa-language fa-2x" aria-hidden="true"></i></a>

                                        @elseif($type==2)
                                            <a href="{{route('vendorFilterTranslate',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}" class="btn btn-outline-primary p-2 m-1"><i class="fa fa-language fa-2x" aria-hidden="true"></i></a>

                                        @endif
                                        @endcan

                                    </td>
                                    <td>
                                        @if($type==1)
                                            <a class="" href="{{route('mealsFilters',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}"> <button type="button" class="btn btn-outline-info p-2 m-1">
                                                    <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                                </button></a>

                                        @elseif($type==2)
                                            <a class="" href="{{route('productsFilters',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}"> <button type="button" class="btn btn-outline-info p-2 m-1">
                                                    <i class="fa fa-eye fa-2x" aria-hidden="true"></i>
                                                </button></a>

                                        @endif

                                    </td>
                                </tr>


                            @endforeach

                            </tbody>
                        </table>

                        <div class="d-flex flex-row justify-content-center">
                            {{ $filters->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


{{--</div>--}}
