<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a></li>
                        @if($type==1)
                            <li class="breadcrumb-item active"><a href="{{route('restaurantFilter',['locale'=>app()->getLocale()])}}">{{__("filter.filterRestaurant")}}</a></li>
                        @elseif($type==2)
                            <li class="breadcrumb-item active"><a href="{{route('vendorsFilter',['locale'=>app()->getLocale()])}}">{{__("filter.filterVendors")}}</a></li>
                        @endif

                        <li class="breadcrumb-item ">{{__("filter.translate")}}</li>
                    </ol>
                    @include('livewire.modal.filter.addTrans')
                    @include('livewire.modal.filter.transUp')
                    @include('livewire.modal.deleteModel')
                    @canany(['is-manager','is-translator'])
                    <a href="#transFilter" data-toggle="modal">
                        <button type="button" class="btn btn-outline-success p-2 m-1">
                            <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                        </button>
                    </a>
                    @endcan

                    @if (session()->has('message'))
                        <div class="container alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ session('message') }}
                        </div>
                    @endif

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("filter.Name")}}
                            </th>
                            <th>
                                {{__("filter.translate")}}
                            </th>
                            <th>
                                {{__("filter.lang")}}
                            </th>
                            <th>
                                {{__("masterControl.Action")}}
                            </th>

                            </thead>

                            <tbody>
                            @foreach($translations as $trans)
                                <tr>
                                    <td>
                                        {{$filterEn->name}}
                                    </td>

                                    <td>
                                        {{$trans->name}}
                                    </td>
                                    <td>
                                        {{$trans->lang->name}}
                                    </td>
                                    <td>
                                        @canany(['is-manager','is-translator'])
                                        <button wire:click="edit({{$trans}})"  data-toggle="modal" data-target="#filterTransUp" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                        </button>


                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$trans->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                        @endcan
                                    </td>

                                </tr>


                            @endforeach

                            </tbody>
                        </table>

                        <div class="d-flex flex-row justify-content-center">
                            {{ $translations->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


{{--</div>--}}
