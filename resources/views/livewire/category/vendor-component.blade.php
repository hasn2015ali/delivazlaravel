<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                href="{{route('dashboard',['locale'=>app()->getLocale()])}}">{{__("masterControl.Dashboard")}}</a>
                        </li>
                        <li class="breadcrumb-item active">{{__("vendor.vendor",['locale'=>app()->getLocale()])}}  </li>

                        {{--                        <li class="breadcrumb-item active">{{$roleId}}</li>--}}
                    </ol>

                    @include('livewire.modal.deleteModel')
                    @include('livewire.modal.vendor.add')
                    <div wire:loading wire:target="getCities">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getVendorsByCity">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getNewVendors">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="getVendorsInMyCity">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="switchState">
                        <x-control-panel.loading/>
                    </div>


                    <div class="d-flex flex-row justify-content-between">
                        @canany(['is-manager','is-data_entry_manager'])
                            <a href="#addVendor" data-toggle="modal">
                                <button type="button" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                                </button>
                            </a>
                            @canany(['is-manager','is-data_entry_manager'])
                                <div class="d-flex flex-row justify-content-start">
{{--                                    @if($countOFNewVendor!=0)--}}
                                        <div class="mr-3 ml-3">
                                            <button type="button" class="btn btn-outline-primary d-flex flex-row "
                                                    wire:click="getNewVendors">
                                                <livewire:category.new-vendor-count-component />
                                            </button>
                                        </div>
{{--                                    @endif--}}
                                    @can('is-data_entry_manager')
                                        <div class="mr-3 ml-3">
                                            <button type="button" class="btn btn-outline-success "
                                                    wire:click="getVendorsInMyCity">
                                                {{__("vendor.VendorsInMyCity")}}
                                            </button>
                                        </div>
                                    @endcan
                                        @can('is-manager')

                                        <div class="dropdown">
                                        <button class="btn btn-outline-success dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            @if($countryName)
                                                {{$countryName}}
                                            @else
                                                {{__("subscriber.selectCountry")}}
                                            @endif
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                            @foreach($countries as $country)
                                                <a class="dropdown-item" href="#"
                                                   wire:click.prevent="getCities({{ $country->country_id }}, '{{ $country->name }}')">{{$country->name}}</a>
                                            @endforeach
                                        </div>
                                    </div>


                                    <div class="dropdown mr-3 ml-3 " id="cities_list">
                                        <button id="dropdown_selecte_btn_city" type="button"
                                                class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown">
                                            @if($countryName)
                                                @if($cityName)
                                                    {{$cityName}}
                                                @else
                                                    {{__("home.selectCity")}}
                                                @endif

                                            @else
                                                {{__("home.selectCity")}}
                                            @endif
                                        </button>
                                        <div class="dropdown-menu">
                                            @if(!empty($countryName))
                                                @foreach($cities as $item)

                                                    <a href="#"
                                                       wire:click.prevent="getShopsByCity({{ $item->city_id }}, '{{ $item->name }}')">
                                                        <p class="dropdown-item">{{$item->name}}</p>

                                                    </a>
                                                @endforeach
                                            @else
                                                <p class="dropdown-item">
                                                    <small> {{__("home.selectCountryFirst")}}</small></p>
                                            @endif
                                        </div>
                                    </div>
                                        @endcan

                                </div>
                            @endcan
                        @endcan

                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if(!$showNewVendor)
                        @if(count($vendorsInCity)!=0)
                            @can('is-manager')
                                @if($cityName)
                                    <h4 class="address">
                                        {{__("vendor.shopsIn")}} {{$cityName}}
                                    </h4>
                                @endif
                            @endcan
                            <table id="myTable" class="table">
                                <thead class=" text-primary">
                                <th>

                                    {{__("vendor.name")}}
                                </th>
                                <th>
{{--                                    {{__("vendor.Email")}}--}}
                                    {{__("vendor.Address")}}

                                </th>
                                <th>
                                    {{__("vendor.Phone")}}
                                </th>

                                <th>
                                    {{__("vendor.created_at")}}

                                </th>
                                @can('is-manager')
                                    <th>
                                        {{__("restaurant.status")}}
                                    </th>
                                @endcan
                                <th>
                                    {{__("vendor.Action")}}
                                </th>
                                </thead>
                                <tbody>
                                @foreach($vendorsInCity as $vendor)
                                    <tr>
                                        <td>
                                            {{$vendor->name}}
                                        </td>
                                        <td>
                                            {{--                                            {{$vendor->user->email}}--}}
{{--                                            {{$vendor->userEmail}}--}}
                                            {{ \Illuminate\Support\Str::of($vendor->address)->words(5) }}

                                        </td>
                                        <td>
                                            {{$vendor->phone1}}
                                        </td>
                                        <td>
                                            {{--                                            {{$vendor->created_at}}--}}
                                            {{(\Carbon\Carbon::parse($vendor->created_at)->format('d/m/Y'))}}

                                        </td>
                                        @can('is-manager')
                                            <td>
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input"
                                                           wire:change="switchState({{$vendor->serviceProviderID}})"
                                                           id="customSwitchUser2{{$vendor->serviceProviderID}}"
                                                           @if($vendor->userStatus==1) checked=" " @endif>
                                                    <label class="custom-control-label" for="customSwitchUser2{{$vendor->serviceProviderID}}"></label>
                                                </div>
                                            </td>
                                        @endcan
                                        <td>
                                            <a href="{{route('shopDetails',['locale'=>app()->getLocale(),'id'=>$vendor->serviceProviderID])}}"
                                               class="btn btn-outline-info p-2 m-1">
                                                {{--                                                {{__("vendor.vendorSettings")}}--}}
                                                <i class="fa fa-cog fa-2x" aria-hidden="true"></i>

                                            </a>
                                            <a href="#delCountry" data-toggle="modal"
                                               wire:click="deleteCall({{$vendor->serviceProviderID}})">
                                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                    <i class="fa fa-trash-o fa-2x"></i>
                                                </button>
                                            </a>
                                        </td>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $vendorsInCity->links() }}
                        @else
                            @if($selectedCityCheck)
                                <div class="alert alert-info">
                                    {{$selectedCityCheck}}
                                </div>
                            @else
                                <div class="alert alert-info">
                                    {{__("vendor.NoShops")}}
                                </div>
                            @endif
                        @endif
                        @else

                            <livewire:category.new-vendors-component :allVendorFilter="$allVendorFilter"/>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
