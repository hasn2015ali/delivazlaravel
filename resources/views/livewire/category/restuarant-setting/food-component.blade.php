<div class="row">
    <div class="col-md-12">
        <div class="">
            <div class="card-header">


                @include('livewire.modal.deleteModel')
                @include('livewire.modal.food.add')
                @include('livewire.modal.food.foodFilter')
                @include('livewire.modal.food.mailConfig')
                @include('livewire.modal.food.size')
                @include('livewire.modal.food.foodSize')
                @include('livewire.modal.food.Additions')

                <div  wire:loading wire:target="getFoodForThisFilter">
                    <x-control-panel.loading />
                </div>
                <div class="d-flex flex-row justify-content-between">
                    @canany(['is-manager','is-data_entry_manager'])
                        <a href="#addFood" data-toggle="modal" wire:click.prevent="restFilde()">
                            <button type="button" class="btn btn-outline-success p-2 m-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </a>

                        <a href="#addSize" data-toggle="modal"  >
                            <button type="button" class="btn btn-outline-info p-3 m-1">
                               <span style="font-size: 16px"> {{__("restaurant.Sizes")}}</span>
                            </button>
                        </a>


                        <div class="dropdown">
                            <button class="btn btn-outline-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              @if($MealFilterName)
                                  {{$MealFilterName}}
                                @else
                                {{__("restaurant.allMealsFilters")}}
                                @endif
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#" wire:click.prevent="getFoodForThisFilter(0)" > {{__("restaurant.allMealsFilters")}}</a>

{{--                                {{dd($allMailFilters)}}--}}
                            @if(count($allMailFilters[0])!=0)

                                    @foreach($allMailFilters as $restaurantFilter)
                                        @foreach($restaurantFilter as $mailFilter)
{{--                                    <a class="dropdown-item" href="{{route('mealsFilters',['locale'=>app()->getLocale(),'id'=>$filter->filter_id])}}" >{{$filter->name}}</a>--}}
                                            <a class="dropdown-item" href="#" wire:click.prevent="getFoodForThisFilter({{$mailFilter['id']}})" >{{$mailFilter['name']}}</a>

                                        @endforeach
                                    @endforeach
                                @endif
                            </div>
                        </div>

                    @endcan

                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="d-flex flex-row justify-content-center">
                        <div  wire:loading wire:target="editFoodFilter">
                            <x-control-panel.loading />
                        </div>
                    </div>
                    <table id="myTable" class="table">
                        <thead class=" text-primary">
                        <th>

                            {{__("restaurant.foodName")}}
                        </th>
                        <th>
                            {{__("restaurant.foodPhoto")}}

                        </th>
                        <th>
                            {{__("restaurant.foodContent")}}
                        </th>
                        <th>
                            {{__("restaurant.foodPrice")}}
                        </th>

                        <th>
                            {{__("restaurant.foodTime")}}
                        </th>
                        <th>
                            {{__("restaurant.Sizes")}}
                        </th>
                        <th>
                            {{__("restaurant.foodFilters")}}
                        </th>
                        <th>
                            {{__("restaurant.Action")}}
                        </th>
                        </thead>
                        <tbody>
                        @if($foods)
                            @foreach($foods as $food)
                        <tr>
                            <td>
                                {{ \Illuminate\Support\Str::of($food->name)->words(2) }}
{{--                                {{$food->name}}--}}
                            </td>
                            <td >
                                @if($food->food->photo)
                                    <img class="food-img-view "  src="{{asset('storage/images/food/'.$food->food->photo)}}"/>
                                @else
                                    {{__("restaurant.empty")}}
                                @endif
                            </td>
                            <td>
                                {{ \Illuminate\Support\Str::of($food->content)->words(1) }}
                            </td>
                            <td>
                                {{$food->food->price}}
                            </td>
                            <td>
                                @if($food->food->time_hour and $food->food->time_minutes)
                                    {{$food->food->time_hour}} &nbsp;<small>H, </small>    {{$food->food->time_minutes}}&nbsp;<small>M</small>
                                @elseif($food->food->time_hour)
                                    {{$food->food->time_hour}} &nbsp;<small>H</small>
                                @elseif($food->food->time_minutes)
                                    {{$food->food->time_minutes}}&nbsp;<small>M</small>
                                @else
                                    {{__("restaurant.empty")}}
                                @endif
                            </td>
                            <td>
                                <button   data-toggle="modal" data-target="#addFoodSize" wire:click="editFoodSize({{$food->food_id}})" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                </button>
                            </td>
                            <td>
                                <button   data-toggle="modal" data-target="#foodFilter" wire:click="editFoodFilter({{$food->food_id}})" class="btn btn-outline-success p-2 m-1">
                                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                </button>
                            </td>
                            <td>
                                <a href="#addAdditions" data-toggle="modal" wire:click="editFoodAddition({{$food->food_id}})">
                                    <button type="button" class="btn btn-outline-info p-2 m-1">
                                        <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                                    </button>
                                </a>
                                <button   data-toggle="modal" data-target="#mailConfig" wire:click="editFood({{$food->food_id}})" class="btn btn-outline-info p-2 m-1">

                                    <i class="fa fa-cog fa-2x" aria-hidden="true"></i>

                                </button>
                                <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$food->food_id}})">
                                    <button type="button" class="btn btn-outline-danger p-2 m-1">
                                        <i class="fa fa-trash-o fa-2x"></i>
                                    </button>
                                </a>
                            </td>



                            @endforeach
                        @endif

                        @if($foodByFilters)
{{--                            {{ dd($foodByFilters) }}--}}
                            @foreach($foodByFilters as $food)
                                <tr>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($food->translation->where('code_lang',App::getlocale())->first()->name)->words(2) }}
{{--                                        {{$food->translation->where('code_lang',App::getlocale())->first()->name}}--}}
                                    </td>
                                    <td >
                                        @if($food->photo)
                                            <img class="food-img-view "  src="{{asset('storage/images/food/'.$food->photo)}}"/>
                                        @else
                                            {{__("restaurant.empty")}}
                                        @endif
                                    </td>
                                    <td>
                                        {{ \Illuminate\Support\Str::of($food->translation->where('code_lang',App::getlocale())->first()->content)->words(1) }}
                                    </td>
                                    <td>
                                        {{$food->price}}
                                    </td>
                                    <td>
                                        @if($food->time_hour and $food->time_minutes)
                                            {{$food->time_hour}} &nbsp;<small>H, </small>    {{$food->time_minutes}}&nbsp;<small>M</small>
                                        @elseif($food->time_hour)
                                            {{$food->time_hour}} &nbsp;<small>H</small>
                                        @elseif($food->time_minutes)
                                            {{$food->time_minutes}}&nbsp;<small>M</small>
                                        @else
                                            {{__("restaurant.empty")}}
                                        @endif
                                    </td>
                                    <td>
                                        <button   data-toggle="modal" data-target="#addFoodSize" wire:click="editFoodSize({{$food->id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>

                                    <td>
                                        <button   data-toggle="modal" data-target="#foodFilter" wire:click="editFoodFilter({{$food->id}})" class="btn btn-outline-success p-2 m-1">
                                            <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>
                                    <td>
                                        <a href="#addAdditions" data-toggle="modal" wire:click="editFoodAddition({{$food->id}})">
                                            <button type="button" class="btn btn-outline-info p-2 m-1">
                                                <i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <button   data-toggle="modal" data-target="#mailConfig" wire:click="editFood({{$food->id}})" class="btn btn-outline-info p-2 m-1">

                                            <i class="fa fa-cog fa-2x" aria-hidden="true"></i>

                                        </button>
                                        <a href="#delCountry" data-toggle="modal" wire:click="deleteCall({{$food->id}})">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </a>
                                    </td>



                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <div class="d-flex flex-row justify-content-center">
                        @if($foods)
                        {{ $foods->links() }}
                        @endif
                            @if($foodByFilters)
                                {{ $foodByFilters->links() }}
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
