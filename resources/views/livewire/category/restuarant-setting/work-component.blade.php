<div>
    <div class="row">
        <div class="col-lg-5 d-flex flex-row justify-content-around">
{{--            <div class="form-group ">--}}
            <div class="form-group">
                <h6 class="user-day"> {{__("restaurant.state")}}</h6>
            </div>
               <div class="form-group ">
                   <div class="custom-control custom-switch ">
                       <input type="checkbox"  class="custom-control-input" wire:change="changeServiceProviderState" id="customSwitch1yy" @if($restaurant->state==1) checked=" " @endif >
                       <label class="custom-control-label user-day" for="customSwitch1yy">
                           <h6 class="user-day">{{$serviceProviderState}}</h6>
                       </label>
                   </div>
               </div>
{{--                <h6 class="user-day"> {{__("restaurant.state")}}</h6>--}}
{{--            </div>--}}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group ">
                <h6 class="user-day"> {{__("restaurant.day")}}</h6>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group d-flex flex-row justify-content-center ">
                <h6 class="user-day"> {{__("restaurant.startFrom")}}</h6>
            </div>
        </div>
        <div class="col-lg-1">
            <div class="form-group ">
                <h6 class="user-day"> {{__("restaurant.to")}}</h6>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group d-flex flex-row justify-content-center ">
                <h6 class="user-day"> {{__("restaurant.EndIn")}}</h6>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group ">
                <h6 class="user-day"> {{__("restaurant.save")}}</h6>
            </div>
        </div>
    </div>
    @foreach($days as $key=>$day)
{{--{{dd($key)}}--}}
       <form wire:submit.prevent="submit({{$key+1}})">
           <div class="row">
               <div class="col-lg-3 d-flex flex-row justify-content-around">
                   <div class="form-group ">
                       <h6 class="mt-4 user-work-address"> {{$day}}</h6>
                   </div>
{{--                   <div class="form-group ">--}}
{{--                       <h6 class="mt-4 user-day"> {{$dayState}}</h6>--}}
{{--                   </div>--}}
                   <div class="form-group handel-select">
                       <select class="form-control  toggle-enabl mt-4" id="" wire:change="dayState" wire:model.defer="day{{$key+1}}">
                           <option value="on">{{__("restaurant.on")}}</option>
                           <option value="off">{{__("restaurant.off")}}</option>
                       </select>
{{--                       <div class="custom-control custom-switch mt-4">--}}
{{--                           <input type="checkbox"  class="custom-control-input "  onchange="setHours({{$key+1}})" id="customSwitch1{{$day}}"  >--}}
{{--                           <label class="custom-control-label" for="customSwitch1{{$day}}"><h6 class="user-day"> {{$dayState}}</h6></label>--}}
{{--                       </div>--}}
                   </div>
               </div>
{{--               @if($day1=="on")--}}
               <div class="col-lg-3 d-flex flex-row justify-content-around">
                   <div class="form-group handel-select">
                       <label class="blue-label"> {{__("restaurant.hour")}}</label>
                       <select class="form-control  toggle-enabl " id="partTime{{$key+1}}" wire:model.defer="hourStart{{$key+1}}">
                           <option value="off">{{__("restaurant.off")}}</option>
{{--                           @for($h=0;$h<24;$h++)--}}
{{--                               <option value="{{$h}}">{{$h}}</option>--}}
{{--                           @endfor--}}
                           @foreach($hoursInDay as $hour)
                             <option value="{{$hour}}">{{$hour}}</option>
                           @endforeach
                       </select>
                   </div>
                   <div class="form-group handel-select">
                       <label class="blue-label"> {{__("restaurant.minutes")}}</label>
                       <select  class="form-control  toggle-enabl" id="partTime{{$key+1}}" wire:model.defer="minutesStart{{$key+1}}">
                           <option value="off">{{__("restaurant.off")}}</option>
{{--                           @for($m=0;$m<60;$m++)--}}
{{--                               <option value="{{$m}}">{{$m}}</option>--}}
{{--                           @endfor--}}
                           @foreach($minutesInHour as $minute)
                               <option value="{{$minute}}">{{$minute}}</option>
                           @endforeach
                       </select>
                   </div>
               </div>
               <div class="col-lg-1">
                   <div class="form-group">
                       <label class="blue-label"> {{__("restaurant.to")}}</label>
                   </div>
               </div>
               <div class="col-lg-3 d-flex flex-row justify-content-around">
                   <div class="form-group handel-select">
                       <label class="blue-label"> {{__("restaurant.hour")}}</label>
                       <select class="form-control toggle-enabl" id="partTime{{$key+1}}"  wire:model.defer="hourEnd{{$key+1}}">
                           <option value="off">{{__("restaurant.off")}}</option>
{{--                           @for($h=0;$h<24;$h++)--}}
{{--                               <option value="{{$h}}">{{$h}}</option>--}}
{{--                           @endfor--}}
                           @foreach($hoursInDay as $hour)
                               <option value="{{$hour}}">{{$hour}}</option>
                           @endforeach
                       </select>
                   </div>
                   <div class="form-group handel-select">
                       <label class="blue-label"> {{__("restaurant.minutes")}}</label>
                       <select class="form-control toggle-enabl" id="partTime{{$key+1}}" wire:model.defer="minutesEnd{{$key+1}}">
                           <option value="off">{{__("restaurant.off")}}</option>
{{--                           @for($m=0;$m<60;$m++)--}}
{{--                               <option value="{{$m}}">{{$m}}</option>--}}
{{--                           @endfor--}}
                           @foreach($minutesInHour as $minute)
                               <option value="{{$minute}}">{{$minute}}</option>
                           @endforeach
                       </select>
                   </div>
               </div>
{{--               @else--}}
{{--                   <div class="col-lg-7">--}}
{{--                       <div class="alert alert-info">--}}
{{--                           {{__("restaurant.dayOff")}}--}}
{{--                       </div>--}}
{{--                   </div>--}}
{{--               @endif--}}
               <div class="col-lg-2">
                <div class="mt-3">
                    <button type="submit"  class="btn btn-outline-info p-2 m-1">
                        <i class="fa fa-floppy-o fa-2x" aria-hidden="true"></i>
                    </button>
                </div>
               </div>
           </div>
       </form>
    @endforeach

</div>
<style>
 .form-group {
        margin-bottom: 10px!important;
        position: relative!important;
        width: 100%!important;
        margin: 5px!important;
    }
    .blue-label{
        color: #2CA8FF !important;
    }
 .disabled {
     background: #ccc;
     cursor: not-allowed;
     pointer-events: none;
     border-width: 1px;
 }
 select.form-control{
        cursor: pointer;
    }
</style>
