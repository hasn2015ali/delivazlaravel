<div>
    <form wire:submit.prevent="submit">
    <div class="row">
        <div class="col-md-12 handle-input">
            <div  wire:loading wire:target="restaurantBanner">
                <x-control-panel.loading />
            </div>
            @if ($restaurantBanner)
                <img class="banner-loaded " src=" {{ $restaurantBanner->temporaryUrl() }}">
            @endif
            <label class="d-flex flex-row">
                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="restaurantBanner" >
            </label>

            @error('restaurantBanner') <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ $message }}</div>
            @enderror
        </div>
    </div>
        <div class="form-group d-flex align-items-center flex-column">

            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
        </div>
    </form>
    @include('livewire.modal.deleteModel2')

    <div class="card-body">
        <div class="table-responsive">
            <table id="myTable" class="table">
                <thead class=" text-primary">
                <th>
                    {{__("restaurant.Banner")}}
                </th>

                <th>
                    {{__("restaurant.BannerState")}}
                </th>
                <th>
                    {{__("masterControl.Action")}}
                </th>


                </thead>

                <tbody>
                @foreach($banners as $banner)
                    <tr>

                        <td>
                            <img class="banner-loaded " src=" {{asset('storage/images/sliders/restaurant/'.$banner->name)}}">

                        </td>

                        <td>
                            <div class="form-group ">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox"  class="custom-control-input" wire:change="changeBannerState({{$banner->id}})" id="customSwitchBa{{$banner->id}}"  @if($banner->state==1) checked=" " @endif>
                                    <label class="custom-control-label" for="customSwitchBa{{$banner->id}}"> </label>
                                </div>
                            </div>

                        </td>
                        <td>
                            <a href="#delCountry2" data-toggle="modal" wire:click="deleteCall({{$banner->id}})">
                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                    <i class="fa fa-trash-o fa-2x"></i>
                                </button>
                            </a>
                        <td>

                    </tr>


                @endforeach

                </tbody>
            </table>

            <div class="d-flex flex-row justify-content-center">
                {{ $banners->links() }}
            </div>
        </div>
    </div>
</div>
