<div>
{{--    about...{{$restaurantID}}--}}

    <form wire:submit.prevent="submit">


        <div class="row">
            <div class="col-md-4">
                <div class="form-group ">
                    <label> {{__("restaurant.name")}}</label>
                    <input type="text" class="form-control"  wire:model.defer="name" placeholder="{{__("restaurant.EnterName")}}">
                    @error('name') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label> {{__("restaurant.nameEN")}}</label>
                    <input type="text" class="form-control"  wire:model.defer="nameEN" placeholder="{{__("restaurant.EnterNameEN")}}">
                    @error('nameEN') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <label> {{__("restaurant.wallet")}}</label><br/>
                    <strong> 0 &nbsp; $</strong>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group ">
                    <div class="custom-control custom-switch">
{{--                        <label> {{__("restaurant.IsRecommended")}}</label>--}}
                        <input type="checkbox"  wire:model.defer="isRecommended" class="custom-control-input" wire:change="changeRecommendedState" id="customSwitch1"  @if($isRecommended==1) checked=" " @endif>
                        <label class="custom-control-label" for="customSwitch1"> {{__("restaurant.IsRecommended")}}</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-7">
                <div class="form-group">
                    <label>{{__("restaurant.Address")}}</label>
                    <input type="text" class="form-control" placeholder="{{__("restaurant.enter_address")}}"wire:model.defer="address" >
                </div>
            </div>
            <div class="col-md-4 handle-input">
                <div  wire:loading wire:target="restaurantImage">
                    <x-control-panel.loading />
                </div>
                {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                <label class="d-flex flex-row">
                    <img class="img-loaded "
                         src="  @if ($restaurantImage) {{ $restaurantImage->temporaryUrl() }}
                         @elseif($restaurantImageOld) {{asset('storage/images/avatars/'.$restaurantImageOld)}}      @endif">

                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="restaurantImage" >
                </label>

                @error('restaurantImage') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="owner-r-v">

            <div class="alert alert-warning d-flex flex-row justify-content-center">
                <strong> {{__("restaurant.owner")}}</strong>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <label> {{__("restaurant.firstName")}}</label>
                        <input type="text" class="form-control"  wire:model.defer="firstName" placeholder="{{__("restaurant.EnterFirstName")}}">
                        @error('firstName') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                        <label> {{__("restaurant.fatherName")}}</label>
                        <input type="text" class="form-control"  wire:model.defer="fatherName" placeholder="{{__("restaurant.EnterFatherName")}}">
                        @error('fatherName') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group ">
                        <label> {{__("restaurant.lastName")}}</label>
                        <input type="text" class="form-control"  wire:model.defer="lastName" placeholder="{{__("restaurant.EnterLastName")}}">
                        @error('lastName') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-md-8">
                    <div class="form-group">
                        <label  for="exampleInputEmail1"> {{__("restaurant.Email_Address")}}</label>
                        <input type="email"  class="form-control" placeholder="{{__("restaurant.Enter_Email_Address")}}" wire:model.defer="email">
                        @error('email') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>{{__("restaurant.Password")}}</label>
                        <input type="text" class="form-control" placeholder="{{__("restaurant.enterPassword")}}"wire:model.defer="password" >
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-6 pr-1">
                <div class="form-group">
                    <label  for=""> {{__("restaurant.MinimumOrderAmount")}}</label>
                    <input type="number"  class="form-control" placeholder="{{__("restaurant.EnterMinimumOrderAmount")}}" wire:model.defer="minimumOrderAmount">
                    @error('minimumOrderAmount') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="form-group">
                    <label>{{__("restaurant.DeliveryMode")}}</label>
                    <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="deliveryMode">
                        <option>---</option>
                        <option value="delivery">Delivery</option>
                        <option value="self">Self Pick-Up</option>
                        <option value="both">Both</option>
                    </select>
                    @error('deliveryMode') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 pr-1">
                <div class="form-group">
                    <label  for=""> {{__("restaurant.DeliveryManCommission")}}</label>
                    <input type="number" step="0.01"  class="form-control" placeholder="{{__("restaurant.EnterDeliveryManCommission")}}" wire:model.defer="deliveryManCommission">
                    @error('deliveryManCommission') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-4 pl-1">
                <div class="form-group">
                    <label>{{__("restaurant.PickCommission")}}</label>
                    <input type="number" step="0.01" class="form-control" placeholder="{{__("restaurant.EnterPickCommission")}}" wire:model.defer="pickCommission">

                    @error('PickCommission') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-md-4 pl-1">
                <div class="form-group">
                    <label>{{__("restaurant.OurCommission")}}</label>
                    <input type="number" step="0.01" class="form-control" placeholder="{{__("restaurant.EnterOurCommission")}}" wire:model.defer="ourCommission">

                    @error('ourCommission') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4 ">
                <div class="form-group">
                    <label>{{__("restaurant.Phone")}}</label>
                    <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone")}}" wire:model.defer="phone1">
                    @error('phone1') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="form-group">
                    <label>{{__("restaurant.Phone2")}}</label>
                    <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone2")}}" wire:model.defer="phone2">
                    @error('phone2') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="form-group">
                    <label>{{__("restaurant.Phone3")}}</label>
                    <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone3")}}" wire:model.defer="phone3">
                    @error('phone') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 ">
                <div class="form-group">
                    <label>{{__("restaurant.provider_url")}}</label>
                    <input type="text" class="form-control" placeholder="{{__("restaurant.Enter_provider_url")}}" wire:model.defer="providerURL">
                    @error('provider_url') <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>


{{--        @if ($showSelectCountry)--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}

{{--                    <div class="form-group">--}}
{{--                        <label>{{__("restaurant.selectCountry")}}</label>--}}
{{--                        <select class="form-control" id="exampleFormControlSelect2" wire:change="getCities" wire:model.defer="selectedCountry">--}}
{{--                            <option>---</option>--}}
{{--                            @foreach($countries as $country)--}}
{{--                                <option  value="{{$country->country_id}}">{{$country->name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                        @error('selectedCountry') <div class="alert alert-danger">--}}
{{--                            <button type="button" class="close" data-dismiss="alert">&times;</button>--}}
{{--                            {{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}


{{--                </div>--}}

{{--                <div class="col-md-6">--}}
{{--                    <div class="form-group">--}}
{{--                        <label>{{__("restaurant.selectCity")}}</label>--}}
{{--                        <select class="form-control" id="exampleFormControlSelect2"  wire:model.defer="selectedCity">--}}
{{--                            <option>---</option>--}}
{{--                            @if($cities)--}}
{{--                                @foreach($cities as $city)--}}
{{--                                    <option value="{{$city->city_id}}">{{$city->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </select>--}}
{{--                        @error('selectedCity') <div class="alert alert-danger">--}}
{{--                            <button type="button" class="close" data-dismiss="alert">&times;</button>--}}
{{--                            {{ $message }}</div>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}




        <div class="form-group d-flex align-items-center flex-column">

            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
        </div>
    </form>
</div>
