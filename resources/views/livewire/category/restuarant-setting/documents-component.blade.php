<div>
    <form wire:submit.prevent="submit">
        <div class="row">
            <div class="col-md-12 handle-input">
                <div  wire:loading wire:target="restaurantDocument">
                    <x-control-panel.loading />
                </div>
                @if ($restaurantDocument)
                    <img class="document-loaded " src=" {{ $restaurantDocument->temporaryUrl() }}">
                @endif
                <label class="d-flex flex-row">
                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="restaurantDocument" >
                </label>

                @error('restaurantDocument') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group d-flex align-items-center flex-column">

            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
        </div>
    </form>
    @include('livewire.modal.deleteModel3')

    <div class="card-body">
        <div class="table-responsive">
            <table id="myTable" class="table">
                <thead class=" text-primary">
                <th>
                    {{__("restaurant.Document")}}
                </th>

                <th>
                    {{__("masterControl.Action")}}
                </th>


                </thead>

                <tbody>
                @foreach($documents as $document)
                    <tr>

                        <td>
                            @if($document->deleteByOwner==1)

                            <p class=" alert alert-info">
                                {{__("restaurant.underReview")}}
                            </p>
                            @endif

                            <img class="document-loaded " src=" {{asset('storage/images/document/restaurant/'.$document->name)}}">

                        </td>


                        <td>
                            <a href="#delCountry3" data-toggle="modal" wire:click="deleteCall({{$document->id}})">
                                <button type="button" class="btn btn-outline-danger p-2 m-1">
                                    <i class="fa fa-trash-o fa-2x"></i>
                                </button>
                            </a>
                        <td>

                    </tr>


                @endforeach

                </tbody>
            </table>

            <div class="d-flex flex-row justify-content-center">
                {{ $documents->links() }}
            </div>
        </div>
    </div>
</div>
