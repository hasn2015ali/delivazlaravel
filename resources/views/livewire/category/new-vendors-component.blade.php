<div>
    @include('livewire.modal.deleteModel2')
    @include('livewire.modal.vendor.editRegistration')

    <div class="d-flex flex-row justify-content-start">
        <h4 class="address">
            {{__("vendor.NewVendors")}}
        </h4>
    </div>
    <div class="table-responsive">
        <table id="myTable" class="table">
            <thead class=" text-primary">
            <th>

                {{__("vendor.name")}}
            </th>
            <th>
                {{__("vendor.OwnerName")}}
            </th>
            <th>
                {{__("vendor.Country")}}
            </th>
            <th>
                {{__("vendor.City")}}
            </th>
            <th>
                {{__("vendor.created_at")}}
            </th>


            <th>
                {{__("vendor.Action")}}
            </th>
            </thead>
            <tbody>


            @forelse($newVendors as $vendor)
                <tr>
                    <td>
                        {{$vendor->name}}
                    </td>
                    <td>
                        {{$vendor->user->firstName}} &nbsp; {{$vendor->user->fatherName}}&nbsp; {{$vendor->user->lastName}}
                    </td>
                    <td>
                        {{$vendor->country->translation->where('code_lang',\Illuminate\Support\Facades\App::getLocale())->first()->name}}
                    </td>
                    <td>
                        {{$vendor->city->translation->where('code_lang',\Illuminate\Support\Facades\App::getLocale())->first()->name}}
                    </td>
                    <td>
                        {{--                                            {{$vendor->created_at}}--}}
                        {{(\Carbon\Carbon::parse($vendor->created_at)->format('d/m/Y'))}}

                    </td>


                    <td>
                        <a href="#editRegistration" data-toggle="modal"
                           wire:click="updateCall({{$vendor->id}})"
                           class="btn btn-outline-warning p-2 m-1">
                            {{--                                                {{__("vendor.vendorSettings")}}--}}
                            <i class="fas fa-edit fa-2x"></i>

                        </a>
                        <a href="#delCountry2" data-toggle="modal"
                           wire:click="deleteCall({{$vendor->id}})">
                            <button type="button" class="btn btn-outline-danger p-2 m-1">
                                <i class="fa fa-trash-o fa-2x"></i>
                            </button>
                        </a>
                    </td>



                    @empty
                        <div class="d-flex flex-row justify-content-center alert alert-info">
                            {{__("vendor.NoNewVendors")}}
                        </div>

            @endforelse

            </tbody>
        </table>
        <div class="d-flex flex-row justify-content-center">

            @if($newVendors)
                {{ $newVendors->links() }}
            @endif
        </div>

    </div>
</div>
