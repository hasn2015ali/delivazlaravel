<div wire:ignore.self class="modal fade" id="productFilter">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("vendor.EditFiltersForThisProduct")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div class="d-flex flex-row justify-content-center">
                    <div  wire:loading wire:target="UpdateProductFilters">
                        <x-control-panel.loading />
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="address">{{__("vendor.oldProductFilter")}}</label>
                        </div>
                    </div>
                </div>
                <div class="row p-3" >
                    @if($product)
                        @foreach($product->filters as $productFilter)
                            {{--{{dd($productFilter)}}--}}
                            <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                <label>{{$productFilter->translation->where('code_lang',$langCode)->first()->name}} </label>
                            </div>

                        @endforeach
                    @endif
                </div>
                <hr style="background: #f96332; "/>
                <form wire:submit.prevent="UpdateProductFilters">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="address">{{__("vendor.updateProductFilter")}}</label>
                                @error('updatedProductFilters') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row p-3" >
                        @foreach($allProductFilters as $vendorFilter)
                            @foreach($vendorFilter as $productFilter)
                                <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                    <label>{{$productFilter['name']}} </label>
                                    <div class="custom-control custom-switch ">
                                        <input value="{{$productFilter['product_filter_id'] }}" type="checkbox" class="custom-control-input" wire:model.defer="updatedProductFilters"  id="updatedProduct{{$productFilter['product_filter_id'] }}"  checked=" ">
                                        <label class="custom-control-label" for="updatedProduct{{$productFilter['product_filter_id'] }}"></label>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach

                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>

            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
