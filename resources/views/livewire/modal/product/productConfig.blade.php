<div wire:ignore.self class="modal fade" id="productConfig">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("vendor.productSitting")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div  wire:loading wire:target="editProduct">
                    <x-control-panel.loading />
                </div>
                <form wire:submit.prevent="updateProductDetails">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group ">
                                <label> {{__("vendor.productName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="name" placeholder="{{__("vendor.EnterName")}}">
                                @error('name') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label>{{__("vendor.productContent")}}</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1"  wire:model.defer="content" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label> {{__("vendor.productWeight")}}</label>
                                <input type="number" step="0.001" class="form-control"  wire:model.defer="weight" placeholder="{{__("vendor.EnterWeight")}}">
                                @error('weight') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  handle-input">
                            <div  wire:loading wire:target="photo">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                {{--                                {{__("vendor.productPhoto")}}--}}
                                @if ($photo or $photoOld)
                                    <img class="img-loaded m-2"
                                         src="  @if ($photo) {{ $photo->temporaryUrl() }}
                                         @elseif($photoOld) {{asset('storage/images/product/'.$photoOld)}} @endif">
                                @else
                                    {{__("vendor.empty")}}
                                @endif
                                <input type="file" class="form-control file-upload photo-input" wire:model.defer="photo" >
                            </label>
                            @error('photo') <span class="error">{{ $message }}</span> @enderror

                        </div>
                        <div class="col-lg-6">
                            <div class="form-group handel-select">
                                <label class=""> {{__("vendor.Size")}}</label>
                                <select class="form-control  toggle-enabl " wire:model.defer="productSizeSelected">
                                    <option value="0">{{__("vendor.NoSize")}}</option>
                                    @foreach($allSizes as $size)
                                        <option value="{{$size->name}}">{{$size->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            @error('productSizeSelected') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label  for=""> {{__("vendor.productPrice")}}</label>
                                <input type="number" step="0.001"  class="form-control " placeholder="{{__("vendor.EnterProductPrice")}}" wire:model.defer="price">
                                @error('price') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 ">

                        </div>
                    </div>



                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>

                <hr style="background: #f96332; "/>
                <div class="d-flex flex-row justify-content-center">
                    <h6 class="address">{{__("vendor.images")}}</h6>
                </div>
                <div  wire:loading wire:target="AddPhotos">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="deleteProductPhoto">
                    <x-control-panel.loading />
                </div>

                    @if(count($oldImages)!=0)
                        <div class="row">
                            @foreach($oldImages as $image)
                                <div class="col-lg-4">
                                    <div class="card" style="width: 20rem;">
                                        <img src="{{asset('storage/images/product/'.$image->name)}}" style="">

                                        <div class="card-body">
                                            <button type="button" class="btn btn-outline-danger p-2 m-1" wire:click="deleteProductPhoto({{$image->id}})">
                                                <i class="fa fa-trash-o fa-2x"></i>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            @endforeach
                        </div>
                    @elseif(count($oldImages)==0)
                      <div class="d-flex flex-row justify-content-center alert alert-info">
                          {{__("vendor.NoImages")}}
                      </div>
                    @endif

                <div class="d-flex flex-row justify-content-center mt-2 mb-2">
                    <h6 class="content-text">{{__("vendor.AddNewImages")}}</h6>
                </div>
                <form wire:submit.prevent="AddPhotos">
                    <div class="row">
                        <div class="col-lg-10 handle-input">
                            <div wire:loading wire:target="images">
                                <x-control-panel.loading />
                            </div>
                            @if( !empty( $images ) )
                                <div class="row">
                                    @foreach ( $images as $image )
                                        <div class="col-lg-4">
                                            <img src="{{ $image->temporaryUrl() }}" alt="" style="width: 250px;" >

                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            <label>{{__("vendor.Add_Images")}}
                                <i class="fa fa-cloud-upload"></i>
                                <input type="file" class="form-control file-upload" wire:model="images" multiple>
                            </label>
                            @error('images.*')
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>

                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
