<div wire:ignore.self class="modal fade" id="addProduct">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("vendor.AddProduct")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submit">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group ">
                                <label> {{__("vendor.productName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="name" placeholder="{{__("vendor.EnterName")}}">
                                @error('name') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label>{{__("vendor.productContent")}}</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="content" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group ">
                                <label> {{__("vendor.productWeight")}}</label>
                                <input type="number" step="0.001" class="form-control"  wire:model.defer="weight" placeholder="{{__("vendor.EnterWeight")}}">
                                @error('weight') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6  handle-input">
                            <div  wire:loading wire:target="photo">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($photo)
                                    <img class="food-img-loaded " src="  {{ $photo->temporaryUrl() }}">
                                @endif

                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="photo" >
                            </label>
                            @error('photo') <span class="error">{{ $message }}</span> @enderror
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group handel-select">
                                <label class=""> {{__("vendor.Size")}}</label>
                                <select class="form-control  toggle-enabl " wire:model.defer="productSizeSelected">
                                    <option value="0">{{__("vendor.NoSize")}}</option>
                                    @foreach($allSizes as $size)
                                        <option value="{{$size->name}}">{{$size->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            @error('productSizeSelected') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label  for=""> {{__("vendor.productPrice")}}</label>
                                <input type="number" step="0.001" class="form-control " placeholder="{{__("vendor.EnterProductPrice")}}" wire:model.defer="price">
                                @error('price') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("vendor.selectProductFilter")}}</label>
                                @error('selectedProductFilters') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row p-3" >
                        @if(count($allProductFilters[0])!=0)
                        @foreach($allProductFilters as $vendorFilter)
                            @foreach($vendorFilter as $productFilter)
                                <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                    <label>{{ $productFilter['name'] }} </label>
                                    <div class="custom-control custom-switch ">
                                        <input value="{{$productFilter['product_filter_id'] }}" type="checkbox" class="custom-control-input" wire:model.defer="selectedProductFilters"  id="customSwitchProduct{{$productFilter['product_filter_id'] }}"  checked=" ">
                                        <label class="custom-control-label" for="customSwitchProduct{{$productFilter['product_filter_id'] }}"></label>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                        @else
                            <div class="alert alert-info">
                                {{__("vendor.validatProductFilter")}}
                            </div>
                        @endif

                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
