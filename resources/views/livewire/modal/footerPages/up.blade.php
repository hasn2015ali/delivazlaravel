<div wire:ignore.self class="modal fade" id="CountryTermUp">
    <div class="modal-dialog translation-modal">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("filter.up_filter")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('address') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('content') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="UpdateTerm">
                    <div class="form-group d-flex align-items-start flex-column">
                        <label> {{__("footerPages.Address")}}</label>
                        <input type="text" class="form-control"  wire:model.defer="address" placeholder="{{__("footerPages.EnterAddress")}}">

                    </div>

                    <div class="form-group d-flex align-items-start flex-column">
                        <label> {{__("footerPages.Content")}}</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="content" rows="8"></textarea>
                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
