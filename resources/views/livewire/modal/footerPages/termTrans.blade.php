<div wire:ignore.self class="modal fade" id="Termtrans">
    <div class="modal-dialog translation-modal">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div align="center">  {{__("footerPages.Translation")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div wire:loading wire:target="getAllTranslation">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="submitTranslation">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="openEdit">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="openDelete">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="closeEdit">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="closeDelete">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="editTranslation">
                    <x-control-panel.loading/>
                </div>
                <div wire:loading wire:target="deleteTranslation">
                    <x-control-panel.loading/>
                </div>

                <div x-data="{ isShowing: false }">
                    <div class="d-flex flex-row justify-content-between">
                        <button type="button" class="btn  p-2 m-1 mb-3 "
                                :class="{'btn-outline-success' : !isShowing, 'btn-outline-danger ': isShowing}"
                                @click="isShowing = !isShowing">
                            <i x-show.transition.in.duration.500ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="!isShowing"
                               class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            <i x-show.transition.in.duration.500ms.origin.top.right.scale.85.out.duration.10ms.origin.bottom.left.scale.95="isShowing"
                               class="fas fa-times-circle fa-2x"></i>
                        </button>
                    </div>

                    @error('contentTrans')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                    @error('contentTrans')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                    @error('selectedLang')
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                    <div class="row"
                         x-show.transition.in.duration.500ms.origin.top.right.scale.85.out.duration.500ms.opacity="isShowing"
                         x-cloak>
                        <div class="col-lg-9">
                            <form wire:submit.prevent="submitTranslation">
                                <div class="form-group d-flex align-items-start flex-column">
                                    <label> {{__("footerPages.Address")}}</label>
                                    <input type="text" class="form-control" wire:model.defer="addressTrans"
                                           placeholder="{{__("footerPages.EnterAddress")}}">

                                </div>

                                <div class="form-group d-flex align-items-start flex-column">
                                    <label> {{__("footerPages.Content")}}</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1"
                                              wire:model.defer="contentTrans" rows="8"></textarea>
                                </div>
                                <div class="form-group d-flex align-items-start flex-column handel-select">
                                    <label for="exampleFormControlSelect1">{{__("footerPages.langSelecte")}}</label>

                                    <select class="form-control" id="exampleFormControlSelect1"
                                            wire:model.defer="selectedLang">
                                        <option hidden>---</option>
                                        @foreach($langs as $lang)
                                            <option value="{{$lang->code}}">{{$lang->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group d-flex align-items-start flex-column">
                                    <input type="submit" class="btn btn-outline-primary"
                                           value="{{__("masterControl.Submit")}}">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                <div class="table-responsive" x-data="{}">
                    {{--                    <table id="myTable" class="table"  @click.away="$wire.closeEdit()">--}}
                    <table id="myTable" class="table">

                        <thead class=" text-primary ">
                        <th class="table-th-address">

                            {{__("footerPages.Address")}}
                        </th>
                        <th>
                            {{__("footerPages.Content")}}
                        </th>
                        <th>
                            {{__("footerPages.Language")}}
                        </th>

                        <th>
                            {{__("footerPages.Action")}}
                        </th>
                        </thead>
                        <tbody class="translation-text">
                        @if(count($allInsertedTrans)!=0)
                            @foreach($allInsertedTrans as $index => $term)
                                <?php $index = $index + 1; ?>
                                @if($index!=$editTransIndex and $index!=$deleteTransIndex)
                                    <tr>
                                        <td>
                                            <div @if($term['code_lang']!="en") wire:click="openEdit({{$index}})" @endif>
                                                {{$term['address']}}
                                            </div>
                                        </td>
                                        <td>
                                            <div @if($term['code_lang']!="en") wire:click="openEdit({{$index}})" @endif>
                                                {{$term['content']}}
                                            </div>
                                        </td>

                                        <td>
                                            <label style="font-size: 16px">
                                                <i class="fa fa-check ml-1 mr-1"
                                                   aria-hidden="true"></i> {{strtoupper($term['code_lang'])}}
                                            </label>
                                        </td>
                                        <td>
                                            @if($term['code_lang']!="en")
                                                @canany(['is-manager','is-translator'])
                                                    <button wire:click="openEdit({{$index}})"
                                                            class="btn btn-outline-success p-2 m-1">
                                                        <i class="fa fa-refresh fa-2x" aria-hidden="true"></i>
                                                    </button>
                                                    <button type="button"
                                                            wire:click="openDelete({{$index}})"
                                                            class="btn btn-outline-danger p-2 m-1">
                                                        <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                                    </button>
                                                @endcan
                                            @endif
                                        </td>
                                    </tr>
                                @elseif($index==$editTransIndex)
                                    <tr class="">
                                        <td class="">
                                            <input type="text" class="form-control"
                                                   wire:model.defer="allInsertedTrans.{{$index-1}}.address"
                                                   placeholder="{{__("footerPages.EnterAddress")}}">
                                            @error('allInsertedTrans.*.address')
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert">&times;
                                                </button>
                                                {{ $message }}</div>
                                            @enderror
                                        </td>
                                        <td class="">
                                            <textarea class="form-control" id="exampleFormControlTextarea1"
                                                      wire:model.defer="allInsertedTrans.{{$index-1}}.content"
                                                      rows="8"></textarea>
                                            @error('allInsertedTrans.*.content')
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert">&times;
                                                </button>
                                                {{ $message }}</div>
                                            @enderror
                                        </td>

                                        <td class="">
                                            <button type="button" class="btn btn-outline-primary"
                                                    wire:click="editTranslation({{$index-1}})">
                                                {{__("masterControl.Submit")}}
                                            </button>
                                        </td>
                                        <td class="">
                                            @canany(['is-manager','is-translator'])
                                                <button wire:click="closeEdit()"
                                                        class="btn btn-outline-danger p-2 m-1">
                                                    <i class="fas fa-times-circle fa-2x"></i>
                                                </button>
                                            @endcan

                                        </td>
                                    </tr>
                                @elseif($index==$deleteTransIndex)
                                    <tr>
                                        <td>

                                        </td>
                                        <td>
                                            <div class="alert alert-danger">
                                                {{__("masterControl.Sure_To_Delete")}}

                                            </div>
                                            <button class="btn btn-outline-danger"
                                                    wire:click="deleteTranslation({{$index-1}})"> {{__("masterControl.Delete")}}</button>
                                            <button type="button" class="btn btn-outline-warning"
                                                    wire:click="closeDelete">{{__("masterControl.Cancel")}}</button>

                                        </td>
                                        <td>

                                        </td>
                                        <td>

                                        </td>

                                    </tr>
                                @endif

                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning"
                        data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>


