<div wire:ignore.self class="modal fade" id="addFoodSize">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("restaurant.EditSizes")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submitFoodSize">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group handel-select">
                                <label class=""> {{__("restaurant.Size")}}</label>
                                <select class="form-control  toggle-enabl " wire:model.defer="foodSizeSelected">
                                    <option value=""></option>
                                    @foreach($allSizes as $size)
                                        <option value="{{$size->name}}">{{$size->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            @error('foodSizeSelected') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                            <div class="form-group ">
                                <label> {{__("restaurant.sizePrice")}}</label>
                                <input type="number" step="0.001" class="form-control"  wire:model.defer="sizePrice" placeholder="{{__("restaurant.EnterSizePrice")}}">
                                @error('sizePrice') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group ">
                                <label> {{__("restaurant.sizeWeight")}}</label>
                                <input type="number" step="0.001" class="form-control"  wire:model.defer="sizeWeight" placeholder="{{__("restaurant.EnterSizeWeight")}}">
                                @error('sizeWeight') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group ">
                                <label> {{__("restaurant.sizeContent")}}</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="sizeContent" rows="3"></textarea>
                                @error('sizeContent') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
                <hr style="background: #f96332; "/>
                <div  wire:loading wire:target="editFoodSize">
                    <x-control-panel.loading />
                </div>
                <div class="d-flex flex-column align-items-start justify-content-start">
                    <h5 class="address">{{__("restaurant.FoodSizes")}}</h5>
                    <div style="width: 100%" class="row">
                        <div class="col-lg-3">
                            <p class="address">
                                {{__("restaurant.SizesName")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.sizeWeight")}}
                            </p>
                        </div>
                        <div class="col-lg-3">
                            <p class="address">
                                {{__("restaurant.sizePrice")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.sizeContent")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.delete")}}
                            </p>
                        </div>
                    </div>

                     @if($allSizesForFood)

                        @foreach($allSizesForFood as $size)
                            <div style="width: 100%" class="row">
                                <div class="col-lg-3">
                                    <p class="content-text">
                                        {{$size->size}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$size->weight}}
                                    </p>
                                </div>
                                <div class="col-lg-3">
                                    <p class="content-text">
                                        {{$size->price}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$size->content}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-outline-danger p-2 m-1" wire:click="deleteFoodSize({{$size->id}})">
                                        <i class="fa fa-trash-o fa-2x"></i>
                                    </button>
                                </div>
                            </div>
                        @endforeach
                            @if(count($allSizesForFood)==0)
                            <div style="width: 100%" class="alert alert-info">
                                {{__("restaurant.noSizeForThisFood")}}
                            </div>
                    @endif

                    @endif
                </div>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
