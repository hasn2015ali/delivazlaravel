<div wire:ignore.self class="modal fade" id="addAdditions">
    <div style="max-width: 95%" class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("restaurant.AddFoodAdditions")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submitAddition">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label> {{__("restaurant.AdditionName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="AdditionName" placeholder="{{__("restaurant.EnterAdditionName")}}">
                                @error('AdditionName') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("restaurant.AdditionContent")}}</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="AdditionContent" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label  for=""> {{__("restaurant.AdditionPrice")}}</label>
                                <input type="number"  step="0.001" class="form-control " placeholder="{{__("restaurant.EnterAdditionPrice")}}" wire:model.defer="AdditionPrice">
                                @error('AdditionPrice') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label  for=""> {{__("restaurant.AdditionWeight")}}</label>
                                <input type="number"  step="0.001" class="form-control " placeholder="{{__("restaurant.EnterAdditionWeight")}}" wire:model.defer="AdditionWeight">
                                @error('AdditionWeight') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6  handle-input">
                            <div  wire:loading wire:target="AdditionPhoto">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($AdditionPhoto)
                                    <img class="food-img-loaded " src="  {{ $AdditionPhoto->temporaryUrl() }}">
                                @endif

                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="AdditionPhoto" >
                            </label>
                            @error('AdditionPhoto') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
                <hr style="background: #f96332; "/>
                <div  wire:loading wire:target="editFoodAddition">
                    <x-control-panel.loading />
                </div>
                <div class="d-flex flex-column align-items-start justify-content-start">
                    <h5 class="address">{{__("restaurant.FoodAdditions")}}</h5>
                    <div style="width: 100%" class="row">
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.AdditionName")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.AdditionContent")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.AdditionPrice")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.AdditionWeight")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.AdditionPhoto")}}
                            </p>
                        </div>
                        <div class="col-lg-2">
                            <p class="address">
                                {{__("restaurant.delete")}}
                            </p>
                        </div>
                    </div>

                    @if($allAdditionForFood)

                        @foreach($allAdditionForFood as $addition)
                            <div style="width: 100%" class="row mb-2" >
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$addition->name}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$addition->content}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$addition->price}}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    <p class="content-text">
                                        {{$addition->weight }}
                                    </p>
                                </div>
                                <div class="col-lg-2">
                                    @if($addition->photo)
                                        <img class="food-img-view "  src="{{asset('storage/images/food/addition/'.$addition->photo)}}"/>
                                    @else
                                        {{__("restaurant.empty")}}
                                    @endif
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" class="btn btn-outline-danger p-2 m-1" wire:click="deleteFoodAddition({{$addition->id}})">
                                        <i class="fa fa-trash-o fa-2x"></i>
                                    </button>
                                </div>
                            </div>
                        @endforeach
                        @if(count($allAdditionForFood)==0)
                            <div style="width: 100%" class="alert alert-info">
                                {{__("restaurant.noAdditionForThisFood")}}
                            </div>
                        @endif

                    @endif
                </div>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
