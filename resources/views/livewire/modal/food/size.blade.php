<div wire:ignore.self class="modal fade" id="addSize">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("restaurant.Sizes")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submitSize">

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group ">
                                <label> {{__("restaurant.Size")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="size" placeholder="{{__("restaurant.EnterSize")}}">
                                @error('size') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>





                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>

                <div class="d-flex flex-column align-items-start justify-content-start">
                    <h5 class="address">{{__("restaurant.MySizes")}}</h5>
                    @foreach($allSizes as $size)
                   <div style="width: 95%" class="row">
                   <div class="col-lg-8">
                       <p class="content-text">
                           {{$size->name}}
                       </p>
                   </div>
                  <div class="col-lg-3">
                      <button type="button" class="btn btn-outline-danger p-2 m-1" wire:click="deleteSize({{$size->id}})">
                          <i class="fa fa-trash-o fa-2x"></i>
                      </button>
                  </div>
                   </div>
                    @endforeach

                    @if(count($allSizes)==0)
                        <div style="width: 100%" class="alert alert-info">
                            {{__("restaurant.noSizeForThisRestaurant")}}
                        </div>
                    @endif

                </div>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
