<div wire:ignore.self class="modal fade" id="addDetails">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("country.Add_CountryDetails")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submit">
                    <div class="form-group ">
                        <label> {{__("country.currency")}}</label>
                        <input type="text" class="form-control"  wire:model.defer="currency" placeholder="{{__("country.EnterCurrency")}}">
                        @error('currency') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group ">
                        <label> {{__("country.currencyCode")}}</label>
                        <input type="text" class="form-control" wire:model.defer="currencyCode" placeholder="{{__("country.EnterCurrencyCode")}}">
                        @error('currencyCode') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group ">
                        <label> {{__("country.numberCode")}}</label>
                        <input type="text" class="form-control" wire:model.defer="codeNumber" placeholder="{{__("country.EnterNumberCode")}}">
                        @error('numberCode') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                    <div class="d-flex align-items-start flex-column">
                        <label> {{__("country.phoneNumber")}}</label>
                        <div style="width: 100%" class="d-flex flex-row justify-content-between">
                            <div style="width: 48%" class="form-group">
                                <input type="text" class="form-control"  wire:model.defer="phone1" placeholder="{{__("country.EnterPhoneNumber")}}">

                            </div>
                            <div style="width: 48%" class="handle-input">
                                <div  wire:loading wire:target="icon1">
                                    <x-control-panel.loading />
                                </div>
                                {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                                <label class="d-flex flex-row">
                                    @if ($icon1)
                                        <img class="food-img-loaded " src="  {{ $icon1->temporaryUrl() }}">
                                    @elseif($oldIcon1)
                                        <img class="food-img-loaded " src="{{asset('storage/images/phone/country/'.$oldIcon1)}}">
                                    @endif

                                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="icon1" >
                                </label>
                                @error('icon1') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        @error('phone1') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror

                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
