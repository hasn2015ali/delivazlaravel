<div wire:ignore.self class="modal fade" id="translateCountryUp">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("country.up_trans")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                @error('trans') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('selectedLang') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror

                <form wire:submit.prevent="update({{$transId}})">
                    <div class="form-group d-flex align-items-center flex-column">
                        <label> {{__("country.translate")}}</label>

                        <input type="text" class="form-control"  wire:model.lazy="trans" placeholder="{{__("country.EnterName")}}">

                    </div>
                    <div class="form-group d-flex align-items-center flex-column">
                        <label for="exampleFormControlSelect1">{{__("country.langSelecte")}}</label>

                        <select class="form-control" id="exampleFormControlSelect1" wire:model.lazy="selectedLang">

                            @foreach($langs as $lang)
                                <option value="{{$lang->id}}" @if($selectedLangName==$lang->name) selected @endif>{{$lang->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.up")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
