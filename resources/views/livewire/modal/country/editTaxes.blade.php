<div wire:ignore.self class="modal fade" id="addTax">
    <script src="https://phpcoder.tech/multiselect/js/jquery.multiselect.js"></script>

    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("country.up_country_Taxes")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('name') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="update">
                    <div class="form-group ">

                        <label for="exampleFormControlSelect1">{{__("tax.SelectTaxes")}}</label>
                        <div wire:ignore>
                            <select  multiple id="taxes_select">
                                @foreach($allTaxes as $tax)
                                    <option value="{{$tax->id}}">{{$tax->name}} &nbsp;
                                        @if( $tax->type=="Ratio")
                                            {{$tax->value}}%
                                        @else
                                            {{$tax->value}}

                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div>

{{--                        <select class="form-control" id="exampleFormControlSelect1"--}}
{{--                                wire:model.defer="selectedTaxes" multiple>--}}
{{--                            <option hidden>---</option>--}}
{{--                          --}}
{{--                        </select>--}}
                        @error('selectedTaxes') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group d-flex justify-content-center">
                        <div wire:loading wire:target="update">
                            <x-control-panel.loading/>
                        </div>
                        <input type="submit"
                               wire:loading.remove wire:target="update"
                               class="btn btn-outline-primary" value="{{__("masterControl.up")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex justify-content-center">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery('#taxes_select').multiselect({
        columns: 1,
        placeholder: 'Select Taxes',
        search: true,
        selectAll: true
    });

    $(document).ready(function () {
        // $('#taskSelect').select2();

        $('#taxes_select').on('change', function (e) {
        @this.set('selectedTaxes', $(this).val());
        });

        $('.ms-selectall').on('click', function (e) {
            $(".ms-selectall").text()==="Select all"?$(".ms-selectall").text("Unselect all"):$(".ms-selectall").text("Select all");
        });

    });

</script>
