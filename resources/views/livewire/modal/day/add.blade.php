<div wire:ignore.self class="modal fade" id="addDay">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("workSystem.Add_Day")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('name') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="submitDay">
                    <div class="form-group d-flex align-items-center flex-column">
                        <label> {{__("workSystem.dayName")}}</label>
                        <input type="text" class="form-control"  wire:model.lazy="dayName" placeholder="{{__("workSystem.EnterName")}}">

                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
