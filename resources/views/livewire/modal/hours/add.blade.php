<div wire:ignore.self class="modal fade" id="addWorkHour">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("workSystem.Add_PartTime")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('startTimeHour') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('startTimeMinutes') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('endTimeHour') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('endTimeMinutes') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="submitPartTime">

                    <div class="form-group d-flex flex-column align-items-center">
                        <h5> {{__("workSystem.Start_Part_Time")}}</h5>
                        <div class="d-flex flex-row justify-content-between">
                            <label class="mr-3 mb-1"> {{__("workSystem.Hour")}}</label>
                            <label class="mr-3 mb-1"> {{__("workSystem.Minutes")}}</label>
                        </div>
                        <div class="d-flex flex-row justify-content-around">
                            <button wire:click="increaseHour" type="button" class="btn btn-outline-success p-2 mr-3 mb-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                            <button wire:click="increaseMinutes" type="button" class="btn btn-outline-success p-2 mr-3 mb-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="d-flex flex-row justify-content-around">
                            <input type="number" class="form-control mr-3 mb-1"  wire:model.lazy="startTimeHour" placeholder="{{__("workSystem.Enter_Start_Part_Time")}}">

                            <input type="number" class="form-control mr-3 mb-1"  wire:model.lazy="startTimeMinutes" placeholder="{{__("workSystem.Enter_Start_Part_Time")}}">

                        </div>

                        <div class="d-flex flex-row justify-content-around">
                            <button wire:click="minHour" type="button" class="btn btn-outline-primary p-2 mr-3 mb-1">
                                <i class="fa fa-minus" aria-hidden="true"></i>

                            </button>
                            <button wire:click="minMinutes" type="button" class="btn btn-outline-primary p-2 mr-3 mb-1">
                                <i class="fa fa-minus" aria-hidden="true"></i>

                            </button>
                        </div>

                    </div>


                    <div class="form-group d-flex flex-column align-items-center mt-5">
                        <h5> {{__("workSystem.End_Part_Time")}}</h5>
                        <div class="d-flex flex-row justify-content-between">
                            <label class="mr-3 mb-1"> {{__("workSystem.Hour")}}</label>
                            <label class="mr-3 mb-1"> {{__("workSystem.Minutes")}}</label>
                        </div>
                        <div class="d-flex flex-row justify-content-around">
                            <button wire:click="increaseEndHour" type="button" class="btn btn-outline-success p-2 mr-3 mb-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                            <button wire:click="increaseEndMinutes" type="button" class="btn btn-outline-success p-2 mr-3 mb-1">
                                <i class="fa fa-plus fa-2x" aria-hidden="true"></i>
                            </button>
                        </div>
                        <div class="d-flex flex-row justify-content-around">
                            <input type="number" class="form-control mr-3 mb-1"  wire:model.lazy="endTimeHour" placeholder="{{__("workSystem.Enter_Start_Part_Time")}}">
                            <input type="number" class="form-control mr-3 mb-1"  wire:model.lazy="endTimeMinutes" placeholder="{{__("workSystem.Enter_Start_Part_Time")}}">

                        </div>

                        <div class="d-flex flex-row justify-content-around">
                            <button wire:click="minEndHour" type="button" class="btn btn-outline-primary p-2 mr-3 mb-1">
                                <i class="fa fa-minus" aria-hidden="true"></i>

                            </button>
                            <button wire:click="minEndMinutes" type="button" class="btn btn-outline-primary p-2 mr-3 mb-1">
                                <i class="fa fa-minus" aria-hidden="true"></i>

                            </button>
                        </div>

                    </div>


                    {{--                    <div class="form-group d-flex align-items-center flex-column">--}}
{{--                        <label> {{__("workSystem.End_Part_Time")}}</label>--}}
{{--                        <input type="time" class="form-control" wire:model.lazy="endTime" placeholder="{{__("workSystem.Enter_End_Part_Time")}}">--}}
{{--                    </div>--}}
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
