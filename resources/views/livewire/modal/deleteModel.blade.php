<div wire:ignore.self class="modal fade" id="delCountry">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center"> {{__("masterControl.deleteSure")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div class="alert alert-danger d-flex align-items-center flex-column">
                    {{__("masterControl.Sure_To_Delete")}}
                </div>
                <div class="d-flex align-items-center flex-column">

                    <button class="btn btn-outline-danger" wire:click="deleteAction"> {{__("masterControl.Delete")}}</button>
                </div>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button  type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>

