<div wire:ignore.self class="modal fade" id="addTax">
    <script src="https://phpcoder.tech/multiselect/js/jquery.multiselect.js"></script>
{{--    <link rel="stylesheet" href="https://phpcoder.tech/multiselect/css/jquery.multiselect.css">--}}

    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div align="center">  {{__("tax.Add_Tax")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div wire:loading wire:target="edit">
                    <x-control-panel.loading/>
                </div>

                <form wire:submit.prevent="submitTax">
                    <div class="form-group ">
                        <label> {{__("tax.Name")}}</label>
                        <input type="text" class="form-control" wire:model.defer="name"
                               placeholder="{{__("tax.EnterName")}}">
                        @error('name')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group ">

                        <label for="exampleFormControlSelect1">{{__("tax.SelectType")}}</label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.lazy="taxType">
                            <option hidden>---</option>
                            <option value="Ratio">Ratio</option>
                            <option value="Static">Static Value</option>
                        </select>
                        @error('taxType')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                    <div
                        class="form-group ">


                        <label for="exampleFormControlSelect1">{{__("tax.SelectCountries")}}</label>

                        <div wire:ignore>
                            <select  multiple id="country_select">
                                @foreach($countries as $country)
                                    <option class="multi-option"
                                            value="{{$country->country_id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label> {{__("tax.Value")}}
                            @if( $taxType=="Ratio")
                                %
                            @endif
                        </label>
                        <input type="text" class="form-control" wire:model.defer="taxValue"
                               placeholder="{{__("tax.EnterValue")}}">
                        @error('taxValue')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group d-flex align-items-center flex-column">
                        <div wire:loading wire:target="submitTax">
                            <x-control-panel.loading/>
                        </div>
                        <input type="submit" class="btn btn-outline-primary"
                               wire:loading.remove wire:target="submitTax"
                               value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning"
                        data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery('#country_select').multiselect({
        columns: 1,
        placeholder: 'Select Country',
        search: true,
        selectAll: true
    });

    $(document).ready(function () {
        // $('#taskSelect').select2();

        $('#country_select').on('change', function (e) {
        @this.set('selectedCountry', $(this).val());
        });

        $('.ms-selectall').on('click', function (e) {
            $(".ms-selectall").text()==="Select all"?$(".ms-selectall").text("Unselect all"):$(".ms-selectall").text("Select all");
        });

    });

</script>
