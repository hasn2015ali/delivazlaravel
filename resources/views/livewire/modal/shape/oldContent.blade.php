<div wire:ignore.self class="modal fade" id="addShowOldContent">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center"> {{__("city.Shape")}} {{$k}}</div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div  wire:loading wire:target="ShowOldContent">
                    <x-control-panel.loading />
                </div>

                @if($oldType=='Filters')
                    <div class="alert alert-info">
                        {{__("city.filterContent")}}<strong class="mr-3 ml-3">{{$oldType}}</strong>
                    </div>

                    <div class=" d-flex align-items-center flex-column">
                        @if($oldContent)
                            @foreach($oldContent as $res)
                                <div class="m-1 form-group"> <label style="font-size: 16px"> <i class="fa fa-check ml-1 mr-1" aria-hidden="true"></i> {{strtoupper($res['name'])}} </label></div>
                            @endforeach
                        @endif
                    </div>

                @endif

                @if($oldType=='Restaurants')
                    <div class="alert alert-info">
                        {{__("city.filterContent")}}<strong class="mr-3 ml-3">{{$oldType}}</strong>
                    </div>

                    <div class=" d-flex align-items-center flex-column">
                        @if($oldContent)
                            @foreach($oldContent as $res)
                                <div class="m-1 form-group"> <label style="font-size: 16px"> <i class="fa fa-check ml-1 mr-1" aria-hidden="true"></i> {{strtoupper($res['name'])}} </label></div>
                            @endforeach
                        @endif
                    </div>

                @endif


                @if($oldType=='AnyThing')
                    <div class="alert alert-info">
                        {{__("city.filterContent")}}<strong class="mr-3 ml-3">{{$oldType}}</strong>
                    </div>

                @endif

                @if($oldType=='Link')
                    <div class="alert alert-info">
                        {{__("city.filterContent")}}<strong class="mr-3 ml-3">{{$oldType}}</strong>
                    </div>

                @endif

            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button  type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>

