<div wire:ignore.self class="modal fade" id="addShapTrans">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.ShapeTranslation")}} <strong class="ml-3 mr-3">{{$k}}</strong> </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div  wire:loading wire:target="transShape">
                    <x-control-panel.loading />
                </div>

                <div  wire:loading wire:target="submitTranslat">
                    <x-control-panel.loading />
                </div>

                @error('trans') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('selectedLang') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="submitTranslat">
                    <div class="form-group d-flex align-items-center flex-column">
                        <label> {{__("city.translate")}}</label>
                        <input type="text" class="form-control"  wire:model.lazy="trans" placeholder="{{__("city.EnterTrans")}}">
                        {{--                        @error('trans') <span class="alert alert-danger">{{ $message }}</span> @enderror--}}
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">
                        <label for="exampleFormControlSelect1">{{__("city.langSelecte")}}</label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.lazy="selectedLang">
                            <option>---</option>
                            @foreach($langs as $lang)
                                <option value="{{$lang->code}}">{{$lang->name}}</option>
                            @endforeach
                        </select>
                        {{--                        @error('selectedLang') <span class="alert alert-danger">{{ $message }}</span> @enderror--}}
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>

                <hr style="background: #f96332; "/>
                <div class="alert alert-info">
                    {{__("city.ShapeTranslation")}}
                </div>

                <div class="row mb-3">
                    <div class="col-lg-3 address"> {{__("city.shapeAddress")}}</div>
                    <div class="col-lg-3 address"> {{__("city.translate")}}</div>
                    <div class="col-lg-3 address"> {{__("city.lang")}}</div>
                    <div class="col-lg-3 address"> {{__("city.Action")}}</div>
                </div>

                @if($allTransForShape)

                    @foreach($allTransForShape as $trans)

                        <div class="row">
                            <div class="col-lg-3"> {{$shapeTrans->translation->where("code_lang","en")->first()->trans}}</div>
                            <div class="col-lg-3"> {{$trans->trans}}</div>
                            <div class="col-lg-3"> {{strtoupper($trans->code_lang)}}</div>
                            <div class="col-lg-3">
                                <button type="button" class="btn btn-outline-danger p-2 m-1" wire:click="deleteTrans({{$trans->id}})">
                                    <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                    @endforeach
                @endif




            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
