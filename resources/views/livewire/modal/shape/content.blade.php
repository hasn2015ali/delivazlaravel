<div wire:ignore.self class="modal fade" id="addContent{{$j}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.ShapeContent")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('address'.$j) <div class="alert alert-danger">
                    <button type="button" class="close mr-2 ml-2" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror




                <div class="alert alert-info">
                    {{__("city.Shape")}}  <strong class="mr-2 ml-2"> {{$j}} </strong>    {{__("city.ShapeDetails")}}
                </div>
{{--                <hr style="background: #f96332; "/>--}}
                <form wire:submit.prevent="submitDetails({{$j}})">
                    <div class="d-flex flex-row justify-content-between nn">
                       <div style="width: 55%" class="handle-input">
                               <div  wire:loading wire:target="photo{{$j}}">
                                   <x-control-panel.loading />
                               </div>
                           <label class="d-flex flex-row">
                               @if (${"photo".$j} or ${"photoOld".$j} and ${"photoOld".$j}!="icon")
                                   <img class="max-w-100 m-2"
                                        src="  @if (${"photo".$j}) {{ ${"photo".$j}->temporaryUrl() }}
                                        @elseif(${"photoOld".$j}) {{asset('storage/images/shape/'.${"photoOld".$j})}} @endif">
                               @else
                                   {{__("city.empty")}}
                               @endif
                               <input type="file" class="form-control file-upload photo-input" wire:model.defer="photo{{$j}}" >
                           </label>
                           @error('photo'.$j) <div class="alert alert-danger">
                               <button type="button" class="close" data-dismiss="alert">&times;</button>
                               {{ $message }}</div>
                           @enderror
                       </div>

                        <div class="form-group d-flex align-items-start flex-column">
                            <label> {{__("city.shapeAddress")}}</label>
                            <input type="text" class="form-control"  wire:model.lazy="address{{$j}}" placeholder="{{__("city.EnterShapeAddress")}}">

                        </div>


                    </div>
                    <div class="form-group d-flex align-items-center flex-column">
                        <div  wire:loading wire:target="submitDetails">
                            <x-control-panel.loading />
                        </div>
                        <input
                            wire:loading.remove wire:target="submitDetails"
                            type="submit" class="btn btn-outline-primary " value="{{__("masterControl.Submit")}}"  >
                    </div>
                </form>
                <hr style="background: #f96332; margin: 2rem 0.5rem;"/>
                    <div class="alert alert-info">
                        {{__("city.ShapeContentInfo")}}
                    </div>

                @error('restaurants'.$j) <div class="alert alert-danger">
                    <button type="button" class="close mr-2 ml-2" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror

                @error('url'.$j) <div class="alert alert-danger">
                    <button type="button" class="close mr-2 ml-2" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror

                @error('content'.$j) <div class="alert alert-danger">
                    <button type="button" class="close mr-2 ml-2" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror


                <div  wire:loading wire:target="changeFilterState">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="changeURLState">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="changeRestaurantState">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="changeAnythingState">
                    <x-control-panel.loading />
                </div>

                    <div class="d-flex flex-row justify-content-between mr-5 ml-5 " style="gap: 10px">
                        <div style="display: inline-block"><label>{{__("city.Filters")}} </label></div>
                        <div class="custom-control custom-switch">

                            <input type="radio" name="content" class="custom-control-input content-filter" wire:change="changeFilterState({{$j}})"   id="content{{$j}}"/>
                            <label  style="display: inline-block" class="custom-control-label" for="content{{$j}}"></label>
                        </div>
                    </div>
                    <div class="d-flex flex-row justify-content-between mr-5 ml-5 " style="gap: 10px">
                        <div style="display: inline-block"><label>{{__("city.URL")}} </label></div>
                        <div class="custom-control custom-switch">

                            <input type="radio" name="content"  class="custom-control-input content-url"  wire:change="changeURLState({{$j}})"  id="URL{{$j}}"/>
                            <label  style="display: inline-block" class="custom-control-label" for="URL{{$j}}"></label>
                        </div>
                    </div>

{{--                    <div class="d-flex flex-row justify-content-between mr-5 ml-5 " style="gap: 10px">--}}
{{--                        <div style="display: inline-block"><label>{{__("city.RestaurantsShape")}} </label></div>--}}
{{--                        <div class="custom-control custom-switch">--}}

{{--                            <input type="radio" name="content"  class="custom-control-input content-url"  wire:change="changeRestaurantState({{$j}})"  id="res{{$j}}"/>--}}
{{--                            <label  style="display: inline-block" class="custom-control-label" for="res{{$j}}"></label>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                <div class="d-flex flex-row justify-content-between mr-5 ml-5 " style="gap: 10px">
                    <div style="display: inline-block"><label>{{__("city.Anything")}} </label></div>
                    <div class="custom-control custom-switch">

                        <input type="radio" name="content"  class="custom-control-input content-url"  wire:change="changeAnythingState({{$j}})"  id="any{{$j}}"/>
                        <label  style="display: inline-block" class="custom-control-label" for="any{{$j}}"></label>
                    </div>
                </div>

                    <hr style="background: #f96332; "/>
<!--                --><?php //$urlShow="urlShow" $j ;
//                dd($urlShow);
//                $this->{"urlShow".$j}
//                ?>
                <div @if($this->{"urlShow".$j}==false) style="display: none" @endif>
                    <form wire:submit.prevent="submitURL({{$j}})">
                        <div class="form-group d-flex align-items-center flex-column">
                            <label> {{__("city.ShapeURL")}}</label>
                            <input type="text" class="form-control"  wire:model.lazy="url{{$j}}" placeholder="{{__("city.EnterShapeURL")}}">


                        </div>
                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="submit" class="btn btn-outline-primary " value="{{__("masterControl.Submit")}}"  >
                        </div>
                    </form>
                </div>

                <div @if($this->{"anyThingShow".$j}==false) style="display: none" @endif>
{{--                    <form wire:submit.prevent="submitAnything({{$j}})">--}}
                        <div class="form-group d-flex align-items-center flex-column">

                            <div class="alert alert-info">
                                {{__("city.anythingNot")}}
                            </div>
                        </div>
                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="button" wire:click="submitAnything({{$j}})" class="btn btn-outline-primary " value="{{__("masterControl.Submit")}}"  >
                        </div>
{{--                    </form>--}}
                </div>

                <div @if($this->{"restaurantShow".$j}==false ) style="display: none" @endif>
                    <form wire:submit.prevent="submitRestaurant({{$j}})">
                        <div class="form-group d-flex align-items-center flex-column">
                            <label> {{__("city.selectRestaurant")}}</label>
                            @foreach($allRestaurants as $restaurant)
                                <div class="col-lg-6 d-flex flex-row justify-content-between" style="gap: 5px ; margin-bottom: 1rem">
                                    <label>{{$restaurant['name']}} </label>
                                    <div  class="custom-control custom-switch  ">

                                        <input type="checkbox" wire:model.defer="restaurants{{$j}}" value="{{$restaurant['serviceProviderID']}}" class="custom-control-input"  id="customSwitchRes1{{$j}} {{$restaurant['serviceProviderID']}}" checked=" " >
                                        <label class="custom-control-label" for="customSwitchRes1{{$j}} {{$restaurant['serviceProviderID']}}"></label>

                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="submit" class="btn btn-outline-primary " value="{{__("masterControl.Submit")}}"  >
                        </div>
                    </form>
                </div>

                <div @if($this->{"contentShow".$j}==false ) style="display: none" @endif>
                    <form wire:submit.prevent="submitContent({{$j}})">
                        <div class="form-group d-flex align-items-center flex-column">
                            <label for="exampleFormControlSelect2"> {{__("city.selectOneFilter")}}</label>

                            @foreach($allFilters as $filter)
                                <div class="col-lg-6 d-flex flex-row justify-content-between" style="gap: 5px ; margin-bottom: 1rem">
                                    <label>{{$filter->name}} </label>
                                    <div  class="custom-control custom-switch  ">

                                        <input type="checkbox" wire:model.defer="content{{$j}}" value="{{$filter->id}}" class="custom-control-input"  id="customSwitch1{{$j}} {{$filter->id}}" checked=" " >
                                        <label class="custom-control-label" for="customSwitch1{{$j}} {{$filter->id}}"></label>

                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                        </div>
                    </form>
                </div>



            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
