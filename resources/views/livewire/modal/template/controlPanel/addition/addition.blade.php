<div class="preview-window"
     x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openAdditionToast"
    x-cloak>
    <div class="header">
        <div class="address">
            {{__('RestaurantControlPanel.AddAddition')}}

        </div>
        <button class="btn btn-close-delivaz" @click.prevent="$wire.closeAdditionToast()">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="content">
        @foreach($addedAddition as $i)
            <div class="addition-items @if(array_key_last($addedAddition)!=$i) d-none @endif">
                <div class="food-info">
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.AdditionName')}}</label>
                        <input class="form-control " wire:model.lazy="additionName.{{$i}}"/>
                        @error('additionName.'.$i)
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="user-input">
                        <label>{{__('RestaurantControlPanel.AdditionDetails')}}</label>
                        <textarea class="form-control" id="exampleFormControlTextarea{{$i}}"
                                  wire:model.lazy="additionDetail.{{$i}}"
                                  rows="2">
                                                    @if(isset($additionDetail[$i]))
                                {{ $additionDetail[$i]}}
                            @endif
                                                </textarea>
                        {{--                                        <input class="form-control " wire:model.lazy="additionDetail.{{$i}}"/>--}}
                        @error('additionDetail')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="food-info">
                    <div class="user-input">
                        <label>
                            {{__('RestaurantControlPanel.AdditionWeight')}}
                            <small>
                                {{__('RestaurantControlPanel.gram')}}
                            </small>
                        </label>
                        <input class="form-control " type="number" wire:model.lazy="additionWeight.{{$i}}"/>
                        @error('additionWeight')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="user-input">
                        <label>
                            {{__('RestaurantControlPanel.AdditionPrice')}}<br/>
{{--                            <small> {{__('RestaurantControlPanel.AdditionPriceNote')}}</small>--}}
                        </label>
                        <input class="form-control " type="number" step="0.01" wire:model.lazy="additionPrice.{{$i}}"/>
                        @error('additionPrice.'.$i)
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="food-info">
                    <div wire:loading wire:target="additionPhoto.{{$i}}">
                        <x-control-panel.loading/>
                    </div>
                    <div class="user-input d-flex justify-content-start">

                        <div class="input-file-container">

                            @if(isset($additionPhoto[$i]))
                                <div class="img">
                                    <img src="{{ $additionPhoto[$i]->temporaryUrl() }}"/>

                                </div>
                            @endif
                            <input type="file" class="form-control banner-input"
                                   wire:model="additionPhoto.{{$i}}"/>
                            <div
                                class="input-handel  @if(isset($additionPhoto[$i])) back-ground-transparent @endif">
                                <div class="items">
                                    <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                            fill="#FE2E17"/>
                                    </svg>

                                    <p class="text1">
                                        {{__('RestaurantControlPanel.DragFoodAdditionPhoto')}}

                                    </p>
                                </div>

                            </div>
                        </div>
                        @error('additionPhoto')
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                        @error('additionPhoto.'.$i)
                        <div class="alert alert-danger2">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>
                    <div class="user-input">
                        <div class="btn-container">
                            <button type="button" class="btn btn-save-delivaz  ml-1 mr-1" @click.prevent="$wire.saveAddition()">
                                {{__("RestaurantControlPanel.SaveChanges")}}
                            </button>
                            <button type="button" class="btn btn-cancle2-delivaz  ml-1 mr-1" @click.prevent="$wire.closeAdditionToast()">
                                {{__("RestaurantControlPanel.Cancel")}}
                            </button>
                        </div>
                    </div>

{{--                    <div class="user-input">--}}
{{--                        <div class="btn-container">--}}

{{--                            @if($i==array_key_last($addedAddition))--}}
{{--                                <button class="btn btn-edit-delivaz"--}}
{{--                                        wire:click.prevent="addAddition({{$i}})">--}}
{{--                                    {{__('RestaurantControlPanel.AddAddition')}}&nbsp;{{$i+1}}--}}
{{--                                </button>--}}
{{--                                <button class="btn btn-cancle2-delivaz"--}}
{{--                                        wire:click.prevent="removeAddition({{$i}})">--}}
{{--                                    {{__('RestaurantControlPanel.Cancel')}}--}}
{{--                                </button>--}}
{{--                            @else--}}
{{--                                <button class="btn btn-cancle2-delivaz"--}}
{{--                                        wire:click.prevent="removeAddition({{$i}})">--}}
{{--                                    {{__('RestaurantControlPanel.Cancel')}}--}}
{{--                                </button>--}}
{{--                            @endif--}}
{{--                        </div>--}}


{{--                    </div>--}}
                </div>

            </div>
        @endforeach

            <div wire:loading wire:target="saveAddition">
                <x-control-panel.loading/>
            </div>
            <div wire:loading wire:target="closeAdditionToast">
                <x-control-panel.loading/>
            </div>


    </div>
{{--    <div class="footer d-flex flex-row justify-content-center">--}}
{{--        <button type="button" class="btn btn-save-delivaz  ml-1 mr-1" @click.prevent="$wire.closeAdditionToast()">--}}
{{--            {{__("RestaurantControlPanel.SaveChanges")}}--}}
{{--        </button>--}}
{{--        <button type="button" class="btn btn-cancle2-delivaz  ml-1 mr-1" @click.prevent="$wire.closeAdditionToast()">--}}
{{--            {{__("RestaurantControlPanel.Cancel")}}--}}
{{--        </button>--}}
{{--    </div>--}}
</div>
