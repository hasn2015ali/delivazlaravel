<div class="preview-window"
     x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openNewSize"
     x-cloak>
    <div class="header">
        <div class="address">
            @if($newSizeUpdate==null)
            {{__('RestaurantControlPanel.AddNewSize')}}
            @else
                {{__('RestaurantControlPanel.UpdateSize')}}
            @endif
        </div>
        <button class="btn btn-close-delivaz" @click.prevent="$wire.cancelSize()">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="content">
        <div class="sizes ">
            <div class="d-flex justify-content-between width-full">
                <div class="user-input">

                    <label>{{__('RestaurantControlPanel.NewSize')}}&nbsp;</label>
                    <div class="dropdown ">
                        <button type="button" class="btn dropdown-toggle"
                                data-toggle="dropdown">
                            @if(isset($newSizeName))
                                {{$newSizeName}}
                            @else
                                {{__('RestaurantControlPanel.SelectSize')}}
                            @endif
                        </button>

                        <div class="dropdown-menu">
                            @foreach($sizes as $key=> $size)
                                {{--                                {{dd($size)}}--}}
                                <a href="#"
                                   wire:click.prevent="setSelectedSize({{json_encode($size)}},{{2}})">
                                    <p class="dropdown-item">
                                        <svg width="24" height="24" viewBox="0 0 24 24"
                                             fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M22 11.0799V11.9999C21.9988 14.1563 21.3005 16.2545 20.0093 17.9817C18.7182 19.7088 16.9033 20.9723 14.8354 21.5838C12.7674 22.1952 10.5573 22.1218 8.53447 21.3744C6.51168 20.6271 4.78465 19.246 3.61096 17.4369C2.43727 15.6279 1.87979 13.4879 2.02168 11.3362C2.16356 9.18443 2.99721 7.13619 4.39828 5.49694C5.79935 3.85768 7.69279 2.71525 9.79619 2.24001C11.8996 1.76477 14.1003 1.9822 16.07 2.85986"
                                                stroke="#209F84" stroke-width="2"
                                                stroke-linecap="round"
                                                stroke-linejoin="round"/>
                                            <path d="M22 4L12 14.01L9 11.01"
                                                  stroke="#209F84"
                                                  stroke-width="2"
                                                  stroke-linecap="round"
                                                  stroke-linejoin="round"/>
                                        </svg>
                                        {{$size['name']}}
                                    </p>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    @error('newSizeName')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;
                        </button>
                        {{ $message }}</div>
                    @enderror


                </div>

                <div class="user-input">

                    <label>
                        {{__('RestaurantControlPanel.FoodSizePrice')}}
                    </label>
                    <input class="form-control " type="number" step="0.01"
                           wire:model.defer="newSizePrice"/>
                    @error('newSizePrice')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;
                        </button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="d-flex justify-content-between width-full align-items-end mt-2">
                <div class="user-input">
                    <label>
                        {{__('RestaurantControlPanel.SizeWeight')}}&nbsp;
                        <small>
                            {{__('RestaurantControlPanel.gram')}}
                        </small>
                    </label>
                    <input class="form-control " type="number" step="0.01"
                           wire:model.defer="newSizeWeight"/>
                    @error('newSizeWeight')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;
                        </button>
                        {{ $message }}</div>
                    @enderror
                </div>
                <div class="user-input">
                    <label>
                        {{__('RestaurantControlPanel.SizeDetails')}}
                    </label>
                    <textarea class="form-control" id="exampleFormControlTextarea"
                              wire:model.lazy="newSizeContent"
                              rows="2">
                    </textarea>
                    @error('newSizeContent')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;
                        </button>
                        {{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="btn-container">
                <button class="btn btn-save-delivaz"
                        wire:click.prevent="saveSize()">
                    {{__('RestaurantControlPanel.SaveChanges')}}
                </button>
                <button class="btn btn-cancle2-delivaz"
                        wire:click.prevent="cancelSize()">
                    {{__('RestaurantControlPanel.Cancel')}}
                </button>
            </div>
        </div>
        <div wire:loading wire:target="saveSize">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="cancelSize">
            <x-control-panel.loading/>
        </div>
    </div>
</div>
