<div class="preview-window"
     x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openNewPhoto"
     x-cloak>
    <div class="header">
        <div class="address">
            {{__('RestaurantControlPanel.AddNewAdditionalPhoto')}}
        </div>
        <button class="btn btn-close-delivaz" @click.prevent="$wire.closeAdditionPhotoToast()">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="content">

        <div wire:loading wire:target="newAdditionalPhoto">
            <x-control-panel.loading/>
        </div>
        <div class="d-flex flex-column user-input2 align-items-end">
            <div class="user-input d-flex justify-content-center width-full">
                <div class="input-file-container">
                    {{--                                <img src="{{asset('/images/avatars/')}}"/>--}}
                    @if($newAdditionalPhoto)
                        <div class="img">
                            <img src="{{ $newAdditionalPhoto->temporaryUrl() }}"/>
                        </div>
                    @endif
                    <input type="file" class="form-control photo-input"
                           wire:model.defer="newAdditionalPhoto"/>

                    <div class="input-handel  @if($newAdditionalPhoto ) back-ground-transparent @endif">
                        <div class="items">
                            <svg width="86" height="56" viewBox="0 0 86 56" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M69.3375 21.3007C66.9008 9.51323 56.0433 0.664062 43 0.664062C32.6442 0.664062 23.65 6.2674 19.1708 14.4674C8.385 15.5607 0 24.2732 0 34.8307C0 46.1399 9.63917 55.3307 21.5 55.3307H68.0833C77.9733 55.3307 86 47.6774 86 38.2474C86 29.2274 78.6542 21.9157 69.3375 21.3007ZM50.1667 31.4141V45.0807H35.8333V31.4141H25.0833L41.7458 15.5266C42.4625 14.8432 43.5733 14.8432 44.29 15.5266L60.9167 31.4141H50.1667Z"
                                    fill="#FE2E17"/>
                            </svg>

                            {{--                                        <i class="fas fa-cloud-upload-alt"></i>--}}
                            <p class="text1">{{__('RestaurantControlPanel.DragFoodPhoto')}}
                            </p>
                        </div>

                    </div>
                </div>

            </div>

            @error('newAdditionalPhoto')
            <div class="alert alert-danger2 w-50">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                {{ $message }}
            </div>
            @enderror

        </div>
        <div wire:loading wire:target="saveBasicInfo">
            <x-control-panel.loading/>
        </div>
        <div wire:loading wire:target="closeAdditionPhotoToast">
            <x-control-panel.loading/>
        </div>
        <div class="btn-container ">
            <button class="btn btn-save-delivaz"
                    wire:click.prevent="saveNewAdditionalPhoto()">
                {{__('RestaurantControlPanel.SaveChanges')}}
            </button>
            <button class="btn btn-cancle2-delivaz"
                    wire:click.prevent="closeAdditionPhotoToast()">
                {{__('RestaurantControlPanel.Cancel')}}
            </button>
        </div>
    </div>

</div>
