<div wire:ignore.self class="modal fade delete-section " id="delCountry2">
    <div class="modal-dialog modal-md">
        <div class="header header-delete d-flex flex-row justify-content-between ">
            <div class="mt-1">
                <h4 class="toast-address">{{__("RestaurantControlPanel.deleteSure")}}</h4>
            </div>
            <div>
                <button class="btn btn-close" data-dismiss="modal"><i class="fas fa-times "></i></button>
            </div>
        </div>
        <div class="modal-content">

                <div class="d-flex flex-column ">
                    <div wire:loading wire:target="deleteAction">
                        <x-control-panel.loading/>
                    </div>
                    <div wire:loading wire:target="deleteCall">
                        <x-control-panel.loading/>
                    </div>
                </div>
                <div class="content">
                    <p> {{__("RestaurantControlPanel.Sure_To_Delete")}}</p>
                </div>

                <div class="d-flex flex-row justify-content-between btn-container">
                    <button class="btn btn-delete-delivaz ml-3 mr-3" wire:click="deleteAction">
                        {{__("RestaurantControlPanel.Delete")}}
                    </button>
                    {{--                    <div align="center" class="modal-footer d-flex align-items-center flex-column">--}}
                    <button type="button" class="btn btn-cancle-delivaz  ml-3 mr-3" data-dismiss="modal">
                        {{__("RestaurantControlPanel.Cancel")}}
                    </button>
                    {{--                    </div>--}}

                </div>

        </div>
    </div>
</div>

