<div class="preview-window-md"
     x-show.transition.in.duration.700ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openEditToast"
     @click.away="$wire.closeEditToast()" x-cloak>
    <div class="header">
        <div class="address">
            {{__('RestaurantControlPanel.EditSize')}}
        </div>
        <button class="btn btn-close-delivaz" @click="$wire.closeEditToast()">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="content">
        <form wire:submit.prevent="updateSize">
            <div class="info w-70">
                <div class="user-input">
                    <label>{{__('RestaurantControlPanel.Size')}}</label>
                    <input class="form-control " wire:model.defer="sizeUP" />
                    @error('sizeUP')
                    <div class="alert alert-danger2">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        {{ $message }}</div>
                    @enderror
                </div>

                <div class="user-input mt-2">
                    <button type="submit" class="btn btn-save-delivaz">{{__('RestaurantControlPanel.SaveChanges')}}</button>
                </div>

            </div>
        </form>

    </div>
    <div class="footer d-flex flex-row justify-content-center">
        <button type="button" class="btn btn-cancle2-delivaz  ml-1 mr-1" @click="$wire.closeEditToast()">
            {{__("RestaurantControlPanel.Cancel")}}
        </button>
    </div>
</div>
