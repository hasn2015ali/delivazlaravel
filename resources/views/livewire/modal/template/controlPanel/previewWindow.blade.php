<div class="preview-window"
     x-show.transition.in.duration.400ms.opacity.out.duration.10ms.origin.bottom.left.scale.95="openPreviewToast"
     @click.away="$wire.closePreviewToast()" x-cloak>
    <div class="header">
        <div class="address">
            @if(isset($documentInPreviewWindow))
                {{$documentInPreviewWindow}}  {{$documentNumber}}
            @endif
            @if(isset($bannerInPreviewWindow))
                {{$bannerInPreviewWindow}} {{$bannerNumber}}
            @endif
        </div>
        <button class="btn btn-close-delivaz" @click="$wire.closePreviewToast()">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="content">
        <div class="photo">
            @if(isset($docImage))
                <img src=" {{asset($imageFolders.$docImage)}}"/>
            @endif
            @if(isset($bannerImage))
                <img src=" {{asset($imageFolders.$bannerImage)}}"/>
            @endif
        </div>
    </div>
    <div class="footer d-flex flex-row justify-content-center">
        <button type="button" class="btn btn-cancle2-delivaz  ml-1 mr-1" @click="$wire.closePreviewToast()">
            {{__("RestaurantControlPanel.Cancel")}}
        </button>
    </div>
</div>
