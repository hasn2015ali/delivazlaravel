<div wire:ignore.self class="modal fade" id="translateCountry">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.Add_trans")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('trans') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                @error('selectedLang') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <form wire:submit.prevent="submit">
                    <div class="form-group d-flex align-items-center flex-column">
                        <label> {{__("city.translate")}}</label>
                        <input type="text" class="form-control"  wire:model.lazy="trans" placeholder="{{__("city.EnterTrans")}}">
{{--                        @error('trans') <span class="alert alert-danger">{{ $message }}</span> @enderror--}}
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">
                        <label for="exampleFormControlSelect1">{{__("city.langSelecte")}}</label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.lazy="selectedLang">
                            <option>---</option>
                            @foreach($langs as $lang)
                                <option value="{{$lang->id}}">{{$lang->name}}</option>
                            @endforeach
                        </select>
{{--                        @error('selectedLang') <span class="alert alert-danger">{{ $message }}</span> @enderror--}}
                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
