<div wire:ignore.self class="modal fade" id="addRecommendedVendor">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.addRecommendedVendor")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submitVendor">
                    <div class="row">
                        <div class="col-lg-12  handle-input">
                            <div  wire:loading wire:target="photoVendor">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($photoVendor)
                                    <img class="food-img-loaded " src="  {{ $photoVendor->temporaryUrl() }}">
                                @endif

                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="photoVendor" >
                            </label>
                            @error('photoVendor') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group ">
                        <label> {{__("city.vendorContent")}}</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="contentVendor" rows="3"></textarea>

                    </div>
                    <div class="form-group ">
                        <label for="exampleFormControlSelect1">{{__("city.vendorSelect")}}</label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="recommendedVendor">
                            <option>---</option>
                            @foreach($vendors as $vendor)
                                <option value="{{$vendor['serviceProviderID']}}">{{$vendor['name']}}</option>
                            @endforeach
                        </select>
                        @error('recommendedVendor') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror

                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
