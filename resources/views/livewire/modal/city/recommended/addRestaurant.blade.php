<div wire:ignore.self class="modal fade" id="addRecommendedRestaurant">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.addRecommendedRestaurant")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submit">
                    <div class="row">
                        <div class="col-lg-12  handle-input">
                            <div  wire:loading wire:target="photo">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($photo)
                                    <img class="food-img-loaded " src="  {{ $photo->temporaryUrl() }}">
                                @endif

                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="photo" >
                            </label>
                            @error('photo') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group ">
                        <label> {{__("city.restaurantContent")}}</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" wire:model.defer="content" rows="3"></textarea>

                    </div>
                    <div class="form-group ">
                        <label for="exampleFormControlSelect1">{{__("city.selectRecommendedRestaurant")}}</label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="recommendedRestaurant">
                            <option>---</option>
                            @foreach($restaurants as $restaurant)
                                <option value="{{$restaurant['serviceProviderID']}}">{{$restaurant['name']}}</option>
                            @endforeach
                        </select>
                        @error('recommendedRestaurant') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror

                    </div>
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
