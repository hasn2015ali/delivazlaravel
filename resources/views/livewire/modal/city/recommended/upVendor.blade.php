<div wire:ignore.self class="modal fade" id="editRecommendedVendor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.upRecommendedVendor")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("city.vendorUp")}}
                            </th>
                            <th>
                                {{__("city.vendorPhoto")}}
                            </th>
                            <th>
                                {{__("city.Action")}}
                            </th>
                            </thead>
                            <tbody>
                            @foreach($recommendedVendors as $res)
                                {{--                                @foreach($res->translation->where('code_lang','en') as $trans)--}}
                                <tr>
                                    <td>
                                        {{$res->serviceProvider->name}}
                                    </td>
                                    <td>
                                        <img class="recommended-photo" src="{{asset('storage/images/sliders/recommended/'.$res->photo)}}">
                                    </td>


                                    <td>
                                        <button wire:click="callDeleteVendor({{$res->id}})" type="button" class="btn btn-outline-danger p-2 m-1">
                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>


                                </tr>
                                {{--                                @endforeach--}}
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
