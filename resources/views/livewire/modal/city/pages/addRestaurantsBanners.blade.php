<div wire:ignore.self class="modal fade" id="addRestaurantsBanners">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">
                    @if($providerType=="restaurant")
                    {{__("city.addRestaurantsPageBanners")}}
                    @elseif($providerType=="shop")
                        {{__("city.addShopsPageBanners")}}
                    @endif
                </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submitRestaurantsBanners">
                    <div class="row">
                        <div class="col-lg-12 ">
                            @if ($restaurantsBanner)
                                <img class=" " src="  {{ $restaurantsBanner->temporaryUrl() }}">
                            @endif
                        </div>

                        <div class="col-lg-12  handle-input">
                            <div  wire:loading wire:target="restaurantsBanner">
                                <x-control-panel.loading />
                            </div>

                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">


                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="restaurantsBanner" >
                            </label>
                            @error('restaurantsBanner') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>

                    <div style="@if(!$showSelectProvider) display: none @endif" class="form-group handel-select">
                        <label for="exampleFormControlSelect1">
                            @if($providerType=="restaurant")
                                {{__("city.selectRestaurantForBanner")}}
                            @elseif($providerType=="shop")
                                {{__("city.selectShopForBanner")}}
                            @endif

                        </label>
                        <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="restaurantID">
                            <option>---</option>
                            @foreach($serviceProviders as $serviceProvider)
                                <option value="{{$serviceProvider['serviceProviderID']}}">{{$serviceProvider['name']}}</option>
                            @endforeach
                        </select>
                        @error('restaurantID')
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}
                        </div>
                        @enderror

                    </div>
{{--                    @endif--}}
                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
