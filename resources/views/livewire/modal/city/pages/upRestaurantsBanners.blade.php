<div wire:ignore.self class="modal fade" id="editRestaurantsBanners">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.upRestaurantsBanners")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                <div class="card-body">
                    <div  wire:loading wire:target="changeRestaurantsBannerState">
                        <x-control-panel.loading />
                    </div>
                    <div  wire:loading wire:target="getRestaurantBanner">
                        <x-control-panel.loading />
                    </div>
                    <div  wire:loading wire:target="getShopsBanner">
                        <x-control-panel.loading />
                    </div>
                    <div class="table-responsive">
                        <table id="myTable" class="table">
                            <thead class=" text-primary">
                            <th>
                                {{__("city.restaurantPhoto")}}
                            </th>
                            <th>
                                {{__("city.state")}}
                            </th>
                            <th>
                                {{__("city.Action")}}
                            </th>
                            </thead>
                            <tbody>
                            @if($restaurantsBanners)
                           @forelse($restaurantsBanners as $banner)
                                <tr>
                                    <td>
                                        @if($type=="shop")
                                        <img class="restaurant-photo" src="{{asset('storage/images/sliders/city/shops/'.$banner->name)}}">

                                        @elseif($type=="restaurant")
                                            <img class="restaurant-photo" src="{{asset('storage/images/sliders/city/restaurants/'.$banner->name)}}">
                                        @endif
                                    </td>

                                    <td>
                                        <div class="form-group ">
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox"  class="custom-control-input" wire:change="changeRestaurantsBannerState({{$banner->id}})" id="customSwitchBa25h{{$banner->id}}"  @if($banner->state==1) checked=" " @endif>
                                                <label class="custom-control-label" for="customSwitchBa25h{{$banner->id}}"> </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <button wire:click="deleteRestaurantsBanner({{$banner->id}})" type="button" class="btn btn-outline-danger p-2 m-1">
                                            <i class="fa fa-trash fa-2x" aria-hidden="true"></i>
                                        </button>
                                    </td>


                                </tr>

                            @empty

                            @endforelse
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
