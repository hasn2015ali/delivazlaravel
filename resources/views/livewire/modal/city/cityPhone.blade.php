<div wire:ignore.self class="modal fade" id="CityPhone">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("city.Add_City_Phone")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">
                @error('phone1') <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{ $message }}</div>
                @enderror
                <div  wire:loading wire:target="submitPhone">
                    <x-control-panel.loading />
                </div>
                <div  wire:loading wire:target="editPhone">
                    <x-control-panel.loading />
                </div>
                <form wire:submit.prevent="submitPhone">
                    <div class=" d-flex flex-column align-items-start">
                        <label> {{__("city.Phone1")}}</label>

                        <div style="width: 100%" class="d-flex flex-row justify-content-between">
                            <div style="width: 48%" class="form-group">
                                <input type="text" class="form-control"  wire:model.defer="phone1" placeholder="{{__("city.EnterPhoneNumber")}}">

                            </div>
                            <div style="width: 48%" class="handle-input">
                                <div  wire:loading wire:target="icon1">
                                    <x-control-panel.loading />
                                </div>
                                {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                                <label class="d-flex flex-row">
                                    @if ($icon1)
                                        <img class="food-img-loaded " src="  {{ $icon1->temporaryUrl() }}">
                                    @elseif($oldIcon1)
                                        <img class="food-img-loaded " src="{{asset('storage/images/phone/city/'.$oldIcon1)}}">
                                    @endif

                                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="icon1" >
                                </label>
                                @error('icon1') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class=" d-flex align-items-start flex-column">
                        <label> {{__("city.Phone2")}}</label>
                        <div style="width: 100%" class="d-flex flex-row justify-content-between">
                            <div style="width: 48%" class="form-group">
                                <input type="text" class="form-control"  wire:model.defer="phone2" placeholder="{{__("city.EnterPhoneNumber")}}">

                            </div>
                            <div style="width: 48%" class="handle-input">
                                <div  wire:loading wire:target="icon2">
                                    <x-control-panel.loading />
                                </div>
                                {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                                <label class="d-flex flex-row">
                                    @if ($icon2)
                                        <img class="food-img-loaded " src="{{ $icon2->temporaryUrl() }}">
                                    @elseif($oldIcon2)
                                        <img class="food-img-loaded " src="{{asset('storage/images/phone/city/'.$oldIcon2)}}">
                                    @endif

                                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="icon2" >
                                </label>
                                @error('icon2') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-start flex-column">
                        <label> {{__("city.Phone3")}}</label>
                        <div style="width: 100%" class="d-flex flex-row justify-content-between">
                            <div style="width: 48%" class="form-group">
                                <input type="text" class="form-control"  wire:model.defer="phone3" placeholder="{{__("city.EnterPhoneNumber")}}">

                            </div>
                            <div style="width: 48%" class="handle-input">
                                <div  wire:loading wire:target="icon3">
                                    <x-control-panel.loading />
                                </div>
                                {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                                <label class="d-flex flex-row">
                                    @if ($icon3)
                                        <img class="food-img-loaded " src="  {{ $icon3->temporaryUrl() }}">
                                    @elseif($oldIcon3)
                                        <img class="food-img-loaded " src="{{asset('storage/images/phone/city/'.$oldIcon3)}}">
                                    @endif

                                    <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="icon3" >
                                </label>
                                @error('icon3') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
