<div wire:ignore.self class="modal fade" id="addVendor">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("vendor.AddVendor")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submit">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("vendor.name")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="name" placeholder="{{__("vendor.EnterName")}}">
                                @error('name') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("vendor.nameEN")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="nameEN" placeholder="{{__("vendor.EnterNameEN")}}">
                                @error('nameEN') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 handle-input">
                            <div wire:loading wire:target="vendorImage">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($vendorImage)

                                    <img class="img-loaded " src="{{ $vendorImage->temporaryUrl() }}">
                                @else

                                    <div class="flex-grow-1 mt-3">
                                        {{__("vendor.Add_vendorImage")}}
                                        <i class="fa fa-cloud-upload"></i>
                                    </div>
                                @endif
                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="vendorImage" >
                            </label>

                            @error('vendorImage') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>{{__("vendor.Address")}}</label>
                                <input type="text" class="form-control" placeholder="{{__("vendor.enter_address")}}"wire:model.defer="address" >
                            </div>
                        </div>
                    </div>
                    <div class="owner-r-v">

                        <div class="alert alert-warning d-flex flex-row justify-content-center">
                            <strong> {{__("vendor.owner")}}</strong>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label> {{__("vendor.firstName")}}</label>
                                    <input type="text" class="form-control"  wire:model.defer="firstName" placeholder="{{__("vendor.EnterFirstName")}}">
                                    @error('firstName') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label> {{__("vendor.fatherName")}}</label>
                                    <input type="text" class="form-control"  wire:model.defer="fatherName" placeholder="{{__("vendor.EnterFatherName")}}">
                                    @error('fatherName') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label> {{__("vendor.lastName")}}</label>
                                    <input type="text" class="form-control"  wire:model.defer="lastName" placeholder="{{__("vendor.EnterLastName")}}">
                                    @error('lastName') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-8">
                                <div class="form-group">
                                    <label  for="exampleInputEmail1"> {{__("vendor.Email_Address")}}</label>
                                    <input type="email"  class="form-control" placeholder="{{__("vendor.Enter_Email_Address")}}" wire:model.defer="email">
                                    @error('email') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>{{__("vendor.Password")}}</label>
                                    <input type="password" class="form-control" placeholder="{{__("vendor.enterPassword")}}"wire:model.defer="password" >
                                    @error('password') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label  for="exampleInputEmail1"> {{__("vendor.MinimumOrderAmount")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("vendor.EnterMinimumOrderAmount")}}" wire:model.defer="minimumOrderAmount">
                                @error('minimumOrderAmount') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("vendor.DeliveryMode")}}</label>
                                <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="deliveryMode">
                                    <option>---</option>
                                    <option value="delivery">Delivery</option>
                                    <option value="self">Self Pick-Up</option>
                                    <option value="both">Both</option>
                                </select>
                                @error('deliveryMode') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label  for="exampleInputEmail1"> {{__("vendor.DeliveryManCommission")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("vendor.EnterDeliveryManCommission")}}" wire:model.defer="deliveryManCommission">
                                @error('deliveryManCommission') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>{{__("vendor.PickCommission")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("vendor.EnterPickCommission")}}" wire:model.defer="pickCommission">

                                @error('PickCommission') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 ">

                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("vendor.Phone")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("vendor.Enter_Phone")}}" wire:model.defer="phone1">
                                @error('phone1') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("vendor.Phone2")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("vendor.Enter_Phone2")}}" wire:model.defer="phone2">
                                @error('phone2') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("vendor.Phone3")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("vendor.Enter_Phone3")}}" wire:model.defer="phone3">
                                @error('phone') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @if ($showSelectCountry)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div wire:loading wire:target="getCities">
                                        <x-control-panel.loading/>
                                    </div>
                                    <label>{{__("vendor.selectCountry")}}</label>
                                    <select class="form-control" id="exampleFormControlSelect2" wire:change="getCities(0,0)" wire:model.defer="selectedCountry">
                                        <option>---</option>
                                        @foreach($countries as $country)
                                            <option  value="{{$country->country_id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('selectedCountry') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__("vendor.selectCity")}}</label>
                                    <select class="form-control" id="exampleFormControlSelect2" wire:change="setCityID"  wire:model.defer="selectedCity">
                                        <option>---</option>
                                        @if($cities)
                                            @foreach($cities as $city)
                                                <option value="{{$city->city_id}}">{{$city->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @error('selectedCity') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("vendor.selectFilter")}}</label>
                                @error('selectedVendorFilters') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row p-3" >
                        @foreach($allVendorFilter as $filter)
                            <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                <label>{{$filter->name}} </label>
                                <div class="custom-control custom-switch ">
                                    <input value="{{$filter->id}}" type="checkbox" class="custom-control-input" wire:model.defer="selectedVendorFilters"  id="customSwitch1{{$filter->id}}"  checked=" ">
                                    <label class="custom-control-label" for="customSwitch1{{$filter->id}}"></label>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
