<div wire:ignore.self class="modal fade" id="editRegistration">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div align="center">  {{__("vendor.CompleteRegistration")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="updateRegistration">
                    <div wire:loading wire:target="updateCall">
                        <x-control-panel.loading/>
                    </div>
                    @if($registeredVendor)

                        <div class="owner-r-v">

                            <div class="alert alert-warning d-flex flex-row justify-content-center">
                                <strong> {{__("vendor.RegisteredRestaurantInformation")}}</strong>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label>
                                            {{__("vendor.FullName")}}:
                                            <strong>
                                                {{$registeredVendor->user->firstName}}
                                                {{$registeredVendor->user->fatherName}}
                                                {{$registeredVendor->user->lastName}}
                                            </strong>

                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <div class="width-20">
                                            <img style="object-fit: cover" class="width-full"
                                                 @if($registeredVendor->avatar)
                                                 src="{{asset('storage/images/avatars/'.$registeredVendor->avatar)}}"
                                                 @else
                                                 src="{{asset('storage/images/avatars/person.png')}}"

                                                @endif>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label>
                                            {{__("vendor.name")}}:
                                            <strong>
                                                {{$registeredVendor->name}}
                                            </strong>
                                            &nbsp; &nbsp; &nbsp; &nbsp;
                                            @if($registeredVendor->nameEN)
                                                {{__("vendor.nameEN")}}:
                                                <strong>
                                                    {{$registeredVendor->nameEN}}
                                                </strong>
                                            @endif

                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label>
                                            {{__("vendor.Address")}}:
                                            <strong>
                                                {{$registeredVendor->address}}
                                            </strong>
                                        </label>

                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            {{__("vendor.Email_Address")}}:
                                            @if($registeredVendor->user->email)
                                            <strong>
                                                {{$registeredVendor->user->email }}
                                            </strong>
                                            @else
                                                <small>
                                                    {{__("vendor.Email_IsNotAdded")}}:
                                                </small>
                                            @endif
                                        </label>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{__("vendor.Phone")}}:
                                            <strong>
                                                {{$registeredVendor->user->phone}}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="owner-r-v">

                        <div class="alert alert-warning d-flex flex-row justify-content-center">
                            <strong> {{__("vendor.AccountActivecation")}}</strong>
                        </div>
                        <div class="row">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> {{__("vendor.MinimumOrderAmount")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("vendor.EnterMinimumOrderAmount")}}"
                                           wire:model.defer="minimumOrderAmount">
                                    @error('minimumOrderAmount')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="form-group">
                                    <label>{{__("vendor.DeliveryMode")}}</label>
                                    <select class="form-control" id="exampleFormControlSelect1"
                                            wire:model.defer="deliveryMode">
                                        <option>---</option>
                                        <option value="delivery">Delivery</option>
                                        <option value="self">Self Pick-Up</option>
                                        <option value="both">Both</option>
                                    </select>
                                    @error('deliveryMode')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> {{__("vendor.DeliveryManCommission")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("vendor.EnterDeliveryManCommission")}}"
                                           wire:model.defer="deliveryManCommission">
                                    @error('deliveryManCommission')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>{{__("vendor.PickCommission")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("vendor.EnterPickCommission")}}"
                                           wire:model.defer="pickCommission">

                                    @error('pickCommission')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label>{{__("vendor.RegistrationState")}} </label>
                                <div class="custom-control custom-switch ">
                                    <input type="checkbox" class="custom-control-input"
                                           wire:model.defer="registrationState" id="customSwitch1">
                                    <label class="custom-control-label" for="customSwitch1"></label>
                                </div>
                                @error('registrationState')
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__("vendor.selectFilter")}}</label>
                                    @error('selectedVendorFilters')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row p-3">
                            @foreach($allVendorFilter as $filter)
                                <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                    <label>{{$filter->name}} </label>
                                    <div class="custom-control custom-switch ">
                                        <input value="{{$filter->id}}" type="checkbox" class="custom-control-input"
                                               wire:model.defer="selectedVendorFilters"
                                               id="customSwitchNew{{$filter->id}}" checked=" ">
                                        <label class="custom-control-label"
                                               for="customSwitchNew{{$filter->id}}"></label>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                            <div wire:loading wire:target="updateRegistration">
                                <x-control-panel.loading/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning"
                        data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
