<div wire:ignore.self class="modal fade" id="editRegistration">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div align="center">  {{__("restaurant.CompleteRegistration")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="updateRegistration">
                    <div  wire:loading wire:target="updateCall">
                        <x-control-panel.loading />
                    </div>
                    @if($registeredRestaurant)

                        <div class="owner-r-v">

                            <div class="alert alert-warning d-flex flex-row justify-content-center">
                                <strong> {{__("restaurant.RegisteredRestaurantInformation")}}</strong>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label>
                                            {{__("restaurant.FullName")}}:
                                            <strong>
                                                {{$registeredRestaurant->user->firstName}}
                                                {{$registeredRestaurant->user->fatherName}}
                                                {{$registeredRestaurant->user->lastName}}
                                            </strong>

                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <div class="width-20">
                                            <img style="object-fit: cover" class="width-full"
                                                 @if($registeredRestaurant->avatar)
                                                 src="{{asset('storage/images/avatars/'.$registeredRestaurant->avatar)}}"
                                                 @else
                                                 src="{{asset('storage/images/avatars/person.png')}}"

                                                @endif>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label>
                                            {{__("restaurant.name")}}:
                                            <strong>
                                                {{$registeredRestaurant->name}}
                                            </strong>
                                            &nbsp;  &nbsp;  &nbsp;  &nbsp;
                                            @if($registeredRestaurant->nameEN)
                                            {{__("restaurant.nameEN")}}:
                                            <strong>
                                                {{$registeredRestaurant->nameEN}}
                                            </strong>
                                            @endif

                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group ">
                                            <label>
                                                {{__("restaurant.Address")}}:
                                                <strong>
                                                    {{$registeredRestaurant->address}}
                                                </strong>
                                            </label>

                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">
                                            {{__("restaurant.Email_Address")}}:
                                            @if($registeredRestaurant->user->email)
                                            <strong>
                                                {{$registeredRestaurant->user->email }}
                                            </strong>
                                            @else
                                                <small>
                                                    {{__("restaurant.Email_IsNotAdded")}}:
                                                </small>
                                            @endif
                                        </label>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            {{__("restaurant.Phone")}}:
                                            <strong>
                                                {{$registeredRestaurant->user->phone}}
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="owner-r-v">

                        <div class="alert alert-warning d-flex flex-row justify-content-center">
                            <strong> {{__("restaurant.AccountActivecation")}}</strong>
                        </div>
                        <div class="row ">
                            <div class="col-md-4 pr-1">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> {{__("restaurant.MinimumOrderAmount")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("restaurant.EnterMinimumOrderAmount")}}"
                                           wire:model.defer="minimumOrderAmount">
                                    @error('minimumOrderAmount')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <div class="form-group">
                                    <label>{{__("restaurant.DeliveryMode")}}</label>
                                    <select class="form-control" id="exampleFormControlSelect1"
                                            wire:model.defer="deliveryMode">
                                        <option>---</option>
                                        <option value="delivery">Delivery</option>
                                        <option value="self">Self Pick-Up</option>
                                        <option value="both">Both</option>
                                    </select>
                                    @error('deliveryMode')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"> {{__("restaurant.DeliveryManCommission")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("restaurant.EnterDeliveryManCommission")}}"
                                           wire:model.defer="deliveryManCommission">
                                    @error('deliveryManCommission')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>{{__("restaurant.PickCommission")}}</label>
                                    <input type="number" class="form-control"
                                           placeholder="{{__("restaurant.EnterPickCommission")}}"
                                           wire:model.defer="pickCommission">

                                    @error('pickCommission')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label>{{__("restaurant.RegistrationState")}} </label>
                                <div class="custom-control custom-switch ">
                                    <input type="checkbox" class="custom-control-input"
                                           wire:model.defer="registrationState" id="customSwitch1">
                                    <label class="custom-control-label" for="customSwitch1"></label>
                                </div>
                                @error('registrationState')
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{__("restaurant.selectFilter")}}</label>
                                    @error('selectedRestaurantFilters')
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row p-3">
                            @foreach($allRestaurantFilter as $filter)
                                <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                                    <label>{{$filter->name}} </label>
                                    <div class="custom-control custom-switch ">
                                        <input value="{{$filter->id}}" type="checkbox" class="custom-control-input"
                                               wire:model.defer="selectedRestaurantFilters"
                                               id="customSwitchNew{{$filter->id}}" checked=" ">
                                        <label class="custom-control-label"
                                               for="customSwitchNew{{$filter->id}}"></label>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <div class="form-group d-flex align-items-center flex-column">

                            <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                            <div wire:loading wire:target="updateRegistration">
                                <x-control-panel.loading/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning"
                        data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
