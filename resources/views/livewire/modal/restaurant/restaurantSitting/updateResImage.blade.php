<div class="modal fade" id="UpRestaurantImage">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">           {{__("restaurant.up_restaurantImage")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <form  >


                    <div class="col-md-11 handle-input">
                        {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                        <label>
                            <i class="fa fa-cloud-upload"></i>
                            <input type="file" class="form-control file-upload" wire:model.lazy="restaurantImage" >
                        </label>
                        @error('restaurantImage') <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ $message }}</div>
                        @enderror
                    </div>

                </form>
                <div class="d-flex flex-row justify-content-center">
                    <button id="up_person_image1" class="btn btn-outline-primary"> {{__("restaurant.Up_Load_image")}}</button>
                </div>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer">
                <button style="margin-right:200px" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
