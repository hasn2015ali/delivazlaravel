<div wire:ignore.self class="modal fade" id="addRestaurant">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header bg-warning">
                <div  align="center">  {{__("restaurant.AddRestaurant")}} </div>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body ">

                <form wire:submit.prevent="submit">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">

                                <label> {{__("restaurant.name")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="name" placeholder="{{__("restaurant.EnterName")}}">
                                @error('name') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("restaurant.nameEN")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="nameEN" placeholder="{{__("restaurant.EnterNameEN")}}">
                                @error('nameEN') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 handle-input">
                            <div wire:loading wire:target="restaurantImage">
                                <x-control-panel.loading />
                            </div>
                            {{--                                <label>{{__("user.Add_Personal_Image")}} </label>--}}
                            <label class="d-flex flex-row">
                                @if ($restaurantImage)

                                    <img class="img-loaded " src="{{ $restaurantImage->temporaryUrl() }}">
                                @else

                                <div class="flex-grow-1 mt-3">
                                    {{__("restaurant.Add_restaurantImage")}}
                                    <i class="fa fa-cloud-upload"></i>
                                </div>
                                @endif
                                <input type="file" class="form-control file-upload photo-input m-3" wire:model.defer="restaurantImage" >
                            </label>

                            @error('restaurantImage') <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ $message }}</div>
                            @enderror
                        </div>
                    </div>
              <div class="row">
                  <div class="col-lg-8">
                      <div class="form-group">
                          <label>{{__("restaurant.Address")}}</label>
                          <input type="text" class="form-control" placeholder="{{__("restaurant.enter_address")}}"wire:model.defer="address" >
                      </div>
                  </div>
              </div>
                <div class="owner-r-v">

                    <div class="alert alert-warning d-flex flex-row justify-content-center">
                                <strong> {{__("restaurant.owner")}}</strong>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("restaurant.firstName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="firstName" placeholder="{{__("restaurant.EnterFirstName")}}">
                                @error('firstName') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("restaurant.fatherName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="fatherName" placeholder="{{__("restaurant.EnterFatherName")}}">
                                @error('fatherName') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group ">
                                <label> {{__("restaurant.lastName")}}</label>
                                <input type="text" class="form-control"  wire:model.defer="lastName" placeholder="{{__("restaurant.EnterLastName")}}">
                                @error('lastName') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                                <label  for="exampleInputEmail1"> {{__("restaurant.Email_Address")}}</label>
                                <input type="email"  class="form-control" placeholder="{{__("restaurant.Enter_Email_Address")}}" wire:model.defer="email">
                                @error('email') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>{{__("restaurant.Password")}}</label>
                                <input type="password" class="form-control" placeholder="{{__("restaurant.enterPassword")}}"wire:model.defer="password" >
                                @error('password') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label  for="exampleInputEmail1"> {{__("restaurant.MinimumOrderAmount")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("restaurant.EnterMinimumOrderAmount")}}" wire:model.defer="minimumOrderAmount">
                                @error('minimumOrderAmount') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("restaurant.DeliveryMode")}}</label>
                                <select class="form-control" id="exampleFormControlSelect1" wire:model.defer="deliveryMode">
                                    <option>---</option>
                                    <option value="delivery">Delivery</option>
                                    <option value="self">Self Pick-Up</option>
                                    <option value="both">Both</option>
                                </select>
                                @error('deliveryMode') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label  for="exampleInputEmail1"> {{__("restaurant.DeliveryManCommission")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("restaurant.EnterDeliveryManCommission")}}" wire:model.defer="deliveryManCommission">
                                @error('deliveryManCommission') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>{{__("restaurant.PickCommission")}}</label>
                                <input type="number"  class="form-control" placeholder="{{__("restaurant.EnterPickCommission")}}" wire:model.defer="pickCommission">

                                @error('pickCommission') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 ">

                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("restaurant.Phone")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone")}}" wire:model.defer="phone1">
                                @error('phone1') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("restaurant.Phone2")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone2")}}" wire:model.defer="phone2">
                                @error('phone2') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label>{{__("restaurant.Phone3")}}</label>
                                <input type="number" class="form-control" placeholder="{{__("restaurant.Enter_Phone3")}}" wire:model.defer="phone3">
                                @error('phone') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @if ($showSelectCountry)
                    <div class="row">
                        <div class="col-md-6">
                            <div wire:loading wire:target="getCities">
                                <x-control-panel.loading/>
                            </div>

                                <div class="form-group">
                                    <label>{{__("restaurant.selectCountry")}}</label>
                                    <select class="form-control" id="exampleFormControlSelect2" wire:change="getCities(0,0)" wire:model.defer="selectedCountry">
                                        <option>---</option>
                                        @foreach($countries as $country)
                                            <option  value="{{$country->country_id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('selectedCountry') <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        {{ $message }}</div>
                                    @enderror
                                </div>


                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
{{--                                <div wire:loading wire:target="getCities">--}}
{{--                                    <x-control-panel.loading/>--}}
{{--                                </div>--}}
                                <label>{{__("restaurant.selectCity")}}</label>
                                <select class="form-control" id="exampleFormControlSelect2"  wire:change="setCityID"  wire:model.defer="selectedCity">
                                    <option>---</option>
                                    @if($cities)
                                        @foreach($cities as $city)

                                            <option value="{{$city->city_id}}">{{$city->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @error('selectedCity') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>{{__("restaurant.selectFilter")}}</label>
                                @error('selectedRestaurantFilters') <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    {{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row p-3" >
                        @foreach($allRestaurantFilter as $filter)
                        <div class="col-lg-3 d-flex flex-row" style="gap: 5px">
                            <label>{{$filter->name}} </label>
                            <div class="custom-control custom-switch ">
                                <input value="{{$filter->id}}" type="checkbox" class="custom-control-input" wire:model.defer="selectedRestaurantFilters"  id="customSwitch1{{$filter->id}}"  checked=" ">
                                <label class="custom-control-label" for="customSwitch1{{$filter->id}}"></label>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div class="form-group d-flex align-items-center flex-column">

                        <input type="submit" class="btn btn-outline-primary" value="{{__("masterControl.Submit")}}">
                    </div>
                </form>
            </div>
            <!-- Modal footer -->
            <div align="center" class="modal-footer d-flex align-items-center flex-column">
                <button style="" type="button" class="btn btn-outline-warning" data-dismiss="modal">{{__("masterControl.Cancel")}}</button>
            </div>
        </div>
    </div>
</div>
