@extends('public.master')

@section('title')

    {{__("home.index")}}
@endsection

@section('content')

    <div class="container">
    <div class="d-flex flex-row  justify-content-center mt-3">
        @if(session()->has('emailMessage'))
            <div style="width: 90%" class="alert alert-info">
                <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                {{ session('emailMessage') }}
                <a class="dropdown-item" href="{{ route('Verify-My-Email',['locale'=>app()->getLocale()]) }}" >
                    <strong>{{__("userFront.verifyMyEmail")}}</strong>
                </a>
            </div>
            {{session()->forget(['emailMessage'])}}
        @endif
            @if(session()->has('mobileMessage'))
                <div style="width: 90%" class="alert alert-info">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    {{ session('mobileMessage') }}
                    <a class="dropdown-item" href="{{ route('Verify-My-Phone',['locale'=>app()->getLocale()]) }}" >
                        <strong> {{__("userFront.PhoneMyVerification")}} </strong>
                    </a>
                </div>
                {{session()->forget(['mobileMessage'])}}
            @endif

            @if(session('verification_status'))
                <div style="width: 90%" class="alert alert-info">
                    <button type="button" class="close mr-3 ml-3" data-dismiss="alert">&times;</button>
                    <strong>
                        {{ session('verification_status') }}
                    </strong>
                </div>
                {{session()->forget(['verification_status'])}}
            @endif
    </div>
    </div>
{{--    {{dd($countries)}}--}}
{{--    old version to choose country and city--}}
{{--        <livewire:template.index.country :countries="$countries"--}}
{{--                                         :checkedCity="$cityTrans"--}}
{{--                                         :country="$country"--}}
{{--                                         :cities="$cities "--}}
{{--                                         :city="$city"--}}
{{--                                         :countrySelected="$country_lang">--}}

            <livewire:template.index.country-component :countries="$countries"
                                             :checkedCity="$cityTrans"
                                             :country="$country"
                                             :cities="$cities "
                                             :city="$city"
                                             :countrySelected="$country_lang">

            <livewire:template.index.shape >
                <livewire:template.mobile >

@endsection
