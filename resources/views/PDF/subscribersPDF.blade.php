<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
</head>

<body>
<table>
    <thead>
    <tr>
        <td><b>All Subscribers In {{$cityName}}</b></td>
    </tr>
    </thead>
    <tbody>
    @foreach($subscribers as $subscriber)
        <tr>
            <td>
                {{$subscriber->email}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>

</html>
