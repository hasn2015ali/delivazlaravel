<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    "validateIdentification"=>"Please Enter Correct Email Or Correct Mobile Phone",
    "validateEmail"=>"This Email has been taken by another user Please try another Email",
    "validatePhone"=>"This Phone has been taken by another user Please try another Phone",
    "nameRequired"=>"Please Enter First Name",
    "passwordRequired"=>"Please Enter Password",
    "password_confirmationRequired"=>"Please Enter Password Confirmation",
    "password_confirmationSame"=>"Password and Password Confirmation Dose not match",
    "password_min"=>"password must be at lest 8 characters",
    "RequiredFirstName"=>"Please Enter Your First Name",
    "RequiredLastName"=>"Please Enter Your Last Name",
    "xxx"=>"xxM",
    "xxx"=>"xxM",


];
