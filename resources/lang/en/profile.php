<?php
return [

    "profile"=>"Profile Page",
    "myprofile"=>"My Profile Details",
    "myprofile1"=>"My Profile",

    "FirstName"=>"First Name: ",
    "LastName"=>"Last Name: ",
    "FatherName"=>"Father Name: ",
    "EmailAddress"=>"Email Address: ",
    "Phone"=>"Phone: ",
    "Address"=>"Address: ",
    "Age"=>"Age: ",
    "Country"=>"Country: ",
    "City"=>"City: ",
    "changePassword"=>"change My Password",
    "oldPassword"=>"Enter Old Password First: ",
    "newPassword"=>"Enter New Password: ",
    "Send"=>"Send",
    "updateSuccessfully"=>"Your Password Has Been Updated Successfully",
    "errorPassword"=>"Please Enter Your Correct Password and Try Again. ",
    "valPassword"=>"Password Is Empty Try Again.",
    "valPassword2"=>"Please Enter Password More Than 7 Characters",
    "errorPassword2"=>"Failed To Update Try Again",
    "WorkHours"=>"My Work Type ",
    "FullTime"=>"Full Time",
    "WorkHours1"=>"My Work Type Is: ",
    "PartTime"=>"Part Time",
    "userDay"=>"Day For Work: ",
    "userPartTime"=>"Hours For Work",
    "from"=>"From",
    "to"=>"To",
    "valPasswordConformation"=>"Password Conformation Is not Same Password ",
    "xxx"=>"xx",
    "xxx"=>"xx",






];
