<?php
return [
    "Countries"=>"Countries",
    "Countries_Menu"=>"Countries Menu",
    "Add_Country"=>"Add New Country",
    "Name"=>"Country Name",
    "EnterName"=>"Enter Country Name",
    "Cities"=>"Cities",
    "Show"=>"Show",
    "Update_Country"=>"Update Country",
    "up"=>"Update",
    "Delete_Country"=>"Delete Country",
    "code"=>"Country Code",
    "EnterCode"=>"Enter Country Code",
    "validate"=>"Please Enter the name of country",
//    "code"=>"code",
    "translate"=>"Translate",
    "translateNOte"=>"You can add the translation according to the site's language, choose the language first, then start the translation and Now you can translate in ",
    "lang"=>"Language",
    "Add_trans"=>"Add New Translate",
    "langSelecte"=>"Select Language",
    "up_trans"=>"Update Translate",
    "validate_selectedLang"=>"Please select Language",
    "up_country"=>"Update Country",
    "translated"=>"Translated in ",
    "countryConfig"=>"Country Setting",
    "CountriesMenu"=>"Countries Menu",
    "currency"=>"Currency",
    "currencyCode"=>"Currency Code",
    "Add_CountryDetails"=>"Add Country Details",
    "EnterCurrencyCode"=>"Enter Currency Code",
    "EnterCurrency"=>"Enter Currency",
    "validateCurrency"=>"Enter Currency",
    "validateCurrencyCode"=>"Enter Currency Code",
    "noInfo"=>"There is No Details For this Country",
    "numberCode"=>"Number",
    "EnterNumberCode"=>"Enter Number",
    "notFound"=>"not Added",
    "phoneNumber"=>"Phone Number",
    "EnterPhoneNumber"=>"Enter Phone Number",
    "countryTaxes"=>"country Taxes",
    "CountryDetails"=>"Country Details",
    "CountryTaxes"=>"Country Taxes",
    "up_country_Taxes"=>"Update Country Taxes",
    "xxx"=>"Submit",
    "xxx"=>"Submit",
    "xxx"=>"Submit",
    "xxx"=>"Submit",
    "xxx"=>"Submit",








];
