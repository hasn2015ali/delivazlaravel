<?php
return [
    "Add_City"=>"إضافة مدينة ",
    "Name"=>"اسم المدينة",
    "up_city"=>"تعديل المدينة ",
    "EnterName"=>"أدخل اسم المدينة",
    "Action"=>"الحدث",

    "validate"=>"الرجاء إدخال اسم المدينة أولا ",
    "cities"=>"المدن",
    "cities_Menu"=>"قائمة المدن",
    "Add_trans"=>"إضافة الترجمة",
    "translate"=>"الترجمة",
    "EnterTrans"=>"أدخل الترجمة",
    "langSelecte"=>"اختر اللغة",
    "up_trans"=>"تعديل الترجمة",
    "lang"=>"اللغة",




];
