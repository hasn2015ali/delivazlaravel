<?php
return [
    "home"=>"الرئيسية",
    "about"=>"حول",
    "sustainability"=>"الاستمرارية",
    "accessibilty"=>"إمكانية الوصول",
    "coustomer_care"=>"رعاية الزبون",
    "FAQ"=>"FAQ",
    "shipping_and_returns"=>"الشحن والإرجاع",
    "store_policy"=>"سياسة المتجر",
    "store_locator"=>"محدد موقع المتاجر",
    "stay_connected"=>"تواصل معنا",
    "all_rights_reserved"=>"جميع الحقوق محفوظة",
    "newsLetter"=>"النشرة الإخبارية",
    "subscribe_and_get_our_lastest_offer"=>"اشترك واحصل على أحدث عروضنا",
    "and_stay_updated"=>"وابقى على اطلاع دائم",
    "type_your_name"=>"اكتب اسمك هنا",
    "subscribe"=>"اشتراك",
    "sell_on_delivaz"=>"البيع عبر Delivaz",
    "Pick_your_odere_from_A_to_Z"=>"اختر طلبك من A إلى Z",
    "get_started"=>"ابدأ",
    "get_your_free_download"=>" احصل على تنزيل مجاني ",
    "of"=>"تطبيقات و اصدارات",
    "app"=>"",
    "get_it_on"=>"احصل عليه من",
    "google_play"=>"متجر غوغل",
    "available_on"=>"وأيضا متاح في",
    "Save_the_page"=>"حفظ الصفحة ",
    "to_desktop"=>"على سطح المكتب",
    "free_download"=>" تنزيل مجاني",
    "type_your_e-mail"=>"اكتب بريدك الالكتروني هنا",
    "sign_in"=>" تسجيل الدخول",
    "join_now"=>"إنشاء حساب",
    "track_order"=>"تتبع الطلب",

    "request_from_a_to_z"=>"اختر طلبك من  A إلى  Z",

    "choose_your_country_and_your_city"=>"اختر بلدك ومدينتك",
    "call_us"=>"اتصل بنا",
    "email_us"=>"أرسل لنا ايميل",
    "address"=>"العنوان",

    "Stores"=>"محلات",
    "delivery_express"=>"التسليم السريع",
    "kaufland"=>"كوفلاند",
    "anything"=>"أي شئ",
    "pharmacy"=>"صيدلية",
    "food"=>"طعام",
    "featuring_food_in_the_city"=>"يتضمن طعام المدينة",
    "food_name"=>"اسم الطعام",


    "delivaz"=>"Delivaz",


    "View_Menu"=>"عرض القائمة ",
    "Track_Order"=>"تتبع الطلب ",
    "SIGN_IN"=>"تسجيل الدخول",
    "Start_Shopping_schedule"=>"  ابدأ التسوق الان ",
    "Your_Daily_orders"=>"  طلباتك اليومية",
    "Order_Now"=>"اطلب الان",
    "phone1"=>"23422",
    "Add_Features"=>" أضف بعض الميزات ",
    "Apple_Store"=>"متجر ابل",

    "Pick"=>"اختر",
    "your"=>"",
    "Order"=>"طلبك",
    "from"=>"من",
    "A"=>"A",
    "to"=>"إلى",
    "Z"=>"Z",

];

?>
