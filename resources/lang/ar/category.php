<?php
return [
    "categories"=>"الأصناف",
    "categories_Menu"=>"قائمة الأصناف",
    "validate"=>"أدخل اسم الصنف",
    "Add_childSection"=>"Add New Category",
    "Name"=>"اسم الصنف ",
    "EnterName"=>"ادخل اسم الصنف",
    "up_category"=>"تحديث الصنف",
    "Action"=>"الحدث",
    "categoryT"=>"ترجمة الصنف",
    "category_MenuT"=>"قائمة ترجمة الاصناف ",
    "translate"=>"ترجمة",
    "lang"=>"اللغة",
    "Add_trans"=>"إضافة ترجمة",
    "validate_selectedLang"=>"اختر اللغة ",
    "EnterTrans"=>"أدخل الترجمة ",
    "langSelecte"=>"اختر اللغة",
    "up_trans"=>"تجديث الترجمة",
    "langSelecte"=>"اختر اللغة ",
    "category"=>"الصنف",
    "showProduct"=>"عرض المنتج",
    "Add_Category"=>"إضافة صنف",






];
