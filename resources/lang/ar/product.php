<?php
return [
    "products"=>"منتجات",
    "products_Menu"=>"قائمة المنتجات",
    "validate"=>"أدخل اسم المنتج ",
    "Add_Product"=>"إضافة منتج جديد",
    "Name"=>"اسم المنتج ",
    "EnterName"=>"أدخل اسم المنتج ",
    "up_product"=>"تحديث المنتج ",
    "Action"=>"الحدث ",
    "productT"=>"ترجمة المنتج ",
    "product_MenuT"=>"قائمة الترجمة للمنتجات  ",
    "translate"=>"الترجمة",
    "lang"=>"اللغة",
    "Add_trans"=>"أضف الترجمة ",
    "validate_selectedLang"=>"اختر اللغة أولا",
    "EnterTrans"=>"أضف الترجمة ",
    "langSelecte"=>"اختر اللغة ",
    "up_trans"=>"تحديث الترجمة ",
    "langSelecte"=>"اختر اللغة  ",
    "product"=>"منتج",
    "showProduct"=>"عرض المنتج ",
    "Content"=>"الوصف ",
    "EnterContent"=>"أدخل الوصف هنا ",
    "validate_ProductContent"=>"أدخل الوصف أولا",
    "transContent"=>"ترجمة الوصف",
    "EnterTransContent"=>"أدخل ترجمة الوصف هنا",
    "translateContent"=>"ترجمة الوصف ",








];
