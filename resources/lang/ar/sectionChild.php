<?php
return [
    "translate"=>"translate",
    "sectionChild"=>"الخدمات",
    "sectionChild_Menu"=>"قائمة الخدمات ",

    "validate"=>" الرجاء أدخل الاسم أولا",
    "Add_childSection"=>"إضافة خدمة جديد",
    "Name"=>"اسم الخدمة",
    "EnterName"=>"أدخل اسم الخدمة",
    "up_childSection"=>"تحديث الخدمة",
    "Action"=>"الحدث",
    "sectionChildT"=>"ترجمة الخدمة",
    "sectionChild_MenuT"=>"قائمة الترجمة للخدمات",
    "translate"=>"الترجمة",
    "lang"=>"اللغة",
    "Add_trans"=>"إضافة ترجمة جديدة",
    "EnterTrans"=>"أدخل الترجمة",
    "langSelecte"=>"اختر اللغة",
    "up_trans"=>"تحديث الترجمة",
    "langSelecte"=>"اختر اللغة ",
    "category"=>"الأصناف",
    "showCategory"=>"عرض الأصناف",

];
