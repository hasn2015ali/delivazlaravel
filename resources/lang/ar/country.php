<?php
return [
    "Countries"=>"الدول",
    "Countries_Menu"=>"قائمة الدول ",
    "Add_Country"=>"إضافة دولة جديدة",
    "Name"=>"اسم الدولة",
    "EnterName"=>"أدخل اسم الدولة هنا",
    "Cancel"=>"تراجع",
    "Cities"=>"المدن",
    "Update_Country"=>"تعديل الدولة",

    "Delete_Country"=>"حذف الدولة",
    "Show"=>"عرض",
    "code"=>"رمز الدولة",
    "EnterCode"=>"أدخل رمز الدولة",
    "Cities"=>"المدن",
    "validate"=>"الرجاء أدخل اسم الدولة أولا",
    "code"=>"رمز الدولة",
    "translate"=>"ترجمة",
    "translateNOte"=>"يمكنك إضافة الترجمة حسب لغة الموقع، اختر اللغة أولا ثم ابدأ الترجمة، الان يمكنك الترجمة في اللغة ",
    "lang"=>"اللغة",
    "Add_trans"=>"إضافة ترجمة جديدة",
    "langSelecte"=>"اختر اللغة",
    "up_trans"=>"تحديث الترجمة",
    "validate_selectedLang"=>"اختر اللغة أولا",
    "up_country"=>"تحديث الدولة ",
    "translated"=>"الترجمات",




];
