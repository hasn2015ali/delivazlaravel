<?php
return [
    "section"=>"الاقسام الرئيسية",
    "section_Menu"=>"قائمة الأقسام",
    "validate"=>"الرجاء أدخل اسم القسم أولا",
    "Add_Section"=>"إضافة قسم جديد",
    "EnterName"=>"أدخل اسم القسم",
    "up_section"=>"تحديث القسم",
    "Name"=>"اسم القسم",
    "Show"=>"عرض  ",
    "validate_selectedLang"=>"الرجاء اختيار اللغة أولا",
    "Add_trans"=>"إضافة ترجمة جديدة",
    "langSelecte"=>"اختر اللغة",
    "up_trans"=>"تحديث الترجمة",
    "lang"=>"اللغة",
    "sectionTrans"=>"ترجمة الاقسام الرئيسية",

    "translate"=>"الترجمة",






];
