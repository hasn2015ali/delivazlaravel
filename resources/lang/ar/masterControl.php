<?php
return [
    "Dashboard"=>"الرئيسية",
    "Cancel"=>"تراجع",
    "Action"=>"الحدث",
    "Sure_To_Delete"=>"هل تريد تأكيد الحذف؟",
    "up"=>"تعديل",
    "Submit"=>"إضافة",
    "Delete"=>"حذف",
    "deleteSure"=>"تأكيد الحذف",
    "successfully_Added"=>"تمت الإضافة بنجاح ",
    "updated_successfully"=>"تم التعديل بنجاح ",
    "Deleted_successfully"=>"تم الحذف بنجاح ",
    "Control_Panel"=>"لوحة التحكم",
    "Delivaz"=>"Delivaz",
    "sectionM"=>"قائمة الأقسام الرئيسية",
    "menu"=>"القائمة الرئيسية",
    "serviceM"=>"قائمة الخدمات",
    "productM"=>"قائمة المنتجات",
    "categoryM"=>"قائمة الاصناف",
    "translations_available"=>"الترجمات المتاحة",
    "translated"=>"الترجمات",


];
