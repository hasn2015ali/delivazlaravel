//notification for admin about new restaurants and shop
// console.log(PUSHER_APP_KEY);
// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher(PUSHER_APP_KEY, {
    cluster: PUSHER_APP_CLUSTER
});

var channel = pusher.subscribe('service-provider-joined');
channel.bind('new-service-provider-joined', function(data) {
    // alert(JSON.stringify(data));
    // alert(data.serviceProviderType);
// console.log(data.serviceProviderType);
//     console.log(data.serviceProviderType=="res");
//     console.log(data.serviceProviderType=="ven");

    if(data.serviceProviderType=="res")
    {
        Livewire.emit('newRestaurantJoined');

    }
    if(data.serviceProviderType=="ven")
    {
        Livewire.emit('newShopJoined');

    }
    Livewire.emit('newServiceProviderJoined');
});
