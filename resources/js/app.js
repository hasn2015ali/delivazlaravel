// require('./bootstrap');


/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
var saveAs = saveAs || function (e) {
    "use strict";
    if (typeof e === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
        return
    }
    var t = e.document, n = function () {
            return e.URL || e.webkitURL || e
        }, r = t.createElementNS("http://www.w3.org/1999/xhtml", "a"), o = "download" in r, a = function (e) {
            var t = new MouseEvent("click");
            e.dispatchEvent(t)
        }, i = /constructor/i.test(e.HTMLElement) || e.safari, f = /CriOS\/[\d]+/.test(navigator.userAgent),
        u = function (t) {
            (e.setImmediate || e.setTimeout)(function () {
                throw t
            }, 0)
        }, s = "application/octet-stream", d = 1e3 * 40, c = function (e) {
            var t = function () {
                if (typeof e === "string") {
                    n().revokeObjectURL(e)
                } else {
                    e.remove()
                }
            };
            setTimeout(t, d)
        }, l = function (e, t, n) {
            t = [].concat(t);
            var r = t.length;
            while (r--) {
                var o = e["on" + t[r]];
                if (typeof o === "function") {
                    try {
                        o.call(e, n || e)
                    } catch (a) {
                        u(a)
                    }
                }
            }
        }, p = function (e) {
            if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)) {
                return new Blob([String.fromCharCode(65279), e], {type: e.type})
            }
            return e
        }, v = function (t, u, d) {
            if (!d) {
                t = p(t)
            }
            var v = this, w = t.type, m = w === s, y, h = function () {
                l(v, "writestart progress write writeend".split(" "))
            }, S = function () {
                if ((f || m && i) && e.FileReader) {
                    var r = new FileReader;
                    r.onloadend = function () {
                        var t = f ? r.result : r.result.replace(/^data:[^;]*;/, "data:attachment/file;");
                        var n = e.open(t, "_blank");
                        if (!n) e.location.href = t;
                        t = undefined;
                        v.readyState = v.DONE;
                        h()
                    };
                    r.readAsDataURL(t);
                    v.readyState = v.INIT;
                    return
                }
                if (!y) {
                    y = n().createObjectURL(t)
                }
                if (m) {
                    e.location.href = y
                } else {
                    var o = e.open(y, "_blank");
                    if (!o) {
                        e.location.href = y
                    }
                }
                v.readyState = v.DONE;
                h();
                c(y)
            };
            v.readyState = v.INIT;
            if (o) {
                y = n().createObjectURL(t);
                setTimeout(function () {
                    r.href = y;
                    r.download = u;
                    a(r);
                    h();
                    c(y);
                    v.readyState = v.DONE
                });
                return
            }
            S()
        }, w = v.prototype, m = function (e, t, n) {
            return new v(e, t || e.name || "download", n)
        };
    if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
        return function (e, t, n) {
            t = t || e.name || "download";
            if (!n) {
                e = p(e)
            }
            return navigator.msSaveOrOpenBlob(e, t)
        }
    }
    w.abort = function () {
    };
    w.readyState = w.INIT = 0;
    w.WRITING = 1;
    w.DONE = 2;
    w.error = w.onwritestart = w.onprogress = w.onwrite = w.onabort = w.onerror = w.onwriteend = null;
    return m
}(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content);
if (typeof module !== "undefined" && module.exports) {
    module.exports.saveAs = saveAs
} else if (typeof define !== "undefined" && define !== null && define.amd !== null) {
    define("FileSaver.js", function () {
        return saveAs
    })
}
// console.log('tes');
let btn_toggel_header = document.querySelector(".btn-menu-mobile");
if (btn_toggel_header) {
    btn_toggel_header.addEventListener('click', () => {
        // console.log('test')
        let menu = document.querySelector('.other');
        let close_icon = document.querySelector('.close-icon-header');
        let open_icon = document.querySelector('.open-icon-header');
        menu.classList.toggle("active-meun");
        close_icon.classList.toggle("show-on-mobil");
        open_icon.classList.toggle("show-on-mobil");

    })
}
// window.addEventListener('name-updated', event => {
//     alert('Name updated to: ' + event.detail.newName);
// })
Livewire.on('alert', param => {
    // alert(param['icon']);
    Swal.fire({
        position: 'center-center',
        icon: param['icon'],
        title: param['title'],
        showConfirmButton: false,
        timer: 4000
    })
})
// console.log('test');
$("#save").click(function () {
    // console.log('sss');
    var blob = new Blob([$("html").html()], {
        type: "text/html;charset=utf-8"
    });
    saveAs(blob, "delivaz.html");
    // FileSaver.saveAs("https://httpbin.org/image", "image.jpg");
});

// console.log('ddd');
let filter_btns = document.querySelectorAll(".service-providers-section .btn-filter");
// console.log(filter_btns);
filter_btns.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        // console.log('ddd');
        filter_btns.forEach((btn) => {
            btn.classList.remove('active');
            // btn.nextElementSibling.classList.remove('show');
        });
        e.target.classList.add('active');
    })
});

let faq_btns = document.querySelectorAll(".faq-section .btn-faq");
faq_btns.forEach((btn) => {
    btn.addEventListener('click', (e) => {
        faq_btns.forEach((btn) => {
            // console.log(btn.parentElement);
            btn.parentElement.classList.remove('active');
            btn.parentElement.nextElementSibling.classList.remove('show');
            // console.log(btn.parentElement.nextElementSibling);
        });
        e.target.parentElement.classList.add('active');
    })
})
// window.addEventListener('load',()=>{

//     setTimeout(()=>{
//          $("#loading").fadeOut("slow");
//     },500)

// })


// Livewire.on('openShape',()=> {
//     dropdownShape();
// })
//
// function dropdownShape() {
//     // let shape_modal = document.querySelector(".shape-modle");
//     return {
//         show: false,
//         open() {
//
//             this.show = true
//             // shape_modal.style.display = "block";
//         },
//         close() {
//             this.show = false
//         },
//         isOpen() {
//             return this.show === true
//         },
//     }
// }

// // Get the button that opens the modal
// var btn_open_shape_model = document.querySelector(".open-model");
// if(btn_open_shape_model!=null)
// {
//     // Get the modal
//     let shape_modal = document.querySelector(".shape-modle");
//
//     // Get the <span> element that closes the modal
//     var span_close = document.querySelector(".close-shape-model");
//
// // When the user clicks the button, open the modal
//     btn_open_shape_model.onclick = function() {
//         shape_modal.style.display = "block";
//         // shape_modal.style.width = "100%";
//     }
//
// // When the user clicks on <span> (x), close the modal
//     span_close.onclick = function() {
//         shape_modal.style.display = "none";
//     }
//
// // When the user clicks anywhere outside of the modal, close it
//     window.onclick = function(event) {
//         if (event.target == shape_modal) {
//             shape_modal.style.display = "none";
//         }
//     }
//
//
// }

let btn_toggel_Filter_Menu = document.querySelector(".btn-menu-filter");
// console.log(btn_toggel_Filter_Menu);
if (btn_toggel_Filter_Menu) {
    btn_toggel_Filter_Menu.addEventListener('click', () => {
        // show-on-mobil
        // console.log('test')
        let menu = document.querySelector('.filters');
        let close_icon = document.querySelector('.close-icon');
        let open_icon = document.querySelector('.open-icon');


        if (menu.classList.contains("show-on-mobil")) {
            menu.classList.remove("opacity-1");
            // close_icon.classList.remove("opacity-0");
            // open_icon.classList.remove("opacity-1");

            menu.classList.add("opacity-0");
            // close_icon.classList.add("opacity-0");
            // open_icon.classList.add("opacity-1");

            setTimeout(function () {
                menu.classList.toggle("show-on-mobil");
                // close_icon.classList.toggle("show-on-mobil");
                // open_icon.classList.toggle("show-on-mobil");

            }, 400);

        } else {
            // close_icon.classList.toggle("show-on-mobil");
            // // console.log(close_icon);
            // open_icon.classList.toggle("show-on-mobil");
            menu.classList.toggle("show-on-mobil");
            setTimeout(function () {
                menu.classList.add("opacity-1");
                // close_icon.classList.add("opacity-1");
                // open_icon.classList.add("opacity-1");

                menu.classList.remove("opacity-0");
                // close_icon.classList.remove("opacity-0");
                // open_icon.classList.remove("opacity-1");


            }, 400);
        }

        // menu.classList.add("transition-700ms");
        close_icon.classList.toggle("show-on-mobil");
        open_icon.classList.toggle("show-on-mobil");

    })
}




window.addEventListener('initialize-date-picker', event => {
    $('#date-schedule-start').datepicker({
        format: 'mm/dd/yyyy',

        autoclose: true,
        container: '.data-start-container',
    });
    $('#date-schedule-end').datepicker({
        format: 'mm/dd/yyyy',

        autoclose: true,
        container: '.data-end-container',
    });
    $('.bs-timepicker').timepicker();
    // alert('Name updated to: ' + event.detail.newName);

})
// $(function () {
//     $('#datetimepicker3').datetimepicker({
//         format: 'LT'
//     });
// });
