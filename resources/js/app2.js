// console.log('test');
let btn_menu_mobile = document.querySelector(".btn-menu-mobile");
if (btn_menu_mobile) {
    btn_menu_mobile.addEventListener('click', () => {
        let close_icon = document.querySelector('.close-icon-header');
        let open_icon = document.querySelector('.open-icon-header');
        let logout = document.querySelector('.logout');
        if (logout.classList.contains("show-on-mobil")) {
            logout.classList.remove("opacity-1");

            logout.classList.add("opacity-0");

            setTimeout(function () {
                logout.classList.toggle("show-on-mobil");

            }, 400);

        } else {
            logout.classList.toggle("show-on-mobil");
            setTimeout(function () {
                logout.classList.add("opacity-1");
                logout.classList.remove("opacity-0");
            }, 400);

        }

        close_icon.classList.toggle("show-on-mobil");
        open_icon.classList.toggle("show-on-mobil");

        console.log("clicked");

    });
}
// vars for sidebar list
let close_icon = document.querySelector('.close-sideBar');
let open_icon = document.querySelector('.open-sideBar');
let sidebar = document.querySelector('.sidebar');
let content_section = document.querySelector('.content-section');
let side_bar_section = document.querySelector('.side-bar-section');

let sidebar_container = document.querySelector('.sidebar-container');
let icon_list = document.querySelector('.icon-list');
// save in local storage
let List_menu = localStorage.getItem('list');
// console.log(List_menu);
if (List_menu == "small") {
    // sidebar.classList.add("translateX-negative-120px");
    content_section.classList.add("width-95");
    content_section.classList.remove("width-82");
    localStorage.setItem('list', 'small');
    sidebar_container.classList.add("d-none");
    icon_list.classList.remove("d-none");
    close_icon.classList.add("d-none");
    // sidebar.classList.remove("translateX-to-0");
    open_icon.classList.remove("d-none");
    side_bar_section.style.width = "5%";
}
let btn_open_close_sideBar = document.querySelector(".btn-open-close-sideBar");
if (btn_open_close_sideBar) {
    btn_open_close_sideBar.addEventListener('click', () => {

        if (sidebar_container.classList.contains("d-none")) {
            setTimeout(function () {
                sidebar_container.classList.remove("d-none");
                icon_list.classList.add("d-none");
            }, 400);
            close_icon.classList.remove("d-none");
            open_icon.classList.add("d-none");
            localStorage.setItem('list', 'big');
            if (content_section.classList.contains("width-95")) {
                content_section.classList.remove("width-95");
                content_section.classList.add("width-82");
                side_bar_section.style.width = "18%";
            }

            // setTimeout(function () {
            // sidebar.classList.add("translateX-to-0");
            // sidebar.classList.remove("translateX-negative-120px");

            // }, 400);
        } else {
            // sidebar.classList.add("translateX-negative-120px");
            content_section.classList.add("width-95");
            content_section.classList.remove("width-82");
            localStorage.setItem('list', 'small');

            setTimeout(function () {
                sidebar_container.classList.add("d-none");
                icon_list.classList.remove("d-none");
                close_icon.classList.add("d-none");
                // sidebar.classList.remove("translateX-to-0");
                open_icon.classList.remove("d-none");

            }, 200);
            setTimeout(function () {
                side_bar_section.style.width = "5%";
            }, 400);


        }
    });
}

// side bar
let btn_side_bar_menu = document.querySelector(".btn-side-bar-menu");
if (btn_side_bar_menu) {
    btn_side_bar_menu.addEventListener('click', () => {
        let close_icon = document.querySelector('.close-icon-sideBar');
        let open_icon = document.querySelector('.open-icon-sideBar');
        let sidebar = document.querySelector('.sidebar');
        let content_section = document.querySelector('.content-section');
        if (sidebar.classList.contains("show-on-mobil-block")) {
            sidebar.classList.remove("opacity-1");
            sidebar.classList.add("opacity-0");
            sidebar.classList.remove("translateX-to-0");
            sidebar.classList.add("translateX-full-negative");
            content_section.classList.remove("translateX-positive-200px");
            content_section.classList.add("translateX-to-0");
            setTimeout(function () {
                sidebar.classList.toggle("show-on-mobil-block");

            }, 400);

        } else {
            sidebar.classList.toggle("show-on-mobil-block");
            setTimeout(function () {
                sidebar.classList.add("opacity-1");
                sidebar.classList.remove("opacity-0");
                sidebar.classList.add("translateX-to-0");
                sidebar.classList.remove("translateX-full-negative");
                content_section.classList.add("translateX-positive-200px");
                content_section.classList.remove("translateX-to-0");
            }, 400);

        }

        close_icon.classList.toggle("show-on-mobil-block");
        open_icon.classList.toggle("show-on-mobil-block");

        // console.log("clicked");
    });
}
window.app = function () {
    return {
        isServiceProviderInfo: false,
        isOwner: false,
        isDocuments: false,
        isPassword: false,
        isList: true,
        toggleServiceProviderInfo() {
            this.isServiceProviderInfo = !this.isServiceProviderInfo;
            this.isOwner = false;
            this.isPassword = false;
            this.isDocuments = false;
        },
        openServiceProviderInfo() {
            this.isServiceProviderInfo = true;
            this.isList = false;
            this.isDocuments=false;
            this.isOwner=false;
            Livewire.emit('restInfo')

        },
        closeServiceProviderInfo() {
            this.isServiceProviderInfo = false;
            this.isList = true;
            this.isDocuments=false;
            this.isOwner=false;
            // console.log('closeinfo');

        },

        toggleOwner() {
            this.isServiceProviderInfo = false;
            this.isOwner = !this.isOwner;
            this.isPassword = false;
        },
        openOwner() {
            this.isOwner = true;
            this.isList = false;
            this.isDocuments=false;
            this.isPassword=false;
            Livewire.emit('restOwner');

            // console.log('openOwner');


        },
        closeOwner() {
            this.isOwner = false;
            this.isList = true;
            this.isDocuments=false;
            this.isPassword=false;
            // console.log('closeOwner');

        },
        openServiceProviderDocuments() {
            this.isDocuments = true;
            this.isList = false;
            this.isPassword=false;
            this.isOwner=false;
            // console.log('open');
            Livewire.emit('restDocument');
        },
        closeServiceProviderDocuments() {
            // console.log('closeDocument');
            this.isDocuments = false;
            this.isList = true;
            this.isPassword=false;
            this.isOwner=false;
        },

        togglePassword() {
            this.isServiceProviderInfo = false;
            this.isOwner = false;
            this.isPassword = !this.isPassword;
        },
        openPassword() {
            this.isPassword = true;
            this.isList = false;
            this.isOwner=false;
            this.isDocuments=false;
            Livewire.emit('restPassword');

        },
        closePassword() {
            this.isPassword = false;
            this.isList = true;
            this.isOwner=false;
            this.isDocuments=false;
        },

        // close() {
        //     this.show = false;
        // },
        // isOpen() {
        //     return this.show === true;
        // }
    };
};


Livewire.on('alert', param => {
    // alert(param['icon']);
    Swal.fire({
        position: 'center-center',
        icon: param['icon'],
        title: param['title'],
        showConfirmButton: false,
        timer: 4000
    })
})

Livewire.on('deleteAction2', () => {
    $('#delCountry2').modal('hide');
})
Livewire.on('addNewSize', () => {
    $('#addNewSize').modal('hide');
})
Livewire.on('updateSize', () => {
    $('#updateSize').modal('hide');
})

//
// var cleave = new Cleave('#phone-format', {
//     phone: true,
//     phoneRegionCode: 'SY'
// });
