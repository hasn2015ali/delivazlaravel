/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;// require('./bootstrap');

/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
var saveAs = saveAs || function (e) {
  "use strict";

  if (typeof e === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
    return;
  }

  var t = e.document,
      n = function n() {
    return e.URL || e.webkitURL || e;
  },
      r = t.createElementNS("http://www.w3.org/1999/xhtml", "a"),
      o = ("download" in r),
      a = function a(e) {
    var t = new MouseEvent("click");
    e.dispatchEvent(t);
  },
      i = /constructor/i.test(e.HTMLElement) || e.safari,
      f = /CriOS\/[\d]+/.test(navigator.userAgent),
      u = function u(t) {
    (e.setImmediate || e.setTimeout)(function () {
      throw t;
    }, 0);
  },
      s = "application/octet-stream",
      d = 1e3 * 40,
      c = function c(e) {
    var t = function t() {
      if (typeof e === "string") {
        n().revokeObjectURL(e);
      } else {
        e.remove();
      }
    };

    setTimeout(t, d);
  },
      l = function l(e, t, n) {
    t = [].concat(t);
    var r = t.length;

    while (r--) {
      var o = e["on" + t[r]];

      if (typeof o === "function") {
        try {
          o.call(e, n || e);
        } catch (a) {
          u(a);
        }
      }
    }
  },
      p = function p(e) {
    if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)) {
      return new Blob([String.fromCharCode(65279), e], {
        type: e.type
      });
    }

    return e;
  },
      v = function v(t, u, d) {
    if (!d) {
      t = p(t);
    }

    var v = this,
        w = t.type,
        m = w === s,
        y,
        h = function h() {
      l(v, "writestart progress write writeend".split(" "));
    },
        S = function S() {
      if ((f || m && i) && e.FileReader) {
        var r = new FileReader();

        r.onloadend = function () {
          var t = f ? r.result : r.result.replace(/^data:[^;]*;/, "data:attachment/file;");
          var n = e.open(t, "_blank");
          if (!n) e.location.href = t;
          t = undefined;
          v.readyState = v.DONE;
          h();
        };

        r.readAsDataURL(t);
        v.readyState = v.INIT;
        return;
      }

      if (!y) {
        y = n().createObjectURL(t);
      }

      if (m) {
        e.location.href = y;
      } else {
        var o = e.open(y, "_blank");

        if (!o) {
          e.location.href = y;
        }
      }

      v.readyState = v.DONE;
      h();
      c(y);
    };

    v.readyState = v.INIT;

    if (o) {
      y = n().createObjectURL(t);
      setTimeout(function () {
        r.href = y;
        r.download = u;
        a(r);
        h();
        c(y);
        v.readyState = v.DONE;
      });
      return;
    }

    S();
  },
      w = v.prototype,
      m = function m(e, t, n) {
    return new v(e, t || e.name || "download", n);
  };

  if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
    return function (e, t, n) {
      t = t || e.name || "download";

      if (!n) {
        e = p(e);
      }

      return navigator.msSaveOrOpenBlob(e, t);
    };
  }

  w.abort = function () {};

  w.readyState = w.INIT = 0;
  w.WRITING = 1;
  w.DONE = 2;
  w.error = w.onwritestart = w.onprogress = w.onwrite = w.onabort = w.onerror = w.onwriteend = null;
  return m;
}(typeof self !== "undefined" && self || typeof window !== "undefined" && window || this.content);

if ( true && module.exports) {
  module.exports.saveAs = saveAs;
} else if ( true && __webpack_require__.amdD !== null && __webpack_require__.amdO !== null) {
  !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
    return saveAs;
  }).call(exports, __webpack_require__, exports, module),
		__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
} // console.log('tes');


var btn_toggel_header = document.querySelector(".btn-menu-mobile");

if (btn_toggel_header) {
  btn_toggel_header.addEventListener('click', function () {
    // console.log('test')
    var menu = document.querySelector('.other');
    var close_icon = document.querySelector('.close-icon-header');
    var open_icon = document.querySelector('.open-icon-header');
    menu.classList.toggle("active-meun");
    close_icon.classList.toggle("show-on-mobil");
    open_icon.classList.toggle("show-on-mobil");
  });
} // window.addEventListener('name-updated', event => {
//     alert('Name updated to: ' + event.detail.newName);
// })


Livewire.on('alert', function (param) {
  // alert(param['icon']);
  Swal.fire({
    position: 'center-center',
    icon: param['icon'],
    title: param['title'],
    showConfirmButton: false,
    timer: 4000
  });
}); // console.log('test');

$("#save").click(function () {
  // console.log('sss');
  var blob = new Blob([$("html").html()], {
    type: "text/html;charset=utf-8"
  });
  saveAs(blob, "delivaz.html"); // FileSaver.saveAs("https://httpbin.org/image", "image.jpg");
}); // console.log('ddd');

var filter_btns = document.querySelectorAll(".service-providers-section .btn-filter"); // console.log(filter_btns);

filter_btns.forEach(function (btn) {
  btn.addEventListener('click', function (e) {
    // console.log('ddd');
    filter_btns.forEach(function (btn) {
      btn.classList.remove('active'); // btn.nextElementSibling.classList.remove('show');
    });
    e.target.classList.add('active');
  });
});
var faq_btns = document.querySelectorAll(".faq-section .btn-faq");
faq_btns.forEach(function (btn) {
  btn.addEventListener('click', function (e) {
    faq_btns.forEach(function (btn) {
      // console.log(btn.parentElement);
      btn.parentElement.classList.remove('active');
      btn.parentElement.nextElementSibling.classList.remove('show'); // console.log(btn.parentElement.nextElementSibling);
    });
    e.target.parentElement.classList.add('active');
  });
}); // window.addEventListener('load',()=>{
//     setTimeout(()=>{
//          $("#loading").fadeOut("slow");
//     },500)
// })
// Livewire.on('openShape',()=> {
//     dropdownShape();
// })
//
// function dropdownShape() {
//     // let shape_modal = document.querySelector(".shape-modle");
//     return {
//         show: false,
//         open() {
//
//             this.show = true
//             // shape_modal.style.display = "block";
//         },
//         close() {
//             this.show = false
//         },
//         isOpen() {
//             return this.show === true
//         },
//     }
// }
// // Get the button that opens the modal
// var btn_open_shape_model = document.querySelector(".open-model");
// if(btn_open_shape_model!=null)
// {
//     // Get the modal
//     let shape_modal = document.querySelector(".shape-modle");
//
//     // Get the <span> element that closes the modal
//     var span_close = document.querySelector(".close-shape-model");
//
// // When the user clicks the button, open the modal
//     btn_open_shape_model.onclick = function() {
//         shape_modal.style.display = "block";
//         // shape_modal.style.width = "100%";
//     }
//
// // When the user clicks on <span> (x), close the modal
//     span_close.onclick = function() {
//         shape_modal.style.display = "none";
//     }
//
// // When the user clicks anywhere outside of the modal, close it
//     window.onclick = function(event) {
//         if (event.target == shape_modal) {
//             shape_modal.style.display = "none";
//         }
//     }
//
//
// }

var btn_toggel_Filter_Menu = document.querySelector(".btn-menu-filter"); // console.log(btn_toggel_Filter_Menu);

if (btn_toggel_Filter_Menu) {
  btn_toggel_Filter_Menu.addEventListener('click', function () {
    // show-on-mobil
    // console.log('test')
    var menu = document.querySelector('.filters');
    var close_icon = document.querySelector('.close-icon');
    var open_icon = document.querySelector('.open-icon');

    if (menu.classList.contains("show-on-mobil")) {
      menu.classList.remove("opacity-1"); // close_icon.classList.remove("opacity-0");
      // open_icon.classList.remove("opacity-1");

      menu.classList.add("opacity-0"); // close_icon.classList.add("opacity-0");
      // open_icon.classList.add("opacity-1");

      setTimeout(function () {
        menu.classList.toggle("show-on-mobil"); // close_icon.classList.toggle("show-on-mobil");
        // open_icon.classList.toggle("show-on-mobil");
      }, 400);
    } else {
      // close_icon.classList.toggle("show-on-mobil");
      // // console.log(close_icon);
      // open_icon.classList.toggle("show-on-mobil");
      menu.classList.toggle("show-on-mobil");
      setTimeout(function () {
        menu.classList.add("opacity-1"); // close_icon.classList.add("opacity-1");
        // open_icon.classList.add("opacity-1");

        menu.classList.remove("opacity-0"); // close_icon.classList.remove("opacity-0");
        // open_icon.classList.remove("opacity-1");
      }, 400);
    } // menu.classList.add("transition-700ms");


    close_icon.classList.toggle("show-on-mobil");
    open_icon.classList.toggle("show-on-mobil");
  });
}

window.addEventListener('initialize-date-picker', function (event) {
  $('#date-schedule-start').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    container: '.data-start-container'
  });
  $('#date-schedule-end').datepicker({
    format: 'mm/dd/yyyy',
    autoclose: true,
    container: '.data-end-container'
  });
  $('.bs-timepicker').timepicker(); // alert('Name updated to: ' + event.detail.newName);
}); // $(function () {
//     $('#datetimepicker3').datetimepicker({
//         format: 'LT'
//     });
// });

/***/ }),

/***/ "./resources/sass/controlPanel/arabic/controlPanel_ar.scss":
/*!*****************************************************************!*\
  !*** ./resources/sass/controlPanel/arabic/controlPanel_ar.scss ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/sass/controlPanel/english/controlPanel.scss":
/*!***************************************************************!*\
  !*** ./resources/sass/controlPanel/english/controlPanel.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/sass/template/en/style.scss":
/*!***********************************************!*\
  !*** ./resources/sass/template/en/style.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/sass/template/en/controlPanel/controlPanelStyles.scss":
/*!*************************************************************************!*\
  !*** ./resources/sass/template/en/controlPanel/controlPanelStyles.scss ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/amd define */
/******/ 	(() => {
/******/ 		__webpack_require__.amdD = function () {
/******/ 			throw new Error('define cannot be used indirect');
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/amd options */
/******/ 	(() => {
/******/ 		__webpack_require__.amdO = {};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/app": 0,
/******/ 			"css/controlPanel": 0,
/******/ 			"css/controlPanel_ar": 0,
/******/ 			"css/template/controlPanel/controlPanelStyles": 0,
/******/ 			"css/template/en_style": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["css/controlPanel","css/controlPanel_ar","css/template/controlPanel/controlPanelStyles","css/template/en_style"], () => (__webpack_require__("./resources/js/app.js")))
/******/ 	__webpack_require__.O(undefined, ["css/controlPanel","css/controlPanel_ar","css/template/controlPanel/controlPanelStyles","css/template/en_style"], () => (__webpack_require__("./resources/sass/controlPanel/arabic/controlPanel_ar.scss")))
/******/ 	__webpack_require__.O(undefined, ["css/controlPanel","css/controlPanel_ar","css/template/controlPanel/controlPanelStyles","css/template/en_style"], () => (__webpack_require__("./resources/sass/controlPanel/english/controlPanel.scss")))
/******/ 	__webpack_require__.O(undefined, ["css/controlPanel","css/controlPanel_ar","css/template/controlPanel/controlPanelStyles","css/template/en_style"], () => (__webpack_require__("./resources/sass/template/en/style.scss")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["css/controlPanel","css/controlPanel_ar","css/template/controlPanel/controlPanelStyles","css/template/en_style"], () => (__webpack_require__("./resources/sass/template/en/controlPanel/controlPanelStyles.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;