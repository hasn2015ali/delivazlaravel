let body = document.body, html = document.documentElement;

let height = Math.max (
  body.scrollHeight,
  body.offsetHeight,
  html.clientHeight,
  html.scrollHeight,
  html.offsetHeight
);
// console.log ('height', height);
let connect_btn = document.querySelector ('.btn-bottom');
let half_heigth = 51 * height / 100;
let top_bottom=document.querySelector ('#top-bottom');
let windoScrollTop=0;
// console.log(top_bottom);
if(top_bottom!=null){
  window.onscroll = function () {
    windoScrollTop = this.pageYOffset;
   if(windoScrollTop>=half_heigth){
     top_bottom.classList.remove("fa-angle-down");
     top_bottom.classList.add("fa-angle-up");
     //   console.log("change");
   }
   if(windoScrollTop<half_heigth){
     top_bottom.classList.remove("fa-angle-up");
     top_bottom.classList.add("fa-angle-down");
     //   console.log("change");
   }
 //   console.log (windoScrollTop);
 };
}


up_down_event=document.querySelector("#up_down_event");
if(up_down_event!=null){
  up_down_event.addEventListener('click',()=>{

    if(windoScrollTop>=half_heigth){
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
      }
      if(windoScrollTop<half_heigth){
        document.body.scrollTop = height;
        document.documentElement.scrollTop = height;
      }


})
}




let drop_choice=document.querySelectorAll('.dropdown_selecte p');
let dropdown_selecte_btn=document.querySelector('#dropdown_selecte_btn');
drop_choice.forEach(p=>{
  p.addEventListener('click',(e)=>{
    text=e.target.textContent;
    // console.log(text);
    dropdown_selecte_btn.innerText=text;
  })
})

let drop_choice_city=document.querySelectorAll('.dropdown_selecte_city p');
let dropdown_selecte_btn_city=document.querySelector('#dropdown_selecte_btn_city');
drop_choice_city.forEach(p=>{
  p.addEventListener('click',(e)=>{
    text=e.target.textContent;
    // console.log(text);
    dropdown_selecte_btn_city.innerText=text;
  })
})

// $(document).ready(function(){
//     console.log(btn_menu_mobile);
//     $(".btn-menu-mobile").click(function(){
//         console.log("text");
//         $(".mobile-menu").slideToggle("slow");
//         console.log("text")
//     });
// });
let btn_menu_mobile=document.querySelector('.btn-menu-mobile');

if(btn_menu_mobile!=null){
    let mobile_menu=document.querySelector('.mobile-menu');

    btn_menu_mobile.addEventListener('click',()=>{
        console.log("text");
        mobile_menu.classList.toggle("header-bottom");
    })
}
