<?php

namespace App\Events;

use App\Models\ServiceProvider;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ServiceProviderJoined implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

//    public $serviceProviderID;
    public $serviceProviderType;
//    /**
//     * Create a new event instance.
//     *
//     * @return void
//     */
    public function __construct($serviceProviderType)
    {
        //
//        dd($serviceProviderID);
        $this->serviceProviderType=$serviceProviderType;
//        $this->serviceProvider=ServiceProvider::find($this->serviceProviderID);
//        dd($this->serviceProvider);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['service-provider-joined'];
//        return new Channel('service-provider-joined');
    }

    public function broadcastAs() {

        return 'new-service-provider-joined';
    }
}
