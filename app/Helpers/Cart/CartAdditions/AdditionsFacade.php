<?php

namespace App\Helpers\Cart\CartAdditions;

use Illuminate\Support\Facades\Facade;

class AdditionsFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'addition'; }
}
