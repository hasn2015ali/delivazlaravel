<?php

namespace App\Helpers\Cart\CartAdditions;
use App\Models\Food;
use Illuminate\Support\Facades\Session;

class Additions
{



    public function __construct()
    {
//        dd($this->get());
        if($this->get() === null)
        {
            $this->set($this->empty());
        }

//        dd($this->get());

    }

    public function getOldQuantity($additionID)
    {
        $cart = $this->get();
//        dd(array_column($cart['additions'], 'id'),array_search($additionID,
//            array_column($cart['additions'], 'id')),$cart);
        $r=array_search($additionID,
            array_column($cart['additions'], 'id'));
//        dd($r);
        if($r===false)
        {
            return 0;
        }
        else
        {
            return   $cart['additions'][$r]['quantity'];

        }

    }

    public function update($additionID,int $type)
    {
        $cart = $this->get();
//        dd(array_column($cart['additions'], 'id'),array_search($additionID,
//            array_column($cart['additions'], 'id')),$cart);
        $r=array_search($additionID,
            array_column($cart['additions'], 'id'));
        if($type==0)
        {
            $cart['additions'][$r]['quantity']=$cart['additions'][$r]['quantity']-1;
        }
        if($type==1)
        {
            $cart['additions'][$r]['quantity']=$cart['additions'][$r]['quantity']+1;
        }

        $this->set($cart);
//        dd($cart['products'][$r]['quantity'],$type);
    }

    public function getIDForAddedItem($addetionForProduct)
    {
        $addetionIDs=[];
        foreach ($addetionForProduct['additions'] as $item)
        {
            array_push($addetionIDs,$item['id']);
        }
        return $addetionIDs;
    }
    public function add($id,$price,$name,$quantity): void
    {
        $cart = $this->get();
        $product=array(
            'id'=>$id,
            'price'=>$price,
            'name'=>$name,
            'quantity'=>$quantity,
        );
//        dd($cart,$product);
        array_push($cart['additions'], $product);
//        dd($cart,$product);
//        array_push($cart['additions'], $product);
        $this->set($cart);
    }

    public function remove($productId): void
    {
        $cart = $this->get();
        array_splice($cart['additions'],
            array_search($productId,
                array_column($cart['additions'], 'id')), 1);
        $this->set($cart);
    }



    public function clear(): void
    {
        $this->set($this->empty());
    }

    public function empty(): array
    {
        return [
            'additions' => [],
        ];
    }



    public function get(): ?array
    {
       return  session('items2');
//        return session()->get('items');
    }

    private function set($cart): void
    {
//        dd($cart);
//        session(['items' => $cart]);
//        $request->session()->put('items',$cart);
       session()->put('items2', $cart);
    }



}
