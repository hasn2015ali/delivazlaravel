<?php

namespace App\Helpers\Cart;

use App\Models\Food;
use Illuminate\Support\Facades\Session;

class Cart
{


    public function __construct()
    {
        if ($this->get() === null) {
            $this->set($this->empty());

        }
//
//        if($this->getAddition() === null)
//        {
//            $this->setAddition($this->emptyAdditions());
//
//        }
    }
//
//    public function setItemAddition($id,$price)
//    {
//////        $additions = $this->emptyAdditions();
//        $additions = $this->getAddition();
////
////        dd($additions);
//        $addition=array(
//            'id'=>$id,
//            'price'=>$price,
//        );
//////        dd($additions,$addition);
////
//        array_push($additions['additions'], $addition);
////        dd($additions,$addition);
//        $this->setAddition($additions);
//
//    }

    public function add($productDetails): void
    {
        $cart = $this->get();
        $product = array(
            'id' => $productDetails['id'],
            'itemID' => $productDetails['itemID'],
            'serviceProviderID' => $productDetails['serviceProviderID'],
            'price' => $productDetails['price'],
            'size' => $productDetails['size'],
            'photo' => $productDetails['photo'],
            'type' => $productDetails['type'],
            'name' => $productDetails['name'],
            'quantity' => $productDetails['quantity'],
            'country_id' => $productDetails['country_id'],
            'city_id' => $productDetails['city_id'],
            'additions' => $productDetails['additions']
        );
//        dd($cart,$product);
        array_push($cart['products'], $product);
//        array_push($cart['products'], $product);
        $this->set($cart);
    }

    public function getIDForAddedItem($itemsInCart)
    {
        $itemIDs = [];
        foreach ($itemsInCart['products'] as $item) {
            array_push($itemIDs, $item['itemID'] . $item['type']);
        }
        return $itemIDs;
    }

    public function checkItemCityAndCountry($city_id,$countryId,$itemsInCart)
    {

//        dd($itemsInCart);
        if(count($itemsInCart['products'])>0)
        {
            if($itemsInCart['products'][0]['country_id']==$countryId and $itemsInCart['products'][0]['city_id']==$city_id ){
                return true;
            }
            else{
                return false;
            }

        }
        else{
            return true;
        }
//        dd($city_id,$countryId,count($cart['products']));
    }
    public function remove($productId): void
    {
        $cart = $this->get();
        array_splice($cart['products'],
            array_search($productId,
                array_column($cart['products'], 'id')), 1);
        $this->set($cart);
    }

//    public function removeAddition($additionId): void
//    {
//        $additions = $this->getAddition();
//        array_splice($additions['additions'],
//            array_search($additionId,
//                array_column($additions['additions'], 'id')), 1);
//        $this->setAddition($additions);
//    }


    public function update($productId, int $type)
    {
        $cart = $this->get();
        $r = array_search($productId,
            array_column($cart['products'], 'id'));
        if ($type == 0) {
            if(  $cart['products'][$r]['quantity'] ==1)
            {
                return;
            }
            $cart['products'][$r]['quantity'] = $cart['products'][$r]['quantity'] - 1;
        }
        if ($type == 1) {
            $cart['products'][$r]['quantity'] = $cart['products'][$r]['quantity'] + 1;
        }

        $this->set($cart);
//        dd($cart['products'][$r]['quantity'],$type);
    }

    public function getTotal($cart)
    {
//        $cart = $this->get();
        $total = 0;
        foreach ($cart['products'] as $item) {
            $total = $total + $item['price'] * $item['quantity'];
        }
        return $total;
//        dd($total);
    }

    public function convertAllItemToArray($items)
    {
//        dd(count($items));
        if(count($items)!=0)
        {
            $cartContent['products'] = [];
            $res1['additions'] = [];
//            dd($items);
            foreach ($items as $key => $item) {

                $res = collect(json_decode($item['additions']))->toArray();
//                dd(count($res['additions']),$res);
                if (count($res['additions']) > 0) {

                    foreach ($res['additions'] as $key => $r) {
                        $res1['additions'][$key] = collect($r)->toArray();
//                  dd($key,$res[$key]);
                    }
                    $res['additions']  = [];
                    $item['additions'] = $res1;
                    $res1['additions'] = [];
                }
                else
                {
                    $emptyAddition['additions']= [];
                    $item['additions']= $emptyAddition;
                }
                array_push($cartContent['products'], $item);

            }
        }
        else
        {
            $cartContent['products']=[];
        }
//        dd($cartContent);
        return $cartContent;
    }

    public function clear(): void
    {
        $this->set($this->empty());
    }

    public function empty(): array
    {
        return [
            'products' => [],
        ];
    }

    public function emptyAdditions(): array
    {
        return [
            'additions' => [],
        ];
    }

    public function get(): ?array
    {
        return request()->session()->get('cart');
    }

    private function set($cart): void
    {
        request()->session()->put('cart', $cart);
    }

//
//    public function getAddition(): ?array
//    {
//       return $value = session('items');
//
////        return request()->session()->get('additions');
//    }
//
//    private function setAddition($add): void
//    {
////        Session::push('additions' , $add);
//        session(['items' => $add]);
////        request()->session()->put('additions', $add);
//    }


}
