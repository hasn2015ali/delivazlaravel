<?php

namespace App\Helpers\GuestAddresses;
use App\Models\Food;
use Illuminate\Support\Facades\Session;

class Addresses
{


    public function __construct()
    {
        if ($this->get() === null) {
            $this->set($this->empty());

        }
    }


    public function add($address): void
    {
        $addresses = $this->get();
        $addressesCount=count($addresses['addresses']);
//        dd($addressesCount);
        $newAddress=array(
            'id'=>$addressesCount+1,
            'addressName'=>$address['addressName'],
            'firstName'=>$address['firstName'],
            'lastName'=>$address['lastName'],
            'mobileNumber'=>$address['mobileNumber'],
            'buildingName'=>$address['buildingName'],
            'addressOnMap'=>$address['addressOnMap'],
            'addressType'=>$address['addressType'],
            'nearestLandmark'=>$address['nearestLandmark'],
            'user_id'=>$address['user_id']
        );
        array_push($addresses['addresses'], $newAddress);
        $this->set($addresses);
        $addressesCount=null;
    }

    public function remove($addressId): void
    {
        $addresses = $this->get();
        array_splice($addresses['addresses'],
            array_search($addressId,
                array_column($addresses['addresses'], 'id')), 1);
        $this->set($addresses);
    }

    public function findAddress($addressID)
    {
        $addresses = $this->get();
          $res=  array_search($addressID,
                array_column($addresses['addresses'], 'id'));
          return $addresses['addresses'][$res];
//          dd($addresses['addresses'][$res]);
    }

    public function update($addressId,$newAddress)
    {
        $addresses = $this->get();

        $res=  array_search($addressId,
            array_column($addresses['addresses'], 'id'));
        $newAddress['id']=$addressId;
        $addresses['addresses'][$res]=$newAddress;
        $this->set($addresses);

    }



    public function clear(): void
    {
        $this->set($this->empty());
    }

    public function empty(): array
    {
        return [
            'addresses' => [],
        ];
    }

    public function get(): ?array
    {
        return request()->session()->get('addresses');
    }

    private function set($addresses): void
    {
        request()->session()->put('addresses', $addresses);
    }

}
