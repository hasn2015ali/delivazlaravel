<?php

namespace App\Helpers\GuestAddresses;

use Illuminate\Support\Facades\Facade;

class AddressesFacade extends Facade
{
    protected static function getFacadeAccessor() { return 'addresses'; }
}
