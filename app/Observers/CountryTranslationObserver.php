<?php

namespace App\Observers;

use App\Models\CountryTranslation;
use Illuminate\Support\Facades\Cache;

class CountryTranslationObserver
{
    /**
     * Handle the CountryTranslation "created" event.
     *
     * @param  \App\Models\CountryTranslation  $countryTranslation
     * @return void
     */
    public function created(CountryTranslation $countryTranslation)
    {
        //
        Cache::forget('allCountries');

    }

    /**
     * Handle the CountryTranslation "updated" event.
     *
     * @param  \App\Models\CountryTranslation  $countryTranslation
     * @return void
     */
    public function updated(CountryTranslation $countryTranslation)
    {
        //
        Cache::forget('allCountries');

    }

    /**
     * Handle the CountryTranslation "deleted" event.
     *
     * @param  \App\Models\CountryTranslation  $countryTranslation
     * @return void
     */
    public function deleted(CountryTranslation $countryTranslation)
    {
        //
        Cache::forget('allCountries');

    }

    /**
     * Handle the CountryTranslation "restored" event.
     *
     * @param  \App\Models\CountryTranslation  $countryTranslation
     * @return void
     */
    public function restored(CountryTranslation $countryTranslation)
    {
        //
    }

    /**
     * Handle the CountryTranslation "force deleted" event.
     *
     * @param  \App\Models\CountryTranslation  $countryTranslation
     * @return void
     */
    public function forceDeleted(CountryTranslation $countryTranslation)
    {
        //
    }
}
