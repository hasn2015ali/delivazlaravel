<?php

namespace App\Observers;

use App\Models\CityTranslation;
use Illuminate\Support\Facades\Cache;

class CityTranslationObserver
{
    /**
     * Handle the CityTranslation "created" event.
     *
     * @param  \App\Models\CityTranslation  $cityTranslation
     * @return void
     */
    public function created(CityTranslation $cityTranslation)
    {
        //
        Cache::forget('allCities');

    }

    /**
     * Handle the CityTranslation "updated" event.
     *
     * @param  \App\Models\CityTranslation  $cityTranslation
     * @return void
     */
    public function updated(CityTranslation $cityTranslation)
    {
        //
        Cache::forget('allCities');

    }

    /**
     * Handle the CityTranslation "deleted" event.
     *
     * @param  \App\Models\CityTranslation  $cityTranslation
     * @return void
     */
    public function deleted(CityTranslation $cityTranslation)
    {
        //
        Cache::forget('allCities');

    }

    /**
     * Handle the CityTranslation "restored" event.
     *
     * @param  \App\Models\CityTranslation  $cityTranslation
     * @return void
     */
    public function restored(CityTranslation $cityTranslation)
    {
        //
    }

    /**
     * Handle the CityTranslation "force deleted" event.
     *
     * @param  \App\Models\CityTranslation  $cityTranslation
     * @return void
     */
    public function forceDeleted(CityTranslation $cityTranslation)
    {
        //
    }
}
