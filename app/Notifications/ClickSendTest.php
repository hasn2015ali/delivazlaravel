<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\ClickSend\ClickSendMessage;
use NotificationChannels\ClickSend\ClickSendChannel;

class ClickSendTest extends Notification
{
    use Queueable;
    public $token;

    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Required
     */
    public function via($notifiable)
    {
        return [ClickSendChannel::class];
    }

    /**
     * Required
     */
    public function getMessage($notifiable)
    {
        $message="We Send This Code {$this->token} To Verify Your Account On Delivaz";

        return $message;

//        return "SMS test to user #{$notifiable->id} with token {$this->token} by ClickSend";
    }

    /**
     * Optional
     */
    public function updateClickSendMessage($message)
    {
        $message->setFrom('+15555555555');

        return $message;
    }
}
