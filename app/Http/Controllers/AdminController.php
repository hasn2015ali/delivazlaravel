<?php

namespace App\Http\Controllers;

use App\Helpers\Cart\CartFacade;
use App\Models\City;
use App\Models\cityBanner;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\filter;
use App\Models\Section;
use App\Models\ServiceProviderBanner;
use App\Models\UserCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Constraint\Count;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

//    public function redirectUser($lang=null){
////        dd($lang);
////        App::setlocale($lang);
//        if(Auth::user()->role_id=='1'){
//
//            $active=null;
//            return   redirect()->route('locale.home',['locale'=>app()->getLocale()]);
////            return view('admin.user.profil',compact('active'));
//        }else{
//            $active=null;
//            return view('admin.user.profil',compact('active'));
////            return  view('home');
//        }
//    }



    public function showTaxes()
    {
        $active = "taxes";
        return view('admin.taxes.tax', compact('active'));
    }

    public function showRestaurantFilter()
    {
        $active = "restaurant";
        return view('admin.category.restaurantFilter', compact('active'));
    }


    public function ShowTerms()
    {
        $active = "footerPages";
        return view('admin.FooterPages.terms', compact('active'));
    }

    public function ShowPrivacyPolicy()
    {
        $active = "footerPages";
        return view('admin.FooterPages.PrivacyPolicy', compact('active'));
    }

    public function ShowCookiesPolicy()
    {
        $active = "footerPages";
        return view('admin.FooterPages.CookiesPolicy', compact('active'));
    }

    public function ShowFaqPage()
    {
        $active = "footerPages";
        return view('admin.FooterPages.FAQ', compact('active'));
    }


//    public function showRestaurantsPage()
//    {
//        $active="pages";
//        return view('admin.pages.restaurantsPage',compact('active'));
//    }
//
//
//    public function showVendorsPage()
//    {
//        $active="pages";
//        return view('admin.pages.vendorsPage',compact('active'));
//    }


    public function showVendorsFilter()
    {
        $active = "vendor";
        return view('admin.category.vendorsFilter', compact('active'));
    }

    public function showSubscribers()
    {
        $active = "Subscriber";
        return view('admin.subscribers.subscriber', compact('active'));
    }

    public function showMealsFilters($lang = null, $id)
    {

        $active = "restaurant";
        return view('admin.category.mealsFilters', compact('active', 'id'));
    }

    public function showMealFilterTranslate($lang = null, $id)
    {
        $active = "restaurant";
        return view('admin.category.mealFilterTranslate', compact('active', 'id'));
    }



    public function showRestaurantDetails($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.details', compact('active', 'restaurantID','restaurantBanners'));
    }
    public function showRestaurantFilters($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.filters', compact('active', 'restaurantID','restaurantBanners'));
    }

    public function showRestaurantFood($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.food', compact('active', 'restaurantID','restaurantBanners'));
    }

    public function showRestaurantWorkHours($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.workHours', compact('active', 'restaurantID','restaurantBanners'));
    }
    public function showRestaurantBanners($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.bannars', compact('active', 'restaurantID','restaurantBanners'));
    }
    public function showRestaurantDocuments($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.documents', compact('active', 'restaurantID','restaurantBanners'));
    }
    public function showRestaurantRatting($lang = null, $restaurantID)
    {
        $active = "restaurant";
        $restaurantBanners=ServiceProviderBanner::where('service_provider_id',$restaurantID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.restaurantSetting.ratting', compact('active', 'restaurantID','restaurantBanners'));
    }

    public function showVendorSetting($lang = null, $id)
    {
        $active = "vendor";
        return view('admin.category.vendorSetting', compact('active', 'id'));
    }


    public function showShopDetails($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.details', compact('active', 'vendorID','shopBanners'));
    }
    public function showShopFilters($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.filters', compact('active', 'vendorID','shopBanners'));
    }

    public function showShopProducts($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.products', compact('active', 'vendorID','shopBanners'));
    }

    public function showShopWorkHours($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.workHours', compact('active', 'vendorID','shopBanners'));
    }
    public function showShopBanners($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.bannars', compact('active', 'vendorID','shopBanners'));
    }
    public function showShopDocuments($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.documents', compact('active', 'vendorID','shopBanners'));
    }
    public function showShopRatting($lang = null, $vendorID)
    {
        $active = "vendor";
        $shopBanners=ServiceProviderBanner::where('service_provider_id',$vendorID)
            ->orderBy('id','desc')->get();

//        $active2=""
        return view('admin.category.shopSetting.ratting', compact('active', 'vendorID','shopBanners'));
    }

    public function showProductsFilters($lang = null, $id)
    {
//        dd($id);
        $active = "vendor";
        return view('admin.category.productsFilters', compact('active', 'id'));
    }

    public function showProductFilterTranslate($lang = null, $id)
    {
//        dd($id);
        $active = "vendor";
        return view('admin.category.productFilterTranslate', compact('active', 'id'));
    }

    public function showRestaurantFilterTranslate($lang = null, $id)
    {

        $active = "restaurant";
        return view('admin.category.filterTranslate', compact('active', 'id'));
    }

    public function showRestaurantRegistered($lang = null)
    {

        $active = "restaurant";
//        dd('aaa');
        return view('admin.category.restaurantRegistered', compact('active'));
    }

    public function showVendorRegistered($lang = null)
    {

        $active = "vendor";
        return view('admin.category.vendorRegistered', compact('active'));
    }


    public function showVendorFilterTranslate($lang = null, $id)
    {

        $active = "vendor";
        return view('admin.category.filterTranslate', compact('active', 'id'));
    }

    public function showProfile($lang = null)
    {
//        dd($lang);

//        App::setlocale($lang);
        $active = null;
        return view('admin.user.profil', compact('active'));

    }

    public function addUser($lang = null, $type)
    {
        //
//        if($type=='admin'){
//            $type="manager";
//        }
//        App::setlocale($lang);
        $active = "users";
        return view('admin.user.addUser', compact('type', 'active'));
    }

    public function updateUser($lang = null, $id, $type)
    {
//        App::setlocale($lang);
        $active = "users";
        return view('admin.user.editUser', compact('id', 'active', 'type'));
    }

    public function index($lang = null, $type)
    {
        //
//        if($type=='admin'){
//            $type="manager";
//        }
//        App::setlocale($lang);
        $active = "users";
        return view('admin.user.users', compact('type', 'active'));
    }


    public function showDashboardForApplication($lang = null)
    {
        $active = "dashboard";
        return view('admin.dashboard', compact('active'));

    }

    public function showDashboard(Request $request ,$lang = null)
    {
        //
//        App::setlocale($lang);
        $itemsInCart=CartFacade::get();
        CartFacade::clear();
        $user = Auth::user();
        $oldItemINCart = UserCart::where('user_id', Auth::id())
            ->get()
            ->toArray();
        $oldItemINCart2 = CartFacade::convertAllItemToArray($oldItemINCart);
        $itemIdsInCart = CartFacade::getIDForAddedItem($oldItemINCart2);
//        dd($itemIdsInCart,$itemIdsInCart);
        if(count($itemsInCart['products'])>0)
        {
            foreach ($itemsInCart['products'] as $key=> $item)
            {
                $itemIdWithType = $item['itemID'] . $item['type'];
//                if($key==2){
//                    dd($itemIdWithType,$itemIdsInCart,!in_array($itemIdWithType, $itemIdsInCart));
//
//                }
                if (!in_array($itemIdWithType, $itemIdsInCart)) {
//                    dd($item);
                    $newAddition=json_encode(($item['additions']));
//            dd($newAddition);
                    UserCart::create([
                        'idDES' => $item['id'],
                        'itemID' =>  $item['itemID'] ,
                        'serviceProviderID' =>  $item['serviceProviderID'],
                        'price' =>  $item['price'] ,
                        'size' =>  $item['size'],
                        'photo' =>$item['photo'],
                        'type' =>$item['type'],
                        'name' =>  $item['name'],
                        'quantity' => $item['quantity'],
                        'additions' => $newAddition,
                        'user_id' => Auth::id(),
                    ]);
                    $newAddition=null;
                }

            }

//
        }
//        dd($itemsInCart,count($itemsInCart['products']));

        if ($user->role_id == 1) {
            $country = null;
            $city = null;
//            return view('public.user.dashboard', compact('country', 'city'));

            if ($request->session()->has('previousURL')) {
                //
                $previousURL=$request->session()->get('previousURL');
                $request->session()->forget('previousURL');
                return redirect($previousURL);
//                dd($previousURL);
            }
            else
            {
                return view('public.user.dashboard', compact('country', 'city'));
            }

        }elseif( $user->role_id == 9 or $user->role_id == 10) {
            $active = "Basic information";
            return redirect()->route('Basic-Info',['locale'=>App::getLocale()]);
//            return view('public.controlPanel.info',compact('active'));
        }
        else {
            $active = "profile";
            return view('admin.user.profil', compact('active'));

        }


    }

    public function showCountries($lang = null)
    {
        //
//        App::setlocale($lang);
        $active = "Countries";
        return view('admin.country.country', compact('active'));
    }

    public function showCountrySetting($lang = null, $id)
    {
        //
//        App::setlocale($lang);
        $active = "Countries";
        session()->flash('countrySetting', 'details');
        return view('admin.country.countrySetting', compact('active', 'id'));
    }

    public function showCountryTaxes($lang = null, $id)
    {
        //
//        App::setlocale($lang);
        $active = "Countries";
        session()->flash('countrySetting', 'taxes');
        return view('admin.country.countryTaxes', compact('active', 'id'));
    }


    public function countryTranslate($lang = null, $id)
    {
        $country = Country::find($id);
//        App::setlocale($lang);

//        if ( in_array( $lang, ['ar', 'en'] ) ) {
//
//            if ( session()->has( 'lang' ) ) {
//            session()->forget( 'lang' );
//            }
//            session()->put( 'lang', $lang );
//            } else {
//            session()->put( 'lang', 'en' );
//            }
        $active = "Countries";
        return view('admin.country.countryTranslate', compact('active', 'country'));
    }

    public function showCountryCity($lang = null, $id)
    {
        //
//        App::setlocale($lang);
        $active = "Countries";
        $country_id = $id;
//        dd($country_id);
        return view('admin.country.city', compact('active', 'country_id'));
    }

    public function cityTranslate($lang = null, $id)
    {
        //
        $city = City::with('country')->find($id);
        $country = $city->country;
//        App::setlocale($lang);
        $active = "Countries";
        return view('admin.country.cityTranslate', compact('active', 'city', 'country'));
    }



    public function showCityShapes($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::where('country_id',$city->country_id)->where('code_lang','en')->first();
        session()->flash('cityDetails', 'shapes');
        return view('admin.country.cityDetails.shaps', compact('active', 'id','country','city'));
    }
    public function showCityBanners($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $cityBanners=cityBanner::where('city_id',$id)->orderBy('id','desc')->get();
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::where('country_id',$city->country_id)->where('code_lang','en')->first();
       session()->flash('cityDetails', 'Banners');
        return view('admin.country.cityDetails.banners', compact('active', 'id','cityBanners','city','country'));
    }

    public function showCityServiceProviders($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::where('country_id',$city->country_id)->where('code_lang','en')->first();
        session()->flash('cityDetails', 'Recommended');
        return view('admin.country.cityDetails.serviceProviderInCity', compact('active', 'id','country','city'));
    }

    public function showCityServiceProviderPages($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::where('country_id',$city->country_id)->where('code_lang','en')->first();
        session()->flash('cityDetails', 'restaurantsPage');
        return view('admin.country.cityDetails.serviceProvidersPage', compact('active', 'id','country','city'));
    }


    public function showAreas($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::where('country_id',$city->country_id)->where('code_lang','en')->first();
        session()->flash('cityDetails', 'areas');
        return view('admin.country.cityDetails.areas', compact('active', 'id','country','city'));
    }


    public function showDeliveryInCity($lang = null, $id)
    {

//        dd('kk');
        $active = "Countries";
        $city=CityTranslation::where('city_id',$id)->where('code_lang','en')->first();
        $country=CountryTranslation::with('country','country.taxs')->where('country_id',$city->country_id)->where('code_lang','en')->first();
        session()->flash('cityDetails', 'delivery');
        return view('admin.country.cityDetails.deliverySetting', compact('active', 'id','country','city'));
    }

    public function workSystem($lang = null)
    {
        //


//        App::setlocale($lang);
        $active = "workSystem";
        return view('admin.user.workSystem', compact('active'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
