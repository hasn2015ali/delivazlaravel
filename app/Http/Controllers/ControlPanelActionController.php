<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Traits\WithCartTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ControlPanelActionController extends Controller
{
    //
    use WithCartTrait;

    public function addToBasket($id)
    {
//        dd($id);
//        $cart = session()->get('cart');
//        dd($cart);
        $food=Food::with('translation')->find($id);
        $name=$food->translation->where('code_lang','en')->first()->name;
        $price=$food->price;
        $photo=$food->photo;
        $size=$food->size;

        $this->addToCart($id,$name,$price,$photo,$size);

        $cart = session()->get('cart');
//        dd($cart);
        return redirect()->back();
//        if($this->addToCart($id,$name,$price,$photo,$size)==1)
//        {
//            $cart = session()->get('cart');
////            dd($cart);
//        };
    }
    public function downLoadPDF($lang=null,$country,$city)
    {
        $user=Auth::user();

        if($user->role_id==8)
        {
            $country= $user->country_id;
            $city=$user->city_id;
        }

        $subscribers= DB::table('subscribers')
            ->select('subscribers.email')
            ->where('country_id', $country)
            ->where('city_id',$city)
            ->orderBy('id','desc')
            ->get();
        $cityName=DB::table('city_translations')
            ->select('city_translations.name')
            ->where('country_id', $country)
            ->where('city_id',$city)
            ->where('code_lang',"en")
            ->first()->name;

//        dd($cityName);
        $pdf = \PDF::loadView('PDF.subscribersPDF', compact('subscribers','cityName'));

        $fileName="subscribersIn".$cityName.".pdf";
        return $pdf->download($fileName);
    }
}
