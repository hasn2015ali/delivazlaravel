<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowInfo()
    {
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        $active="Basic information";
//        dd($active);
        return view('public.controlPanel.info',compact('active','user'));
    }
    public function ShowSchedulingTime()
    {
        $active="Work Times";
        return view('public.controlPanel.schedulingTime',compact('active'));
    }
    public function ShowBanners()
    {
        $active="Banners";
        return view('public.controlPanel.banners',compact('active'));
    }

    public function ShowSizes()
    {
        $user=Auth::user();
        if($user->role_id==10)
        {
            $active="productSizes";

        }
        elseif($user->role_id==9)
        {
            $active="foodSizes";

        }
        return view('public.controlPanel.food.sizes',compact('active'));
    }

    public function ShowProducts()
    {
        $active="Products";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.products',compact('active','user'));
    }
    public function ShowFoods()
    {
//        dd('kk');
        $active="Foods";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.foods',compact('active','user'));
    }

    public function AddNewFood()
    {
        $active="Foods";
        $typ="food";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.food.add',compact('active','user','typ'));
    }

    public function UpdateFood($lang,$id)
    {
        $active="Foods";
        $typ="food";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.food.update',compact('active','user','typ','id'));
    }
    public function UpdateProduct($lang,$id)
    {
        $active="Products";
        $typ="product";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.food.update',compact('active','user','typ','id'));
    }

    public function AddNewProduct()
    {
        $active="Products";
        $typ="product";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.food.add',compact('active','user','typ'));
    }
    public function EditMyMenu()
    {
        $user=Auth::user();
        if($user->role_id==10)
        {
            $active="productMenu";

        }
        elseif($user->role_id==9)
        {
            $active="foodMenu";

        }
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.food.menu',compact('active','user'));
    }

    public function ShowOrders()
    {
        $active="Orders";
        $userID=\Illuminate\Support\Facades\Auth::id();
        $user=\App\Models\User::with('serviceProvider','country','city')->find($userID);
        return view('public.controlPanel.orders',compact('active','user'));
    }
    public function ShowReviews()
    {
        $active="Ratting && Reviews";
        return view('public.controlPanel.reviews',compact('active'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
