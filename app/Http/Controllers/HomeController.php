<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\Language;
use App\Traits\WithCountryAndCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    use WithCountryAndCity;

    public $countries;


    //
    public function index($lang = null, $country = null, $city = null)
    {
//        dd("test");
        $cityTrans = null;

        $country_lang = null;
        $langueges = ['en', 'ar', 'rus', 'ukr', 'fre'];
        if (!in_array($lang, $langueges)) {
            $lang = "en";
        }


//        dd($oldLang);
        $this->getCountries();
        $countries = $this->countries;
//        dd($this->countries);


        if ($country == null) {
//            $cities = CityTranslation::where('code_lang','=',$lang)->get()  ;
            $cities = null;
//            dd("test");
        } else {

            $country = str_replace('_', ' ', $country);
            $checkedCountry = CountryTranslation::where('name', $country)->get('country_id')->first();
            if ($checkedCountry) {
                $country_id = $checkedCountry->country_id;
                $country = CountryTranslation::where('country_id', $country_id)
                    ->where('code_lang', '=', "en")
                    ->get('name')->first()->name;
                $country_lang = CountryTranslation::where('country_id', $country_id)
                    ->where('code_lang', '=', App::getLocale())
                    ->get('name')->first()->name;
                $cities = CityTranslation::with('city')
                    ->where('code_lang', $lang)
                    ->where('country_id', $country_id)
                    ->orderBy('name', 'asc')
                    ->get();
            } else {
                return redirect('/en');
            }
//            $country_id=CountryTranslation::where('name',$country)->get('country_id')->first()->country_id;


        }

        if ($city != null) {
            $checkedCity = CityTranslation::with('city')->where('name', $city)->first();
//            dd($city,$checkedCity);
            $cityID = $checkedCity->city_id;
            $cityTrans = CityTranslation::where('city_id', $cityID)
                ->where('code_lang', App::getLocale())
                ->first()->name;
            $city = CityTranslation::where('city_id', $cityID)
                ->where('code_lang', "en")
                ->first()->name;

            if ($checkedCity) {
//                dd('rrr');
//                dd($checkedCity->city->state);
                if ($checkedCity->city->state == 0) {
                    return view("public.cityNotAvailable", compact('city', 'country'));

                } elseif ($checkedCity->city->state == 1) {
                    return view("public.cityPage", compact('countries', 'cities', 'country', 'city', 'lang','cityTrans','country_lang'));

                }

            } else {
                return redirect('/en');

            }

        } else {
//            dd($countries,$cities);
            return view("home", compact('countries', 'cities', 'country', 'city', 'lang','cityTrans','country_lang'));

        }

    }

    public function getCountryEn($id)
    {

        $countryId = CountryTranslation::find($id)->country_id;

        $countryEn = CountryTranslation::where('country_id', $countryId)->where('code_lang', '=', 'en')->first()->name;
        $countryEn = str_replace(' ', '_', $countryEn);
        return $countryEn;

//        dd($countryEn);
    }

    public function getCityEn($id)
    {

//       $city_id=CityTranslation::find($id)->city_id;
//       $city_en=City::with('translation')->find($city_id)->translation->where('code_lang','=','en')->first()->name;
        $city_en = CityTranslation::where('code_lang', 'en')->where('city_id', $id)->first()->name;

        return $city_en;

    }

}
