<?php

namespace App\Http\Controllers\fortify;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\Password;
use Laravel\Fortify\Contracts\FailedPasswordResetLinkRequestResponse;
use Laravel\Fortify\Contracts\RequestPasswordResetLinkViewResponse;
use Laravel\Fortify\Contracts\SuccessfulPasswordResetLinkRequestResponse;
use Laravel\Fortify\Fortify;
use Nexmo\Laravel\Facade\Nexmo;

class PasswordResetLinkController extends Controller
{
//    protected $phone;
    //
    /**
     * Show the reset password link request view.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Laravel\Fortify\Contracts\RequestPasswordResetLinkViewResponse
     */
    public function create(Request $request): RequestPasswordResetLinkViewResponse
    {
        return app(RequestPasswordResetLinkViewResponse::class);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Support\Responsable
     */
    public function store(Request $request): Responsable
    {
        $messages = [
            "name.required" => trans( 'auth.Required' ),
            "password.required" => trans( 'auth.passwordRequired' ),
            "password_confirmation.required" => trans( 'auth.password_confirmationRequired' ),
            "password_confirmation.same" => trans( 'auth.password_confirmationSame' ),
            "password.min" => trans( 'auth.password_min' ),
            "email.email" => trans( 'auth.validateIdentification' ),

            "email.required" => trans( 'auth.validateIdentification' ),

            "email.unique" => trans( 'auth.validateEmail' ),
        ];

//        $request->validate([Fortify::email() => 'required|email'], $messages);
        if(filter_var($request->email,FILTER_VALIDATE_EMAIL))
        {
            $request->validate(['email'=> 'required|email'], $messages);
            // We will send the password reset link to this user. Once we have attempted
            // to send the link, we will examine the response then see the message we
            // need to show to the user. Finally, we'll send out a proper response.
            $status = $this->broker()->sendResetLink(
                $request->only(Fortify::email())
            );

            return $status == Password::RESET_LINK_SENT
                ? app(SuccessfulPasswordResetLinkRequestResponse::class, ['status' => $status])
                : app(FailedPasswordResetLinkRequestResponse::class, ['status' => $status]);
        }
        else{

//            dd('phone');
            $phone=$request->email;
            $code=3333;
//            $code=rand(1111,9999);
//            Nexmo::message()->send([
//                'to'   => $phone,
//                'from' => trans( 'userFront.PhoneVerification' ),
//                'text' => $code
//
//            ]);
//           dd($result);
            if(session()->has('verification_code_reset'))
            {
                session()->forget(['verification_code_reset']);
//               session(['verification_code'=>$code]);
                session()->put('verification_code_reset',  $code);

            }
            else
            {
                session(['verification_code_reset'=>$code]);
            }

            session(['verification_reset_status'=>trans( 'userFront.VerificationAnother' )])  ;
//            redirect()->route('password.request',['locale'=>app()->getLocale()]);
        }




    }

    public function resetByPhone()
    {

    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    protected function broker(): PasswordBroker
    {
        return Password::broker(config('fortify.passwords'));
    }
}
