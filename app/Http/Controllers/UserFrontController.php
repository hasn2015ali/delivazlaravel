<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserFrontController extends Controller
{

    public function logout(Request $request)
    {

        Auth::logout();
        $previousURL = url()->previous();
        if ($previousURL) {
            //
            return redirect($previousURL);
//                dd($previousURL);
        } else {
            $country = null;
            $city = null;
            $locale = App::getLocale();
            return redirect('/' . $locale . '/' . $country . '/' . $city);
//                return view('public.user.dashboard', compact('country', 'city'));
        }

    }
    //

//    public function ShowDashboard ($lang=null,$country=null,$city=null)
//    {
////        dd('fff');
//        return view('public.user.dashboard',compact('country','city'));
//
//    }
    public function ShowWishlist($lang = null, $country = null, $city = null)
    {
//        dd('fff');
        return view('public.user.Wishlist', compact('country', 'city'));

    }

    public function RegisterISNotComplete($lang = null, $country = null, $city = null)
    {
//        dd('fff');
        return view('auth.RegisterISNotComplete', compact('country', 'city'));


    }

    public function checkout($lang)
    {
        $country = null;
        $city = null;
        return view('public.user.checkOut', compact('country', 'city'));

    }


}
