<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    public function getCities(Request $request , $lang ,$country=null,$city=null){

        $cities = City::where('country_id','=', $country)->get();
        $data = "";
        $i=0;
        if(count($cities)>0)
        foreach($cities as $item){
            if($i==0) {


                $data .= '<button id="dropdown_selecte_btn_city" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        ' . $cities[0]->name . '

                    </button>';
                $data .= '<div class="dropdown-menu" >';
            }
            $data .= '<p class="dropdown-item" >'. $item->name.'</p>';
            $i++;
        }
        $data .=' </div>';
        return $data;



    }
}
