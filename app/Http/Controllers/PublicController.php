<?php

namespace App\Http\Controllers;

use App\Mail\EmailVerificationCodeSend;
use App\Models\CountryTranslation;
use App\Models\Food;
use App\Models\Product;
use App\Models\ServiceProvider;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithCountryAndCity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Traits\WithUserValidationTrait;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;

class PublicController extends Controller
{
    use WithCountryAndCity;
    public $country,$city;
    // protected $locale=App::getLocale();
    // public $redirectTo = App::getLocale().'/home';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function RegisterShopOrRestaurant($lang=null,$country=null,$city=null)
    {

        if(Auth::check())
        {
            return redirect()->route('Dashboard',['locale'=>App::getLocale()]);

        }
        else
        {
            return view('public.user.registerShopOrRestaurant',compact('city','country'));

        }

    }
    public function showFood($lang=null,$country=null,$city=null,$serviceProvider=null,$foodID)
    {
        $type="food";
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd($food);
        return view('public.category.product',compact('country','city','type','serviceProvider','foodID'));

    }


    public function showProduct($lang=null,$country=null,$city=null,$serviceProvider=null, $productID)
    {
//        dd($product);
        $type="product";
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
        return view('public.category.product',compact('country','city','type','serviceProvider','productID'));

    }

    public function showAboutPage($lang=null,$country=null,$city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd('fff');
        return view('public.Additions.about',compact('country','city'));

    }

    public function ShowTerms($lang=null,$country=null,$city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd($lang,$country);
        return view('public.FooterPages.terms',compact('country','city'));

    }
    public function ShowPrivacyPolicy($lang=null,$country=null,$city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd('fff');
        return view('public.FooterPages.PrivacyPolicy',compact('country','city'));

    }
    public function ShowCookiesPolicy($lang=null,$country=null,$city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd('fff');
        return view('public.FooterPages.CookiesPolicy',compact('country','city'));

    }
    public function ShowFaqPage($lang=null,$country=null,$city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd($lang,$country,$city);
        return view('public.Additions.FAQ',compact('country','city'));

    }

    public function handelCountryAndCityPrefix($country,$city)
    {
        $res=$this->checkCountryNameInURL($country);
        if( $res=="notfound")
        {
            return redirect('/en');
        }
        else
        {
            $this->country=$res ;
        }


        $res1=$this->checkCityNameInURL($city);
        if( $res1=="notfound")
        {
            return redirect('/en');
        }
        else
        {
            $this->city=$res1 ;
        }
    }

    public function showItemsInShape($lang=null , $country=null , $city=null, $shape)
    {

        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
        return view('public.category.serviceProviders',compact('country','city','lang','shape'));

    }

    public function showRestaurantCity($lang=null , $country=null , $city=null , $serviceProviderSlug)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
//        dd($serviceProviderSlug);
//        dd($country);

        $serviceProvider=ServiceProvider::with('city','filters')
            ->where('slug',$serviceProviderSlug)->first();
//        dd($serviceProvider);
        return view('public.category.serviceProvider',compact('country','city','lang','serviceProvider'));

    }

    public function showAnyThing($lang=null , $country=null , $city=null )
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;
        return view('public.anything.anything',compact('country','city','lang'));

    }

    public function show($lang=null , $country=null , $city=null)
    {
        $this->handelCountryAndCityPrefix($country,$city);
        $country=$this->country;
        $city=$this->city;

//        dd('ll');
        return view('public.user.verify-mobile',compact('country','city','lang'));

    }


        public function findOrCreateUser($user, $provider)
    {
        // $messages = [
        //     "email.email" => trans( 'auth.validateIdentification' ),
        //     "email.unique" => trans( 'auth.validateEmail' ),
        // ];

        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        if($user->name==null)
        {
            $userName = strstr($user->email, '@', true); // As of PHP 5.3.0

        }
        else
        {
            $userName=$user->name;

        }
        if(filter_var($user->email,FILTER_VALIDATE_EMAIL))
        {
            $emailCheck = DB::table('users')
            ->select('email')
            ->where('email',$user->email)
            ->first();
        if($emailCheck !=null )
        {
            // dd('yyy');
            // $m=trans( 'auth.validateEmail' );
            // session()->put('errorIdentification',  $m);
            // return redirect()->route('register',['locale'=>app()->getLocale()]);
            return 1;

        }


            // Validator::make($user,['email' => ['email','unique:users,email']],$messages)->validate();
        }
        // dd( $userName);

//        verify Email
        if($user->email)
        {
            $date=now();
        }
        else
        {
            $date=null;
        }


            $registedUser= User::create([
                'firstName'     => $userName,
                'email'    => $user->email,
                'email_verified_at'=>$date,
                'provider' => $provider,
                'provider_id' => $user->id
            ]);
//        dd($date,$registedUser);
        return $registedUser;

    }
    public function redirectToFacebook()
    {
//        dd('ddd');
        return Socialite::driver('facebook')->redirect();

    }
    public function handelFacebookCallBack()
    {
        $user=Socialite::driver('facebook')->user();
        $provider="facebook";
        // dd($user);
        $authUser=$this->findOrCreateUser($user,$provider);
        if( filter_var($authUser,FILTER_VALIDATE_INT))
        {
            $m=trans( 'auth.validateEmail' );
            session()->put('errorIdentification',  $m);
            return redirect()->route('login',['locale'=>app()->getLocale()]);
        }
        else
        {
            Auth::login($authUser, true);
            return redirect(App::getLocale().'/');
        }

    }

    public function redirectToGoogle()
    {
//        dd('ddd');
//        return Socialite::driver('google')->redirect();
        // return Socialite::driver('google')
        //     ->stateless()
        //     ->redirect();
        return Socialite::driver('google')->setScopes(['openid', 'email'])

        ->redirect();
    }
    public function handelGoogleCallBack()
    {
//        dd('test');
    //    $user=Socialite::driver('google')->stateless()->user();
//        try {
    // dd('test');
            $user = Socialite::driver('google')->stateless()->user();
            $provider="google";
            // dd($user);
            $authUser=$this->findOrCreateUser($user,$provider);
            if( filter_var($authUser,FILTER_VALIDATE_INT))
            {
                $m=trans( 'auth.validateEmail' );
                session()->put('errorIdentification',  $m);
                return redirect()->route('login',['locale'=>app()->getLocale()]);
            }
            else
            {
                Auth::login($authUser, true);
                return redirect(App::getLocale().'/');
            }

            //  dd($user);
//        } catch (\Exception $e) {
//            return redirect('/en/login')->with('error','Try after some time');
//        }
//        $existingUser = User::where('social_id', $user->id)->first();

//        dd($user);
    }


    public function showVerifyPage($lang=null , $country=null , $city=null)
    {
        // dd('test');
        return view('public.user.verify-email',compact('country','city'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
