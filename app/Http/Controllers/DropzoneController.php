<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DropzoneController extends Controller
{
    //

    public function addPersonalPhotoFromProfile(Request $request){
        $user=User::find(Auth::user()->id);
        $image=$request->file('file');
        $imageName=time().".".$image->extension();
        if( $image->storeAs('public/images/avatars',$imageName))
        {
            if($user->avatar!=null)
            {
                $oldAvatar=$user->avatar;
                Storage::delete('public/images/avatars/'.$oldAvatar);
            }
            $user->avatar=$imageName;
            $user->save();
            $output="<h6 class='alert alert-success'>".trans( 'user.ImageUpdated' )." </h6>";

        }


        return $output;

    }

    public function fetchMyPersonalImage()
    {
        $user=User::find(Auth::user()->id);
        $avatar=$user->avatar;
//        dd($avatar);

        if($avatar==null)
        {
            $output="<img class='avatar border-gray' src='".asset('/storage/images/avatars/person.png')."'/>";


        }
        else
        {
            $output="<img class='avatar border-gray' src='".asset('/storage/images/avatars/'.$avatar)."'/>";


        }

        return $output;
    }

    public function addPhotoFromAdmin(Request $request){
        $user=User::find($request->id);
        $image=$request->file('file');
        $imageName=time().".".$image->extension();
//        dd($imageName);
       if( $image->storeAs('public/images/avatars',$imageName))
       {
           if($user->avatar!=null)
           {
               $oldAvatar=$user->avatar;
               Storage::delete('public/images/avatars/'.$oldAvatar);
           }

           $user->avatar=$imageName;
           $user->save();
       }


        return response()->json(['success'=>$imageName]);

    }

    public function fetchPersonalImage($id){
//        $user=User::find($id);
//        $avatar=$user->avatar;
//        dd($avatar);
        $avatar = DB::table('users')
            ->select('avatar')
            ->where('id',$id)
            ->first()
            ->avatar;
        if($avatar==null)
        {
            $output="<img class='avatar border-gray' src='".asset('/storage/images/avatars/person.png')."'/>";


        }
        else
        {
            $output="<img class='avatar border-gray' src='".asset('/storage/images/avatars/'.$avatar)."'/>";


        }

//        $output ='<img class="avatar border-gray"
//                 src="'.asset('storage\images\avatars\'.$avatar.'")>';

        return $output;
    }
    public function addUserFiles(Request $request)
    {
//        dd($request->id);
        $image=$request->file('file');
//        foreach ($images as $image)
//        {
            $imageName=time().rand(1,1000).".".$image->extension();
//        dd($imageName);
            if( $image->storeAs('public/images/userDetails',$imageName))
            {

                UserImage::create([
                    'user_id'=>$request->id,
                    'name'=>$imageName,
                ]);
//            }




        }
        return response()->json(['success'=>$imageName]);

    }

    public function fetchUserImages($id)
    {
        $output1="";
        $images = DB::table('user_images')
            ->select('name','id')
            ->where('user_id',$id)
            ->get();
//        dd(count($images)==0);


        if(count($images)==0)
        {
            $output1="<h6 class='alert alert-info'>".trans( 'user.noImageFound' )." </h6>";


        }
        else
        {
           foreach ($images as $image)
           {
//               dd($image->id);
               $output1= $output1."<div class='card card-img-doc m-3 ' style='width: 20rem; position: relative'>
                         <button id='btn' class='btn btn-outline-warning btn-load-img'>".trans( 'user.Show_image' ) ."</button>
                         <img class='card-img-top img-doc' src='".asset('/storage/images/userDetails/'.$image->name)."' />
                           <button id='".$image->id."'  class='btn btn-outline-danger btn-delete-image-detail '>".trans( 'user.delete_image' ) ."</a>
                          </div>";
           }

        }
//        dd($output1);
        return $output1;
    }


    public function deleteImageDetail($id)
    {
        $image=UserImage::find($id);
        if($image->delete()){
            Storage::delete('public/images/userDetails/'.$image->name);
            $output1="<h6 class='alert alert-success'>".trans( 'user.ImageDelSuccess' )." </h6>";
        }
        else
        {
            $output1="<h6 class='alert alert-danger'>".trans( 'user.ImageDelFailed' )." </h6>";
        }
        return response()->json(['success'=>$output1]);
    }

}



