<?php

namespace App\Http\Middleware;

use App\Models\CityTranslation;
use Closure;
use Illuminate\Http\Request;
use  \App\Traits\WithCountryAndCity;

class EnsureCityState
{
    use WithCountryAndCity;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cityName = $request->city;
//        $lang=$request->locale;
        $countryName = $request->country;
//        dd($cityName,$lang,$countryName);
        $city = CityTranslation::with('city')
            ->where('name', $cityName)
            ->first();
        if ($city != null) {
            $cityState = $city->city->state;
            if ($cityState == 1) {
                return $next($request);

            } else {

                $city = $cityName;
                $country = $countryName;
                return response(view('public.cityNotAvailable', compact('city', 'country')));
            }
        } else {
            return redirect('/en');
        }

//
//        dd($cityState);

    }
}
