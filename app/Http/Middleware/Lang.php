<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        dd($request->locale);
//        dd($lang);
        $langueges=['en','ar','rus','ukr','fre'];
        if(!in_array($request['locale'] ,$langueges))
        {
//            dd('r');
//            $request->locale="en";
            app()->setLocale("en");
        }
        else
        {
            app()->setLocale($request['locale']);
        }

        return $next($request);
    }
}
