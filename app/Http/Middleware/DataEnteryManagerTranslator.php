<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class DataEnteryManagerTranslator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Gate::allows('is-manager')  or Gate::allows('is-data_entry_manager') or Gate::allows('is-translator') )
        {
            return $next($request);

        }
        else
        {
            return redirect('/en');
        }

    }
}
