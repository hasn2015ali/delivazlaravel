<?php

namespace App\Http\Middleware;

use App\Models\ServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UserState
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        dd(Auth::user()->status);
        $user=Auth::user();
        if($user->role_id==10 or $user->role_id==9)
        {
           $serviceProvider=ServiceProvider::select('registration_state')
               ->where('user_id',$user->id)
               ->first();
           if($serviceProvider->registration_state==0)
           {
               $lang=App::getLocale();
               return redirect()->route('RegisterISNotComplete',['locale'=>$lang]);

//               return redirect('/'.App::getLocale().'/RegisterISNotComplete');

           }
           else
           {
               if($user->status==1)
               {
                   return $next($request);

               }
               elseif($user->status==0)
               {
//            dd('gggg');
                   return redirect('/blockedUser');

               }
           }
        }
        if($user->status==1)
        {
            return $next($request);

        }
        elseif($user->status==0)
        {
//            dd('gggg');
            return redirect('/blockedUser');

        }
    }
}
