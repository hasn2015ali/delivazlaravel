<?php

namespace App\Http\Livewire\Category;

use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\ProductFilterTrans;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithLanguegesTrait;
class ProductFilterTransComponent extends Component
{
    use WithPagination ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    public $productFilterId;
    public $langs;
    public $ProductFilterEn;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $mainFilter;
    public $productTrans;
    public $transId;

    public function messages()
    {
        return [
            'trans.required' => trans( 'filter.validateTrans' ),
            'selectedLang.required' => trans( 'filter.validate_selectedLang' ),

        ];
    }
    public function submit(){
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);

        $lang=Language::find($this->selectedLang);
        $newtrans=ProductFilterTrans::create([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,
            'filter_id' => $this->mainFilter->filter_id,
            'product_filter_id' => $this->ProductFilterEn->product_filter_id,
        ]);



//        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();
        $this->emit('newProductFilterTransAdded');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restFilde(){
        $this->trans="";
        $this->selectedLang="";
    }
    public  function edit($filter)
    {
        $this->productTrans=$filter;
        $this->trans=$filter['name'];
        $this->selectedLang=$filter['lang']['id'];
        $this->selectedLangName=$filter['lang']['name'];
        $this->transId=$filter['id'];
    }

    public function update()
    {

        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);
        $lang=Language::find($this->selectedLang);
        $filter=ProductFilterTrans::find($this->productTrans['id']);
        $filter->update(['name'=>$this->trans ,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,]);
        $this->restFilde();
        $this->emit('productFilterTransUp');
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function deleteCall($id)
    {
        $this->transId=$id;
    }
    public function deleteAction()
    {
        $filter=ProductFilterTrans::find($this->transId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }


    public function mount()
    {
        $this->ProductFilterEn=ProductFilterTrans::where('product_filter_id',$this->productFilterId)
            ->where('code_lang','en')
            ->first();
//        dd($this->ProductFilterEn->filter_id);
        $this->mainFilter=filterTranslate::where('code_lang','en')
            ->where('filter_id',$this->ProductFilterEn->filter_id)
            ->first();
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');

    }
    public function render()
    {
        $translations=ProductFilterTrans::with('productFilter','lang')
            ->where('product_filter_id',$this->productFilterId)
            ->where('code_lang','!=','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.product-filter-trans-component',compact('translations'));
    }
}
