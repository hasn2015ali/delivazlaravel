<?php

namespace App\Http\Livewire\Category;

use App\Models\CountryTranslation;
use App\Models\filterTranslate;
use App\Models\ServiceProvider;
use App\Models\ServiceProviderBanner;
use App\Models\ServiceProviderDocument;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Traits\WithCountryAndCity;
use Livewire\WithPagination;
use App\Traits\WithRestaurantTrait;
use Livewire\WithFileUploads;
use App\Traits\WithUserValidationTrait;
class VendorComponent extends Component
{
    use WithPagination ,WithCountryAndCity
        ,WithRestaurantTrait, WithFileUploads, WithUserValidationTrait;
    protected $paginationTheme = 'bootstrap';
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

    //vars for WithRestaurantTrait
    public $loggedInUser;
//    public $selectedCountry=null;
//    public $selectedCity=null;

    //vars for RestaurantComponent

    public $showSelectCountry=true;
    public $name;
    public $nameEN;
    public $address;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $selectedVendorFilters=[];
    public $allVendorFilter;
    public $vendorImage;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $minimumOrderAmount;
    public $password;
    public $firstName;
    public $fatherName;
    public $lastName;
    public $type="ven";
    public $roleID=10;
    public $vendorID;
    public $vendorInCountry=false;
    public $countryID;
    public $countryName;
    public $cityID;
    public $cityName;
    public $showNewVendor=false;
    public $countOFNewVendor;
    protected $listeners = [
        'updateNewVendorCount'=>'setCountOfNewVendors',
//        'getItemByFilter'=>'render',
//        'refresh'=>'$refresh'
    ];

    public function switchState($resID)
    {
        $res=ServiceProvider::with('user')->find($resID);

        if($res->user->status==0)
        {
            $res->user->status=1;
            $res->user->save();
        }
        elseif ($res->user->status==1)
        {
            $res->user->status=0;
            $res->user->save();
        }
    }
    public function getNewVendors()
    {
        $this->cityID=null;
        $this->countryID=null;
        $this->countryName=null;
        $this->cityName=null;
        $this->showNewVendor=true;
        $this->emitTo('category.new-vendor-component', 'getVendors');
    }
    public function getVendorsInMyCity()
    {
        $this->cityID=Auth::user()->city_id;
        $this->showNewVendor=false;
    }
    public function getCities($countryID,$countryName)
    {

        $this->showNewVendor=false;
        $this->countryName=$countryName;
        $this->countryID=$countryID;
        $this->getCitiesForCountry($this->countryID);

    }
    public function setCityID()
    {
        $this->cityID=$this->selectedCity;
        $this->cityName=$this->getCityName($this->selectedCity,App::getLocale());;


    }

    public function getShopsByCity($cityID,$cityName)
    {
//        dd($cityID,$cityName);
        $this->cityID=$cityID;
        $this->cityName=$cityName;

    }
    public function messages()
    {

        return [
            'name.required' => trans( 'vendor.validateName' ),
            'email.required' => trans( 'vendor.validateEmail' ),
            'firstName.required' => trans( 'vendor.validateFirstName' ),
            'lastName.required' => trans( 'vendor.validateLastName' ),
            'selectedCity.required' => trans( 'vendor.validateCity' ),
            'selectedCountry.required' => trans( 'vendor.validateCountry' ),
            'selectedVendorFilters.size' => trans( 'vendor.validateSelectedVendorFilters' ),
            'password.required' => trans( 'user.validate_password' ),


        ];
    }

    public function submit()
    {
//        dd($this->selectedCountry,$this->selectedCity);
        $this->name= str_replace(['-','_','*'], ' ', $this->name);
//        dd($this->name);
        $this->name=trim($this->name);
        $slug= str_replace(' ', '-', $this->name);
//        dd($slug,$this->name);
        // validate from user trait
        if($this->checkServiceProviderName($this->name)==1)
        {
            return;
        }
        if($this->vendorImage)
        {
            $imageName=time().".".$this->vendorImage->extension();
            $this->vendorImage->storeAs('public/images/avatars',$imageName);
        }
        else
        {
            $imageName=null;
        }
        // validate from user trait
        if($this->checkEmail($this->email)==1)
        {
            return;
        }
        $this->validate([
            'name' => 'required',
            'selectedCity' => 'required',
            'selectedCountry' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'selectedVendorFilters' => 'array|required',
            'password' => 'required',

        ]);

        $password=Hash::make($this->password);
        $newUser=User::create([
            'firstName' => $this->firstName,
            'fatherName' => $this->fatherName,
            'lastName' => $this->lastName,
//            'avatar' => $this->filterID,
            'email' => $this->email,
            'password' => $password,
            'country_id' =>  $this->selectedCountry,
            'city_id' =>  $this->selectedCity,
            'role_id' =>  $this->roleID,
        ]);
//        dd('tst');


        $serviceProvider=ServiceProvider::create([
            'name' => $this->name,
            'nameEN' => $this->nameEN,
            'address' => $this->address,
            'minimum_order_amount' => $this->minimumOrderAmount,
            'mode' => $this->deliveryMode,
            'delivery_man_commission' => $this->deliveryManCommission,
            'pick_commission' => $this->pickCommission,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'phone3' => $this->phone3,
            'type' => $this->type,
            'user_id' => $newUser->id,
            'avatar'=>$imageName,
            'slug' => $slug,

        ]);
        $serviceProvider->filters()->attach($this->allVendorFilter);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('addNewVendor');
        $this->emit('alert',['icon'=>'success','title'=>$message]);


    }
    public function restFilde()
    {
        $this->firstName=null;
        $this->fatherName=null;
        $this->lastName=null;
        $this->email=null;
        $this->password=null;
        $this->name=null;
        $this->nameEN=null;
        $this->address=null;
        $this->minimumOrderAmount=null;
        $this->deliveryMode=null;
        $this->deliveryManCommission=null;
        $this->pickCommission=null;
        $this->phone2=null;
        $this->phone3=null;
        $this->phone1=null;
        $this->selectedVendorFilters=[];
        $this->vendorImage=null;
    }



    public function mount()
    {
        $this->getCountries();
        $this->setRoleForManager();
        $this->allVendorFilter=filterTranslate::where('code_lang', App::getLocale())
            ->where('type',2)->get();
        $this->setCountOfNewVendors();

    }
    public function setCountOfNewVendors()
    {
        $this->countOFNewVendor=$this->getNewShopsCount();

    }


    public function deleteCall($id)
    {
        $this->vendorID=$id;
    }
    public function deleteAction()
    {
        $res=ServiceProvider::find($this->vendorID);
        $userId=$res->user->id;

        $user=User::find($userId);
        $bannerIMG=ServiceProviderBanner::where('service_provider_id',$res->id)->get();
//        dd($bannerIMG);
        if($bannerIMG)
        {
            foreach ($bannerIMG as $img)
            {
                Storage::delete('public/images/sliders/vendor/'.$img->name);
            }
        }
        $documentIMG=ServiceProviderDocument::where('service_provider_id',$res->id)->get();
//dd($documentIMG);
        if($documentIMG)
        {
            foreach ($documentIMG as $img)
            {
                Storage::delete('public/images/document/vendor/'.$img->name);
            }
        }
        if($res->avatar)
        {
            Storage::delete('public/images/avatars/'.$res->avatar);

        }

        $user->delete();
        $res->filters()->detach();


        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }

    public function render()
    {
        $vendorsInCity=null;
        $selectedCityCheck=null;
        if(Auth::user()->role_id==2)
        {
            if($this->cityID==null)
            {
                $selectedCityCheck=__("vendor.selectCityCheck");
            }
            $vendorsInCity= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','users.status as userStatus','users.email as userEmail','service_providers.name',
                    'service_providers.created_at','service_providers.phone1','service_providers.address',
                    'service_providers.name','service_providers.id as serviceProviderID','service_providers.registration_state')
                ->where('service_providers.registration_state', 1)
                ->where('users.role_id', 10)
                ->where('users.city_id',$this->cityID)
                ->orderBy('service_providers.id','desc')
                ->paginate(10);
        }
        elseif (Auth::user()->role_id==8)
        {
            $vendorsInCity= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','users.status as userStatus','users.email as userEmail','service_providers.name',
                    'service_providers.created_at','service_providers.phone1',
                    'service_providers.address','service_providers.name',
                    'service_providers.id as serviceProviderID','service_providers.registration_state')
                ->where('service_providers.registration_state', 1)
                ->where('users.role_id', 10)
                ->where('users.city_id',$this->cityID)
                ->orderBy('service_providers.id','desc')
                ->paginate(10);

        }
        return view('livewire.category.vendor-component',compact('selectedCityCheck','vendorsInCity'));
    }
}
