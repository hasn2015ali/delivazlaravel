<?php

namespace App\Http\Livewire\Category;

use App\Traits\WithRestaurantTrait;
use Livewire\Component;

class NewVendorCountComponent extends Component
{
    use WithRestaurantTrait;
    public $countOFNewVendor;
    protected $listeners = [
        'updateNewVendorCount'=>'render',
        'newShopJoined'=>'render',
//        'refresh'=>'$refresh'
    ];
    public function render()
    {
//                dd('newVen');

        $this->countOFNewVendor=$this->getNewShopsCount();
//        if($this->countOFNewVendor==0)
//        {
//            $this->emitTo('category.vendor-component', 'updateNewVendorCount');
//
//        }
        return view('livewire.category.new-vendor-count-component');
    }
}
