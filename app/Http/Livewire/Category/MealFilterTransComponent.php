<?php

namespace App\Http\Livewire\Category;

use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\MealFilterTrans;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithLanguegesTrait;


class MealFilterTransComponent extends Component
{
    use WithPagination ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    public $mealFilterId;
    public $langs;
    public $mealFilterEn;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $mainFilter;
    public $mealTrans;
    public $transId;

    public function messages()
    {
        return [
            'trans.required' => trans( 'filter.validateTrans' ),
            'selectedLang.required' => trans( 'filter.validate_selectedLang' ),

        ];
    }
    public function submit(){
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);

        $lang=Language::find($this->selectedLang);
        $newtrans=MealFilterTrans::create([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,
            'filter_id' => $this->mainFilter->filter_id,
            'meal_filter_id' => $this->mealFilterEn->meal_filter_id,
        ]);



//        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();
        $this->emit('newMealFilterTransAdded');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restFilde(){
        $this->trans="";
        $this->selectedLang="";
    }
    public  function edit($filter)
    {
        $this->mealTrans=$filter;
        $this->trans=$filter['name'];
        $this->selectedLang=$filter['lang']['id'];
        $this->selectedLangName=$filter['lang']['name'];
        $this->transId=$filter['id'];
    }

    public function update()
    {

        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);
        $lang=Language::find($this->selectedLang);
        $filter=MealFilterTrans::find($this->mealTrans['id']);
        $filter->update(['name'=>$this->trans ,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,]);
        $this->restFilde();
        $this->emit('mealFilterTransUp');
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function deleteCall($id)
    {
        $this->transId=$id;
    }
    public function deleteAction()
    {
        $filter=MealFilterTrans::find($this->transId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }


    public function mount()
    {
        $this->mealFilterEn=MealFilterTrans::where('meal_filter_id',$this->mealFilterId)
            ->where('code_lang','en')
            ->first();
//        dd($this->mealFilterEn->filter_id);
        $this->mainFilter=filterTranslate::where('code_lang','en')
            ->where('filter_id',$this->mealFilterEn->filter_id)
            ->first();
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');

    }
    public function render()
    {
        $translations=MealFilterTrans::with('mealFilter','lang')
            ->where('meal_filter_id',$this->mealFilterId)
            ->where('code_lang','!=','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.meal-filter-trans-component',compact('translations'));
    }
}
