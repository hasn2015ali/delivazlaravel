<?php

namespace App\Http\Livewire\Category;

use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\MealFilter;
use App\Models\MealFilterTrans;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithLanguegesTrait;
class MealFilterComponent extends Component
{
    use WithPagination ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    public $filterID;
    public $type;
    public $translations;
    public $name;
    public $mealFilter;
    public $mealFilterId;
    public $langs;
    public $languegeCount;
    public $mainFilter;
    public $FilterMenu;

    public function messages()
    {
        return [
            'name.required' => trans( 'filter.validate' ),
        ];
    }
    public function submit(){
        $this->validate([
            'name' => 'required',
        ]);

        $en_lang=Language::where('code','en')->first();
        $mealFilter= MealFilter::create([
            'filter_id' => $this->filterID,
        ]);
        $filter_trans=MealFilterTrans::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'filter_id' => $this->filterID,
            'meal_filter_id' => $mealFilter->id,
        ]);

        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newMealFilterAdded');
    }
    public function restFilde()
    {
        $this->name="";
    }
    public  function edit($filter)
    {
        $this->mealFilter=$filter;
        $this->name=$filter['name'];
    }

    public function update(){
        $this->validate([
            'name' => 'required',
        ]);
        $filter=MealFilterTrans::find($this->mealFilter['id']);
        $filter->update(['name'=>$this->name]);
        $message=trans( 'masterControl.updated_successfully' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('filterUp');
    }
    public function deleteCall($id){
        $this->mealFilterId=$id;
    }
    public function deleteAction()
    {
        $filter=MealFilter::find($this->mealFilterId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }


    public function showTranslation($id)
    {
        $this->translations=MealFilterTrans::where('meal_filter_id',$id)->get('code_lang');
//        dd( $this->translations);

    }

    public function mount()
    {
//        dd('tst');
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->languegeCount=$this->getAllLanguegesCount();
        $this->mainFilter=filterTranslate::where('code_lang','en')
            ->where('filter_id',$this->filterID)
            ->first();
//        dd( $this->filterID);
        $this->FilterMenu=filterTranslate::where('code_lang','en')
            ->where('type',1)
            ->get();
//        dd($this->FilterMenu,$this->type);

    }
    public function render()
    {
        $filters=MealFilterTrans::where('filter_id',$this->filterID)
            ->where('code_lang','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.meal-filter-component',compact('filters'));
    }
}
