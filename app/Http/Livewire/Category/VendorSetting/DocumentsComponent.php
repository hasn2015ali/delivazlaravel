<?php

namespace App\Http\Livewire\Category\VendorSetting;

use App\Models\ServiceProviderDocument;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class DocumentsComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $vendorID;
    public $vendorDocument;
    public $documentId;


    public function messages()
    {

        return [
            'vendorDocument.required' => trans( 'vendor.validateVendorDocument' ),
        ];
    }

    public function submit()
    {

        $this->validate([
            'vendorDocument' => 'required',
        ]);
        if($this->vendorDocument)
        {
            $imageName=time().rand(1,100).".".$this->vendorDocument->extension();
            $this->vendorDocument->storeAs('public/images/document/vendor',$imageName);
            $newDocument= ServiceProviderDocument::create([
                'name' => $imageName,
                'service_provider_id' => $this->vendorID,
            ]);
        }

        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->vendorDocument="";

    }


    public function deleteCall($id){
        $this->documentId=$id;
    }
    public function deleteAction()
    {
        $document=ServiceProviderDocument::find($this->documentId);
        if(Storage::delete('public/images/document/vendor/'.$document->name))
        {
            $document->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction3');
        }


    }

    public function render()
    {
        $documents=ServiceProviderDocument::where('service_provider_id',$this->vendorID)
            ->orderBy( 'id', 'desc' )
            ->paginate(2);
        return view('livewire.category.vendor-setting.documents-component',compact('documents'));
    }
}
