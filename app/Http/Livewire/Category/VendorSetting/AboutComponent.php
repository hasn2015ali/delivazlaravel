<?php

namespace App\Http\Livewire\Category\VendorSetting;

use App\Models\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Traits\WithCountryAndCity;
use App\Traits\WithRestaurantTrait;
use Livewire\WithFileUploads;
use App\Traits\WithUserValidationTrait;
class AboutComponent extends Component
{

    use WithCountryAndCity ,WithRestaurantTrait, WithFileUploads, WithUserValidationTrait;
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;
    //vars for WithRestaurantTrait
    public $loggedInUser;
//    public $selectedCountry=null;
//    public $selectedCity=null;

    //vars for AboutComponent
    public $vendorID;
    public $showSelectCountry=true;
    public $isRecommended;
    public $vendorImage;
    public $vendorImageOld;
    public $name;
    public $oldName;
    public $nameEN;
    public $address;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $minimumOrderAmount;
    public $password;
    public $firstName;
    public $fatherName;
    public $lastName;
    public $type="ven";
    public $roleID=10;
    public $vendor;
    public $wallet;
    public $providerURL;
    public $ourCommission;

    public function mount()
    {
        $this->setRoleForManager();
        $this->getCountries();
        $this->vendor=ServiceProvider::with('user')->find($this->vendorID);
        $this->name=$this->vendor->name;
        $this->oldName=$this->vendor->name;
        $this->nameEN=$this->vendor->nameEN;
        $this->email=$this->vendor->user->email;
        $this->phone1=$this->vendor->phone1;
        $this->phone2=$this->vendor->phone2;
        $this->phone3=$this->vendor->phone3;
        $this->address=$this->vendor->address;
        $this->vendorImageOld=$this->vendor->avatar;
        $this->deliveryMode=$this->vendor->mode;
        $this->deliveryManCommission=$this->vendor->delivery_man_commission;
        $this->pickCommission=$this->vendor->pick_commission;
        $this->ourCommission=$this->vendor->delivaz_commission;
        $this->minimumOrderAmount=$this->vendor->minimum_order_amount;
        $this->firstName=$this->vendor->user->firstName;
        $this->fatherName=$this->vendor->user->fatherName;
        $this->lastName=$this->vendor->user->lastName;
        $this->isRecommended=$this->vendor->isRecommended;
        $this->wallet=$this->vendor->user->balance;
        $this->providerURL=$this->vendor->provider_url;
//        $this->vendorImage=$this->vendor->user->balance;

//        $this->selectedCity=$this->vendor->user->city_id;
//        $this->selectedCountry=$this->vendor->user->country_id;
        if($this->vendorImageOld==null)
        {
            $this->vendorImageOld="person.png";
        }
        if($this->wallet==null)
        {
            $this->wallet=0;
        }





    }
    public function messages()
    {

        return [
            'name.required' => trans( 'vendor.validateName' ),
            'email.required' => trans( 'vendor.validateEmail' ),
            'firstName.required' => trans( 'restaurant.validateFirstName' ),
            'lastName.required' => trans( 'restaurant.validateLastName' ),
//            'selectedCity.required' => trans( 'vendor.validateCity' ),
//            'selectedCountry.required' => trans( 'vendor.validateCountry' ),
            'selectedVendorFilters.size' => trans( 'vendor.validateSelectedVendorFilters' ),
//            'password.required' => trans( 'user.validate_password' ),


        ];
    }

    public function submit()
    {

        $this->name= str_replace(['-','_','*'], ' ', $this->name);
        $this->name=trim($this->name);
        $slug= str_replace(' ', '-', $this->name);
        if( $this->oldName!= $this->name)
        {
            if($this->checkServiceProviderName($this->name)==1)
            {
                return;
            }
        }

        if($this->email!=$this->vendor->user->email)
        {
            if($this->checkEmail($this->email)==1)
            {
                return;
            }
        }
        if($this->password==null)
        {
            $newPassword=$this->vendor->user->password;
        }
        else
        {
            $newPassword=bcrypt($this->password);
        }
//        dd($newPassword);

        if($this->vendorImage)
        {
            if($this->vendor->avatar==null)
            {
                $imageName=time().".".$this->vendorImage->extension();
                $this->vendorImage->storeAs('public/images/avatars',$imageName);
            }
            else
            {
                Storage::delete('public/images/avatars/'.$this->vendor->avatar);
                $imageName=time().".".$this->vendorImage->extension();
                $this->vendorImage->storeAs('public/images/avatars',$imageName);
            }

        }
        else
        {
            $imageName=$this->vendor->avatar;
        }

        $this->validate([
            'name' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
//            'password' => 'required',

        ]);
//        dd( $this->vendor->user);

        $this->vendor->user->update([
            'firstName' => $this->firstName,
            'fatherName' => $this->fatherName,
            'lastName' => $this->lastName,
//            'avatar' => $this->filterID,
            'email' => $this->email,
            'password' => $newPassword,
        ]);
//        dd($imageName);

       $this->vendor->name= $this->name;
        $this->vendor->nameEN=$this->nameEN;
        $this->vendor->phone1=$this->phone1;
        $this->vendor->phone2=$this->phone2;
        $this->vendor->phone3=$this->phone3;
        $this->vendor->address=$this->address;
        $this->vendor->avatar=$imageName;
        $this->vendor->mode=$this->deliveryMode;
        $this->vendor->delivery_man_commission=$this->deliveryManCommission;
        $this->vendor->pick_commission=$this->pickCommission;
        $this->vendor->minimum_order_amount= $this->minimumOrderAmount;
        $this->vendor->provider_url= $this->providerURL;
        $this->vendor->slug= $slug;
        $this->vendor->delivaz_commission=$this->ourCommission;

        $this->vendor->save();
//        $this->isRecommended=$this->vendor->isRecommended;
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);


    }
    public function changeRecommendedState()
    {
//        dd($this->isRecommended);
        if($this->isRecommended==true)
        {
            $this->vendor->isRecommended=1;
            $this->vendor->save();
        }
        elseif($this->isRecommended==false)
        {
            $this->vendor->isRecommended=0;
            $this->vendor->save();
        }
    }
    public function getCities()
    {
        $this->getCitiesForCountry($this->selectedCountry);
    }
    public function render()
    {
        return view('livewire.category.vendor-setting.about-component');
    }
}
