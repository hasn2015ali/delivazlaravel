<?php

namespace App\Http\Livewire\Category\VendorSetting;

use App\Models\Language;
use App\Models\ProductAdditions;
use App\Models\ProductFilterTrans;
use App\Models\Product;
use App\Models\ProductPhotos;
use App\Models\ProductSizes;
use App\Models\ProductTranslation;
use App\Models\ServiceProvider;
use App\Models\serviceProviderSizes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
class ProductComponent extends Component
{
    use WithPagination ,WithFileUploads ;
    protected $paginationTheme = 'bootstrap';

    public $allProductFilters=[];
    public $vendorID;
    public $languageSelected;
    public $photo;
    public $name;
    public $content;
    public $price;
    public $selectedProductFilters=[];
    public $updatedProductFilters=[];
    public $vendor;
    public $product;
    public $langCode;
    public $photoOld;
    public $productUp;
    public $productID;

    public $productByFiltersShow=false;
    public $ProductFilterID;
    public $ProductFilterName;
    public $size;
    public $allSizes;
    public $productSizeSelected;
    public $sizePrice;
    public $allSizesForProduct;
    public $weight;

    public $AdditionName;
    public $AdditionContent;
    public $AdditionPhoto;
    public $AdditionPrice;
    public $AdditionWeight;
    public $allAdditionForProduct;
    public $images= [];
    public $oldImages=[];
    public $sizeWeight;
    public $sizeContent;



    public function deleteProductPhoto($id)
    {
        $image=ProductPhotos::find($id);
        if(Storage::delete('public/images/product/'.$image->name))
        {
            $image->delete();
        }

        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->oldImages=ProductPhotos::where('product_id',$this->productID)->get();

    }
    public function AddPhotos()
    {
        $this->validate([
            'images.*' => 'image|required'
        ]);
//        dd($this->productID);
        if(count($this->images)!=0)
        {
            foreach ($this->images as $image)
            {
                $imageName=time().rand(1,1000).".".$image->extension();
                $image->storeAs('public/images/product',$imageName);
                ProductPhotos::create([
                    'product_id'=>$this->productID,
                    'name'=>$imageName,
                ]);
            }
            $message=trans( 'masterControl.successfully_Added' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->oldImages=ProductPhotos::where('product_id',$this->productID)->get();

        }
        else
        {
            $message=trans( 'vendor.imagesEmpty' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }

        $this->images=[];
//        dd($this->images);
    }
    public function editProductAddition($productID)
    {
        $this->productID=$productID;
        $this->allAdditionForProduct=ProductAdditions::where('product_id',$productID)->get();
        $this->product=Product::find($productID);

    }
    public function deleteProductAddition($additionID)
    {
        $addition=ProductAdditions::find($additionID);
        $addition->delete();
        $this->allAdditionForProduct=ProductAdditions::where('product_id',$this->productID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function submitAddition()
    {

        $this->validate([
            'AdditionName' => 'required',
            'AdditionPrice' => 'required',
        ]);
        if($this->AdditionPhoto)
        {
            $imageName=time().rand(1,100).".".$this->AdditionPhoto->extension();
            $this->AdditionPhoto->storeAs('public/images/product/addition',$imageName);
        }
        else
        {
            $imageName=null;
        }
        $addition=ProductAdditions::create([
            'price' => $this->AdditionPrice,
            'name' => $this->AdditionName,
            'content' => $this->AdditionContent,
            'weight' => $this->AdditionWeight,
            'photo' => $imageName,
            'product_id' => $this->productID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restAdditionFilde();
        $this->allAdditionForProduct->push($addition);
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restAdditionFilde()
    {
        $this->AdditionPrice=null;
        $this->AdditionName=null;
        $this->AdditionContent=null;
        $this->AdditionWeight=null;
        $this->AdditionPhoto=null;

    }
    public function editProductSize($productID)
    {
        $this->productID=$productID;
        $this->allSizesForProduct=ProductSizes::where('product_id',$productID)->get();
        $this->product=Product::find($productID);

    }
    public function deleteProductSize($sizeID)
    {
        $size=ProductSizes::find($sizeID);
        $size->delete();
        $this->allSizesForProduct=ProductSizes::where('product_id',$this->productID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function submitProductSize()
    {
        $this->validate([
            'productSizeSelected' => 'required',
            'sizePrice' => 'required',
        ]);
        $size=ProductSizes::create([
            'price' => $this->sizePrice,
            'size' => $this->productSizeSelected,
            'weight' => $this->sizeWeight,
            'content' => $this->sizeContent,
            'product_id' => $this->productID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restSizeFilde();
        $this->allSizesForProduct->push($size);
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }

    public function submitSize()
    {
        $this->validate([
            'size' => 'required',
        ]);

        $size= serviceProviderSizes::create([
            'name' => $this->size,
            'service_provider_id' => $this->vendorID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restSizeFilde();
        $this->allSizes->push($size);
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function deleteSize($sizeID)
    {
        $size=serviceProviderSizes::find($sizeID);
//        dd($size->name);
//        $size->delete();
        DB::table('products')
            ->where('size', $size->name)
            ->update(['size' => null]);
        DB::table('product_sizes')->where('size', $size->name)->delete();
        $size->delete();
        $this->allSizes=serviceProviderSizes::where('service_provider_id',$this->vendorID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restSizeFilde()
    {
        $this->size=null;
        $this->sizePrice=null;
        $this->productSizeSelected=null;
    }


    public function getProductForThisFilter($filterID)
    {
//        dd('gg');

        if($filterID==0)
        {
            $this->productByFiltersShow=false;
            $this->ProductFilterName=trans('vendor.allProductsFilters');
        }
        else{
            $this->productByFiltersShow=true;
            $this->ProductFilterID=$filterID;
            $this->ProductFilterName=ProductFilterTrans::where('product_filter_id',$filterID)
                ->where('code_lang',App::getlocale())
                ->first()->name;
        }

//        dd( $this->ProductFilterName);
    }


    public function messages()
    {
        return [
            'name.required' => trans( 'vendor.validateProductName' ),
            'price.required' => trans( 'vendor.validateProductPrice' ),
            'selectedProductFilters.required' => trans( 'vendor.validateProductFilter' ),
            'updatedProductFilters.required' => trans( 'vendor.validateProductFilter' ),
            'size.required' => trans( 'vendor.validateSize' ),
            'productSizeSelected.required' => trans( 'vendor.validateProductSizeSelected' ),
            'sizePrice.required' => trans( 'vendor.validateSizePrice' ),
            'AdditionName.required' => trans( 'vendor.validateAdditionName' ),
            'AdditionPrice.required' => trans( 'vendor.validateAdditionPrice' ),

        ];
    }
    public function submit()
    {
        $this->validate([
            'name' => 'required',
            'price' => 'required',
            'selectedProductFilters' => 'array|required',

        ]);
        $en_lang=Language::where('code','en')->first();
        if($this->photo)
        {
            $imageName=time().rand(1,100).".".$this->photo->extension();
            $this->photo->storeAs('public/images/product',$imageName);
        }
        else
        {
            $imageName=null;
        }

        $product= Product::create([
            'photo' => $imageName,
            'price' => $this->price,
            'service_provider_id' => $this->vendorID,
        ]);
        $product_trans=ProductTranslation::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'product_id' => $product->id,
            'content' => $this->content,
            'service_provider_id' => $this->vendorID,
        ]);
        $product->filters()->attach($this->selectedProductFilters);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newProductAdded');
    }
    public function restFilde()
    {
        $this->name="";
        $this->content="";
        $this->photo="";
        $this->price="";
        $this->weight="";
        $this->productSizeSelected="";
        $this->selectedProductFilters=[];
    }

    public function mount()
    {
        $this->langCode=App::getlocale();
        $this->languageSelected=Language::where('code',$this->langCode)->first();
        $this->vendor=ServiceProvider::with('filters')->find($this->vendorID);
//        dd($this->vendor->filters);
        foreach ($this->vendor->filters as $vendorFilter)
        {
            $productFilterForThisVendorFilter=ProductFilterTrans::where('lang_id', $this->languageSelected->id)
                ->where('filter_id',$vendorFilter->id)->get()->toArray();
            array_push($this->allProductFilters,$productFilterForThisVendorFilter);

        }
        $this->allSizes=serviceProviderSizes::where('service_provider_id',$this->vendorID)->get();

//        dd($this->allProductFilters);

//        $this->allProductFilters=ProductFilterTrans::where('lang_id', $this->languageSelected->id)
//            ->get();
    }

    public function editProductFilter($productID)
    {
        $this->product=Product::with('filters')->find($productID);

//        dd(  $this->product->id);
    }
    public function UpdateProductFilters()
    {
        $this->validate([
            'updatedProductFilters' => 'array|required',
        ]);
        $this->product->filters()->detach();
        $this->product->filters()->attach($this->updatedProductFilters);
        $this->product=Product::with('filters')->find($this->product->id);
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->updatedProductFilters=[];
    }
    public function editProduct($id)
    {
        $this->productID=$id;
//        dd($this->productID);
        $this->oldImages=ProductPhotos::where('product_id',$id)->get();

        $this->productUp=ProductTranslation::with('product')->where('product_id',$id)
            ->where('code_lang','en')->first();
//        dd($this->product);
        $this->name=$this->productUp->name;
        $this->content=$this->productUp->content;
        $this->price=$this->productUp->product->price;
        $this->photoOld=$this->productUp->product->photo;
        $this->weight=$this->productUp->product->weight;
        $this->productSizeSelected=$this->productUp->product->size;

//        $this->name=$this->product->translation
    }

    public function updateProductDetails()
    {

        $this->validate([
            'name' => 'required',
            'price' => 'required',
        ]);

//        dd("dd");
        if($this->photo)
        {
            if($this->productUp->product->photo==null)
            {
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/product',$imageName);
            }
            else
            {
                Storage::delete('public/images/product/'.$this->productUp->product->photo);
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/product',$imageName);
            }

        }
        else
        {
            $imageName=$this->productUp->product->photo;
        }


        $this->productUp->name=$this->name;
        $this->productUp->content=$this->content;
        $this->productUp->save();
        $product=Product::find($this->productUp->product_id);

//            $this->productUp->product->update([
//                'photo' => $imageName,
//                'price' => $this->price,
//                'time_hour' => $this->productHour,
//                'time_minutes' => $this->productMinutes,
//            ]);

        $product->price=$this->price;
        $product->photo=$imageName;
        $product->weight=$this->weight ;
        $product->size=$this->productSizeSelected;
        $product->save();

//        dd($this->productUp->save());
        $this->restFilde();
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newProductUpdated');
    }

    public function deleteCall($id){
        $this->productID=$id;
    }
    public function deleteAction()
    {
        $product=Product::find($this->productID);
        $product->delete();
        $product->filters()->detach();
        if($product->photo!=null)
        {
            Storage::delete('public/images/product/'.$product->photo);
        }

        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }
    public function render()
    {
        $products=null;
        $productByFilters=null;
        if(!$this->productByFiltersShow)
        {
            $products=ProductTranslation::with('product')
                ->where('lang_id', $this->languageSelected->id)
                ->where('service_provider_id',$this->vendorID)
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
//            dd('gg');
            $productByFilters = Product::with('translation')->whereHas('filters', function ($query) {
                $query->where('product_filters.id', $this->ProductFilterID);
            })  ->where('service_provider_id',$this->vendorID)
                ->orderBy('id','desc')
                ->paginate(10);

        }
        return view('livewire.category.vendor-setting.product-component',compact('products','productByFilters'));
    }
}
