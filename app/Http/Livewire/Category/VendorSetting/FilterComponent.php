<?php

namespace App\Http\Livewire\Category\VendorSetting;

use App\Models\filterTranslate;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class FilterComponent extends Component
{
    public $vendorID;
    public $vendor;
    public $allVendorFilter;
    public $selectedFiltersForVendor=[];
    public $newSelectedFilters=[];
    public $langCode;
    public function messages()
    {


        return [
            'newSelectedFilters.required' => trans( 'vendor.validateSelectedVendorFilters' ),

        ];
    }

    public function mount()
    {
        $this->langCode=App::getlocale();
        $this->vendor=ServiceProvider::with('filters','user')->find($this->vendorID);
        $this->selectedFiltersForVendor=$this->vendor->filters;
        $this->allVendorFilter=filterTranslate::where('type',2)->get();

    }
    public function submit()
    {
        $this->validate([
            'newSelectedFilters' => 'required',
        ]);
        $this->vendor->filters()->detach();
        $this->vendor->filters()->attach($this->newSelectedFilters);
        $this->selectedFiltersForVendor=ServiceProvider::with('filters')
            ->find($this->vendorID)->filters;
        $this->newSelectedFilters=[];
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
//        session()->put('vFilterTabActive', 'banners');
//        $locale=App::getLocale();
//        return redirect($locale.'/vendorSetting/'.$this->vendorID);
    }

    public function render()
    {
        return view('livewire.category.vendor-setting.filter-component');
    }
}
