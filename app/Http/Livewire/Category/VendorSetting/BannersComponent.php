<?php

namespace App\Http\Livewire\Category\VendorSetting;

use App\Models\ServiceProviderBanner;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class BannersComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $vendorID;
    public $vendorBanner;
    public $bannerId;


    public function messages()
    {

        return [
            'vendorBanner.required' => trans( 'vendor.validateVendorBanner' ),
        ];
    }

    public function submit()
    {

        $this->validate([
            'vendorBanner' => 'required',
        ]);
        if($this->vendorBanner)
        {
            $imageName=time().rand(1,100).".".$this->vendorBanner->extension();
            $this->vendorBanner->storeAs('public/images/sliders/vendor',$imageName);
            $newBanner= ServiceProviderBanner::create([
                'name' => $imageName,
                'state' => 0,
                'service_provider_id' => $this->vendorID,
            ]);
        }

        $locale=App::getLocale();
        session()->put('vTabActive', 'banners');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        return redirect($locale.'/shopBanners/'.$this->vendorID);

    }
    public function mount()
    {
//        $this
    }
    public function changeBannerState($id)
    {
//        dd('tst');
        $banner=ServiceProviderBanner::find($id);
        if($banner->state==0)
        {
            $banner->state=1;
            $banner->save();
        }
        elseif($banner->state==1)
        {
            $banner->state=0;
            $banner->save();
        }
    }

    public function deleteCall($id){
        $this->bannerId=$id;
    }
    public function deleteAction()
    {
        $banner=ServiceProviderBanner::find($this->bannerId);
        if(Storage::delete('public/images/sliders/vendor/'.$banner->name))
        {
            $banner->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction2');
        }

        $locale=App::getLocale();
        session()->put('vTabActive', 'banners');
        return redirect($locale.'/shopBanners/'.$this->vendorID);
    }

    public function render()
    {
        $banners=ServiceProviderBanner::where('service_provider_id',$this->vendorID)
            ->orderBy( 'id', 'desc' )
            ->paginate(2);
        return view('livewire.category.vendor-setting.banners-component',compact('banners'));
    }
}
