<?php

namespace App\Http\Livewire\Category;

use App\Models\CityTranslation;
use App\Models\CountryTranslation;
use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\ServiceProvider;
use App\Models\ServiceProviderBanner;
use App\Models\ServiceProviderDocument;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Traits\WithCountryAndCity;
use Livewire\WithPagination;
use App\Traits\WithRestaurantTrait;
use Livewire\WithFileUploads;
use App\Traits\WithUserValidationTrait;
use App\Traits\WithImageValidationTrait;
class RestaurantComponent extends Component
{
    use WithPagination ,WithCountryAndCity ,WithUserValidationTrait
        ,WithRestaurantTrait, WithFileUploads, WithImageValidationTrait;
    protected $paginationTheme = 'bootstrap';
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

    //vars for WithRestaurantTrait
    public $loggedInUser;
//    public $selectedCountry=null;
//    public $selectedCity=null;

    //vars for RestaurantComponent

    public $showSelectCountry=true;
    public $name;
    public $nameEN;
    public $address;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $selectedRestaurantFilters=[];
    public $allRestaurantFilter;
    public $restaurantImage;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $minimumOrderAmount;
    public $password;
    public $firstName;
    public $fatherName;
    public $lastName;
    public $type="res";
    public $roleID=9;
    public $restaurantID;
    public $restaurantInCountry=false;
    public $countryID;
    public $countryName;
    public $cityName;
    public $cityID;
    public $countOFNewRestaurant;
    public $showNewRestaurant=false;

    protected $listeners = [
        'updateNewRestaurantCount'=>'setCountOfNewRestaurants',
//        'getItemByFilter'=>'render',
//        'refresh'=>'$refresh'
    ];


    public function switchState($resID)
    {
        $res=ServiceProvider::with('user')->find($resID);

                  if($res->user->status==0)
                  {
                      $res->user->status=1;
                      $res->user->save();
                  }
                  elseif ($res->user->status==1)
                  {
                      $res->user->status=0;
                      $res->user->save();
                  }
    }
    public function getNewRestaurants()
    {
        $this->cityID=null;
        $this->countryID=null;
        $this->countryName=null;
        $this->cityName=null;
        $this->showNewRestaurant=true;
        $this->emitTo('category.new-restuarnt-component', 'getRestaurants');
    }
    public function getRestaurantsInMyCity()
    {
        $this->cityID=Auth::user()->city_id;
        $this->showNewRestaurant=false;
    }
    public function getCities($countryID,$countryName)
    {

        $this->showNewRestaurant=false;
        $this->countryName=$countryName;
        $this->countryID=$countryID;
        $this->getCitiesForCountry($this->countryID);

    }
    public function setCityID()
    {
        $this->cityID=$this->selectedCity;
        $this->cityName=$this->getCityName($this->selectedCity,App::getLocale());


    }

    public function getRestaurantsByCity($cityID,$cityName)
    {
//        dd($cityID,$cityName);
        $this->cityID=$cityID;
        $this->cityName=$cityName;

    }

    public function messages()
    {


        return [

            'name.required' => trans( 'restaurant.validateName' ),
            'email.required' => trans( 'restaurant.validateEmail' ),
            'firstName.required' => trans( 'restaurant.validateFirstName' ),
            'lastName.required' => trans( 'restaurant.validateLastName' ),
            'selectedCity.required' => trans( 'restaurant.validateCity' ),
            'selectedCountry.required' => trans( 'restaurant.validateCountry' ),
            'selectedRestaurantFilters.required' => trans( 'restaurant.validateSelectedRestaurantFilters' ),
            'password.required' => trans( 'user.validate_password' ),
        ];
    }

    public function submit()
    {
        $this->name= str_replace(['-','_','*'], ' ', $this->name);
        $this->name=trim($this->name);
        $slug= str_replace(' ', '-', $this->name);

//        dd($slug);
        // validate from user trait
        if($this->checkServiceProviderName($this->name)==1)
        {
            return;
        }
        if($this->checkEmail($this->email)==1)
        {
            return;
        }
      if($this->restaurantImage)
        {
            $imageName=time().".".$this->restaurantImage->extension();
            $this->restaurantImage->storeAs('public/images/avatars',$imageName);
        }
        else
        {
            $imageName=null;
        }

        $this->validate([
            'name' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'selectedCity' => 'required',
            'selectedCountry' => 'required',
            'email' => 'required|email',
            'selectedRestaurantFilters' => 'array|required',
            'password' => 'required',
//            'restaurantImage' => 'required|mimes:csv,txt|size:1024',
//            'restaurantImage' => 'mimes:png,gif,jpg,PNG,Gif,jpeg,JPG,JPEG,GIF',

        ]);

        $password=Hash::make($this->password);
        $newUser=User::create([
            'firstName' => $this->firstName,
            'fatherName' => $this->fatherName,
            'lastName' => $this->lastName,
//            'avatar' => $this->filterID,
            'email' => $this->email,
            'password' => $password,
            'country_id' =>  $this->selectedCountry,
            'city_id' =>  $this->selectedCity,
            'role_id' =>  $this->roleID,
        ]);

        $serviceProvider=ServiceProvider::create([
            'name' => $this->name,
            'nameEN' => $this->nameEN,
            'address' => $this->address,
            'minimum_order_amount' => $this->minimumOrderAmount,
            'mode' => $this->deliveryMode,
            'delivery_man_commission' => $this->deliveryManCommission,
            'pick_commission' => $this->pickCommission,
            'phone1' => $this->phone1,
            'phone2' => $this->phone2,
            'phone3' => $this->phone3,
            'type' => $this->type,
            'user_id' => $newUser->id,
            'avatar'=>$imageName,
            'slug' => $slug,

        ]);
        $serviceProvider->filters()->attach($this->selectedRestaurantFilters);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('addNewRestaurant');
        $this->emit('alert',['icon'=>'success','title'=>$message]);


    }
    public function restFilde()
    {
        $this->firstName=null;
        $this->fatherName=null;
        $this->lastName=null;
        $this->email=null;
        $this->password=null;
        $this->name=null;
        $this->nameEN=null;
        $this->address=null;
        $this->minimumOrderAmount=null;
        $this->deliveryMode=null;
        $this->deliveryManCommission=null;
        $this->pickCommission=null;
        $this->phone2=null;
        $this->phone3=null;
        $this->phone1=null;
        $this->selectedRestaurantFilters=[];
        $this->restaurantImage=null;
    }

    public function mount()
    {
        $this->getCountries();

        $this->setRoleForManager();
        $this->allRestaurantFilter=filterTranslate::where('code_lang', App::getLocale())
            ->where('type',1)->get();

        $this->setCountOfNewRestaurants();
    }
    public function setCountOfNewRestaurants()
    {
        $this->countOFNewRestaurant=$this->getNewRestaurantCount();

//        dd($this->countOFNewRestaurant);
    }

//    public function getCities()
//    {
//        $this->getCitiesForCountry($this->selectedCountry);
//    }
    public function deleteCall($id){
        $this->restaurantID=$id;
    }
    public function deleteAction()
    {
        $res=ServiceProvider::find($this->restaurantID);
        $userId=$res->user->id;

        $user=User::find($userId);
        $bannerIMG=ServiceProviderBanner::where('service_provider_id',$res->id)->get();
//        dd($bannerIMG);
     if($bannerIMG)
     {
         foreach ($bannerIMG as $img)
         {
             Storage::delete('public/images/sliders/restaurant/'.$img->name);
         }
     }
        $documentIMG=ServiceProviderDocument::where('service_provider_id',$res->id)->get();
//dd($documentIMG);
     if($documentIMG)
     {
         foreach ($documentIMG as $img)
         {
             Storage::delete('public/images/document/restaurant/'.$img->name);
         }
     }
     if($res->avatar)
     {
         Storage::delete('public/images/avatars/'.$res->avatar);

     }

        $user->delete();
        $res->filters()->detach();


        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }
    public function render()
    {
        $restaurantsInCity=null;
        $selectedCityCheck=null;
        if(Auth::user()->role_id==2)
        {
            if($this->cityID==null)
            {
                $selectedCityCheck=__("restaurant.selectCityCheck");
            }
            $restaurantsInCity= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','users.status as userStatus','users.email as userEmail','service_providers.name',
                    'service_providers.created_at','service_providers.phone1'  ,'service_providers.name',
                    'service_providers.address','service_providers.id as serviceProviderID','service_providers.registration_state')
                ->where('service_providers.registration_state', 1)
                ->where('users.role_id', 9)
                ->where('users.city_id',$this->cityID)
                ->orderBy('service_providers.id','desc')
                ->paginate(10);
        }
        elseif (Auth::user()->role_id==8)
        {
//            $this->cityID=Auth::user()->city_id;
            $restaurantsInCity= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','users.status as userStatus','users.email as userEmail','service_providers.name',
                    'service_providers.created_at','service_providers.phone1','service_providers.address',
                    'service_providers.name','service_providers.id as serviceProviderID','service_providers.registration_state')
                ->where('service_providers.registration_state', 1)
                ->where('users.role_id', 9)
                ->where('users.city_id',$this->cityID)
                ->orderBy('service_providers.id','desc')
                ->paginate(10);

        }

        return view('livewire.category.restaurant-component',compact('restaurantsInCity','selectedCityCheck'));
    }
}
