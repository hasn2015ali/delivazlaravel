<?php

namespace App\Http\Livewire\Category;

use App\Models\filter;
use App\Models\filterTranslate;
use App\Models\Language;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithLanguegesTrait;
class FilterComponent extends Component
{
    use WithPagination ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    public $type;
    public $langs;
    public $languegeCount;
    public $name;
    public $filter;
    public $filterId;
    public $translations;


    public function messages()
    {
        return [
            'name.required' => trans( 'filter.validate' ),
        ];
    }
    public function submit(){
        $this->validate([
            'name' => 'required',
        ]);

        $en_lang=Language::where('code','en')->first();
        $filter= filter::create([
            'type' => $this->type,
        ]);
        $filter_trans=filterTranslate::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'filter_id' => $filter->id,
            'type' => $this->type,
        ]);

        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newFilterAdded');
    }
    public function restFilde()
    {
        $this->name="";
    }
    public  function edit($filter)
    {
        $this->filter=$filter;
        $this->name=$filter['name'];
    }

    public function update(){
        $this->validate([
            'name' => 'required',
        ]);
        $filter=filterTranslate::find($this->filter['id']);
        $filter->update(['name'=>$this->name]);
        $message=trans( 'masterControl.updated_successfully' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('filterUp');
    }
    public function deleteCall($id){
        $this->filterId=$id;
    }
    public function deleteAction()
    {
        $filter=filter::find($this->filterId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }
    public function showTranslation($id)
    {
//        dd($id);
        $this->translations=filterTranslate::where('filter_id',$id)->get('code_lang');

    }

    public function mount(){

        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->languegeCount=$this->getAllLanguegesCount();

    }

    public function render()
    {
        $filters=filterTranslate::where('type',$this->type)
            ->where('code_lang','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.filter-component',compact('filters'));
    }
}
