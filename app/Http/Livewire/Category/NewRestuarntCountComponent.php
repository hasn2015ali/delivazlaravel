<?php

namespace App\Http\Livewire\Category;

use App\Traits\WithRestaurantTrait;
use Livewire\Component;

class NewRestuarntCountComponent extends Component
{
    use WithRestaurantTrait;
    public $countOFNewRestaurant;
    protected $listeners = [
        'updateNewRestaurantCount'=>'render',
        'newRestaurantJoined'=>'render',
//        'refresh'=>'$refresh'
    ];
    public function render()
    {
//        dd('newRes');
        $this->countOFNewRestaurant=$this->getNewRestaurantCount();
//        if($this->countOFNewRestaurant==0 or $this->countOFNewRestaurant==1)
//        {
//            $this->emitTo('category.restaurant-component', 'updateNewRestaurantCount');
//
//        }

        return view('livewire.category.new-restuarnt-count-component');
    }
}
