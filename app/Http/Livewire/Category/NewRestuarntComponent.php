<?php

namespace App\Http\Livewire\Category;

use App\Events\ServiceProviderJoined;
use App\Mail\AcceptedRegister;
use App\Mail\EmailVerificationCodeSend;
use App\Models\filterTranslate;
use App\Models\ServiceProvider;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithCountryAndCity;
use App\Traits\WithImageValidationTrait;
use App\Traits\WithRestaurantTrait;
use App\Traits\WithUserValidationTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class NewRestuarntComponent extends Component
{
    use WithPagination ,WithCountryAndCity ,WithUserValidationTrait
        ,WithRestaurantTrait, WithFileUploads, WithImageValidationTrait;
    protected $paginationTheme = 'bootstrap';
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

//    public $countOFNewRestaurant;
    public $restaurantID;
    public $allRestaurantFilter;

    public $minimumOrderAmount;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $registrationState;
    public $selectedRestaurantFilters=[];
    public $registeredRestaurant;

    protected $listeners = [
        'getRestaurants'=>'render',
//        'getItemByFilter'=>'render',
//        'refresh'=>'$refresh'
    ];


    public function hydrate()
    {
//        $this->resetErrorBag();
        $this->resetValidation();
    }
    public function messages()
    {


        return [

            'registrationState.required' => trans( 'restaurant.validateRegistrationState' ),
            'selectedRestaurantFilters.required' => trans( 'restaurant.validateSelectedRestaurantFilters' ),
        ];
    }

    public function updateCall($restaurantID)
    {
        $this->restFilde();
        $this->restaurantID=$restaurantID;
        $this->registeredRestaurant=ServiceProvider::with('user:users.id,users.phone,users.email,users.firstName,users.fatherName,users.lastName')
            ->find($restaurantID);
    }
    public function updateRegistration()
    {
        $this->registrationState==true?$state=1:$state=0;
        $this->validate([
            'registrationState' => 'required',
            'selectedRestaurantFilters' => 'array|required',
        ]);
//        dd($this->registrationState,$state);
//        $res=ServiceProvider::with('user:users.id,users.phone,users.email')->find($this->restaurantID);
        $this->registeredRestaurant->mode= $this->deliveryMode;
        $this->registeredRestaurant->delivery_man_commission=$this->deliveryManCommission;
        $this->registeredRestaurant->pick_commission=$this->pickCommission;
        $this->registeredRestaurant->registration_state=$state;
        $this->registeredRestaurant->minimum_order_amount=$this->minimumOrderAmount;
        $this->registeredRestaurant->save();
        $this->registeredRestaurant->filters()->attach($this->selectedRestaurantFilters);
        $this->restFilde();
        $this->emitTo('category.new-restuarnt-count-component', 'updateNewRestaurantCount');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('updateRegistration');
        $this->emitTo('category.restaurant-component', 'updateNewRestaurantCount');
        $text=__("serviceProvider.textSendWhenActiveRegistration");
        if($this->registeredRestaurant->user->email!=null)
        {
//            dd('lll');
            Mail::to($this->registeredRestaurant->user->email)->send(new AcceptedRegister($text));
//            dd('lll')
        }
        if($this->registeredRestaurant->user->phone!=null)
        {
            $user1=User::find($this->registeredRestaurant->user_id);
            try {
                $user1->notify(new ClickSendTest($text));
            } catch (\Exception $e) {
                // do something when error
//                session()->put('mobileMessage', trans('userFront.FailedToSendCodeToPhone'));
            }

        }


    }

    public function mount()
    {

//       $this->countOFNewRestaurant=$this->getNewRestaurantCount();
//       dd($this->countOFNewRestaurant);
    }
    //    }
    public function deleteCall($id){
        $this->restaurantID=$id;
    }
    public function deleteAction()
    {
        $res=ServiceProvider::with('user')->find($this->restaurantID);
        $userId=$res->user->id;
        $user=User::find($userId);
        $user->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        event(new ServiceProviderJoined("res"));

//        $this->emitTo('category.new-restuarnt-count-component', 'updateNewRestaurantCount');
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction2');
        $this->emitTo('category.restaurant-component', 'updateNewRestaurantCount');
    }

    public function restFilde()
    {
        $this->minimumOrderAmount=null;
        $this->deliveryMode=null;
        $this->deliveryManCommission=null;
        $this->pickCommission=null;
        $this->selectedRestaurantFilters=[];
        $this->registrationState=null;
    }

    public function render()
    {
        $user=Auth::user();
        if($user->role_id==2)
        {
            $newRestaurants = ServiceProvider::with('user', 'country', 'country.translation', 'city', 'city.translation')
                ->where('type', 'res')
                ->where('registration_state', 0)
                ->paginate(10);
        } elseif ($user->role_id==8)
        {
            $newRestaurants=ServiceProvider::with('user', 'country', 'country.translation', 'city', 'city.translation')
                ->whereHas('city', function (Builder $query) use ($user) {
                    $query->where('cities.id',  $user->city_id);
                })
                ->where('registration_state',0)
                ->where('type','res')
                ->paginate(10);
        }
        return view('livewire.category.new-restuarnt-component',compact('newRestaurants'));
    }
}
