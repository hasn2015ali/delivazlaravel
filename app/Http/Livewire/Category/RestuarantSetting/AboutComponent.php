<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\ServiceProvider;
use App\Traits\WithUserValidationTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Traits\WithCountryAndCity;
use App\Traits\WithRestaurantTrait;
use Livewire\WithFileUploads;

class AboutComponent extends Component
{
    use WithCountryAndCity ,WithRestaurantTrait, WithFileUploads, WithUserValidationTrait;
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;
    //vars for WithRestaurantTrait
    public $loggedInUser;
//    public $selectedCountry=null;
//    public $selectedCity=null;

    //vars for AboutComponent
    public $restaurantID;
    public $showSelectCountry=true;
    public $isRecommended;
    public $restaurantImage;
    public $restaurantImageOld;
    public $name;
    public $oldName;
    public $nameEN;
    public $address;
    public $email;
    public $phone1;
    public $phone2;
    public $phone3;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $minimumOrderAmount;
    public $password;
    public $firstName;
    public $fatherName;
    public $lastName;
    public $type="res";
    public $roleID=9;
    public $restaurant;
    public $wallet;
    public $providerURL;
    public $ourCommission;

    public function mount()
    {
        $this->setRoleForManager();
        $this->getCountries();
        $this->restaurant=ServiceProvider::with('user')->find($this->restaurantID);
        $this->name=$this->restaurant->name;
        $this->oldName=$this->restaurant->name;
        $this->nameEN=$this->restaurant->nameEN;
        $this->email=$this->restaurant->user->email;
        $this->phone1=$this->restaurant->phone1;
        $this->phone2=$this->restaurant->phone2;
        $this->phone3=$this->restaurant->phone3;
        $this->address=$this->restaurant->address;
        $this->restaurantImageOld=$this->restaurant->avatar;
        $this->deliveryMode=$this->restaurant->mode;
        $this->deliveryManCommission=$this->restaurant->delivery_man_commission;
        $this->pickCommission=$this->restaurant->pick_commission;
        $this->ourCommission=$this->restaurant->delivaz_commission;
        $this->minimumOrderAmount=$this->restaurant->minimum_order_amount;
        $this->firstName=$this->restaurant->user->firstName;
        $this->fatherName=$this->restaurant->user->fatherName;
        $this->lastName=$this->restaurant->user->lastName;
        $this->isRecommended=$this->restaurant->isRecommended;
        $this->wallet=$this->restaurant->user->balance;
         $this->providerURL=$this->restaurant->provider_url;

        if($this->restaurantImageOld==null)
        {
            $this->restaurantImageOld="person.png";
        }
        if($this->wallet==null)
        {
            $this->wallet=0;
        }



    }

    public function messages()
    {

        return [
            'name.required' => trans( 'restaurant.validateName' ),
            'email.required' => trans( 'restaurant.validateEmail' ),
            'firstName.required' => trans( 'restaurant.validateFirstName' ),
            'lastName.required' => trans( 'restaurant.validateLastName' ),
            'selectedVendorFilters.size' => trans( 'restaurant.validateSelectedVendorFilters' ),

        ];
    }

    public function submit()
    {
        $this->name= str_replace(['-','_','*'], ' ', $this->name);
        $this->name=trim($this->name);
        $slug= str_replace(' ', '-', $this->name);
        if( $this->oldName!= $this->name)
        {
            if($this->checkServiceProviderName($this->name)==1)
            {
                return;
            }
        }

        if($this->email!=$this->restaurant->user->email)
        {
            if($this->checkEmail($this->email)==1)
            {
                return;
            }
        }
        if($this->password==null)
        {
            $newPassword=$this->restaurant->user->password;
        }
        else
        {
            $newPassword=bcrypt($this->password);
        }

        if($this->restaurantImage)
        {
            if($this->restaurant->avatar==null)
            {
                $imageName=time().".".$this->restaurantImage->extension();
                $this->restaurantImage->storeAs('public/images/avatars',$imageName);
            }
            else
            {
                Storage::delete('public/images/avatars/'.$this->restaurant->avatar);
                $imageName=time().".".$this->restaurantImage->extension();
                $this->restaurantImage->storeAs('public/images/avatars',$imageName);
            }

        }
        else
        {
            $imageName=$this->restaurant->avatar;
        }

        $this->validate([
            'name' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
        ]);


        $this->restaurant->user->update([
            'firstName' => $this->firstName,
            'fatherName' => $this->fatherName,
            'lastName' => $this->lastName,
//            'avatar' => $this->filterID,
            'email' => $this->email,
            'password' => $newPassword,
        ]);
        $this->restaurant->name= $this->name;
        $this->restaurant->nameEN=$this->nameEN;
        $this->restaurant->phone1=$this->phone1;
        $this->restaurant->phone2=$this->phone2;
        $this->restaurant->phone3=$this->phone3;
        $this->restaurant->address=$this->address;
        $this->restaurant->avatar=$imageName;
        $this->restaurant->mode=$this->deliveryMode;
        $this->restaurant->delivery_man_commission=$this->deliveryManCommission;
        $this->restaurant->pick_commission=$this->pickCommission;
        $this->restaurant->minimum_order_amount= $this->minimumOrderAmount;
        $this->restaurant->provider_url= $this->providerURL;
        $this->restaurant->delivaz_commission=$this->ourCommission;
        $this->restaurant->save();
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);


    }
    public function changeRecommendedState()
    {
        if($this->isRecommended==true)
        {
            $this->restaurant->isRecommended=1;
            $this->restaurant->save();
        }
        elseif($this->isRecommended==false)
        {
            $this->restaurant->isRecommended=0;
            $this->restaurant->save();
        }
    }
    public function getCities()
    {
        $this->getCitiesForCountry($this->selectedCountry);
    }
    public function render()
    {
//        dd('tset');
        return view('livewire.category.restuarant-setting.about-component');
    }
}
