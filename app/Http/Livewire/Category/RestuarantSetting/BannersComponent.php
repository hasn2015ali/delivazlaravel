<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\ServiceProviderBanner;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class BannersComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $restaurantID;
    public $restaurantBanner;
    public $bannerId;
//    public $banners;

    public function messages()
    {

        return [
            'restaurantBanner.required' => trans( 'restaurant.validateRestaurantBanner' ),
        ];
    }

    public function submit()
    {

        $this->validate([
            'restaurantBanner' => 'required',
        ]);
        if($this->restaurantBanner)
        {
            $imageName=time().rand(1,100).".".$this->restaurantBanner->extension();
            $this->restaurantBanner->storeAs('public/images/sliders/restaurant',$imageName);
            $newBanner= ServiceProviderBanner::create([
                'name' => $imageName,
                'state' => 0,
                'service_provider_id' => $this->restaurantID,
            ]);
        }


//        $this->emitUp('bannerAdded',$newBanner);
//        $this->emitUp('reviewSectionRefresh');
        $locale=App::getLocale();
//        session()->put('tabActive', 'banners');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
//        $this->restaurantBanner=null;
        return redirect($locale.'/restaurantBanners/'.$this->restaurantID);



    }
    public function mount()
    {
//        $this
    }
    public function changeBannerState($id)
    {
//        dd('tst');
        $banner=ServiceProviderBanner::find($id);
        if($banner->state==0)
        {
            $banner->state=1;
            $banner->save();
        }
        elseif($banner->state==1)
        {
            $banner->state=0;
            $banner->save();
        }
    }

    public function deleteCall($id){
        $this->bannerId=$id;
    }
    public function deleteAction()
    {
        $banner=ServiceProviderBanner::find($this->bannerId);
        if(Storage::delete('public/images/sliders/restaurant/'.$banner->name))
        {
            $banner->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction2');
        }

        $locale=App::getLocale();
//        session()->put('tabActive', 'banners');
        return redirect($locale.'/restaurantBanners/'.$this->restaurantID);
    }

    public function render()
    {
        $banners=ServiceProviderBanner::where('service_provider_id',$this->restaurantID)
            ->orderBy( 'id', 'desc' )
            ->paginate(2);
        return view('livewire.category.restuarant-setting.banners-component',compact('banners'));
    }
}
