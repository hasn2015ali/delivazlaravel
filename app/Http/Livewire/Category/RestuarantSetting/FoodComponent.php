<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\filterTranslate;
use App\Models\Food;
use App\Models\FoodAdditions;
use App\Models\FoodPhotos;
use App\Models\FoodSizes;
use App\Models\FoodTranslation;
use App\Models\Language;
use App\Models\MealFilter;
use App\Models\MealFilterTrans;
use App\Models\ServiceProvider;
use App\Models\serviceProviderSizes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class FoodComponent extends Component
{
    use WithPagination ,WithFileUploads ;
    protected $paginationTheme = 'bootstrap';

    public $allMailFilters=[];
    public $restaurantID;
    public $languageSelected;
    public $photo;
    public $name;
    public $content;
    public $price;
    public $foodHour;
    public $foodMinutes;
    public $selectedFoodFilters=[];
    public $updatedFoodFilters=[];
    public $restaurant;
    public $food;
    public $langCode;
    public $photoOld;
    public $foodUp;
    public $foodID;
    public $foodByFiltersShow=false;
    public $MealFilterID;
    public $MealFilterName;
    public $size;
    public $allSizes;
    public $foodSizeSelected;
    public $sizePrice;
    public $allSizesForFood;
    public $weight;

    public $AdditionName;
    public $AdditionContent;
    public $AdditionPhoto;
    public $AdditionPrice;
    public $AdditionWeight;
    public $allAdditionForFood;
    public $images= [];
    public $oldImages=[];
    public $sizeWeight;
    public $sizeContent;


    public function deleteProductPhoto($id)
    {
        $image=FoodPhotos::find($id);
        if(Storage::delete('public/images/food/'.$image->name))
        {
            $image->delete();
        }

        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->oldImages=FoodPhotos::where('food_id',$this->foodID)->get();

    }
    public function AddPhotos()
    {
        $this->validate([
            'images.*' => 'image|required'
        ]);
//        dd($this->foodID);
        if(count($this->images)!=0)
        {
            foreach ($this->images as $image)
            {
                $imageName=time().rand(1,1000).".".$image->extension();
                $image->storeAs('public/images/food',$imageName);
                FoodPhotos::create([
                    'food_id'=>$this->foodID,
                    'name'=>$imageName,
                ]);
            }
            $message=trans( 'masterControl.successfully_Added' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->oldImages=FoodPhotos::where('food_id',$this->foodID)->get();

        }
        else
        {
            $message=trans( 'vendor.imagesEmpty' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }

        $this->images=[];
//        dd($this->images);
    }
    public function editFoodAddition($foodID)
    {
        $this->foodID=$foodID;
        $this->allAdditionForFood=FoodAdditions::where('food_id',$foodID)->get();
        $this->food=Food::find($foodID);

    }
    public function deleteFoodAddition($additionID)
    {
        $addition=FoodAdditions::find($additionID);
        $addition->delete();
        $this->allAdditionForFood=FoodAdditions::where('food_id',$this->foodID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function submitAddition()
    {

        $this->validate([
            'AdditionName' => 'required',
            'AdditionPrice' => 'required',
        ]);
        if($this->AdditionPhoto)
        {
            $imageName=time().rand(1,100).".".$this->AdditionPhoto->extension();
            $this->AdditionPhoto->storeAs('public/images/food/addition',$imageName);
        }
        else
        {
            $imageName=null;
        }
        $addition=FoodAdditions::create([
            'price' => $this->AdditionPrice,
            'name' => $this->AdditionName,
            'content' => $this->AdditionContent,
            'weight' => $this->AdditionWeight,
            'photo' => $imageName,
            'food_id' => $this->foodID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restAdditionFilde();
        $this->allAdditionForFood->push($addition);
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restAdditionFilde()
    {
        $this->AdditionPrice=null;
        $this->AdditionName=null;
        $this->AdditionContent=null;
        $this->AdditionWeight=null;
        $this->AdditionPhoto=null;

    }
    public function editFoodSize($foodID)
    {
        $this->foodID=$foodID;
        $this->allSizesForFood=FoodSizes::where('food_id',$foodID)->get();
        $this->food=Food::find($foodID);

    }
    public function deleteFoodSize($sizeID)
    {
        $size=FoodSizes::find($sizeID);
        $size->delete();
        $this->allSizesForFood=FoodSizes::where('food_id',$this->foodID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function submitFoodSize()
    {
        $this->validate([
            'foodSizeSelected' => 'required',
            'sizePrice' => 'required',
        ]);
        $size=FoodSizes::create([
            'price' => $this->sizePrice,
            'size' => $this->foodSizeSelected,
            'weight' => $this->sizeWeight,
            'content' => $this->sizeContent,
            'food_id' => $this->foodID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restSizeFilde();
        $this->allSizesForFood->push($size);
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }

    public function submitSize()
    {
        $this->validate([
            'size' => 'required',
        ]);

        $size= serviceProviderSizes::create([
            'name' => $this->size,
            'service_provider_id' => $this->restaurantID,
        ]);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restSizeFilde();
        $this->allSizes->push($size);
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function deleteSize($sizeID)
    {
        $size=serviceProviderSizes::find($sizeID);
        DB::table('food')
            ->where('size', $size->name)
            ->update(['size' => null]);
        DB::table('food_sizes')->where('size', $size->name)->delete();
        $size->delete();
        $this->allSizes=serviceProviderSizes::where('service_provider_id',$this->restaurantID)->get();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restSizeFilde()
    {
        $this->size=null;
        $this->sizePrice=null;
        $this->foodSizeSelected=null;
    }


    public function getFoodForThisFilter($filterID)
    {
//        dd('gg');

        if($filterID==0)
        {
            $this->foodByFiltersShow=false;
            $this->MealFilterName=trans('restaurant.allMealsFilters');
        }
        else{
            $this->foodByFiltersShow=true;
            $this->MealFilterID=$filterID;
            $this->MealFilterName=MealFilterTrans::where('meal_filter_id',$filterID)
                ->where('code_lang',App::getlocale())
                ->first()->name;
        }

//        dd( $this->MealFilterName);
    }
    public function messages()
    {
        return [
            'name.required' => trans( 'restaurant.validateFoodName' ),
            'price.required' => trans( 'restaurant.validateFoodPrice' ),
            'selectedFoodFilters.required' => trans( 'restaurant.validateFoodFilter' ),
            'updatedFoodFilters.required' => trans( 'restaurant.validateFoodFilter' ),
            'size.required' => trans( 'restaurant.validateSize' ),
            'foodSizeSelected.required' => trans( 'restaurant.validateFoodSizeSelected' ),
            'sizePrice.required' => trans( 'restaurant.validateSizePrice' ),
            'AdditionName.required' => trans( 'restaurant.validateAdditionName' ),
            'AdditionPrice.required' => trans( 'restaurant.validateAdditionPrice' ),


        ];
    }
    public function submit()
    {
//        dd($this->foodSizeSelected);
        $this->validate([
            'name' => 'required',
            'price' => 'required',
            'selectedFoodFilters' => 'array|required',

        ]);
        $en_lang=Language::where('code','en')->first();
        if($this->photo)
        {
            $imageName=time().rand(1,100).".".$this->photo->extension();
            $this->photo->storeAs('public/images/food',$imageName);
        }
        else
        {
            $imageName=null;
        }
        $this->weight==""||$this->weight==null?$this->weight=0 : $this->weight=$this->weight;
        $this->foodHour==""||$this->foodHour==null?$this->foodHour=0 : $this->foodHour=$this->foodHour;
        $this->foodMinutes==""||$this->foodMinutes==null?$this->foodMinutes=0 : $this->foodMinutes=$this->foodMinutes;
        $food= Food::create([
            'photo' => $imageName,
            'price' => $this->price,
            'size' => $this->foodSizeSelected,
            'time_hour' => $this->foodHour,
            'weight'=>$this->weight,
            'time_minutes' => $this->foodMinutes,
            'service_provider_id' => $this->restaurantID,
        ]);
        $food_trans=FoodTranslation::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'food_id' => $food->id,
            'content' => $this->content,
            'service_provider_id' => $this->restaurantID,
        ]);

        $food->filters()->attach($this->selectedFoodFilters);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newFoodAdded');
    }
    public function restFilde()
    {
        $this->name="";
        $this->content="";
        $this->photo="";
        $this->foodMinutes="";
        $this->foodHour="";
        $this->price="";
        $this->weight="";
        $this->foodSizeSelected="";
        $this->selectedFoodFilters=[];
    }

    public function mount()
    {
        $this->langCode=App::getlocale();
        $this->languageSelected=Language::where('code',$this->langCode)->first();
        $this->restaurant=ServiceProvider::with('filters')->find($this->restaurantID);
//        dd($this->restaurant->filters);
        foreach ($this->restaurant->filters as $restaurantFilter)
        {
            $mailFilterForThisRestaurantFilter=MealFilterTrans::where('lang_id', $this->languageSelected->id)
            ->where('filter_id',$restaurantFilter->id)->get()->toArray();
            array_push($this->allMailFilters,$mailFilterForThisRestaurantFilter);

        }
        $this->allSizes=serviceProviderSizes::where('service_provider_id',$this->restaurantID)->get();


//        dd( $this->allSizes,$this->restaurantID);
//        dd($this->allMailFilters[0][0]['id']);

//        $this->allMailFilters=MealFilterTrans::where('lang_id', $this->languageSelected->id)
//            ->get();
    }

    public function editFoodFilter($foodID)
    {

        $this->food=Food::with('filters')->find($foodID);

//        dd(  $this->food->id);
    }
    public function UpdateFoodFilters()
    {
        $this->validate([
            'updatedFoodFilters' => 'array|required',
        ]);
        $this->food->filters()->detach();
        $this->food->filters()->attach($this->updatedFoodFilters);
        $this->food=Food::with('filters')->find($this->food->id);
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->updatedFoodFilters=[];
    }
    public function editFood($id)
    {
        $this->foodID=$id;
        $this->oldImages=FoodPhotos::where('food_id',$this->foodID)->get();
        $this->foodUp=FoodTranslation::with('food')->where('food_id',$id)
        ->where('code_lang','en')->first();
//        dd($this->foodUp->size);

        $this->name=$this->foodUp->name;
        $this->content=$this->foodUp->content;
        $this->price=$this->foodUp->food->price;
        $this->weight=$this->foodUp->food->weight;
        $this->photoOld=$this->foodUp->food->photo;
        $this->foodHour=$this->foodUp->food->time_hour;
        $this->foodMinutes=$this->foodUp->food->time_minutes;
        $this->foodSizeSelected=$this->foodUp->food->size;
//        $this->name=$this->food->translation
    }

    public function updateFoodDetails()
    {

        $this->validate([
            'name' => 'required',
            'price' => 'required',
        ]);

//        dd("dd");
        if($this->photo)
        {
            if($this->foodUp->food->photo==null)
            {
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/food',$imageName);
            }
            else
            {
                Storage::delete('public/images/food/'.$this->foodUp->food->photo);
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/food',$imageName);
            }

        }
        else
        {
            $imageName=$this->foodUp->food->photo;
        }


        $this->foodUp->name=$this->name;
        $this->foodUp->content=$this->content;
            $this->foodUp->save();
            $food=Food::find($this->foodUp->food_id);

//            $this->foodUp->food->update([
//                'photo' => $imageName,
//                'price' => $this->price,
//                'time_hour' => $this->foodHour,
//                'time_minutes' => $this->foodMinutes,
//            ]);
        $this->weight==""||$this->weight==null?$this->weight=0 : $this->weight=$this->weight;
        $this->foodHour==""||$this->foodHour==null?$this->foodHour=0 : $this->foodHour=$this->foodHour;
        $this->foodMinutes==""||$this->foodMinutes==null?$this->foodMinutes=0 : $this->foodMinutes=$this->foodMinutes;

        $this->foodSizeSelected=="0"?$this->foodSizeSelected=null : $this->foodSizeSelected=$this->foodSizeSelected;
//        if($this->foodHour==null ){$this->foodHour=0;}
//       if($this->foodMinutes==null ){$this->foodMinutes=0;}
            $food->price=$this->price;
            $food->weight=$this->weight;
            $food->size=$this->foodSizeSelected;
            $food->photo=$imageName;
            $food->time_hour= $this->foodHour;
            $food->time_minutes=$this->foodMinutes;
            $food->save();

//        dd($this->foodUp->save());
        $this->restFilde();
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newFoodUpdated');
    }

    public function deleteCall($id){
        $this->foodID=$id;
    }
    public function deleteAction()
    {
        $food=Food::find($this->foodID);
        $food->delete();
        $food->filters()->detach();
        if($food->photo!=null)
        {
            Storage::delete('public/images/food/'.$food->photo);
        }

        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }

    public function render()
    {
        $foods=null;
        $foodByFilters=null;
        if(!$this->foodByFiltersShow)
        {
            $foods=FoodTranslation::with('food')
                ->where('lang_id', $this->languageSelected->id)
                ->where('service_provider_id',$this->restaurantID)
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
//            dd('gg');
            $foodByFilters = Food::with('translation')->whereHas('filters', function ($query) {
                $query->where('meal_filters.id', $this->MealFilterID);
            })  ->where('service_provider_id',$this->restaurantID)
                ->orderBy('id','desc')
                ->paginate(10);
//            $foodByFilters=MealFilter::with('foods','foods.translation')->find($this->MealFilterID)->foods;
//            $foodByFilters=Food::with('filters')->orderBy('id','desc')->get();

//            foreach ($foodByFilters as $food)
//            {
//                $foods=$foodByFilters;
//
//            }
//            dd($foodByFilters);

        }

        return view('livewire.category.restuarant-setting.food-component',compact('foods','foodByFilters'));
    }
}
