<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\ServiceProvider;
use App\Models\ServiceProviderPartTime;
use Livewire\Component;

class WorkComponent extends Component
{
    public $restaurantID;
    public $days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    public $daySelected;
    //day1
    public $hourStart1="off";
    public $minutesStart1="off";
    public $hourEnd1="off";
    public $minutesEnd1="off";

    //day2
    public $hourStart2="off";
    public $minutesStart2="off";
    public $hourEnd2="off";
    public $minutesEnd2="off";

    //day3
    public $hourStart3="off";
    public $minutesStart3="off";
    public $hourEnd3="off";
    public $minutesEnd3="off";
    //day4
    public $hourStart4="off";
    public $minutesStart4="off";
    public $hourEnd4="off";
    public $minutesEnd4="off";
    //day5
    public $hourStart5="off";
    public $minutesStart5="off";
    public $hourEnd5="off";
    public $minutesEnd5="off";
    //day6
    public $hourStart6="off";
    public $minutesStart6="off";
    public $hourEnd6="off";
    public $minutesEnd6="off";
    //day7
    public $hourStart7="off";
    public $minutesStart7="off";
    public $hourEnd7="off";
    public $minutesEnd7="off";
    public $dayState="Off";
    public $day1="on";
    public $day2="on";
    public $day3="on";
    public $day4="on";
    public $day5="on";
    public $day7="on";
    public $day6="on";
    public $restaurant;
    public $serviceProviderState;
    public $hoursInDay;
    public $minutesInHour;

//    public $vv;

    public function changeServiceProviderState()
    {
        if($this->restaurant->state==1)
        {
            $this->restaurant->state=0;
            $this->serviceProviderState=trans( 'restaurant.Closed' );
            $this->restaurant->save();
        }
        elseif($this->restaurant->state==0)
        {
            $this->restaurant->state=1;
            $this->serviceProviderState=trans( 'restaurant.Opened' );

            $this->restaurant->save();
        }
    }

    public function dayState()
    {
        if($this->day1=="off")
        {
            $this->hourStart1="off";
            $this->minutesStart1="off";
            $this->hourEnd1="off";
            $this->minutesEnd1="off";
        }
        if($this->day2=="off")
        {
            $this->hourStart2="off";
            $this->minutesStart2="off";
            $this->hourEnd2="off";
            $this->minutesEnd2="off";
        }
        if($this->day3=="off")
        {
            $this->hourStart3="off";
            $this->minutesStart3="off";
            $this->hourEnd3="off";
            $this->minutesEnd3="off";
        }

        if($this->day4=="off")
        {
            $this->hourStart4="off";
            $this->minutesStart4="off";
            $this->hourEnd4="off";
            $this->minutesEnd4="off";
        }
        if($this->day5=="off")
        {
            $this->hourStart5="off";
            $this->minutesStart5="off";
            $this->hourEnd5="off";
            $this->minutesEnd5="off";
        }
        if($this->day6=="off")
        {
            $this->hourStart6="off";
            $this->minutesStart6="off";
            $this->hourEnd6="off";
            $this->minutesEnd6="off";
        }
        if($this->day7=="off")
        {
            $this->hourStart7="off";
            $this->minutesStart7="off";
            $this->hourEnd7="off";
            $this->minutesEnd7="off";
        }


    }

    public function setValueForAttributs()
    {
        //for day1
        $day1=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',1)->first();
        if($day1)
        {
            $this->day1=$day1->day_state;
            $this->hourStart1=$day1->startTimeHour;
            $this->minutesStart1=$day1->startTimeMinutes;
            $this->hourEnd1=$day1->endTimeHour;
            $this->minutesEnd1=$day1->endTimeMinutes;
        }

        //for day2
        $day2=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',2)->first();
        if($day2)
        {
            $this->day2=$day2->day_state;
            $this->hourStart2=$day2->startTimeHour;
            $this->minutesStart2=$day2->startTimeMinutes;
            $this->hourEnd2=$day2->endTimeHour;
            $this->minutesEnd2=$day2->endTimeMinutes;
        }


        //for day3
        $day3=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',3)->first();
        if($day3)
        {
            $this->day3=$day3->day_state;
            $this->hourStart3=$day3->startTimeHour;
            $this->minutesStart3=$day3->startTimeMinutes;
            $this->hourEnd3=$day3->endTimeHour;
            $this->minutesEnd3=$day3->endTimeMinutes;
//            dd($this->hourStart3,$day3->startTimeHour);
        }

        //for day4
        $day4=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',4)->first();
        if($day4)
        {
            $this->day4=$day4->day_state;
            $this->hourStart4=$day4->startTimeHour;
            $this->minutesStart4=$day4->startTimeMinutes;
            $this->hourEnd4=$day4->endTimeHour;
            $this->minutesEnd4=$day4->endTimeMinutes;
        }

        //for day5
        $day5=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',5)->first();
        if($day5)
        {
            $this->day5=$day5->day_state;
            $this->hourStart5=$day5->startTimeHour;
            $this->minutesStart5=$day5->startTimeMinutes;
            $this->hourEnd5=$day5->endTimeHour;
            $this->minutesEnd5=$day5->endTimeMinutes;
        }

        //for day6
        $day6=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',6)->first();
        if($day6)
        {
            $this->day6=$day6->day_state;
            $this->hourStart6=$day6->startTimeHour;
            $this->minutesStart6=$day6->startTimeMinutes;
            $this->hourEnd6=$day6->endTimeHour;
            $this->minutesEnd6=$day6->endTimeMinutes;
        }

        //for day7
        $day7=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',7)->first();
        if($day7)
        {
            $this->day7=$day7->day_state;
            $this->hourStart7=$day7->startTimeHour;
            $this->minutesStart7=$day7->startTimeMinutes;
            $this->hourEnd7=$day7->endTimeHour;
            $this->minutesEnd7=$day7->endTimeMinutes;
        }

    }
    public function mount()
    {
        $this->hoursInDay= array_keys($this->getAllHoursInDay());
        $this->minutesInHour= array_keys($this->getAllMinutesInHour());

        $this->setValueForAttributs();
        $this->restaurant=ServiceProvider::where('id',$this->restaurantID)
            ->first();
        if($this->restaurant->state==1)
        {
            $this->serviceProviderState=trans( 'restaurant.Opened' );
        }
        elseif($this->restaurant->state==0)
        {
            $this->serviceProviderState=trans( 'restaurant.Closed' );

        }

    }


    public function validatDayOn($hS,$mS,$hE,$mE)
    {
        if($hS=="off")
        {
            $m=trans( 'restaurant.hSVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($mS=="off")
        {
            $m=trans( 'restaurant.mSVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($hE=="off")
        {
            $m=trans( 'restaurant.hEVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($mE=="off")
        {
            $m=trans( 'restaurant.mEVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }

    }

    public function insertData($day,$hS,$mS,$hE,$mE,$state)
    {
        ServiceProviderPartTime::create([
            'startTimeHour' => $hS,
            'startTimeMinutes' => $mS,
            'endTimeHour' => $hE,
            'endTimeMinutes' => $mE,
            'day_state' => $state,
            'day_id' => $day,
            'service_provider_id' => $this->restaurantID,
        ]);
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }
    public function updateData($day,$hS,$mS,$hE,$mE,$state)
    {
        $up=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',$day)->first();
        $up->startTimeHour=$hS;
        $up->startTimeMinutes=$mS;
        $up->endTimeHour=$hE;
        $up->endTimeMinutes=$mE;
        $up->day_state=$state;

        $up->save();
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }

    public function checkUpdateOrAdd($day)
    {
        $checkUpdateOrAdd=ServiceProviderPartTime::where('service_provider_id',$this->restaurantID)
            ->where('day_id',$day)->first();
        if($checkUpdateOrAdd==null)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    public function submit($id)
    {
        if($id==1)
        {
            if($this->day1=="on")
            {
               if( $this->validatDayOn($this->hourStart1,$this->minutesStart1,$this->hourEnd1,$this->minutesEnd1)==1)
               {
                   return;
               }
                if($this->checkUpdateOrAdd(1)==0)
                {
                    $this->insertData(1,$this->hourStart1,$this->minutesStart1,$this->hourEnd1,$this->minutesEnd1,"on");

                }
                elseif ($this->checkUpdateOrAdd(1)==1)
                {
                    $this->updateData(1,$this->hourStart1,$this->minutesStart1,$this->hourEnd1,$this->minutesEnd1,"on");

                }

            }
            elseif ($this->day1=="off")
            {
                if($this->checkUpdateOrAdd(1)==0)
                {
                    $this->insertData(1,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(1)==1)
                {
                    $this->updateData(1,"off","off","off","off","off");

                }
            }

//            dd($this->hourStart1,$this->minutesStart1,$this->hourEnd1,$this->minutesEnd1);

        }
        if($id==2)
        {
            if($this->day2=="on")
            {
//                $this->vv= "hourStart".$id;
//                dd($this->vv);
                if( $this->validatDayOn($this->hourStart2,$this->minutesStart2,$this->hourEnd2,$this->minutesEnd2)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(2)==0)
                {
                    $this->insertData(2,$this->hourStart2,$this->minutesStart2,$this->hourEnd2,$this->minutesEnd2,"on");

                }
                elseif ($this->checkUpdateOrAdd(2)==1)
                {
                    $this->updateData(2,$this->hourStart2,$this->minutesStart2,$this->hourEnd2,$this->minutesEnd2,"on");

                }
            }
            elseif ($this->day2=="off")
            {
                if($this->checkUpdateOrAdd(2)==0)
                {
                    $this->insertData(2,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(2)==1)
                {
                    $this->updateData(2,"off","off","off","off","off");

                }
            }
//            dd($this->hourStart2,$this->minutesStart2,$this->hourEnd2,$this->minutesEnd2);

        }

        if($id==3)
        {
            if($this->day3=="on")
            {
                if( $this->validatDayOn($this->hourStart3,$this->minutesStart3,$this->hourEnd3,$this->minutesEnd3)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(3)==0)
                {
                    $this->insertData(3,$this->hourStart3,$this->minutesStart3,$this->hourEnd3,$this->minutesEnd3,"on");

                }
                elseif ($this->checkUpdateOrAdd(3)==1)
                {
                    $this->updateData(3,$this->hourStart3,$this->minutesStart3,$this->hourEnd3,$this->minutesEnd3,"on");

                }
            }
            elseif ($this->day3=="off")
            {
                if($this->checkUpdateOrAdd(3)==0)
                {
                    $this->insertData(3,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(3)==1)
                {
                    $this->updateData(3,"off","off","off","off","off");

                }
            }
        }
        if($id==4)
        {
            if($this->day4=="on")
            {
                if( $this->validatDayOn($this->hourStart4,$this->minutesStart4,$this->hourEnd4,$this->minutesEnd4)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(4)==0)
                {
                    $this->insertData(4,$this->hourStart4,$this->minutesStart4,$this->hourEnd4,$this->minutesEnd4,"on");

                }
                elseif ($this->checkUpdateOrAdd(4)==1)
                {
                    $this->updateData(4,$this->hourStart4,$this->minutesStart4,$this->hourEnd4,$this->minutesEnd4,"on");

                }
            }
            elseif ($this->day4=="off")
            {
                if($this->checkUpdateOrAdd(4)==0)
                {
                    $this->insertData(4,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(4)==1)
                {
                    $this->updateData(4,"off","off","off","off","off");

                }
            }
        }
        if($id==5)
        {
            if($this->day5=="on")
            {
                if( $this->validatDayOn($this->hourStart5,$this->minutesStart5,$this->hourEnd5,$this->minutesEnd5)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(5)==0)
                {
                    $this->insertData(5,$this->hourStart5,$this->minutesStart5,$this->hourEnd5,$this->minutesEnd5,"on");

                }
                elseif ($this->checkUpdateOrAdd(5)==1)
                {
                    $this->updateData(5,$this->hourStart5,$this->minutesStart5,$this->hourEnd5,$this->minutesEnd5,"on");

                }
            }
            elseif ($this->day5=="off")
            {
                if($this->checkUpdateOrAdd(5)==0)
                {
                    $this->insertData(5,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(5)==1)
                {
                    $this->updateData(5,"off","off","off","off","off");

                }
            }
        }
        if($id==6)
        {
            if($this->day6=="on")
            {
                if( $this->validatDayOn($this->hourStart6,$this->minutesStart6,$this->hourEnd6,$this->minutesEnd6)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(6)==0)
                {
                    $this->insertData(6,$this->hourStart6,$this->minutesStart6,$this->hourEnd6,$this->minutesEnd6,"on");

                }
                elseif ($this->checkUpdateOrAdd(6)==1)
                {
                    $this->updateData(6,$this->hourStart6,$this->minutesStart6,$this->hourEnd6,$this->minutesEnd6,"on");

                }
            }
            elseif ($this->day6=="off")
            {
                if($this->checkUpdateOrAdd(6)==0)
                {
                    $this->insertData(6,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(6)==1)
                {
                    $this->updateData(6,"off","off","off","off","off");

                }
            }
        }
        if($id==7)
        {
            if($this->day7=="on")
            {
                if( $this->validatDayOn($this->hourStart7,$this->minutesStart7,$this->hourEnd7,$this->minutesEnd7)==1)
                {
                    return;
                }
                if($this->checkUpdateOrAdd(7)==0)
                {
                    $this->insertData(7,$this->hourStart7,$this->minutesStart7,$this->hourEnd7,$this->minutesEnd7,"on");

                }
                elseif ($this->checkUpdateOrAdd(7)==1)
                {
                    $this->updateData(7,$this->hourStart7,$this->minutesStart7,$this->hourEnd7,$this->minutesEnd7,"on");

                }
            }
            elseif ($this->day7=="off")
            {
                if($this->checkUpdateOrAdd(7)==0)
                {
                    $this->insertData(7,"off","off","off","off","off");

                }
                elseif ($this->checkUpdateOrAdd(7)==1)
                {
                    $this->updateData(7,"off","off","off","off","off");

                }
            }
        }

    }


    public function getAllHoursInDay( $start = 0, $end = 86400, $step = 3600, $format = 'g:i a' ) {
        $times = array();
        foreach ( range( $start, $end, $step ) as $timestamp ) {
            $hour_mins = gmdate( 'H', $timestamp );
            if ( ! empty( $format ) )
                $times[$hour_mins] = gmdate( $format, $timestamp );
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function getAllMinutesInHour( $start = 0, $end = 3600, $step = 60, $format = 'g:i a' ) {
        $times = array();
        foreach ( range( $start, $end, $step ) as $timestamp ) {
            $hour_mins = gmdate( 'i', $timestamp );
            if ( ! empty( $format ) )
                $times[$hour_mins] = gmdate( $format, $timestamp );
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function render()
    {
        return view('livewire.category.restuarant-setting.work-component');
    }
}
