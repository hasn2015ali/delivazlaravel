<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\filterTranslate;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class FiltersComponent extends Component
{
    public $restaurantID;
    public $restaurant;
    public $allRestaurantFilter;
    public $selectedFiltersForRestaurant=[];
    public $newSelectedFilters=[];
    public $langCode;


    public function messages()
    {


        return [
            'newSelectedFilters.required' => trans( 'restaurant.validateSelectedRestaurantFilters' ),

        ];
    }

    public function mount()
    {
        $this->langCode=App::getlocale();
        $this->restaurant=ServiceProvider::with('filters','user')->find($this->restaurantID);
        $this->selectedFiltersForRestaurant=$this->restaurant->filters;
//        ->translation->where('code',$langCode)->first()
        $this->allRestaurantFilter=filterTranslate::where('type',1)->get();
//        dd( $this->selectedFiltersForRestaurant[0]->translation->where('code_lang',$langCode)->first());

    }
    public function submit()
    {
        $this->validate([
            'newSelectedFilters' => 'required',
        ]);
        $this->restaurant->filters()->detach();
        $this->restaurant->filters()->attach($this->newSelectedFilters);
        $this->selectedFiltersForRestaurant=ServiceProvider::with('filters')
            ->find($this->restaurantID)->filters;
//        dd($this->selectedFiltersForRestaurant);
        $this->newSelectedFilters=[];
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

        $locale=App::getLocale();
//        session()->put('filtersActive', 'filters');
//        return redirect($locale.'/restaurantFilters/'.$this->restaurantID);

//        dd($this->newSelectedFilters);
    }

    public function render()
    {
        return view('livewire.category.restuarant-setting.filters-component');
    }
}
