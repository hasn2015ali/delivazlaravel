<?php

namespace App\Http\Livewire\Category\RestuarantSetting;

use App\Models\ServiceProviderDocument;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class DocumentsComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $restaurantID;
    public $restaurantDocument;
    public $documentId;
//    public $documents;

    public function messages()
    {

        return [
            'restaurantDocument.required' => trans( 'restaurant.validateRestaurantDocument' ),
        ];
    }

    public function submit()
    {

        $this->validate([
            'restaurantDocument' => 'required',
        ]);
        if($this->restaurantDocument)
        {
            $imageName=time().rand(1,100).".".$this->restaurantDocument->extension();
            $this->restaurantDocument->storeAs('public/images/document/restaurant',$imageName);
            $newDocument= ServiceProviderDocument::create([
                'name' => $imageName,
                'service_provider_id' => $this->restaurantID,
            ]);
        }

        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->restaurantDocument="";

    }


    public function deleteCall($id){
        $this->documentId=$id;
    }
    public function deleteAction()
    {
        $document=ServiceProviderDocument::find($this->documentId);
        if(Storage::delete('public/images/document/restaurant/'.$document->name))
        {
            $document->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction3');
        }


    }

    public function render()
    {
        $documents=ServiceProviderDocument::where('service_provider_id',$this->restaurantID)
            ->orderBy( 'id', 'desc' )
            ->paginate(2);
        return view('livewire.category.restuarant-setting.documents-component',compact('documents'));
    }
}
