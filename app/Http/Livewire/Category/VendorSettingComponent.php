<?php

namespace App\Http\Livewire\Category;

use App\Models\ServiceProviderBanner;
use Livewire\Component;

class VendorSettingComponent extends Component
{
    public $vendorID;
    public $vendorBanners;

    public function mount()
    {
        $this->vendorBanners=ServiceProviderBanner::where('service_provider_id',$this->vendorID)
            ->orderBy('id','desc')->get();
//                dd($this->vendorBanners);

    }
    public function render()
    {
        return view('livewire.category.vendor-setting-component');
    }
}
