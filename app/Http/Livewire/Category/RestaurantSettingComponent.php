<?php

namespace App\Http\Livewire\Category;

use App\Models\ServiceProviderBanner;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class RestaurantSettingComponent extends Component
{
    public $restaurantID;
    public $restaurantBanners;
    protected $listeners = ['bannerAdded' => 'incrementBanners'];
//    protected $listeners = ['reviewSectionRefresh' => '$refresh'];

    public function incrementBanners($newBanner)
    {
//        dd($this->restaurantBanners->push($newBanner),$newBanner);
//
        $this->restaurantBanners=ServiceProviderBanner::where('service_provider_id',$this->restaurantID)
            ->orderBy('id','desc')->get();
//        $this->restaurantBanners->push($newBanner);
//        dd($this->restaurantBanners);

    }

    public function mount()
    {
        $this->restaurantBanners=ServiceProviderBanner::where('service_provider_id',$this->restaurantID)
            ->orderBy('id','desc')->get();
//                dd($this->restaurantBanners);

    }
    public function render()
    {
//        dd('tst');
        return view('livewire.category.restaurant-setting-component');
    }
}
