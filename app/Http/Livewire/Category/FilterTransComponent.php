<?php

namespace App\Http\Livewire\Category;

use App\Models\filterTranslate;
use App\Models\Language;
use Livewire\Component;
use Livewire\WithPagination;

class FilterTransComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $filterId;
    public $filterEn;
    public $langs;
    public $transId;
    public $mainFilter;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $type ;


    public function messages()
    {
        return [
            'trans.required' => trans( 'filter.validateTrans' ),
            'selectedLang.required' => trans( 'filter.validate_selectedLang' ),

        ];
    }
    public function submit(){
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);

        $lang=Language::find($this->selectedLang);
        $newtrans=filterTranslate::create([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,
            'filter_id' => $this->filterId,
            'type' => $this->filterEn->type,
        ]);



//        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();
        $this->emit('newFilterTransAdded');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function restFilde(){
        $this->trans="";
        $this->selectedLang="";
    }
    public  function edit($filter)
    {
        $this->mainFilter=$filter;
        $this->trans=$filter['name'];
        $this->selectedLang=$filter['lang']['id'];
        $this->selectedLangName=$filter['lang']['name'];
        $this->transId=$filter['id'];
    }

    public function update()
    {

        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);
        $lang=Language::find($this->selectedLang);
        $filter=filterTranslate::find($this->mainFilter['id']);
        $filter->update(['name'=>$this->trans ,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,]);
        $this->restFilde();
        $this->emit('filterTransUp');
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function deleteCall($id)
    {
        $this->transId=$id;
    }
    public function deleteAction()
    {
        $filter=filterTranslate::find($this->transId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }



    public function mount()
    {
        $this->filterEn=filterTranslate::where('filter_id',$this->filterId)
            ->where('code_lang','en')
            ->first();
        $this->type=$this->filterEn->type;
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');

    }
    public function render()
    {
        $translations=filterTranslate::with('filter','lang')
            ->where('filter_id',$this->filterId)
            ->where('code_lang','!=','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.filter-trans-component',compact('translations'));
    }
}
