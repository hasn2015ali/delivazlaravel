<?php

namespace App\Http\Livewire\Category;

use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\ProductFilter;
use App\Models\ProductFilterTrans;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithLanguegesTrait;
class ProductFilterComponent extends Component
{
    use WithPagination ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    public $filterID;
    public $type;
    public $translations;
    public $name;
    public $productFilter;
    public $productFilterId;
    public $langs;
    public $languegeCount;
    public $mainFilter;
    public $FilterMenu;

    public function messages()
    {
        return [
            'name.required' => trans( 'filter.validate' ),
        ];
    }
    public function submit(){
        $this->validate([
            'name' => 'required',
        ]);

        $en_lang=Language::where('code','en')->first();
        $productFilter= ProductFilter::create([
            'filter_id' => $this->filterID,
        ]);
        $filter_trans=ProductFilterTrans::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'filter_id' => $this->filterID,
            'product_filter_id' => $productFilter->id,
        ]);

        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newProductFilterAdded');
    }
    public function restFilde()
    {
        $this->name="";
    }
    public  function edit($filter)
    {
        $this->productFilter=$filter;
        $this->name=$filter['name'];
    }

    public function update(){
        $this->validate([
            'name' => 'required',
        ]);
        $filter=ProductFilterTrans::find($this->productFilter['id']);
        $filter->update(['name'=>$this->name]);
        $message=trans( 'masterControl.updated_successfully' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('ProductFilterUp');
    }
    public function deleteCall($id){
        $this->productFilterId=$id;
    }
    public function deleteAction()
    {
        $filter=ProductFilter::find($this->productFilterId);
        $filter->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }


    public function showTranslation($id)
    {
        $this->translations=ProductFilterTrans::where('product_filter_id',$id)->get('code_lang');
//        dd( $this->translations);

    }

    public function mount()
    {
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->languegeCount=$this->getAllLanguegesCount();
        $this->mainFilter=filterTranslate::where('code_lang','en')
            ->where('filter_id',$this->filterID)
            ->first();
        $this->FilterMenu=filterTranslate::where('code_lang','en')
            ->where('type',2)
            ->get();
//        dd($this->FilterMenu,$this->type);

    }
    public function render()
    {
        $filters=ProductFilterTrans::where('filter_id',$this->filterID)
            ->where('code_lang','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.category.product-filter-component',compact('filters'));
    }
}
