<?php

namespace App\Http\Livewire\Category;

use App\Events\ServiceProviderJoined;
use App\Mail\AcceptedRegister;
use App\Models\ServiceProvider;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithCountryAndCity;
use App\Traits\WithImageValidationTrait;
use App\Traits\WithRestaurantTrait;
use App\Traits\WithUserValidationTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class NewVendorsComponent extends Component
{
    use WithPagination ,WithCountryAndCity ,WithUserValidationTrait
        ,WithRestaurantTrait, WithFileUploads, WithImageValidationTrait;
    protected $paginationTheme = 'bootstrap';
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

//    public $countOFNewVendor;
    public $vendorID;
    public $allVendorFilter;

    public $minimumOrderAmount;
    public $deliveryMode;
    public $deliveryManCommission;
    public $pickCommission;
    public $registrationState;
    public $selectedVendorFilters=[];
    public $registeredVendor;

    protected $listeners = [
        'getVendors'=>'render',
//        'getItemByFilter'=>'render',
//        'refresh'=>'$refresh'
    ];


    public function hydrate()
    {
//        $this->resetErrorBag();
        $this->resetValidation();
    }
    public function messages()
    {


        return [

            'registrationState.required' => trans( 'vendor.validateRegistrationState' ),
            'selectedVendorFilters.required' => trans( 'vendor.validateSelectedVendorFilters' ),
        ];
    }

    public function updateCall($vendorID)
    {
        $this->restFilde();
        $this->vendorID=$vendorID;
        $this->registeredVendor=ServiceProvider::with('user:users.id,users.phone,users.email,users.firstName,users.fatherName,users.lastName')
            ->find($vendorID);
    }
    public function updateRegistration()
    {
        $this->registrationState==true?$state=1:$state=0;
        $this->validate([
            'registrationState' => 'required',
            'selectedVendorFilters' => 'array|required',
        ]);
//        dd($this->registrationState,$state);
         $this->registeredVendor=ServiceProvider::find($this->vendorID);
         $this->registeredVendor->mode= $this->deliveryMode;
         $this->registeredVendor->delivery_man_commission=$this->deliveryManCommission;
         $this->registeredVendor->pick_commission=$this->pickCommission;
         $this->registeredVendor->registration_state=$state;
         $this->registeredVendor->minimum_order_amount=$this->minimumOrderAmount;
         $this->registeredVendor->save();
         $this->registeredVendor->filters()->attach($this->selectedVendorFilters);
        $this->restFilde();
        $this->emitTo('category.new-vendor-count-component', 'updateNewVendorCount');
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('updateRegistration');
        $this->emitTo('category.vendor-component', 'updateNewVendorCount');
        $text=__("serviceProvider.textSendWhenActiveRegistration");
        if( $this->registeredVendor->user->email!=null)
        {
//            dd('lll');
            Mail::to( $this->registeredVendor->user->email)->send(new AcceptedRegister($text));
//            dd('lll')
        }
        if( $this->registeredVendor->user->phone!=null)
        {
            $user1=User::find( $this->registeredVendor->user_id);
            try {
                $user1->notify(new ClickSendTest($text));
            } catch (\Exception $e) {
                // do something when error
//                session()->put('mobileMessage', trans('userFront.FailedToSendCodeToPhone'));
            }

        }


    }

    public function mount()
    {

//       $this->countOFNewVendor=$this->getNewVendorCount();
//       dd($this->countOFNewVendor);
    }
    //    }
    public function deleteCall($id){
        $this->vendorID=$id;
    }
    public function deleteAction()
    {
        $res=ServiceProvider::with('user')->find($this->vendorID);
        $userId=$res->user->id;
        $user=User::find($userId);
        $user->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        event(new ServiceProviderJoined("ven"));
//        $this->emitTo('category.new-vendor-count-component', 'updateNewVendorCount');
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction2');
        $this->emitTo('category.vendor-component', 'updateNewVendorCount');
    }

    public function restFilde()
    {
        $this->minimumOrderAmount=null;
        $this->deliveryMode=null;
        $this->deliveryManCommission=null;
        $this->pickCommission=null;
        $this->selectedVendorFilters=[];
        $this->registrationState=null;
    }

    public function render()
    {
        $user=Auth::user();
        if($user->role_id==2)
        {
            $newVendors = ServiceProvider::with('user', 'country', 'country.translation', 'city', 'city.translation')
                ->where('type', 'ven')
                ->where('registration_state', 0)
                ->paginate(10);
        }
        elseif ($user->role_id==8)
        {
            $newVendors=ServiceProvider::with('user', 'country', 'country.translation', 'city', 'city.translation')
                ->whereHas('city', function (Builder $query) use ($user) {
                    $query->where('cities.id',  $user->city_id);
                })
                ->where('registration_state',0)
                ->where('type','ven')
                ->paginate(10);
        }
        return view('livewire.category.new-vendors-component',compact('newVendors'));
    }
}
