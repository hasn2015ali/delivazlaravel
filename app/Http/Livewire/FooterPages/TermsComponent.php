<?php

namespace App\Http\Livewire\FooterPages;

use App\Models\CountryTerm;
use App\Models\CountryTermTrans;
use App\Models\Language;
use App\Traits\WithCountryAndCity;
use App\Traits\WithLanguegesTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class TermsComponent extends Component
{
    use WithPagination ,WithCountryAndCity ,WithLanguegesTrait;
    protected $paginationTheme = 'bootstrap';
    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

    //vars for With Langueges Trait
    public $translations;

    public $countryName;
    public $countryId=0;

    public $termID;
    public $address;
    public $content;
    public $langs;
    public $languegeCount;
    public $term;

    // var for translation

//    public $selectedLangName;
    public $allInsertedTrans=[];
    public $addressTrans;
    public $contentTrans;
    public $selectedLang;
    public $editTransIndex=null;
    public $deleteTransIndex=null;
    public $trans;
    public $newTerm;

    public $type;




    protected $listeners = [
        'getCities' => '$refresh',
        'editTranslation' => '$refresh',
        'deleteTranslation' => '$refresh',
    ];

    public function messages()
    {

        return [
            'address.required' => trans( 'footerPages.validateAddress' ),
            'content.required' => trans( 'footerPages.validateContent' ),
            'addressTrans.required' => trans( 'footerPages.validateAddressTrans' ),
            'contentTrans.required' => trans( 'footerPages.validateContentTrans' ),
            'selectedLang.required' => trans( 'footerPages.validateLang' ),
            'allInsertedTrans.*.address.required' => trans( 'footerPages.validateAddress' ),
            'allInsertedTrans.*.content.required' => trans( 'footerPages.validateContent' ),
        ];
    }

    public function openEdit($index)
    {
//        dd($index);
        $this->editTransIndex=$index;
    }

    public function openDelete($index)
    {
        $this->deleteTransIndex=$index;

    }

    public function editTranslation($index)
    {
//        dd($index);
        $this->validate([
            'allInsertedTrans.*.address' => 'required',
            'allInsertedTrans.*.content' => 'required',
        ]);
        $term=$this->allInsertedTrans[$index];
//        dd($this->allInsertedTrans[$index]['address']);
        $this->trans=CountryTermTrans::find($term['id']);
        $this->trans->update([
            'address' => $this->allInsertedTrans[$index]['address'],
            'content' => $this->allInsertedTrans[$index]['content'],
            ]);
        $this->trans=null;
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->getAllInsertedTranslation($term['term_id']);
        $this->closeEdit();
    }
    public function deleteTranslation($index)
    {
//        dd($index,$this->allInsertedTrans[$index]);
        $term=$this->allInsertedTrans[$index];
//        dd($this->allInsertedTrans[$index]['address']);
        $this->trans=CountryTermTrans::find($term['id']);
        $this->trans->delete();
        $this->trans=null;
        $this->getAllInsertedTranslation($term['term_id']);
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->closeDelete();

    }

    public function getAllInsertedTranslation($termID)
    {
        $this->allInsertedTrans=CountryTermTrans::with('countryTerm')
            ->where('term_id', $termID)
            ->where('type',$this->type)
            ->orderBy('id','desc')
            ->get()
            ->toArray();
    }

    public function closeEdit()
    {
        $this->editTransIndex=null;

    }
    public function closeDelete()
    {
        $this->deleteTransIndex=null;

    }

    public function getAllTranslation($termID)
    {
        $this->getAllInsertedTranslation($termID);
        $this->term=CountryTermTrans::with('countryTerm')
            ->where('term_id', $termID)
            ->first();
    }

    public function submitTranslation ()
    {
//        dd( $this->selectedLangName);
        $this->validate([
            'addressTrans' => 'required',
            'contentTrans' => 'required',
            'selectedLang' => 'required',
        ]);

        CountryTermTrans::create([
            'country_id' =>  $this->term->country_id,
            'address' => $this->addressTrans,
            'code_lang' => $this->selectedLang,
            'term_id' =>  $this->term->term_id,
            'content' => $this->contentTrans,
            'type' => $this->type,
        ]);


        $this->getAllInsertedTranslation($this->term->term_id);

        // to set translations in Translation Trait
        $this->showTranslation($this->term->term_id);

        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
//        $this->emit('newTermAdded');

    }
    public function submit()
    {
//        dd($this->countryId);
        $this->validate([
            'address' => 'required',
            'content' => 'required',
        ]);

        $this->newTerm= CountryTerm::create([
            'country_id' => $this->countryId,
            'type' => $this->type,
        ]);
        CountryTermTrans::create([
            'country_id' => $this->countryId,
            'address' => $this->address,
            'code_lang' => "en",
            'term_id' => $this->newTerm->id,
            'content' => $this->content,
            'type' => $this->type,
        ]);
        $this->newTerm=null;
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newTermAdded');
    }
    public function showTranslation($id)
    {
        $this->translations=CountryTermTrans::where('term_id',$id)->get('code_lang');

    }
    public function getCities($countryID,$countryName)
    {
        if($countryID==0)
        {
            $this->countryName=__("footerPages.TermsForAllCountry");
            $this->countryId=0;
        }
        else
        {
            $this->countryName=$countryName;
            $this->countryId=$countryID;
//            $this->getCitiesForCountry($this->countryId);
        }

    }

    public function setCountryConfig()
    {
      $this->restFilde();
      if($this->countryId==null)
      {
          $this->countryId=0;
      }
    }

    public function restFilde()
    {
        $this->address=null;
        $this->content=null;
        $this->addressTrans=null;
        $this->contentTrans=null;

    }

    public function edit($termID)
    {
        $this->termID=$termID;
        $this->term=CountryTermTrans::where('term_id',$this->termID)->where('code_lang' , "en")->first();
        $this->address=$this->term->address;
        $this->content=$this->term->content;
    }

    public function UpdateTerm()
    {
        $this->validate([
            'address' => 'required',
            'content' => 'required',
        ]);

        $this->term->update([
            'address' => $this->address,
            'content' => $this->content,
        ]);
        $this->restFilde();
        $this->term=null;
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('TermUp');
    }



    public function mount()
    {
//        dd($this->type);
        $this->getCountries();
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->languegeCount=$this->getAllLanguegesCount();
//        $this->countryName=__("footerPages.TermsForAllCountry");

    }

    public function callDelete($id){
        $this->termID=$id;
    }
    public function deleteAction(){
        $this->term=CountryTerm::find($this->termID);
         $this->term->delete();
         $this->term=null;
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }


    public function render()
    {

//        dd('ttt');
//        $this->countryName==null?  $this->countryName=__("footerPages.TermsForAllCountry") : $this->countryName;
//        dd($this->countryId);
        $countryTerms= CountryTermTrans::with('countryTerm')
            ->where('country_id', $this->countryId)
            ->where('code_lang',"en")
            ->where('type',$this->type)
            ->orderBy('id','desc')
            ->paginate(10);
//        dd($countryTerms);
        return view('livewire.footer-pages.terms-component',compact('countryTerms'));
    }
}
