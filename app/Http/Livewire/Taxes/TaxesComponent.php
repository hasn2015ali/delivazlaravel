<?php

namespace App\Http\Livewire\Taxes;

use App\Models\Taxs;
use App\Traits\WithCountryAndCity;
use Livewire\Component;
use Livewire\WithPagination;

class TaxesComponent extends Component
{
    use WithPagination ,WithCountryAndCity ;
    protected $paginationTheme = 'bootstrap';
    public $countries;
    public $name;
    public $taxType;
    public $selectedCountry=[];
    public $taxValue;
    public $idDelete;
    public $taxID=null;
    public $attributes;

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }

    public function messages()
    {
        return [
            'name.required' => trans( 'country.validate' ),
        ];
    }
    public function restValues()
    {
        $this->name=null;
        $this->taxType=null;
        $this->taxValue=null;
        $this->selectedCountry=[];
    }
    public function edit($tax)
    {
//        dd($tax,collect($tax['countries'])->pluck('id')->toArray());
        $this->taxID=$tax['id'];
        $this->taxValue=$tax['value'];
        $this->taxType=$tax['type'];
        $this->name=$tax['name'];
        $this->selectedCountry=collect($tax['countries'])->pluck('id')->toArray();

//        dd($this->selectedCountry);
    }
    public function callDelete($id){
        $this->idDelete=$id;
    }
    public function deleteAction(){
        $tax= Taxs::find($this->idDelete);
        $tax->delete();
        $tax->countries()->detach();
        $tax=null;
        $this->idDelete=null;
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }
    public function submitNew($formData)
    {
        dd($formData, $this->selectedCountry);
    }
    public function submitTax()
    {

        if(in_array(0,$this->selectedCountry))
        {
            $this->selectedCountry=[];
        }
//        $this->selectedCountry==0?$this->selectedCountry=[];
//        dd($this->selectedCountry);
       $this->validate([
            'name' => 'required',
            'taxType' => 'required',
            'taxValue' => 'required',
        ]);

        $newTax=Taxs::updateOrCreate(['id' => $this->taxID],[
            'name' => $this->name,
            'type' => $this->taxType,
            'value' => $this->taxValue,

        ]);
//        dd($newTax->countries,$this->taxID);
        if(count($this->selectedCountry)>0)
        {
            if(count($newTax->countries)>0)
            {
                $newTax->countries()->detach();
            }
            $newTax->countries()->attach($this->selectedCountry);
        }
        $this->restValues();
        $this->emit('newTaxAdded');
        if( $this->taxID!=null)
        {
            $message=trans( 'masterControl.updated_successfully' );
            $this->taxID=null;
        }
        else
        {
            $message=trans( 'masterControl.successfully_Added' );
        }
        $newTax=null;
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function mount()
    {

        $this->getCountries();
//        dd($this->countries);
    }
    public function render()
    {
        $taxes=Taxs::with('countries')
            ->orderBy('id','asc')
//            ->get();
            ->paginate(10);
        return view('livewire.taxes.taxes-component',compact('taxes'));
    }
}
