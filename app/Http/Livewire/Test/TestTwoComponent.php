<?php

namespace App\Http\Livewire\Test;

use Livewire\Component;

class TestTwoComponent extends Component
{

    public $visible = false;


    public function open()
    {

        $this->visible = true;
    }

    public function close()
    {

        $this->visible = false;
    }

    public function render()
    {
        return view('livewire.test.test-two-component');
    }
}
