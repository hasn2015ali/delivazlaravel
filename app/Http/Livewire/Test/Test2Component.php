<?php

namespace App\Http\Livewire\Test;

use Livewire\Component;

class Test2Component extends Component
{
    public $visible = false;

    public function open()
    {

        $this->visible = true;
    }

    public function close()
    {



        $this->visible = false;
    }

    public function render()
    {
        return view('livewire.test.test2-component');
    }
}
