<?php

namespace App\Http\Livewire\Test;

use App\cart\CartFacade;
use App\Models\Country;
use App\Models\Food;
use App\Models\FoodTranslation;
use App\Models\ServiceProvider;
use App\Models\User;
use App\Traits\WithCartTrait;
use Illuminate\Support\Facades\DB;
//use League\CommonMark\Cursor;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Pagination\Cursor;
use Illuminate\Support\Collection;
//use MongoDB\Driver\Cursor;

class TestComponent extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $photo;
    protected $paginationTheme = 'bootstrap';
//    use WithCartTrait;
    public $cart1=[];
    public $cart;
    public $cartContent;
    public   $additions =[];
    public $additionIDs=[];
    protected $listeners = [
//        'addToBasket' => '$refresh',
//        'setAddition' => 'setAddition',
    ];


    public $selectedIntervals="Daily";
    public $selectedIntervalsTranslated;
    public $ongoingState=false;
    public $scheduleSelected=false;


    public function setScheduleSelected()
    {

        $this->scheduleSelected=true;
    }



    public function changeOngoingState()
    {

        $this->ongoingState==true?$this->ongoingState=false:$this->ongoingState=true;
        $this->dispatchBrowserEvent('initialize-date-picker');
    }
    public function selectIntervals($interval)
    {
        if($interval=="Daily"){
            $this->selectedIntervalsTranslated= __("checkout.Daily");
        }
        elseif ($interval=="EveryTwoDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryTwoDays");

        }
        elseif ($interval=="EveryThreeDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryThreeDays");

        }
        elseif ($interval=="EveryFourDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryFourDays");

        }
        elseif ($interval=="EveryFiveDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryFiveDays");

        }
        elseif ($interval=="EverySixDays"){
            $this->selectedIntervalsTranslated= __("checkout.EverySixDays");

        }
        elseif ($interval=="EveryWeek"){
            $this->selectedIntervalsTranslated= __("checkout.EveryWeek");

        }

        $this->selectedIntervals=$interval;
        $this->dispatchBrowserEvent('close-drop-down');
//        dd($interval);
    }
    public function render()
    {
        $this->selectedIntervalsTranslated= __("checkout.Daily");
        //where tips
//        $q->where(function ($query) {
//
//            $query->where('gender', 'Male')
//
//                ->where('age', '>=', 18);
//
//        })->orWhere(function($query) {
//
//            $query->where('gender', 'Female')
//
//                ->where('age', '>=', 65);
//
//        })

//        $q->where('a', 1);
//
//        $q->orWhere(['b' => 2, 'c' => 3]);

//        $names = User::all()->reject(function ($user) {
//            return $user->role_id == 1;
//        })->map(function ($user) {
//            return $user->firstName;
//        });
//        $users =User::with('city','country')->get();
        $users =User::whereRole_id(1)->get()->sortByDesc('firstName');
//        dd($users);
//        $users = $users->unique();
//        $users =User::all()->reject(function ($user) {
//        return $user->role_id == 1;
//    })->map(function ($user) {
//            return $user->id;
//        });
//        $users = $users->except([1, 2, 3,7,101,102,103,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]);
//        $users = $users->load('country','city');
//        $users = $users->loadMissing('country','city');


//        $users = $users->diff(User::whereIn('id', [1, 2, 3,7,101,102,103,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28])->get());
//        dd($users);

//        $collection = collect([1, 'false', 2, 3, 4]);
//
//        $filtered = $collection->filter(function ($value, $key) {
//            if($value==2)
//            {
//                return false;
//            }
//            return $value ;
//        });
        $e=null;
//       $e= ServiceProvider::cursorPaginate(15);
//        $e1= ServiceProvider::Paginate(10);

//        $e = DB::table('users')->orderBy('id')->cursorPaginate(15);
//        $collection = collect([1, 2, 3, null, false, '', 0, []]);
//
//        $collection->filter()->all();
//       dd($collection);
//        dd($e,$e1);
//        $users=DB::table('user_details')->paginate(15);
//        dd($users);

        return view('livewire.test.test-component',compact('users'));
    }
}
