<?php

namespace App\Http\Livewire\Template\Action;

use App\Helpers\GuestAddresses\AddressesFacade;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DeleteComponent extends Component
{
    public $itemID;
    public $modelName;
    public $userID=null;

    public function deleteAction()
    {

        if($this->modelName=='App\Models\OrderAddress' and $this->userID==null)
        {
            AddressesFacade::remove($this->itemID);
            $this->emitUp('itemDeleted');
            $this->dispatchBrowserEvent('close-delete-toast');
            return;
//            dd('kkk');
        }
        $item=$this->modelName::find($this->itemID);
        $item->delete();
        $item=null;
        $this->emitUp('itemDeleted');
        $this->dispatchBrowserEvent('close-delete-toast');

//        $this->modelName=null;
//        $this->itemID=null;

//        $this->modelName= str_replace('"', '', $this->modelName);
//        $item="";
//        dd($this->itemID,$this->modelName,$item);

    }

    public function mount()
    {
        if(Auth::check())
        {
            $this->userID=Auth::id();
        }
    }
    public function render()
    {
        return view('livewire.template.action.delete-component');
    }
}
