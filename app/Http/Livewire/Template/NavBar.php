<?php

namespace App\Http\Livewire\Template;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NavBar extends Component
{
    public $city;
    public $country;
    public $counter = 0;
    public $user;

    protected $listeners = [
        'userUpdated'=>'mount'
    ];
    public function mount()
    {
        if(Auth::check()){
            $this->user = User::find(Auth::id());
            if ($this->user->role_id == 1 || $this->user->role_id == 9 || $this->user->role_id == 10) {
                if ((isset($this->user->email) && !$this->user->email_verified_at)) {
                    $this->counter = $this->counter + 1;
                }
                if (isset($this->user->phone)) {
                    if ($this->user->phone != 0 && !$this->user->phone_verified) {
                        $this->counter = $this->counter + 1;

                    }
                }
            }
        }


    }

    public function render()
    {
        return view('livewire.template.nav-bar');
    }
}
