<?php

namespace App\Http\Livewire\Template;

use Livewire\Component;

class Mobile extends Component
{
    public function render()
    {
        return view('livewire.template.mobile');
    }
}
