<?php

namespace App\Http\Livewire\Template\AnyThing;

use App\Traits\WithCountryAndCity;
use Carbon\CarbonImmutable;
use Carbon\CarbonPeriod;
use Illuminate\Support\Carbon;
use Livewire\Component;
use Livewire\WithFileUploads;

class AnyThingComponent extends Component
{
    use WithFileUploads,WithCountryAndCity;
    public $country;
    public $city;
    public $photo;
    public $openPhotoToast=false;
    public $confrimed=false;
    public $less=true;
    public $between=false;
    public $between500=false;
    public $between800=false;
    private $today;
    public $tomorrow;
    public $nextTwo;
    public $nextThree;
    public $nextFour;
    public $nextFive;
    public $nextSex;
    public $daySlots=[];
    public $showSlots=false;
    public $selectedDay;
    public $selectedSlot;
    public $anyThingText;
    public $currencyCode;



    public function setSlot($slot)
    {
      $this->selectedSlot=$slot;
//      dd($this->selectedSlot);
    }
    public function setDay($dayNumber)
    {
//        dd($dayNumber);
        if($dayNumber=='ASAP')
        {
//            dd('dd');
            $this->selectedDay="ASAP";
            $this->closeSlots();
        }
        elseif($dayNumber=='Today')
        {
            $this->selectedDay=$dayNumber;
            $this->showSlots();
        }
        elseif($dayNumber=='Tomorrow')
        {
            $this->selectedDay=$dayNumber;
            $this->showSlots();
        }
        elseif($dayNumber!='ASAP' and $dayNumber!='Today' and$dayNumber!='Tomorrow'  )
        {
            $this->selectedDay=$dayNumber;
            $this->showSlots();
        }
//        dd($this->selectedDay,$dayNumber);

    }
    public function showSlots()
    {
        $this->showSlots=true;
    }
    public function closeSlots()
    {
        $this->showSlots=false;
    }

    public function mount()
    {
//        $this->today = Carbon::today();
//        $this->tomorrow = Carbon::tomorrow();
        $this->currencyCode=$this->getCountryCurrency();
        $this->today = CarbonImmutable::now();
        $this->tomorrow = $this->today->add(1, 'day')->format('D j M ');
        $this->nextTwo = $this->today->add(2, 'day')->format('D j M ');
        $this->nextThree = $this->today->add(3, 'day')->format('D j M ');
        $this->nextFour = $this->today->add(4, 'day')->format('D j M ');
        $this->nextFive = $this->today->add(5, 'day')->format('D j M ');
        $this->nextSex = $this->today->add(6, 'day')->format('D j M ');

//        dd($this->tomorrow);
//        toFormattedDateString
//        format('l jS \of F ')
//        dd($this->tomorrow->format('D jS \of M '),$this->today->format('D jS \of M ')
//            ,$this->nextTwo->format('D jS \of M '),$this->nextThree->format('D jS \of M ')
//            ,$this->nextFour->format('D jS \of M '),$this->nextFive->format('D jS \of M '));

        $period = new CarbonPeriod('00:00', '30 minutes', '23:00'); // for create use 24 hours format later change format
        foreach($period as $key=> $item){
//            dd($key);
            if($key==0)
            {
                $start=$item->format("H:i");
            }
            else
            {
                $end=$item->format("H:i");
                $slot=$start." - ".$end;
                array_push($this->daySlots,$slot);
                $start=$end;

            }
        }
//        dd($this->daySlots);

    }
    public function setLessActive()
    {
//        dd('ii');
        $this->less=true;
        $this->between=false;
        $this->between500=false;
        $this->between800=false;
    }
    public function setBetweenActive()
    {
        $this->less=false;
        $this->between=true;
        $this->between500=false;
        $this->between800=false;
//        dd($this->between);
    }
    public function setBetween500Active()
    {
        $this->less=false;
        $this->between=false;
        $this->between500=true;
        $this->between800=false;
    }
    public function setBetween800Active()
    {
        $this->less=false;
        $this->between=false;
        $this->between500=false;
        $this->between800=true;
    }

    public function addPhoto()
    {
        $this->confrimed=true;
        $this->openPhotoToast=false;
    }
    public function closeToast()
    {
        $this->openPhotoToast=false;
        $this->photo=null;
    }

    public function removePhoto()
    {
        $this->photo=null;
        $this->confrimed=false;
    }
    public function render()
    {
//        dd($this->country,$this->city);
        return view('livewire.template.any-thing.any-thing-component');
    }
}
