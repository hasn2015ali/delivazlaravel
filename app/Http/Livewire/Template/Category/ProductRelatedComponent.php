<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\Food;
use App\Models\Product;
use Livewire\Component;

class ProductRelatedComponent extends Component
{
    public $item;
    public $relatedItems;
    public $serviceProviderID;
    public $filter;
    public $type;
    public $folders;
    public $country;
    public $city;
    public $serviceProvider;



    public function mount()
    {
//        dd($this->serviceProvider);
        $this->serviceProviderID=$this->item->service_provider_id;
        $this->filter=$this->item->filters->first();
        if($this->type=="food")
        {
            $this->folders="storage/images/food/";
            $this->relatedItems= Food::with('translation')->whereHas('filters', function ($query) {
                $query->where('meal_filters.id', $this->filter->id);
            })->where('service_provider_id', $this->serviceProviderID)
                ->orderBy('id', 'desc')
                ->get()
                ->take(5);
        }
        if($this->type=="product")
        {
            $this->folders="storage/images/product/";
            $this->relatedItems=Product::with('translation')->whereHas('filters', function ($query)  {
                $query->where('product_filters.id', $this->filter->id);
            })->where('service_provider_id', $this->serviceProviderID)
                ->orderBy('id', 'desc')
                ->get()
                ->take(5);

        }

//        $this->relatedItems=
//        dd($this->relatedItems,$this->filter);
    }
    public function render()
    {
        return view('livewire.template.category.product-related-component');
    }
}
