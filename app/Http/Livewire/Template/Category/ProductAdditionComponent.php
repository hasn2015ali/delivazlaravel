<?php

namespace App\Http\Livewire\Template\Category;


use App\Helpers\Cart\CartAdditions\AdditionsFacade;
use App\Traits\WithServiceProviderTrairt;
use Livewire\Component;

class ProductAdditionComponent extends Component
{
    use  WithServiceProviderTrairt;
    public $addition=[];
    public $foldersForAddition;
    public $currencyCode;
    public $qtn=0;
    public $delivaz_commission;
    public $priceAfterCommission;


    public function minusQuantity()
    {

//        dd(AdditionsFacade::get());
        $this->qtn==0?$this->qtn:$this->qtn--;
        if($this->qtn==0)
        {
            AdditionsFacade::remove($this->addition['id']);

        }else
        {
            AdditionsFacade::update($this->addition['id'],0);

        }
        $this->emitTo('template.category.product-component', 'getNewPrice');
//        $this->emitUp('postAdded');


    }
    public function plusQuantity()
    {
//        AdditionsFacade::getOldQuantity($this->addition['id']);
//        dd(AdditionsFacade::get(),AdditionsFacade::getOldQuantity($this->addition['id']),$this->addition['id']);
//        dd(AdditionsFacade::get(),$this->qtn);
        if($this->qtn==0)
        {
            $this->qtn++;

            AdditionsFacade::add($this->addition['id'], $this->priceAfterCommission,
                $this->addition['name'], $this->qtn);

        }else
        {
            $this->qtn++;
            AdditionsFacade::update($this->addition['id'],1);
        }
//        $this->oldQtn=$this->qtn;
        $this->emitTo('template.category.product-component', 'getNewPrice');

//        dd($this->qtn,$this->oldQtn);
//        dd(AdditionsFacade::get());




    }
    public function mount()
    {
        $this->priceAfterCommission=$this->addition['price'];
//        $this->priceAfterCommission=$this->calculatePriceWithCommission( $this->addition['price'],$this->delivaz_commission);

    }
    public function render()
    {
        $this->qtn=AdditionsFacade::getOldQuantity($this->addition['id']);
//        dd($this->addition);
        return view('livewire.template.category.product-addition-component');
    }
}
