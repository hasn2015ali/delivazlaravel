<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\ServiceProviderBanner;
use Livewire\Component;

class ServiceProviderBanners extends Component
{
    public $serviceProvider;
    public $banners;
    public $folders;
    public function mount()
    {
        $this->banners=ServiceProviderBanner::where('service_provider_id',$this->serviceProvider->id)
            ->where('state',1)
            ->get();
        if($this->serviceProvider->type=="ven")
        {
            $this->folders="storage/images/sliders/vendor/";

        }
        if($this->serviceProvider->type=="res")
        {
            $this->folders="storage/images/sliders/restaurant/";

        }
//        dd($this->banners,$this->serviceProvider->type);
    }
    public function render()
    {
        return view('livewire.template.category.service-provider-banners');
    }
}
