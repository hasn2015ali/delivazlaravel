<?php

namespace App\Http\Livewire\Template\Category;


use App\Helpers\Cart\CartFacade;
use App\Models\Food;
use App\Models\Product;
use App\Models\UserCart;
use App\Traits\WithServiceProviderTrairt;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AddToBasketComponent extends Component
{
    use WithServiceProviderTrairt;

    public $filterType;
    public $item;
    public $currencyCode;
    public $qtn = 1;
    public $itemType;
    public $itemID;
    public $itemIdsInCart = [];
    public $itemIdWithType;
    public $productDetails=[];
    public $serviceProvider;
    public $city_id;
    public $country_id;
    public $itemsInCart;
//    public $serviceProviderInCart;
    protected $listeners = [
        'removeFromCart' => 'generateArrayOfItemsIDSInBasket',
////        'getItemByFilter'=>'render',
        'refresh' => '$refresh'
    ];


    public $delivazCommission;
    public $priceWithCommission;


    public function minusQuantity()
    {
        $this->qtn=$this->qtn-1;
    }
    public function plusQuantity()
    {
        $this->qtn=$this->qtn+1;
    }

    public function addToBasket()
    {
//        dd($this->country_id,$this->city_id);
//        $cart=CartFacade::get();
//        dd($cart);

        $cityCheck=CartFacade::checkItemCityAndCountry($this->city_id,$this->country_id,$this->itemsInCart);
        if(!$cityCheck){
            $message = trans('serviceProvider.cityCheck');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
//        dd($cityCheck);
        $this->generateArrayOfItemsIDSInBasket();
        if (in_array($this->itemIdWithType, $this->itemIdsInCart)) {
            $message = trans('serviceProvider.AlreadyAdded');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if ($this->filterType == "res") {
            $productItem = Food::with('translation')->find($this->item->id);
        }
        if ($this->filterType == "ven") {
            $productItem = Product::with('translation')->find($this->item->id);

        }

        $service_provider_id = $productItem->service_provider_id;
        $idDesc = $productItem->id . $this->filterType . rand(10, 1000);
        $size = $productItem->size;
        $photo = $productItem->photo;
        $additions["additions"] = [];
        $name = $productItem->translation->where('code_lang', 'en')->first()->name;
        $this->productDetails['id'] = $idDesc;
        $this->productDetails['itemID'] = $productItem->id;
        $this->productDetails['serviceProviderID'] = $service_provider_id;
        $this->productDetails['price'] = $this->priceWithCommission;
        $this->productDetails['size'] = $size;
        $this->productDetails['photo'] = $photo;
        $this->productDetails['type'] = $this->filterType;
        $this->productDetails['name'] = $name;
        $this->productDetails['country_id'] = $this->country_id;
        $this->productDetails['city_id'] = $this->city_id;
        $this->productDetails['quantity'] = $this->qtn;
        $this->productDetails['additions'] = $additions;

//        dd($this->productDetails);
        if (Auth::check()) {
            $newAddition = json_encode($this->productDetails['additions']);
//            dd($newAddition);
            UserCart::create([
                'idDES' => $this->productDetails['id'],
                'itemID' => $this->productDetails['itemID'],
                'serviceProviderID' => $this->productDetails['serviceProviderID'],
                'price' => $this->productDetails['price'],
                'size' => $this->productDetails['size'],
                'photo' => $this->productDetails['photo'],
                'type' => $this->productDetails['type'],
                'name' => $this->productDetails['name'],
                'quantity' => $this->productDetails['quantity'],
                'country_id' => $this->productDetails['country_id'],
                'city_id' => $this->productDetails['city_id'],
                'additions' => $newAddition,
                'user_id' => Auth::id(),
            ]);
            $newAddition = null;
        } else {
            CartFacade::add($this->productDetails);

        }
        $this->productDetails = [];
        $this->generateArrayOfItemsIDSInBasket();
        $this->emitTo('template.user.cart-component', 'cartUpdated');

    }

    public function mount($filterType, $item, $currencyCode)
    {
//        dd($this->serviceProvider);
        $this->country_id=$this->serviceProvider->city->country_id;
        $this->city_id=$this->serviceProvider->city->id;

        $this->priceWithCommission = $this->calculatePriceWithCommission($item->price, $this->delivazCommission);
        $this->currencyCode = $currencyCode;
        $this->item = $item;
        $this->filterType = $filterType;
        $this->generateArrayOfItemsIDSInBasket();
//        dd($this->filterType,$this->item->id);
//        dd($this->item->id,$this->itemIdWithType,$this->itemIdsInCart,in_array($this->itemIdWithType,$this->itemIdsInCart));
//        dd(CartFacade::getIDForAddedItem($itemInCart));
//dd($currencyCode);
    }

    public function generateArrayOfItemsIDSInBasket()
    {
        if (Auth::check()) {
            $items = UserCart::where('user_id', Auth::id())
                ->get()
                ->toArray();
            $this->itemsInCart = CartFacade::convertAllItemToArray($items);
            $items = null;
        } else {
            $this->itemsInCart = CartFacade::get();
        }

        $this->itemIdsInCart = CartFacade::getIDForAddedItem($this->itemsInCart);
        $this->itemIdWithType = $this->item->id . $this->filterType;
        $itemInCart = [];
    }

    public function render()
    {

        return view('livewire.template.category.add-to-basket-component');
    }
}
