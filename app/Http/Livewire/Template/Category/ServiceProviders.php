<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\filter;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Carbon;
use Livewire\Component;

class ServiceProviders extends Component
{

    use  WithCountryAndCity;

    public $country;
    public $city;
    public $countryID;
    public $cityID;
    public $filter;
    public $recommended=[];
    public $notRecommended=[];
    public $type;
    public $time;
    public $pageType;

    public $filterWithRecommended;
    public $filterWithNotRecommended;

    public $filterID;

    public $obj=[];
    public $currentDay;
    public $currentHour;
    public $currentMinute;
    public $HourStart=null;
    public $MinuteStart=null;
    public $HourEnd=null;
    public $MinuteEnd=null;
    public $objNot=[];
    public $HourStartNot=null;
    public $MinuteStartNot=null;
    public $HourEndNot=null;
    public $MinuteEndNot=null;
    public $year=2021;
    public $month=4;
    public $day1=4;
    public $now;
    public $begintime;
    public $endtime;
    public $nowNot;
    public $begintimeNot;
    public $endtimeNot;

    public $shape;

    protected $listeners = ['getServiceProviderByFilter',
        'pageType',
        'setCurrentDay'
//        'refreshProducts'=>'$refresh'
    ];

    public function setCurrentDay($day,$hour,$minute)
    {
//        dd($day);
        $day==0? $day=7:$day=$day;
        $this->currentHour=$hour;
        $this->currentMinute=$minute;
        $this->currentDay=$day;
//        dd($this->currentDay, $this->currentMinute,$this->currentHour);
    }
    public function pageType()
    {
//        dd($this->pageType);
        $this->dispatchBrowserEvent('page-type', ['page1' => $this->pageType]);
    }


    public function mount()
    {
//        dd($this->shape);
        $this->cityID=$this->getCityId($this->city);
//        dd($this->city,$this->cityID);

    }

    public function getServiceProviderByFilter($filterID)
    {
//        $this->filterID=$filterID;
//        dd($filterID);
//        $this->filter=filter::with(['serviceProvider:id,name,avatar,state,isRecommended',
//            'serviceProvider.partTime:day_id,day_state,startTimeHour,startTimeMinutes,endTimeHour,endTimeMinutes,service_provider_id'])
//            ->select('id','type')
//            ->findOrFail($filterID);

//        $this->filter->type==1 ? $this->type=__('serviceProvider.Restaurants'):$this->type=__('serviceProvider.Vendors');
//        $this->filter->type==1 ? $this->pageType="Restaurants":$this->pageType="Shops";
//        $this->recommended=$this->filter->serviceProvider()->where('isRecommended',1)->get();
//        $this->notRecommended=$this->filter->serviceProvider()->where('isRecommended',0)->get();




//        dd($this->recommended,$this->notRecommended);

        //best query
//        $users = App\User::with(['posts' => function ($query) {
//            $query->where('title', 'like', '%first%');
//        }])->get();


        $this->filterWithRecommended=filter::with([
            'serviceProvider' => function ($query) {
                $query
//                    ->select('id','name','state','isRecommended')
                    ->where('isRecommended', 1)
                    ->whereHas('user',
                        function($query)  {
                            $query->where('city_id', $this->cityID);
                        });},
            'serviceProvider.partTime'=> function ($query) {
                $query
//                    ->select('id','name','state','isRecommended')
                    ->where('day_state', "on")   ;},
            'serviceProvider.user'=> function ($query) {
                $query
//                    ->select('id','name','state','isRecommended')
                    ->where('city_id',$this->cityID)   ;}])
            ->select('id','type')
            ->where('id',$filterID)
            ->get()
            ->toArray();

//        $this->filterWithNotRecommended=filter::with([
//            'serviceProvider' => function ($query) {
//                $query
// //            ->select('id','name','state','isRecommended')
//                    ->where('isRecommended', 0)   ;},
//            'serviceProvider.partTime'=> function ($query) {
//                $query
////                    ->select('id','name','state','isRecommended')
//                    ->where('day_state', "on")   ;},
//            'serviceProvider.user'=> function ($query) {
//                $query
////                    ->select('id','name','state','isRecommended')
//                    ->where('city_id',$this->cityID)   ;}])
//            ->select('id','type')
//            ->where('id',$filterID)
//            ->get()
//            ->toArray();
        $this->filterWithNotRecommended=filter::with([
            'serviceProvider' => function ($query) {
                $query
                    //            ->select('id','name','state','isRecommended')
                    ->where('isRecommended', 0)
                    ->whereHas('user',
                        function($query)  {
                            $query->where('city_id', $this->cityID);
                        });
              },
            'serviceProvider.partTime'=> function ($query) {
                $query
//                    ->select('id','name','state','isRecommended')
                    ->where('day_state', "on")   ;},
            'serviceProvider.user'=> function ($query) {
                $query
//                    ->select('id','name','state','isRecommended')
                    ->where('city_id',$this->cityID)   ;}])
            ->select('id','type')
            ->where('id',$filterID)
            ->get()
            ->toArray();
//dd($this->filterWithRecommended[0]['type']);
        $this->filterWithRecommended[0]['type']==1 ? $this->type=__('serviceProvider.Restaurants'):$this->type=__('serviceProvider.Vendors');
        $this->filterWithRecommended[0]['type']==1 ? $this->pageType="Restaurants":$this->pageType="Shops";

//        $this->dispatchBrowserEvent('recommended-initialize');
//        $this->dispatchBrowserEvent('recommended-set');
//        $this->dispatchBrowserEvent('recommended-get');


        //        $this->emit('refreshProducts');
//                dd($this->filterWithRecommended,  $this->filterWithNotRecommended);

    }
    public function render()
    {
//        $timezone = date_default_timezone_get();
//        $now=Carbon::now();
//        dd($now,$timezone);
        return view('livewire.template.category.service-providers');
    }
}
