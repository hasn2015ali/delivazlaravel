<?php

namespace App\Http\Livewire\Template\Category;


use App\Models\CountryTranslation;
use App\Models\Food;
use App\Models\Product;
use App\Models\UserWishlist;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;

class ServiceProviderItems extends Component
{
    use WithPagination, WithCountryAndCity;
    protected $paginationTheme = 'bootstrap';
    public $items;
    public $serviceProvider;
    public $filterType;
    public $filterID;
    public $folders;
    public $firstFilter;
    public $country;
    public $city;
    public $currencyCode;
    public $itemsID=[];
    public $getByFilter=false;

    public $itemsB;
    public $delivazCommission=0;

    protected $listeners = [
        'getItemByFilter',
//        'getItemByFilter'=>'render',
//        'refresh'=>'$refresh'
    ];



    public function getQueryString()
    {
        return [];
    }
    public function setDelivazCommission()
    {
        if($this->serviceProvider->delivaz_commission==null)
        {
            $this->delivazCommission=0;
        }
        else
        {

            $this->delivazCommission=$this->serviceProvider->delivaz_commission;

        }
    }
    public function mount()
    {
//        dd($this->city,$this->country);
        $this->filterType=$this->serviceProvider->type;
        $this->setDelivazCommission();
//        dd($this->serviceProvider->delivaz_commission,$this->delivazCommission);
        $countryDetails=CountryTranslation::with('country')
            ->where('name',$this->country)
            ->first();
        if(isset($countryDetails->country->currency_code))
        {
            $this->currencyCode=$countryDetails->country->currency_code;

        }
//        dd(Auth::check());


//        dd($countryDetails->country->currency_code);
    }
    public function getItemByFilter($filterId, $type,$filterName)
    {
//        dd($type,$filterId,$filterName);
//        $this->emitTo('template.category.add-to-basket-component', 'refresh');

        if($type==0 and $filterId==0)
        {
            $this->getByFilter=false;

        }
        else
        {
            $this->filterType = $type;
            $this->filterID = $filterId;
            $this->firstFilter=$filterName;
            $this->getByFilter=true;

        }
        $this->resetPage();
//        $this->emit('refresh');
    }

    public function render()
    {
//        Artisan::call('cache:clear');
        $itemsByFilters=null;
        if($this->getByFilter)
        {
            if ($this->filterType == "res") {
                $this->folders="storage/images/food/";

//            $itemsByFilters = Cache::remember('foodByFilter'.$this->filterID.$this->serviceProvider->id,60*60*24,function (){
//                return  Food::with('translation')->whereHas('filters', function ($query) {
//                    $query->where('meal_filters.id', $this->filterID);
//                })->where('service_provider_id', $this->serviceProvider->id)
//                    ->orderBy('id', 'desc')
//                    ->paginate(15);
//            });
//            $itemsByFilters->paginate(15);

                $itemsByFilters = Food::with('translation')->whereHas('filters', function ($query) {
                    $query->where('meal_filters.id', $this->filterID);
                })->where('service_provider_id', $this->serviceProvider->id)
                    ->orderBy('id', 'desc')
                    ->get();
//                    ->paginate(15);
//            dd($itemsByFilters,$itemsByFilters->currentPage());
            }
            if ($this->filterType == "ven") {
                $this->folders="storage/images/product/";
//            $itemsByFilters=Cache::remember('productByFilter'.$this->filterID.$this->serviceProvider->id,60*60*24,function (){
//                return Product::with('translation')->whereHas('filters', function ($query)  {
//                $query->where('product_filters.id', $this->filterID);
//            })->where('service_provider_id', $this->serviceProvider->id)
//                ->orderBy('id', 'desc')
//                ->paginate(15);
//            });
                $itemsByFilters = Product::with('translation')->whereHas('filters', function ($query)  {
                    $query->where('product_filters.id', $this->filterID);
                })->where('service_provider_id', $this->serviceProvider->id)
                    ->orderBy('id', 'desc')
                    ->get();
//                    ->paginate(15);
            }
        }
        else{
            if ($this->filterType == "res") {
                $this->folders="storage/images/food/";
                $itemsByFilters = Food::with('translation')
                    ->where('service_provider_id', $this->serviceProvider->id)
                    ->orderBy('id', 'desc')
                    ->get();
//                    ->paginate(15);
//            dd($itemsByFilters,$itemsByFilters->currentPage());
            }
            if ($this->filterType == "ven") {
                $this->folders="storage/images/product/";
//
                $itemsByFilters = Product::with('translation')
                    ->where('service_provider_id', $this->serviceProvider->id)
                    ->orderBy('id', 'desc')
                    ->get();
//                    ->paginate(15);
            }
        }

        if(Auth::check())
        {
            $userID=Auth::id();
            $wishes=UserWishlist::where('user_id', $userID)
                ->where('type', $this->filterType)
                ->get();
            foreach ($wishes as $wish)
            {
                array_push($this->itemsID,$wish->item_id);
            }
//            dd($this->itemsID);
        }
//        dd($this->getCurrentPage());
//        dd($itemsByFilters);
//        $this->emit('refresh');
        return view('livewire.template.category.service-provider-items',compact('itemsByFilters'));
    }
}
