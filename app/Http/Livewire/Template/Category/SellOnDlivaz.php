<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\CityServiceProviders;
use App\Traits\WithCountryAndCity;
use Livewire\Component;

class SellOnDlivaz extends Component
{
    protected $listeners = ['getPageType',  //for Restaurants and Shops Page
    ];
    public $page;
    public $folders;
    public $cover;
    public $cityID;
    public $city;
    public $serviceProviderType;
    use WithCountryAndCity;

    // to set cover For Restaurants and shops Page
    public function getPageType($pageType)
    {
//        this event is emitting from page public/category/serviceProviders
//        $pageType=$pageType;
        $photoType="cover";
        if($pageType=="Restaurants")
        {
            $providerType="restaurant";
            $this->folders="storage/images/cover/city/restaurants/";
            $cover=CityServiceProviders::where('city_id',$this->cityID)
                ->where('photo_type',$photoType)
                ->where('provider_type',$providerType)
                ->first();
            if($cover)
            {
                $this->cover= $cover->name;
            }

        }
        elseif ($pageType=="Shops")
        {
            $providerType="shop";
            $this->folders="storage/images/cover/city/shops/";
            $cover=CityServiceProviders::where('city_id',$this->cityID)
                ->where('photo_type',$photoType)
                ->where('provider_type',$providerType)
               ->first();
            if($cover)
            {
                $this->cover= $cover->name;
            }
        }
//
    }

//  set folders for cover
    public function mount()
    {

        if($this->city!=null)
        {
            $this->cityID=$this->getCityId($this->city);

        }
        if($this->serviceProviderType!=null)
        {
//            dd($this->serviceProviderType);
            $photoType="cover";
            if($this->serviceProviderType=="Restaurant")
            {
                $providerType="restaurant";
                $this->folders="storage/images/cover/city/restaurants/";
                $cover=CityServiceProviders::where('city_id',$this->cityID)
                    ->where('photo_type',$photoType)
                    ->where('provider_type',$providerType)
                    ->first();
                if($cover)
                {
                    $this->cover= $cover->name;
                }

            }
            elseif ($this->serviceProviderType=="Shop")
            {
                $providerType="shop";
                $this->folders="storage/images/cover/city/shops/";
                $cover=CityServiceProviders::where('city_id',$this->cityID)
                    ->where('photo_type',$photoType)
                    ->where('provider_type',$providerType)
                    ->first();
                if($cover)
                {
                    $this->cover= $cover->name;
                }
            }
        }
//        dd($this->page);
    }
    public function render()
    {
        return view('livewire.template.category.sell-on-dlivaz');
    }
}
