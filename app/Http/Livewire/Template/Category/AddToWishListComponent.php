<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\UserWishlist;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AddToWishListComponent extends Component
{
    use  WithCountryAndCity;

    public $country;
    public $city;
    public $item;
    public $filterType;
    public $itemsID=[];
    public $userID;


    public function addToWishlist()
    {

        $this->userID = Auth::id();
        if ($this->userID == null) {
            $message = trans('serviceProvider.wishlistUserCheck');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $countryID = $this->getCountryId($this->country);
        $cityID = $this->getCityId($this->city);
        $check = UserWishlist::where('user_id', $this->userID)->where('type', $this->filterType)
            ->where('item_id', $this->item->id)->first();
        if ($check == null) {
            UserWishlist::create([
                'item_id' => $this->item->id,
                'service_provider_id' => $this->item->service_provider_id,
                'city_id' => $cityID,
                'country_id' => $countryID,
                'type' => $this->filterType,
                'user_id' => $this->userID,
            ]);
        } else {
            $check->delete();
        }
        $this->emitTo('template.user.cart-component', 'wishesUpdated');
        $this->getWishesIDs();

    }

    public function getWishesIDs()
    {
        $this->itemsID=[];
        $wishes=UserWishlist::where('user_id', $this->userID)
            ->where('type', $this->filterType)
            ->get();
        foreach ($wishes as $wish)
        {
            array_push($this->itemsID,$wish->item_id);
        }
//        dd($this->itemsID,$wishes,$this->filterType,$this->userID);
    }

    public function mount ()
    {
        $this->userID=Auth::id();
        $this->getWishesIDs();
    }
    public function render()
    {

        return view('livewire.template.category.add-to-wish-list-component');
    }
}
