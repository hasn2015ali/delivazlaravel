<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\Food;
use App\Models\ServiceProviderMenu;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ServiceProviderFilters extends Component
{
    public $serviceProvider;
    public $mealFilters;
    public $productFilters;
    public $langCode;
    public $languageSelected;
    public $filterType;
    public $firstFilterID;
    public $firstFilterName;

    public $filterKey="all";
    public $activeFilter="all";


    public function mount()
    {
        $this->langCode = App::getlocale();
        if ($this->serviceProvider->filters[0]['type'] == 1) {
            $this->mealFilters = ServiceProviderMenu::with('mailFilterTrans')
                ->where('service_provider_id', $this->serviceProvider->id)
                ->where('state', 1)
                ->orderBy('order', 'ASC')
                ->get();
            if (count($this->mealFilters) != 0) {
                $this->firstFilterID = $this->mealFilters[0]->filter_id;
                $this->firstFilterName = $this->mealFilters[0]->mailFilterTrans->where('code_lang', $this->langCode)->first()->name;
            }

            $this->filterType = "res";

//            dd($this->mealFilters);
        } elseif ($this->serviceProvider->filters[0]['type'] == 2) {
            $this->productFilters = ServiceProviderMenu::with('productFilterTrans')
                ->where('service_provider_id', $this->serviceProvider->id)
                ->where('state', 1)
                ->orderBy('order', 'ASC')
                ->get();

            if (count($this->productFilters) != 0) {
                $this->firstFilterID = $this->productFilters[0]->filter_id;
                $this->firstFilterName = $this->productFilters[0]->productFilterTrans->where('code_lang', $this->langCode)->first()->name;
            }
            $this->filterType = "ven";

//            dd($this->productFilters);
        }

//        if ($this->serviceProvider->filters[0]['type'] == 1) {
//
//            $allFilterUsed= DB::table('food_filter_relations')
//                ->select('food_filter_relations.filter_id')
//                ->join('food', 'food_filter_relations.food_id', '=', 'food.id')
//                ->where('food.service_provider_id',  $this->serviceProvider->id)
//                ->pluck('filter_id');
//            $this->mealFilters = DB::table('service_provider_filters')
//                ->where('service_provider_filters.service_provider_id', '=', $this->serviceProvider->id)
//                ->join('meal_filter_trans', 'service_provider_filters.filter_id', '=', 'meal_filter_trans.filter_id')
//                ->whereIn('meal_filter_trans.meal_filter_id',$allFilterUsed)
//                ->where('meal_filter_trans.code_lang',  $this->langCode)
//                ->orderBy('meal_filter_trans.name','asc')
//                ->get();
//            if(count($this->mealFilters)!=0)
//            {
//                $this->firstFilterID=$this->mealFilters[0]->meal_filter_id;
//                $this->firstFilterName=$this->mealFilters[0]->name;
//            }
//
//            $this->filterType="res";
//        } elseif ($this->serviceProvider->filters[0]['type'] == 2) {
//            $allFilterUsed= DB::table('product_filters_relations')
//                ->select('product_filters_relations.filter_id')
//                ->join('products', 'product_filters_relations.product_id', '=', 'products.id')
//                ->where('products.service_provider_id',  $this->serviceProvider->id)
//                ->pluck('filter_id');
//            $this->productFilters = DB::table('service_provider_filters')
//                ->where('service_provider_filters.service_provider_id', '=', $this->serviceProvider->id)
//                ->join('product_filter_trans', 'service_provider_filters.filter_id', '=', 'product_filter_trans.filter_id')
//                ->whereIn('product_filter_trans.product_filter_id',$allFilterUsed)
//                ->where('product_filter_trans.code_lang',  $this->langCode)
//                ->orderBy('product_filter_trans.name','asc')
//                ->get();
//            if(count($this->productFilters)!=0)
//            {
//                $this->firstFilterID=$this->productFilters[0]->product_filter_id;
//                $this->firstFilterName=$this->productFilters[0]->name;
//            }
//
//            $this->filterType="ven";
//
//        }

    }

    public function getProductByFilter($filter_id,$filterType,$filterName,$filterKey)
    {
//        $this->activeFilter=$filterKey;
        $this->emitTo('template.category.service-provider-items', 'getItemByFilter',$filter_id,$filterType,$filterName);

        $this->filterKey=$filterKey;
//        dd($filterType,$filter_id,$filterName);
    }
    public function render()
    {
        return view('livewire.template.category.service-provider-filters');
    }
}
