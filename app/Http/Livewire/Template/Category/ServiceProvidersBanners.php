<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\CityServiceProviders;
use App\Traits\WithCountryAndCity;
use Livewire\Component;

class ServiceProvidersBanners extends Component
{
    public $pageType;
    public $banners;
    public $bannerType;
    public $cityID;
    public $city;
    public $country;
    public $folderName;
    use WithCountryAndCity;
    protected $listeners = ['getPageType',

    ];
    public function getPageType($pageType)
    {
//        this event is emitting from page public/category/serviceProviders
       $this->pageType=$pageType;
       $this->getBanners();
       $this->dispatchBrowserEvent('show-banners');
//       dd($this->banners);
    }

    public function getBanners()
    {
//        restaurant shop from data base
//        Restaurants Shops from getPageType Event
        $this->pageType=="Restaurants"?$this->bannerType="restaurant":$this->bannerType="shop";
        $this->pageType=="Restaurants"?$this->folderName="restaurants":$this->folderName="shops";
        $photoType="banner";
        $this->banners=CityServiceProviders::with('serviceProvider:service_providers.id,service_providers.slug')->where('city_id',$this->cityID)
            ->where('photo_type',$photoType)
            ->where('provider_type',$this->bannerType)
            ->where('state',1)->get();
//        dd($this->banners);

    }
    public function mount()
    {
        $this->cityID=$this->getCityId($this->city);
//        dd($this->cityID);
    }
    public function render()
    {
        return view('livewire.template.category.service-providers-banners');
    }
}
