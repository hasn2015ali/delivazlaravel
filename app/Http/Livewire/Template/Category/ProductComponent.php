<?php

namespace App\Http\Livewire\Template\Category;


use App\Helpers\Cart\CartAdditions\AdditionsFacade;
use App\Helpers\Cart\CartFacade;
use App\Models\CountryTranslation;
use App\Models\Food;
use App\Models\Product;
use App\Models\UserCart;
use App\Models\UserWishlist;
use App\Traits\WithCountryAndCity;
use App\Traits\WithServiceProviderTrairt;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ProductComponent extends Component
{
    use  WithServiceProviderTrairt, WithCountryAndCity;

    public $foodID;
    public $productID;
    public $type;
//    public $imgs=['16128506053.jpg','161285493531.jpg','161160774821.png'];
//    public $sizes=['Small','Medium','Large','XLarge','XXLarge'];
    public $selectedSize = 0;
    public $selectedSizeName;
    public $sizePrice = 0;

    public $item;
    public $country;
    public $city;
    public $serviceProvider;
    public $currencyCode;
    public $folders;
    public $foldersForAddition;
    public $itemPrice;
    public $addedItem;
    public $addedItemID = [];
    public $itemType;
    public $ProductQtn = 1;
    public $allProductPhoto = [];
    public $delivaz_commission;
    public $isFavourite;
    public $userID;
    public $productIDDescription;
    public $itemIdsInCart;
    public $itemIdWithType;

    //for check old and new order is same serviceProvider ??
    public $cardToast = false;
    public $serviceProviderInCart;
    public $disabledAdd;
    private $productDetails=[];
    public $country_id;
    public $city_id;
    public $itemsInCart;

    protected $listeners = [
        'getNewPrice',
        'removeFromCart'=>'checkItemInCart',
//        'refresh'=>'$refresh'
    ];


    //for check old and new order is same serviceProvider ??
    public function deleteOldOrder()
    {
        CartFacade::clear();
        $id = $this->item->id . $this->itemType . rand(1, 100);
        $service_provider_id = $this->item->service_provider_id;
        $cart = CartFacade::get();
        $name = $this->item->translation->where('code_lang', 'en')->first()->name;
        $additions = AdditionsFacade::get();
        CartFacade::add($id, $service_provider_id, $name, $this->ProductQtn, $this->itemPrice, $this->selectedSizeName, $this->item->photo, $this->itemType, $additions);
        $this->emitTo('template.user.cart-component', 'cartUpdated');
        $this->cardToast = false;
    }

    //for check old and new order is same serviceProvider ??
    public function cancelNewOrder()
    {
        $this->cardToast = false;
    }

    public function initializeProductDetails()
    {
        $idDesc = $this->item->id . $this->itemType . rand(10,1000) ;
        $service_provider_id = $this->item->service_provider_id;
        $name = $this->item->translation
            ->where('code_lang', 'en')
            ->first()->name;
        $additions = AdditionsFacade::get();
        $this->productDetails['id'] = $idDesc;
        $this->productDetails['itemID'] = $this->item->id;
        $this->productDetails['serviceProviderID'] = $service_provider_id;
        $this->productDetails['price'] = $this->itemPrice;
        $this->productDetails['size'] = $this->selectedSizeName;
        $this->productDetails['photo'] = $this->item->photo;
        $this->productDetails['type'] = $this->itemType;
        $this->productDetails['name'] = $name;
        $this->productDetails['quantity'] = $this->ProductQtn;
        $this->productDetails['country_id'] = $this->country_id;
        $this->productDetails['city_id'] = $this->city_id;
        $this->productDetails['additions'] = $additions;
        $this->storeProductDetails($this->productDetails);
    }
    public function addToBasket()
    {
        $this->initializeProductDetails();
        $this->checkItemInCart();
//        dd($this->city_id,$this->country_id,$this->itemsInCart);
        $cityCheck=CartFacade::checkItemCityAndCountry($this->city_id,$this->country_id,$this->itemsInCart);
        if(!$cityCheck){
            $message = trans('serviceProvider.cityCheck');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if($this->disabledAdd==true)
        {
            $message = trans('serviceProvider.AlreadyAdded');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if(Auth::check())
        {
//            dd($this->productDetails['additions']);
            $newAddition=json_encode($this->productDetails['additions']);
//            dd($newAddition);
            UserCart::create([
                'idDES' => $this->productDetails['id'],
                'itemID' =>  $this->productDetails['itemID'] ,
                'serviceProviderID' =>  $this->productDetails['serviceProviderID'],
                'price' =>  $this->productDetails['price'] ,
                'size' =>  $this->productDetails['size'],
                'photo' =>$this->productDetails['photo'],
                'type' =>$this->productDetails['type'],
                'name' =>  $this->productDetails['name'],
                'quantity' => $this->productDetails['quantity'],
                'city_id' => $this->productDetails['city_id'],
                'country_id' => $this->productDetails['country_id'],
                'additions' => $newAddition,
                'user_id' => Auth::id(),
            ]);
            $newAddition=null;

//            dd($this->productDetails);
        }
        else
        {
            CartFacade::add($this->productDetails);

        }
        $idDesc = null;
        $service_provider_id = null;
        $name = null;
        $additions = [];


//        dd($productDetails);
//        dd($id, $service_provider_id, $name, $this->ProductQtn, $this->itemPrice,
//            $this->selectedSizeName, $this->item->photo, $this->itemType, $additions);
//            CartFacade::add($id, $service_provider_id, $name, $this->ProductQtn, $this->itemPrice, $this->selectedSizeName, $this->item->photo, $this->itemType, $additions);
        $this->emitTo('template.user.cart-component', 'cartUpdated');
        $this->checkItemInCart();

    }

    public function checkItemInCart()
    {
        if(Auth::check())
        {
            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->itemsInCart=CartFacade::convertAllItemToArray($items);
            $item=null;
//            dd($items,$this->itemsInCart);
        } else
        {
            $this->itemsInCart = CartFacade::get();
        }
        $productDeatails=$this->getProductDetails();
        unset( $productDeatails['quantity']);
        unset( $productDeatails['id']);

//        dd($productDeatails,$itemInCart['products']);
        foreach ($this->itemsInCart['products'] as $key=>$item) {
            if(Auth::check())
            {
                unset( $item['idDES']);
                unset( $item['user_id']);
                unset( $item['created_at']);
                unset( $item['updated_at']);

                $item['price']=(int) $item['price'];
            }
            unset( $item['quantity']);
            unset( $item['id']);

//            if($key==2)
//            {
//                dd( $item,$productDeatails,$productDeatails==$item);
//
//            }

            if($productDeatails==$item)
            {
                $this->disabledAdd=true;
                break;
            }
            else{
                $this->disabledAdd=false;
            }
        }
//        dd($productDeatails,$this->itemsInCart['products']);
    }

    public function addToWishlist()
    {

        $check = $this->checkProductWishList();

        if ($check['auth'] == null) {
            $message = trans('serviceProvider.wishlistUserCheck');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
        } else {
            $countryID = $this->getCountryId($this->country);
            $cityID = $this->getCityId($this->city);

            if ($check['product'] == null) {
                UserWishlist::create([
                    'item_id' => $this->item->id,
                    'service_provider_id' => $this->item->service_provider_id,
                    'city_id' => $cityID,
                    'country_id' => $countryID,
                    'type' => $this->itemType,
                    'user_id' => $this->userID,
                ]);
                $this->isFavourite = true;
            } else {
                $result = UserWishlist::where('user_id', $this->userID)->where('type', $this->itemType)
                    ->where('item_id', $this->item->id)->first();
                $result->delete();
                $this->isFavourite = false;
            }
            $this->emitTo('template.user.cart-component', 'wishesUpdated');
        }


    }

    public function checkProductWishList()
    {
        $this->userID = Auth::id();
        if ($this->userID == null) {
            $check['auth'] = null;
        } else {
            $check['auth'] = true;
        }

        $result = UserWishlist::where('user_id', $this->userID)->where('type', $this->itemType)
            ->where('item_id', $this->item->id)->first();
//        dd($check);
        if ($result == null) {
            $this->isFavourite = false;
            $check['product'] = null;
        } else {
            $this->isFavourite = true;
            $check['product'] = $result;

        }
        return $check;

    }

    public function minusProductQuantity()
    {
        $this->ProductQtn == 0 ? $this->ProductQtn : $this->ProductQtn--;
//        $this->getNewPrice();

    }

    public function plusProductQuantity()
    {
        $this->ProductQtn++;
//        $this->getNewPrice();
    }

    public function getNewPrice()
    {


        //get selected Size Price
        $sizePrice = $this->getStoredSize();
//        dd($sizePrice);
        // get all Addition Price
        $addition = AdditionsFacade::get();
//        dd($addition);
        $additionsPrice = 0;
        foreach ($addition['additions'] as $add) {
//                dd($add);
            $additionsPrice = $additionsPrice + $add['price'] * $add['quantity'];
        }

        $this->itemPrice = $sizePrice + $additionsPrice;
        $productDetails=$this->getProductDetails();
        $productDetails['additions'] = $addition;
        $productDetails['price'] = $this->itemPrice;
        $this->storeProductDetails($productDetails);
        $productDetails=null;
        $sizePrice = null;
        $additionsPrice = 0;
        $this->checkItemInCart();

    }

    public function selectSize($index, $sizePrice, $sizeName)
    {
        $this->storeSizePrice($sizePrice);
//        $this->sizePrice = $sizePrice;
        $this->selectedSizeName = $sizeName;

        if ($this->selectedSize == $index) {
            $this->selectedSize = 0;

        } else {
            $this->selectedSize = $index;

        }
        $productDetails=$this->getProductDetails();
        $productDetails['size'] = $this->selectedSizeName;
//        dd($productDetails,$this->selectedSizeName);
        $this->storeProductDetails($productDetails);
        $this->getNewPrice();


        $productDetails=null;
    }

    public function storeSizePrice($price)
    {
        session()->put('sizePrice', $price);
    }

    public function getStoredSize()
    {
        return session('sizePrice');
//        return session()->get('items');
    }

    public function storeProductDetails($ProductDetails)
    {
        session()->put('ProductDetails', $ProductDetails);
    }

    public function getProductDetails()
    {
        return session('ProductDetails');
//        return session()->get('items');
    }

    public function mount()
    {
        AdditionsFacade::clear();
        $this->addedItem = AdditionsFacade::get();
        if ($this->foodID != null) {
            $this->item = Food::with('translation', 'sizes', 'additions', 'photos',
                'serviceProvider:service_providers.id,service_providers.delivaz_commission,service_providers.user_id','serviceProvider.city')->findOrFail($this->foodID);
            $this->folders = "storage/images/food/";
            $this->foldersForAddition = "storage/images/food/addition/";
            $this->itemType = "res";

        }


        if ($this->productID != null) {
            $this->item = Product::with('translation', 'sizes', 'additions', 'photos',
                'serviceProvider:service_providers.id,service_providers.delivaz_commission,service_providers.user_id','serviceProvider.city')->findOrFail($this->productID);
            $this->folders = "storage/images/product/";
            $this->foldersForAddition = "storage/images/product/addition/";
            $this->itemType = "ven";

        }

        $this->city_id=$this->item->serviceProvider->city->id;
        $this->country_id=$this->item->serviceProvider->city->country_id;
//        dd($this->city_id,$this->country_id,$this->item->serviceProvider->city);
//        dd(asset( $this->folders),asset( $this->folders)."/".$this->item->photo);
        if (isset($this->item->photo)) {
            array_push($this->allProductPhoto, asset($this->folders) . "/" . $this->item->photo);

        }
        if (count($this->item->photos->toArray()) > 0) {
            foreach ($this->item->photos->toArray() as $photo) {
                array_push($this->allProductPhoto, asset($this->folders) . "/" . $photo['name']);

            }
        }


        // if there is delivazCommission
        $this->delivaz_commission = $this->item->serviceProvider->delivaz_commission;
        $this->itemPrice = $this->calculatePriceWithCommission($this->item->price, $this->delivaz_commission);
//        dd($this->itemPrice,$this->delivaz_commission);
        $this->storeSizePrice($this->itemPrice);
        $this->selectedSizeName = $this->item->size;
//        $this->getNewPrice();
        $countryDetails = CountryTranslation::with('country')
            ->where('name', $this->country)
            ->first();
        if (isset($countryDetails->country->currency_code)) {
            $this->currencyCode = $countryDetails->country->currency_code;

        }

        $this->checkProductWishList();
        $this->initializeProductDetails();

        $this->checkItemInCart();
//        dd($this->isFavourite);
//     dd($this->item->additions,$this->productID);
    }

    public function render()
    {
        return view('livewire.template.category.product-component');
    }
}
