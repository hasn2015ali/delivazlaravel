<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\CityShapcontent;
use App\Models\CityShapTranslation;
use App\Models\filter;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class ServiceProvidersFilters extends Component
{
    use WithCountryAndCity;
    public $country;
    public $city;
    public $shape;
    public $countryID;
    public $cityID;
    public $filters;
    public $shapeID;
    public $shapeContent;
    public $firstFilterID;


    public function mount()
    {
        $this->countryID=$this->getCountryId($this->country);
        $this->cityID=$this->getCityId($this->city);

        $this->shape= str_replace('-', ' ', $this->shape);
        $shapeCheck=CityShapTranslation::where('trans',$this->shape)->first();
        if(isset($shapeCheck))
        {
            $this->shapeID=$shapeCheck->id;

        }
        else
        {
            return redirect()->route('index', ['locale' => App::getLocale(),'country'=>$this->country,'city'=>$this->city]);

//            return redirect('/en'.$this->country.'/'.$this->city);
        }
        $this->shapeContent=CityShapcontent::with('filter.translation')->where('shape_id',$this->shapeID)->get();

        $this->firstFilterID=$this->shapeContent[0]['filter']->id;


//        $this->emitTo('template.category.service-providers','getServiceProviderByFilter', $firstFilterID);
//        dd($firstFilterID);
        //        $this->filters=$shapeContent->filter;
//        dd( $this->shapeContent);
    }


    public function render()
    {

        return view('livewire.template.category.service-providers-filters');
    }
}
