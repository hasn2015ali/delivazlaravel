<?php

namespace App\Http\Livewire\Template\Category;

use Livewire\Component;

class ProductPhotoComponent extends Component
{
    public $photos=[];
    public $itemPhoto;
    public $folders;

    public function render()
    {
        return view('livewire.template.category.product-photo-component');
    }
}
