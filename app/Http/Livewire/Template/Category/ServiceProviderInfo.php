<?php

namespace App\Http\Livewire\Template\Category;

use App\Models\ServiceProviderPartTime;
use Carbon\Carbon;
use Livewire\Component;

class ServiceProviderInfo extends Component
{
    public $serviceProvider;
    public $address;
    public $photo;
    public $nowDay;
    public $nowHour;
    public $nowMinute;
//    public $startTimeHour;
//    public $startTimeMinutes;
//    public $endTimeHour;
//    public $endTimeMinutes;
    public $startTime;
    public $endTime;
    public $closedToday=false;
    protected $listeners = [
        'setCurrentDay'
    ];

    public function setCurrentDay($nowDay,$nowHour,$nowMinute)
    {
        $nowDay==0? $nowDay=7:$nowDay=$nowDay;
        $this->nowDay=$nowDay;
        $this->nowHour=$nowHour;
        $this->nowMinute=$nowMinute;

        $workInDay=ServiceProviderPartTime::where('day_id',$nowDay)
            ->where('day_state',"on")
            ->where('service_provider_id',$this->serviceProvider->id)
            ->first();
        if($workInDay!=null)
        {

            $setStartTime = Carbon::now();
            $setStartTime->hour = $workInDay->startTimeHour;
            $setStartTime->minute = $workInDay->startTimeMinutes;
            $this->startTime=$setStartTime->format("H:i");

            $setEndTime = Carbon::now();
            $setEndTime->hour = $workInDay->endTimeHour;
            $setEndTime->minute = $workInDay->endTimeMinutes;
            $this->endTime=$setEndTime->format("H:i");
//            dd($this->startTime);
        }
        else
        {
            $this->closedToday=true;

        }
//        dd( $this->endTimeMinutes);




//        dd($this->startTimeHour,$this->startTimeMinutes,$this->endTimeHour, $this->endTimeMinutes);
//        dd($this->nowDay,$this->nowHour,$this->nowMinute);
    }
    public function mount()
    {
        $this->address=$this->serviceProvider->address;
        $this->photo=$this->serviceProvider->avatar;
//        dd($this->address,$this->photo);
    }

    public function render()
    {
        return view('livewire.template.category.service-provider-info');
    }
}
