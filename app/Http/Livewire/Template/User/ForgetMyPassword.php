<?php

namespace App\Http\Livewire\Template\User;

use App\Models\User;
use App\Notifications\ClickSendTest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordCodeSend;

class ForgetMyPassword extends Component
{
    public $email;
    public $code;
    public $identificationShow=true;
    public $codShow=false;
    public $newPasswordShow=false;
    public $submitedCode;
    public $password;
    public $passwordConfirmation;
    public $userIdentification;
    public $type;
    public $user;

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }


    public function sendAnotherCode()
    {
        if($this->type=="email")
        {
            $this->submitedCode=rand(1111,9999);
            Mail::to($this->email)->send(new ResetPasswordCodeSend( $this->submitedCode));
        }
        else
        {
//            $user = User::where('phone',$this->email)->first();

            try {
                $this->user->notify(new ClickSendTest( $this->submitedCode));
            }
            catch (\Exception $e) {
                // do something when error
                return $e->getMessage();
            }
        }

//        $this->submitedCode=2222;
        session()->put('resetPasswordState',  trans( 'userFront.sendAnotherCodeSuccess' ));
    }

    public function submitNewPassword()
    {
        $messages = [
            "password.required" => trans( 'auth.passwordRequired' ),
            "passwordConfirmation.required" => trans( 'auth.password_confirmationRequired' ),
            "passwordConfirmation.same" => trans( 'auth.password_confirmationSame' ),
            "password.min" => trans( 'auth.password_min' ),

        ];
        $this->validate([
            'password' => 'required|min:8',
            'passwordConfirmation' => 'required|same:password',

        ],$messages);
        $newPassword=bcrypt($this->password);
        DB::table('users')->where($this->type,$this->userIdentification)
            ->update(['password'=>$newPassword]);

        session()->put('resetPasswordState',  trans( 'userFront.resetSuccessfly' ));
        return redirect()->route('login',['locale'=>app()->getLocale()]);
    }

    public function submitCode()
    {
        $messages = [
            "code.size" => trans( 'userFront.codeSize' ),
            "code.numeric" => trans( 'userFront.codeNumeric' ),

        ];
        $this->validate([
            'code' => 'numeric',
        ],$messages);

        if($this->code==$this->submitedCode)
        {
            session()->put('resetPasswordState',  trans( 'userFront.rightCode' ));
            $this->newPasswordShow=true;
            $this->codShow=false;
            $this->code=null;
            $this->submitedCode=null;
        }
        else
        {
            session()->put('resetPasswordState',  trans( 'userFront.falseCode' ));

        }

    }

    public function submitIdentification()
    {
        $messages = [
            "email.email" => trans( 'auth.validateIdentification' ),
            "email.numeric" => trans( 'auth.validateIdentification' ),

        ];
//        dd('test');
        if(filter_var($this->email,FILTER_VALIDATE_EMAIL))
        {
            $this->validate([
                'email' => 'email',
            ],$messages);
            $emailCheck = DB::table('users')
                ->select('email')
                ->where('email',$this->email)
                ->first();
            if(isset($emailCheck))
            {
                $this->submitedCode=rand(1111,9999);
                Mail::to($this->email)->send(new ResetPasswordCodeSend( $this->submitedCode));
                session()->put('resetPasswordState',  trans( 'userFront.emailFond' ));
                $this->codShow=true;
                $this->identificationShow=false;
                $this->userIdentification=$this->email;
                $this->type='email';


            }
            else
            {
                session()->put('resetPasswordState',  trans( 'userFront.emailNotFond' ));


            }

//            dd($request->email);
        }
        else
        {
            $this->validate([
                'email' => 'numeric',
            ],$messages);
            $phoneCheck = DB::table('users')
                ->select( 'phone')
                ->where('phone',$this->email)
                ->first();
            if(isset($phoneCheck))
            {
                $this->submitedCode=rand(1111,9999);
//                $this->submitedCode=1111;

                $this->user = User::where('phone',$this->email)->first();

                try {
                    $this->user->notify(new ClickSendTest( $this->submitedCode));
                }
                catch (\Exception $e) {
                    // do something when error
                    return $e->getMessage();
                }
                session()->put('resetPasswordState',  trans( 'userFront.phoneFond' ));
                $this->codShow=true;
                $this->identificationShow=false;
                $this->userIdentification=$this->email;
                $this->type='phone';


            }
            else
            {
                session()->put('resetPasswordState',  trans( 'userFront.phoneNotFond' ));


            }
//            dd($request->email);
        }
//        $this->email=null;
    }
    public function render()
    {
        return view('livewire.template.user.forget-my-password');
    }
}
