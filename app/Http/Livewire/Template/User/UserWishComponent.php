<?php

namespace App\Http\Livewire\Template\User;

use App\Helpers\Cart\CartFacade;
use App\Models\Food;
use App\Models\Product;
use App\Models\UserCart;
use App\Models\UserWishlist;
use App\Traits\WithServiceProviderTrairt;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class UserWishComponent extends Component
{
    use WithServiceProviderTrairt;
    public $wish;
    public $folders;
    public $item;
    public $serviceProviderSlug;
    public $country;
    public $city;
    public $itemID;
    public $itemType;
    public $priceWithCommission;
    public $delivazCommissin;
    public $itemIdsInCart;
    public $itemIdWithType;
    public $userID;
    public $qtn=1;
    public $productDetails;
    public $city_id;
    public $country_id;
    public $itemsInCart;
    protected $listeners = [
        'removeFromCart'=>'getItemsFromCart'
//        'wishesUpdated',
//        'getItemByFilter'=>'render',
//        'getItemByFilter'=>'$refresh'
    ];

    public function mount()
    {
//        dd($this->wish);
        $this->country = $this->wish->country->translation[0]->name;
        $this->city = $this->wish->city->translation[0]->name;
        $this->country_id = $this->wish->country_id;
        $this->city_id = $this->wish->city_id;
        $this->itemID=$this->item->id;
        $this->itemType=$this->wish->type ;
        $this->delivazCommissin=$this->wish->serviceProvider->delivaz_commission;
        $this->priceWithCommission=$this->calculatePriceWithCommission($this->item->price,$this->delivazCommissin);
        $this->generateArrayOfItemsIDSInBasket();
    }


    public function addToBasket()
    {

        $this->generateArrayOfItemsIDSInBasket();
        $cityCheck=CartFacade::checkItemCityAndCountry($this->city_id,$this->country_id,$this->itemsInCart);
        if(!$cityCheck){
            $message = trans('serviceProvider.cityCheck');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if (in_array($this->itemIdWithType, $this->itemIdsInCart)) {
            $message = trans('serviceProvider.AlreadyAdded');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
//        dd($this->itemType );

        if ($this->itemType == "res") {
            $productItem = Food::with('translation')->find($this->itemID);
        }
        if ($this->itemType == "ven") {
            $productItem = Product::with('translation')->find($this->itemID);

        }

        $service_provider_id = $productItem->service_provider_id;
        $idDesc = $productItem->id . $this->itemType . rand(10, 1000);
        $size = $productItem->size;
        $photo = $productItem->photo;
        $additions["additions"] = [];
        $name = $productItem->translation->where('code_lang', 'en')->first()->name;

        if (Auth::check()) {
            $newAddition = json_encode($additions);
//            dd($newAddition);
            UserCart::create([
                'idDES' => $idDesc,
                'itemID' =>  $productItem->id,
                'serviceProviderID' => $service_provider_id,
                'price' => $this->priceWithCommission,
                'size' => $size,
                'photo' => $photo,
                'type' => $this->itemType,
                'name' => $name,
                'quantity' =>  $this->qtn,
                'country_id' => $this->country_id,
                'city_id' => $this->city_id,
                'additions' => $newAddition,
                'user_id' => Auth::id(),
            ]);
            $newAddition = null;
        }

        $this->generateArrayOfItemsIDSInBasket();
        $this->emitTo('template.user.cart-component', 'cartUpdated');
    }
    public function generateArrayOfItemsIDSInBasket()
    {

        $this->getItemsFromCart();
        $this->itemIdWithType = $this->itemID . $this->itemType;
        $itemInCart = [];
//        dd($this->itemIdsInCart,$this->itemIdWithType,$this->itemID , $this->itemType);
    }
    public function getItemsFromCart()
    {
        if (Auth::check()) {
            $items = UserCart::where('user_id', Auth::id())
                ->get()
                ->toArray();
            $this->itemsInCart = CartFacade::convertAllItemToArray($items);
            $item = null;
        } else {
            $this->itemsInCart = CartFacade::get();
        }

        $this->itemIdsInCart = CartFacade::getIDForAddedItem($this->itemsInCart);
    }
    public function removeWish( )
    {
        $wish = UserWishlist::where('user_id', $this->userID)
            ->where('type', $this->itemType)
            ->where('item_id', $this->itemID)
            ->first();
        $wish->delete();
        $wish=null;
        $this->emitTo('template.user.cart-component', 'wishesUpdated');
        $this->emitUp('removeWish');

    }

    public function render()
    {
        return view('livewire.template.user.user-wish-component');
    }
}
