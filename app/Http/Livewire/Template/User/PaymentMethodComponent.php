<?php

namespace App\Http\Livewire\Template\User;

use Livewire\Component;

class PaymentMethodComponent extends Component
{
    public function render()
    {
        return view('livewire.template.user.payment-method-component');
    }
}
