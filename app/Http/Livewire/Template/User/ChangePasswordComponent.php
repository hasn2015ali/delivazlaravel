<?php

namespace App\Http\Livewire\Template\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ChangePasswordComponent extends Component
{
    public $isPassword=false;
    public $oldPassword;
    public $newPassword;
    public $showNewPasswordFild=false;
    public $showOldPasswordFild=true;
    public $newPasswordConformation;

    public function toggleIsPassword()
    {
        $this->isPassword=!$this->isPassword;
        $this->showNewPasswordFild=false;
        $this->showOldPasswordFild=true;
    }

    public function submitNewPassword()
    {
//        dd(strlen($this->newPassword));
        if($this->newPassword=="" or $this->newPassword==null){
            $message=trans( 'profile.valPassword' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }
        if($this->newPassword!=$this->newPasswordConformation){
            $message=trans( 'profile.valPasswordConformation' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }
        elseif(strlen($this->newPassword) < 7 )
        {
            $message=trans( 'profile.valPassword2' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }
        elseif(strlen($this->newPassword) > 7)
        {
            $user=User::find(auth()->user()->id);
            $user->password=bcrypt($this->newPassword);
            if($user->save()){
                $message=trans( 'profile.updateSuccessfully' );
                $this->emit('alert',['icon'=>'success','title'=>$message]);
            }else{
                $message=trans( 'profile.errorPassword2' );
                $this->emit('alert',['icon'=>'error','title'=>$message]);
            }


        }

    }
    public function submitOldPassword()
    {
        if(Hash::check($this->oldPassword, auth()->user()->password) or $this->oldPassword="" or $this->oldPassword=null)
        {
//            return "success";
            $this->showNewPasswordFild=true;
            $this->showOldPasswordFild=false;
            $this->oldPassword="";
        }
        else
        {
            $this->oldPassword="";
            $message=trans( 'profile.errorPassword' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
        }

    }
    public function render()
    {
        return view('livewire.template.user.change-password-component');
    }
}
