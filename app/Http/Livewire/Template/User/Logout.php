<?php

namespace App\Http\Livewire\Template\User;

use Livewire\Component;

class Logout extends Component
{
    public function render()
    {
        return view('livewire.template.user.logout');
    }
}
