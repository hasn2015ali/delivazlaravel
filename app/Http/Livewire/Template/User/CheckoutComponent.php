<?php

namespace App\Http\Livewire\Template\User;

use App\Helpers\Cart\CartFacade;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\UserCart;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CheckoutComponent extends Component
{
    use WithCountryAndCity;
    public $countries=[];
    public $cities=[];
    public $selectedCountry=[];
    public $selectedCity=[];
    public $countriesCode=[];

    public $selectedAddressID;
    public $addressType;
    public $showAddressSection=true;
    public $showAddNewAddressSection=false;
    public $showPaymentSection=false;
    public $showOrderSummerySection=false;
    public $itemsInCart;
    public $countryName;
    public $cityName;
    public $country;
    public $cashCost=10;
    public $deliveryCharge=20;
    public $atDoorCost=0;
    public $asapCost=10;
    public $scheduleCost=20;
    public $totalForItemCart;



    protected $listeners = ['goToAddNewAddress','editUserAddress' ,'selectNewAddress','goToOrderSummery'];





    public function selectNewAddress($selectAddress)
    {
        $this->selectedAddressID=$selectAddress['addressID'];
        $this->addressType=$selectAddress['type'];
//        dd($selectAddress);
    }


    public function goBackToAddresses()
    {
        $this->showAddressSection=true;
        $this->showAddNewAddressSection=false;
        $this->showPaymentSection=false;
        $this->showOrderSummerySection=false;
    }

    public function goToPayment()
    {
        if($this->selectedAddressID==null)
        {
            $message=trans( 'checkout.checkSelectedAddress' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return;
        }
        $this->showAddressSection=false;
        $this->showAddNewAddressSection=false;
        $this->showPaymentSection=true;
        $this->showOrderSummerySection=false;
        $this->emit('updateOrderInfo');
    }

    public function goToOrderSummery()
    {
        $this->showAddressSection=false;
        $this->showAddNewAddressSection=false;
        $this->showPaymentSection=false;
        $this->showOrderSummerySection=true;
        $this->emit('updateOrderInfo');

    }
    public function goToAddNewAddress()
    {
        $this->showAddressSection=false;
        $this->showAddNewAddressSection=true;
        $this->showPaymentSection=false;
        $this->showOrderSummerySection=false;
    }

    public function editUserAddress($addressID)
    {
//        dd($addressID);
        $this->showAddressSection=false;
        $this->showAddNewAddressSection=true;
        $this->showPaymentSection=false;
        $this->showOrderSummerySection=false;
    }

    public function mount()
    {

        $this->getCountries();
        $this->countries->toArray();

        if(Auth::check())
        {

            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->itemsInCart=CartFacade::convertAllItemToArray($items);
            $item=null;

        }
        else{

            $this->itemsInCart = CartFacade::get();
        }

        if(isset($this->itemsInCart['products'][0]['country_id']))
        {
            $this->country=Country::with('translation')
                ->find($this->itemsInCart['products'][0]['country_id']);
            $this->countryName=$this->country->translation->where('code_lang','en')->first()->name;
            $this->cityName=CityTranslation::where('city_id',$this->itemsInCart['products'][0]['city_id'])
                ->first()->name;
        }
        else{
            $this->countryName=null;
            $this->cityName=null;
        }

        $this->totalForItemCart = CartFacade::getTotal($this->itemsInCart);
//        dd($this->countriesCode);
//        dd();
    }
    public function getCities($country)
    {
        $country=collect(json_decode($country))->toArray();
//        dd($country,$country->country_id);
        $this->getCitiesForCountry($country['country_id']);
        $this->cities->toArray();
        $this->selectedCountry=$country;
//        dd($this->selectedCountry,collect($this->selectedCountry)->toArray());
    }
    public function setCity($city)
    {
        $city=collect(json_decode($city))->toArray();
        $this->selectedCity=$city;

    }
    public function render()
    {
        return view('livewire.template.user.checkout-component');
    }
}
