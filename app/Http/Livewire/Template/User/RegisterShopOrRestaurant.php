<?php

namespace App\Http\Livewire\Template\User;

use App\Events\ServiceProviderJoined;
use App\Mail\EmailVerificationCodeSend;
use App\Models\ServiceProvider;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithCountryAndCity;
use App\Traits\WithUserValidationTrait;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;

class RegisterShopOrRestaurant extends Component
{
    use WithFileUploads,WithCountryAndCity,WithUserValidationTrait;
    //var for WithCountryAndCity
    public $countries;
    public $cities;

    public $photo=null;
    public $selectedCity;
    public $selectedCountry;
    public $countryName;
    public $cityName;
    public $type;
    public $DefaultName;
    public $EnglishName;
    public $address;
    public $FathersName;
    public $FirstName;
    public $LastName;
    public $Phone;
    public $Email;
    public $Password;
    public $PasswordConfirmation;
    public function messages()
    {
        return [
            'selectedCountry.required' => trans( 'serviceProvider.validate_selectedCountry' ),
            'selectedCity.required' => trans( 'serviceProvider.validate_selectedCity' ),
            'type.required' => trans( 'serviceProvider.validate_type' ),
            'DefaultName.required' => trans( 'serviceProvider.validate_DefaultName' ),
            'address.required' => trans( 'serviceProvider.validate_address' ),
            'FirstName.required' => trans( 'serviceProvider.validate_FirstName' ),
            'LastName.required' => trans( 'serviceProvider.validate_LastName' ),
            'Phone.required' => trans( 'serviceProvider.validate_Phone' ),
            'PasswordConfirmation.required' => trans( 'serviceProvider.validate_PasswordConfirmation' ),
            'Password.required' => trans( 'serviceProvider.validate_Password' ),
            "Email.email" => trans('serviceProvider.validateEmail'),
            "Email.unique" => trans('serviceProvider.validateEmailUnique'),
            "Phone.unique" => trans('serviceProvider.validatePhoneUnique'),
            "Phone.numeric" => trans('serviceProvider.validatePhone'),
            "PasswordConfirmation.same" => trans('serviceProvider.password_confirmationSame'),
            "Password.min" => trans('auth.password_min'),

//            'personalImage.image' => trans( 'serviceProvider.imgVal' ),
//            'images.*.image' => trans( 'serviceProvider.imgVal' ),
//            'workType.required' => trans( 'serviceProvider.validate_workType' ),


        ];
    }

    public function hydrate()
    {
        $this->resetValidation();
    }
    public function submit()
    {
//        event(new ServiceProviderJoined($this->type));
//
//        return ;
//        dd('fff');
//        session(['start_user_time'=>time()]);
//        dd(session()->get('start_user_time'));
        $this->validate([
            'selectedCountry' => 'required',
            'selectedCity' => 'required',
            'type' => 'required',
            'DefaultName' => 'required',
            'address' => 'required',
            'FirstName' => 'required',
            'LastName' => 'required',
            'Phone' => 'required|numeric|unique:users,phone',
            'Password' => 'required|min:8',
            'PasswordConfirmation' => 'required|same:Password',
        ]);
        if($this->Email!=null)
        {
                $this->validate([
                    'Email' => 'email|unique:users,email',
                ]);
        }

        if($this->photo!=null )
        {
            $this->validate([
                'photo' => 'image|mimes:png,gif,jpg,PNG,Gif,jpeg,JPG,JPEG,GIF',
            ]);
        }
        // set role ID
        $this->type=="res"?$roleID=9:$roleID=10;

        $this->DefaultName= str_replace(['-','_','*'], ' ', $this->DefaultName);
        $this->DefaultName=trim($this->DefaultName);
        $slug= str_replace(' ', '-', $this->DefaultName);

        // validate from user trait
        if($this->checkServiceProviderName($this->DefaultName)==1)
        {
            return;
        }
        if($this->photo)
        {
            $imageName=time().".".$this->photo->extension();
            $this->photo->storeAs('public/images/avatars',$imageName);
        }
        else
        {
            $imageName=null;
        }
        DB::beginTransaction();
        $user = User::create([
            'firstName' => $this->FirstName,
            'lastName' => $this->LastName,
            'email' => $this->Email,
            'phone' => $this->Phone,
            'password' => Hash::make($this->Password),
            'country_id'=>$this->selectedCountry,
            'city_id'=>$this->selectedCity,
            'fatherName'=>$this->FirstName,
            'role_id'=>$roleID,
        ]);
        $serviceProvider=ServiceProvider::create([
            'name' => $this->DefaultName,
            'nameEN' => $this->EnglishName,
            'address' => $this->address,
            'type' => $this->type,
            'user_id' => $user->id,
            'avatar'=>$imageName,
            'slug' => $slug,
            'registration_state'=>0,

        ]);
        DB::commit();
        // send verfication code to email
        if($this->Email!=null)
        {
            $this->sendVerificationCodeToEmail();
//            $submitedCode = rand(1111, 9999);
//            session(['start_user_time_for_Email'=>time()]);
//            Mail::to($this->Email)->send(new EmailVerificationCodeSend($submitedCode));
//            if (session()->has('email_verification_code')) {
//                session()->forget(['email_verification_code']);
//                session()->put('email_verification_code', $submitedCode);
//
//            } else {
//                session(['email_verification_code' => $submitedCode]);
//            }
//            session()->put('emailMessage', trans('userFront.emailRegisterSend'));
        }
        // send verfication code to mobile
        if($this->Phone!=null)
        {
            $user1 = $user;
            $this->sendVerificationCodeToPhone($user1);
//            $code = rand(1111, 9999);
//            session(['start_user_time_for_phone'=>time()]);

//            dd($user1);
//            try {
//                $user1->notify(new ClickSendTest($code));
//            } catch (\Exception $e) {
//                // do something when error
//                session()->put('mobileMessage', trans('userFront.FailedToSendCodeToPhone'));
//            }
//            if (session()->has('verification_code')) {
//                session()->forget(['verification_code']);
//                session()->put('verification_code', $code);
//
//            } else {
//                session(['verification_code' => $code]);
//            }
//            session()->put('mobileMessage', trans('userFront.sendSms'));
        }
        //send notification to admin
        event(new ServiceProviderJoined($this->type));
        $message=trans( 'serviceProvider.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);



//        $this->emitTo('category.vendor-component', 'updateNewVendorCount');
//        $this->emitTo('category.restaurant-component', 'updateNewRestaurantCount');
        Auth::login($user);
        return redirect()->route('Dashboard',['locale'=>App::getLocale()]);
    }


    public function getCities($country_id,$name)
    {
        $this->selectedCountry=$country_id;
        $this->countryName=$name;
        $this->getCitiesForCountry($this->selectedCountry);
    }
    public function sendCityVal($cityID,$cityName)
    {
        $this->selectedCity=$cityID;
        $this->cityName=$cityName;
    }
    public function restFilde()
    {
         $this->photo=null;
         $this->selectedCity=null;
         $this->selectedCountry=null;
         $this->countryName=null;
         $this->cityName=null;
         $this->type=null;
         $this->DefaultName=null;
         $this->EnglishName=null;
         $this->address=null;
         $this->FathersName=null;
         $this->FirstName=null;
         $this->LastName=null;
         $this->Phone=null;
         $this->Email=null;
         $this->Password=null;
         $this->PasswordConfirmation=null;
    }
    public function mount()
    {
        $this->getCountries();
    }
    public function render()
    {
        return view('livewire.template.user.register-shop-or-restaurant');
    }
}
