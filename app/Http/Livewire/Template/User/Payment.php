<?php

namespace App\Http\Livewire\Template\User;

use Livewire\Component;

class Payment extends Component
{
    public function render()
    {
        return view('livewire.template.user.payment');
    }
}
