<?php

namespace App\Http\Livewire\Template\User;

use App\Mail\EmailVerificationCodeSend;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class VerifyEmail extends Component
{
    public $code;

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function SubmitVerifyEmail()
    {
        $messages = [
            "code.digits" => trans( 'userFront.codeDigits' ),
        ];
        $this->validate([
            'code' => 'digits:4',
        ],$messages);

        if(time() - session()->get('start_user_time_for_Email') < 300)
        {
            $verification_code= session()->get('email_verification_code');
            if($this->code==$verification_code)
            {
                session(['verification_status'=>trans( 'userFront.VerificationDoneSuccessfully' )])  ;
                $date=now();
                DB::table('users')->where('id',Auth::id())->update(['email_verified_at'=>$date]);
                return redirect(App::getLocale().'/');

            }
            else
            {
                session(['verification_status'=>trans( 'userFront.VerificationFailed' )])  ;
//                return redirect()->route('Verify-My-Email',['locale'=>app()->getLocale()]);
                return ;
            }
        }
        else
        {
            session(['verification_status'=>trans( 'userFront.VerificationTimeOut' )])  ;
//            return redirect()->route('Verify-My-Email',['locale'=>app()->getLocale()]);
            return;
        }


    }

    public function sendEmailAnotherVerifyCode()
    {
        //        dd('ff');
        $user=User::find(Auth::id());
        $email=$user->email;
//        $code=rand(1111,9999);
//        $user1=null;
        $submitedCode=rand(1111,9999);
        session(['start_user_time_for_Email'=>time()]);
        Mail::to($email)->send(new EmailVerificationCodeSend( $submitedCode));
        if(session()->has('email_verification_code'))
        {
            session()->forget(['email_verification_code']);
            session()->put('email_verification_code',  $submitedCode);

        }
        else
        {
            session(['email_verification_code'=>$submitedCode]);
        }

        session(['verification_status'=>trans( 'userFront.VerificationRest' )])  ;

    }


    public function render()
    {
        return view('livewire.template.user.verify-email');
    }
}
