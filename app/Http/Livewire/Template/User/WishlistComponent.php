<?php

namespace App\Http\Livewire\Template\User;


use App\Helpers\Cart\CartFacade;
use App\Models\Food;
use App\Models\Product;
use App\Models\UserCart;
use App\Models\UserWishlist;
use App\Traits\WithServiceProviderTrairt;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class WishlistComponent extends Component
{
    use WithServiceProviderTrairt,WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $userID;
    public $itemID;
    protected $listeners = [
        'removeWish'=>'render'

    ];

    public function getQueryString()
    {
        return [];
    }

    public function mount()
    {
        $this->userID = Auth::id();
    }

    public function render()
    {
        $wishes = UserWishlist::with(['country:countries.id',
            'city:cities.id',
            'city.translation' => function ($query) {
                $query->select('name', 'city_id')
                    ->where('code_lang', 'en')
                    ->get();
            }, 'country.translation' => function ($query) {
                $query->select('name', 'country_id')
                    ->where('code_lang', 'en')
                    ->get();
            },
            'product:products.id,products.photo,products.service_provider_id,products.price',
            'food:food.id,food.photo,food.service_provider_id,food.price',
            'serviceProvider:service_providers.id,service_providers.slug,service_providers.delivaz_commission',
            'user',
            'food.translation',
            'product.translation'])->where('user_id', $this->userID)
            ->paginate(10);

        $this->resetPage();
//        dd($wishes);
        return view('livewire.template.user.wishlist-component', compact('wishes'));
    }
}
