<?php

namespace App\Http\Livewire\Template\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
class PersonalImage extends Component
{
    use WithFileUploads;


    public $photo;
    public $user;

    protected $listeners = [
        'userUpdated',

//        'getItemByFilter'=>'render',
//        'getItemByFilter'=>'$refresh'
    ];

    public function userUpdated()
    {
        $this->user=User::find(Auth::id());
    }
    public function changePhoto()
    {
        $user=User::find(Auth::user()->id);
        $imageName=time().".".$this->photo->extension();
        if( $this->photo->storeAs('public/images/avatars',$imageName))
        {
            if($user->avatar!=null)
            {
                $oldAvatar=$user->avatar;
                Storage::delete('public/images/avatars/'.$oldAvatar);
            }
            $user->avatar=$imageName;
            $user->save();
            $message = trans('userFront.photoChangeSuccess');
            $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        }
//        dd($this->photo);
    }
    public function mount()
    {
        $this->user=Auth::user();
    }
    public function render()
    {
        return view('livewire.template.user.personal-image');
    }
}
