<?php

namespace App\Http\Livewire\Template\User\Checkout;

use Livewire\Component;

class AddressComponent extends Component
{
    public $address;
    public $type;
    public $selectedAddress;
    public $activeAddress;

    public function editUserAddress($addressID,$type)
    {
        $address['id']=$addressID;
        $address['type']=$type;
        $this->emitTo('template.user.checkout.add-new-address-component', 'editUserAddress',$address);
        $this->emitUp('editUserAddress',$addressID);
        $this->dispatchBrowserEvent('close-drop-down');
    }
    public function setAddressSelect($type,$address)
    {
        $res['addressID']=$address['id'];
        $res['type']=$type;
        $address['type']=$type;
//        $address['fullID']=$address['id'].$type;
        $this->selectedAddress=$address['id'].$type;
//        dd($address,$type);

//        dd($type);
        $this->emitUp('setActiveAddress',$address['id'].$type);
        $this->emitUp('selectNewAddress',$res);
        request()->session()->put('AddressSelected', $address);
        $this->emitTo('template.user.checkout.payment-component', 'addressSelected');
//        if($type=="to")
        $res=[];
//        dd($this->selectedAddress);
    }
    public function render()
    {
        return view('livewire.template.user.checkout.address-component');
    }
}
