<?php

namespace App\Http\Livewire\Template\User\Checkout;

use Livewire\Component;

class OrderSchedulingComponent extends Component
{
    public function render()
    {
        return view('livewire.template.user.checkout.order-scheduling-component');
    }
}
