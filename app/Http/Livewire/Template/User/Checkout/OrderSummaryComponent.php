<?php

namespace App\Http\Livewire\Template\User\Checkout;

use App\Helpers\Cart\CartFacade;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\UserCart;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class OrderSummaryComponent extends Component
{

    public $itemsInCart;
    public $totalForItemCart;
    public $totalOrder;
    public $country;
    public $currency_code;
    public $storedAddress;
    public $foldersForFood="storage/images/food/";
    public $foldersForProduct="storage/images/product/";
    public $cityName;
    public $countryName;
    public $cashCost;
    public $deliveryCharge;
    public $atDoorCost;
    public $asapCost;
    public $scheduleCost;


    public function mount()
    {

//        dd( request()->session()->get('AddressSelected'));

        $this->totalForItemCart=CartFacade::getTotal($this->itemsInCart);
        $this->totalOrder=$this->totalForItemCart+$this->deliveryCharge;
        if(isset($this->country->currency_code))
        {
            $this->currency_code=$this->country->currency_code;
        }
//        dd($this->itemsInCart['products'][0]['country_id'],$this->country);
//        $this->country=Country::find()
    }
    public function render()
    {
        $this->storedAddress=request()->session()->get('AddressSelected');
        return view('livewire.template.user.checkout.order-summary-component');
    }
}
