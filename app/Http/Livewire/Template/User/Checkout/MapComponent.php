<?php

namespace App\Http\Livewire\Template\User\Checkout;

use Livewire\Component;

class MapComponent extends Component
{
    public function render()
    {
        return view('livewire.template.user.checkout.map-component');
    }
}
