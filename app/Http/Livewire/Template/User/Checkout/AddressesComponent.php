<?php

namespace App\Http\Livewire\Template\User\Checkout;

use App\Helpers\GuestAddresses\AddressesFacade;
use App\Models\OrderAddress;
use App\Models\OrderToAddress;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AddressesComponent extends Component
{
    public $openDeleteToast=false;
    public $myAddresses=[];
    public $myAddressesTo=[];
    public $user_id;
    public $activeAddress;
//    public $selectedAddress;
//    public $itemID;
//    public $itemType;
    protected $listeners = ['refreshInfo'=>'mount','itemDeleted'=>'mount','setActiveAddress'];





    public function mount()
    {
        if(Auth::check())
        {
            $this->user_id=Auth::id();
//            dd($this->user_id);
            $this->myAddresses=OrderAddress::where('user_id',$this->user_id)
                ->get()->toArray();
            $this->myAddressesTo=OrderToAddress::where('user_id',$this->user_id)
                ->get()->toArray();
        }
        else
        {
//            AddressesFacade::clear();
            $GuestAddresses=AddressesFacade::get();
            $this->myAddresses= $GuestAddresses['addresses'];
//            dd($this->myAddresses);
        }

//        dd($this->myAddresses,$this->myAddressesTo);

    }
    public function setActiveAddress($activeAddress)
    {
        $this->activeAddress=$activeAddress;
    }
    public function goToAddNewAddress()
    {
//        dd("ooo");
        $this->emitTo('template.user.checkout.add-new-address-component', 'addNewOne');
        $this->emitUp('goToAddNewAddress');

    }


    public function render()
    {
        return view('livewire.template.user.checkout.addresses-component');
    }
}
