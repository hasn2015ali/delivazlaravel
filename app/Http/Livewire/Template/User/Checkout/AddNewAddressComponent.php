<?php

namespace App\Http\Livewire\Template\User\Checkout;

use App\Helpers\GuestAddresses\AddressesFacade;
use App\Models\OrderAddress;
use App\Models\OrderToAddress;
use App\Traits\WithCountryAndCity;
use App\Traits\WithUserValidationTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class AddNewAddressComponent extends Component
{
    use WithCountryAndCity, WithUserValidationTrait;

    public $countriesCode = [];
    public $showGiftSection = false;
    public $updateAddress = false;

    public $addressName;
    public $firstName;
    public $lastName;
    public $mobileNumber;
    public $validateMobileNumber;
    public $buildingName;
    public $nearestLandmark;
    public $addressType;
    public $firstNameRecipient;
    public $lastNameRecipient;
    public $recipientMobileNumber;
    public $validateRecipientMobileNumber;
    public $recipientsBuilding;
    public $firstNameSender;
    public $lastNameSender;
    public $senderMobileNumber;
    public $validateSenderMobileNumber;
    public $addressOnMap;


    public $selectedCountryCode;
    public $selectedCountryCodeForRecipient;
    public $userId=null;
    public $updatingAddressID;

    protected $listeners = ['setAddressOnMap', 'editUserAddress', 'addNewOne'];

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }

    public function addNewOne()
    {
        $this->showGiftSection = false;
        $this->updateAddress = false;
        $this->restFilde();
        $this->getUserInfo();

    }

    public function setAddressOnMap($address)
    {
        $this->addressOnMap = $address;
//        dd($this->addressOnMap);
    }

    public function updatedMobileNumber()
    {
        $messages = [
            "selectedCountryCode.required" => trans('checkout.selectedCountryCodeRequired'),
        ];
        $res = $this->validate([
            'selectedCountryCode' => 'required',
        ], $messages);

//        dd($this->mobileNumber,$this->selectedCountryCode);
        $result = $this->IsValidPhone($this->selectedCountryCode, $this->mobileNumber);
        if ($result == null) {
            $this->addError('mobileNumber', trans('checkout.ErrorPhoneValidation'));
            $this->validateMobileNumber = null;
        } else {
            $this->validateMobileNumber = $result;
        }
    }

    public function updatedRecipientMobileNumber()
    {
        $result = $this->IsValidPhone($this->selectedCountryCodeForRecipient, $this->recipientMobileNumber);
        if ($result == null) {
            $this->addError('recipientMobileNumber', trans('checkout.ErrorPhoneValidation'));
            $this->validateRecipientMobileNumber = null;
        } else {
            $this->validateRecipientMobileNumber = $result;
        }
    }

    public function updatedSenderMobileNumber()
    {
        $result = $this->IsValidPhone($this->selectedCountryCode, $this->senderMobileNumber);
        if ($result == null) {
            $this->addError('senderMobileNumber', trans('checkout.ErrorPhoneValidation'));
            $this->validateSenderMobileNumber = null;
        } else {
            $this->validateSenderMobileNumber = $result;
        }
    }

    public function editUserAddress($address)
    {
//        dd($address);
        $this->updatingAddressID=$address['id'];

        if(Auth::check())
        {
            if ($address['type'] == "me") {
                $this->showGiftSection = false;
                $this->updateAddress = true;
                $addressEdited = OrderAddress::find($address['id']);
                $this->addressName = $addressEdited->addressName;
                $this->firstName = $addressEdited->firstName;
                $this->lastName = $addressEdited->lastName;
//            $this->mobileNumber=$addressEdited->mobileNumber;
                $this->addressOnMap=$addressEdited->addressOnMap;
                $this->addressType = $addressEdited->addressType;
                $this->buildingName = $addressEdited->buildingName;
                $this->nearestLandmark = $addressEdited->nearestLandmark;

                $res = $this->getPhoneNumberAndCountryCode($addressEdited->mobileNumber);
                $this->mobileNumber = $res['mobileNumber'];
                $this->selectedCountryCode = $res['countryCode'];
                $this->validateMobileNumber = $addressEdited->mobileNumber;

//            $res=$this->getPhoneNumberAndCountryCode("361198577845578857895","mobileNumber", trans( 'checkout.ErrorPhoneValidation'));
//            dd($res);
            }
            if ($address['type'] == "to") {
                $this->showGiftSection = true;
                $this->updateAddress = true;
                $addressEdited = OrderToAddress::find($address['id']);
                $this->addressName = $addressEdited->addressName;
                $this->firstNameRecipient = $addressEdited->firstNameRecipient;
                $this->lastNameRecipient = $addressEdited->lastNameRecipient;
//            $this->recipientMobileNumber=$addressEdited->mobileNumberRecipient;
                $this->addressOnMap = $addressEdited->addressOnMap;
                $this->recipientsBuilding = $addressEdited->buildingNameRecipient;
                $this->buildingName = $addressEdited->buildingName;
                $this->nearestLandmark = $addressEdited->nearestLandmark;
                $this->firstNameSender = $addressEdited->firstNameSender;
                $this->lastNameSender = $addressEdited->lastNameSender;
//            $this->senderMobileNumber=$addressEdited->mobileNumberSender;

                $res = $this->getPhoneNumberAndCountryCode($addressEdited->mobileNumberRecipient);
                $this->recipientMobileNumber = $res['mobileNumber'];
                $this->selectedCountryCodeForRecipient = $res['countryCode'];
                $this->validateRecipientMobileNumber = $addressEdited->mobileNumberRecipient;

                $res2 = $this->getPhoneNumberAndCountryCode($addressEdited->mobileNumberSender);
                $this->senderMobileNumber = $res2['mobileNumber'];
                $this->selectedCountryCode = $res2['countryCode'];
                $this->validateSenderMobileNumber = $addressEdited->mobileNumberSender;

            }
            $this->dispatchBrowserEvent('go-old-address', ['oldAddress' => $addressEdited->addressOnMap]);

        }
        else
        {
            if ($address['type'] == "me") {
                $this->showGiftSection = false;
                $this->updateAddress = true;
//                dd(AddressesFacade::findAddress($address['id']));
//                $addressEdited = OrderAddress::find($address['id']);
                $addressEdited = AddressesFacade::findAddress($address['id']);
                $this->addressName = $addressEdited['addressName'];
                $this->firstName = $addressEdited['firstName'];
                $this->lastName = $addressEdited['lastName'];
//            $this->mobileNumber=$addressEdited->mobileNumber;
                $this->addressOnMap=$addressEdited['addressOnMap'];
                $this->addressType = $addressEdited['addressType'];
                $this->buildingName = $addressEdited['buildingName'];
                $this->nearestLandmark = $addressEdited['nearestLandmark'];

                $res = $this->getPhoneNumberAndCountryCode($addressEdited['mobileNumber']);
                $this->mobileNumber = $res['mobileNumber'];
                $this->selectedCountryCode = $res['countryCode'];
                $this->validateMobileNumber = $addressEdited['mobileNumber'];
                $this->dispatchBrowserEvent('go-old-address', ['oldAddress' => $addressEdited['addressOnMap']]);

//            $res=$this->getPhoneNumberAndCountryCode("361198577845578857895","mobileNumber", trans( 'checkout.ErrorPhoneValidation'));
//            dd($res);
            }
        }



    }

    public function addNewAddress()
    {
//        $this->dispatchBrowserEvent('reset-map');
//
//        return;


//        dd("test");



        $messages = [
            "addressName.required" => trans('checkout.addressNameRequired'),
            "firstName.required" => trans('checkout.firstNameRequired'),
            "lastName.required" => trans('checkout.lastNameRequired'),
            "mobileNumber.required" => trans('checkout.mobileNumberRequired'),
            "buildingName.required" => trans('checkout.buildingNameRequired'),
            "addressOnMap.required" => trans('checkout.addressOnMapRequired'),

            "firstNameSender.required" => trans('checkout.firstNameSenderRequired'),
            "lastNameSender.required" => trans('checkout.lastNameSenderRequired'),
            "senderMobileNumber.required" => trans('checkout.senderMobileNumberRequired'),
            "firstNameRecipient.required" => trans('checkout.firstNameRecipientRequired'),
            "lastNameRecipient.required" => trans('checkout.lastNameRecipientRequired'),
            "recipientsBuilding.required" => trans('checkout.recipientsBuildingRequired'),
            "recipientMobileNumber.required" => trans('checkout.recipientMobileNumberRequired'),
            "selectedCountryCode.required" => trans('checkout.selectedCountryCodeRequired'),

            "selectedCountryCodeForRecipient.required" => trans('checkout.selectedCountryCodeRequired'),

        ];
        if($this->userId!=0)
        {
            if (!$this->showGiftSection) {
                // validate mobile number
                $result = $this->IsValidPhone($this->selectedCountryCode, $this->mobileNumber);
                if ($result == null) {
                    $this->addError('mobileNumber', trans('checkout.ErrorPhoneValidation'));
                    $this->validateMobileNumber = null;
                    return;
                }
                $res = $this->validate([
                    'addressName' => 'required',
                    'firstName' => 'required',
                    'lastName' => 'required',
                    'mobileNumber' => 'required',
                    'buildingName' => 'required',
                    'addressOnMap' => 'required',
                    'selectedCountryCode' => 'required',


                ], $messages);

                OrderAddress::updateOrCreate(['id'=>$this->updatingAddressID],
                    [
                        'addressName' => $this->addressName,
                        'firstName' => $this->firstName,
                        'lastName' => $this->lastName,
                        'mobileNumber' => $this->validateMobileNumber,
                        'buildingName' => $this->buildingName,
                        'addressOnMap' => $this->addressOnMap,
                        'addressType' => $this->addressType,
                        'nearestLandmark' => $this->nearestLandmark,
                        'user_id' => $this->userId,
                    ]);

            } else {
                $result2 = $this->IsValidPhone($this->selectedCountryCodeForRecipient, $this->recipientMobileNumber);
                if ($result2 == null) {
                    $this->addError('recipientMobileNumber', trans('checkout.ErrorPhoneValidation'));
                    $this->validateRecipientMobileNumber = null;
                    return;
                }

                $result3 = $this->IsValidPhone($this->selectedCountryCode, $this->senderMobileNumber);
                if ($result3 == null) {
                    $this->addError('senderMobileNumber', trans('checkout.ErrorPhoneValidation'));
                    $this->validateSenderMobileNumber = null;
                    return;
                }

                $res = $this->validate([
                    'addressName' => 'required',
                    'firstNameSender' => 'required',
                    'lastNameSender' => 'required',
                    'senderMobileNumber' => 'required',
                    'firstNameRecipient' => 'required',
                    'lastNameRecipient' => 'required',
                    'recipientsBuilding' => 'required',
                    'recipientMobileNumber' => 'required',
                    'selectedCountryCode' => 'required',
                    'selectedCountryCodeForRecipient' => 'required',
                    'addressOnMap' => 'required',

                ], $messages);

                OrderToAddress::updateOrCreate(['id'=>$this->updatingAddressID],
                    [
                        'addressName' => $this->addressName,
                        'firstNameRecipient' => $this->firstNameRecipient,
                        'lastNameRecipient' => $this->lastNameRecipient,
                        'mobileNumberRecipient' => $this->validateRecipientMobileNumber,
                        'buildingNameRecipient' => $this->recipientsBuilding,
                        'addressOnMap' => $this->addressOnMap,
                        'firstNameSender' => $this->firstNameSender,
                        'lastNameSender' => $this->lastNameSender,
                        'mobileNumberSender' => $this->validateSenderMobileNumber,
                        'nearestLandmark' => $this->nearestLandmark,
                        'user_id' => $this->userId,
                    ]);
            }


        }
        else
        {
//            dd(AddressesFacade::get());
//            AddressesFacade::clear();

           $this->validate([
                'addressName' => 'required',
                'firstName' => 'required',
                'lastName' => 'required',
                'mobileNumber' => 'required',
                'buildingName' => 'required',
                'addressOnMap' => 'required',
                'selectedCountryCode' => 'required',


            ], $messages);

//           dd($this->updateAddress);
            $address['addressName']=$this->addressName;
            $address['firstName']=$this->firstName;
            $address['lastName']=$this->lastName;
            $address['mobileNumber']=$this->validateMobileNumber;
            $address['buildingName']=$this->buildingName;
            $address['addressOnMap']=$this->addressOnMap;
            $address['nearestLandmark']=$this->nearestLandmark;
            $address['user_id']=$this->userId;
            $address['addressType']=$this->addressType;
            if($this->updateAddress==true)
            {
                AddressesFacade::update($this->updatingAddressID,$address);
            }
            else
            {
                AddressesFacade::add($address);

            }
//            dd($address);

        }
        if($this->updateAddress==true)
        {
            $this->updatingAddressID=null;
            $message = trans('masterControl.updated_successfully');
            $this->emit('alert', ['icon' => 'success', 'title' => $message]);

        }
        else
        {
            $message = trans('masterControl.successfully_Added');
            $this->emit('alert', ['icon' => 'success', 'title' => $message]);
            $this->restFilde();
            $this->getUserInfo();
        }
        $this->emitTo('template.user.checkout.addresses-component', 'refreshInfo');
        $this->emit('goBackToAddresses');
//        dd($res);
    }

    public function setCountryCode($code)
    {
        $this->selectedCountryCode = $code;
    }

    public function setCountryCodeForRecipient($code)
    {
        $this->selectedCountryCodeForRecipient = $code;
    }

    public function restFilde()
    {
        $this->addressName = null;
        $this->firstName = null;
        $this->lastName = null;
        $this->mobileNumber = null;
        $this->buildingName = null;
        $this->addressOnMap = null;
        $this->nearestLandmark = null;

        $this->firstNameSender = null;
        $this->firstNameSender = null;
        $this->lastNameSender = null;
        $this->senderMobileNumber = null;
        $this->firstNameRecipient = null;
        $this->lastNameRecipient = null;
        $this->recipientsBuilding = null;
        $this->recipientMobileNumber = null;

        $this->validateMobileNumber = null;
        $this->validateSenderMobileNumber = null;
        $this->validateRecipientMobileNumber = null;

        $this->dispatchBrowserEvent('reset-map');
//        $this->selectedCountryCode=null;

    }

    public function changeGiftState()
    {
        $this->showGiftSection == false ? $this->showGiftSection = true : $this->showGiftSection = false;
    }

    public function getUserInfo()
    {
        if (Auth::user() != null) {
            $this->userId = Auth::id();
            $this->firstName = Auth::user()->firstName;
            $this->lastName = Auth::user()->lastName;
//            dd($this->firstName);

        } else {
            $this->userId = "0";
        }
    }

    public function mount()
    {
        $this->countriesCode = $this->getCountriesCodes();

        $this->getUserInfo();
//        Auth::user()?$this->userId=Auth::id():$this->userId="0";
//        dd(Auth::user(),$this->userId);
    }

    public function render()
    {
        return view('livewire.template.user.checkout.add-new-address-component');
    }
}
