<?php

namespace App\Http\Livewire\Template\User\Checkout;

use App\Helpers\Cart\CartFacade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class PaymentComponent extends Component
{
    public $selectedPayment="card";
    protected $listeners = ['selectedPaymentMethod','addressSelected','setTotalOrder'];
    public $addressSelected;
    public $repeatOrderState=false;
    public $selectedIntervals="Daily";
    public $selectedIntervalsTranslated;
    public $ongoingState=false;

    public $atDoorSelected=false;
    public $inDaySelected=false;
    public $scheduleSelected=false;


    public $atDoorOption=true;
    public $user;
    public $balanceInWallet=0;
    public $isUserLogin;
    public $itemsInCart;
    public $cityName;
    public $countryName;
    public $country;
    public $orderIdentifier;
    public $subtotal;
    public $cashCost;
    public $deliveryCharge;
    public $atDoorCost;
    public $asapCost;
    public $scheduleCost;
    public $currency_code;
    public $totalOrder;
    public $totalForItemCart;


    public function setTotalOrder($totalOrder)
    {
        $this->totalOrder=$totalOrder;
    }

    public function setDoorSelected()
    {
        if($this->atDoorSelected!=true)
        {
            $this->atDoorSelected=true;
            $this->inDaySelected=false;
            $this->scheduleSelected=false;
            $this->emit('addNewCost','atDoor');
            $this->emit('removeCost','ASAP');
            $this->emit('removeCost','schedule');

        }
        else
        {
            $this->resetOrderOptions();
            $this->emit('removeCost','atDoor');
        }

    }

    public function setInDaySelected()
    {
        if(  $this->inDaySelected!=true)
        {
            $this->atDoorSelected=false;
            $this->inDaySelected=true;
            $this->scheduleSelected=false;
            $this->emit('addNewCost','ASAP');
            $this->emit('removeCost','atDoor');
            $this->emit('removeCost','schedule');

        }
        else
        {
            $this->resetOrderOptions();
            $this->emit('removeCost','ASAP');
        }

    }
    public function setScheduleSelected()
    {
        if(  $this->scheduleSelected!=true)
        {
            $this->atDoorSelected=false;
            $this->inDaySelected=false;
            $this->scheduleSelected=true;
            $this->emit('addNewCost','schedule');
            $this->emit('removeCost','ASAP');
            $this->emit('removeCost','atDoor');
        }
        else
        {
            $this->resetOrderOptions();
            $this->emit('removeCost','schedule');
        }

    }

    public function getOrderOptionSelected()
    {
        if($this->atDoorSelected==true){ return "atDoor";}
        if($this->inDaySelected==true){ return "inDay";}
        if($this->scheduleSelected==true){ return "scheduled";}
        return null;

    }

    public function resetOrderOptions()
    {
        $this->atDoorSelected=false;
        $this->inDaySelected=false;
        $this->scheduleSelected=false;
    }
    public function changeRepeatOrderState()
    {
        $this->repeatOrderState==true?$this->repeatOrderState=false:$this->repeatOrderState=true;
        if($this->repeatOrderState==false)
        {
            $this->ongoingState=false;
        }
        $this->dispatchBrowserEvent('initialize-date-picker');

//        dd("ll");
    }

    public function changeOngoingState()
    {

        $this->ongoingState==true?$this->ongoingState=false:$this->ongoingState=true;
        $this->dispatchBrowserEvent('initialize-date-picker');
    }
    public function selectIntervals($interval)
    {
        if($interval=="Daily"){
            $this->selectedIntervalsTranslated= __("checkout.Daily");
        }
        elseif ($interval=="EveryTwoDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryTwoDays");

        }
        elseif ($interval=="EveryThreeDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryThreeDays");

        }
        elseif ($interval=="EveryFourDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryFourDays");

        }
        elseif ($interval=="EveryFiveDays"){
            $this->selectedIntervalsTranslated= __("checkout.EveryFiveDays");

        }
        elseif ($interval=="EverySixDays"){
            $this->selectedIntervalsTranslated= __("checkout.EverySixDays");

        }
        elseif ($interval=="EveryWeek"){
            $this->selectedIntervalsTranslated= __("checkout.EveryWeek");

        }

        $this->selectedIntervals=$interval;
        $this->dispatchBrowserEvent('close-drop-down');
//        dd($interval);
    }
    public function selectedPaymentMethod($paymentMethod)
    {
//        dd($paymentMethod);
        $this->selectedPayment=$paymentMethod;
        if($this->selectedPayment=="cash")
        {
            $this->atDoorOption=false;
            $this->atDoorSelected==true?$this->atDoorSelected=false:$this->atDoorSelected=false;
        }
        else
        {
            $this->atDoorOption=true;

        }
//        elseif($this->selectedPayment='wallet')
//        {
//
//            if($this->user->balance==null)
//            {
//
//            }
//            dd($this->user->balance);
////            $this->atDoorOption=true;
//        }
//
//        dd($this->selectedPayment);
    }

    public function placeOrder()
    {
        $this->orderIdentifier=rand(1000000,1000000000);
        $orderOption=$this->getOrderOptionSelected();
        
//        dd($this->itemsInCart,$this->orderIdentifier,$this->addressSelected['id'],
//            $this->addressSelected['type'],$this->selectedPayment,$orderOption,$this->totalOrder);
        $this->emitUp('goToOrderSummery');
    }

    public function addressSelected()
    {
        if(Session::has('AddressSelected'))
        {
            $this->addressSelected=request()->session()->get('AddressSelected');
        }
    }
    public function mount()
    {
        $this->selectedIntervalsTranslated= __("checkout.Daily");

        $this->isUserLogin=Auth::check();
        if(isset($this->country->currency_code))
        {
            $this->currency_code=$this->country->currency_code;
        }
//        dd($this->isUserLogin);
       if(Auth::check())
       {
           $this->user=Auth::user();
           $this->user->balance==null?$this->balanceInWallet=0.00:$this->balanceInWallet=$this->user->balance;
       }
        $this->totalOrder = $this->totalForItemCart + $this->deliveryCharge;
    }
    public function render()
    {


        return view('livewire.template.user.checkout.payment-component');
    }
}
