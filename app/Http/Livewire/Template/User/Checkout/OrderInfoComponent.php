<?php

namespace App\Http\Livewire\Template\User\Checkout;

use App\Helpers\Cart\CartFacade;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\UserCart;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class OrderInfoComponent extends Component
{
    public $step;
    public $itemsInCart;
    public $totalForItemCart;
    public $totalOrder;
    public $country;
    public $currency_code;
    public $storedAddress;
    public $countryName;
    public $cityName;
    public $locale;
    public $showCashCost = false;
    public $showAtDoorCost = false;
    public $showASAPCost = false;
    public $showScheduleCost = false;
    public $cashCost;
    public $deliveryCharge;
    public $atDoorCost;
    public $asapCost;
    public $scheduleCost;

    protected $listeners = ['updateOrderInfo' => 'render', 'addNewCost', 'removeCost'];

    public function addNewCost($type)
    {
//        dd("dd");
        if ($type == "cash") {
            if ($this->cashCost > 0) {
                $this->showCashCost = true;
                $this->totalOrder = $this->totalOrder + $this->cashCost;
            }
        }

        if ($type == "atDoor") {
            if ($this->atDoorCost > 0) {
                $this->showAtDoorCost = true;
                $this->totalOrder = $this->totalOrder + $this->atDoorCost;
            }
        }

        if ($type == "ASAP") {
            if ($this->asapCost > 0) {
                $this->showASAPCost = true;
                $this->totalOrder = $this->totalOrder + $this->asapCost;
            }
        }

        if ($type == "schedule") {
            if ($this->scheduleCost > 0) {
                $this->showScheduleCost = true;
                $this->totalOrder = $this->totalOrder + $this->scheduleCost;
            }
        }

        $this->emitUp('setTotalOrder',$this->totalOrder);

    }

    public function removeCost($type)
    {
        if ($type == "cash") {
            if ($this->showCashCost == true) {
                $this->showCashCost = false;
                $this->totalOrder = $this->totalOrder - $this->cashCost;
            }

        }

        if ($type == "atDoor") {
            if ($this->showAtDoorCost == true) {
                $this->showAtDoorCost = false;
                $this->totalOrder = $this->totalOrder - $this->atDoorCost;
            }
        }

        if ($type == "ASAP") {
            if ($this->showASAPCost == true) {
                $this->showASAPCost = false;
                $this->totalOrder = $this->totalOrder - $this->asapCost;
            }

        }

        if ($type == "schedule") {
            if ($this->showScheduleCost == true) {
                $this->showScheduleCost = false;
                $this->totalOrder = $this->totalOrder - $this->scheduleCost;
            }

        }

        $this->emitUp('setTotalOrder',$this->totalOrder);
    }

    public function mount()
    {

//        dd( request()->session()->get('AddressSelected'));
        $this->locale = App::getLocale();

//        dd($this->storedAddress);
//        $this->totalForItemCart = CartFacade::getTotal($this->itemsInCart);
        $this->totalOrder = $this->totalForItemCart + $this->deliveryCharge;
//        $this->country=Country::with('translation')
//            ->find($this->itemsInCart['products'][0]['country_id']);
//        $this->countryName=$this->country->translation->where('code_lang','en')->first()->name;
//        $this->cityName=CityTranslation::where('city_id',$this->itemsInCart['products'][0]['city_id'])
//            ->first()->name;
        if (isset($this->country->currency_code)) {
            $this->currency_code = $this->country->currency_code;
        }
//        dd($this->itemsInCart['products'][0]['country_id'],$this->country);
//        $this->country=Country::find()
    }

    public function render()
    {
        $this->storedAddress = request()->session()->get('AddressSelected');
//        dd($this->storedAddress);
        return view('livewire.template.user.checkout.order-info-component');
    }
}
