<?php

namespace App\Http\Livewire\Template\User;

use App\Mail\EmailVerificationCodeSend;
use App\Models\Country;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithUserValidationTrait;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class PersonalInfo extends Component
{
    use WithUserValidationTrait;
    public $user;
    public $address;
    public $firstName;
    public $fatherName;
    public $lastName;
    public $email;
    public $phone1;
    public $oldPhone;
    public $oldEmail;
    public $isShowing=false;
    public $countryCodeNumber;
    public $allCountriesCodes;
    public $phoneNumberRegionCode;
    public $phoneNumberFormatedByRegion;




//    protected $listeners = [
//        'refreshData'=>'$refresh'
//    ];
    public function hydrate()
    {
        $this->resetErrorBag();
    }
    public function messages()
    {
        return [
            'firstName.required' => trans( 'userFront.validateFirstName' ),
            'lastName.required' => trans( 'userFront.validateLastName' ),
            'email.required' => trans( 'userFront.validateEmail' ),
            'phone1.required' => trans( 'userFront.validatePhone1' ),
            'address.required' => trans( 'userFront.validateAddress' ),

        ];
    }
    public function toggleIsShowing()
    {
        $this->isShowing=!$this->isShowing;
//        dd($this->allCountriesCodes);
//        $this->allCountriesCodes=Country::select('code_number')
//            ->where('code_number','!=',0)
//            ->get();
    }
    public function updateInfo()
    {


//                dd($this->address,$this->firstName,$this->fatherName,$this->lastName,$this->email,$this->phone1);


        if(!filter_var($this->email,FILTER_VALIDATE_EMAIL))
        {
            $message=trans( 'userFront.ErrorEmail' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return;
        }
        $this->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'phone1' => 'required|numeric',
            'address' => 'required',
        ]);

        $result=$this->IsValidPhone($this->countryCodeNumber,$this->phone1);
        if($result==null)
        {
            $this->addError('phone1', trans( 'userFront.ErrorPhoneValidation'));
            return;
//            $this->validPhone=null;
        }


        $user=User::find($this->user->id);

        if($this->oldEmail!=$this->email)
        {
            if($this->checkEmail($this->email)==1)
            {
                return;
            }
            $user->email_verified_at=null;
            $this->sendVerificationCodeToEmail();
        }
        if($this->oldPhone!=$this->countryCodeNumber.$this->phone1)
        {
            if($this->checkPhone($this->phone1)==1)
            {
                return;
            }
            $user->phone_verified=null;
            $this->sendVerificationCodeToPhone($user);
        }

        $user->firstName=$this->firstName;
        $user->fatherName=$this->fatherName;
        $user->lastName=$this->lastName;
        $user->address=$this->address;
        $user->phone =$this->countryCodeNumber.$this->phone1;
        $user->email =$this->email;
        $user->save();
        $message=trans( 'userFront.updatedInfoSuccessfully' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->dispatchBrowserEvent('closeEditForm');
//        $this->emit('refreshData');
        $this->user=User::find($this->user->id);
        $this->oldPhone=$this->user->phone;
        $this->oldEmail=$this->user->email;
        $this->emitTo('template.user.personal-image', 'userUpdated');
        $this->emitTo('template.nav-bar', 'userUpdated');
        $this->toggleIsShowing();
        $this->getUerInfo();


    }
    public function restFilde()
    {
        $this->firstName=null;
        $this->fatherName=null;
        $this->lastName=null;
        $this->address=null;
        $this->phone1=null;
        $this->email=null;

    }
    public function mount()
    {
        $this->getUerInfo();
//        $this->user=User::with('country')->find(Auth::id());



//        $this->countryCodeNumber=$this->user->country->code_number;
    }

    public function getUerInfo()
    {
//        $this->user=Auth::user();
//        dd($this->user);
        $this->user=User::find(Auth::id());
        $this->firstName=$this->user->firstName;
        $this->fatherName=$this->user->fatherName;
        $this->lastName=$this->user->lastName;
        $this->address=$this->user->address;
        $this->allCountriesCodes=Country::select('code_number')
            ->where('code_number','!=',0)
            ->get()->toArray();
        if( $this->user->email!=null)
        {
            $this->email= $this->user->email;
            $this->oldEmail=$this->user->email;
        }
        if( $this->user->phone!=null)
        {
            $this->phone1= $this->user->phone;
            $this->oldPhone=$this->user->phone;


//            dd($this->allCountriesCodes[0]->code_number);
            try {
                $number = PhoneNumber::parse("+" . $this->phone1);
                $this->countryCodeNumber=$number->getCountryCode();
                $this->phone1=$number->getNationalNumber();
                $this->phoneNumberRegionCode=$number->getRegionCode();
                $this->phoneNumberFormatedByRegion=$number->formatForCallingFrom($this->phoneNumberRegionCode);
            }
            catch (PhoneNumberParseException $exception)
            {

            }
        }
    }

    public function updatedPhone1()
    {
//        dd($this->countryCodeNumber,$this->phone1);
        $result=$this->IsValidPhone($this->countryCodeNumber,$this->phone1);
        if($result==null)
        {
            $this->addError('phone1', trans( 'userFront.ErrorPhoneValidation'));
//            $this->validPhone=null;
        }
        else
        {
//            $this->validPhone=$result;
        }
    }

    public function render()
    {

        return view('livewire.template.user.personal-info');
    }
}
