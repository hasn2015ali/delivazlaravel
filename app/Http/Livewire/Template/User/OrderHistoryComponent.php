<?php

namespace App\Http\Livewire\Template\User;

use Livewire\Component;

class OrderHistoryComponent extends Component
{
    public function render()
    {
        return view('livewire.template.user.order-history-component');
    }
}
