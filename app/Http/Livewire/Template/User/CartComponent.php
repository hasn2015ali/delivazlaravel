<?php

namespace App\Http\Livewire\Template\User;

use App\Helpers\Cart\CartFacade;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\UserCart;
use App\Models\UserWishlist;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CartComponent extends Component
{
    public $cartContent;
    public $country;
    public $city;
    public $currencyCode;
    public $foldersForFood="storage/images/food/";
    public $foldersForProduct="storage/images/product/";
    public $total;
    public $test=[];
    public $wishes=[];


    protected $listeners = [
        'cartUpdated',
        'wishesUpdated',
//        'getItemByFilter'=>'render',
//        'getItemByFilter'=>'$refresh'
    ];


    public function wishesUpdated()
    {
        if(Auth::check())
        {
            $userID=Auth::id();
            $this->wishes=UserWishlist::select('id')->where('user_id',$userID)->get();
        }

    }
    public function mount()
    {
//        CartFacade::clear();
        $this->wishesUpdated();
        if(Auth::check())
        {
//            $this->cartContent=  CartFacade::get();
//            dd($this->cartContent);
            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->cartContent=CartFacade::convertAllItemToArray($items);
            $item=null;

//            dd( $this->cartContent,$cartContent1);
        }
        else
        {
            $this->cartContent=  CartFacade::get();

        }

        if(isset($this->cartContent['products'][0]['country_id']))
        {
            $this->country=Country::find($this->cartContent['products'][0]['country_id']);

        }
        if(isset($this->country->currency_code))
        {
            $this->currencyCode=$this->country->currency_code;
        }



//       dd($this->cartContent);
        $this->total=CartFacade::getTotal($this->cartContent);
    }
    public function minusQuantity($itemId)
    {
        if(Auth::check())
        {
            $item=UserCart::find($itemId);
            if($item->quantity==1){
                return;
            }
            $item->quantity=$item->quantity-1;
            $item->save();
            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->cartContent=CartFacade::convertAllItemToArray($items);
            $item=null;

        }
        else{
            CartFacade::update($itemId,0);
            $this->cartContent = CartFacade::get();
        }
        $this->total=CartFacade::getTotal($this->cartContent);

//        dd($itemId);
    }
    public function plusQuantity($itemId)
    {

        if(Auth::check())
        {
            $item=UserCart::find($itemId);
            $item->quantity=$item->quantity+1;
            $item->save();
            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->cartContent=CartFacade::convertAllItemToArray($items);
            $item=null;
        }
        else
        {
            CartFacade::update($itemId,1);
            $this->cartContent = CartFacade::get();
        }
        $this->total=CartFacade::getTotal($this->cartContent);


    }
    public function cartUpdated()
    {
//        dd('fff');
        if(Auth::check())
        {

            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->cartContent=CartFacade::convertAllItemToArray($items);
            $item=null;
        }
        else
        {
            $this->cartContent = CartFacade::get();
        }
        $this->total=CartFacade::getTotal($this->cartContent);

    }
    public function removeFromCart($itemID): void
    {
//        dd($itemID);
        if(Auth::check())
        {
            $item=UserCart::find($itemID);
            $item->delete();
            $items=UserCart::where('user_id',Auth::id())
                ->get()
                ->toArray();
            $this->cartContent=CartFacade::convertAllItemToArray($items);
            $item=null;
        }
        else
        {
            CartFacade::remove($itemID);
            $this->cartContent = CartFacade::get();
        }

        $this->total=CartFacade::getTotal( $this->cartContent);
        $this->emitTo('template.category.add-to-basket-component', 'removeFromCart');
        $this->emitTo('template.user.user-wish-component', 'removeFromCart');
        $this->emitTo('template.category.product-component', 'removeFromCart');

    }

    public function render()
    {
        return view('livewire.template.user.cart-component');
    }
}
