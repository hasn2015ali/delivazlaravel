<?php

namespace App\Http\Livewire\Template\User;

use App\Models\User;
use App\Notifications\ClickSendTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class VerifyPhone extends Component
{
    public $code;

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function verifyPhone()
    {

        $messages = [
            "code.digits" => trans( 'userFront.codeDigits' ),
        ];
        $this->validate([
            'code' => 'digits:4',
        ],$messages);

        if(time() - session()->get('start_user_time_for_phone') < 300)
        {
            $verification_code= session()->get('verification_code');
            if($this->code==$verification_code)
            {
                session(['verification_status'=>trans( 'userFront.VerificationDoneSuccessfully' )])  ;
                $date=now();
                DB::table('users')->where('id',Auth::id())->update(['phone_verified'=>$date]);
                return redirect(App::getLocale().'/');

            }
            else
            {
                session(['verification_status'=>trans( 'userFront.VerificationFailed' )])  ;
                return redirect()->route('Verify-My-Phone',['locale'=>app()->getLocale()]);

            }
        }
        else
        {
            session(['verification_status'=>trans( 'userFront.VerificationTimeOut' )])  ;
            return redirect()->route('Verify-My-Email',['locale'=>app()->getLocale()]);
        }


    }

    public function sendAnotherVerifyCode()
    {

        $user=User::find(Auth::id());
        $phone=$user->phone;
//        $code=1111;
        $code=rand(1111,9999);
        session(['start_user_time_for_phone'=>time()]);

        try {
            $user->notify(new ClickSendTest( $code));
        }
        catch (\Exception $e) {

            session(['verification_status'=>trans( 'userFront.FailedToSendCodeToPhone' )]) ;
            return redirect()->back();
        }
        if(session()->has('verification_code'))
        {
            session()->forget(['verification_code']);
            session()->put('verification_code',  $code);

        }
        else
        {
            session(['verification_code'=>$code]);
        }

        session(['verification_status'=>trans( 'userFront.VerificationRest' )])  ;
        return redirect()->route('Verify-My-Phone',['locale'=>app()->getLocale()]);
    }
    public function render()
    {
        return view('livewire.template.user.verify-phone');
    }
}
