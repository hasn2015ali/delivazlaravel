<?php

namespace App\Http\Livewire\Template;

use App\Models\Subscriber;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\DB;
use Livewire\Component;


class Footer extends Component
{
    use WithCountryAndCity ;

    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;
    public $countryId=null;
    public $cityId=null;
    public $userName;
    public $email;


    public $country;
    public $city;

//    public $countryValue=null;

    public function getCities($countryID,$countryName)
    {
//        dd($country);
        $this->selectedCountry=$countryName;
        $this->countryId=$countryID;
        $this->getCitiesForCountry($this->countryId);
//        dd($this->cities);
    }
    public function sendCityVal($cityID,$cityName)
    {
//        dd($cityID,$cityName);

        $this->selectedCity=$cityName;
        $this->cityId=$cityID;
//                dd($this->selectedCity, $this->cityId);
    }


    public function submitSubscriber()
    {
//                dd($this->selectedCity, $this->cityId);

       if( $this->validateData()==1)
       {
           return;
       }

        Subscriber::create([
            'name' => $this->userName,
            'email' => $this->email,
            'city_id' => $this->cityId,
            'country_id' => $this->countryId,
        ]);

        $message=trans( 'userFront.subscribeSuccessfully' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }

    public function restFilde()
    {
        $this->userName=null;
        $this->email=null;
        $this->countryId=null;
        $this->selectedCountry=null;
        $this->cityId=null;
        $this->selectedCity=null;
    }

    public function validateData()
    {
        if($this->countryId==null)
        {
            $message=trans( 'userFront.validateFooterCountry' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return 1;
        }
        elseif ($this->cityId==null)
        {
            $message=trans( 'userFront.validateFooterCity' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return 1;
        }
        elseif ($this->email==null)
        {
            $message=trans( 'userFront.validateFooterEmail' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return 1;
        }
        elseif ($this->userName==null)
        {
            $message=trans( 'userFront.validateFooterUserName' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return 1;
        }
        elseif ($this->email!=null)
        {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
                $message=trans( 'userFront.validateCorrectEmail' );
                $this->emit('alert',['icon'=>'error','title'=>$message]);
                return 1;
            }

            $emailCheck = DB::table('subscribers')
                ->select('email')
                ->where('email',$this->email)
                ->first();
            if($emailCheck !=null )
            {
                $m=trans( 'user.emailVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }


        }

    }

    public function render()
    {
        $this->getCountries();
//        dd($this->countries);
        return view('livewire.template.footer');
    }
}
