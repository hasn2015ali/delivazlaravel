<?php

namespace App\Http\Livewire\Template\FooterPages;

use App\Models\CountryTermTrans;
use App\Models\CountryTranslation;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class FaqComponent extends Component
{


    public function render()
    {
        $cookies=CountryTermTrans::where('type','FAQ')
            ->where('code_lang',App::getLocale())->get();
        return view('livewire.template.footer-pages.faq-component',compact('cookies'));
    }
}
