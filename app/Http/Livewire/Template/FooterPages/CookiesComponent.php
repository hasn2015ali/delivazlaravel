<?php

namespace App\Http\Livewire\Template\FooterPages;

use App\Models\CountryTermTrans;
use App\Models\CountryTranslation;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class CookiesComponent extends Component
{
    public $country;


    public function mount()
    {
        $this->country==null ? $this->country=0 : $this->country=CountryTranslation::where('name',$this->country)->first()->country_id;
    }

    public function render()
    {
        $cookies=CountryTermTrans::where('type','cookie')->where('country_id',$this->country)
                                   ->where('code_lang',App::getLocale())->get();
        if($this->country!=0)
        {
            if(count($cookies)==0)
            {
                $cookies=CountryTermTrans::where('type','cookie')->where('country_id',0)
                    ->where('code_lang',App::getLocale())->get();
            }
        }
        return view('livewire.template.footer-pages.cookies-component',compact('cookies'));
    }
}
