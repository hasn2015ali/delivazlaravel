<?php

namespace App\Http\Livewire\Template\Index;

use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\App;
use Livewire\Component;

class CountryComponent extends Component
{
    use WithCountryAndCity;
    public $countries;
    public $cities ;
    public $countrySelected;
    public $city;
    public $country;
    public $checkedCity;
    public $locale;
    public $countryNameInURL;
    public $cityNameInURL;

    public function mount()
    {
        $this->locale=App::getLocale();
//        dd($this->checkedCity);
    }

    public function goToSelectedCity($city)
    {
//        dd($this->locale,$city['name'],$this->countryNameInURL);
        if( $this->locale=="en"){
            $this->cityNameInURL=$city['name'];
        }
        else{
            $this->cityNameInURL=$this->getCityName($city['city_id'],"en");
        }
        $this->countryNameInURL= session('selectedCountryURL');
//        dd($this->locale.'/'.$this->countryNameInURL.'/'.$this->cityNameInURL);

        $url=$this->locale.'/'.$this->countryNameInURL.'/'.$this->cityNameInURL;
        return redirect($url);

    }
    public function getCities($country)
    {
        $this->countrySelected=$country['name'];
        $this->getCitiesForCountry($country['country_id']);
        if( $this->locale=="en"){
            $this->countryNameInURL=$country['name'];
        }
        else{
            $this->countryNameInURL=$this->getCountryName($country['country_id'],"en");
        }
        session()->put('selectedCountryURL',$this->countryNameInURL);
        $this->dispatchBrowserEvent('country-updated',['country' => $this->countryNameInURL]);
//        $countryNameInURL=null;
//        dd($this->cities);
    }
    public function render()
    {
        return view('livewire.template.index.country-component');
    }
}
