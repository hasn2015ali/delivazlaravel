<?php

namespace App\Http\Livewire\Template\Index;

use Livewire\Component;

class Shape extends Component
{
    public function submit()
    {
//        dd('rr');
        $m=trans( 'home.countryValidate' );
        $this->emit('alert',['icon'=>'error','title'=>$m]);
    }
    public function render()
    {
        return view('livewire.template.index.shape');
    }
}
