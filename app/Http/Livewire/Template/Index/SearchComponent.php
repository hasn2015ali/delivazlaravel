<?php

namespace App\Http\Livewire\Template\Index;

use App\Models\Food;
use App\Models\FoodTranslation;
use App\Models\ProductTranslation;
use App\Models\ServiceProvider;
use Livewire\Component;

class SearchComponent extends Component
{
    public $showDropdown = false;
    public $query;
    public $restaurants = [];
    public $foods = [];
    public $shops = [];
    public $products = [];
    public $country;
    public $city;
    public $highlightIndex=0;
    public $highlightIndexForAllPage=true;
    public $allResultCount;
    public $x1;
    public $x2;
    public $x3;
    public $x4;
    public $r;
    public $f;
    public $s;
    public $p;
    public $allResult=[];
    public $length;




    public function updatingQuery($value)
    {
        //
//        dd($this->query,$value);
        $this->length=strlen($value);
        if($this->length<3)
        {
            $this->resetSearchResult();
            $this->showDropdown = true;

            return;
        }
        $this->resetSearchResult();
        $this->dispatchBrowserEvent('scroll-start');
        $value = preg_replace("/[^a-zA-Z0-9а-яА-Яا-ي' ']+/", "", $value);
        $value = trim($value);
//        $value = filter_var($value, FILTER_SANITIZE_STRING);
//        dd($value);
        if ($value == "") {
            $this->resetSearchResult();
            $this->closeDropDown();
            return;
        }
        $this->showDropdown = true;
        $this->getResult($value);


//        dd($this->allResultCount, $this->r,$this->f,$this->s,$this->p);
    }

    public function getResult($value)
    {
        $this->restaurants = ServiceProvider::with(['country:countries.id',
            'city'=> function ($query) {
                $query->select('cities.id', 'cities.state')
//                    ->where('state', 1)
                    ->get();
            },
            'city.translation' => function ($query) {
                $query->select('name', 'city_id')
                    ->where('code_lang', 'en')
                    ->get();
            }, 'country.translation' => function ($query) {
                $query->select('name', 'country_id')
                    ->where('code_lang', 'en')
                    ->get();
            }])
            ->whereHas('city', function ($query) {
                $query->where('cities.state', 1);
            })
            ->where('type', 'res')
            ->where('name', 'like', '%' . $value . '%')
            ->select(['id', 'slug', 'name', 'user_id'])
            ->get()
            ->toArray();
//        dd($this->restaurants);
        $this->shops = ServiceProvider::with(['country:countries.id',
            'city:cities.id,cities.state',
            'city.translation' => function ($query) {
                $query->select('name', 'city_id')
                    ->where('code_lang', 'en')
                    ->get();
            }, 'country.translation' => function ($query) {
                $query->select('name', 'country_id')
                    ->where('code_lang', 'en')
                    ->get();
            }])
            ->whereHas('city', function ($query) {
                $query->where('cities.state', 1);
            })
            ->where('type', 'ven')
            ->where('name', 'like', '%' . $value . '%')
            ->select(['id', 'slug', 'name', 'user_id'])
            ->get()
            ->toArray();
//        dd($this->shops);
        $this->foods = FoodTranslation::with(['food:food.id,food.service_provider_id',
            'food.serviceProvider:service_providers.id,service_providers.slug,service_providers.user_id',
            'food.serviceProvider.country:countries.id', 'food.serviceProvider.country.translation' => function ($query) {
                $query->select('name', 'country_id')
                    ->where('code_lang', 'en')
                    ->get();
            }, 'food.serviceProvider.city:cities.id,cities.state', 'food.serviceProvider.city.translation' => function ($query) {
                $query->select('name', 'city_id')
                    ->where('code_lang', 'en')
                    ->get();
            }])
            ->whereHas('food.serviceProvider.city', function ($query) {
                $query->where('cities.state', 1);
            })
            ->where('name', 'like', '%' . $value . '%')
            ->select(['food_id', 'name'])
            ->get()
            ->toArray();
//        dd($this->foods);
        $this->products = ProductTranslation::with(['product:products.id,products.service_provider_id',
            'product.serviceProvider:service_providers.id,service_providers.slug,service_providers.user_id',
            'product.serviceProvider.country:countries.id', 'product.serviceProvider.country.translation' => function ($query) {
                $query->select('name', 'country_id')
                    ->where('code_lang', 'en')
                    ->get();
            }, 'product.serviceProvider.city:cities.id,cities.state', 'product.serviceProvider.city.translation' => function ($query) {
                $query->select('name', 'city_id')
                    ->where('code_lang', 'en')
                    ->get();
            }])
            ->whereHas('product.serviceProvider.city', function ($query) {
                $query->where('cities.state', 1);
            })
            ->where('name', 'like', '%' . $value . '%')
            ->select(['product_id', 'name'])
            ->get()
            ->toArray();
        $this->allResult=array_merge($this->restaurants,$this->foods,$this->shops,$this->products);
        $this->x1=count($this->restaurants);
        $this->x2=$this->x1+count($this->foods);
        $this->x3=$this->x2+count($this->shops);
        $this->x4=$this->x3+count($this->products);
        $this->allResultCount = count($this->restaurants) + count($this->shops) + count($this->foods) + count($this->products);

        $this->x1!=0? $this->r=0:$this->r=null;
        $this->x2!=0? $this->f=$this->allResultCount-(count($this->foods)+count($this->shops)+count($this->products)):$this->f=null;
        $this->x3!=0? $this->s=$this->allResultCount-(count($this->shops)+count($this->products)):$this->s=null;
        $this->x4!=0? $this->p=$this->allResultCount-(count($this->products)):$this->p=null;

    }

    public function resetSearchResult()
    {
        $this->restaurants = [];
        $this->foods = [];
        $this->shops = [];
        $this->products = [];
        $this->query = "";
        $this->highlightIndex = 0;
        $this->allResultCount=0;
        $this->allResult=[];
    }
    public function closeDropDown()
    {
        $this->showDropdown = false;

    }
    public function closeAndResetSearchResult()
    {
        $this->resetSearchResult();
        $this->closeDropDown();
    }

    public function incrementHighlight()
    {

        $direction="down";
        if($this->highlightIndex === $this->allResultCount - 1)
        {
            $direction="top";
        }
        if ($this->highlightIndex === $this->allResultCount - 1) {
            $this->highlightIndex = 0;
        }
        else{
            $this->highlightIndex++;

        }
        $this->dispatchBrowserEvent('scroll-down', ['direction' => $direction,'count'=>$this->allResultCount]);

    }

    public function decrementHighlight()
    {
        $direction="top";
//        dd($this->highlightIndex,$this->allResultCount - 1);
        if($this->highlightIndex==0)
        {
            $direction="down";
        }
        if ($this->highlightIndex === 0) {
            $this->highlightIndex = $this->allResultCount - 1;
        }
        else{
            $this->highlightIndex--;
        }


        $this->dispatchBrowserEvent('scroll-up', ['direction' => $direction,'count'=>$this->allResultCount]);

    }

    public function selectResult()
    {
//        dd('dd');

//        $allResultRes=array_merge($this->restaurants,$this->foods);
//        $allResultShop=array_merge($this->shops,$this->products);
//        $allResult=array_merge($allResultRes,$allResultShop);
//        dd($this->highlightIndex , $this->$this->x1);
        if($this->highlightIndex < $this->x1){
//            dd('dd');
            $result = $this->allResult[$this->highlightIndex];
            $this->redirect(route('Restaurant',
                ['locale' => app()->getLocale(),
                    'country' => $result['country']['translation'][0]['name'],
                    'city' => $result['city']['translation'][0]['name'],
                    'serviceProvider' => $result['slug']]));
        }

        if($this->highlightIndex >= $this->x1 and $this->highlightIndex < $this->x2){

            $result = $this->allResult[$this->highlightIndex];
            $this->redirect(route('Food',['locale'=>app()->getLocale(),
                'country'=>$result['food']['service_provider']['country']['translation'][0]['name'],
                'city'=>$result['food']['service_provider']['city']['translation'][0]['name'],
                'serviceProvider'=>$result['food']['service_provider']['slug'],
                'foodID'=>$result['food_id']]));

        }


        if($this->highlightIndex >= $this->x2 and $this->highlightIndex < $this->x3){

            $result = $this->allResult[$this->highlightIndex];
            $this->redirect(route('Shop',
                ['locale' => app()->getLocale(),
                    'country' => $result['country']['translation'][0]['name'],
                    'city' => $result['city']['translation'][0]['name'],
                    'serviceProvider' => $result['slug']]));
//            dd($result);

        }


//        if (in_array($this->highlightIndex, $productKeies)) {

        if($this->highlightIndex >= $this->x3 and $this->highlightIndex < $this->x4){
            $result = $this->allResult[$this->highlightIndex];
            $this->redirect(route('Product',['locale'=>app()->getLocale(),
                'country'=>$result['product']['service_provider']['country']['translation'][0]['name'],
                'city'=>$result['product']['service_provider']['city']['translation'][0]['name'],
                'serviceProvider'=>$result['product']['service_provider']['slug'],
                'productID'=>$result['product_id']]));
        }

    }

    public function mount()
    {
        $this->resetSearchResult();
    }

    public function render()
    {
        return view('livewire.template.index.search-component');
    }
}
