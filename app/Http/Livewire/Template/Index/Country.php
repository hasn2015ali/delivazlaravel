<?php

namespace App\Http\Livewire\Template\Index;

use Livewire\Component;

class Country extends Component
{
    public $countries;
    public $cities ;
    public $countrySelected;
    public $city;
    public $country;
    public $checkedCity;

    public function mount()
    {
//        dd($this->checkedCity);
    }

    public function render()
    {
        return view('livewire.template.index.country');
    }
}
