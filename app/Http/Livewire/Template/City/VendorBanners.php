<?php

namespace App\Http\Livewire\Template\City;

use App\Models\cityRecommended;
use App\Models\CityTranslation;
use Livewire\Component;

class VendorBanners extends Component
{
    public $cityBanner;
    public $city;
    public $cityID;
    public $country;

    public function mount()
    {

        $this->cityID=CityTranslation::where('name',$this->city)
            ->where('code_lang','en')
            ->first()->city_id;
//        dd($this->cityID);
    }
    public function render()
    {
        $vendorBanners=cityRecommended::with('serviceProvider')
            ->where('city_id',$this->cityID)
            ->where('type','ven')
            ->orderBy('id','asc')->get();
        return view('livewire.template.city.vendor-banners',compact('vendorBanners'));
    }
}
