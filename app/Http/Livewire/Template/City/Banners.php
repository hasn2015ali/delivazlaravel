<?php

namespace App\Http\Livewire\Template\City;

use App\Models\cityBanner;
use App\Models\CityTranslation;
use Livewire\Component;

class Banners extends Component
{
    public $cityBanner;
    public $city;
    public $country;
    public $cityID;

    public function mount()
    {

        $this->cityID=CityTranslation::where('name',$this->city)
            ->where('code_lang','en')
            ->first()->city_id;
//        dd($this->cityID);
    }
    public function render()
    {
        $cityBanners=cityBanner::with('serviceProvider')->where('city_id',$this->cityID)
            ->where( 'state', 1 )
            ->get();
//        dd($cityBanner);
        return view('livewire.template.city.banners',compact('cityBanners'));
    }
}
