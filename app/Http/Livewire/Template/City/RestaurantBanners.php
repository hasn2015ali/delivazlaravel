<?php

namespace App\Http\Livewire\Template\City;

use App\Models\cityBanner;
use App\Models\cityRecommended;
use App\Models\CityTranslation;
use Livewire\Component;

class RestaurantBanners extends Component
{
    public $cityBanner;
    public $city;
    public $cityID;
    public $country;

    public function mount()
    {

        $this->cityID=CityTranslation::where('name',$this->city)
            ->where('code_lang','en')
            ->first()->city_id;
//        dd($this->cityID);
    }
    public function render()
    {
        $restaurantBanners=cityRecommended::with('serviceProvider')
            ->where('city_id',$this->cityID)
            ->where('type','res')
            ->orderBy('id','asc')->get();
        return view('livewire.template.city.restaurant-banners',compact('restaurantBanners'));
    }
}
