<?php

namespace App\Http\Livewire\Template\City;

use App\Models\CityShapcontent;
use App\Models\cityShape;
use App\Models\CityTranslation;
use Livewire\Component;

class Shape extends Component
{
    public $country;
    public $city;
    public $cityID;
    public $restaurantInShape;

    public function mount()
    {

        $this->cityID=CityTranslation::where('name',$this->city)
            ->where('code_lang','en')
            ->first()->city_id;
//        dd($this->cityID);
    }

    public function openShape($shapeID)
    {
        $this->dispatchBrowserEvent('open-shape');
        $this->restaurantInShape=CityShapcontent::with('serviceProvider:name,nameEN,id')->where('shape_id',$shapeID)
            ->where('type','restaurant')
            ->get();
//        $this->emit('open-shape');
//        dd($shapeID, $this->restaurantInShape);
    }
    public function render()
    {
        $cityShape=cityShape::with('translation','content')->where('city_id',$this->cityID)
//            ->orderBy('name','ASC')
//            ->orderByRaw('CONVERT(name, INTEGER) ASC')

            ->orderByRaw('CONVERT(name, SIGNED) ASC')
            ->get();
//        dd($cityShape);
        return view('livewire.template.city.shape',compact('cityShape'));
    }
}
