<?php

namespace App\Http\Livewire\Template\ControlPanel;

use Livewire\Component;

class ServiceProviderNotification extends Component
{
    public $count;


    public function mount()
    {
        $this->count=0;
    }
    public function render()
    {
        return view('livewire.template.control-panel.service-provider-notification');
    }
}
