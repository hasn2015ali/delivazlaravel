<?php

namespace App\Http\Livewire\Template\ControlPanel;

use Livewire\Component;

class Footer extends Component
{
    public function render()
    {
        return view('livewire.template.control-panel.footer');
    }
}
