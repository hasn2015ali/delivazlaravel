<?php

namespace App\Http\Livewire\Template\ControlPanel;

use App\Models\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NavBar extends Component
{
    public $country;
    public $city;
    public $user;
    public $active;
    public $serviceProviderPhoto;

    protected $listeners = [
        'personalInfoUpdated'
    ];

    public function personalInfoUpdated()
    {
        $this->user=User::find(Auth::id());
    }

    public function mount()
    {
        $this->user=auth()->user();
        $this->serviceProviderPhoto=ServiceProvider::select('avatar')->where('user_id',\auth()->id())->first();
//        dd($this->serviceProviderPhoto);
    }
    public function render()
    {
        return view('livewire.template.control-panel.nav-bar');
    }
}
