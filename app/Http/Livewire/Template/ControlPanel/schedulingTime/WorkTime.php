<?php

namespace App\Http\Livewire\Template\ControlPanel\schedulingTime;

use App\Models\ServiceProvider;
use App\Models\ServiceProviderPartTime;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class WorkTime extends Component
{
    public $hoursInDay;
    public $minutesInHour;
    public $days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    public $serviceProvider;
    public $serviceProviderState;
    public $startHour=[];
    public $endHour=[];
    public $startMinute=[];
    public $endMinute=[];
    public $workTimeDays=[1=>"off",2=>"off",3=>"off",4=>"off",5=>"off",6=>"off",7=>"off"];

//

    public function mount()
    {

        $this->hoursInDay= array_keys($this->getAllHoursInDay());
        $this->minutesInHour= array_keys($this->getAllMinutesInHour());
//        dd($this->hoursInDay, $this->minutesInHour,$this->days);

        $this->serviceProvider=ServiceProvider::where('user_id',Auth::id())->first();
        $this->serviceProvider->state==0?$this->serviceProviderState= "0":$this->serviceProviderState="1";
        $this->getOldValue();
//        dd($this->serviceProvider);

    }
    public function getOldValue()
    {
        foreach ($this->workTimeDays as $key=>$day)
        {
            $dayWork=ServiceProviderPartTime::where('service_provider_id',$this->serviceProvider->id)
                ->where('day_id',$key)->first();
            if(isset($dayWork))
            {
                $this->workTimeDays[$key]=$dayWork->day_state;
                $this->startHour[$key]=$dayWork->startTimeHour;
                $this->startMinute[$key]=$dayWork->startTimeMinutes;
                $this->endHour[$key]=$dayWork->endTimeHour;
                $this->endMinute[$key]=$dayWork->endTimeMinutes;
            }
            else
            {
                $this->workTimeDays[$key]="off";
                $this->startHour[$key]="off";
                $this->startMinute[$key]="off";
                $this->endHour[$key]="off";
                $this->endMinute[$key]="off";
            }
        }
    }
    public function setStartHour($day,$hour)
    {
        $this->startHour[$day]=$hour;
        $this->workTimeDays[$day]=="off"?$this->workTimeDays[$day]="on":"";

    }
    public function setStartMinute($day,$minute)
    {
        $this->startMinute[$day]=$minute;
        $this->workTimeDays[$day]=="off"?$this->workTimeDays[$day]="on":"";

    }
    public function setEndHour($day,$hour)
    {
        $this->endHour[$day]=$hour;
        $this->workTimeDays[$day]=="off"?$this->workTimeDays[$day]="on":"";

    }
    public function setEndMinute($day,$minute)
    {
        $this->endMinute[$day]=$minute;
        $this->workTimeDays[$day]=="off"?$this->workTimeDays[$day]="on":"";

    }
    public function setDayState($day)
    {
        $this->workTimeDays[$day]=="off"?$this->workTimeDays[$day]="on":$this->workTimeDays[$day]="off";
        if( $this->workTimeDays[$day]=="off")
        {
            $this->startHour[$day]="off";
            $this->endHour[$day]="off";
            $this->startMinute[$day]="off";
            $this->endMinute[$day]="off";

        }
    }
    public function validateDayOn($hS,$mS,$hE,$mE)
    {
        if($hS=="off")
        {
            $m=trans( 'RestaurantControlPanel.hSVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($mS=="off")
        {
            $m=trans( 'RestaurantControlPanel.mSVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($hE=="off")
        {
            $m=trans( 'RestaurantControlPanel.hEVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        if($mE=="off")
        {
            $m=trans( 'RestaurantControlPanel.mEVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }

    }
    public function insertData($day,$hS,$mS,$hE,$mE,$state)
    {
        ServiceProviderPartTime::create([
            'startTimeHour' => $hS,
            'startTimeMinutes' => $mS,
            'endTimeHour' => $hE,
            'endTimeMinutes' => $mE,
            'day_state' => $state,
            'day_id' => $day,
            'service_provider_id' => $this->serviceProvider->id,
        ]);
        $message=trans( 'RestaurantControlPanel.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }
    public function updateData($day,$hS,$mS,$hE,$mE,$state)
    {
        $up=ServiceProviderPartTime::where('service_provider_id',$this->serviceProvider->id)
            ->where('day_id',$day)->first();
        $up->startTimeHour=$hS;
        $up->startTimeMinutes=$mS;
        $up->endTimeHour=$hE;
        $up->endTimeMinutes=$mE;
        $up->day_state=$state;

        $up->save();
        $message=trans( 'RestaurantControlPanel.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }

    public function checkUpdateOrAdd($day)
    {
        $checkUpdateOrAdd=ServiceProviderPartTime::where('service_provider_id',$this->serviceProvider->id)
            ->where('day_id',$day)->first();
        if($checkUpdateOrAdd==null)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    public function saveDayWork($day)
    {
        if($this->workTimeDays[$day]=="on")
        {
            if($this->validateDayOn($this->startHour[$day],$this->startMinute[$day],$this->endHour[$day],$this->endMinute[$day])==1)
            {
                return;
            }
        }
        if($this->checkUpdateOrAdd($day)==0)
        {
            $this->insertData($day,$this->startHour[$day],$this->startMinute[$day],$this->endHour[$day],$this->endMinute[$day],$this->workTimeDays[$day]);
        }
        else
        {
            $this->updateData($day,$this->startHour[$day],$this->startMinute[$day],$this->endHour[$day],$this->endMinute[$day],$this->workTimeDays[$day]);
        }
//        dd($day);
    }
    public function changeServiceProviderState()
    {
//        dd($this->serviceProvider->state);
        if($this->serviceProvider->state==1)
        {
            $this->serviceProvider->state=0;
            $this->serviceProviderState= "0";
            $this->serviceProvider->save();
        }
        elseif($this->serviceProvider->state==0)
        {
            $this->serviceProvider->state=1;
            $this->serviceProviderState="1";
            $this->serviceProvider->save();
        }
    }



    public function getAllHoursInDay( $start = 0, $end = 86400, $step = 3600, $format = 'g:i a' ) {
        $times = array();
        foreach ( range( $start, $end, $step ) as $timestamp ) {
            $hour_mins = gmdate( 'H', $timestamp );
            if ( ! empty( $format ) )
                $times[$hour_mins] = gmdate( $format, $timestamp );
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function getAllMinutesInHour( $start = 0, $end = 3600, $step = 60, $format = 'g:i a' ) {
        $times = array();
        foreach ( range( $start, $end, $step ) as $timestamp ) {
            $hour_mins = gmdate( 'i', $timestamp );
            if ( ! empty( $format ) )
                $times[$hour_mins] = gmdate( $format, $timestamp );
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function render()
    {
//        dd($this->startHour,$this->startMinute,$this->endHour,$this->endMinute);
        return view('livewire.template.control-panel.scheduling-time.work-time');
    }
}
