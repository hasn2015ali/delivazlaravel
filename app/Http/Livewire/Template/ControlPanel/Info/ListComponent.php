<?php

namespace App\Http\Livewire\Template\ControlPanel\Info;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class ListComponent extends Component
{
    public $user;
    public $pageName;
    protected $listeners = [
        'personalInfoUpdated'
    ];

    public function personalInfoUpdated()
    {
        $this->user=User::find(Auth::id());
    }
//


    public function render()
    {
//        dd($this->user);
        return view('livewire.template.control-panel.info.list-component');
    }
}
