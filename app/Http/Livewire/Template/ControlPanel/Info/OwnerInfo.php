<?php

namespace App\Http\Livewire\Template\ControlPanel\Info;

use App\Mail\EmailVerificationCodeSend;
use App\Models\User;
use App\Notifications\ClickSendTest;
use App\Traits\WithUserValidationTrait;
//use Brick\PhoneNumber\PhoneNumberParseException;
//use Faker\Provider\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberFormat;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;
use Livewire\WithFileUploads;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;
class OwnerInfo extends Component
{

    use WithFileUploads,WithUserValidationTrait;
    public $user;
    public $photo;

//    public $oldPhoto="/storage/images/avatars/person240.png";
    public $oldPhoto="storage/images/avatars/1626353984.png";
    public $validPhone;

    public $firstName;
    public $fatherName;
    public $lastName;
    public $phone;
    public $email;
    public $oldPhone;
    public $oldEmail;
    public $address;
    public $codeNumber;
    protected $listeners = ['restOwner'=>'mount'];

    //notes for phone number type
    //FIXED_LINE = 0;
     //MOBILE = 1;
    //FIXED_LINE_OR_MOBILE = 2;
    //UNKNOWN = 10;

    public function hydrate()
    {
        $this->resetErrorBag();
    }
    public function messages()
    {

        return [
            'firstName.required' => trans( 'RestaurantControlPanel.validateFirstName' ),
            'lastName.required' => trans( 'RestaurantControlPanel.validateLastName' ),
            'email.required' => trans( 'RestaurantControlPanel.validateEmail' ),
            'phone.required' => trans( 'RestaurantControlPanel.validatePhoneRequired' ),
            'phone.numeric' => trans( 'RestaurantControlPanel.validatePhoneNumeric' ),
            'address.required' => trans( 'RestaurantControlPanel.validateAddress' ),

        ];
    }


    public function updatedPhone()
    {
        $result=$this->IsValidPhone($this->codeNumber,$this->phone);
        if($result==null)
        {
            $this->addError('phone', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
            $this->validPhone=null;
        }
        else
        {
            $this->validPhone=$result;
        }
    }
    public function updateOwnerDetails()
    {

        if(!filter_var($this->email,FILTER_VALIDATE_EMAIL))
        {
            $message=trans( 'userFront.ErrorEmail' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return;
        }
        $this->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
        ]);

        $user=User::find($this->user->id);

        $result=$this->IsValidPhone($this->codeNumber,$this->phone);
        if($result==null)
        {
            $this->addError('phone', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
            $this->validPhone=null;
            $message=trans( 'RestaurantControlPanel.ErrorPhoneValidation' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return;
        }
        else
        {
            $this->validPhone=$result;
        }
        if($this->oldEmail!=$this->email)
        {
            if($this->checkEmail($this->email)==1)
            {
                return;
            }
            $user->email_verified_at=null;
            $this->sendVerificationCodeToEmail();
        }
//        dd($this->oldPhone,$this->codeNumber.$this->phone);
        if($this->oldPhone!=$this->codeNumber.$this->phone)
        {
//            dd($this->oldPhone,$this->phone);
            if($this->checkPhone($this->codeNumber.$this->phone)==1)
            {
                return;
            }
            $user->phone_verified=null;
            $this->sendVerificationCodeToPhone($user);
        }
        $user->firstName=$this->firstName;
        $user->fatherName=$this->fatherName;
        $user->lastName=$this->lastName;
        $user->email=$this->email;
        $user->address=$this->address;
        $user->phone=$this->codeNumber.$this->phone;


        $user->save();
        $this->oldEmail=$this->email;
        $this->oldPhone=$this->codeNumber.$this->phone;

        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emitTo('template.control-panel.info.list-component','personalInfoUpdated');
        $this->emitTo('template.control-panel.nav-bar','personalInfoUpdated');

    }
    public function mount()
    {
        $this->firstName=$this->user->firstName;
        $this->fatherName=$this->user->fatherName;
        $this->lastName=$this->user->lastName;
        $this->email=$this->user->email;
//        dd($this->user->phone);
        $number=null;
        try {
            $number = PhoneNumber::parse("+".$this->user->phone);

        }
        catch (PhoneNumberParseException $e) {
            $this->addError('phone', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
//            dd($e->getMessage(),$number);
            // 'The string supplied is too short to be a phone number.'
        }
//        dd($number);
        if($number!=null)
        {
            $this->phone=$number->getNationalNumber();

        }
        else
        {
            $this->phone=$this->user->phone;
        }

        $this->oldEmail=$this->user->email;
        $this->oldPhone=$this->user->phone;
        $this->address=$this->user->address;
        $this->codeNumber=$this->user->country->code_number;


    }

    public function render()
    {
        return view('livewire.template.control-panel.info.owner-info');
    }
}
