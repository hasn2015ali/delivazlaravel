<?php

namespace App\Http\Livewire\Template\ControlPanel\Info;

use App\Models\ServiceProvider;
use App\Models\ServiceProviderDocument;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class DocumentComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $user;
    public $serviceProviderDocument;
    public $serviceProviderID;
    public $serviceProvider;
    public $document;
    public $documentID;
    public $openDeleteToast=false;
    public $openPreviewToast=false;
    public $documentNumber;
    public $docImage;
    public $imageFolders;
    public $documentInPreviewWindow;
    protected $listeners = ['restDocument'=>'mount'];

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }
    public function messages()
    {

        return [
            'serviceProviderDocument.required' => trans( 'restaurant.validateRestaurantBanner' ),
        ];
    }

    public function RemoveCurrentServiceProviderDocument()
    {
        $this->serviceProviderDocument=null;
    }
    public function submitDocument()
    {
        $this->validate([
            'serviceProviderDocument' => 'required',
        ]);
        if($this->serviceProviderDocument)
        {
            $imageName=time().rand(1,100).".".$this->serviceProviderDocument->extension();
            if($this->user->role_id==9)
            {
                $this->serviceProviderDocument->storeAs('public/images/document/restaurant',$imageName);

            }
            elseif ($this->user->role_id==10)
            {
                $this->serviceProviderDocument->storeAs('public/images/document/vendor',$imageName);

            }

            ServiceProviderDocument::create([
                'name' => $imageName,
                'state' => 0,
                'service_provider_id' => $this->serviceProviderID,
            ]);
        }

        $message=trans( 'RestaurantControlPanel.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->serviceProviderDocument=null;
    }
    public function deleteCall($id){
        $this->documentID=$id;
        $this->openToast();
//        dd($this->documentID);
    }
    public function closeToast()
    {
        $this->openDeleteToast=false;

    }
    public function openToast()
    {
        $this->openDeleteToast=true;

    }
    public function deleteAction()
    {
        $this->document=ServiceProviderDocument::find($this->documentID);
        $this->document->deleteByOwner=1;
        $this->document->save();
        $message=trans( 'RestaurantControlPanel.DeleteDocuments' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->document=null;
        $this->documentID=null;
        $this->closeToast();
    }
    public function CancelDelete($docID)
    {
        $this->document=ServiceProviderDocument::find($docID);
        $this->document->deleteByOwner=0;
        $this->document->save();
        $message=trans( 'RestaurantControlPanel.CancelDeleteDocuments' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->document=null;
    }
    public function closePreviewToast()
    {
        $this->openPreviewToast=false;
        $this->documentNumber=null;
        $this->docImage=null;
    }
    public function openPreviewToast($documentNumber,$documentPhoto)
    {
        $this->openPreviewToast=true;
        $this->documentNumber=$documentNumber;
        $this->docImage=$documentPhoto;
    }
    public function mount()
    {
//        dd($this->user->role_id);
        $this->documentInPreviewWindow= __("RestaurantControlPanel.Document");
        $this->serviceProvider=ServiceProvider::where('user_id',Auth::id())->first();
        $this->serviceProviderID=$this->serviceProvider->id;

        if($this->user->role_id==9)
        {
            $this->imageFolders='storage/images/document/restaurant/';

        }
        elseif ($this->user->role_id==10)
        {
            $this->imageFolders='storage/images/document/vendor/';

        }
    }
    public function render()
    {
        $documents=ServiceProviderDocument::where('service_provider_id',$this->serviceProviderID)
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.template.control-panel.info.document-component',compact('documents'));
    }
}
