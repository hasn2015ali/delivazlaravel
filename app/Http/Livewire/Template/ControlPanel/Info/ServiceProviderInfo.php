<?php

namespace App\Http\Livewire\Template\ControlPanel\Info;

use App\Models\Country;
use App\Models\ServiceProvider;
use App\Traits\WithRestaurantTrait;
use App\Traits\WithUserValidationTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class ServiceProviderInfo extends Component
{
    use WithFileUploads,WithRestaurantTrait,WithUserValidationTrait;
    public $user;
    public $photo;
    public $DefaultName;
    public $oldDefaultName;
    public $EnglishName;
//    public $oldPhoto="/storage/images/avatars/person240.png";
    public $oldPhoto="storage/images/avatars/1626353984.png";
    public $phone1;
    public $phone2;
    public $validPhone1;
    public $validPhone2;
    public $phone3;
    public $selectDeliveryMode;
    public $minimumOrderAmount;
    public $address;
    public $oldAvatar;
    public $codeNumber;
    public $serviceProvider;
    public $taxesInCountry;
    public $selectedTax=[];
    public $serviceProviderCountry;
    public $countryCurrency;
    protected $listeners = ['restInfo'=>'mount'];


    public function hydrate()
    {
        $this->resetErrorBag();
    }

    public function messages()
    {

        return [
            'DefaultName.required' => trans( 'RestaurantControlPanel.validateDefaultName' ),
            'phone1.required' => trans( 'RestaurantControlPanel.validatePhone1' ),
            'address.required' => trans( 'RestaurantControlPanel.validateAddress' ),
        ];
    }

    public function updatedPhone1()
    {
        $result=$this->IsValidPhone($this->codeNumber,$this->phone1);
        if($result==null)
        {
            $this->addError('phone1', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
            $this->validPhone1=null;
        }
        else
        {
            $this->validPhone1=$result;
        }
    }
    public function updatedPhone2()
    {
        $result=$this->IsValidPhone($this->codeNumber,$this->phone2);
        if($result==null)
        {
            $this->addError('phone2', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
            $this->validPhone2=null;
        }
        else
        {
            $this->validPhone2=$result;
        }
    }
    public function updateInfo()
    {
        $this->DefaultName= str_replace(['-','_','*'], ' ', $this->DefaultName);
        $this->DefaultName=trim($this->DefaultName);
        $slug= str_replace(' ', '-', $this->DefaultName);
        if( $this->oldDefaultName!= $this->DefaultName)
        {
            if($this->checkServiceProviderName($this->DefaultName)==1)
            {
                return;
            }
        }


        if($this->photo)
        {
            if($this->serviceProvider->avatar==null)
            {
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/avatars',$imageName);
            }
            else
            {
                Storage::delete('public/images/avatars/'.$this->serviceProvider->avatar);
                $imageName=time().".".$this->photo->extension();
                $this->photo->storeAs('public/images/avatars',$imageName);
            }

        }
        else
        {
            $imageName=$this->serviceProvider->avatar;
        }

        $this->validate([
            'DefaultName' => 'required',
            'phone1' => 'required',
            'address' => 'required',
        ]);


        $result=$this->IsValidPhone($this->codeNumber,$this->phone1);
        if($result==null)
        {
            $this->addError('phone1', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
            $this->validPhone1=null;
            $message=trans( 'RestaurantControlPanel.ErrorPhoneValidation' );
            $this->emit('alert',['icon'=>'error','title'=>$message]);
            return;
        }
        else
        {
            $this->validPhone1=$result;
        }

        if($this->phone2!=null)
        {
            $result2=$this->IsValidPhone($this->codeNumber,$this->phone2);
            if($result2==null)
            {
                $this->addError('phone2', trans( 'RestaurantControlPanel.ErrorPhoneValidation'));
                $this->validPhone2=null;
                $message=trans( 'RestaurantControlPanel.ErrorPhoneValidation' );
                $this->emit('alert',['icon'=>'error','title'=>$message]);
                return;
            }
            else
            {
                $this->validPhone2=$result2;
            }
        }




        $this->serviceProvider->name= $this->DefaultName;
        $this->serviceProvider->nameEN=$this->EnglishName;
        $this->serviceProvider->phone1=$this->phone1;
        $this->serviceProvider->phone2=$this->phone2;
        $this->serviceProvider->address=$this->address;
        $this->serviceProvider->avatar=$imageName;
        $this->serviceProvider->mode=$this->selectDeliveryMode;
        $this->serviceProvider->minimum_order_amount= $this->minimumOrderAmount;
        $this->serviceProvider->tax_id= $this->selectedTax['id'];

        $this->serviceProvider->save();
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }

    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectedTax['value']=$tax['value'];
        $this->selectedTax['id']=$tax['id'];
        $this->selectedTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }
    public function mount()
    {
//        dd($this->user->country_id);
//        $this->serviceProvider=$this->user->serviceProvider;
        $this->serviceProvider=ServiceProvider::with('tax')->where('user_id',Auth::id())->first();

        $this->DefaultName=$this->user->serviceProvider->name;
        $this->oldDefaultName=$this->user->serviceProvider->name;
        $this->EnglishName=$this->user->serviceProvider->nameEN;
        $this->address=$this->user->serviceProvider->address;
        $this->phone1=$this->user->serviceProvider->phone1;
        $this->phone2=$this->user->serviceProvider->phone2;
        $this->oldAvatar=$this->user->serviceProvider->avatar;
        $this->minimumOrderAmount=$this->user->serviceProvider->minimum_order_amount;
        $this->selectDeliveryMode=$this->user->serviceProvider->mode;
        $this->codeNumber=$this->user->country->code_number;

        $this->serviceProviderCountry=Country::with('taxs')
            ->find($this->user->country_id);
        $this->countryCurrency=$this->serviceProviderCountry->currency_code;
        $this->taxesInCountry=$this->serviceProviderCountry->taxs->toArray();
//        dd($this->taxesInCountry,$this->serviceProvider->tax_id,$this->selectedTax,count($this->selectedTax));
        if($this->serviceProvider->tax_id!=null)
        {
//            dd($this->serviceProvider);
            $this->selectedTax=$this->serviceProvider->tax->toArray();
//            dd($this->selectedTax);
        }
//        dd($this->taxesInCountry);

    }
    public function sendDeliveryModeValue($value)
    {
        $this->selectDeliveryMode=$value;
    }
    public function render()
    {
        return view('livewire.template.control-panel.info.service-provider-info');
    }
}
