<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\Food;
use App\Models\FoodAdditions;
use App\Models\FoodPhotos;
use App\Models\FoodSizes;
use App\Models\FoodTranslation;
use App\Models\Language;
use App\Models\MealFilterTrans;
use App\Models\ServiceProvider;
use App\Models\serviceProviderSizes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddNewFoodComponent extends Component
{
    use WithFileUploads;

    public $locale;
    public $foodPhoto;
    public $hoursInDay;
    public $minutesInHour;
    public $foodHour;
    public $foodMinute;
    public $sizes;
    public $allSizes;
    public $restaurant;
    public $selectedSize = [];
    public $selectedSizeName = [];
    public $addedSizes = [1 => 1];
    public $price = [];
    public $additionName = [];
    public $additionDetail = [];
    public $additionWeight = [];
    public $additionPrice = [];
    public $additionPhoto = [];
    public $additionPhotoStored = [];
    public $addedAddition = [];
    public $storedAddition = [];
    public $otherFoodPhoto1;
    public $otherFoodPhoto2;
    public $otherFoodPhoto3;
    public $allFoodFilters = [];
    public $allFoodFiltersSelected = [];
    public $foodName;
    public $foodDetails;
    public $foodWeight;
    public $openAdditionToast = false;
    public $additionCounter = 0;
    public $sizeCounter = 1;
    public $newSizeStored = [];
    public $sizeDetail = [];
    public $sizeWeight = [];
    public $addNewSize = false;
    public $currencyCode;
    public $isSize = false;
    public $serviceProviderTax=[];
    public $selectedFoodTax=[];
    public $countryCurrency;
    public $taxesInCountry;

    protected $listeners = [
        'removeSize' => '$refresh',
//    'addAnotherSize',
//        'cancelSize',
    ];

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }


    public function messages()
    {
        return [
            'selectedFoodTax.required' => trans('RestaurantControlPanel.validateSelectedFoodTax'),
            'foodPhoto.required' => trans('RestaurantControlPanel.validateFoodPhoto'),
            'price.required' => trans('RestaurantControlPanel.validateFoodPrice'),
            'additionName.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'additionName.*.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.*.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'foodName.required' => trans('RestaurantControlPanel.validateFoodName'),
            'allFoodFiltersSelected.required' => trans('RestaurantControlPanel.validateAllFoodFiltersSelected'),
            'price.*.required' => trans('RestaurantControlPanel.validateFoodPrice'),
        ];
    }


    public function addNewFood()
    {
//        dd($this->addedAddition,$this->additionPhoto,$this->sizeWeight,$this->addedSizes);
//        dd($this->price,$this->selectedSize,$this->allFoodFiltersSelected);



        //validate Default size if There is Any Addition Sizes
        if (count($this->price) > 1) {
            if (!isset($this->selectedSize[1])) {
                $message = trans('RestaurantControlPanel.validateDefaultSizeName');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        //validate Default Food Weight if There is Any Addition Sizes
        foreach ($this->sizeWeight as $weight) {
            if (isset($weight) and $weight != null and $weight != "") {
                if ($this->foodWeight == null and $this->foodWeight == "") {
                    $message = trans('RestaurantControlPanel.validateDefaultFoodWeight');
                    $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                    return;
                }
            }
        }

//        dd($this->foodWeight,$this->sizeWeight);
//        dd(count($this->sizeWeight),$this->sizeWeight);
//        if (count($this->sizeWeight)>0 ) {

//        }


        //validate size and price
        $this->validate([
            'foodPhoto' => 'required',
            'foodName' => 'required',
            'allFoodFiltersSelected' => 'array|required',
            "price" => "required|array|",
            'price.*' => 'required',
            'selectedFoodTax' => 'required',

        ]);
        // get Default Value
        isset($this->selectedSize[1]) ? $defaultSize = $this->selectedSizeName[1] : $defaultSize = null;
        $defaultPrice = $this->price[1];

        // get other Size
        foreach ($this->price as $key => $price) {
//            isset($this->selectedSize[$key])?$newSize=$this->selectedSize[$key]:$this->addError('selectedSize', trans( 'restaurant.validateSizeName' ).$key);

            if (!isset($this->selectedSize[$key]) and $key != 1) {
                $message = trans('RestaurantControlPanel.validateSizeName');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        if (count($this->price) != count($this->newSizeStored) + 1) {
            $message = trans('RestaurantControlPanel.validateLastSize');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

//        dd('test');
        // validate addition

        if (count($this->addedAddition) != 0) {
//                dd(count($this->addedAddition),$this->price,$this->selectedSize,$defaultSize,$defaultPrice);

            $this->validate([
//            'selectedSize' => 'required',
                "additionName" => "required|array|",
                'additionName.*' => 'required',
                "additionPrice" => "required|array|",
                'additionPrice.*' => 'required',
            ]);
        }

        if (count($this->addedAddition) != count($this->storedAddition)) {
            $message = trans('RestaurantControlPanel.validateLastAddition');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $en_lang = Language::where('code', 'en')->first();
        if ($this->foodPhoto) {
            $imageName = time() . rand(1000, 1000000) . "." . $this->foodPhoto->extension();
            $this->foodPhoto->storeAs('public/images/food', $imageName);
        } else {
            $imageName = null;
        }
        $this->foodWeight == "" || $this->foodWeight == null ? $this->foodWeight = "00" : $this->foodWeight = $this->foodWeight;
        $this->foodHour == "" || $this->foodHour == null ? $this->foodHour = "00" : $this->foodHour = $this->foodHour;
        $this->foodMinute == "" || $this->foodMinute == null ? $this->foodMinute = "00" : $this->foodMinute = $this->foodMinute;

        DB::beginTransaction();
        $food = Food::create([
            'photo' => $imageName,
            'price' => $defaultPrice,
            'size' => $defaultSize,
            'time_hour' => $this->foodHour,
            'weight' => $this->foodWeight,
            'time_minutes' => $this->foodMinute,
            'service_provider_id' => $this->restaurant->id,
            'tax_id' =>$this->selectedFoodTax['id'],
        ]);
        FoodTranslation::create([
            'name' => $this->foodName,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'food_id' => $food->id,
            'content' => $this->foodDetails,
            'service_provider_id' => $this->restaurant->id,
        ]);

        // insert Other Sizes
        if (count($this->addedSizes) > 1) {
            foreach ($this->addedSizes as $newSize) {

                if ($newSize != 1) {
                    $this->sizeWeight[$newSize] == "" || $this->sizeWeight[$newSize] == null ? $this->sizeWeight[$newSize] = 0 : $this->sizeWeight[$newSize] = $this->sizeWeight[$newSize];

                    FoodSizes::create([
                        'price' => $this->price[$newSize],
                        'size' => $this->selectedSizeName[$newSize],
                        'weight' => $this->sizeWeight[$newSize],
                        'content' => $this->sizeDetail[$newSize],
                        'food_id' => $food->id,
                    ]);
                }
            }
        }

        // insert Addition
        if (count($this->addedAddition) > 0) {
            foreach ($this->addedAddition as $newAddition) {
                if (isset($this->additionPhoto[$newAddition])) {
                    $additionImage = time() . rand(1000, 1000000) . "." . $this->additionPhoto[$newAddition]->extension();
                    $this->additionPhoto[$newAddition]->storeAs('public/images/food/addition', $additionImage);
                } else {
                    $additionImage = null;
                }
                $this->additionWeight[$newAddition] == "" || $this->additionWeight[$newAddition] == null ? $this->additionWeight[$newAddition] = 0 : $this->additionWeight[$newAddition] = $this->additionWeight[$newAddition];
                FoodAdditions::create([
                    'price' => $this->additionPrice[$newAddition],
                    'name' => $this->additionName[$newAddition],
                    'content' => $this->additionDetail[$newAddition],
                    'weight' => $this->additionWeight[$newAddition],
                    'photo' => $additionImage,
                    'food_id' => $food->id,
                ]);
                $additionImage = null;
            }
        }
        // insert other photo
        if (isset($this->otherFoodPhoto1)) {
            $otherImageName1 = time() . rand(1000, 1000000) . "." . $this->otherFoodPhoto1->extension();
            $this->otherFoodPhoto1->storeAs('public/images/food', $otherImageName1);
            FoodPhotos::create([
                'food_id' => $food->id,
                'name' => $otherImageName1,
            ]);
            $otherImageName1 = null;
        }
        if (isset($this->otherFoodPhoto2)) {
            $otherImageName2 = time() . rand(1000, 1000000) . "." . $this->otherFoodPhoto2->extension();
            $this->otherFoodPhoto2->storeAs('public/images/food', $otherImageName2);
            FoodPhotos::create([
                'food_id' => $food->id,
                'name' => $otherImageName2,
            ]);
            $otherImageName2 = null;
        }

        if (isset($this->otherFoodPhoto3)) {
            $otherImageName3 = time() . rand(1000, 1000000) . "." . $this->otherFoodPhoto3->extension();
            $this->otherFoodPhoto3->storeAs('public/images/food', $otherImageName3);
            FoodPhotos::create([
                'food_id' => $food->id,
                'name' => $otherImageName3,
            ]);
            $otherImageName3 = null;
        }
//        if (count($this->otherFoodPhotos) > 0) {
//            foreach ($this->otherFoodPhotos as $image) {
//                $otherImageName = time() . rand(1000, 1000000) . "." . $image->extension();
//                $image->storeAs('public/images/food', $otherImageName);
//                FoodPhotos::create([
//                    'food_id' => $food->id,
//                    'name' => $otherImageName,
//                ]);
//                $otherImageName = null;
//            }
//        }

        DB::commit();
        $food->filters()->attach($this->allFoodFiltersSelected);
        $imageName = null;
        $defaultPrice = null;
        $defaultSize = null;
        $this->restFilde();
        $message = trans('masterControl.successfully_Added');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);


//        }

//        dd(count($this->addedAddition), $this->price, $this->selectedSize, $defaultSize, $defaultPrice);
    }

    public function restFilde()
    {
        $this->foodHour = null;
        $this->foodWeight = null;
        $this->foodMinute = null;
        $this->foodDetails = null;
        $this->foodName = null;
        $this->foodPhoto = null;
        $this->price = [];
        $this->selectedSize = [];
        $this->selectedSizeName = [];
        $this->addedSizes = [1 => 1];
        $this->newSizeStored = [];
        $this->sizeWeight = [];
        $this->sizeDetail = [];
        $this->addedAddition = [];
        $this->storedAddition = [];
        $this->additionName = [];
        $this->additionDetail = [];
        $this->additionWeight = [];
        $this->additionPrice = [];
        $this->additionPhoto = [];
        $this->otherFoodPhoto1 = null;
        $this->otherFoodPhoto2 = null;
        $this->otherFoodPhoto3 = null;
        $this->sizes=$this->allSizes;

        $this->allFoodFiltersSelected = [];

    }

    public function removeOtherPhoto($key)
    {
        unset($this->otherFoodPhotos[$key]);
    }

    public function removeAllOtherPhoto()
    {
        $this->otherFoodPhotos = [];
    }
//    public function updatedAdditionPhoto()
//    {
//        $lastKey=array_key_last($this->additionPhoto);
//        $this->additionPhotoStored[$lastKey]=$this->additionPhoto[$lastKey];
//        $this->additionPhoto[$lastKey]=null;
////     dd($this->additionPhoto,$lastKey,$this->additionPhotoStored);
//    }
    public function addAddition()
    {
//        dd( $this->additionName);

        if (!isset($this->price[1])) {
            $message = trans('RestaurantControlPanel.validateFoodPrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

        $this->openAdditionToast = true;
        $this->additionCounter++;
        $this->addedAddition[$this->additionCounter] = $this->additionCounter;
        $this->additionName[$this->additionCounter] = null;
        $this->additionDetail[$this->additionCounter] = null;
        $this->additionWeight[$this->additionCounter] = null;
        $this->additionPrice[$this->additionCounter] = null;
//        dd($this->openAdditionToast);
//        $this->additionPhoto[$i]=null;
//        $this->additionPhotoStored[$i]=0;

//        dd($this->additionPhoto, $this->additionPhotoStored);
    }

    public function saveAddition()
    {
        $this->validate([
//            'selectedSize' => 'required',
            "additionName" => "required|array|",
            'additionName.*' => 'required',
            "additionPrice" => "required|array|",
            'additionPrice.*' => 'required',
            "additionPhoto" => "required|array|",
            'additionPhoto.*' => 'required',
        ]);
        $this->openAdditionToast = false;
        $this->storedAddition[$this->additionCounter] = $this->additionCounter;

//        dd($this->addedAddition,$this->additionName);
    }


    public function closeAdditionToast()
    {
        $this->openAdditionToast = false;
        $this->removeAddition($this->additionCounter);
    }

    public function removeAddition($i)
    {
        unset($this->additionName[$i]);
        unset($this->additionDetail[$i]);
        unset($this->additionPrice[$i]);
        unset($this->addedAddition[$i]);
        unset($this->additionWeight[$i]);
        if (isset($this->additionPhoto[$i])) {
            unset($this->additionPhoto[$i]);
        }
        if (isset($this->storedAddition[$i])) {
            unset($this->storedAddition[$i]);
        }
//        dd($this->addedAddition,$this->storedAddition);
//        unset($this->additionPhotoStored[$i]);

    }

    public function removeAllAddition()
    {
        $this->additionName = [];
        $this->additionDetail = [];
        $this->additionPrice = [];
        $this->additionPhoto = [];
        $this->addedAddition = [];
        $this->additionPhotoStored = [];
        $this->storedAddition = [];

    }

    public function openSizeToast()
    {
        $this->isSize = true;
    }

    public function closeSizeToast()
    {
        $this->isSize = false;
    }

    public function addAdditionalSize()
    {
//        dd($this->newSizeStored,$this->addedSizes);
        if (!isset($this->price[1])) {
            $message = trans('RestaurantControlPanel.validateFoodPrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if (!isset($this->selectedSize[1])) {
            $message = trans('RestaurantControlPanel.validateDefaultSizeName');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $this->sizeCounter++;
        $this->isSize = true;
//        $this->addNewSize=true;
        $this->addedSizes[$this->sizeCounter] = $this->sizeCounter;
        $this->selectedSize[$this->sizeCounter] = null;
        $this->selectedSizeName[$this->sizeCounter] = null;
        $this->price[$this->sizeCounter] = null;
        $this->sizeDetail[$this->sizeCounter] = null;
        $this->sizeWeight[$this->sizeCounter] = null;
//        $message = trans('RestaurantControlPanel.validateSizePrice');
//        $this->emit('sizeAdded');
//        $this->openSizeToast();
//        dd('ff',$this->addNewSize);
    }

    public function saveSize($i)
    {

//        dd($this->sizeWeight[$i],$this->foodWeight);
        if (!isset($this->selectedSize[$i]) and $i != 1) {
            $message = trans('RestaurantControlPanel.validateSizeName');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

        if (isset($this->sizeWeight[$i]) and $i != 1 and $this->sizeWeight[$i] != "" and $this->sizeWeight[$i] != null) {
            if (!isset($this->foodWeight)) {
                $message = trans('RestaurantControlPanel.validateDefaultFoodWeight');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        if (!isset($this->price[$i]) and $i != 1) {
            $message = trans('RestaurantControlPanel.validateSizePrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $this->newSizeStored[$i] = $i;
//        $this->addNewSize=false;
        $this->isSize = false;

//        $this->closeSizeToast();
    }

    public function cancelSize()
    {
        $this->removeSize($this->sizeCounter);
//        $this->addNewSize=false;
        $this->closeSizeToast();
    }

    public function removeSize($sizeNumber)
    {
//        dd($sizeNumber, $this->selectedSize,$this->price);

//        dd($sizeNumber,$this->selectedSize[$sizeNumber]);

//        $sizeNumber++;
        foreach ($this->allSizes as $key => $size) {
            if ($this->selectedSize[$sizeNumber] == $size['id']) {
                $this->sizes[$key] = $size;
//                break;
            }
        }
        unset($this->selectedSize[$sizeNumber]);
        unset($this->selectedSizeName[$sizeNumber]);
        if ($sizeNumber != 1) {
            unset($this->price[$sizeNumber]);
            unset($this->addedSizes[$sizeNumber]);
            unset($this->sizeWeight[$sizeNumber]);
            unset($this->sizeDetail[$sizeNumber]);
        }

        if (isset($this->newSizeStored[$sizeNumber])) {
            unset($this->newSizeStored[$sizeNumber]);
        }


//        $this->addNewSize=null;

//        dd($this->addedSizes,$this->newSizeStored,$this->price,$sizeNumber);
    }

    public function setSelectedSize($size, $i)
    {
//        dd($size);

        $this->selectedSize[$i] = $size['id'];
        $this->selectedSizeName[$i] = $size['name'];

        foreach ($this->sizes as $key => $size) {
            if ($size['id'] == $this->selectedSize[$i]) {
                unset($this->sizes[$key]);
            }
        }

//        dd($this->selectedSize,$this->sizes,$this->allSizes);
    }

    public function setFoodHour($foodHour)
    {
        $this->foodHour = $foodHour;
    }

    public function setFoodMinute($foodMinute)
    {
        $this->foodMinute = $foodMinute;
    }
    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectedFoodTax['value']=$tax['value'];
        $this->selectedFoodTax['id']=$tax['id'];
        $this->selectedFoodTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }
    public function mount()
    {
//        dd($this->selectedSize);
        $this->locale = App::getLocale();


        $this->restaurant = ServiceProvider::with(['filters', 'size' => function ($query) {
            $query->select('id', 'name', 'service_provider_id')
                ->get();
        }])->where('user_id', Auth::id())->first();
//        dd(Auth::user()->country_id);
        $this->sizes = $this->restaurant->size->toArray();
        $this->allSizes = $this->sizes;
        if($this->restaurant->tax_id!=null)
        {
//            dd($this->serviceProvider);
            $this->serviceProviderTax=$this->restaurant->tax->toArray();
            $this->selectedFoodTax=$this->serviceProviderTax;
//            dd($this->selectedTax);
        }

        $countryDetails = Country::with('taxs')
            ->findOrFail(Auth::user()->country_id);
//        $this->countryCurrency=$countryDetails->currency_code;
        $this->taxesInCountry=$countryDetails->taxs->toArray();
//        dd($countryDetails);
        if (isset($countryDetails->currency_code)) {
            $this->currencyCode = $countryDetails->currency_code;

        }
//        dd($this->restaurant->filters );
        foreach ($this->restaurant->filters as $restaurantFilter) {
            $mailFilterForThisRestaurantFilter = MealFilterTrans::select(['id', 'name', 'code_lang', 'filter_id', 'meal_filter_id'])
                ->where('code_lang', $this->locale)
                ->where('filter_id', $restaurantFilter->id)->get()->toArray();
//            dd($mailFilterForThisRestaurantFilter);
            array_push($this->allFoodFilters, $mailFilterForThisRestaurantFilter);

        }
//        dd($this->allFoodFilters);
//        dd($this->restaurant,$this->sizes);
        $this->hoursInDay = array_keys($this->getAllHoursInDay());
        $this->minutesInHour = array_keys($this->getAllMinutesInHour());
    }

    public function getAllHoursInDay($start = 0, $end = 86400, $step = 3600, $format = 'g:i a')
    {
        $times = array();
        foreach (range($start, $end, $step) as $timestamp) {
            $hour_mins = gmdate('H', $timestamp);
            if (!empty($format))
                $times[$hour_mins] = gmdate($format, $timestamp);
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function getAllMinutesInHour($start = 0, $end = 3600, $step = 60, $format = 'g:i a')
    {
        $times = array();
        foreach (range($start, $end, $step) as $timestamp) {
            $hour_mins = gmdate('i', $timestamp);
            if (!empty($format))
                $times[$hour_mins] = gmdate($format, $timestamp);
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function render()
    {
        return view('livewire.template.control-panel.food.add-new-food-component');
    }
}
