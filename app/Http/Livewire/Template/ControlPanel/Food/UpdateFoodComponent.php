<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\Food;
use App\Models\FoodAdditions;
use App\Models\FoodPhotos;
use App\Models\FoodSizes;
use App\Models\FoodTranslation;
use App\Models\Language;
use App\Models\MealFilterTrans;
use App\Models\ServiceProvider;
use App\Traits\WithFoodToasts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdateFoodComponent extends Component
{
    use WithFileUploads, WithFoodToasts;

    public $foodID;
    public $locale;
    public $food;
    public $foodPhoto;
    public $oldFoodPhoto;
    public $currencyCode;
    public $foodName;
    public $foodDetails;
    public $foodWeight;
    public $sizes = [];
    public $allSizes = [];
    public $restaurant;
    public $hoursInDay;
    public $minutesInHour;
    public $allFoodFilters = [];
    public $foodHour;
    public $foodMinute;
    public $defaultSelectedSize;
    public $defaultSelectedSizeName;
    public $defaultPrice;
    public $sizeCounter;
    public $newSizeStored;
    public $openDeleteToast = false;
    public $editBasicInfo = false;
    public $openNewPhoto = false;
    public $openNewSize = false;
    public $openNewAddition = false;
    public $photoID;
    public $deleteType;
    public $newAdditionalPhoto;
    public $newSizeName;
    public $newSizeId;
    public $newSizePrice;
    public $newSizeWeight;
    public $newSizeContent;
    public $newSizeUpdate=null;
    public $additionName;
    public $additionDetail;
    public $additionWeight;
    public $additionPrice;
    public $additionPhoto;
    public $additionPhotoOld;
    public $updatedAddition=null;
    public $additionID;
    public $openEditFilters=false;
    public $allFoodFiltersSelected=[];
    public $foodAdditionPhotoPath='/storage/images/food/addition/';
    public $serviceProviderTax=[];
    public $selectedFoodTax=[];
    public $countryCurrency;
    public $taxesInCountry;

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }


    public function messages()
    {
        return [
            'selectedFoodTax.required' => trans('RestaurantControlPanel.validateSelectedFoodTax'),

            'additionPhoto.required' => trans('RestaurantControlPanel.validateAdditionPhoto'),
            'foodPhoto.required' => trans('RestaurantControlPanel.validateFoodPhoto'),
            'price.required' => trans('RestaurantControlPanel.validateFoodPrice'),
            'newSizePrice.required' => trans('RestaurantControlPanel.validateFoodPrice'),
            'newSizeName.required' => trans('RestaurantControlPanel.validateNewSizeName'),
            'additionName.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'additionName.*.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.*.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'foodName.required' => trans('RestaurantControlPanel.validateFoodName'),
            'allFoodFiltersSelected.required' => trans('RestaurantControlPanel.validateAllFoodFiltersSelected'),
            'price.*.required' => trans('RestaurantControlPanel.validateFoodPrice'),
        ];
    }


    public function editFilters()
    {
        $this->openEditFilters=true;
        //set Old Meal Filters
        $this->getOldFilters();

    }
    public function getOldFilters()
    {
        $this->allFoodFiltersSelected=[];
        foreach ($this->food->filters->toArray() as $filters)
        {
            $filterID= (string) $filters['id'];
            array_push($this->allFoodFiltersSelected,$filterID);
        }
    }
    public function saveNewFilters()
    {
//        dd($this->allFoodFiltersSelected);
        $this->validate([
            'allFoodFiltersSelected' => 'array|required',
        ]);
        $this->food->filters()->detach();
        $this->food->filters()->attach($this->allFoodFiltersSelected);
        $this->allFoodFilters=[];
       $this->mount();
        $message=trans( 'RestaurantControlPanel.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->cancelNewFilters();
    }
    public function cancelNewFilters()
    {
        $this->openEditFilters=false;
        $this->getOldFilters();

    }
    public function saveNewAdditionalPhoto()
    {
        $this->validate([
            'newAdditionalPhoto' => 'required',
        ]);
        $otherImageName3 = time() . rand(1000, 1000000) . "." . $this->newAdditionalPhoto->extension();
        $this->newAdditionalPhoto->storeAs('public/images/food', $otherImageName3);
        FoodPhotos::create([
            'food_id' => $this->food->id,
            'name' => $otherImageName3,
        ]);
        $otherImageName3 = null;
        $this->closeAdditionPhotoToast();
        $this->mount();
        $message = trans('RestaurantControlPanel.successfully_Added');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
    }

    public function saveBasicInfo()
    {
        //validate size and price
        $this->validate([
            'foodName' => 'required',
            "defaultPrice" => "required",
            'selectedFoodTax' => 'required',
        ]);


        if ($this->foodPhoto) {
            $imageName = time() . rand(1000, 1000000) . "." . $this->foodPhoto->extension();
            $this->foodPhoto->storeAs('public/images/food', $imageName);
            Storage::delete('public/images/food/' . $this->oldFoodPhoto);
        } else {
            $imageName = $this->oldFoodPhoto;
        }
        $this->foodWeight == "" || $this->foodWeight == null ? $this->foodWeight = "00" : $this->foodWeight = $this->foodWeight;
        $this->foodHour == "" || $this->foodHour == null ? $this->foodHour = "00" : $this->foodHour = $this->foodHour;
        $this->foodMinute == "" || $this->foodMinute == null ? $this->foodMinute = "00" : $this->foodMinute = $this->foodMinute;

        DB::beginTransaction();
        $this->food->update([
            'photo' => $imageName,
            'price' => $this->defaultPrice,
            'size' => $this->defaultSelectedSizeName,
            'time_hour' => $this->foodHour,
            'weight' => $this->foodWeight,
            'time_minutes' => $this->foodMinute,
            'tax_id' =>$this->selectedFoodTax['id'],
        ]);
        $this->food->translation[0]->update([
            'name' => $this->foodName,
            'content' => $this->foodDetails,
        ]);
        DB::commit();
        $message = trans('RestaurantControlPanel.Updated_Successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->cancelBasicInfo();
    }

    public function removeAdditionalPhoto($photoID)
    {
        $this->photoID = $photoID;
        $this->openDeleteToast();
        $this->deleteType = "AdditionalPhoto";
    }
    public function callDeleteAddition($additionID)
    {
        $this->additionID=$additionID;
        $this->openDeleteToast();
        $this->deleteType = "Addition";
    }

    public function deleteAction()
    {
        if ($this->deleteType == "AdditionalPhoto") {
            $photo = FoodPhotos::find($this->photoID);
            Storage::delete('public/images/food/' . $photo->name);
            $photo->delete();
            $photo = null;
        }
        if ($this->deleteType == "size") {
            $foodSize = FoodSizes::find($this->newSizeId);
            foreach ($this->allSizes as $key => $size) {
                if ($foodSize->size == $size['name']) {
                    $this->sizes[$key] = $size;
                }
            }
//            dd($this->sizes);
            $foodSize->delete();
            $foodSize = null;
        }
        if ($this->deleteType == "Addition") {
            $addition = FoodAdditions::find($this->additionID);
            Storage::delete('public/images/food/addition' . $addition->photo);
            $addition->delete();
            $addition = null;
        }
        $message = trans('RestaurantControlPanel.Deleted_successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->closeToast();
    }
    public function saveAddition()
    {
        $this->validate([
            "additionName" => "required",
            'additionPrice' => 'required',
        ]);

        if (isset($this->additionPhoto)) {
            $additionImage = time() . rand(1000, 1000000) . "." . $this->additionPhoto->extension();
            $this->additionPhoto->storeAs('public/images/food/addition', $additionImage);
        } elseif(isset($this->additionPhotoOld)){
            $additionImage=$this->additionPhotoOld;
        }else {
            $additionImage = null;
        }
        $this->additionWeight == "" || $this->additionWeight == null ? $this->additionWeight = 0 : $this->additionWeight = $this->additionWeight;

        if($this->updatedAddition==null)
        {
            $this->validate([
                'additionPhoto' => 'required',
            ]);
            FoodAdditions::create([
                'price' => $this->additionPrice,
                'name' => $this->additionName,
                'content' => $this->additionDetail,
                'weight' => $this->additionWeight,
                'photo' => $additionImage,
                'food_id' => $this->food->id,
            ]);
            $message = trans('RestaurantControlPanel.successfully_Added');

        }
        else
        {
            $this->updatedAddition->update([
                'price' => $this->additionPrice,
                'name' => $this->additionName,
                'content' => $this->additionDetail,
                'weight' => $this->additionWeight,
                'photo' => $additionImage,
            ]);
            $message = trans('RestaurantControlPanel.Updated_Successfully');

        }

        $additionImage = null;
        $this->resetNewAdditionValue();

        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->closeAdditionToast();
    }
    public function updateAddition($additionID)
    {
        $this->updatedAddition=FoodAdditions::find($additionID);
        $this->additionName=$this->updatedAddition->name;
        $this->additionDetail=$this->updatedAddition->content;
        $this->additionWeight=$this->updatedAddition->weight;
        $this->additionPrice=$this->updatedAddition->price;
        $this->additionPhotoOld=$this->updatedAddition->photo;
        $this->openAdditionToast();


    }
    public function resetNewAdditionValue()
    {
        $this->additionName=null;
        $this->additionPhoto=null;
        $this->additionWeight=null;
        $this->additionPrice=null;
        $this->additionDetail=null;
        $this->additionPhotoOld=null;
    }
    public function updateSize($sizeID)
    {
        $this->newSizeId=$sizeID;
        $this->newSizeUpdate=FoodSizes::find($sizeID);
        $this->newSizeName=$this->newSizeUpdate->size;
        $this->newSizePrice=$this->newSizeUpdate->price;
        $this->newSizeContent=$this->newSizeUpdate->content;
        $this->newSizeWeight=$this->newSizeUpdate->weight;
        $this->openNewSize();
    }
    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectedFoodTax['value']=$tax['value'];
        $this->selectedFoodTax['id']=$tax['id'];
        $this->selectedFoodTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }
    public function mount()
    {
        $this->locale = App::getLocale();
        $this->restaurant = ServiceProvider::with(['filters', 'size' => function ($query) {
            $query->select('id', 'name', 'service_provider_id')
                ->get();
        }])->where('user_id', Auth::id())->first();
        $this->food = Food::with(['translation' => function ($query) {
            $query->select('name', 'content', 'code_lang', 'food_id')
                ->where('code_lang', App::getlocale())
                ->get();
        }, 'sizes', 'additions', 'filters', 'photos'])->find($this->foodID);
        $this->sizes = $this->restaurant->size->toArray();
        $this->allSizes = $this->sizes;

        if($this->food->tax_id!=null)
        {
//            dd($this->serviceProvider);
//            $this->serviceProviderTax=$this->restaurant->tax->toArray();
            $this->selectedFoodTax=$this->food->tax->toArray();
//            dd($this->selectedTax);
        }
//        dd($this->food->filters->toArray(),$this->allFoodFiltersSelected);


//        dd($this->food->sizes->toArray(),$this->sizes);
        $countryDetails = Country::with('taxs')->findOrFail(Auth::user()->country_id);
        $this->taxesInCountry=$countryDetails->taxs->toArray();
//        dd($countryDetails);
        if (isset($countryDetails->currency_code)) {
            $this->currencyCode = $countryDetails->currency_code;

        }
        foreach ($this->restaurant->filters as $restaurantFilter) {
            $mailFilterForThisRestaurantFilter = MealFilterTrans::select(['id', 'name', 'code_lang', 'filter_id', 'meal_filter_id'])
                ->where('code_lang', $this->locale)
                ->where('filter_id', $restaurantFilter->id)->get()->toArray();
//            dd($mailFilterForThisRestaurantFilter);
            array_push($this->allFoodFilters, $mailFilterForThisRestaurantFilter);

        }
//        dd($this->allFoodFilters);
//        dd($this->restaurant,$this->sizes);
        $this->hoursInDay = array_keys($this->getAllHoursInDay());
        $this->minutesInHour = array_keys($this->getAllMinutesInHour());
        $this->oldFoodPhoto = $this->food->photo;
        $this->foodName = $this->food->translation[0]['name'];
        $this->foodDetails = $this->food->translation[0]['content'];
        $this->foodWeight = $this->food->weight;
        $this->defaultPrice = $this->food->price;
        if (isset($this->food->size) and $this->food->size != null) {
            foreach ($this->allSizes as $size) {
                if ($size['name'] == $this->food->size) {
                    $this->defaultSelectedSize = $size['id'];
                }
            }
            $this->defaultSelectedSizeName = $this->food->size;
        }

        // set avalaibleizes
        if (count($this->food->sizes->toArray()) == 0) {
            foreach ($this->sizes as $key => $size) {
                if ($size['name'] == $this->defaultSelectedSizeName) {
                    unset($this->sizes[$key]);
                }
            }

        } else {
            foreach ($this->food->sizes->toArray() as $storedSize) {
                foreach ($this->sizes as $key => $size) {
                    if ($size['name'] == $storedSize['size'] or $size['name'] == $this->defaultSelectedSizeName) {
                        unset($this->sizes[$key]);
                    }
                }
            }

        }

    }

    public function setSelectedSize($size, $i)
    {
//        dd($size,$i);

        if ($i == 2) {
            $this->newSizeId = $size['id'];
            $this->newSizeName = $size['name'];
            foreach ($this->sizes as $key => $size) {
                if ($size['id'] == $this->newSizeId) {
                    unset($this->sizes[$key]);
                }
            }
        } else {
            $this->defaultSelectedSize = $size['id'];
            $this->defaultSelectedSizeName = $size['name'];
            foreach ($this->sizes as $key => $size) {
                if ($size['id'] == $this->defaultSelectedSize) {
                    unset($this->sizes[$key]);
                }
            }
        }
    }

    public function saveSize()
    {
        $this->validate([
            'newSizeName' => 'required',
            "newSizePrice" => "required",
        ]);
        $this->newSizeWeight == null || $this->newSizeWeight == "" ? $this->newSizeWeight = 0 : $this->newSizeWeight = $this->newSizeWeight;
//     dd($this->newSizeWeight);
        if($this->newSizeUpdate==null)
        {
            FoodSizes::create([
                'price' => $this->newSizePrice,
                'size' => $this->newSizeName,
                'weight' => $this->newSizeWeight,
                'content' => $this->newSizeContent,
                'food_id' => $this->food->id,
            ]);
            $message = trans('RestaurantControlPanel.successfully_Added');

        }
        else
        {
            $this->newSizeUpdate->update([
                'price' => $this->newSizePrice,
                'size' => $this->newSizeName,
                'weight' => $this->newSizeWeight,
                'content' => $this->newSizeContent,
                ]);
            $this->newSizeUpdate=null;
            $message = trans('RestaurantControlPanel.Updated_Successfully');

        }

        $this->resetNewSizeValue();
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->cancelNewSize();

    }

    public function callDeleteSize($sizeID)
    {
        $this->newSizeId = $sizeID;
        $this->deleteType = "size";
        $this->openDeleteToast();
    }

    public function resetNewSizeValue()
    {
        $this->newSizeName = null;
        $this->newSizeId = null;
        $this->newSizeContent = null;
        $this->newSizePrice = null;
        $this->newSizeWeight = null;
    }

    public function cancelSize()
    {
        $this->resetNewSizeValue();
        $this->cancelNewSize();
    }

    public function removeSize()
    {

        foreach ($this->allSizes as $key => $size) {
            if ($this->defaultSelectedSize == $size['id']) {
                $this->sizes[$key] = $size;
            }
        }

        $this->defaultSelectedSizeName = null;
        $this->defaultSelectedSize = null;

    }

    public function setFoodHour($foodHour)
    {
        $this->foodHour = $foodHour;
    }

    public function setFoodMinute($foodMinute)
    {
        $this->foodMinute = $foodMinute;
    }

    public function getAllHoursInDay($start = 0, $end = 86400, $step = 3600, $format = 'g:i a')
    {
        $times = array();
        foreach (range($start, $end, $step) as $timestamp) {
            $hour_mins = gmdate('H', $timestamp);
            if (!empty($format))
                $times[$hour_mins] = gmdate($format, $timestamp);
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function getAllMinutesInHour($start = 0, $end = 3600, $step = 60, $format = 'g:i a')
    {
        $times = array();
        foreach (range($start, $end, $step) as $timestamp) {
            $hour_mins = gmdate('i', $timestamp);
            if (!empty($format))
                $times[$hour_mins] = gmdate($format, $timestamp);
            else $times[$hour_mins] = $hour_mins;
        }
        return $times;
    }

    public function render()
    {
        return view('livewire.template.control-panel.food.update-food-component');
    }
}
