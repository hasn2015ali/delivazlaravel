<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\Food;
use App\Models\FoodAdditions;
use App\Models\Product;
use App\Models\ProductAdditions;
use App\Models\ProductPhotos;
use App\Models\FoodSizes;
use App\Models\FoodTranslation;
use App\Models\Language;
use App\Models\MealFilterTrans;
use App\Models\ProductFilterTrans;
use App\Models\ProductSizes;
use App\Models\ProductTranslation;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class AddNewProductComponent extends Component
{
    use WithFileUploads;

    public $locale;
    public $productPhoto;
    public $sizes;
    public $allSizes;
    public $shop;
    public $selectedSize = [];
    public $selectedSizeName = [];
    public $addedSizes = [1 => 1];
    public $price = [];
    public $additionName = [];
    public $additionDetail = [];
    public $additionWeight = [];
    public $additionPrice = [];
    public $additionPhoto = [];
    public $additionPhotoStored = [];
    public $addedAddition = [];
    public $storedAddition = [];
    public $otherProductPhoto1;
    public $otherProductPhoto2;
    public $otherProductPhoto3;
    public $allProductFilters = [];
    public $allProductFiltersSelected = [];
    public $productName;
    public $productDetails;
    public $productWeight;
    public $openAdditionToast = false;
    public $additionCounter = 0;
    public $sizeCounter = 1;
    public $newSizeStored = [];
    public $sizeDetail = [];
    public $sizeWeight = [];
    public $addNewSize = false;
    public $currencyCode;
    public $isSize = false;
    public $serviceProviderTax=[];
    public $selectedProductTax=[];
    public $countryCurrency;
    public $taxesInCountry;

    protected $listeners = [
        'removeSize' => '$refresh',
//    'addAnotherSize',
//        'cancelSize',
    ];

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }


    public function messages()
    {
        return [
            'selectedProductTax.required' => trans('RestaurantControlPanel.validateSelectedProductTax'),
            'productPhoto.required' => trans('RestaurantControlPanel.validateProductPhoto'),
            'price.required' => trans('RestaurantControlPanel.validateProductPrice'),
            'additionName.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'additionName.*.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.*.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'productName.required' => trans('RestaurantControlPanel.validateProductName'),
            'allProductFiltersSelected.required' => trans('RestaurantControlPanel.validateAllProductFiltersSelected'),
            'price.*.required' => trans('RestaurantControlPanel.validateProductPrice'),
        ];
    }


    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectedProductTax['value']=$tax['value'];
        $this->selectedProductTax['id']=$tax['id'];
        $this->selectedProductTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }

    public function addNewFood()
    {
//        dd($this->addedAddition,$this->additionPhoto,$this->sizeWeight,$this->addedSizes);
//        dd($this->price,$this->selectedSize,$this->allProductFiltersSelected);



        //validate Default size if There is Any Addition Sizes
        if (count($this->price) > 1) {
            if (!isset($this->selectedSize[1])) {
                $message = trans('RestaurantControlPanel.validateDefaultSizeName');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        //validate Default Food Weight if There is Any Addition Sizes
        foreach ($this->sizeWeight as $weight) {
            if (isset($weight) and $weight != null and $weight != "") {
                if ($this->productWeight == null and $this->productWeight == "") {
                    $message = trans('RestaurantControlPanel.validateDefaultFoodWeight');
                    $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                    return;
                }
            }
        }

//        dd($this->productWeight,$this->sizeWeight);
//        dd(count($this->sizeWeight),$this->sizeWeight);
//        if (count($this->sizeWeight)>0 ) {

//        }


        //validate size and price
        $this->validate([
            'productPhoto' => 'required',
            'productName' => 'required',
            'allProductFiltersSelected' => 'array|required',
            "price" => "required|array|",
            'price.*' => 'required',
            'selectedProductTax' => 'required',

        ]);
        // get Default Value
        isset($this->selectedSize[1]) ? $defaultSize = $this->selectedSizeName[1] : $defaultSize = null;
        $defaultPrice = $this->price[1];

        // get other Size
        foreach ($this->price as $key => $price) {
//            isset($this->selectedSize[$key])?$newSize=$this->selectedSize[$key]:$this->addError('selectedSize', trans( 'restaurant.validateSizeName' ).$key);

            if (!isset($this->selectedSize[$key]) and $key != 1) {
                $message = trans('RestaurantControlPanel.validateSizeName');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        if (count($this->price) != count($this->newSizeStored) + 1) {
            $message = trans('RestaurantControlPanel.validateLastSize');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

//        dd('test');
        // validate addition

        if (count($this->addedAddition) != 0) {
//                dd(count($this->addedAddition),$this->price,$this->selectedSize,$defaultSize,$defaultPrice);

            $this->validate([
//            'selectedSize' => 'required',
                "additionName" => "required|array|",
                'additionName.*' => 'required',
                "additionPrice" => "required|array|",
                'additionPrice.*' => 'required',
            ]);
        }

        if (count($this->addedAddition) != count($this->storedAddition)) {
            $message = trans('RestaurantControlPanel.validateLastAddition');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $en_lang = Language::where('code', 'en')->first();
        if ($this->productPhoto) {
            $imageName = time() . rand(1000, 1000000) . "." . $this->productPhoto->extension();
            $this->productPhoto->storeAs('public/images/product', $imageName);
        } else {
            $imageName = null;
        }
        $this->productWeight == "" || $this->productWeight == null ? $this->productWeight = "00" : $this->productWeight = $this->productWeight;

        DB::beginTransaction();
        $product = Product::create([
            'photo' => $imageName,
            'price' => $defaultPrice,
            'size' => $defaultSize,
            'weight' => $this->productWeight,
            'service_provider_id' => $this->shop->id,
            'tax_id' =>$this->selectedProductTax['id'],
        ]);
        ProductTranslation::create([
            'name' => $this->productName,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'product_id' => $product->id,
            'content' => $this->productDetails,
            'service_provider_id' => $this->shop->id,
        ]);

        // insert Other Sizes
        if (count($this->addedSizes) > 1) {
            foreach ($this->addedSizes as $newSize) {

                if ($newSize != 1) {
                    $this->sizeWeight[$newSize] == "" || $this->sizeWeight[$newSize] == null ? $this->sizeWeight[$newSize] = 0 : $this->sizeWeight[$newSize] = $this->sizeWeight[$newSize];

                    ProductSizes::create([
                        'price' => $this->price[$newSize],
                        'size' => $this->selectedSizeName[$newSize],
                        'weight' => $this->sizeWeight[$newSize],
                        'content' => $this->sizeDetail[$newSize],
                        'product_id' => $product->id,
                    ]);
                }
            }
        }

        // insert Addition
        if (count($this->addedAddition) > 0) {
            foreach ($this->addedAddition as $newAddition) {
                if (isset($this->additionPhoto[$newAddition])) {
                    $additionImage = time() . rand(1000, 1000000) . "." . $this->additionPhoto[$newAddition]->extension();
                    $this->additionPhoto[$newAddition]->storeAs('public/images/product/addition', $additionImage);
                } else {
                    $additionImage = null;
                }
                $this->additionWeight[$newAddition] == "" || $this->additionWeight[$newAddition] == null ? $this->additionWeight[$newAddition] = 0 : $this->additionWeight[$newAddition] = $this->additionWeight[$newAddition];
                ProductAdditions::create([
                    'price' => $this->additionPrice[$newAddition],
                    'name' => $this->additionName[$newAddition],
                    'content' => $this->additionDetail[$newAddition],
                    'weight' => $this->additionWeight[$newAddition],
                    'photo' => $additionImage,
                    'product_id' => $product->id,
                ]);
                $additionImage = null;
            }
        }
        // insert other photo
        if (isset($this->otherProductPhoto1)) {
            $otherImageName1 = time() . rand(1000, 1000000) . "." . $this->otherProductPhoto1->extension();
            $this->otherProductPhoto1->storeAs('public/images/product', $otherImageName1);
            ProductPhotos::create([
                'product_id' => $product->id,
                'name' => $otherImageName1,
            ]);
            $otherImageName1 = null;
        }
        if (isset($this->otherProductPhoto2)) {
            $otherImageName2 = time() . rand(1000, 1000000) . "." . $this->otherProductPhoto2->extension();
            $this->otherProductPhoto2->storeAs('public/images/product', $otherImageName2);
            ProductPhotos::create([
                'product_id' => $product->id,
                'name' => $otherImageName2,
            ]);
            $otherImageName2 = null;
        }

        if (isset($this->otherProductPhoto3)) {
            $otherImageName3 = time() . rand(1000, 1000000) . "." . $this->otherProductPhoto3->extension();
            $this->otherProductPhoto3->storeAs('public/images/product', $otherImageName3);
            ProductPhotos::create([
                'product_id' => $product->id,
                'name' => $otherImageName3,
            ]);
            $otherImageName3 = null;
        }

        DB::commit();
        $product->filters()->attach($this->allProductFiltersSelected);
        $imageName = null;
        $defaultPrice = null;
        $defaultSize = null;
        $this->restFilde();
        $message = trans('masterControl.successfully_Added');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);

    }

    public function restFilde()
    {

        $this->productWeight = null;
        $this->productDetails = null;
        $this->productName = null;
        $this->productPhoto = null;
        $this->price = [];
        $this->selectedSize = [];
        $this->selectedSizeName = [];
        $this->addedSizes = [1 => 1];
        $this->newSizeStored = [];
        $this->sizeWeight = [];
        $this->sizeDetail = [];
        $this->addedAddition = [];
        $this->storedAddition = [];
        $this->additionName = [];
        $this->additionDetail = [];
        $this->additionWeight = [];
        $this->additionPrice = [];
        $this->additionPhoto = [];
        $this->otherProductPhoto1 = null;
        $this->otherProductPhoto2 = null;
        $this->otherProductPhoto3 = null;
        $this->sizes=$this->allSizes;

        $this->allProductFiltersSelected = [];

    }


    public function addAddition()
    {
//        dd( $this->additionName);

        if (!isset($this->price[1])) {
            $message = trans('RestaurantControlPanel.validateProductPrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

        $this->openAdditionToast = true;
        $this->additionCounter++;
        $this->addedAddition[$this->additionCounter] = $this->additionCounter;
        $this->additionName[$this->additionCounter] = null;
        $this->additionDetail[$this->additionCounter] = null;
        $this->additionWeight[$this->additionCounter] = null;
        $this->additionPrice[$this->additionCounter] = null;
//        dd($this->openAdditionToast);
//        $this->additionPhoto[$i]=null;
//        $this->additionPhotoStored[$i]=0;

//        dd($this->additionPhoto, $this->additionPhotoStored);
    }

    public function saveAddition()
    {
        $this->validate([
//            'selectedSize' => 'required',
            "additionName" => "required|array|",
            'additionName.*' => 'required',
            "additionPrice" => "required|array|",
            'additionPrice.*' => 'required',
            "additionPhoto" => "required|array|",
            'additionPhoto.*' => 'required',
        ]);
        $this->openAdditionToast = false;
        $this->storedAddition[$this->additionCounter] = $this->additionCounter;

//        dd($this->addedAddition,$this->additionName);
    }


    public function closeAdditionToast()
    {
        $this->openAdditionToast = false;
        $this->removeAddition($this->additionCounter);
    }

    public function removeAddition($i)
    {
        unset($this->additionName[$i]);
        unset($this->additionDetail[$i]);
        unset($this->additionPrice[$i]);
        unset($this->addedAddition[$i]);
        unset($this->additionWeight[$i]);
        if (isset($this->additionPhoto[$i])) {
            unset($this->additionPhoto[$i]);
        }
        if (isset($this->storedAddition[$i])) {
            unset($this->storedAddition[$i]);
        }
//        dd($this->addedAddition,$this->storedAddition);
//        unset($this->additionPhotoStored[$i]);

    }

    public function removeAllAddition()
    {
        $this->additionName = [];
        $this->additionDetail = [];
        $this->additionPrice = [];
        $this->additionPhoto = [];
        $this->addedAddition = [];
        $this->additionPhotoStored = [];
        $this->storedAddition = [];

    }

    public function openSizeToast()
    {
        $this->isSize = true;
    }

    public function closeSizeToast()
    {
        $this->isSize = false;
    }

    public function addAdditionalSize()
    {
//        dd($this->newSizeStored,$this->addedSizes);
        if (!isset($this->price[1])) {
            $message = trans('RestaurantControlPanel.validateProductPrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        if (!isset($this->selectedSize[1])) {
            $message = trans('RestaurantControlPanel.validateDefaultSizeName');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $this->sizeCounter++;
        $this->isSize = true;
//        $this->addNewSize=true;
        $this->addedSizes[$this->sizeCounter] = $this->sizeCounter;
        $this->selectedSize[$this->sizeCounter] = null;
        $this->selectedSizeName[$this->sizeCounter] = null;
        $this->price[$this->sizeCounter] = null;
        $this->sizeDetail[$this->sizeCounter] = null;
        $this->sizeWeight[$this->sizeCounter] = null;
//        $message = trans('RestaurantControlPanel.validateSizePrice');
//        $this->emit('sizeAdded');
//        $this->openSizeToast();
//        dd('ff',$this->addNewSize);
    }

    public function saveSize($i)
    {

//        dd($this->sizeWeight[$i],$this->productWeight);
        if (!isset($this->selectedSize[$i]) and $i != 1) {
            $message = trans('RestaurantControlPanel.validateSizeName');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }

        if (isset($this->sizeWeight[$i]) and $i != 1 and $this->sizeWeight[$i] != "" and $this->sizeWeight[$i] != null) {
            if (!isset($this->productWeight)) {
                $message = trans('RestaurantControlPanel.validateDefaultFoodWeight');
                $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                return;
            }
        }
        if (!isset($this->price[$i]) and $i != 1) {
            $message = trans('RestaurantControlPanel.validateSizePrice');
            $this->emit('alert', ['icon' => 'error', 'title' => $message]);
            return;
        }
        $this->newSizeStored[$i] = $i;
//        $this->addNewSize=false;
        $this->isSize = false;

//        $this->closeSizeToast();
    }

    public function cancelSize()
    {
        $this->removeSize($this->sizeCounter);
//        $this->addNewSize=false;
        $this->closeSizeToast();
    }

    public function removeSize($sizeNumber)
    {
//        dd($sizeNumber, $this->selectedSize,$this->price);

//        dd($sizeNumber,$this->selectedSize[$sizeNumber]);

//        $sizeNumber++;
        foreach ($this->allSizes as $key => $size) {
            if ($this->selectedSize[$sizeNumber] == $size['id']) {
                $this->sizes[$key] = $size;
//                break;
            }
        }
        unset($this->selectedSize[$sizeNumber]);
        unset($this->selectedSizeName[$sizeNumber]);
        if ($sizeNumber != 1) {
            unset($this->price[$sizeNumber]);
            unset($this->addedSizes[$sizeNumber]);
            unset($this->sizeWeight[$sizeNumber]);
            unset($this->sizeDetail[$sizeNumber]);
        }

        if (isset($this->newSizeStored[$sizeNumber])) {
            unset($this->newSizeStored[$sizeNumber]);
        }


//        $this->addNewSize=null;

//        dd($this->addedSizes,$this->newSizeStored,$this->price,$sizeNumber);
    }

    public function setSelectedSize($size, $i)
    {
//        dd($size);

        $this->selectedSize[$i] = $size['id'];
        $this->selectedSizeName[$i] = $size['name'];

        foreach ($this->sizes as $key => $size) {
            if ($size['id'] == $this->selectedSize[$i]) {
                unset($this->sizes[$key]);
            }
        }

//        dd($this->selectedSize,$this->sizes,$this->allSizes);
    }


    public function mount()
    {
//        dd($this->selectedSize);
        $this->locale = App::getLocale();


        $this->shop = ServiceProvider::with(['filters', 'size' => function ($query) {
            $query->select('id', 'name', 'service_provider_id')
                ->get();
        }])->where('user_id', Auth::id())->first();
//        dd(Auth::user()->country_id);
        $this->sizes = $this->shop->size->toArray();
        $this->allSizes = $this->sizes;
        if($this->shop->tax_id!=null)
        {
//            dd($this->serviceProvider);
            $this->serviceProviderTax=$this->shop->tax->toArray();
            $this->selectedProductTax=$this->serviceProviderTax;
//            dd($this->selectedTax);
        }
        $countryDetails = Country::with('taxs')
            ->findOrFail(Auth::user()->country_id);
//        $this->countryCurrency=$countryDetails->currency_code;
        $this->taxesInCountry=$countryDetails->taxs->toArray();
//        dd($countryDetails);
        if (isset($countryDetails->currency_code)) {
            $this->currencyCode = $countryDetails->currency_code;

        }
//        dd($this->shop->filters );
        foreach ($this->shop->filters as $shopFilter) {
            $productFilterForThisRestaurantFilter = ProductFilterTrans::select(['id', 'name', 'code_lang', 'filter_id', 'product_filter_id'])
                ->where('code_lang', $this->locale)
                ->where('filter_id', $shopFilter->id)->get()->toArray();
//            dd($productFilterForThisRestaurantFilter);
            array_push($this->allProductFilters, $productFilterForThisRestaurantFilter);

        }

    }

    public function render()
    {
        return view('livewire.template.control-panel.food.add-new-product-component');
    }
}
