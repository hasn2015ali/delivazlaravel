<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\Food;
use App\Models\FoodTranslation;
use App\Models\MealFilterTrans;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class FoodComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $user;
    public $locale;
    public $restaurant;
    public $allFoodFilters=[];
    public $foodByFiltersShow=false;
    public $MealFilterName;
    public $MealFilterID;
    public $isFilterList=false;
    public $openDeleteToast=false;
    public $foodID;
    public $currencyCode;



    public function getQueryString()
    {
        return [];
    }

    public function mount()
    {
        $this->locale=App::getLocale();
        $this->restaurant=ServiceProvider::with('filters')->where('user_id',Auth::id())->first();
        foreach ($this->restaurant->filters as $restaurantFilter)
        {
            $mailFilterForThisRestaurantFilter=MealFilterTrans::where('code_lang',App::getlocale())
                ->where('filter_id',$restaurantFilter->id)->get()->toArray();
            array_push($this->allFoodFilters,$mailFilterForThisRestaurantFilter);
        }
        $countryDetails=Country::findOrFail(Auth::user()->country_id);
//        dd($countryDetails);
        if(isset($countryDetails->currency_code))
        {
            $this->currencyCode=$countryDetails->currency_code;

        }
    }
    public function openFilterList()
    {
        $this->isFilterList=true;
//        dd($this->isFilterList);
    }
    public function closeFilterList()
    {
        $this->isFilterList=false;
    }
    public function getFoodForThisFilter($filterID)
    {
//        dd('gg');

        $this->closeFilterList();
        if($filterID==0)
        {
            $this->foodByFiltersShow=false;
            $this->MealFilterName=trans('restaurant.allMealsFilters');
        }
        else{
            $this->foodByFiltersShow=true;
            $this->MealFilterID=$filterID;
            $this->MealFilterName=MealFilterTrans::where('meal_filter_id',$filterID)
                ->where('code_lang',App::getlocale())
                ->first()->name;
        }
        $this->resetPage();


//        dd( $this->MealFilterName);
    }

    public function deleteCall($id){
        $this->foodID=$id;
        $this->openToast();
//        dd($this->bannerId);
    }
    public function closeToast()
    {
        $this->openDeleteToast=false;

    }
    public function openToast()
    {
        $this->openDeleteToast=true;

    }
    public function deleteAction()
    {
        $food=Food::with('photos')->find($this->foodID);
        $food->delete();
        $food->filters()->detach();
        if($food->photo!=null)
        {
            Storage::delete('public/images/food/'.$food->photo);
        }
        foreach ($food->photos as $photo)
        {
            Storage::delete('public/images/food/'.$photo->name);
        }
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
        $food=null;
        $this->foodID=null;
        $this->closeToast();
    }
    public function render()
    {
        $foods=null;
        if(!$this->foodByFiltersShow)
        {
            $foods = Food::with(['translation' => function($query){
                $query->select('name', 'content','code_lang','food_id')
                    ->where('code_lang',App::getlocale())
                    ->get();
        }])
                ->where('service_provider_id',$this->restaurant->id)
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
//            dd('gg');
            $foods = Food::with(['translation' => function($query){
                $query->select('name', 'content','code_lang','food_id')
                    ->where('code_lang',App::getlocale())
                    ->get();
            }])->whereHas('filters', function ($query) {
                $query->where('meal_filters.id', $this->MealFilterID);
            })  ->where('service_provider_id',$this->restaurant->id)
                ->orderBy('id','desc')
                ->paginate(10);
        }

//        dd($foods);
        return view('livewire.template.control-panel.food.food-component',compact('foods'));
    }
}
