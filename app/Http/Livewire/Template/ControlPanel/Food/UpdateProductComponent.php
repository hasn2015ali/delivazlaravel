<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\Product;
use App\Models\ProductAdditions;
use App\Models\ProductFilterTrans;
use App\Models\ProductPhotos;
use App\Models\ProductSizes;
use App\Models\MealFilterTrans;
use App\Models\ServiceProvider;
use App\Traits\WithFoodToasts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class UpdateProductComponent extends Component
{
    use WithFileUploads, WithFoodToasts;
    public $productID;
    public $locale;
    public $product;
    public $productPhoto;
    public $oldProductPhoto;
    public $currencyCode;
    public $productName;
    public $productDetails;
    public $productWeight;
    public $sizes = [];
    public $allSizes = [];
    public $shop;
    public $allProductFilters = [];
    public $productHour;
    public $productMinute;
    public $defaultSelectedSize;
    public $defaultSelectedSizeName;
    public $defaultPrice;
    public $openDeleteToast = false;
    public $editBasicInfo = false;
    public $openNewPhoto = false;
    public $openNewSize = false;
    public $openNewAddition = false;
    public $photoID;
    public $deleteType;
    public $newAdditionalPhoto;
    public $newSizeName;
    public $newSizeId;
    public $newSizePrice;
    public $newSizeWeight;
    public $newSizeContent;
    public $newSizeUpdate=null;
    public $additionName;
    public $additionDetail;
    public $additionWeight;
    public $additionPrice;
    public $additionPhoto;
    public $additionPhotoOld;
    public $updatedAddition=null;
    public $additionID;
    public $openEditFilters=false;
    public $allProductFiltersSelected=[];
    public $productAdditionPhotoPath='/storage/images/product/addition/';
    public $serviceProviderTax=[];
    public $selectedProductTax=[];
    public $countryCurrency;
    public $taxesInCountry;

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }


    public function messages()
    {
        return [
            'selectedProductTax.required' => trans('RestaurantControlPanel.validateSelectedProductTax'),
            'additionPhoto.required' => trans('RestaurantControlPanel.validateAdditionPhoto'),
            'productPhoto.required' => trans('RestaurantControlPanel.validateProductPhoto'),
            'price.required' => trans('RestaurantControlPanel.validateProductPrice'),
            'newSizePrice.required' => trans('RestaurantControlPanel.validateProductPrice'),
            'newSizeName.required' => trans('RestaurantControlPanel.validateNewSizeName'),
            'additionName.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'additionName.*.required' => trans('RestaurantControlPanel.validateAdditionName'),
            'additionPrice.*.required' => trans('RestaurantControlPanel.validateAdditionPrice'),
            'productName.required' => trans('RestaurantControlPanel.validateProductName'),
            'allProductFiltersSelected.required' => trans('RestaurantControlPanel.validateAllProductFiltersSelected'),
            'price.*.required' => trans('RestaurantControlPanel.validateProductPrice'),
        ];
    }
    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectedProductTax['value']=$tax['value'];
        $this->selectedProductTax['id']=$tax['id'];
        $this->selectedProductTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }

    public function editFilters()
    {
        $this->openEditFilters=true;
        //set Old Meal Filters
        $this->getOldFilters();

    }
    public function getOldFilters()
    {
        $this->allProductFiltersSelected=[];
        foreach ($this->product->filters->toArray() as $filters)
        {
            $filterID= (string) $filters['id'];
            array_push($this->allProductFiltersSelected,$filterID);
        }
    }
    public function saveNewFilters()
    {
//        dd($this->allProductFiltersSelected);
        $this->validate([
            'allProductFiltersSelected' => 'array|required',
        ]);
        $this->product->filters()->detach();
        $this->product->filters()->attach($this->allProductFiltersSelected);
        $this->allProductFilters=[];
        $this->mount();
        $message=trans( 'RestaurantControlPanel.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->cancelNewFilters();
    }
    public function cancelNewFilters()
    {
        $this->openEditFilters=false;
        $this->getOldFilters();

    }
    public function saveNewAdditionalPhoto()
    {
        $this->validate([
            'newAdditionalPhoto' => 'required',
        ]);
        $otherImageName3 = time() . rand(1000, 1000000) . "." . $this->newAdditionalPhoto->extension();
        $this->newAdditionalPhoto->storeAs('public/images/product', $otherImageName3);
        ProductPhotos::create([
            'product_id' => $this->product->id,
            'name' => $otherImageName3,
        ]);
        $otherImageName3 = null;
        $this->closeAdditionPhotoToast();
        $this->mount();
        $message = trans('RestaurantControlPanel.successfully_Added');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
    }

    public function saveBasicInfo()
    {
        //validate size and price
        $this->validate([
            'productName' => 'required',
            "defaultPrice" => "required",
            'selectedProductTax' => 'required',
        ]);


        if ($this->productPhoto) {
            $imageName = time() . rand(1000, 1000000) . "." . $this->productPhoto->extension();
            $this->productPhoto->storeAs('public/images/product', $imageName);
            Storage::delete('public/images/product/' . $this->oldProductPhoto);
        } else {
            $imageName = $this->oldProductPhoto;
        }
        $this->productWeight == "" || $this->productWeight == null ? $this->productWeight = "00" : $this->productWeight = $this->productWeight;
        $this->productHour == "" || $this->productHour == null ? $this->productHour = "00" : $this->productHour = $this->productHour;
        $this->productMinute == "" || $this->productMinute == null ? $this->productMinute = "00" : $this->productMinute = $this->productMinute;

        DB::beginTransaction();
        $this->product->update([
            'photo' => $imageName,
            'price' => $this->defaultPrice,
            'size' => $this->defaultSelectedSizeName,
            'time_hour' => $this->productHour,
            'weight' => $this->productWeight,
            'tax_id' =>$this->selectedProductTax['id'],
        ]);
        $this->product->translation[0]->update([
            'name' => $this->productName,
            'content' => $this->productDetails,
        ]);
        DB::commit();
        $message = trans('RestaurantControlPanel.Updated_Successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->cancelBasicInfo();
    }

    public function removeAdditionalPhoto($photoID)
    {
        $this->photoID = $photoID;
        $this->openDeleteToast();
        $this->deleteType = "AdditionalPhoto";
    }
    public function callDeleteAddition($additionID)
    {
        $this->additionID=$additionID;
        $this->openDeleteToast();
        $this->deleteType = "Addition";
    }

    public function deleteAction()
    {
        if ($this->deleteType == "AdditionalPhoto") {
            $photo = ProductPhotos::find($this->photoID);
            Storage::delete('public/images/product/' . $photo->name);
            $photo->delete();
            $photo = null;
        }
        if ($this->deleteType == "size") {
            $productSize = ProductSizes::find($this->newSizeId);
            foreach ($this->allSizes as $key => $size) {
                if ($productSize->size == $size['name']) {
                    $this->sizes[$key] = $size;
                }
            }
//            dd($this->sizes);
            $productSize->delete();
            $productSize = null;
        }
        if ($this->deleteType == "Addition") {
            $addition = ProductAdditions::find($this->additionID);
            Storage::delete('public/images/product/addition' . $addition->photo);
            $addition->delete();
            $addition = null;
        }
        $message = trans('RestaurantControlPanel.Deleted_successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->closeToast();
    }
    public function saveAddition()
    {
        $this->validate([
            "additionName" => "required",
            'additionPrice' => 'required',
        ]);

        if (isset($this->additionPhoto)) {
            $additionImage = time() . rand(1000, 1000000) . "." . $this->additionPhoto->extension();
            $this->additionPhoto->storeAs('public/images/product/addition', $additionImage);
        } elseif(isset($this->additionPhotoOld)){
            $additionImage=$this->additionPhotoOld;
        }else {
            $additionImage = null;
        }
        $this->additionWeight == "" || $this->additionWeight == null ? $this->additionWeight = 0 : $this->additionWeight = $this->additionWeight;

        if($this->updatedAddition==null)
        {
            $this->validate([
                'additionPhoto' => 'required',
            ]);
            ProductAdditions::create([
                'price' => $this->additionPrice,
                'name' => $this->additionName,
                'content' => $this->additionDetail,
                'weight' => $this->additionWeight,
                'photo' => $additionImage,
                'product_id' => $this->product->id,
            ]);
            $message = trans('RestaurantControlPanel.successfully_Added');

        }
        else
        {
            $this->updatedAddition->update([
                'price' => $this->additionPrice,
                'name' => $this->additionName,
                'content' => $this->additionDetail,
                'weight' => $this->additionWeight,
                'photo' => $additionImage,
            ]);
            $message = trans('RestaurantControlPanel.Updated_Successfully');

        }

        $additionImage = null;
        $this->resetNewAdditionValue();

        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->closeAdditionToast();
    }
    public function updateAddition($additionID)
    {
        $this->updatedAddition=ProductAdditions::find($additionID);
        $this->additionName=$this->updatedAddition->name;
        $this->additionDetail=$this->updatedAddition->content;
        $this->additionWeight=$this->updatedAddition->weight;
        $this->additionPrice=$this->updatedAddition->price;
        $this->additionPhotoOld=$this->updatedAddition->photo;
        $this->openAdditionToast();


    }
    public function resetNewAdditionValue()
    {
        $this->additionName=null;
        $this->additionPhoto=null;
        $this->additionWeight=null;
        $this->additionPrice=null;
        $this->additionDetail=null;
        $this->additionPhotoOld=null;
    }
    public function updateSize($sizeID)
    {
        $this->newSizeId=$sizeID;
        $this->newSizeUpdate=ProductSizes::find($sizeID);
        $this->newSizeName=$this->newSizeUpdate->size;
        $this->newSizePrice=$this->newSizeUpdate->price;
        $this->newSizeContent=$this->newSizeUpdate->content;
        $this->newSizeWeight=$this->newSizeUpdate->weight;
        $this->openNewSize();
    }

    public function mount()
    {
        $this->locale = App::getLocale();
        $this->shop = ServiceProvider::with(['filters', 'size' => function ($query) {
            $query->select('id', 'name', 'service_provider_id')
                ->get();
        }])->where('user_id', Auth::id())->first();
        $this->product = Product::with(['translation' => function ($query) {
            $query->select('name', 'content', 'code_lang', 'product_id')
                ->where('code_lang', App::getlocale())
                ->get();
        }, 'sizes', 'additions', 'filters', 'photos'])->find($this->productID);
        $this->sizes = $this->shop->size->toArray();
        $this->allSizes = $this->sizes;

        if($this->product->tax_id!=null)
        {
            $this->selectedProductTax=$this->product->tax->toArray();
        }

        $countryDetails = Country::with('taxs')->findOrFail(Auth::user()->country_id);
        $this->taxesInCountry=$countryDetails->taxs->toArray();
//        dd($countryDetails);
        if (isset($countryDetails->currency_code)) {
            $this->currencyCode = $countryDetails->currency_code;

        }
        foreach ($this->shop->filters as $shopFilter) {
            $mailFilterForThisRestaurantFilter = ProductFilterTrans::select(['id', 'name', 'code_lang', 'filter_id', 'product_filter_id'])
                ->where('code_lang', $this->locale)
                ->where('filter_id', $shopFilter->id)->get()->toArray();
//            dd($mailFilterForThisRestaurantFilter);
            array_push($this->allProductFilters, $mailFilterForThisRestaurantFilter);

        }
//        dd($this->allProductFilters);
//        dd($this->shop,$this->sizes);

        $this->oldProductPhoto = $this->product->photo;
        $this->productName = $this->product->translation[0]['name'];
        $this->productDetails = $this->product->translation[0]['content'];
        $this->productWeight = $this->product->weight;
        $this->defaultPrice = $this->product->price;
        if (isset($this->product->size) and $this->product->size != null) {
            foreach ($this->allSizes as $size) {
                if ($size['name'] == $this->product->size) {
                    $this->defaultSelectedSize = $size['id'];
                }
            }
            $this->defaultSelectedSizeName = $this->product->size;
        }

        // set avalaibleizes
        if (count($this->product->sizes->toArray()) == 0) {
            foreach ($this->sizes as $key => $size) {
                if ($size['name'] == $this->defaultSelectedSizeName) {
                    unset($this->sizes[$key]);
                }
            }

        } else {
            foreach ($this->product->sizes->toArray() as $storedSize) {
                foreach ($this->sizes as $key => $size) {
                    if ($size['name'] == $storedSize['size'] or $size['name'] == $this->defaultSelectedSizeName) {
                        unset($this->sizes[$key]);
                    }
                }
            }

        }

    }

    public function setSelectedSize($size, $i)
    {
//        dd($size,$i);

        if ($i == 2) {
            $this->newSizeId = $size['id'];
            $this->newSizeName = $size['name'];
            foreach ($this->sizes as $key => $size) {
                if ($size['id'] == $this->newSizeId) {
                    unset($this->sizes[$key]);
                }
            }
        } else {
            $this->defaultSelectedSize = $size['id'];
            $this->defaultSelectedSizeName = $size['name'];
            foreach ($this->sizes as $key => $size) {
                if ($size['id'] == $this->defaultSelectedSize) {
                    unset($this->sizes[$key]);
                }
            }
        }
    }

    public function saveSize()
    {
        $this->validate([
            'newSizeName' => 'required',
            "newSizePrice" => "required",
        ]);
        $this->newSizeWeight == null || $this->newSizeWeight == "" ? $this->newSizeWeight = 0 : $this->newSizeWeight = $this->newSizeWeight;
//     dd($this->newSizeWeight);
        if($this->newSizeUpdate==null)
        {
            ProductSizes::create([
                'price' => $this->newSizePrice,
                'size' => $this->newSizeName,
                'weight' => $this->newSizeWeight,
                'content' => $this->newSizeContent,
                'product_id' => $this->product->id,
            ]);
            $message = trans('RestaurantControlPanel.successfully_Added');

        }
        else
        {
            $this->newSizeUpdate->update([
                'price' => $this->newSizePrice,
                'size' => $this->newSizeName,
                'weight' => $this->newSizeWeight,
                'content' => $this->newSizeContent,
            ]);
            $this->newSizeUpdate=null;
            $message = trans('RestaurantControlPanel.Updated_Successfully');

        }

        $this->resetNewSizeValue();
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->mount();
        $this->cancelNewSize();

    }

    public function callDeleteSize($sizeID)
    {
        $this->newSizeId = $sizeID;
        $this->deleteType = "size";
        $this->openDeleteToast();
    }

    public function resetNewSizeValue()
    {
        $this->newSizeName = null;
        $this->newSizeId = null;
        $this->newSizeContent = null;
        $this->newSizePrice = null;
        $this->newSizeWeight = null;
    }

    public function cancelSize()
    {
        $this->resetNewSizeValue();
        $this->cancelNewSize();
    }

    public function removeSize()
    {

        foreach ($this->allSizes as $key => $size) {
            if ($this->defaultSelectedSize == $size['id']) {
                $this->sizes[$key] = $size;
            }
        }

        $this->defaultSelectedSizeName = null;
        $this->defaultSelectedSize = null;

    }

    public function render()
    {
        return view('livewire.template.control-panel.food.update-product-component');
    }
}
