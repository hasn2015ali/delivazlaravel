<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\Country;
use App\Models\Product;
use App\Models\ProductFilterTrans;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;

class ProductComponent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $user;
    public $locale;
    public $shop;
    public $allProductFilters=[];
    public $productByFiltersShow=false;
    public $ProductFilterName;
    public $ProductFilterID;
    public $isFilterList=false;
    public $openDeleteToast=false;
    public $productID;
    public $currencyCode;


    public function getQueryString()
    {
        return [];
    }

    public function mount()
    {
        $this->locale=App::getLocale();
        $this->shop=ServiceProvider::with('filters')->where('user_id',Auth::id())->first();
        foreach ($this->shop->filters as $shopFilter)
        {
            $productFilterForThisShopFilter=ProductFilterTrans::where('code_lang',App::getlocale())
                ->where('filter_id',$shopFilter->id)->get()->toArray();
            array_push($this->allProductFilters,$productFilterForThisShopFilter);
        }
        $countryDetails=Country::findOrFail(Auth::user()->country_id);
//        dd($countryDetails);
        if(isset($countryDetails->currency_code))
        {
            $this->currencyCode=$countryDetails->currency_code;

        }
    }
    public function openFilterList()
    {
        $this->isFilterList=true;
//        dd($this->isFilterList);
    }
    public function closeFilterList()
    {
        $this->isFilterList=false;
    }
    public function getProductForThisFilter($filterID)
    {
//        dd('gg');

        $this->closeFilterList();
        if($filterID==0)
        {
            $this->productByFiltersShow=false;
            $this->ProductFilterName=trans('RestaurantControlPanel.allProductsFilters');
        }
        else{
            $this->productByFiltersShow=true;
            $this->ProductFilterID=$filterID;
            $this->ProductFilterName=ProductFilterTrans::where('product_filter_id',$filterID)
                ->where('code_lang',App::getlocale())
                ->first()->name;
        }
        $this->resetPage();


//        dd( $this->ProductFilterName);
    }

    public function deleteCall($id){
        $this->productID=$id;
        $this->openToast();
//        dd($this->bannerId);
    }
    public function closeToast()
    {
        $this->openDeleteToast=false;

    }
    public function openToast()
    {
        $this->openDeleteToast=true;

    }
    public function deleteAction()
    {
        $product=Product::with('photos')->find($this->productID);
        $product->delete();
        $product->filters()->detach();
        if($product->photo!=null)
        {
            Storage::delete('public/images/product/'.$product->photo);
        }
        foreach ($product->photos as $photo)
        {
            Storage::delete('public/images/product/'.$photo->name);
        }
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
        $product=null;
        $this->productID=null;
        $this->closeToast();
    }
    public function render()
    {
        $products=null;
        if(!$this->productByFiltersShow)
        {
            $products = Product::with(['translation' => function($query){
                $query->select('name', 'content','code_lang','product_id')
                    ->where('code_lang',App::getlocale())
                    ->get();
            }])
                ->where('service_provider_id',$this->shop->id)
                ->orderBy('id','desc')
                ->paginate(10);
        }
        else
        {
//            dd('gg');
            $products = Product::with(['translation' => function($query){
                $query->select('name', 'content','code_lang','product_id')
                    ->where('code_lang',App::getlocale())
                    ->get();
            }])->whereHas('filters', function ($query) {
                $query->where('product_filters.id', $this->ProductFilterID);
            })  ->where('service_provider_id',$this->shop->id)
                ->orderBy('id','desc')
                ->paginate(10);
        }

        return view('livewire.template.control-panel.food.product-component',compact('products'));
    }
}
