<?php

namespace App\Http\Livewire\Template\ControlPanel\Food;

use App\Models\ServiceProvider;
use App\Models\ServiceProviderMenu;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use PDO;

class MenuComponent extends Component
{
    public $locale;
    public $user;
    public $serviceProvider;
    public $menuFilters;
    public $langCode;
    public $order;
    public $active;
    public $allFilterUsed;

    public function saveFilter()
    {
//        $activeFilter = Arr::where($this->active, function ($value, $key) {
////            return is_string($value);
//            return $value!=false;
//        });
//        dd($this->order,$this->active, $this->allFilterUsed,$activeFilter);
        $newOrder=$this->order;
        $newActive=$this->active;
//        dd($newActive,$newOrder);
        foreach ($newActive as $key => $filter) {
            if ($filter == true) {
//                dd($this->active[22],$this->order[22]==0,$newOrder);
                if (!isset($newOrder[$key]) or $newOrder[$key]=="0"  ) {
//                    dd($key,$newOrder[$key],$newOrder,$newOrder);
                    $filter=$this->getFilterAfterWhere($key,$this->menuFilters);
                    $firstKey=array_key_first($filter);
                    $message = trans('RestaurantControlPanel.filterOrderError')." ".$filter[$firstKey]['name'];
                    $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                    $this->setAllMenuFilters();
                    return;
                }
                ServiceProviderMenu::updateOrCreate(
                    ['service_provider_id' => $this->serviceProvider->id, 'filter_id' => $key],
                    ['state' => 1,'order'=>$newOrder[$key]]
                );
                $this->setAllMenuFilters();

            } elseif ($filter == false) {
                if (!isset($newOrder[$key])) {
                    $filter=$this->getFilterAfterWhere($key,$this->menuFilters);
                    $firstKey=array_key_first($filter);
                    $message = trans('RestaurantControlPanel.filterOrderError')." ".$filter[$firstKey]['name'];
                    $this->emit('alert', ['icon' => 'error', 'title' => $message]);
                    $this->setAllMenuFilters();
                    return;
                }
                ServiceProviderMenu::updateOrCreate(
                    ['service_provider_id' => $this->serviceProvider->id, 'filter_id' => $key],
                    ['state' => 0,'order'=>0]
                );
                $this->setAllMenuFilters();
            }
        }
        $newOrder=null;
        $newActive=null;
        $message = trans('RestaurantControlPanel.successfully_Added');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);

    }
    public function cancelNewChanges()
    {
        $this->order=null;
        $this->active=null;
        $this->menuFilters=null;
        $this->setAllMenuFilters();
    }

    public function getFilterAfterWhere($key,$menuFilters)
    {
//        dd($key,$menuFilters);
        if ($this->user->role_id == 9) {
            $filters=Arr::where($menuFilters, function ($value, $key1)use ($key) {
                return $value['meal_filter_id'] == $key;
            });
        } elseif ($this->user->role_id == 10) {
            $filters=Arr::where($menuFilters, function ($value, $key1)use ($key) {
                return $value['product_filter_id'] == $key;
            });
        }
        return $filters;

    }
    public function setAllMenuFilters()
    {
//        DB::setFetchMode(PDO::FETCH_ASSOC);
        if ($this->user->role_id == 9) {
            $allFilterUsed = DB::table('food_filter_relations')
                ->select('food_filter_relations.filter_id')
                ->join('food', 'food_filter_relations.food_id', '=', 'food.id')
                ->where('food.service_provider_id', $this->serviceProvider->id)
                ->pluck('filter_id');
//            dd($allFilterUsed);
            $mealFilters = DB::table('service_provider_filters')
                ->where('service_provider_filters.service_provider_id', '=', $this->serviceProvider->id)
                ->join('meal_filter_trans', 'service_provider_filters.filter_id', '=', 'meal_filter_trans.filter_id')
                ->select(['meal_filter_trans.filter_id', 'meal_filter_trans.meal_filter_id', 'meal_filter_trans.name'])
                ->whereIn('meal_filter_trans.meal_filter_id', $allFilterUsed)
                ->where('meal_filter_trans.code_lang', $this->langCode)
                ->orderBy('meal_filter_trans.name', 'asc')
                ->get()->toArray();
            $this->allFilterUsed = $allFilterUsed;
            $this->menuFilters = $mealFilters;

//            dd($this->menuFilters,$this->allFilterUsed);
        } elseif ($this->user->role_id == 10) {
            $allFilterUsed = DB::table('product_filters_relations')
                ->select('product_filters_relations.filter_id')
                ->join('products', 'product_filters_relations.product_id', '=', 'products.id')
                ->where('products.service_provider_id', $this->serviceProvider->id)
                ->pluck('filter_id');
            $productFilters = DB::table('service_provider_filters')
                ->where('service_provider_filters.service_provider_id', '=', $this->serviceProvider->id)
                ->join('product_filter_trans', 'service_provider_filters.filter_id', '=', 'product_filter_trans.filter_id')
                ->select(['product_filter_trans.filter_id', 'product_filter_trans.product_filter_id', 'product_filter_trans.name'])
                ->whereIn('product_filter_trans.product_filter_id', $allFilterUsed)
                ->where('product_filter_trans.code_lang', $this->langCode)
                ->orderBy('product_filter_trans.name', 'asc')
                ->get()->toArray();
            $this->allFilterUsed = $allFilterUsed;
            $this->menuFilters = $productFilters;

//            dd($this->menuFilters);
        }
        $this->menuFilters = collect($this->menuFilters)->map(function($x){ return (array) $x; })->toArray();

//        DB::setFetchMode(PDO::FETCH_CLASS);
        $menuOld=ServiceProviderMenu::select(['filter_id','order','state'])
            ->where('service_provider_id',$this->serviceProvider->id)
            ->get()->toArray();
       foreach ($menuOld as $filter)
       {
           $key=$filter['filter_id'];

           if($filter['state']==1)
           {
               $this->active[$key]=true;
           }
           else{
               $this->active[$key]=false;
           }
           $order=$filter['order'];
           $this->order[$key]="$order";
       }
//        dd($menuOld);
    }
    public function mount()
    {
        $this->locale = App::getLocale();
        $this->user = Auth::user();
        $this->langCode = App::getLocale();
        $this->serviceProvider = ServiceProvider::where('user_id', $this->user->id)->first();

        $this->setAllMenuFilters();


//        dd($this->menuFilters,$this->allFilterUsed);
    }

    public function render()
    {
        return view('livewire.template.control-panel.food.menu-component');
    }
}
