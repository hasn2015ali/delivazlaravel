<?php

namespace App\Http\Livewire\Template\ControlPanel\Sizes;

use App\Models\ServiceProvider;
use App\Models\serviceProviderSizes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class SizeComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $serviceProvider;
    public $serviceProviderID;
    public $sizeName;
    public $sizeId;
    public $selectedSize;
    public $user;
    public $locale;
    public $openDeleteToast;
    public $openEditToast;
    public $sizeUP;


    protected $listeners = [
//        'submit' => '$refresh',
//        'update' => '$refresh',
//        'deleteAction' => '$refresh',
    ];
    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }

    public function messages()
    {

        return [
            'sizeName.required' => trans('RestaurantControlPanel.validateSize'),
            'sizeUP.required' => trans('RestaurantControlPanel.validateSize'),

        ];
    }

    public function submit()
    {
        $this->validate([
            'sizeName' => 'required',
        ]);

        serviceProviderSizes::create([
            'name' => $this->sizeName,
            'service_provider_id' => $this->serviceProviderID,
        ]);
        $message = trans('RestaurantControlPanel.successfully_Added');
        $this->restSizeFilde();
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->emit('addNewSize');

    }

    public function deleteCall($id)
    {
        $this->openDeleteToast=true;
        $this->sizeId = $id;
    }
    public function openEditToast($sizeID)
    {
        $this->openEditToast=true;
        $this->restSizeFilde();
        $this->selectedSize=serviceProviderSizes::find($sizeID);
        $this->sizeUP=$this->selectedSize->name;
    }
    public function updateSize()
    {
        $this->validate([
            'sizeUP' => 'required',
        ]);
        $this->selectedSize->name=$this->sizeUP;
        $this->selectedSize->save();
        $this->selectedSize=null;
        $this->sizeUP=null;
        $this->restSizeFilde();
        $message = trans('RestaurantControlPanel.Updated_Successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->emit('updateSize');
        $this->closeEditToast();
    }
    public function closeEditToast()
    {

        $this->openEditToast=false;
    }
    public function closeToast()
    {
        $this->openDeleteToast=false;
//                dd("ff");

    }
    public function deleteAction()
    {
        $this->selectedSize = serviceProviderSizes::find($this->sizeId);
        $this->selectedSize->delete();
//        sleep(1);
        $this->sizeId = null;
        $this->selectedSize=null;
        $message = trans('RestaurantControlPanel.Deleted_successfully');
        $this->emit('alert', ['icon' => 'success', 'title' => $message]);
        $this->emit('deleteAction2');
        $this->closeToast();
    }




    public function restSizeFilde()
    {
        $this->sizeName = null;
    }


    public function mount()
    {
        $this->serviceProvider = ServiceProvider::where('user_id', Auth::id())->first();
        $this->serviceProviderID = $this->serviceProvider->id;
        $this->user=Auth::user();
        $this->locale=App::getLocale();
    }

    public function render()
    {
        $sizes = serviceProviderSizes::where('service_provider_id', $this->serviceProviderID)
            ->orderBy('id', 'desc')
            ->paginate(5);
        return view('livewire.template.control-panel.sizes.size-component', compact('sizes'));
    }
}
