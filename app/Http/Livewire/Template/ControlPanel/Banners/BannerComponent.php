<?php

namespace App\Http\Livewire\Template\ControlPanel\Banners;

use App\Models\ServiceProvider;
use App\Models\ServiceProviderBanner;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class BannerComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $serviceProviderBanner;
    public $bannerId;
    public $serviceProviderID;
    public $serviceProvider;
    public $banner;
    public $openDeleteToast=false;
    public $user;
    public $imageFolders;
    public $bannerInPreviewWindow;
    public $openPreviewToast;
    public $bannerNumber;
    public $bannerImage;

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }
    public function messages()
    {

        return [
            'serviceProviderBanner.required' => trans( 'restaurant.validateRestaurantBanner' ),
        ];
    }


    public function closePreviewToast()
    {
        $this->openPreviewToast=false;
        $this->bannerNumber=null;
        $this->bannerImage=null;
    }
    public function openPreviewToast($bannerNumber,$bannerPhoto)
    {
        $this->openPreviewToast=true;
        $this->bannerNumber=$bannerNumber;
        $this->bannerImage=$bannerPhoto;
    }
    public function mount()
    {
        $this->bannerInPreviewWindow= __("RestaurantControlPanel.Banner");
        $this->user=Auth::user();
        $this->serviceProvider=ServiceProvider::where('user_id',Auth::id())->first();
        $this->serviceProviderID=$this->serviceProvider->id;
        if($this->user->role_id==9)
        {
            $this->imageFolders='storage/images/sliders/restaurant/';

        }
        elseif ($this->user->role_id==10)
        {
            $this->imageFolders='storage/images/sliders/vendor/';

        }

    }
    public function submit()
    {

        $this->validate([
            'serviceProviderBanner' => 'required',
        ]);
        if($this->serviceProviderBanner)
        {
            $imageName=time().rand(1,100).".".$this->serviceProviderBanner->extension();
            if($this->user->role_id==9)
            {
                $this->serviceProviderBanner->storeAs('public/images/sliders/restaurant',$imageName);

            }
            elseif ($this->user->role_id==10)
            {
                $this->serviceProviderBanner->storeAs('public/images/sliders/vendor',$imageName);

            }

            $newBanner= ServiceProviderBanner::create([
                'name' => $imageName,
                'state' => 0,
                'service_provider_id' => $this->serviceProviderID,
            ]);
        }

        $message=trans( 'RestaurantControlPanel.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->serviceProviderBanner=null;

    }

    public function RemoveCurrentServiceProviderBanner()
    {
        $this->serviceProviderBanner=null;
    }
    public function deleteCall($id){
        $this->bannerId=$id;
        $this->openToast();
//        dd($this->bannerId);
    }
    public function closeToast()
    {
        $this->openDeleteToast=false;

    }
    public function openToast()
    {
        $this->openDeleteToast=true;

    }
    public function deleteAction()
    {
        $this->banner=ServiceProviderBanner::find($this->bannerId);
//        dd($this->bannerId,$this->banner);
        $deleteFromStorage=null;
        if($this->user->role_id==9)
        {
            $deleteFromStorage=Storage::delete('public/images/sliders/restaurant/'.$this->banner->name);

        }
        elseif ($this->user->role_id==10)
        {
            $deleteFromStorage=Storage::delete('public/images/sliders/vendor/'.$this->banner->name);

        }
        if($deleteFromStorage)
        {
            $this->banner->delete();
            $this->banner=null;
            $message=trans( 'RestaurantControlPanel.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction2');
        }
        $deleteFromStorage=null;
        $this->banner=null;
        $this->bannerId=null;
        $this->closeToast();
    }
    public function render()
    {
        $banners=ServiceProviderBanner::where('service_provider_id',$this->serviceProviderID)
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.template.control-panel.banners.banner-component',compact('banners'));
    }
}
