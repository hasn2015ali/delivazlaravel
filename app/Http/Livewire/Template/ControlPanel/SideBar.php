<?php

namespace App\Http\Livewire\Template\ControlPanel;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SideBar extends Component
{
    public $active;
    public $user ;

    public function mount()
    {
        $this->user=Auth::user();
    }
    public function render()
    {
        return view('livewire.template.control-panel.side-bar');
    }
}
