<?php

namespace App\Http\Livewire\Country;

use  App\Models\Language;
use  App\Models\CountryTranslation;
use Livewire\Component;
use Livewire\WithPagination;

class CountriesTranslate extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $country;
    public $lang_code;
    public $lang;
    public $name;
    public $langs;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $transId;
    public $idDelete;
    public $countryEn;

    public function messages()
    {
        return [
            'trans.required' => trans( 'country.validate' ),
            'selectedLang.required' => trans( 'country.validate_selectedLang' ),
        ];
    }


    public function submit()
    {
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',
        ]);
        $lang=Language::find($this->selectedLang);
        $newtrans=CountryTranslation::create([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'code_lang' =>$lang->code,
            'country_id' => $this->country->id,
        ]);
//        $this->translations->prepend($newtrans);


        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();


        $this->emit('newTransAdded');
    }
    public function edit($trans){
//        dd($trans);
        $this->trans=$trans['name'];
        $this->selectedLang=$trans['lang']['id'];
        $this->selectedLangName=$trans['lang']['name'];
        $this->transId=$trans['id'];
    }

    public function update($id){
//        dd($id);
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',
        ]);
        $lang=Language::find($this->selectedLang);
        $trans= CountryTranslation::find($id);
        $trans->update([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'country_id' => $this->country->id,
            'code_lang' =>$lang->code,
        ]);

        session()->flash('message',trans( 'masterControl.updated_successfully' ) );
        $this->restFilde();
        $this->emit('transCountryUp');
    }

    public function restFilde(){
        $this->trans="";
        $this->selectedLang="";
    }


    public function mount(){
//   dd($country);
//
//        $this->lang_code=session()->get('lang');
////        dd($this->lang_code);
//
//        $this->lang=Language::all()->where( 'code','==',$this->lang_code )->first()->name;

//        $this->translations=CountryTranslation::with('lang')->paginate(2);

//        $this->translations=CountryTranslation::with('lang')->orderBy('id','asc')->get();
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->countryEn=CountryTranslation::where('country_id',$this->country->id)
            ->where('code_lang','en')
            ->first();

    }

    public function deleteCall($id){
//        dd($id);
        $this->idDelete=$id;

    }
    public function deleteAction(){
//dd($id);
       $trans=CountryTranslation::findOrfail($this->idDelete);
       $trans->delete();
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }
    public function render()
    {

        $translations=CountryTranslation::where('country_id',$this->country->id)
            ->where('code_lang','!=','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);

        return view('livewire.country.countries-translate', compact('translations'));

    }
}
