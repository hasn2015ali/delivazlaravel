<?php

namespace App\Http\Livewire\Country;

use App\Models\Country;
use App\Models\Taxs;
use Livewire\Component;

class CountryTaxesComponent extends Component
{
    public  $counytryID;
    public $country;
    public $idDelete;
    public $selectedTaxes=[];
    public $allTaxes;



    public function resetValues()
    {
        $this->selectedTaxes=[];
    }
    public function update()
    {
        $this->validate([
            'selectedTaxes' => 'required',
        ]);
        $country= Country::find($this->counytryID);
        $country->taxs()->detach();
        $country->taxs()->attach($this->selectedTaxes);
        $country=null;
        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('newTaxAdded');


    }
    public function callDelete($id){
        $this->idDelete=$id;
    }
    public function deleteAction(){
        $country= Country::find($this->counytryID);
        $country->taxs()->wherePivot('tax_id', $this->idDelete)->detach();
        $country=null;
        $this->idDelete=null;
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }
    public function mount()
    {
        $this->allTaxes=Taxs::all();
        $this->country=Country::with('translation')->find($this->counytryID);
    }
    public function render()
    {
        $taxes=$this->country->taxs()->paginate(10);
//        dd($taxes);
        return view('livewire.country.country-taxes-component',compact('taxes'));
    }
}
