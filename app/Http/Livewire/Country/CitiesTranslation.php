<?php

namespace App\Http\Livewire\Country;

use App\Models\CityTranslation;
use App\Models\CountryTranslation;
use App\Models\Language;
use Livewire\Component;
use Livewire\WithPagination;

class CitiesTranslation extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $city;
    public $lang_code;
    public $lang;
    public $name;
    public $langs;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $transId;
    public $idDelete;
    public $country;
    public $cityEn;
    public $countryId;
    public function messages()
    {
        return [
            'trans.required' => trans( 'city.validate' ),
            'selectedLang.required' => trans( 'country.validate_selectedLang' ),
        ];
    }


    public function submit()
    {
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',
        ]);
        $lang=Language::find($this->selectedLang);
        $newtrans=CityTranslation::create([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'city_id' => $this->city->id,
            'code_lang' =>$lang->code,
            'country_id' => $this->countryId,

        ]);
//        $this->translations->prepend($newtrans);


        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();
        $this->emit('newTransAdded');
    }
    public function edit($trans){
//        dd($trans);
        $this->trans=$trans['name'];
        $this->selectedLang=$trans['lang']['id'];
        $this->selectedLangName=$trans['lang']['name'];
        $this->transId=$trans['id'];
    }

    public function update($id){
//        dd($id);
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',
        ]);
        $lang=Language::find($this->selectedLang);
        $trans= CityTranslation::find($id);
        $trans->update([
            'name' => $this->trans,
            'lang_id' => $this->selectedLang,
            'city_id' => $this->city->id,
            'code_lang' =>$lang->code,
            'country_id' => $this->countryId,
        ]);

        session()->flash('message',trans( 'masterControl.updated_successfully' ) );
        $this->restFilde();
        $this->emit('transCountryUp');
    }

    public function restFilde(){
        $this->trans="";
        $this->selectedLang="";
    }


    public function mount(){
//dd($this->country);
        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');
        $this->cityEn=CityTranslation::where('city_id',$this->city->id)
            ->where('code_lang','en')
            ->first();
        $this->countryId=$this->cityEn->country_id;
//        dd( $this->countryId);
    }

    public function deleteCall($id){
//        dd($id);
        $this->idDelete=$id;

    }
    public function deleteAction(){
//dd($id);
        $trans=CityTranslation::findOrfail($this->idDelete);
        $trans->delete();
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }
    public function render()
    {
        $translations=CityTranslation::where('city_id',$this->city->id)
            ->where('code_lang','!=','en')
            ->orderBy( 'id', 'desc' )
            ->paginate(10);
        return view('livewire.country.cities-translation',compact('translations'));
    }
}
