<?php

namespace App\Http\Livewire\Country;

use App\Models\Country ;
use App\Models\CountryTranslation;
use App\Models\Language;
use App\Traits\WithLanguegesTrait;
use Livewire\Component;
use Livewire\WithPagination;
//use Illuminate\Support\Facades\DB;
class Countries extends Component
{
    use WithPagination ,WithLanguegesTrait ;
    protected $paginationTheme = 'bootstrap';
    public $name;
    public $code;
    public $idDelete;
    public $translations;
    public $languegeCount;

//    public $countries;



    public function messages()
    {
        return [
            'name.required' => trans( 'country.validate' ),
        ];
    }

    public function mount(){
//        $this->countries=country::orderBy( 'id', 'desc' )->get();
//        $this->countries=country::all();
//        $c=$this->getAllLanguegesCount();
//        dd($c);
        $this->languegeCount=$this->getAllLanguegesCount();

    }
    public function submit()
    {
        $this->validate([
            'name' => 'required',
        ]);
        $newCountry=country::create([
            'code' => $this->code,
        ]);
        $en_lang=Language::where('code','en')->first();
        CountryTranslation::create([
            'name' => $this->name,
            'lang_id' => $en_lang->id,
            'code_lang' => "en",
            'country_id' => $newCountry->id,
        ]);
//        $this->countries->prepend($newCountry);

//        $message1= trans( 'masterControl.successfully_Added' );
        session()->flash('message', trans( 'masterControl.successfully_Added' ));
        $this->restFilde();
//        $this->emit('newCountryAdded',$message1);
        $this->emit('newCountryAdded');

    }

    public function edit($country){
//dd($country['country']['code']);
        $this->name=$country['name'];
        $this->code=$country['country']['code'];
        $this->idDelete=$country['id'];
    }
    public function update(){
        $this->validate([
            'name' => 'required',
        ]);
        $country= CountryTranslation::find($this->idDelete);
        $country->update([
            'name' => $this->name,
        ]);
        $country->country->update([
            'code' => $this->code,
        ]);

        session()->flash('message',trans( 'masterControl.updated_successfully' ) );
        $this->restFilde();
        $this->emit('CountryUp');
    }
    public function callDelete($id){
        $this->idDelete=$id;
    }
    public function deleteAction(){
        $country_en= CountryTranslation::find($this->idDelete);
        $country_en->country->delete();
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }

    public function restFilde(){
        $this->name="";
        $this->code="";
    }
    public function showTranslation($id){
     $this->translations=CountryTranslation::where('country_id',$id)->get('code_lang');
//        $translations=count($translations);
//        $this->emit('showTranslations');
    }

    public function render()
    {
        $countries=CountryTranslation::with('country')
            ->where('code_lang','en')
            ->orderBy('name','asc')
//            ->get();
            ->paginate(10);
//        dd($countries);
        return view('livewire.country.countries',compact('countries'));
    }
}
