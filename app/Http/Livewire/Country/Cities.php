<?php

namespace App\Http\Livewire\Country;

use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\Language;
use App\Traits\WithLanguegesTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
class Cities extends Component
{
    use WithPagination ,WithLanguegesTrait,WithFileUploads;
    protected $paginationTheme = 'bootstrap';
    public $name;
    public $countryId;
    public $country;
    public $city;
    public $cityId;
    public $translations;
    public $languegeCount;

    public $phone1;
    public $phone2;
    public $phone3;
    public $cityUp;

    public $icon1;
    public $icon2;
    public $icon3;

    public $oldIcon1;
    public $oldIcon2;
    public $oldIcon3;


    public function messages()
    {
        return [
            'name.required' => trans( 'city.validate' ),
            'phone1.required' => trans( 'city.validatePhone1' ),
        ];
    }
    public function switchState($cityID)
    {
//        dd($cityID);
        $city=City::find($cityID);
        if($city->state==0)
        {
            $city->state=1;
            $city->save();
        }
        elseif ($city->state==1)
        {
            $city->state=0;
            $city->save();
        }

    }
    public function editPhone($city)
    {
//     dd($city);
        $this->cityUp=$city;
        $city['phone1']==0? $this->phone1=null: $this->phone1=$city['phone1'];
        $city['phone2']==0? $this->phone2=null: $this->phone2=$city['phone2'];
        $city['phone3']==0? $this->phone3=null: $this->phone3=$city['phone3'];
        $this->oldIcon1=$city['icon1'];
        $this->oldIcon2=$city['icon2'];
        $this->oldIcon3=$city['icon3'];

    }

    public function submitPhone()
    {
//             dd( $this->cityUp);
        $city=City::find($this->cityUp['id']);

        $this->validate([
            'phone1' => 'required',
        ]);

        if($this->icon1)
        {
            Storage::delete('public/images/phone/city/'.$city['icon1']);
            $imageName=time().rand(0,1000).".".$this->icon1->extension();
            $this->icon1->storeAs('public/images/phone/city', $imageName);
            $city->icon1=$imageName;
            $this->oldIcon1=$imageName;
            $this->icon1=null;

        }
        if($this->icon2)
        {
            Storage::delete('public/images/phone/city/'.$city['icon2']);
            $imageName=time().rand(0,1000).".".$this->icon2->extension();
            $this->icon2->storeAs('public/images/phone/city', $imageName);
            $city->icon2=$imageName;
            $this->oldIcon2=$imageName;
            $this->icon2=null;

        }
        if($this->icon3)
        {
            Storage::delete('public/images/phone/city/'.$city['icon3']);
            $imageName=time().rand(0,1000).".".$this->icon3->extension();
            $this->icon3->storeAs('public/images/phone/city', $imageName);
            $city->icon3=$imageName;
            $this->oldIcon3=$imageName;
            $this->icon3=null;

        }
        $city->phone1=$this->phone1;
        if($this->phone2!=null)
        {
            $city->phone2=$this->phone2;
        }
        if($this->phone3!=null)
        {
            $city->phone3=$this->phone3;
        }
        $city->save();

        $message=trans( 'masterControl.updated_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
//        $this->emit('newMealFilterAdded');

//      dd( $this->phone1, $this->phone2, $this->phone3);
    }
    public function mount(){

//        dd($this->countryId);
        $this->country=Country::with('cities','translation')->find($this->countryId);
        $this->languegeCount=$this->getAllLanguegesCount();
//        dd($this->country);
    }
    public function submit(){
        $this->validate([
            'name' => 'required',
        ]);
        $en_lang=Language::where('code','en')->first();
       $city= City::create([
            'country_id' => $this->countryId,
        ]);
       $city_trans=CityTranslation::create([
           'name' => $this->name,
           'city_id' => $city->id,
           'lang_id' => $en_lang->id,
           'code_lang' => "en",
           'country_id' => $this->countryId,
       ]);

        session()->flash('message',trans( 'masterControl.successfully_Added' ) );
        $this->restFilde();
        $this->emit('newCityAdded');
    }
    public function restFilde(){
        $this->name="";
    }
    public  function edit($city){
        $this->city=$city;
        $this->name=$city['name'];
    }

    public function update(){
        $this->validate([
            'name' => 'required',
        ]);
        $city=CityTranslation::find($this->city['id']);
        $city->update(['name'=>$this->name]);
        session()->flash('message',trans( 'masterControl.updated_successfully' ) );
        $this->restFilde();
        $this->emit('cityUp');
    }
    public function callDelete($id){
        $this->cityId=$id;
    }
    public function deleteAction(){
        $city=City::find($this->cityId);
        $city->delete();
        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
    }
    public function showTranslation($id){
        $this->translations=CityTranslation::where('city_id',$id)->get('code_lang');

    }
    public function render()
    {
//        $cities=City::with('translation')->where('country_id',$this->countryId)
//            ->orderBy( 'id', 'desc' )
//            ->paginate(10);
        $cities=CityTranslation::with('city')->where('country_id',$this->countryId)
            ->where('code_lang','en')
            ->orderBy( 'name','asc')
            ->paginate(10);
        return view('livewire.country.cities',compact('cities'));
    }
}
