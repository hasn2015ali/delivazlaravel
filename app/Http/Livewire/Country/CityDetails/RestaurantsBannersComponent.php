<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\CityServiceProviders;
use Livewire\Component;

class RestaurantsBannersComponent extends Component
{
    public $cityID;
    public $restaurantsBanners;

    public function mount()
    {
        $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
            ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->get();
    }
    public function render()
    {
        return view('livewire.country.city-details.restaurants-banners-component');
    }
}
