<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\CityShapcontent;
use App\Models\cityShape;
use App\Models\CityShapTranslation;
use App\Models\filterTranslate;
use App\Models\Language;
use App\Models\ServiceProvider;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;
class ShapeComponent extends Component
{
    use  WithFileUploads;
    public $cityID;
    //shape 1
    public $photo;
    public $photoOld;
    public $content=[];
    //shape 1
    public $photo1;
    public $photoOld1;
    public $content1=[];
    public $shape1;
    public $state1=0;
    public $address1;
    public $url1;
    public $restaurants1=[];
    public $urlShow1=false;
    public $contentShow1=false;
    public $restaurantShow1=false;
    public $anyThingShow1=false;

    //shape 2
    public $photo2;
    public $photoOld2;
    public $content2=[];
    public $shape2;
    public $state2=0;
    public $address2;
    public $url2;
    public $restaurants2=[];
    public $urlShow2=false;
    public $contentShow2=false;
    public $restaurantShow2=false;
    public $anyThingShow2=false;

    //shape 3
    public $photo3;
    public $photoOld3;
    public $content3=[];
    public $shape3;
    public $state3=0;
    public $address3;
    public $url3;
    public $restaurants3=[];
    public $urlShow3=false;
    public $contentShow3=false;
    public $restaurantShow3=false;
    public $anyThingShow3=false;


    //shape 4
    public $photo4;
    public $photoOld4;
    public $content4=[];
    public $shape4;
    public $state4=0;
    public $address4;
    public $url4;
    public $restaurants4=[];
    public $urlShow4=false;
    public $contentShow4=false;
    public $restaurantShow4=false;
    public $anyThingShow4=false;

    //shape 5
    public $photo5;
    public $photoOld5;
    public $content5=[];
    public $shape5;
    public $state5=0;
    public $address5;
    public $url5;
    public $restaurants5=[];
    public $urlShow5=false;
    public $contentShow5=false;
    public $restaurantShow5=false;
    public $anyThingShow5=false;

    //shape 6
    public $photo6;
    public $photoOld6;
    public $content6=[];
    public $shape6;
    public $state6=0;
    public $address6;
    public $url6;
    public $restaurants6=[];
    public $urlShow6=false;
    public $contentShow6=false;
    public $restaurantShow6=false;
    public $anyThingShow6=false;

    //shape 7
    public $photo7;
    public $photoOld7;
    public $content7=[];
    public $shape7;
    public $state7=0;
    public $address7;
    public $url7;
    public $restaurants7=[];
    public $urlShow7=false;
    public $contentShow7=false;
    public $restaurantShow7=false;
    public $anyThingShow7=false;


    //shape 8
    public $photo8;
    public $photoOld8;
    public $content8=[];
    public $shape8;
    public $state8=0;
    public $address8;
    public $url8;
    public $restaurants8=[];
    public $urlShow8=false;
    public $contentShow8=false;
    public $restaurantShow8=false;
    public $anyThingShow8=false;


    //shape 9
    public $photo9;
    public $photoOld9;
    public $content9=[];
    public $shape9;
    public $state9=0;
    public $address9;
    public $url9;
    public $restaurants9=[];
    public $urlShow9=false;
    public $contentShow9=false;
    public $restaurantShow9=false;
    public $anyThingShow9=false;


    //shape 10
    public $photo10;
    public $photoOld10;
    public $content10=[];
    public $shape10;
    public $state10=0;
    public $address10;
    public $url10;
    public $restaurants10=[];
    public $urlShow10=false;
    public $contentShow10=false;
    public $restaurantShow10=false;
    public $anyThingShow10=false;


    //shape 11
    public $photo11;
    public $photoOld11;
    public $content11=[];
    public $shape11;
    public $state11=0;
    public $address11;
    public $url11;
    public $restaurants11=[];
    public $urlShow11=false;
    public $contentShow11=false;
    public $restaurantShow11=false;
    public $anyThingShow11=false;


    //shape 12
    public $photo12;
    public $photoOld12;
    public $content12=[];
    public $shape12;
    public $state12=0;
    public $address12;
    public $url12;
    public $restaurants12=[];
    public $urlShow12=false;
    public $contentShow12=false;
    public $restaurantShow12=false;
    public $anyThingShow12=false;


    public $allFilters;
    public $allRestaurants=[];
    public $imageName;
    public $shape;

    public $i;
    public $urlShow=false;
    public $anyThingShow=false;
    public $restaurantShow=false;
    public $contentShow=false;

    public $oldContent=[];
    public $oldURL=null;
    public $oldType=null;

    public $k;

    public $shapeTrans;
    public $transId;
    public $langs;
    public $trans;
    public $selectedLang;
    public $selectedLangName;
    public $allTransForShape;

//    public $disabled='disabled-btn';

//    public $selectedRestaurants=[];

    public function hydrate()
    {
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function messages()
    {

//        restaurants content url
        return [
            'address1.required' => trans( 'city.valAddress' ),
            'address3.required' => trans( 'city.valAddress' ),
            'address4.required' => trans( 'city.valAddress' ),
            'address5.required' => trans( 'city.valAddress' ),
            'address6.required' => trans( 'city.valAddress' ),
            'address7.required' => trans( 'city.valAddress' ),
            'address8.required' => trans( 'city.valAddress' ),
            'address9.required' => trans( 'city.valAddress' ),
            'address10.required' => trans( 'city.valAddress' ),
            'address11.required' => trans( 'city.valAddress' ),
            'address12.required' => trans( 'city.valAddress' ),


            'restaurants1.required' => trans( 'city.valRestaurants' ),
            'restaurants3.required' => trans( 'city.valRestaurants' ),
            'restaurants4.required' => trans( 'city.valRestaurants' ),
            'restaurants5.required' => trans( 'city.valRestaurants' ),
            'restaurants6.required' => trans( 'city.valRestaurants' ),
            'restaurants7.required' => trans( 'city.valRestaurants' ),
            'restaurants8.required' => trans( 'city.valRestaurants' ),
            'restaurants9.required' => trans( 'city.valRestaurants' ),
            'restaurants10.required' => trans( 'city.valRestaurants' ),
            'restaurants11.required' => trans( 'city.valRestaurants' ),
            'restaurants12.required' => trans( 'city.valRestaurants' ),

            'content1.required' => trans( 'city.valContent' ),
            'content3.required' => trans( 'city.valContent' ),
            'content4.required' => trans( 'city.valContent' ),
            'content5.required' => trans( 'city.valContent' ),
            'content6.required' => trans( 'city.valContent' ),
            'content7.required' => trans( 'city.valContent' ),
            'content8.required' => trans( 'city.valContent' ),
            'content9.required' => trans( 'city.valContent' ),
            'content10.required' => trans( 'city.valContent' ),
            'content11.required' => trans( 'city.valContent' ),
            'content12.required' => trans( 'city.valContent' ),

            'url1.required' => trans( 'city.valUrl' ),
            'url3.required' => trans( 'city.valUrl' ),
            'url4.required' => trans( 'city.valUrl' ),
            'url5.required' => trans( 'city.valUrl' ),
            'url6.required' => trans( 'city.valUrl' ),
            'url7.required' => trans( 'city.valUrl' ),
            'url8.required' => trans( 'city.valUrl' ),
            'url9.required' => trans( 'city.valUrl' ),
            'url10.required' => trans( 'city.valUrl' ),
            'url11.required' => trans( 'city.valUrl' ),
            'url12.required' => trans( 'city.valUrl' ),

            'trans.required' => trans( 'filter.validateTrans' ),
            'selectedLang.required' => trans( 'filter.validate_selectedLang' ),
        ];
    }

    public function setShapeNumber($i)
    {
        $this->i=$i;
        $this->{"photo".$i} = null;
        $this->{"content".$i} = [];
        $this->{"restaurants".$i} = [];
        $this->{"url".$i} = null;
        $this->{"contentShow".$i} = false;
        $this->{"urlShow".$i} = false;
        $this->{"restaurantShow".$i} = false;
        $this->{"anyThingShow".$i} = false;
    }

    public function ShowOldContent($shapeNumber)
    {
        $this->oldType=null;
        $this->oldContent=[];
        $this->oldURL=null;
        $this->k=$shapeNumber;
        $shape=cityShape::where('name',$shapeNumber)->where('city_id',$this->cityID)->first();
//        dd($shape->url);
        if(isset($shape->content))
        {
            if(count($shape->content)!=0)
            {
                if($shape->content[0]['type']=='filter')
                {
                    $this->oldType="Filters";

                    foreach ($shape->content as $filter)
                    {
//                        dd($filter->filter->translation);
                            $filtersSelected=filterTranslate::where('filter_id',$filter->content_id)
                                ->where('code_lang',App::getLocale())->first();

                            array_push($this->oldContent,$filtersSelected);

                    }
//                    dd($this->oldContent);

                }elseif ($shape->content[0]['type']=='restaurant')
                {
                    $this->oldType="Restaurants";

                    foreach ($shape->content as $filter)
                    {
//                        dd($filter->filter->translation);
                        $filtersSelected=ServiceProvider::where('id',$filter->content_id)->first();

                        array_push($this->oldContent,$filtersSelected);

                    }
                }
            }elseif ($shape->url=="anything")
            {
                $this->oldType="AnyThing";
//                dd('kk');
            }
            elseif (isset($shape->url) and $shape->url!="anything")
            {
                $this->oldType="Link";
//                dd('kk');
            }

        }



//        dd($shape);
    }

    public function changeFilterState($shape)
    {
//        dd('fff');
        for ($i=1;  $i<13 ;$i++) {


            if ($shape == $i) {
//                dd($shape,$i);

//                $contents=$this->{"content".$i};

                $this->{"contentShow".$i} = true;
                $this->{"urlShow".$i} = false;
                $this->{"restaurantShow".$i} = false;
                $this->{"anyThingShow".$i} = false;

                $this->{"restaurants".$i} = [];
                $this->{"url".$i} = null;
//            dd( $this->contentShow1);

//                dd($this->{"contentShow".$i}, $this->{"urlShow".$i}, $this->{"restaurantShow".$i}
//                ,$this->{"anyThingShow".$i}, $this->{"restaurants".$i}, $this->{"url".$i});

                break;
            }
        }
    }

    public function changeRestaurantState($shape)
    {
        for ($i=1; $i<13; $i++) {


            if ($shape == $i)
            {
                $this->{"contentShow".$i} = false;
                $this->{"urlShow".$i} = false;
                $this->{"restaurantShow".$i} = true;
                $this->{"anyThingShow".$i} = false;

                $this->{"content".$i} = [];
                $this->{"url".$i} = null;


            break;
            }
        }
    }

    public function changeAnythingState($shape)
    {
        for ($i=1; $i<13; $i++) {


            if ($shape == $i)
            {
                $this->{"contentShow".$i} = false;
                $this->{"urlShow".$i} = false;
                $this->{"restaurantShow".$i} = false;
                $this->{"anyThingShow".$i} = true;

                $this->{"content".$i} = [];
                $this->{"restaurants".$i} = [];
                $this->{"url".$i} = null;


                break;
            }
        }
    }


    public function changeURLState($shape)
    {
        for ($i=1; $i<13; $i++) {


            if ($shape == $i)
            {
                $this->{"contentShow".$i} = false;
                $this->{"urlShow".$i} = true;
                $this->{"restaurantShow".$i} = false;
                $this->{"anyThingShow".$i} = false;

                $this->{"content".$i} = [];
                $this->{"restaurants".$i} = [];
//                $this->{"url".$i} = null;


                break;
            }
        }
    }

    public function submitContent($shape)
    {
//        dd($shape);
        for ($i=1; $i<13;$i++)
        {

            if($shape==$i)
            {
                $this->validate([
                    'content'.$i => 'required',
                ]);

                $contents=$this->{"content".$i};
                $url=$this->{"url".$i};
                $restaurants=$this->{"restaurants".$i};
                $shape=cityShape::with('content')->where('name',$i)->where('city_id',$this->cityID)->first();

                if(isset($shape))
                {
                    if(count($shape->content)!=0 or isset($shape->url))
                    {
                        if(count($shape->content)!=0)
                        {
                            DB::table('city_shapcontents')->where('shape_id', $shape->id)->delete();

                        }
                        if(isset($shape->url))
                        {

                            $shape->url=null;
                            $shape->save();
                        }

                        foreach ($contents as $filterID)
                        {
                            CityShapcontent::create([
                                'shape_id' => $shape->id,
                                'content_id'=>$filterID,
                                'type' => "filter",
                            ]);
                        }

                        $message=trans( 'masterControl.updated_successfully' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
//                  dd('yes');
                    }
                    else
                    {
                        foreach ($contents as $filterID)
                        {
                            CityShapcontent::create([
                                'shape_id' => $shape->id,
                                'content_id'=>$filterID,
                                'type' => "filter",
                            ]);
                        }
//                  dd('no');

                        $message=trans( 'masterControl.successfully_Added' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
                    }
                }
                else
                {
                    $m=trans( 'city.ShapeNot' );
                    $this->emit('alert',['icon'=>'error','title'=>$m]);
                }

//                dd($shape,$content,$url,$restaurants);

                break;
//                dd( $this->content1,$this->url1,$this->restaurants1);
//
            }
        }

    }


    public function submitURL($shape)
    {
        for ($i=1;  $i<13; $i++)
        {
            if($shape==$i)
            {
                $this->validate([
                    'url'.$i => 'required',
                ]);


                $content=$this->{"content".$i};
                $url=$this->{"url".$i};
                $restaurants=$this->{"restaurants".$i};

                $shape=cityShape::with('content')->where('name',$i)->where('city_id',$this->cityID)->first();

                if(isset($shape))
                {
                    if(count($shape->content)!=0 or isset($shape->url) )
                    {
                        if(count($shape->content)!=0)
                        {
                            DB::table('city_shapcontents')->where('shape_id', $shape->id)->delete();

                        }
                        $shape->url=$url;
                        $shape->save();
                        $message=trans( 'masterControl.updated_successfully' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
//                  dd('yes');
                    }
                    else
                    {
                        $shape->url=$url;
                        $shape->save();

                        $message=trans( 'masterControl.successfully_Added' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
                    }
                }
                else
                {
                    $m=trans( 'city.ShapeNot' );
                    $this->emit('alert',['icon'=>'error','title'=>$m]);
                }
                break;

//                dd($shape,$content,$url,$restaurants);

            }
        }
    }
    public function submitAnything($shape)
    {
        for ($i=1;  $i<13; $i++)
        {

            if($shape==$i)
            {
                $content=$this->{"content".$i};
//                $url=$this->{"url".$i};
                $url="anything";

                $restaurants=$this->{"restaurants".$i};
                $shape=cityShape::with('content')->where('name',$i)->where('city_id',$this->cityID)->first();

                if(isset($shape))
                {
                    if(count($shape->content)!=0 or isset($shape->url) )
                    {
                        if(count($shape->content)!=0)
                        {
                            DB::table('city_shapcontents')->where('shape_id', $shape->id)->delete();

                        }
                        $shape->url=$url;
                        $shape->save();
                        $message=trans( 'masterControl.updated_successfully' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
                    }
                    else
                    {
                        $shape->url=$url;
                        $shape->save();

                        $message=trans( 'masterControl.successfully_Added' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
                    }
                }
                else
                {
                    $m=trans( 'city.ShapeNot' );
                    $this->emit('alert',['icon'=>'error','title'=>$m]);
                }
                break;
//                dd($shape,$content,$url,$restaurants);

            }
        }
    }

    public function submitRestaurant($shape)
    {
        for ($i=1;  $i<13;$i++)
        {
            if($shape==$i)
            {
                $contents=$this->{"content".$i};
                $url=$this->{"url".$i};
                $restaurants=$this->{"restaurants".$i};
                $this->validate([
                    'restaurants'.$i => 'required',
                ]);
                $shape=cityShape::with('content')->where('name',$i)->where('city_id',$this->cityID)->first();

                if(isset($shape))
                {
                    if(count($shape->content)!=0 or isset($shape->url))
                    {
                        if(count($shape->content)!=0)
                        {
                            DB::table('city_shapcontents')->where('shape_id', $shape->id)->delete();

                        }
                        if(isset($shape->url))
                        {

                            $shape->url=null;
                            $shape->save();
                        }
                        foreach ($restaurants as $restaurantID)
                        {
                            CityShapcontent::create([
                                'shape_id' => $shape->id,
                                'content_id'=>$restaurantID,
                                'type' => "restaurant",
                            ]);
                        }

                        $message=trans( 'masterControl.updated_successfully' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
//                  dd('yes');
                    }
                    else
                    {
                        foreach ($restaurants as $restaurantID)
                        {
                            CityShapcontent::create([
                                'shape_id' => $shape->id,
                                'content_id'=>$restaurantID,
                                'type' => "restaurant",
                            ]);
                        }
//                  dd('no');

                        $message=trans( 'masterControl.successfully_Added' );
                        $this->emit('alert',['icon'=>'success','title'=>$message]);
                    }
                }
                else
                {
                    $m=trans( 'city.ShapeNot' );
                    $this->emit('alert',['icon'=>'error','title'=>$m]);
                }
                break;
//                dd($shape,$content,$url,$restaurants);

            }
        }
    }



    public function mount()
    {
        $this->allFilters=filterTranslate::where('code_lang',App::getLocale())->get();
        $restaurants= DB::table('users')
            ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
            ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
            ->where('role_id', 9)
            ->where('city_id',$this->cityID)
            ->get();
        $this->allRestaurants = array_map(function($item) {
            return (array)$item;
        }, $restaurants->toArray());


        for ($i=1;  $i<13; $i++)
        {
            $this->{"shape".$i}=cityShape::with('translation','content')
                ->where('name',$i)->where('city_id',$this->cityID)->first();

            if($this->{"shape".$i})
            {
//                dd('uu');
                $shape=$this->{"shape".$i};
//                $content=$this->{"photoOld".$i};

                $this->{"photoOld".$i}=$shape->icon;
                $this->{"state".$i}=$shape->state;
                $this->{"address".$i}=$shape->translation->where('code_lang','en')->first()->trans;

//                if(count($shape->content)!=0)
//                {
////                    dd($shape->content[0]['type']);
//                    if($shape->content[0]['type']=='filter')
//                    {
//
//                        foreach ($shape->content as $filter)
//                        {
//                            dd($filter->filter->translation);
////                            $filtersSelected=filterTranslate::where('filter_id',$filter->content_id)
////                                ->where('code_lang',App::getLocale())->first();
//                        }
//
//                    }
//                }
            }
        }


        $this->langs=Language::orderBy('id','asc')->get()->where('code','!=','en');


    }


    public function checkAddORUpdate($shapeC,$cityID,$photo)
    {
       $this->shape=cityShape::where('name',$shapeC)->where('city_id',$cityID)->first();
        if(isset($this->shape))
        {
            if($photo)
            {
//                dd($shape);
                if($this->shape->icon=="icon")
                {
                    $this->imageName=time().".".$photo->extension();
                    $photo->storeAs('public/images/shape', $this->imageName);
                }
                else
                {
                    Storage::delete('public/images/shape/'.$this->shape->icon);
                    $this->imageName=time().".".$photo->extension();
                    $photo->storeAs('public/images/shape', $this->imageName);
                }

            }
            else
                {
                $this->imageName=$this->shape->icon;

               }

            return 1;
        }
        else
            {
                if($photo)
                {
//                dd('jjj',$photo);
                    $this->imageName=time().".".$photo->extension();
                    $photo->storeAs('public/images/shape', $this->imageName);
                }
                elseif(!isset($photo))
                {
//                dd('kkk');
                    $this->imageName="icon";
                }

            return 0;
             }

    }
    public function submitDetails($shape)
    {
//        dd('jj');
        for ($i=1;  $i<13 ;$i++)
        {
//            dd($j);
//            $i=$j-1;
            if($shape==$i)
            {
                $this->validate([
                    'address'.$i => 'required',
                ]);
                $photo=$this->{"photo".$i};
                $address=$this->{"address".$i};
//                dd($shape,$photo,$address);

                if($this->checkAddORUpdate($i,$this->cityID,$photo)==0)
                {
                    $newShape= cityShape:: create([
                        'name' => $i,
                        'icon' => $this->imageName,
                        'state' => 0,
                        'city_id' =>  $this->cityID,
                    ]);

                    CityShapTranslation:: create([
                        'trans' => $address,
                        'code_lang'=>'en',
                        'shape_id' => $newShape->id,
                        'city_id' => $this->cityID,
                        'shape_number' =>  $i,
                    ]);
                    $this->{"photoOld".$i}=$this->imageName;

                    $message=trans( 'masterControl.successfully_Added' );
                    $this->emit('alert',['icon'=>'success','title'=>$message]);
                } elseif ($this->checkAddORUpdate($i,$this->cityID,$this->{"photo".$i})==1)
                {
                    $this->shape->icon=  $this->imageName;
                    $this->shape->save();
                    $shapeTrans=CityShapTranslation::where('shape_id',$this->shape->id)
                        ->where('city_id',$this->cityID) ->where('code_lang','en')->first();
                    $shapeTrans->trans=$address;
                    $shapeTrans->save();
                    $this->{"photoOld".$i}=$this->imageName;

                    $message=trans( 'masterControl.updated_successfully' );
                    $this->emit('alert',['icon'=>'success','title'=>$message]);
                }



            }
        }
    }




    public function deleteTrans($transID)
    {
        $trans=CityShapTranslation::find($transID);
        $trans->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
//        dd($transID);
        $this->allTransForShape=CityShapTranslation::where('shape_number',$this->k)->where('code_lang','!=','en')
            ->where('city_id',$this->cityID)->get();
    }


    public function transShape($shapeNumber)
    {
        $this->trans=null;
        $this->selectedLang=null;


        $this->k=$shapeNumber;
        $this->shapeTrans=cityShape::with('translation','content')
            ->where('name',$shapeNumber)->where('city_id',$this->cityID)->first();
        $this->allTransForShape=CityShapTranslation::where('shape_number',$shapeNumber)->where('code_lang','!=','en')
            ->where('city_id',$this->cityID)->get();
//        dd( $this->allTransForShape);
    }
    public function submitTranslat()
    {
        $this->validate([
            'trans' => 'required',
            'selectedLang' => 'required',

        ]);
//dd($this->trans,$this->selectedLang,$this->shapeTrans->id, $this->cityID,$this->k);
        CityShapTranslation:: create([
            'trans' => $this->trans,
            'code_lang'=>$this->selectedLang,
            'shape_id' =>  $this->shapeTrans->id,
            'city_id' => $this->cityID,
            'shape_number' =>  $this->k,
        ]);
        $this->allTransForShape=CityShapTranslation::where('shape_number',$this->k)->where('code_lang','!=','en')
            ->where('city_id',$this->cityID)->get();
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);

        $this->trans=null;
        $this->selectedLang=null;
    }
    public function changeShapeState($shapeNumber)
    {
        $shape=cityShape::where('name',$shapeNumber) ->where('city_id',$this->cityID)->first();
        if($shape)
        {
            if($shape->state==0)
            {
                $shape->state=1;
                $shape->save();
            } elseif ($shape->state==1)
            {
                $shape->state=0;
                $shape->save();
            }
        }
        else
        {
            $m=trans( 'city.ShapeNot' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
        }

    }


    public function render()
    {
        return view('livewire.country.city-details.shape-component');
    }
}
