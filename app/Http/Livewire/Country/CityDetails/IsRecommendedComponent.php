<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\cityRecommended;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
class IsRecommendedComponent extends Component
{
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $cityID;
    public $recommendedRestaurant;
    public $content;
    public $photo;
    public $restaurants;
    public $vendors;
    public $recommendedRestaurants;
    public $photoVendor;
    public $recommendedVendor;
    public $contentVendor;
    public $recommendedVendors;

    public function messages()
    {

        return [
            'recommendedRestaurant.required' => trans( 'city.validateRecommendedRestaurant' ),
            'photo.required' => trans( 'city.validatePhoto' ),
            'recommendedVendor.required' => trans( 'city.validateRecommendedVendor' ),
            'photoVendor.required' => trans( 'city.validatePhoto' ),


        ];
    }
    public function submitVendor()
    {

        $this->validate([
            'recommendedVendor' => 'required',
            'photoVendor' => 'required',
        ]);

            $imageName=time().rand(1,100).".".$this->photoVendor->extension();
            $this->photoVendor->storeAs('public/images/sliders/recommended',$imageName);
            $newVendor= cityRecommended::create([
                'photo' => $imageName,
                'content' => $this->contentVendor,
                'type' => "ven",
                'service_provider_id' =>  $this->recommendedVendor,
                'city_id' => $this->cityID,
            ]);
//
        $locale=App::getLocale();
        session()->put('cityDetails', 'Recommended');
//        $message=trans( 'masterControl.successfully_Added' );
//        $this->emit('alert',['icon'=>'success','title'=>$message]);
        return redirect($locale.'/cityServiceProviders/'.$this->cityID);



    }

    public function submit()
    {

        $this->validate([
            'recommendedRestaurant' => 'required',
            'photo' => 'required',

        ]);

        $imageName=time().rand(1,100).".".$this->photo->extension();
        $this->photo->storeAs('public/images/sliders/recommended',$imageName);
        $newRestaurant= cityRecommended::create([
            'photo' => $imageName,
            'content' => $this->content,
            'type' => "res",
            'service_provider_id' =>  $this->recommendedRestaurant,
            'city_id' => $this->cityID,
        ]);
//
        $locale=App::getLocale();
        session()->put('cityDetails', 'Recommended');
//        $message=trans( 'masterControl.successfully_Added' );
//        $this->emit('alert',['icon'=>'success','title'=>$message]);
        return redirect($locale.'/cityServiceProviders/'.$this->cityID);



    }

    public function mount()
    {
        $this->recommendedRestaurants=cityRecommended::with('serviceProvider')
            ->where('city_id',$this->cityID)
            ->where('type','res')
            ->orderBy('id','asc')->get();

        $this->recommendedVendors=cityRecommended::with('serviceProvider')
            ->where('city_id',$this->cityID)
            ->where('type','ven')
            ->orderBy('id','asc')->get();

//        $this->recommendedRestaurants=cityRecommended::with('serviceProvider')->orderBy('id','asc')->get();
        $resRole=9;
        $restaurants= DB::table('users')
            ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
            ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
            ->where('users.role_id', $resRole)
            ->where('users.city_id',$this->cityID)
            ->get();
        $this->restaurants = array_map(function($item) {
            return (array)$item;
        }, $restaurants->toArray());

//        dd($this->restaurants);
        $venRole=10;
        $vendors= DB::table('users')
            ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
            ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
            ->where('users.role_id', $venRole)
            ->where('users.city_id',$this->cityID)
            ->get();
        $this->vendors = array_map(function($item) {
            return (array)$item;
        }, $vendors->toArray());
//        dd($this->vendors, $this->restaurants);

    }
    public function callDeleteVendor($id)
    {
        //        dd($this->idRecommended);
        $vendor=cityRecommended::find($id);
//        dd($vendor);
        if(Storage::delete('public/images/sliders/recommended/'.$vendor->photo))
        {
            $vendor->delete();
        }

        $locale=App::getLocale();
        session()->put('cityDetails', 'Recommended');
//        $message=trans( 'masterControl.successfully_Added' );
//        $this->emit('alert',['icon'=>'success','title'=>$message]);
        return redirect($locale.'/cityServiceProviders/'.$this->cityID);
    }
    public function callDelete($id)
    {
//        dd($this->idRecommended);
        $restaurant=cityRecommended::find($id);
//        dd($restaurant);
        if(Storage::delete('public/images/sliders/recommended/'.$restaurant->photo))
        {
            $restaurant->delete();
//            $message=trans( 'masterControl.Deleted_successfully' );
//            $this->emit('alert',['icon'=>'success','title'=>$message]);
//            $this->emit('deleteAction2');
        }

        $locale=App::getLocale();
        session()->put('cityDetails', 'Recommended');
//        $message=trans( 'masterControl.successfully_Added' );
//        $this->emit('alert',['icon'=>'success','title'=>$message]);
        return redirect($locale.'/cityServiceProviders/'.$this->cityID);
    }
    public function render()
    {

//        dd($recommendedRestaurant);
        return view('livewire.country.city-details.is-recommended-component');
    }
}
