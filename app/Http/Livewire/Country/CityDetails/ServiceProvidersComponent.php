<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\cityBanner;
use App\Models\CityServiceProviders;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class ServiceProvidersComponent extends Component
{
    use WithFileUploads , WithPagination;
    public $cityID;
    public $restaurantsBanner;
    public $photoType;
    public $providerType;
    public $restaurantsCover;
    public $shopsCover;

    public $restaurantsBanners;

    public $type;

    public $restaurants=[];
    public $vendors=[];
    public $serviceProviders=[];

    public $restaurantID;
    public $vendorID;
    public $showSelectProvider=false;

//    public $typeRestaurant="restaurant";

    public function setTypeRestaurantBanner()
    {
        $this->photoType="banner";
        $this->providerType="restaurant";
        $this->showSelectProvider=true;
        $this->serviceProviders=$this->restaurants;
    }
    public function setTypeShopCover()
    {
        $this->photoType="cover";
        $this->providerType="shop";
        $this->showSelectProvider=false;
        $this->serviceProviders=[];

    }

    public function setTypeShopsBanner()
    {
        $this->photoType="banner";
        $this->providerType="shop";
        $this->showSelectProvider=true;
        $this->serviceProviders=$this->vendors;


    }
    public function setTypeRestaurantCover()
    {
        $this->photoType="cover";
        $this->providerType="restaurant";
        $this->showSelectProvider=false;
        $this->serviceProviders=[];


    }

    public function getShopsBanner()
    {
        $this->type="shop";
        $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
            ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->get();

    }
    public function getRestaurantBanner()
    {
//        dd("ll");
        $this->type="restaurant";
        $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
            ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->get();
    }
    public function mount()
    {

        $this->restaurantsCover=CityServiceProviders::where('photo_type',"cover")
            ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->first();

        $this->shopsCover=CityServiceProviders::where('photo_type',"cover")
            ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->first();

        $resRole=9;
        $restaurants= DB::table('users')
            ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
            ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
            ->where('users.role_id', $resRole)
            ->where('users.city_id',$this->cityID)
            ->get();
        $this->restaurants = array_map(function($item) {
            return (array)$item;
        }, $restaurants->toArray());

//        dd($this->restaurants);
        $venRole=10;
        $vendors= DB::table('users')
            ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
            ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
            ->where('users.role_id', $venRole)
            ->where('users.city_id',$this->cityID)
            ->get();
        $this->vendors = array_map(function($item) {
            return (array)$item;
        }, $vendors->toArray());

//        $this->allServiceProvider=ServiceProvider::where('city_id',$this->cityID)
//            ->select('name','id','type')->get()->toArray();
//        dd($this->restaurants,$this->vendors);
    }
    public function messages()
    {

        return [
            'restaurantsBanner.required' => trans( 'city.validateRestaurantsBanner' ),
        ];
    }

    public function submitRestaurantsBanners()
    {
//        dd($this->cityID);


        $serviceProviderID=0;
        $this->validate([
            'restaurantsBanner' => 'required',
//            'restaurantID' => 'required',

        ]);
//dd($this->restaurantsBanner,$serviceProviderID);
        if($this->restaurantsBanner)
        {

            $imageName=time().rand(1,100).".".$this->restaurantsBanner->extension();
            if(  $this->photoType=="banner" and $this->providerType=="restaurant")
            {
                $this->validate([
//                    'restaurantsBanner' => 'required',
            'restaurantID' => 'required',
                ]);
                $serviceProviderID=$this->restaurantID;
                $this->restaurantsBanner->storeAs('public/images/sliders/city/restaurants',$imageName);

            }
            elseif( $this->photoType=="cover" and $this->providerType=="restaurant")
            {

                $oldCover=CityServiceProviders::where('photo_type',"cover")
                    ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->first();
                if(isset($oldCover))
                {
                    Storage::delete('public/images/cover/city/restaurants/'.$oldCover->name);
                    $oldCover->delete();
                }
                $this->restaurantsBanner->storeAs('public/images/cover/city/restaurants',$imageName);

            }
            elseif ( $this->photoType=="banner" and $this->providerType=="shop")
            {
                $serviceProviderID=$this->restaurantID;
                $this->restaurantsBanner->storeAs('public/images/sliders/city/shops',$imageName);

            }
            elseif ($this->photoType=="cover" and $this->providerType=="shop")
            {

                $oldCover=CityServiceProviders::where('photo_type',"cover")
                    ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->first();
                if(isset($oldCover))
                {
                    Storage::delete('public/images/cover/city/shops/'.$oldCover->name);
                    $oldCover->delete();
                }
                $this->restaurantsBanner->storeAs('public/images/cover/city/shops',$imageName);

            }
          CityServiceProviders::create([
                'name' => $imageName,
                'photo_type' => $this->photoType,
                'provider_type' =>  $this->providerType,
                'service_provider_id' =>$serviceProviderID,
                'state' => 0,
                'city_id' => $this->cityID,
            ]);
        }

        $locale=App::getLocale();
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        session()->put('cityDetails', 'restaurantsPage');
        return redirect($locale.'/cityServiceProviderPages/'.$this->cityID);


    }

    public function changeRestaurantsBannerState($id)
    {
//        dd('tst');
        $banner=CityServiceProviders::find($id);
        if($banner->state==0)
        {
            $banner->state=1;
            $banner->save();
            if($this->type=="restaurant")
            {
                $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
                    ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->get();
            }elseif ($this->type=="shop")
            {
                $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
                    ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->get();
            }

        }
        elseif($banner->state==1)
        {
            $banner->state=0;
            $banner->save();
            if($this->type=="restaurant")
            {
                $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
                    ->where('provider_type',"restaurant")->where( 'city_id' , $this->cityID)->get();
            }elseif ($this->type=="shop")
            {
                $this->restaurantsBanners=CityServiceProviders::where('photo_type',"banner")
                    ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->get();
            }
        }
    }

    public function deleteRestaurantsBanner($bannerID)
    {
        $banner=CityServiceProviders::find($bannerID);
        if(Storage::delete('public/images/sliders/city/restaurants/'.$banner->name))
        {
            $banner->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);

        }
        if(Storage::delete('public/images/sliders/city/shops/'.$banner->name))
        {
            $banner->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);

        }

        $locale=App::getLocale();
        session()->put('cityDetails', 'restaurantsPage');
        return redirect($locale.'/cityServiceProviderPages/'.$this->cityID);
    }

    public function render()
    {
        return view('livewire.country.city-details.service-providers-component');
    }
}
