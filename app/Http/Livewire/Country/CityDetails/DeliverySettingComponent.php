<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\CityDeliverySetting;
use App\Models\Country;
use Livewire\Component;

class DeliverySettingComponent extends Component
{
    public $cityID;
    public $selectDeliveryTax=[];
    public $country;
    public $city;
    public $countryCurrency;
    public $taxesInCountry;
    public $areaMin;
    public $areaMax;
    public $priceIn;
    public $workerRatioIN;
    public $priceOut;
    public $workerRatioOut;

    public $cityDelivary;

    public function hydrate()
    {
        $this->resetErrorBag();
//        $this->resetValidation();
    }

    public function messages()
    {
        return [
            'areaMin.required' => trans( 'city.validateAreaMin' ),
            'areaMax.required' => trans( 'city.validateAreaMax' ),
            'priceIn.required' => trans( 'city.validatePriceIn' ),
            'workerRatioIN.required' => trans( 'city.validateWorkerRatioIN' ),
            'priceOut.required' => trans( 'city.validatePriceOut' ),
            'workerRatioOut.required' => trans( 'city.validateWorkerRatioOut' ),
            'selectDeliveryTax.required' => trans( 'city.validateSelectDeliveryTax' ),

        ];
    }

    public function saveSetting()
    {
//        dd($this->priceIn,$this->priceOut,$this->workerRatioIN,$this->selectDeliveryTax);
        $this->validate([
            'areaMin' => 'required',
            'areaMax' => 'required',
            'priceIn' => 'required',
            'workerRatioIN' => 'required',
            'priceOut' => 'required',
            'workerRatioOut' => 'required',
            'selectDeliveryTax' => 'required',
        ]);

        CityDeliverySetting::updateOrCreate(['city_id' => $this->cityID],[
            'city_id' => $this->cityID,
            'price_in_area' => $this->priceIn,
            'price_out_area' => $this->priceOut,
            'worker_in_Ratio' => $this->workerRatioIN,
            'worker_out_Ratio' => $this->workerRatioOut,
            'tax_id' => $this->selectDeliveryTax['id'],
            'min_distance' => $this->areaMin,
            'max_distance' => $this->areaMax,

        ]);
        $message=trans( 'masterControl.successfully_Saved' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }

    public function sendTaxValue($tax)
    {
//        dd($tax);
        $this->selectDeliveryTax['value']=$tax['value'];
        $this->selectDeliveryTax['id']=$tax['id'];
        $this->selectDeliveryTax['type']=$tax['type'];

//        dd($this->selectedTax);
    }
    public function mount()
    {

        $this->countryCurrency=$this->country->country->currency_code;
        $this->taxesInCountry=$this->country->country->taxs->toArray();
        $this->cityDelivary=CityDeliverySetting::with('tax')
            ->where('city_id',$this->cityID)
            ->get()->toArray();
        if(count($this->cityDelivary)!=0)
        {
            $this->cityID= $this->cityDelivary[0]['city_id'] ;
            $this->priceIn=$this->cityDelivary[0]['price_in_area'] ;
            $this->priceOut=$this->cityDelivary[0]['price_out_area' ];
            $this->workerRatioIN= $this->cityDelivary[0]['worker_in_Ratio'];
            $this->workerRatioOut= $this->cityDelivary[0]['worker_out_Ratio'] ;
            $this->selectDeliveryTax=$this->cityDelivary[0]['tax'] ;
            $this->areaMin  =$this->cityDelivary[0]['min_distance'] ;
            $this->areaMax =$this->cityDelivary[0]['max_distance'] ;
//            dd($this->selectDeliveryTax);
        }


//        dd($this->taxesInCountry,$this->serviceProvider->tax_id,$this->selectedTax,count($this->selectedTax));
//        if($this->serviceProvider->tax_id!=null)
//        {
////            dd($this->serviceProvider);
//            $this->selectedTax=$this->serviceProvider->tax->toArray();
////            dd($this->selectedTax);
//        }
    }
    public function render()
    {
        return view('livewire.country.city-details.delivery-setting-component');
    }
}
