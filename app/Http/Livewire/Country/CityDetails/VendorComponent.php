<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\cityRecommended;
use Livewire\Component;

class VendorComponent extends Component
{
    public $recommendedVendors;
    public $cityID;
    public $idRecommended;
    public function mount()
    {
        $this->recommendedVendors=cityRecommended::with('serviceProvider')
            ->where('type','ven')
            ->where('city_id',$this->cityID)
            ->orderBy('id','asc')->get();

    }
    public function render()
    {
        return view('livewire.country.city-details.vendor-component');
    }
}
