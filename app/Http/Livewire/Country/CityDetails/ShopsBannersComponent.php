<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\CityServiceProviders;
use Livewire\Component;

class ShopsBannersComponent extends Component
{
    public $cityID;
    public $shopsBanners;

    public function mount()
    {
        $this->shopsBanners=CityServiceProviders::where('photo_type',"banner")
            ->where('provider_type',"shop")->where( 'city_id' , $this->cityID)->get();
    }
    public function render()
    {
        return view('livewire.country.city-details.shops-banners-component');
    }
}
