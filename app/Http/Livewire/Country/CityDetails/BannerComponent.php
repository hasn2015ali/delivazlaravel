<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\cityBanner;
use App\Models\cityShape;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class BannerComponent extends Component
{
//    public $cityID;
    use WithFileUploads , WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $cityID;
    public $cityBanner;
    public $bannerId;
    public $serviceProviders=[];
    public $serviceProviderID;
    public $type;

    public function messages()
    {

        return [
            'cityBanner.required' => trans( 'city.validateCityBanner' ),
            'serviceProviderID.required' => trans( 'city.validateServiceProviderForBanner' ),

        ];
    }

    public function updatingType($value)
    {
        //
//        dd($value);
        if($value=="restaurant")
        {
            $resRole=9;
            $serviceProviders= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
                ->where('users.role_id', $resRole)
                ->where('users.city_id',$this->cityID)
                ->get();
            $this->serviceProviders = array_map(function($item) {
                return (array)$item;
            }, $serviceProviders->toArray());
        }
        elseif ($value=="shop")
        {
            $venRole=10;
            $serviceProviders= DB::table('users')
                ->join('service_providers', 'users.id', '=', 'service_providers.user_id')
                ->select('users.id as userID','service_providers.name' ,'service_providers.name','service_providers.id as serviceProviderID')
                ->where('users.role_id', $venRole)
                ->where('users.city_id',$this->cityID)
                ->get();
            $this->serviceProviders = array_map(function($item) {
                return (array)$item;
            }, $serviceProviders->toArray());
        }
    }

    public function submit()
    {

//        dd($this->type);
        $this->validate([
            'cityBanner' => 'required',
            'serviceProviderID' => 'required',

        ]);
        if($this->cityBanner)
        {
            $imageName=time().rand(1,100).".".$this->cityBanner->extension();
            $this->cityBanner->storeAs('public/images/sliders/city',$imageName);
            $newBanner= cityBanner::create([
                'name' => $imageName,
                'state' => 0,
                'type' => $this->type,
                'city_id' => $this->cityID,
                'service_provider_id' => $this->serviceProviderID,

            ]);
        }

        $locale=App::getLocale();
        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        session()->put('cityDetails', 'Banners');
        return redirect($locale.'/cityBanners/'.$this->cityID);

    }
    public function mount()
    {


//        dd($this->serviceProviders);
    }
    public function changeBannerState($id)
    {
//        dd('tst');
        $banner=cityBanner::find($id);
        if($banner->state==0)
        {
            $banner->state=1;
            $banner->save();
        }
        elseif($banner->state==1)
        {
            $banner->state=0;
            $banner->save();
        }
    }

    public function deleteCall($id){
        $this->bannerId=$id;
    }
    public function deleteAction()
    {
//        dd("hh");
        $banner=cityBanner::find($this->bannerId);
        if(Storage::delete('public/images/sliders/city/'.$banner->name))
        {
            $banner->delete();
            $message=trans( 'masterControl.Deleted_successfully' );
            $this->emit('alert',['icon'=>'success','title'=>$message]);
            $this->emit('deleteAction2');
        }

        $locale=App::getLocale();
        session()->put('cityDetails', 'Banners');
        return redirect($locale.'/cityBanners/'.$this->cityID);
    }

    public function render()
    {
        $banners=cityBanner::where('city_id',$this->cityID)
            ->orderBy( 'id', 'desc' )
            ->paginate(2);
        return view('livewire.country.city-details.banner-component',compact('banners'));
    }
}
