<?php

namespace App\Http\Livewire\Country\CityDetails;

use App\Models\cityRecommended;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class RestaurantComponent extends Component
{
    public $recommendedRestaurants;
    public $cityID;
    public $idRecommended;
    public function mount()
    {
        $this->recommendedRestaurants=cityRecommended::with('serviceProvider')
            ->where('type','res')
            ->where('city_id',$this->cityID)
            ->orderBy('id','asc')->get();

    }


    public function render()
    {
        return view('livewire.country.city-details.restaurant-component');
    }
}
