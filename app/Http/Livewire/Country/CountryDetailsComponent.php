<?php

namespace App\Http\Livewire\Country;

use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\Language;
use App\Traits\WithLanguegesTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class CountryDetailsComponent extends Component
{
    use WithPagination ,WithLanguegesTrait,WithFileUploads;
    public $counytryID;
    public $country;
    public $currency;
    public $currencyCode;
    public $codeNumber=0;
    public $phone1=0;
    public $icon1;
    public $oldIcon1;

    public function messages()
    {
        return [
            'currency.required' => trans( 'country.validateCurrency' ),
            'currencyCode.required' => trans( 'country.validateCurrencyCode' ),

        ];
    }

    public function submit()
    {
        $this->validate([
            'currency' => 'required',
            'currencyCode' => 'required',

        ]);
        $this->codeNumber==null?$this->codeNumber=0:$this->codeNumber;
//        $this->phone1==null?$this->phone1=0:$this->phone1;
        if($this->icon1)
        {
            Storage::delete('public/images/phone/country/'.$this->country['icon1']);
            $imageName=time().rand(0,1000).".".$this->icon1->extension();
            $this->icon1->storeAs('public/images/phone/country', $imageName);
            $this->country->icon1=$imageName;
            $this->oldIcon1=$imageName;
            $this->icon1=null;

        }
//        dd($this->codeNumber);
        $this->country->currency=$this->currency;
        $this->country->currency_code=$this->currencyCode;
//        $this->country->code_number=$this->codeNumber;
        if($this->phone1)
        {
            $this->country->phone1=$this->phone1;
        }
        if($this->codeNumber)
        {
            $this->country->code_number=$this->codeNumber;
        }
        $this->country->save();
//        $this->country->update($this->validate());
        $message=trans( 'masterControl.successfully_Added' );
//        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('countryDetailsAdded');


    }

    public function setValue()
    {

        $this->country=Country::with('translation')->find($this->counytryID);
        $this->currency=$this->country->currency;
        $this->currencyCode=$this->country->currency_code;
        $this->oldIcon1= $this->country->icon1;

        $this->country->code_number==0?$this->codeNumber=null:$this->codeNumber=$this->country->code_number;
        $this->country->phone1==0?$this->phone1=null:$this->phone1=$this->country->phone1;

    }
    public function restFilde()
    {
        $this->currencyCode="";
        $this->currency="";
    }


    public function mount()
    {
        $this->country=Country::with('translation')->find($this->counytryID);
        $this->currency=$this->country->currency;
        $this->currencyCode=$this->country->currency_code;
        $this->oldIcon1= $this->country->icon1;

        $this->country->code_number==0?$this->codeNumber=null:$this->codeNumber=$this->country->code_number;
        $this->country->phone1==0?$this->phone1=null:$this->phone1=$this->country->phone1;
//        dd($this->country->phone1,$this->phone1, $this->country->code_number,$this->codeNumber);
    }

    public function render()
    {
        return view('livewire.country.country-details-component');
    }
}
