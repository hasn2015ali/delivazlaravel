<?php

namespace App\Http\Livewire\User;

use App\Models\CityTranslation;
use App\Models\CountryTranslation;
use App\Models\Day;
use App\Models\DayWorkPartTime;
use App\Models\DeliveryWorkerDetail;
use App\Models\HourWorkPartTime;
use App\Models\Language;
use App\Models\User;
use App\Models\UserImage;
use App\Models\WorkHour;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Traits\WithUserValidationTrait;
class AddUser extends Component
{
    use WithFileUploads ,WithUserValidationTrait;

    public $type;
    public $firstName;
    public $father;
    public $lastName;
    public $phone;
    public $address;
    public $email;
    public $age;
    public $password;
    public $user;
    public $images=[];
    public $personalImage;
    public $partTimes;
    public $showPartTime=false;
    public $days;
    public $daySelected= [];
    public $partTimeSelected=[];
    public $workType=0;
    public $rolId;
    public $countries;
    public $selectedCountry=null;
    public $cities;
    public $selectedCity=null;
    public $languageSelected;
    public $langId;
    public $Select_Delivery_Manager;
    public $Delivery_Managers;
    public $loggedINUserRole;
    public $showSelectCountry=true;

//    public $createdUserId;

    public function messages()
    {
        return [
            'firstName.required' => trans( 'user.validate_first' ),
//            'fatherName.required' => trans( 'user.validate_father' ),
            'lastName.required' => trans( 'user.validate_last' ),
            'phone.required' => trans( 'user.validate_phone' ),
            'email.required' => trans( 'user.validate_email' ),
            'password.required' => trans( 'user.validate_password' ),
            'personalImage.image' => trans( 'user.imgVal' ),
//            'images.*.image' => trans( 'user.imgVal' ),
            'workType.required' => trans( 'user.validate_workType' ),


        ];
    }

    public function mount()
    {
//        dd(App::getlocale());
        $langCode=App::getlocale();
        $this->languageSelected=Language::where('code',$langCode)->first();
//        dd($this->languageSelected->id);
        $this->days=Day::all();
//        $this->countries=CountryTranslation::CountriesEn();
        $this->countries=CountryTranslation::where('lang_id', $this->languageSelected->id)->get();
//        dd( $this->countries);
        $this->setRoleID();

        //coo operations
        if(Auth::user()->role_id==3)
        {

            $this->showSelectCountry=false;
            $this->selectedCountry=Auth::user()->country_id;
            $this->cities=CityTranslation::where('lang_id',$this->languageSelected->id)
                ->where('country_id',$this->selectedCountry)->get() ;
        }

    }
    public function getCities()
    {

      $this->cities=CityTranslation::where('lang_id',$this->languageSelected->id)
          ->where('country_id',$this->selectedCountry)->get() ;
//        dd($countryId, $this->cities);
    }

    public function getSystemManager()
    {
        if($this->type=="Delivery_workers")
        {
            $this->Delivery_Managers=User::where('role_id',4)->where('city_id',$this->selectedCity)->get();

        }
    }

    public function getPartTimes()
    {
        $this->partTimes=WorkHour::all();
        $this->showPartTime=true;
    }
    public function hidePartTimes ()
    {
        $this->showPartTime=false;

    }

    public function submit() {
        //validation in userTrait
        if($this->validateCoo()==1)
        {
            return;
        }
        if($this->validateOthers()==1)
        {
            return;
        }
        if($this->validatePartTime()==1)
        {
            return;
        }
        if($this->validateDeliveryWorker()==1)
        {
            return;
        }
        if($this->checkEmail($this->email)==1)
        {
            return;
        }
        if($this->checkPhone($this->phone)==1)
        {
            return;
        }

        $this->validate([
            'firstName' => 'required',
//            'fatherName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'password' => 'required',
//            'personalImage' => 'image|mimes:png,gif,jpg,PNG,Gif,jpeg,JPG,JPEG,GIF',
//            'images.*' => 'image',
//            'images.*' => 'image|mimes:png,gif,jpg,PNG,Gif,jpeg,JPG,JPEG,GIF',
        ]);
        $password=Hash::make($this->password);

        if($this->personalImage)
        {
            $imageName=time().".".$this->personalImage->extension();
            $this->personalImage->storeAs('public/images/avatars',$imageName);
        }
        else
        {
            $imageName=null;
        }

       $newUser= User::create([
            'firstName' => $this->firstName,
            'fatherName' => $this->father,
            'lastName' => $this->lastName,
            'phone' =>$this->phone,
            'email' => $this->email,
            'address' => $this->address,
            'password' =>$password,
            'age' =>$this->age,
           'avatar' =>$imageName,
            'role_id' =>$this->rolId,
           'country_id' =>$this->selectedCountry,
           'city_id' =>$this->selectedCity,
        ]);
        if($this->type=='Delivery_workers')
        {
            DeliveryWorkerDetail::create([
            'delivery_worker_id' => $newUser->id,
            'delivery_system_manager_id' => $this->Select_Delivery_Manager,
        ]);

        }

        if( $this->insertWorkType($newUser)==1)
        {
            return;
        }

        if($this->images)
        {
            foreach ($this->images as $image)
            {
                $imageName=time().rand(1,1000).".".$image->extension();
                $image->storeAs('public/images/userDetails',$imageName);
                UserImage::create([
                    'user_id'=>$newUser->id,
                    'name'=>$imageName,
                ]);
            }

        }
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);

     }

     public function insertWorkType($newUser)
     {
         if($this->type!="manager")
         {
             if($this->workType==0)
             {
                 DayWorkPartTime::create([
                     'day_id' => 0,
                     'user_id' => $newUser->id,
                 ]);

                 HourWorkPartTime::create([
                     'part_time_id' => 0,
                     'user_id' => $newUser->id,
                 ]);

             }
             else
             {
                 foreach ($this->daySelected as $dayId)
                 {
                     DayWorkPartTime::create([
                         'day_id' => $dayId,
                         'user_id' => $newUser->id,
                     ]);

                 }

                 foreach ($this->partTimeSelected as $partId)
                 {
                     HourWorkPartTime::create([
                         'part_time_id' => $partId,
                         'user_id' => $newUser->id,
                     ]);
                 }
             }


         }
     }

    public function restFilde(){
        $this->firstName="";
        $this->father="";
        $this->lastName="";
        $this->age="";
        $this->email="";
        $this->address="";
        $this->phone="";
        $this->hoursworks="";
        $this->password="";
        $this->personalImage="";
        $this->images="";
        $this->selectedCity=null;
        $this->selectedCountry=null;

    }
    public function render()
    {
        return view('livewire.user.add-user');
    }


}
