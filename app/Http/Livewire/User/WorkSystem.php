<?php

namespace App\Http\Livewire\User;

use App\Models\Day;
use App\Models\WorkHour;
use Livewire\Component;
use Livewire\WithPagination;

class WorkSystem extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $dayName;
    public $dayId;
    public $startTime;
    public $endTime;
    public $startTimeMinutes=0;
    public $startTimeHour=0;
    public $endTimeHour=0;
    public $endTimeMinutes=0;
    public $PartTimeId;


    public function messages()
    {
        return [
            'dayName.required' => trans( 'workSystem.validateDay' ),
            'startTimeHour.required' => trans( 'workSystem.valStartTimeHour' ),
            'startTimeMinutes.required' => trans( 'workSystem.valStartTimeMinutes' ),
            'endTimeHour.required' => trans( 'workSystem.valEndTimeHour' ),
            'endTimeMinutes.required' => trans( 'workSystem.valEndTimeMinutes' ),

        ];
    }

    public function submitDay()
    {
        $this->validate([
            'dayName' => 'required',

        ]);
        $day= Day::create([
            'name' => $this->dayName,
        ]);

        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('addDay');

    }
    public function submitPartTime()
    {
        $this->validate([
            'startTimeHour' => 'required',
            'startTimeMinutes' => 'required',
            'endTimeHour' => 'required',
            'endTimeMinutes' => 'required',
        ]);
        $partTime= WorkHour::create([
            'startTimeHour' => $this->startTimeHour,
            'startTimeMinutes' => $this->startTimeMinutes,
            'endTimeHour' => $this->endTimeHour,
            'endTimeMinutes' => $this->endTimeMinutes,

        ]);

        $message=trans( 'masterControl.successfully_Added' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('addPartTime');
    }


    public function increaseHour()
    {
        if($this->startTimeHour<23)
        {
            $this->startTimeHour++;
        }
        else
        {
            $this->startTimeHour=0  ;
        }

    }

    public  function increaseMinutes()
    {
        if($this->startTimeMinutes<59)
        {
            $this->startTimeMinutes++;
        }
        else
        {
            $this->startTimeMinutes=0  ;
        }
    }

    public function minHour()
    {
        if($this->startTimeHour>0)
        {
            $this->startTimeHour--;
        }
        else
        {
            $this->startTimeHour=23  ;
        }
    }

    public function minMinutes()
    {
        if($this->startTimeMinutes>0)
        {
            $this->startTimeMinutes--;
        }
        else
        {
            $this->startTimeMinutes=59  ;
        }

    }

    //end start time

    public function increaseEndHour()
    {
        if($this->endTimeHour<23)
        {
            $this->endTimeHour++;
        }
        else
        {
            $this->endTimeHour=0  ;
        }

    }

    public  function increaseEndMinutes()
    {
        if($this->endTimeMinutes<59)
        {
            $this->endTimeMinutes++;
        }
        else
        {
            $this->endTimeMinutes=0  ;
        }
    }

    public function minEndHour()
    {
        if($this->endTimeHour>0)
        {
            $this->endTimeHour--;
        }
        else
        {
            $this->endTimeHour=23  ;
        }
    }

    public function minEndMinutes()
    {
        if($this->endTimeMinutes>0)
        {
            $this->endTimeMinutes--;
        }
        else
        {
            $this->endTimeMinutes=59  ;
        }

    }
    public function restFilde()
    {
        $this->dayName="";


    }


    public function deleteCall($id)
    {
        $this->PartTimeId=$id;
    }
    public function deleteAction()
    {
        $user=WorkHour::find($this->PartTimeId);
        $user->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('deleteAction');
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }


    public function render()
    {
//        $days=Day::orderBy( 'id', 'desc' )
//            ->paginate(10);

        $partTimes=WorkHour::orderBy( 'id', 'desc' )
            ->paginate(10);

        return view('livewire.user.work-system',compact('partTimes'));
    }
}
