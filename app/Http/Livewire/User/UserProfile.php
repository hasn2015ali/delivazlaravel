<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class UserProfile extends Component
{
    public $user;


    public function mount()
    {
        $this->user=User::with('country','dayWorkPartTime','hourWorkPartTime')
        ->find(Auth::user()->id);
//        dd($this->user->country->translation);
    }




    public function render()
    {
        $userDetail=User::find(Auth::user()->id);
        return view('livewire.user.user-profile',compact('userDetail'));
    }
}
