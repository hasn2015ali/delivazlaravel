<?php

namespace App\Http\Livewire\User;

use App\Models\Day;
use App\Models\DayWorkPartTime;
use App\Models\HourWorkPartTime;
use App\Models\User;
use App\Models\WorkHour;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class UpdateUserWorkHours extends Component
{
    public $userId;
    public $type;
    public $daysWork;
    public $partTimesWork;
    public $workType;
    public $workTypSelected;
    public $daySelected=[];
    public $days;
    public $partTimes;
    public $partTimeSelected=[];
    public $showPartTime;
    public $user;
//    public $showSelectedTimes;



    public function mount()
    {

        $this->user=User::find($this->userId);
        $this->daysWork=DayWorkPartTime::with('days:name,id')->where('user_id',$this->userId)->get();

        $this->partTimesWork=HourWorkPartTime::with('workHour')->where('user_id',$this->userId)->get();

//        dd($this->daysWork[0]['day_id'],$this->partTimes);
//        $this->partTimesWork=WorkHour::all();
//        dd( $this->daysWork, $this->partTimesWork);
        if($this->daysWork[0]['day_id']==0)
        {
            $this->workType=0;
        }
        else
        {
            $this->workType=1;
            $this->partTimes=WorkHour::all();
            $this->days=Day::all();
            $this->showPartTime=true;

        }
//        dd('test');
    }
    public function submitPartTime()
    {
        if($this->validatePartTime()==1)
        {
            return;
        }
//        dd($this->partTimeSelected,$this->daySelected,$this->workType);
        $this->insertWorkType($this->user);
        $message=trans( 'masterControl.successfully_Added' );
        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }
    public function  restFilde()
    {
        $this->daySelected=[];
        $this->partTimeSelected=[];
        $this->workTypSelected=null;
    }

    public function insertWorkType($User)
    {
        if($this->type!="manager")
        {
//            dd($this->workTypSelected,$this->daySelected,$this->partTimeSelected);
            DB::table('hour_work_part_times')->where('user_id', $this->userId)->delete();
            DB::table('day_work_part_times')->where('user_id', $this->userId)->delete();
            if($this->workTypSelected=="0")
            {
                DayWorkPartTime::create([
                    'day_id' => 0,
                    'user_id' => $User->id,
                ]);

                HourWorkPartTime::create([
                    'part_time_id' => 0,
                    'user_id' => $User->id,
                ]);
                $this->workType=0;
            }
            else
            {
                foreach ($this->daySelected as $dayId)
                {
                    DayWorkPartTime::create([
                        'day_id' => $dayId,
                        'user_id' => $User->id,
                    ]);

                }

                foreach ($this->partTimeSelected as $partId)
                {
                    HourWorkPartTime::create([
                        'part_time_id' => $partId,
                        'user_id' => $User->id,
                    ]);
                }

                $this->daysWork="";
                $this->partTimesWork="";
                $this->daysWork=DayWorkPartTime::with('days:name,id')->where('user_id',$this->userId)->get();
                $this->partTimesWork=HourWorkPartTime::with('workHour')->where('user_id',$this->userId)->get();
                $this->workType=1;
//                dd( $this->daysWork, $this->partTimesWork);
            }




        }
    }

    public function validatePartTime()
    {
        if($this->workTypSelected==""){
            $m=trans( 'user.dayVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1;
        }
        if($this->workTypSelected==1)
        {
            if(empty($this->daySelected))
            {
                $m=trans( 'user.dayVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1;
            }
            if(empty($this->partTimeSelected))
            {
                $m1=trans( 'user.HourVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m1]);

                return 1;
            }
        }

    }


    public function hidePartTimes ()
    {
        $this->showPartTime=false;

    }


    public function getPartTimes()
    {
        $this->partTimes=WorkHour::all();
        $this->days=Day::all();
        $this->showPartTime=true;
    }

    public function render()
    {
        return view('livewire.user.update-user-work-hours');
    }
}
