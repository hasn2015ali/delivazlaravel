<?php

namespace App\Http\Livewire\User;

use App\Models\CityTranslation;
use App\Models\CountryTranslation;
use App\Models\Language;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Traits\WithUserValidationTrait;
class UpdateUser extends Component
{
    use WithUserValidationTrait ;
    public $userId;
    public $type;
    public $user;
    public $firstName;
    public $father;
    public $lastName;
    public $phone;
    public $address;
    public $email;
    public $age;
    public $password;
    public $hoursworks=null;
    public $images;
    public $personalImage;
    public $selectedCountry;
    public $selectedCity;
    public $countries;
    public $cities;
    public $languageSelected;
    public $Select_Delivery_Manager;
    public $Delivery_Managers;
    public $showSelectCountry=true;



    public function messages()
    {
        return [
            'firstName.required' => trans( 'user.validate_first' ),
//            'fatherName.required' => trans( 'user.validate_father' ),
            'lastName.required' => trans( 'user.validate_last' ),
            'phone.required' => trans( 'user.validate_phone' ),
            'email.required' => trans( 'user.validate_email' ),
//            'password.required' => trans( 'user.validate_password' ),
            'personalImage.image' => trans( 'user.imgVal' ),
//            'images.*.image' => trans( 'user.imgVal' ),

        ];
    }

    public function submit()
    {
        $this->validate([
            'firstName' => 'required',
//            'fatherName' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
//            'password' => 'required',
        ]);

        if($this->validateOthers()==1)
        {
            return;
        }
        if($this->validateDeliveryWorker()==1)
        {
            return;
        }
        if($this->email!=$this->user->email)
        {
            if($this->checkEmail($this->email)==1)
            {
                return;
            }
        }
        if($this->password==null)
        {
            $password=$this->user->password;
        }
        else
        {
            $password=bcrypt($this->password);
        }

//        $password=Hash::make($this->password);
        $this->user->firstName=$this->firstName;
        $this->user->fatherName= $this->father;
        $this->user->lastName=$this->lastName;
        $this->user->phone=$this->phone;
        $this->user->address= $this->address;
        $this->user->email= $this->email;
        $this->user->age =$this->age;
        $this->user->password =$password;
        $this->user->country_id=$this->selectedCountry;
        $this->user->city_id=$this->selectedCity;
        $this->user->save();

        if($this->type=="Delivery_workers")
        {
            $this->user->delivaryWorker->update([
                'delivery_system_manager_id' => $this->Select_Delivery_Manager,
            ]);

        }


        $message=trans( 'masterControl.updated_successfully' );
//        $this->restFilde();
        $this->emit('alert',['icon'=>'success','title'=>$message]);

    }
    public function getCities()
    {

        $this->cities=CityTranslation::where('lang_id',$this->languageSelected->id)
            ->where('country_id',$this->selectedCountry)->get() ;
        $this->selectedCity=null;
//        dd($countryId, $this->cities);
    }

    public function getSystemManager()
    {
        if($this->type=="Delivery_workers")
        {
            $this->Delivery_Managers=User::where('role_id',4)->where('city_id',$this->selectedCity)->get();
            $this->Select_Delivery_Manager=null;

        }
    }



    public function mount()
    {


        $langCode=App::getlocale();
        $this->languageSelected=Language::where('code',$langCode)->first();
        $this->user=User::find($this->userId);
        $this->firstName=$this->user->firstName;
        $this->father=$this->user->fatherName;
        $this->lastName=$this->user->lastName;
        $this->phone=$this->user->phone;
        $this->address=$this->user->address;
        $this->email=$this->user->email;
        $this->age=$this->user->age;
        $this->selectedCountry=$this->user->country_id;
        $this->selectedCity=$this->user->city_id;
        $this->countries=CountryTranslation::where('lang_id', $this->languageSelected->id)->get();
        if($this->type=='Delivery_System_Team' or $this->type=='Delivery_Costumer_Team' or $this->type=='Delivery_workers' or $this->type=='Customers')
        {
            $this->cities=CityTranslation::where('lang_id',$this->languageSelected->id)
                ->where('country_id',$this->selectedCountry)->get() ;
        }
        if($this->type=="Delivery_workers")
        {
            $this->Select_Delivery_Manager=$this->user->delivaryWorker->delivery_system_manager_id;
            $this->Delivery_Managers=User::where('role_id',4)->where('city_id',$this->user->city_id)->get();
        }
//        dd( $this->user);

//        $this->password=$this->user->password;
//        $this->hoursworks=$this->user->firstName;
        //coo operations
        if(Auth::user()->role_id==3)
        {
//            dd(Auth::user()->role_id==3);

            $this->showSelectCountry=false;
            $this->selectedCountry=Auth::user()->country_id;
            $this->cities=CityTranslation::where('lang_id',$this->languageSelected->id)
                ->where('country_id',$this->selectedCountry)->get() ;
        }

    }
    public function render()
    {
        return view('livewire.user.update-user');
    }
}
