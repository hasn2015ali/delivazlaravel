<?php

namespace App\Http\Livewire\User;

use App\Models\DayWorkPartTime;
use App\Models\HourWorkPartTime;
use App\Models\User;
use Livewire\Component;

class MyWorkType extends Component
{
    public $workType;
    public $daysWork;
    public $partTimesWork;
    public $user;

    public function mount()
    {
        $this->user=User::find(auth()->user()->id);
        if($this->user->role_id==2)
        {
            $this->workType=0;
        }
        else
        {
            $this->daysWork=DayWorkPartTime::with('days:name,id')->where('user_id',$this->user->id)->get();
            $this->partTimesWork=HourWorkPartTime::with('workHour')->where('user_id',$this->user->id)->get();

            if($this->daysWork[0]['day_id']==0)
            {
                $this->workType=0;
            }
            else
            {
                $this->workType=1;

            }
        }


    }
    public function render()
    {
        return view('livewire.user.my-work-type');
    }
}
