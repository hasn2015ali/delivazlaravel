<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use App\Traits\WithUserValidationTrait;

class Users extends Component
{

    use WithPagination ,WithUserValidationTrait;
    public $type;
    public $rolId;
    public $userId;
    public $loggedINUserRole;
    public $m;


    public function mount()
    {
        $this->loggedINUserRole=Auth::user()->role_id;
        $this->setRoleID();

    }

    public function switchState($id)
    {
//        dd($id);
        $user=User::find($id);
        if($user->status==0)
        {
            $user->status=1;
            $user->save();
        }
        elseif ($user->status==1)
        {
            $user->status=0;
            $user->save();
        }

    }
    public function deleteCall($id)
    {
        $this->userId=$id;
    }
    public function deleteAction()
    {
        $user=User::with('images')->find($this->userId);
        Storage::delete('public/images/avatars/'.$user->avatar);
        foreach ($user->images as $image)
        {
            Storage::delete('public/images/userDetails/'.$image->name);
        }
        $user->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
//        session()->flash('message',trans( 'masterControl.Deleted_successfully' ) );
        $this->emit('deleteAction');
        $this->emit('alert',['icon'=>'success','title'=>$message]);
    }

    public function render()
    {
//        dd($this->loggedINUserRole);
        if($this->loggedINUserRole==2)
        {
            $users=User::with('images')
                ->where('role_id',$this->rolId)
                ->orderBy('id','desc')
                ->paginate(10);
//            return view('livewire.user.users',compact('users'));
        }
        elseif($this->loggedINUserRole==3)
        {
            if($this->rolId==2 or $this->rolId==7 or $this->rolId==8)
            {
//                return redirect('/en');
                $this->m="Sorry you cannot access and show users";
                $users=null;
                return view('livewire.user.users',compact('users'));

            }
            else
            {
                $cooCountryID=Auth::user()->country_id;
                $users=User::with('images')
                    ->where('role_id',$this->rolId)
                    ->where('country_id',$cooCountryID)
                    ->orderBy('id','desc')
                    ->paginate(10);
            }

        }
        return view('livewire.user.users',compact('users'));


    }
}
