<?php

namespace App\Http\Livewire\User;

use App\Traits\WithRestaurantTrait;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NotifictationComponent extends Component
{
    use WithRestaurantTrait;
    public $countOFNewRestaurant=0;
    public $countOFNewVendor=0;
    public $user;
    public $allNotifications;
    protected $listeners = [
//        'updateNewRestaurantCount'=>'render',
//        'getItemByFilter'=>'render',
        'newServiceProviderJoined'=>'render'
    ];

    public function mount()
    {
       $this->user= Auth::user();
    }
    public function getNewRestaurantsNotifications()
    {
        $this->countOFNewRestaurant=$this->getNewRestaurantCount();

    }

    public function getNewShopsNotifications()
    {
        $this->countOFNewVendor=$this->getNewShopsCount();

    }

    public function render()
    {
//        dd('test');
        if($this->user->role_id==2 or $this->user->role_id==8)
        {
           $this->getNewRestaurantsNotifications();
           $this->getNewShopsNotifications();
        }
        $this->allNotifications=$this->countOFNewRestaurant+$this->countOFNewVendor;

        return view('livewire.user.notifictation-component');
    }
}
