<?php

namespace App\Http\Livewire\Subscribers;

use App\Models\Subscriber;
use App\Traits\WithCountryAndCity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class SubscriberComponent extends Component
{
    use WithPagination ,WithCountryAndCity;

    //vars for WithCountryAndCity Trait
    public $countries;
    public $languageSelected;
    public $cities;
    public $selectedCountry=null;
    public $selectedCity=null;

    public $countryName;
    public $countryId;
    public $cityId;
    public $cityName;

    public $isSubcsribers=false;
    public $subscriberID;

    public $countryIdSub;
    public $cityIdSub;

    public function downLoadPDF()
    {
        if(Auth::user()->role_id==2)
        {
            $subscribers= DB::table('subscribers')
            ->select('subscribers.email')
                ->where('country_id', $this->countryId)
                ->where('city_id',$this->cityId)
                ->orderBy('id','desc')
                ->get();
        }
        elseif (Auth::user()->role_id==8)
        {
            $this->isSubcsribers=true;

            $user=Auth::user();
            $subscribers= DB::table('subscribers')
                ->select('subscribers.email')
                ->where('country_id', $user->country_id)
                ->where('city_id',$user->city_id)
                ->orderBy('id','desc')
                ->get();
        }

        $ciy= $this->cityName;
        $pdf = \PDF::loadView('PDF.subscribersPDF', compact('subscribers','ciy'));

//        dd($ciy,$pdf);

        return $pdf->download('subscribers.pdf');
    }
    public function getCities($countryID,$countryName)
    {
//        dd($country);
        $this->countryName=$countryName;
        $this->countryId=$countryID;
        $this->countryIdSub=$countryID;

        $this->getCitiesForCountry($this->countryId);
//        dd($this->cities);
    }

    public function sendCityVal($cityID,$cityName)
    {


        $this->cityName=$cityName;
        $this->cityId=$cityID;
        $this->cityIdSub=$cityID;
        $this->isSubcsribers=true;

    }

    public function mount()
    {
        $this->getCountries();

        $user=Auth::user();
        if($user->role_id==8)
        {
            $this->countryIdSub= $user->country_id;
            $this->cityIdSub=$user->city_id;
            $this->isSubcsribers=true;
        }


    }
    public function deleteCall($id)
    {
        $this->subscriberID=$id;
    }

    public function deleteAction()
    {
        $Subscriber = Subscriber::find($this->subscriberID);
        $Subscriber->delete();
        $message=trans( 'masterControl.Deleted_successfully' );
        $this->emit('alert',['icon'=>'success','title'=>$message]);
        $this->emit('deleteAction');
    }
    public function render()
    {

            $subscribers= DB::table('subscribers')
                ->where('country_id', $this->countryIdSub)
                ->where('city_id',$this->cityIdSub)
                ->orderBy('id','desc')
                ->paginate(10);

        return view('livewire.subscribers.subscriber-component',compact('subscribers'));
    }
}
