<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'email','city_id','country_id',
    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }
}
