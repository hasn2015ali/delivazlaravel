<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class filterTranslate extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'code_lang', 'filter_id','type','lang_id',
    ];
    public function filter()
    {
        return $this->belongsTo('App\Models\filter','filter_id');
    }
    public function lang()
    {
        return $this->belongsTo('App\Models\Language','lang_id');
    }
}
