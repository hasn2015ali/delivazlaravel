<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduledOrder extends Model
{
    use HasFactory, \Znck\Eloquent\Traits\BelongsToThrough;
    protected $fillable = [
        'date', 'time', 'repeatOrder','startDate',
        'intervals', 'ongoing', 'endDate','order_id',

    ];
    public function customerOrder()
    {
        return $this->belongsTo('App\Models\CustomerOrder','order_id');
    }
    public function user()
    {
        return $this->belongsToThrough('App\Models\User', 'App\Models\CustomerOrder');
    }
}
