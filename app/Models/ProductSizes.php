<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSizes extends Model
{
    use HasFactory;
    protected $fillable = [
        'price',
        'size',
        'product_id',
        'weight',
        'content',
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
