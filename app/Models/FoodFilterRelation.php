<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodFilterRelation extends Model
{
    use HasFactory;
    protected $fillable = [
        'food_id',
        'filter_id',
    ];
}
