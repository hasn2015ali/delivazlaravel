<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProvider extends Model
{
    use HasFactory ,\Znck\Eloquent\Traits\BelongsToThrough;
//    use \Znck\Eloquent\Traits\BelongsToThrough;
    protected $fillable = [
        'name',
        'nameEN',
        'address',
        'minimum_order_amount',
        'mode',
        'delivery_man_commission',
        'pick_commission',
        'phone1',
        'phone2',
        'phone3',
        'type',
        'user_id',
        'avatar',
        'provider_url',
        'slug',
        'delivaz_commission',
        'registration_state',
        'tax_id',

    ];

    public function tax()
    {
        return $this->belongsTo('App\Models\Taxs','tax_id');
    }

    public function filters()
    {
        return $this->belongsToMany(filter::class, 'service_provider_filters', 'service_provider_id', 'filter_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function banner()
    {
        return $this->hasMany('App\Models\ServiceProviderBanner','service_provider_id');
    }

    public function document()
    {
        return $this->hasMany('App\Models\ServiceProviderDocument','service_provider_id');
    }

    public function size()
    {
        return $this->hasMany('App\Models\serviceProviderSizes','service_provider_id');
    }

    public function partTime()
    {
        return $this->hasMany('App\Models\ServiceProviderPartTime','service_provider_id');
    }


    public function citRecommended()
    {
        return $this->hasOne('App\Models\cityRecommended','service_provider_id');
    }

    public function Shape()
    {
        return $this->hasMany('App\Models\CityShapcontent','content_id');
    }


    public function serviceProvidersPageBanner()
    {
        return $this->hasMany('App\Models\CityServiceProviders','service_provider_id');
    }

    public function cityPageBanner()
    {
        return $this->hasMany('App\Models\cityBanner','service_provider_id');
    }

    public function foods()
    {
        return $this->hasMany('App\Models\Food','service_provider_id');
    }
    public function products()
    {
        return $this->hasMany('App\Models\Product','service_provider_id');
    }

    public function city()
    {
        return $this->belongsToThrough('App\Models\City', 'App\Models\User');
    }

    public function country()
    {
        return $this->belongsToThrough('App\Models\Country', 'App\Models\User');
    }

    public function userWishlist()
    {
        return $this->hasMany('App\Models\UserWishlist','service_provider_id');
    }

    public function menu()
    {
        return $this->hasMany('App\Models\ServiceProviderMenu','service_provider_id');
    }


}
