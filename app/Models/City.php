<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $fillable = [
       'country_id','state','phone1','phone2','phone3','icon1','icon2','icon3',
    ];
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }

    public function translation()
    {
        return $this->hasMany('App\Models\CityTranslation','city_id');
    }
    public function shape()
    {
        return $this->hasMany('App\Models\cityShape','city_id');
    }

    public function deliverySetting()
    {
        return $this->hasOne('App\Models\CityDeliverySetting','city_id');
    }

    public function banner()
    {
        return $this->hasMany('App\Models\cityBanner','city_id');
    }
    public function citRecommended()
    {
        return $this->hasMany('App\Models\cityRecommended','city_id');
    }

    public function serviceProviderPage()
    {
        return $this->hasMany('App\Models\CityServiceProviders','city_id');
    }

    public function user()
    {
        return $this->hasMany('App\Models\User','city_id');
    }

    public function subscriber()
    {
        return $this->hasMany('App\Models\Subscriber','city_id');
    }

    public function userWish()
    {
        return $this->hasOne('App\Models\UserWishlist','city_id');
    }

    public function serviceProviders()
    {
        return $this->hasManyThrough('App\Models\ServiceProvider','App\Models\User');
    }

}
