<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'content',
        'code_lang',
        'lang_id',
        'product_id',
        'service_provider_id',
    ];
    public function product()
    {
        return $this->belongsTo('App\Models\Product','product_id');
    }
}
