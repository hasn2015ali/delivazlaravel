<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryTerm extends Model
{
    use HasFactory;
    protected $fillable = [
        'country_id','type',
    ];

      public function translation()
      {
          return $this->hasMany('App\Models\CountryTermTrans','term_id');
      }
}
