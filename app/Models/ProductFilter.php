<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFilter extends Model
{
    use HasFactory;
    protected $fillable = [
        'filter_id',
    ];
    public function translation()
    {
        return $this->hasMany('App\Models\ProductFilterTrans','product_filter_id');
    }

    public function filters()
    {
        return $this->belongsToMany(Product::class, 'product_filters_relations', 'filter_id', 'product_id');
    }

    public function menu()
    {
        return $this->belongsTo('ServiceProviderMenu', 'filter_id');
    }
}
