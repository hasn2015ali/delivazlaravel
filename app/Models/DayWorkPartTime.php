<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DayWorkPartTime extends Model
{
    protected $fillable = [
        'day_id','user_id',
    ];
    use HasFactory;
    public function days()
    {
        return $this->belongsTo('App\Models\Day','day_id');

    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
