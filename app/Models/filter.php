<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class filter extends Model
{
    use HasFactory;
    protected $fillable = [
       'type',
    ];

    public function translation()
    {
        return $this->hasMany('App\Models\filterTranslate','filter_id');
    }

    public function serviceProvider()
    {
        return $this->belongsToMany(ServiceProvider::class, 'service_provider_filters', 'filter_id', 'service_provider_id');
    }
    public function Shape()
    {
        return $this->hasMany('App\Models\CityShapcontent','content_id');
    }
}
