<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityServiceProviders extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'photo_type',
        'provider_type',
        'state',
        'city_id',
        'service_provider_id'
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');
    }


}
