<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodTranslation extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'content',
        'code_lang',
        'lang_id',
        'food_id',
        'service_provider_id',
    ];
    public function food()
    {
        return $this->belongsTo('App\Models\Food','food_id');
    }
}
