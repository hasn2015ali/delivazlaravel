<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;
   protected $fillable = [
    'code', 'flag','currency','currency_code','code_number','phone1','icon1',
];

    public function taxs()
    {
        return $this->belongsToMany(Taxs::class, 'country_tax', 'country_id', 'tax_id');
    }

    public function translation()
    {
        return $this->hasMany('App\Models\CountryTranslation','country_id');
    }

    public function cities()
    {
        return $this->hasMany('App\Models\City','country_id');
    }
    public function users()
    {
        return $this->hasMany('App\Models\User','country_id');
    }

    public function subscriber()
    {
        return $this->hasMany('App\Models\Subscriber','country_id');
    }


    public function countryTerm()
    {
        return $this->hasMany('App\Models\CountryTerm','country_id');
    }

    public function userWish()
    {
        return $this->hasOne('App\Models\UserWishlist','country_id');
    }

    public function serviceProviders()
    {
        return $this->hasManyThrough('App\Models\ServiceProvider','App\Models\User');
    }

}
