<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityShapcontent extends Model
{
    use HasFactory;
    protected $fillable = [
        'shape_id',
        'content_id',
        'type',
    ];

    public function shape()
    {
        return $this->belongsTo('App\Models\cityShape','shape_id');
    }

    public function filter()
    {
        return $this->belongsTo('App\Models\filter','content_id');
    }


    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','content_id');
    }

}
