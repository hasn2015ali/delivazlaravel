<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryWorkerDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'delivery_worker_id', 'delivery_system_manager_id',
    ];
    public function userWorker()
    {
        return $this->belongsTo('App\Models\User','delivery_worker_id');
    }
    public function userSystemManager()
    {
        return $this->belongsTo('App\Models\User','delivery_system_manager_id');
    }
}
