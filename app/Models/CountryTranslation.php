<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'country_id', 'lang_id','code_lang',
    ];
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }

    public function lang()
    {
        return $this->belongsTo('App\Models\Language','lang_id');
    }

    public function scopeCountriesEn($query)
    {
        return $query->where('lang_id',3)->get(['country_id','name']);
    }

    public function scopeNameEn($query,$id)
    {
        return $query->where('lang_id',3)->where('country_id',$id)->get('name');
    }
}
