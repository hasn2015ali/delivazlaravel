<?php

namespace App\Models;

use App\Notifications\ClickSendTest;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Notification;
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'fatherName',
        'lastName',
        'avatar',
        'email',
        'phone',
        'address',
        'age',
        'password',
        'role_id',
        'balance',
        'code_lang',
        'language_id',
        'country_id',
        'city_id',
        'status',
        'two_factor_secret',
        'two_factor_recovery_codes',
        'email_verified_at',
        'phone_verified',
        'provider',
        'provider_id',


    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
//        'email_verified_at' => 'datetime',
//        'phone_verified_at'=>  'datetime'
    ];

    public function routeNotificationForClickSend()
    {

        return $this->phone;
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }
    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');
    }


    public function images()
    {
        return $this->hasMany('App\Models\UserImage','user_id');
    }

    public function dayWorkPartTime()
    {
        return $this->hasMany('App\Models\DayWorkPartTime','user_id');
    }

    public function hourWorkPartTime()
    {
        return $this->hasMany('App\Models\HourWorkPartTime','user_id');
    }

    public function delivaryWorker()
    {
        return $this->hasOne('App\Models\DeliveryWorkerDetail','delivery_worker_id');
    }

    public function delivarySystemManager()
    {
        return $this->hasMany('App\Models\DeliveryWorkerDetail','delivery_system_manager_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role','role_id');

    }

    public function serviceProvider()
    {
        return $this->hasOne('App\Models\ServiceProvider','user_id');
    }

    public function hasAnyRole($role)
    {
        return null !== $this->role()->where('role',$role)->first();
    }

    public function wishes()
    {
        return $this->hasMany('App\Models\UserWishlist','user_id');
    }


        public function orderAddress()
    {
        return $this->hasMany('App\Models\OrderAddress','user_id');
    }

    public function userCart()
    {
        return $this->hasMany('App\Models\UserCart','user_id');
    }

    public function customerOrder()
    {
        return $this->hasMany('App\Models\CustomerOrder', 'user_id');

    }
    public function scheduledOrder()
    {
        return $this->hasManyThrough('App\Models\ScheduledOrder','App\Models\CustomerOrder','user_id','order_id');
    }
}
