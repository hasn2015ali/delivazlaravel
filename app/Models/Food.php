<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    use HasFactory;
    protected $fillable = [
        'photo',
        'price',
        'weight',
        'size',
        'time_hour',
        'time_minutes',
        'service_provider_id',
        'tax_id',
    ];

    public function tax()
    {
        return $this->belongsTo('App\Models\Taxs','tax_id');
    }

    public function translation()
    {
        return $this->hasMany('App\Models\FoodTranslation','food_id');
    }
    public function sizes()
    {
        return $this->hasMany('App\Models\FoodSizes','food_id');
    }
    public function additions()
    {
        return $this->hasMany('App\Models\FoodAdditions','food_id');
    }
    public function filters()
    {
        return $this->belongsToMany(MealFilter::class, 'food_filter_relations', 'food_id', 'filter_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\FoodPhotos','food_id');
    }

    public function userWish()
    {
        return $this->hasOne('App\Models\UserWishlist','item_id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider', 'service_provider_id');
    }

    public function OrderDetail()
    {
        return $this->hasOne('App\Models\OrderDetail','product_id');
    }

}
