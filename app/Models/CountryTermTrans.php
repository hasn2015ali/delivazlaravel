<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryTermTrans extends Model
{
    use HasFactory;
    protected $fillable = [
        'country_id','address','code_lang','term_id','content','type',
    ];

    public function countryTerm()
    {
        return $this->belongsTo('App\Models\CountryTerm','term_id');
    }
}
