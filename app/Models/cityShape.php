<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cityShape extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'url',
        'icon',
        'state',
        'city_id',
    ];


//    protected $casts = [
//        'name' => 'float',
//    ];

//    protected $casts = [
//        'name' => 'int',
//    ];

//    public function getNameAttribute($value)
//    {
//        return ($value + "lll");
//    }
    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }

    public function translation()
    {
        return $this->hasMany('App\Models\CityShapTranslation','shape_id');
    }

    public function content()
    {
        return $this->hasMany('App\Models\CityShapcontent','shape_id');
    }
}
