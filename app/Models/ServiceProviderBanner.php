<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderBanner extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'state',
        'service_provider_id',
    ];

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');
    }

}
