<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id', 'size', 'additions','quantity',
        'price', 'order_id',

    ];
    public function customerOrder()
    {
        return $this->belongsTo('App\Models\CustomerOrder','order_id');

    }

    public function food()
    {
        return $this->belongsTo('App\Models\Food', 'product_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}
