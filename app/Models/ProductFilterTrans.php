<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFilterTrans extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'code_lang', 'filter_id','lang_id','product_filter_id',
    ];
    public function productFilter()
    {
        return $this->belongsTo('App\Models\ProductFilter','product_filter_id');
    }
    public function lang()
    {
        return $this->belongsTo('App\Models\Language','lang_id');
    }
}
