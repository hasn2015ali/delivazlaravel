<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderToAddress extends Model
{
    use HasFactory;
    protected $fillable = [
        'addressName', 'firstNameRecipient', 'lastNameRecipient','mobileNumberRecipient',
        'buildingNameRecipient', 'addressOnMap', 'firstNameSender',
        'lastNameSender','mobileNumberSender','nearestLandmark','user_id',

    ];

    public function customerOrder()
    {
        return $this->hasOne('App\Models\CustomerOrder','address_id');

    }
}
