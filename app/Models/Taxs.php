<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Taxs extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'type',
        'value',
    ];
    public function countries()
    {
        return $this->belongsToMany(Country::class, 'country_tax', 'tax_id', 'country_id');
    }

    public function serviceProvider()
    {
        return $this->hasOne('App\Models\ServiceProvider','tax_id');
    }

    public function food()
    {
        return $this->hasOne('App\Models\Food','tax_id');
    }

    public function product()
    {
        return $this->hasOne('App\Models\Product','tax_id');
    }

    public function cityDeliverySetting()
    {
        return $this->hasOne('App\Models\CityDeliverySetting','tax_id');
    }
}
