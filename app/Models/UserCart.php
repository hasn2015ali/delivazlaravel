<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    use HasFactory;
    protected $fillable = [
        'idDES', 'itemID','serviceProviderID','price','size','photo',
        'type','name','quantity','additions',
        'user_id','country_id','city_id',
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User','user_id');
    }
}
