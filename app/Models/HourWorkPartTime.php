<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HourWorkPartTime extends Model
{
    protected $fillable = [
        'part_time_id','user_id',
    ];
    use HasFactory;
    public function workHour()
    {
        return $this->belongsTo('App\Models\WorkHour','part_time_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
