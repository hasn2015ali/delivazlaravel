<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{
    use HasFactory, \Staudenmeir\EloquentHasManyDeep\HasRelationships;
//    use \Staudenmeir\EloquentHasManyDeep\HasRelationships;
    protected $fillable = [
        'name', 'city_id', 'lang_id','code_lang', 'country_id',
    ];
    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }

    public function lang()
    {
        return $this->belongsTo('App\Models\Language','lang_id');
    }


}
