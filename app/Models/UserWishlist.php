<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWishlist extends Model
{
    use HasFactory;
    protected $fillable = [
        'item_id',
        'service_provider_id',
        'city_id',
        'country_id',
        'type',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }

    public function food()
    {
        return $this->belongsTo('App\Models\Food','item_id');

    }
    public function product()
    {
        return $this->belongsTo('App\Models\Product','item_id');

    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country','country_id');

    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');

    }

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');

    }

}
