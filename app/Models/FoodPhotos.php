<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodPhotos extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'food_id',
    ];

    public function food()
    {
        return $this->belongsTo('App\Models\Food','food_id');
    }

}
