<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodSizes extends Model
{
    use HasFactory;
    protected $fillable = [
        'price',
        'size',
        'food_id',
        'weight',
        'content',
    ];

    public function food()
    {
        return $this->belongsTo('App\Models\Food','food_id');
    }
}
