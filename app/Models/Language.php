<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $fillable =['name','code','icon'];
    public function country()
    {
        return $this->hasMany('App\Models\CountryTranslation','lang_id');
    }
    public function city()
    {
        return $this->hasMany('App\Models\CityTranslation','lang_id');
    }

    public function filter()
    {
        return $this->hasMany('App\Models\filterTranslate','lang_id');
    }
    public function mealFilter()
    {
        return $this->hasMany('App\Models\MealFilterTrans','lang_id');
    }
    public function productFilter()
    {
        return $this->hasMany('App\Models\ProductFilterTrans','lang_id');
    }
}
