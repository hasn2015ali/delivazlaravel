<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealFilterTrans extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'code_lang', 'filter_id','lang_id','meal_filter_id',
    ];
    public function mealFilter()
    {
        return $this->belongsTo('App\Models\MealFilter','meal_filter_id');
    }
    public function lang()
    {
        return $this->belongsTo('App\Models\Language','lang_id');
    }
}
