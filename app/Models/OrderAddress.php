<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    use HasFactory;
    protected $fillable = [
        'addressName', 'firstName', 'lastName','mobileNumber',
        'buildingName', 'addressOnMap', 'addressType','nearestLandmark',
        'user_id',
    ];
    public function customerOrder()
    {
        return $this->hasOne('App\Models\CustomerOrder','address_id');

    }
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
