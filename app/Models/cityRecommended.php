<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cityRecommended extends Model
{
    use HasFactory;
    protected $fillable = [
        'content',
        'photo',
        'city_id',
        'type',
        'service_provider_id',
    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');
    }
}
