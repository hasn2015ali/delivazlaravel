<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class CityShapTranslation extends Model
{
    use HasFactory;

    protected $fillable = [
        'trans',
        'code_lang',
        'shape_id',
        'city_id',
        'shape_number',
        'slug'
    ];

    public function getSlugAttribute($value){
//        $this->attributes['trans'] = Str::slug($value, '-');
//        $this->attributes['slug'] = Str::slug($value, '-');
        $this->attributes['slug'] = "kkk";

    }
    public function shape()
    {
        return $this->belongsTo('App\Models\cityShape','shape_id');
    }




}
