<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderMenu extends Model
{
    use HasFactory;
    protected $fillable = [
        'service_provider_id',
        'filter_id',
        'order',
        'state',
    ];
    public function mailFilter()
    {
        return $this->hasOne('App\Models\MealFilter','id','filter_id');
    }

    public function mailFilterTrans()
    {
        return $this->hasManyThrough('App\Models\MealFilterTrans', 'App\Models\MealFilter','id','meal_filter_id','filter_id','id');
    }

    public function ProductFilter()
    {
        return $this->hasOne('App\Models\ProductFilter','id','filter_id');
    }

    public function productFilterTrans()
    {
        return $this->hasManyThrough('App\Models\ProductFilterTrans', 'App\Models\ProductFilter','id','product_filter_id','filter_id','id');
    }

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');
    }
}
