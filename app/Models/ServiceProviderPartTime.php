<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProviderPartTime extends Model
{
    use HasFactory;
    protected $fillable = [
        'startTimeHour', 'startTimeMinutes','endTimeHour','endTimeMinutes','day_id','service_provider_id','day_state',
    ];

    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider','service_provider_id');
    }

}
