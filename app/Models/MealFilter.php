<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealFilter extends Model
{
    use HasFactory;
    protected $fillable = [
        'filter_id',
    ];
    public function translation()
    {
        return $this->hasMany('App\Models\MealFilterTrans','meal_filter_id');
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class, 'food_filter_relations', 'filter_id', 'food_id');
    }
}
