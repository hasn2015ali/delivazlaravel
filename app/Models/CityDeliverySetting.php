<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityDeliverySetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'city_id',
        'price_in_area',
        'price_out_area',
        'worker_in_Ratio',
        'worker_out_Ratio',
        'tax_id',
        'min_distance',
        'max_distance',

    ];

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }
    public function tax()
    {
        return $this->belongsTo('App\Models\Taxs','tax_id');
    }
}
