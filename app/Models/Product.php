<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'content',
        'photo',
        'price',
        'weight',
        'size',
        'service_provider_id',
        'tax_id',
    ];
    public function tax()
    {
        return $this->belongsTo('App\Models\Taxs','tax_id');
    }
    public function translation()
    {
        return $this->hasMany('App\Models\ProductTranslation','product_id');
    }
    public function sizes()
    {
        return $this->hasMany('App\Models\ProductSizes','product_id');
    }
    public function additions()
    {
        return $this->hasMany('App\Models\ProductAdditions','product_id');
    }
    public function filters()
    {
        return $this->belongsToMany(ProductFilter::class, 'product_filters_relations', 'product_id', 'filter_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\ProductPhotos','product_id');
    }

    public function userWish()
    {
        return $this->hasOne('App\Models\UserWishlist','item_id');
    }
    public function serviceProvider()
    {
        return $this->belongsTo('App\Models\ServiceProvider', 'service_provider_id');
    }
    public function OrderDetail()
    {
        return $this->hasOne('App\Models\OrderDetail','product_id');
    }
}
