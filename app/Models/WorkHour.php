<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class WorkHour extends Model
{

    use HasFactory;
    protected $fillable = [
        'startTimeHour', 'startTimeMinutes','endTimeHour','endTimeMinutes',
    ];
    public function dayWorkPartTime()
    {
        return $this->hasMany('App\Models\HourWorkPartTime','part_time_id');

    }
}
