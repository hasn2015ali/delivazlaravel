<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    use HasFactory;
    protected $fillable = [
        'identifier', 'address_id', 'address_type','paymentType','orderOption',
        'subtotal', 'deliveryCharge', 'tax','orderTo','state','user_id',

    ];

    public function scheduledOrder()
    {
        return $this->hasOne('App\Models\ScheduledOrder','order_id');
    }

    public function orderAddress()
    {
        return $this->belongsTo('App\Models\OrderAddress','address_id');
    }

    public function orderToAddress()
    {
        return $this->belongsTo('App\Models\OrderToAddress','address_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }
    public function orderDetail()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id');
    }

}
