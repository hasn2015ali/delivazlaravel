<?php

namespace App\Actions\Fortify;

use App\Mail\EmailVerificationCodeSend;
use App\Models\User;
use App\Notifications\ClickSendTest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;


class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        $phone = null;
        $email = null;
//        $user=null;
        $messages = [
            "name.required" => trans('auth.RequiredFirstName'),
            "lastName.required" => trans('auth.RequiredLastName'),
            "password.required" => trans('auth.passwordRequired'),
            "password_confirmation.required" => trans('auth.password_confirmationRequired'),
            "password_confirmation.same" => trans('auth.password_confirmationSame'),
            "password.min" => trans('auth.password_min'),
            "email.email" => trans('auth.validateIdentification'),
            "email.unique" => trans('auth.validateEmail'),
        ];
        $messagesMobile = [
            "email.unique" => trans('auth.validatePhone'),
            "email.numeric" => trans('auth.validateIdentification'),
        ];

        if (filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {

            Validator::make($input, ['email' => ['email', 'unique:users,email']], $messages)->validate();
            $email = $input['email'];
            Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'lastName' => ['required', 'string', 'max:255'],
//            'password' => $this->passwordRules(),
                'password' => 'required|min:8',
                'password_confirmation' => 'required|same:password'
            ], $messages)->validate();


//            dd($input['name'],$input['lastName']);
            $user = User::create([
                'firstName' => $input['name'],
                'lastName' => $input['lastName'],
                'email' => $email,
                'phone' => $phone,
                'password' => Hash::make($input['password']),
            ]);
            $submitedCode = rand(1111, 9999);
            session(['start_user_time_for_Email'=>time()]);
            Mail::to($email)->send(new EmailVerificationCodeSend($submitedCode));
            if (session()->has('email_verification_code')) {
                session()->forget(['email_verification_code']);
//               session(['email_verification_code'=>$code]);
                session()->put('email_verification_code', $submitedCode);

            } else {
                session(['email_verification_code' => $submitedCode]);
            }
//            session()->put('mobileMessage',  trans( 'userFront.sendSms' ));

            session()->put('emailMessage', trans('userFront.emailRegisterSend'));

        } else {
            Validator::make($input, ['email' => ['numeric', 'unique:users,phone']], $messagesMobile)->validate();
            $phone = $input['email'];


            Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'lastName' => ['required', 'string', 'max:255'],
                'password' => 'required|min:8',
                'password_confirmation' => 'required|same:password'
            ], $messages)->validate();


            $user = User::create([
                'firstName' => $input['name'],
                'lastName' => $input['lastName'],
                'email' => $email,
                'phone' => $phone,
                'password' => Hash::make($input['password']),
            ]);
            $user1 = $user;
            $code = rand(1111, 9999);
            session(['start_user_time_for_phone'=>time()]);
//            $code=1111;
//           dd($user,"uuu");

            try {

                $user1->notify(new ClickSendTest($code));
            } catch (\Exception $e) {
                // do something when error
//                dd($e->getMessage());
//               return $e->getMessage();
                session()->put('mobileMessage', trans('userFront.FailedToSendCodeToPhone'));
                return $user;
            }

//           dd($result);
            if (session()->has('verification_code')) {
                session()->forget(['verification_code']);
//               session(['verification_code'=>$code]);
                session()->put('verification_code', $code);

            } else {
                session(['verification_code' => $code]);
            }
            session()->put('mobileMessage', trans('userFront.sendSms'));
        }


//       dd($user);

        return $user;
    }
}
