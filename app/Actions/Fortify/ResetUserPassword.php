<?php

namespace App\Actions\Fortify;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\ResetsUserPasswords;

class ResetUserPassword implements ResetsUserPasswords
{
    use PasswordValidationRules;

    /**
     * Validate and reset the user's forgotten password.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function reset($user, array $input)
    {
//        dd('reset');
        $messages = [
            "name.required" => trans( 'auth.Required' ),
            "password.required" => trans( 'auth.passwordRequired' ),
            "password_confirmation.required" => trans( 'auth.password_confirmationRequired' ),
            "password_confirmation.same" => trans( 'auth.password_confirmationSame' ),
            "password.min" => trans( 'auth.password_min' ),
            "email.email" => trans( 'auth.validateIdentification' ),
            "email.unique" => trans( 'auth.validateEmail' ),
        ];
        Validator::make($input, [
//            'password' => $this->passwordRules(),
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password'
        ], $messages)->validate();

        $user->forceFill([
            'password' => Hash::make($input['password']),
        ])->save();

//        return redirect()->route('login',['locale'=>App::getLocale()]);
    }
}
