<?php
namespace App\Traits;


use App\Models\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
trait WithRestaurantTrait {

    public function setRoleForManager()
    {
        $this->loggedInUser=Auth::user();
        $this->loggedInUser->role_id==2 ? $this->showSelectCountry=true : $this->showSelectCountry=false;
        if($this->loggedInUser->role_id==8)
        {
            $this->selectedCountry=$this->loggedInUser->country_id;
            $this->selectedCity=$this->loggedInUser->city_id;
        }
    }
    public function getNewRestaurantCount()

    {
        $user=Auth::user();
        if($user->role_id==2)
        {
            $restaurant = ServiceProvider::select('id', 'registration_state', 'type')
                ->where('registration_state', 0)
                ->where('type', "res")
                ->get();
        }elseif ($user->role_id==8)
        {
            $restaurant=ServiceProvider::select('id','registration_state','type','user_id')
                ->whereHas('city', function (Builder $query) use ($user) {
                    $query->where('cities.id',  $user->city_id);
                })
                ->where('registration_state',0)
                ->where('type','res')
                ->get();
        }

        return count($restaurant);
    }

    public function getNewShopsCount()

    {
        $user=Auth::user();
        if($user->role_id==2)
        {
            $shops=ServiceProvider::select('id','registration_state','type')
                ->where('registration_state',0)
                ->where('type','ven')
                ->get();
        }elseif($user->role_id==8)
        {
            $shops=ServiceProvider::select('id','registration_state','type','user_id')
                ->whereHas('city', function (Builder $query) use ($user) {
                    $query->where('cities.id',  $user->city_id);
                })
                ->where('registration_state',0)
                ->where('type','ven')
                ->get();
        }

//        dd($shops);

        return count($shops);
    }


}
