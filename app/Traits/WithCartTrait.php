<?php

namespace App\Traits;


use App\Models\Food;
use Illuminate\Support\Facades\Session;

trait WithCartTrait
{


    public function addToCart($id,$name,$price,$photo,$size)
    {

        $this->cart1 = session()->get('cart');
//        dd($cart);
//        $cart=null;
//        session()->put('cart', $cart);
//
//        return 1;
//        session()->forget('cart');
//       dd( session()->forget('cart') );
//        session()->forget('cart');
//        dd($id,$name,$price,$photo,$size ,$cart);

//        dd($cart);
        // if cart is empty then this the first product
        if (!$this->cart1) {
//            dd($cart);
            $this->cart1 = [
                $id => [
                    "name" => $name,
                    "quantity" => 1,
                    "price" => $price,
                    "photo" => $photo,
                    "size" => $size
                ]
            ];

            session()->put('cart', $this->cart1);

            return 1;
        }

        // if cart not empty then check if this product exist then increment quantity
        if (isset($this->cart1[$id])) {

            $this->cart1[$id]['quantity']= $this->cart1[$id]['quantity'] + 1;

//            session()->increment('quantity', $incrementBy = 1);
//dd($this->cart1[$id]['quantity']);
            session()->put('cart', $this->cart1);

            return 2;

        }
//        dd("+++");
        // if item not exist in this->cart1 then add to this->cart1 with quantity = 1
        $this->cart1[$id] = array(
            "name" => $name,
            "quantity" => 1,
            "price" => $price,
            "photo" => $photo,
            "size" => $size
        );
//        $product= array(
//            'id'=>$id,
//            "name" => $name,
//            "quantity" => 1,
//            "price" => $price,
//            "photo" => $photo,
//            "size" => $size
//        );
//        array_push($this->cart1,  $product);

//        $this->cart1->push($product);
//        array_push($this->cart1[$id],$product);
        session()->put('cart', $this->cart1);
//        dd($this->cart1);
        return 3;


    }


    public function removeItems($id)
    {
        $this->cart1 = session()->get('cart');
        unset($this->cart1);
        session()->put('cart', $this->cart1);
    }

}
