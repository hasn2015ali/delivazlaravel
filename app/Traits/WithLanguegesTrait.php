<?php
namespace App\Traits;
use App\Models\Language;

trait WithLanguegesTrait {
     function getAllLangueges()
    {
        $languages=Language::all();
        return $languages;
    }

    public function getAllLanguegesCount()
    {
        $languages=Language::all();
        return count($languages);
    }
}
