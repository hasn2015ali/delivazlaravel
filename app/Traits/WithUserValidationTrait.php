<?php
namespace App\Traits;
use App\Mail\EmailVerificationCodeSend;
use App\Models\Language;
use App\Notifications\ClickSendTest;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberFormat;
use Brick\PhoneNumber\PhoneNumberParseException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

trait WithUserValidationTrait {

    public function checkEmail($email)
    {
        $emailCheck = DB::table('users')
            ->select('email')
            ->where('email',$email)
            ->whereNotNull('email')
            ->first();
        if($emailCheck !=null )
        {
            $m=trans( 'user.emailVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        else
        {
            return 0;
        }
    }
    public function checkServiceProviderName($name)
    {
        //check name
        $nameCheck = DB::table('service_providers')
            ->select('name')
            ->where('name',$name)
            ->whereNotNull('name')
            ->first();
        if($nameCheck !=null )
        {
            $m=trans( 'restaurant.serviceProviderNameVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1 ;
        }
        else
        {
//            return 0;
            $slug= str_replace(' ', '-', $name);
            $slugCheck = DB::table('service_providers')
                ->select('slug')
                ->where('slug',$slug)
                ->whereNotNull('slug')
                ->first();
            if($slugCheck !=null )
            {
                $m=trans( 'restaurant.serviceProviderSlugVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }
            else
            {
                return 0;
            }
        }

        //check for slug

    }

    public function checkPhone($phone)
    {
        $phoneCheck = DB::table('users')
            ->select( 'phone')
            ->where('phone',$phone)
            ->first();
        if($phoneCheck!=null)
        {
            $m=trans( 'user.phoneVal' );
            $this->emit('alert',['icon'=>'error','title'=>$m]);
            return 1;
        }
        else
        {
            return 0;
        }
    }

    public function sendVerificationCodeToPhone($user)
    {
        $code=rand(1111,9999);
        try {
            $user->notify(new ClickSendTest( $code));
            session()->put('mobileMessage',  trans( 'userFront.sendSms' ));
            session(['start_user_time_for_phone'=>time()]);
        }
        catch (\Exception $e) {
            // do something when error
//               return $e->getMessage();
            session()->put('mobileMessage',  trans( 'userFront.FailedToSendCodeToPhone' ));
        }

//           dd($result);
        if(session()->has('verification_code'))
        {
            session()->forget(['verification_code']);
//               session(['verification_code'=>$code]);
            session()->put('verification_code',  $code);
        }
        else
        {
            session(['verification_code'=>$code]);
        }
    }
    public function sendVerificationCodeToEmail()
    {
        $submitedCode=rand(1111,9999);
        try {
            Mail::to($this->email)->send(new EmailVerificationCodeSend( $submitedCode));
            session()->put('emailMessage',  trans( 'userFront.emailRegisterSend' ));
            session(['start_user_time_for_Email'=>time()]);
        }
        catch (\Exception $exception)
        {
            session()->put('emailMessage',  trans( 'userFront.emailIsNotSend' ));

        }
        if(session()->has('email_verification_code'))
        {
            session()->forget(['email_verification_code']);
            session()->put('email_verification_code',  $submitedCode);
        }
        else
        {
            session(['email_verification_code'=>$submitedCode]);
        }
    }

    public function IsValidPhone($countryCode,$phoneNumber)
    {
        $number=null;
        $validPhone=null;
//        dd("+".$countryCode.$phoneNumber);
        try {
            $number = PhoneNumber::parse("+".$countryCode.$phoneNumber);
            if($number->isValidNumber()) //isValidNumber or isPossibleNumber
            {
                $validPhone=$number->format(PhoneNumberFormat::INTERNATIONAL);
            }
            else
            {
//                $this->addError('phone', 'You Have Entered Invalid Phone Number, Please Enter Another One');
                $validPhone=null;
            }
        }
        catch (PhoneNumberParseException $e) {
            $validPhone=null;
//            $this->addError('phone', 'You Have Entered Invalid Phone Number, Please Enter Another One');
        }
        return $validPhone;
    }

    public function getPhoneNumberAndCountryCode($fullNumber ,$filedName=null,$errorMessage=null)
    {

//        dd($fullNumber,$filedName,$errorMessage);

        $number=null;
        try {
            $number = PhoneNumber::parse("+".$fullNumber);
//            dd("lll");

        }
        catch (PhoneNumberParseException $e) {
            if($filedName!=null and $errorMessage !=null)
            {
//                dd("jjj");
                $this->addError($filedName,$errorMessage);

            }
//            dd($e->getMessage(),$number);
            // 'The string supplied is too short to be a phone number.'
        }
        if($number!=null)
        {
            $res['mobileNumber']=$number->getNationalNumber();
            $res['countryCode']="+".$number->getCountryCode();

        }
        else
        {
            $res['mobileNumber']=$fullNumber;
        }
//        dd($res);

        return $res;
    }

    public function setRoleID()
    {
        if($this->type=="Manager")
        {
            $this->rolId="2";
        }
        elseif ($this->type=="Translator")
        {
            $this->rolId="7";
        }
        elseif ($this->type=="COO")
        {
            $this->rolId="3";

        }
        elseif ($this->type=="Delivery_System_Team")
        {
            $this->rolId="4";

        }
        elseif ($this->type=="Delivery_Costumer_Team")
        {
            $this->rolId="5";

        }
        elseif ($this->type=="Delivery_workers")
        {
            $this->rolId="6";

        }
        elseif ($this->type=="Customers")
        {
            $this->rolId="1";

        }
        elseif ($this->type=="Data_Entry_Manager")
        {
            $this->rolId="8";

        }
        elseif ($this->type=="restaurant")
        {
            $this->rolId="9";

        }
        elseif ($this->type=="vendor")
        {
            $this->rolId="10";

        }
    }

    public function validateCoo()
    {
        if($this->type=='COO')
        {
            if($this->selectedCountry==null)
            {
                $m=trans( 'user.selectedCountryVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }

        }
    }

    public function validateDeliveryWorker()
    {
        if($this->type=='Delivery_workers')
        {
            if($this->Select_Delivery_Manager==null)
            {
                $m=trans( 'user.DeliveryWorkersVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }

        }
    }

    public function validatePartTime()
    {
        if($this->workType==1)
        {
            if(empty($this->daySelected))
            {
                $m=trans( 'user.dayVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1;
            }
            if(empty($this->partTimeSelected))
            {
                $m1=trans( 'user.HourVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m1]);

                return 1;
            }
        }

    }

    public function validateOthers()
    {
        if($this->type=='Delivery_System_Team' or $this->type=='Delivery_Costumer_Team'
            or $this->type=='Delivery_workers' or $this->type=='Customers' or $this->type=='Data_Entry_Manager')
        {
            if($this->selectedCountry==null or $this->selectedCountry=="---")
            {
                $m=trans( 'user.selectedCountryVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }
            if($this->selectedCity==null or $this->selectedCity=="---" )
            {
                $m=trans( 'user.selectedCityVal' );
                $this->emit('alert',['icon'=>'error','title'=>$m]);
                return 1 ;
            }

        }
    }

//    public function validateServiceProvider()
//    {
//
//    }
}
