<?php

namespace App\Traits;


use App\Models\Food;
use Illuminate\Support\Facades\Session;

trait WithFoodToasts
{
    public function openEditBasicInfo()
    {
        $this->editBasicInfo=true;
    }
    public function cancelBasicInfo()
    {
        $this->editBasicInfo=false;
    }
    public function closeAdditionPhotoToast()
    {
        $this->openNewPhoto=false;
        $this->newAdditionalPhoto=null;
    }
    public function openAdditionPhotoToast()
    {
        $this->openNewPhoto=true;
    }
    public function openDeleteToast()
    {
        $this->openDeleteToast=true;

    }
    public function closeToast() //delete Window
    {
        $this->photoID=null;
        $this->openDeleteToast=false;
    }

    //sizes toasts
    public function openNewSize()
    {
        $this->openNewSize=true;
    }
    public function cancelNewSize()
    {
        $this->openNewSize=false;
        $this->newSizeUpdate=null;
    }

    //addition toasts
    public function closeAdditionToast()
    {
        $this->openNewAddition=false;
        $this->updatedAddition=null;
    }
    public function openAdditionToast()
    {
        $this->openNewAddition=true;
    }
}
