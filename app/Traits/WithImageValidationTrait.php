<?php
namespace App\Traits;


trait WithImageValidationTrait {
  public function imageValidation($imageExtension)
  {
      $allow=['png','gif','jpg','PNG','Gif','jpeg','JPG','JPEG','GIF'];

      if(!in_array($imageExtension,$allow))
      {
          $m=trans( 'masterControl.validateImage' );
          $this->emit('alert',['icon'=>'error','title'=>$m]);
          return 1;
      }
      else
      {
          return 0;
      }


  }
}
