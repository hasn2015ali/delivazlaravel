<?php

namespace App\Traits;

use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\Language;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

trait WithCountryAndCity
{

    public function checkCountryNameInURL($countryTrans)
    {
        if($countryTrans!=null)
        {
            $countryTransCheck = CountryTranslation::where('name', $countryTrans)->first();
//        dd($countryTransCheck);
            if ($countryTransCheck != null) {
                $countryTransCheck->toArray();
//            dd( $countryTransCheck->toArray());
                if ($countryTransCheck['code_lang'] != "en") {
                    $country = CountryTranslation::where('country_id', $countryTransCheck['country_id'])
                        ->where('code_lang', 'en')
                        ->first()->name;
                    return $country;
                } else {
                    return $countryTrans;
                }
            } else {
                return "notfound";
//            dd('notfound');
            }
        }
        else
        {
            return null;
        }

    }

    public function checkCityNameInURL($cityTrans)
    {

        if ($cityTrans != null) {
            $cityTransCheck = CityTranslation::where('name', $cityTrans)->first();
//        dd($cityTransCheck);
            if ($cityTransCheck != null) {
                $cityTransCheck->toArray();
//            dd( $cityTransCheck->toArray());
                if ($cityTransCheck['code_lang'] != "en") {
                    $city = CityTranslation::where('city_id', $cityTransCheck['city_id'])
                        ->where('code_lang', 'en')
                        ->first()->name;
                    return $city;
                } else {
                    return $cityTrans;
                }
            } else {
                return "notfound";
//            dd('notfound');
            }
        }
        else
        {
            return null;
        }
    }

    public function getCountries()
    {
        $langCode = App::getlocale();
//        $this->countries = CountryTranslation::with('country:countries.id')
//            ->select('name','country_id','code_lang')
//            ->where('code_lang', $langCode)
//            ->orderBy('name','asc')
//            ->get();
//        $this->languageSelected=Language::select('id')->where('code',$langCode)->first();


//        Cache::forget('allCountries');
        if (session()->has('oldLang')) {
            $oldLang = session()->get('oldLang');
            if ($oldLang == null or $oldLang != App::getLocale()) {
                session(['oldLang' => App::getLocale()]);
                Cache::forget('allCountries');
            }
        } else {
            session(['oldLang' => App::getLocale()]);
            Cache::forget('allCountries');
        }

        $this->countries = Cache::remember('allCountries', 60 * 60 * 24, function () use ($langCode) {
            return CountryTranslation::select('id', 'name', 'country_id', 'code_lang')
                ->where('code_lang', $langCode)
                ->orderBy('name', 'asc')
                ->get();
        });
    }

    public function getCitiesForCountry($countryID)
    {
//        dd($countryID);
//        $this->cities=Cache::remember('allCities',60*60*24,function ()use ($countryID){
//            return CityTranslation::with('city')->where('lang_id',$this->languageSelected->id)
//                ->where('country_id',$countryID)
//                ->orderBy('name','asc')
//                ->get();
//        }) ;
        $langCode = App::getlocale();
        $this->cities = CityTranslation::with('city')->where('code_lang', $langCode)
            ->where('country_id', $countryID)
            ->orderBy('name', 'asc')
            ->get();
    }

    public function getCountryId($countryName)
    {
        $country = CountryTranslation::where('code_lang', 'en')
            ->where('name', $countryName)
            ->first();
        return $country->country_id;
    }

    public function getCityId($cityName)
    {
        $city = CityTranslation::where('code_lang', 'en')
            ->where('name', $cityName)
            ->first();
        return $city->city_id;
    }

    public function getCountryName($countryId, $lang)
    {
//        dd($countryId,$lang);
        $country = CountryTranslation::where('code_lang', $lang)
            ->where('country_id', $countryId)
            ->first();
//        dd($countryId,$lang,$country->name);

        return $country->name;

    }

    public function getCityName($cityId, $lang)
    {
        $city = CityTranslation::where('code_lang', $lang)
            ->where('city_id', $cityId)
            ->first();
        return $city->name;
    }


    public function getCountryDetailsByName($countryName)
    {
        $country = CountryTranslation::with('country')->where('code_lang', 'en')
            ->where('name', $countryName)
            ->first();
        return $country;
    }

    public function getCityDetailsByName($cityName)
    {
        $city = CityTranslation::with('city')
            ->where('code_lang', 'en')
            ->where('name', $cityName)
            ->first();
        return $city;
    }

    public function getCountryCurrency()
    {
        $countryDetails = CountryTranslation::with('country')
            ->where('name', $this->country)
            ->first();
        if (isset($countryDetails->country->currency_code)) {
            return $countryDetails->country->currency_code;
        } else {
            return null;
        }
    }


    public function getCountriesCodes()
    {
        $countriesCode = Country::select('code_number')
            ->where('code_number', "!=", 0)
            ->get()
            ->toArray();

        return $countriesCode;

    }

}
