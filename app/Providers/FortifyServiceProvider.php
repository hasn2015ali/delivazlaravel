<?php

namespace App\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Http\Request;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        Fortify::ignoreRoutes();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);
//        Fortify::loginView(function (){
//            return view('auth.login');
//        });
//
//        Fortify::loginView(fn () => view('auth.adminLogin'));
//        Fortify::registerView(fn () => view('auth.register'));
        Fortify::loginView(function (Request $request) {
//            App::setlocale($lang);
//            dd($request->locale);
            $country = null;
            $city = null;
            $previousURL = url()->previous();

            $refusedURLArray =
                [asset(App::getLocale() . '/login'), asset(App::getLocale() . '/register')
                    , asset(App::getLocale() . '/RegisterShopOrRestaurant'),
                    asset(App::getLocale() . '/forgot-password')];

//            dd(in_array($previousURL,$refusedURLArray),$previousURL);
            if (isset($previousURL) and !in_array($previousURL, $refusedURLArray)) {

                session(['previousURL' => $previousURL]);
            }
//            dd(isset($previousURL) and !in_array($previousURL, $refusedURLArray),url()->previous(), session(['previousURL']));

//            dd(url()->previous());

            return view('public.user.login', compact('city', 'country'));
//            return view('auth.adminLogin');
        });
//        Fortify::loginView(function () {
//            return view('auth.adminLogin');
//        });
        Fortify::registerView(function () {
            $country = null;
            $city = null;

//

            return view('public.user.register', compact('country', 'city'));
        });
        Fortify::requestPasswordResetLinkView(function () {
            $country = null;
            $city = null;
            return view('auth.passwords.reset', compact('city', 'country'));
        });

        Fortify::authenticateUsing(function (Request $request) {
            $messages = [
                "email.numeric" => trans('auth.validateIdentification'),
                "email.email" => trans('auth.validateIdentification')
            ];

            if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                $request->validate(['email' => 'email'], $messages);
                $email = $request->email;
                $user = User::where('email', $email)
                    ->first();
            } else {
//            dd('phone');
                $request->validate(['email' => 'numeric'], $messages);
                $phone = $request->email;
                $user = User::where('phone', $phone)
                    ->first();
            }


            if ($user &&
                Hash::check($request->password, $user->password)) {
                return $user;
            }
        });

        Fortify::requestPasswordResetLinkView(function () {
            $country = null;
            $city = null;
            return view('public.user.forget-password', compact('country', 'city'));

        });

        Fortify::resetPasswordView(function ($request) {
            $country = null;
            $city = null;
            return view('public.user.reset-password', ['request' => $request, 'country' => $country, 'city' => $city]);
        });


    }
}
