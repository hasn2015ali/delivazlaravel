<?php

namespace App\Providers;



use App\Helpers\Cart\Cart;
use App\Helpers\Cart\CartAdditions\Additions;
use App\Helpers\GuestAddresses\Addresses;
use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Country;
use App\Models\CountryTranslation;
use App\Models\MealFilterTrans;
use App\Models\ProductFilterTrans;
use App\Observers\CityObserver;
use App\Observers\CityTranslationObserver;
use App\Observers\CountryObserver;
use App\Observers\CountryTranslationObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */


    public function register()
    {
        //
//        App::bind('cart', function()
//        {
//            return new Cart;
//        });

        $this->app->bind('cart',function (){
            return new Cart;
        });

        $this->app->bind('addition',function (){
            return new Additions;
        });

        $this->app->bind('addresses',function (){
            return new Addresses;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $firstMealFilter=MealFilterTrans::where('code_lang','en')->first();
        $firstProductFiler=ProductFilterTrans::where('code_lang','en')->first();


        View::share('firstMealFilter', $firstMealFilter);
        View::share('firstProductFiler',$firstProductFiler);
        Country::observe(CountryObserver::class);
        CountryTranslation::observe(CountryTranslationObserver::class);
        City::observe(CityObserver::class);
        CityTranslation::observe(CityTranslationObserver::class);

    }
}
