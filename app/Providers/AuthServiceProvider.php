<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('is-manager',function ($user){
//            dd( $user->hasAnyRole('maneger'));
            return $user->hasAnyRole('maneger');
        });

        Gate::define('is-translator',function ($user){
            return $user->hasAnyRole('translator');
        });

        Gate::define('is-coo',function ($user){
            return $user->hasAnyRole('coo');
        });

        Gate::define('is-data_entry_manager',function ($user){
            return $user->hasAnyRole('data_entry_manager');
        });

        Gate::define('is_delivery_system_team',function ($user){
            return $user->hasAnyRole('delivery_system_team');
        });

    }
}
