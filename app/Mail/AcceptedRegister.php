<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AcceptedRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $text;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text)
    {
        $this->text=$text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject(trans( 'serviceProvider.RegisterCompleted' ))
            ->from('support@delivaz.com','Delivaz Support')
            ->view('emails.register.accept');
    }
}
