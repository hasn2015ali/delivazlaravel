<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Swift_Mime_SimpleMessage;

class EmailVerificationCodeSend extends Mailable
{
    use Queueable, SerializesModels;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        //
        $this->code = $code;
    }
    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {

        return  $this
//->withSwiftMessage(function ($message) {
//            $message->setPriority(Swift_Mime_SimpleMessage::PRIORITY_HIGHEST);
////            $message->getHeaders()->addTextHeader(
//                'Content-Type', 'text/html; charset=\"iso-8859-1\"\n'
//            );
//            $message->getHeaders()->addTextHeader(
//                'X-Priority', '1 (Highest)\n'
//            );
//            $message->getHeaders()->addTextHeader(
//                'X-MSMail-Priority', 'High\n'
//            );
//            $message->getHeaders()->addTextHeader(
//                'Importance', 'High'
//            );

//        })
            ->subject(trans( 'userFront.EmailVerification' ))
            ->from('support@delivaz.com','Delivaz Support')
            ->view('emails.email.verify');
    }
}
