<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFilterTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_filter_trans', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("code_lang")->default('en');
            $table->unsignedBigInteger("filter_id");
            $table->unsignedBigInteger("product_filter_id");
            $table->unsignedBigInteger("lang_id");
            $table->foreign('product_filter_id')
                ->references('id')->on('product_filters')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_filter_trans');
    }
}
