<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityShapTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_shap_translations', function (Blueprint $table) {
            $table->id();
            $table->string('trans');
            $table->string("code_lang")->default('en');
            $table->unsignedBigInteger("shape_id");
            $table->unsignedBigInteger("shape_number");
            $table->unsignedBigInteger('city_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_shap_translations');
    }
}
