<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food', function (Blueprint $table) {
            $table->id();
            $table->float("price")->default(0);
            $table->string("photo")->nullable();
            $table->integer("time_hour")->default(0);
            $table->integer("time_minutes")->default(0);
            $table->unsignedBigInteger("service_provider_id");
            $table->foreign('service_provider_id')
                ->references('id')->on('service_providers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food');
    }
}
