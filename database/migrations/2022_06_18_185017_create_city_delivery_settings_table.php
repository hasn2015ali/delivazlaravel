<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityDeliverySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_delivery_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("city_id");
            $table->float("price_in_area");
            $table->float("price_out_area");
            $table->float("worker_in_Ratio");
            $table->float("worker_out_Ratio");
            $table->unsignedBigInteger("tax_id");
            $table->integer("min_distance");
            $table->integer("max_distance");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_delivery_settings');
    }
}
