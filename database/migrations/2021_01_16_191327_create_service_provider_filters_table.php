<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProviderFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_filters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("service_provider_id");
            $table->unsignedBigInteger("filter_id");
            $table->foreign('service_provider_id')
                ->references('id')->on('service_providers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_filters');
    }
}
