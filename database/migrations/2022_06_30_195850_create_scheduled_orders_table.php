<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_orders', function (Blueprint $table) {
            $table->id();
            $table->date("date")->nullable();
            $table->time("time")->nullable();
            $table->boolean("repeatOrder")->nullable();
            $table->date("startDate")->nullable();
            $table->string("intervals")->nullable();
            $table->boolean("ongoing")->nullable();
            $table->date("endDate")->nullable();

            $table->unsignedBigInteger("order_id");
            $table->foreign('order_id')
                ->references('id')->on('customer_orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_orders');
    }
}
