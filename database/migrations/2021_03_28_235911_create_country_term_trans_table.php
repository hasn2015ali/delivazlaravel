<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTermTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_term_trans', function (Blueprint $table) {
            $table->id();
            $table->text("address")->nullable();
            $table->text("content")->nullable();
            $table->string("code_lang")->default('en');
            $table->unsignedBigInteger("country_id");
            $table->unsignedBigInteger("term_id");
            $table->foreign('term_id')
                ->references('id')->on('country_terms')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country_term_trans');
    }
}
