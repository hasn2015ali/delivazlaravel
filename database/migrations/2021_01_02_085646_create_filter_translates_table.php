<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filter_translates', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("code_lang")->default('en');
            $table->integer('type')->default(1);
            $table->unsignedBigInteger("filter_id");
            $table->unsignedBigInteger("lang_id");
            $table->foreign('filter_id')
                ->references('id')->on('filters')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filter_translates');
    }
}
