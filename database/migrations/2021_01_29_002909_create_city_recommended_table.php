<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCityRecommendedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_recommended', function (Blueprint $table) {
            $table->id();
            $table->text("content")->nullable();
            $table->string("type")->nullable();
            $table->string("photo")->nullable();
            $table->unsignedBigInteger("service_provider_id");
            $table->unsignedBigInteger("city_id");
            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_recommended');
    }
}
