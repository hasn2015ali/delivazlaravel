<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBannerTypeAndServiceProviderIdToCityServiceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('city_service_providers', function (Blueprint $table) {
            //
            $table->unsignedBigInteger("service_provider_id");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('city_service_providers', function (Blueprint $table) {
            //
        });
    }
}
