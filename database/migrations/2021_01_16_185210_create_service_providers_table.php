<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_providers', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string("nameEN")->nullable();
            $table->text("address")->nullable();
            $table->integer("minimum_order_amount")->nullable();
            $table->string("mode")->nullable();
            $table->float("delivery_man_commission")->nullable();
            $table->float("pick_commission")->nullable();
            $table->bigInteger("phone1")->default(0);
            $table->bigInteger("phone2")->default(0);
            $table->bigInteger("phone3")->default(0);
            $table->string("type")->nullable();
            $table->string("avatar")->nullable();
            $table->unsignedBigInteger("user_id");
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_providers');
    }
}
