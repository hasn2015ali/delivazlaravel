<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_carts', function (Blueprint $table) {
            $table->id();
            $table->string('idDES');
            $table->unsignedBigInteger('itemID');
            $table->unsignedBigInteger('serviceProviderID');
            $table->string('price');
            $table->string('size')->nullable();
            $table->string('photo')->nullable();
            $table->string('type');
            $table->string('name');
            $table->string('quantity');
            $table->json('additions');
            $table->unsignedBigInteger("user_id");
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_carts');
    }
}
