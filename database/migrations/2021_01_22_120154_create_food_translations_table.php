<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_translations', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->text("content")->nullable();
            $table->string("code_lang")->default('en');
            $table->unsignedBigInteger("lang_id");
            $table->unsignedBigInteger("food_id");
            $table->unsignedBigInteger("service_provider_id");
            $table->foreign('food_id')
                ->references('id')->on('food')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_translations');
    }
}
