<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderToAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_to_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('addressName')->nullable();
            $table->string('firstNameRecipient')->nullable();
            $table->string('lastNameRecipient')->nullable();
            $table->string('mobileNumberRecipient')->nullable();
            $table->string('buildingNameRecipient')->nullable();
            $table->string('addressOnMap')->nullable();
            $table->string('firstNameSender')->nullable();
            $table->string('lastNameSender')->nullable();
            $table->string('mobileNumberSender')->nullable();
            $table->unsignedBigInteger("user_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_to_addresses');
    }
}
