<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orders', function (Blueprint $table) {
            $table->id();
            $table->string('identifier')->nullable();
            $table->unsignedBigInteger("address_id")->nullable();
            $table->string('paymentType')->nullable();
            $table->string('orderOption')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('deliveryCharge')->nullable();
            $table->string('tax')->nullable();
            $table->boolean('orderTo')->nullable();
            $table->string('state')->nullable();
            $table->unsignedBigInteger("user_id");
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_orders');
    }
}
