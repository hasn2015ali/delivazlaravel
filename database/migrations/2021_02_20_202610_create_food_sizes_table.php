<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFoodSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('food_sizes', function (Blueprint $table) {
            $table->id();
            $table->float("price")->default(0);
            $table->string("size")->nullable();
            $table->unsignedBigInteger("food_id");
            $table->foreign('food_id')
                ->references('id')->on('food')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('food_sizes');
    }
}
