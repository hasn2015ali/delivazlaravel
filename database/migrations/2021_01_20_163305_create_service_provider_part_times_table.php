<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceProviderPartTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_provider_part_times', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("day_id");
            $table->string("day_state");

            $table->string('startTimeHour');
            $table->string('startTimeMinutes');
            $table->string('endTimeHour');
            $table->string('endTimeMinutes');
            $table->unsignedBigInteger("service_provider_id");
            $table->foreign('service_provider_id')
                ->references('id')->on('service_providers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_provider_part_times');
    }
}
