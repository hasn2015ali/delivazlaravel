<?php
use \App\Http\Controllers\FrontEnd\ServiceProviderController;
Route::group(['middleware' => ['lang', 'auth','userState','ensureServiceProviderType'], 'prefix' => '{locale}/'], function ($locale) {

    Route::get('/Basic-Info', [ServiceProviderController::class, 'ShowInfo'])->name('Basic-Info');
    Route::get('/Scheduling-Time', [ServiceProviderController::class, 'ShowSchedulingTime'])->name('Scheduling-Time');
    Route::get('/Banners', [ServiceProviderController::class, 'ShowBanners'])->name('Banners');
    Route::get('/Sizes', [ServiceProviderController::class, 'ShowSizes'])->name('Sizes');
    Route::get('/Orders', [ServiceProviderController::class, 'ShowOrders'])->name('Orders');
    Route::get('/Reviews', [ServiceProviderController::class, 'ShowReviews'])->name('Reviews');
    Route::get('/Edit-Menu', [ServiceProviderController::class, 'EditMyMenu'])->name('EditMenu');

});// end locale;

Route::group(['middleware' => ['lang', 'auth','userState','trustShopOwner'], 'prefix' => '{locale}/'], function ($locale) {

    Route::get('/Products', [ServiceProviderController::class, 'ShowProducts'])->name('ControlPanel.Products');
    Route::get('/Add-New-Product', [ServiceProviderController::class, 'AddNewProduct'])->name('AddNewProduct');
    Route::get('/Update-Product/{id}', [ServiceProviderController::class, 'UpdateProduct'])->name('UpdateProduct');


});// end locale;

Route::group(['middleware' => ['lang', 'auth','userState','trustRestaurantOwner'], 'prefix' => '{locale}/'], function ($locale) {

    Route::get('/Food', [ServiceProviderController::class, 'ShowFoods'])->name('ControlPanel.Food');
    Route::get('/Add-New-Food', [ServiceProviderController::class, 'AddNewFood'])->name('AddNewFood');
    Route::get('/Update-Food/{id}', [ServiceProviderController::class, 'UpdateFood'])->name('UpdateFood');


});// end locale;
