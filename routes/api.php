<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\User\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|14|JypBqHoaYS3toHi2THfLCR2955zZ1EuQQ9dJcn8O
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/getCountryEn/{id}', [App\Http\Controllers\HomeController::class, 'getCountryEn'])->name("getCountryEn");
Route::get('/getCityEn/{id}', [App\Http\Controllers\HomeController::class, 'getCityEn'])->name("getCityEn");

Route::post('/apiLogin', [UserController::class, 'login'])->name("apiLogin");

Route::group(['middleware'=>'auth:sanctum'],function () {
//    Route::get('/getCountryEn/{id}', [App\Http\Controllers\HomeController::class, 'getCountryEn'])->name("getCountryEn");


});
