<?php

use App\Http\Controllers\UserFrontController;
use App\Models\Language;
use App\Models\OrderAddress;
use App\Models\OrderToAddress;
use App\Models\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

use App\Events\ServiceProviderJoined;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::group(['prefix' => '{locale}/{country?}/{city?}'], function() {
//Route::get('/', 'App\Http\Controllers\HomeController@index');
//Route::get('/cities', 'App\Http\Controllers\CountryController@getCities')->name("cities.get");
//});
// Route::get('/su',function(){
//     Artisan::call('storage:link');
//     Log::debug("adsaf");
//     return "storage ";
// });

include ('fortifyRoutes.php');
//customize logout method
Route::group(['prefix'=>'{locale}','middleware' =>  ['web','lang']], function() {
    Route::post('logout', [UserFrontController::class, 'logout'])->name('logout');
}) ;
Route::get('/addToCart/{id}', 'App\Http\Controllers\ControlPanelActionController@addToBasket')->name("addToCart");

//Route::get('/slug',function(){
//$s=\App\Models\ServiceProvider::all();
//foreach ($s as $e)
//{
//    $n=$e->name;
//    $slug= str_replace(' ', '-', $n);
//    $e->slug=$slug;
//    $e->save();
//}
//dd($s);

//});
Route::get('/testPusher', function () {
//    $id="ven";
//    event(new ServiceProviderJoined($id));
//    return "Event has been sent!";
});

Route::get('/test',function(){
//    $t=\App\Models\CountryTranslation::CountriesEn();
//    return $t;
    $country="country";
    $city="city";
//    $e1= ServiceProvider::cursorPaginate(5);
//    return \App\cart\CartFacade::sayTest("sss");
//
//   return view('public.category.product',compact('city','country'));
    return view('public.test',compact('city','country'));
//    return view('public.controlPanel.dasboard',compact('city','country'));

//    $s=\App\Models\City::with('serviceProviders')->find(119);
//    $s=\App\Models\ServiceProvider::with(['city','country'])->find(2);
//    $s=\App\Models\CityTranslation::with('serviceProviders')->where('city_id',119)->get();
//    dd($s);

//    OrderAddress::Create(
//        [
//            'addressName' => "fffff",
//            'firstNameRecipient' => "ggggg",
//            'lastNameRecipient' => "hhhh",
//            'mobileNumber' => "+380 94 576 6429",
//            'buildingName' => "kkkkkkk",
//            'addressOnMap' => "Tartous port, Almeena Street, Tartus‎, Syria",
//            'user_id' => 173,
//        ]);
//    OrderToAddress::Create(
//        [
//            'addressName' => "fffff",
//            'firstNameRecipient' => "ggggg",
//            'lastNameRecipient' => "hhhh",
//            'mobileNumberRecipient' => "+380 94 576 6429",
//            'buildingName' => "kkkkkkk",
//            'addressOnMap' => "Tartous port, Almeena Street, Tartus‎, Syria",
//            'user_id' => 173,
//        ]);

});
//Route::get('/test', 'App\Http\Controllers\PublicController@sendSMS')->name("test");


Route::get('/blockedUser',function(){

    return view('auth.block.blockAccount');

});
//Route::get('/RegisterISNotComplete',function(){
//
//    $lang=null;
//    $country=null;
//    $city=null;
//    return view('auth.RegisterISNotComplete',compact('country','city'));
//
//});

Route::get('/',function(){
    return redirect('/en');
});


//Route::get('/login',function(){
//    return redirect('/'.App::getLocale().'/login');
//})->name('login');

include('controlPanelRouts/managerRoutes.php');
include('controlPanelRouts/translatorRouts.php');
include('controlPanelRouts/cooRouts.php');
include('controlPanelRouts/dataEntryManager.php');
include('controlPanelRouts/allRolesRouts.php');
include('controlPanelRouts/dataEntryManagerWithTranslator.php');
include('controlPanelRouts/DeliverySystemTeam.php');





Route::get('/login/facebook', 'App\Http\Controllers\PublicController@redirectToFacebook')->name("login.facebook");
Route::get('/login/facebook/callBack', 'App\Http\Controllers\PublicController@handelFacebookCallBack')->name("login.callBackFacebook");

Route::get('/login/google', 'App\Http\Controllers\PublicController@redirectToGoogle')->name("login.google");
Route::get('/login/google/callBack', 'App\Http\Controllers\PublicController@handelGoogleCallBack')->name("login.callBackGoogle");

Route::get('/login/twitter', 'App\Http\Controllers\PublicController@redirectToTwitter')->name("login.twitter");
Route::get('/login/twitter/callBack', 'App\Http\Controllers\PublicController@handelTwitterCallBack')->name("login.callBackTwitter");

include('controlPanelFront/controlPanelRouts.php');
include('frontEndRouts.php');




//Auth::routes();



