<?php
//controlPanel Routes
Route::group(['middleware' => ['auth']], function($locale) {


    Route::post('/addPersonalPhotoFromProfile', [App\Http\Controllers\DropzoneController::class, 'addPersonalPhotoFromProfile'])->name('addPersonalPhotoFromProfile');
    Route::get('/fetchMyPersonalImage', [App\Http\Controllers\DropzoneController::class, 'fetchMyPersonalImage'])->name('fetchMyPersonalImage');
    Route::get('/showProfile', [App\Http\Controllers\AdminController::class, 'showProfile'])->name('showProfile');


});
Route::group(['middleware' => ['auth','lang','userState'],'prefix' => '{locale}/'], function($locale) {

//    Route::get('/home', [App\Http\Controllers\AdminController::class, 'redirectUser'])->name('home');
    Route::get('/Dashboard', [App\Http\Controllers\AdminController::class, 'showDashboard'])->name('Dashboard');
    Route::get('/dashboard', [App\Http\Controllers\AdminController::class, 'showDashboardForApplication'])->name('dashboard');



}) ;// end controlPanel Routes;
