<?php
//controlPanel Routes

Route::group(['middleware' => ['auth']], function($locale) {

    Route::post('/addPhotoFromAdmin', [App\Http\Controllers\DropzoneController::class, 'addPhotoFromAdmin'])->name('addPhotoFromAdmin');
    Route::get('/fetchPersonalImage/{id}', [App\Http\Controllers\DropzoneController::class, 'fetchPersonalImage'])->name('fetchPersonalImage');
//    Route::get('/fetchPersonalImage/{id}', [App\Http\Controllers\DropzoneController::class, 'fetchPersonalImage'])->name('fetchPersonalImage');
    Route::get('/fetchUserImages/{id}', [App\Http\Controllers\DropzoneController::class, 'fetchUserImages'])->name('fetchUserImages');
    Route::post('/addUserFiles', [App\Http\Controllers\DropzoneController::class, 'addUserFiles'])->name('addUserFiles');

    Route::get('/deleteImageDetail/{id}', [App\Http\Controllers\DropzoneController::class, 'deleteImageDetail'])->name('deleteImageDetail');

});

Route::group(['middleware' => ['auth','lang','coo','userState'],'prefix' => '{locale}/'], function($locale) {


    Route::get('/users/{type}', [App\Http\Controllers\AdminController::class, 'index'])->name('users');

//    Route::get('/showProfile', [App\Http\Controllers\AdminController::class, 'showProfile'])->name('showProfile');
    Route::get('/addUser/{type}', [App\Http\Controllers\AdminController::class, 'addUser'])->name('addUser');
    Route::get('/updateUser/{id}/{type}', [App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');


    Route::get('/areasInCity/{id}', [App\Http\Controllers\AdminController::class, 'showAreas'])->name('city.areas');

    Route::get('/deliveryInCity/{id}', [App\Http\Controllers\AdminController::class, 'showDeliveryInCity'])->name('city.delivery');

}) ;// end controlPanel Routes;
