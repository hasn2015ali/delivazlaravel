<?php
Route::group(['middleware' => ['auth','lang','dataEntryManagerTranslator','userState'],'prefix' => '{locale}/'], function($locale) {


/// must be data entry manager with translator and manger
    Route::get('/showCountries', [App\Http\Controllers\AdminController::class, 'showCountries'])->name('showCountries');
    Route::get('/showCountryCity/{id}', [App\Http\Controllers\AdminController::class, 'showCountryCity'])->name('showCountryCity');
    Route::get('/countryTranslate/{id}', [App\Http\Controllers\AdminController::class, 'countryTranslate'])->name('countryTranslate');
    Route::get('/cityTranslate/{id}', [App\Http\Controllers\AdminController::class, 'cityTranslate'])->name('cityTranslate');

    Route::get('/cityShapes/{id}', [App\Http\Controllers\AdminController::class, 'showCityShapes'])->name('cityShapes');
    Route::get('/cityBanners/{id}', [App\Http\Controllers\AdminController::class, 'showCityBanners'])->name('cityBanners');
    Route::get('/cityServiceProviders/{id}', [App\Http\Controllers\AdminController::class, 'showCityServiceProviders'])->name('cityServiceProviders');
    Route::get('/cityServiceProviderPages/{id}', [App\Http\Controllers\AdminController::class, 'showCityServiceProviderPages'])->name('cityServiceProviderPages');


    //    Route::get('/addUser/{type}', [App\Http\Controllers\AdminController::class, 'addUser'])->name('addUser');
//    Route::get('/updateUser/{id}/{type}', [App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');




}) ;// end controlPanel Routes;
