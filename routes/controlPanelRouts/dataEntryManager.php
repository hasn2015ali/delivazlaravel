<?php
Route::group(['middleware' => ['auth','lang','dataEntryManager','userState'],'prefix' => '{locale}/'], function($locale) {


//    Route::get('/users/{type}', [App\Http\Controllers\AdminController::class, 'index'])->name('users');
//
    Route::get('/restaurantRegistered', [App\Http\Controllers\AdminController::class, 'showRestaurantRegistered'])->name('restaurantRegistered');
// restaurant setting
    Route::get('/restaurantDetails/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantDetails'])->name('restaurantDetails');
    Route::get('/restaurantFilters/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantFilters'])->name('restaurantFilters');
    Route::get('/restaurantFood/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantFood'])->name('restaurantFood');
    Route::get('/restaurantWorkHours/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantWorkHours'])->name('restaurantWorkHours');
    Route::get('/restaurantBanners/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantBanners'])->name('restaurantBanners');
    Route::get('/restaurantDocuments/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantDocuments'])->name('restaurantDocuments');
    Route::get('/restaurantRatting/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantRatting'])->name('restaurantRatting');


    Route::get('/vendorRegistered', [App\Http\Controllers\AdminController::class, 'showVendorRegistered'])->name('vendorRegistered');
    Route::get('/vendorSetting/{id}', [App\Http\Controllers\AdminController::class, 'showVendorSetting'])->name('vendorSetting');

    // shop setting
    Route::get('/shopDetails/{id}', [App\Http\Controllers\AdminController::class, 'showShopDetails'])->name('shopDetails');
    Route::get('/shopFilters/{id}', [App\Http\Controllers\AdminController::class, 'showShopFilters'])->name('shopFilters');
    Route::get('/shopProducts/{id}', [App\Http\Controllers\AdminController::class, 'showShopProducts'])->name('shopProducts');
    Route::get('/shopWorkHours/{id}', [App\Http\Controllers\AdminController::class, 'showShopWorkHours'])->name('shopWorkHours');
    Route::get('/shopBanners/{id}', [App\Http\Controllers\AdminController::class, 'showShopBanners'])->name('shopBanners');
    Route::get('/shopDocuments/{id}', [App\Http\Controllers\AdminController::class, 'showShopDocuments'])->name('shopDocuments');
    Route::get('/shopRatting/{id}', [App\Http\Controllers\AdminController::class, 'showShopRatting'])->name('shopRatting');



    Route::get('/subscribers', [App\Http\Controllers\AdminController::class, 'showSubscribers'])->name('subscribers');
    Route::get('/subscribers-print/{country}/{city}', [App\Http\Controllers\ControlPanelActionController::class, 'downLoadPDF'])->name('subscribers-print');


//    Route::get('/restaurantsPage', [App\Http\Controllers\AdminController::class, 'showRestaurantsPage'])->name('restaurantsPage');
//    Route::get('/vendorsPage', [App\Http\Controllers\AdminController::class, 'showVendorsPage'])->name('vendorsPage');
//

/// must be data entry manager with translator and manger
//    Route::get('/showCountries', [App\Http\Controllers\AdminController::class, 'showCountries'])->name('showCountries');
//    Route::get('/showCountryCity/{id}', [App\Http\Controllers\AdminController::class, 'showCountryCity'])->name('showCountryCity');
//    Route::get('/countryTranslate/{id}', [App\Http\Controllers\AdminController::class, 'countryTranslate'])->name('countryTranslate');
//    Route::get('/cityTranslate/{id}', [App\Http\Controllers\AdminController::class, 'cityTranslate'])->name('cityTranslate');
//    Route::get('/citySetting/{id}', [App\Http\Controllers\AdminController::class, 'showCitySetting'])->name('citySetting');


    //    Route::get('/addUser/{type}', [App\Http\Controllers\AdminController::class, 'addUser'])->name('addUser');
//    Route::get('/updateUser/{id}/{type}', [App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');




}) ;// end controlPanel Routes;
