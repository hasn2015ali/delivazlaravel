<?php
Route::group(['middleware' => ['auth','lang','deliverySystemTeam','userState'],'prefix' => '{locale}/'], function($locale) {


    Route::get('/orders', [App\Http\Controllers\OrdersController::class, 'showOrders'])->name('system.orders');
//    Route::get('/restaurantSetting/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantSetting'])->name('restaurantSetting');


}) ;// end controlPanel Routes;
