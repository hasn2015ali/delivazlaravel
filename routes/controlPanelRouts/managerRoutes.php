<?php
//controlPanel Routes
Route::group(['middleware' => ['auth','lang','manager','userState'],'prefix' => '{locale}/'], function($locale) {

//    Route::get('/users/{type}', [App\Http\Controllers\AdminController::class, 'index'])->name('users');
//
//    Route::get('/showProfile', [App\Http\Controllers\AdminController::class, 'showProfile'])->name('showProfile');
//    Route::get('/addUser/{type}', [App\Http\Controllers\AdminController::class, 'addUser'])->name('addUser');
//    Route::get('/updateUser/{id}/{type}', [App\Http\Controllers\AdminController::class, 'updateUser'])->name('updateUser');

    Route::get('/workSystem', [App\Http\Controllers\AdminController::class, 'workSystem'])->name('workSystem');
    Route::get('/countrySetting/{id}', [App\Http\Controllers\AdminController::class, 'showCountrySetting'])->name('countrySetting');
    Route::get('/countryTaxes/{id}', [App\Http\Controllers\AdminController::class, 'showCountryTaxes'])->name('country.taxes');

    Route::get('/showTaxes', [App\Http\Controllers\AdminController::class, 'showTaxes'])->name('taxes.show');



}) ;// end controlPanel Routes;
