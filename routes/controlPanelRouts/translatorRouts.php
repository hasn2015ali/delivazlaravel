<?php
//controlPanel Routes
Route::group(['middleware' => ['auth','lang','translator','userState'],'prefix' => '{locale}/'], function($locale) {


    Route::get('/restaurantFilter', [App\Http\Controllers\AdminController::class, 'showRestaurantFilter'])->name('restaurantFilter');
    Route::get('/vendorsFilter', [App\Http\Controllers\AdminController::class, 'showVendorsFilter'])->name('vendorsFilter');
    Route::get('/restaurantFilterTranslate/{id}', [App\Http\Controllers\AdminController::class, 'showRestaurantFilterTranslate'])->name('restaurantFilterTranslate');
    Route::get('/vendorFilterTranslate/{id}', [App\Http\Controllers\AdminController::class, 'showVendorFilterTranslate'])->name('vendorFilterTranslate');

    Route::get('/mealsFilters/{id}', [App\Http\Controllers\AdminController::class, 'showMealsFilters'])->name('mealsFilters');
    Route::get('/mealFilterTranslate/{id}', [App\Http\Controllers\AdminController::class, 'showMealFilterTranslate'])->name('mealFilterTranslate');


    Route::get('/productsFilters/{id}', [App\Http\Controllers\AdminController::class, 'showProductsFilters'])->name('productsFilters');
    Route::get('/productFilterTranslate/{id}', [App\Http\Controllers\AdminController::class, 'showProductFilterTranslate'])->name('productFilterTranslate');

    Route::get('/terms-conditions', [App\Http\Controllers\AdminController::class, 'ShowTerms'])->name('terms-conditions');
    Route::get('/privacy-policy', [App\Http\Controllers\AdminController::class, 'ShowPrivacyPolicy'])->name('privacy-policy');
    Route::get('/cookies-policy', [App\Http\Controllers\AdminController::class, 'ShowCookiesPolicy'])->name('cookies-policy');
    Route::get('/faq-page', [App\Http\Controllers\AdminController::class, 'ShowFaqPage'])->name('faq-page');



}) ;// end controlPanel Routes;
