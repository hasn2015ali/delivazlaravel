<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserFrontController;
use App\Http\Controllers\PublicController;

Route::group(['middleware' => ['lang'], 'prefix' => '{locale}/'], function ($locale) {

    Route::post('/forgetMyPassword', 'App\Http\Controllers\PublicController@forgetMyPassword')->name("forgetMyPassword");

    Route::get('/About-Us', 'App\Http\Controllers\PublicController@showAboutPage')->name("About-Us");
    Route::get('/RegisterShopOrRestaurant', [PublicController::class ,'RegisterShopOrRestaurant'])->name("Register-Shop-Or-Restaurant");
    Route::get('/RegisterISNotComplete', [UserFrontController::class, 'RegisterISNotComplete'])->name('RegisterISNotComplete');


});// end locale;

Route::group(['middleware' => ['lang', 'auth'], 'prefix' => '{locale}/'], function ($locale) {

    Route::get('/Verify-My-Phone', 'App\Http\Controllers\PublicController@show')->name("Verify-My-Phone");
    Route::get('/Verify-My-Email', 'App\Http\Controllers\PublicController@showVerifyPage')->name("Verify-My-Email");


});// end locale;


Route::group(['middleware' => ['lang', 'auth','userState'], 'prefix' => '{locale}/'], function ($locale) {

   Route::get('/My-Wishlist', [UserFrontController::class, 'ShowWishlist'])->name('My-Wishlist');


});// end locale;


Route::group(['middleware' => ['lang'], 'prefix' => '{locale}/'], function ($locale) {


    Route::group(['prefix' => '{country}/'], function ($country) {


        Route::group(['prefix' => '{city}'], function ($city) {


        }); // end city;
//
        Route::get('/Terms-Conditions', [PublicController::class, 'ShowTerms'])->name('Terms-Conditions');
        Route::get('/Privacy-Policy', [PublicController::class, 'ShowPrivacyPolicy'])->name('Privacy-Policy');
        Route::get('/Cookies-Policy', [PublicController::class, 'ShowCookiesPolicy'])->name('Cookies-Policy');
//        Route::get('/FAQ', [App\Http\Controllers\PublicController::class, 'ShowFaqPage'])->name('FAQ');

    }); // end country;

    Route::get('/Terms-Conditions', [PublicController::class, 'ShowTerms'])->name('Terms-Conditions');
    Route::get('/Privacy-Policy', [PublicController::class, 'ShowPrivacyPolicy'])->name('Privacy-Policy');
    Route::get('/Cookies-Policy', [PublicController::class, 'ShowCookiesPolicy'])->name('Cookies-Policy');
    Route::get('/FAQ', [PublicController::class, 'ShowFaqPage'])->name('FAQ');


});// end locale;


Route::group(['middleware' => ['lang'], 'prefix' => '{locale}/'], function ($locale) {
    Route::get('/checkout', [UserFrontController::class,'checkout'])->name("user.checkout");

    Route::group(['prefix' => '{country}'], function ($country) {

        Route::group(['middleware' => 'ensureCityState','prefix' => '{city}'], function ($city) {

            Route::get('/AnyThing', [PublicController::class, 'showAnyThing'])->name("AnyThing");

            Route::get('/', [HomeController::class, 'index'])->name('index');
            Route::get('/{shape}', 'App\Http\Controllers\PublicController@showItemsInShape')->name("Items-In");
            Route::get('/Restaurant/{serviceProvider:slug}', 'App\Http\Controllers\PublicController@showRestaurantCity')->name("Restaurant");
            Route::get('/Shop/{serviceProvider:slug}', 'App\Http\Controllers\PublicController@showRestaurantCity')->name("Shop");



            Route::group(['prefix' => '{serviceProvider}'], function ($serviceProvider) {
                Route::get('/Food/{foodID}', [PublicController::class, 'showFood'])->name("Food");
                Route::get('/Product/{productID}', [PublicController::class, 'showProduct'])->name("Product");

            }); // end service provider;
        }); // end city;

        Route::get('/', [HomeController::class, 'index'])->name("locale.home");


    }); // end country;

    Route::get('/', [HomeController::class, 'index'])->name("locale.home");
    Route::get('/cities', 'App\Http\Controllers\CountryController@getCities')->name("cities.get");


});// end locale;
