const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/app2.js', 'public/js')
    .js('resources/js/pusherScripts.js', 'public/js')
    .sass('resources/sass/controlPanel/arabic/controlPanel_ar.scss', 'public/css/controlPanel_ar.css')
    .sass('resources/sass/controlPanel/english/controlPanel.scss', 'public/css/controlPanel.css')
    .sass('resources/sass/template/en/style.scss', 'public/css/template/en_style.css')
    // .sass('resources/sass/template/ar/style.scss', 'public/css/template/ar_style.css')
    // .sass('resources/sass/template/fre/style.scss', 'public/css/template/fre_style.css')
    // .sass('resources/sass/template/rus/style.scss', 'public/css/template/rus_style.css')
    // .sass('resources/sass/template/ukr/style.scss', 'public/css/template/ukr_style.css')
    .sass('resources/sass/template/en/controlPanel/controlPanelStyles.scss', 'public/css/template/controlPanel/controlPanelStyles.css');
